#!/usr/bin/python

from subprocess import call
import tempfile
import sys
import os
import shutil
import glob

if len(sys.argv) != 3:
  print "error: wrong number of arguments!"
  sys.exit()

source_dir=sys.argv[1]
number_of_threads=int(sys.argv[2])

class releaseInfo:
    def __init__(self, debian_or_ubuntu, release_name, release_version):
        self.debian_or_ubuntu = debian_or_ubuntu
        self.release_name = release_name
        self.release_version = release_version

vec = []
#vec.append(releaseInfo("debian", "squeeze", "6.0"))

#vec.append(releaseInfo("ubuntu", "precise", "12.04"))
vec.append(releaseInfo("debian", "sid", "8.0"))

#vec.append(releaseInfo("debian", "wheezy", "7.0"))

os.mkdir("ubuntu")
os.mkdir("ubuntu/amd64")
os.mkdir("ubuntu/i386")
os.mkdir("debian")
os.mkdir("debian/amd64")
os.mkdir("debian/i386")
cwd_dir=os.getcwd()

#for arch in ("i386","amd64"):
for arch in ("amd64",):
 for i in vec:
  print i.release_name
  dirpath = tempfile.mkdtemp()
  os.chdir(dirpath)
  return_code = call(["cmake","-DBUILD_FOR_DEBIAN_OR_UBUNTU_ARCH=%s" % (arch), "-DBUILD_FOR_DEBIAN_OR_UBUNTU_RELEASE=%s" % (i.release_name),"-DBUILD_FOR_DEBIAN_OR_UBUNTU=%s" % (i.debian_or_ubuntu), "-DCPU_COUNT_FOR_BUILD_IN_VAGRANT=%i" % (number_of_threads), source_dir]) 
  if return_code != 0 :
    print "error: cmake failed"
    sys.exit()
  return_code2 = call(["sh","build_debian_package.sh", "%i" % (number_of_threads)])
  if return_code != 0 :
    print "error: build_debian_package.sh failed"
    sys.exit()
  target_dir="%s/%s/%s/%s" % (cwd_dir, i.debian_or_ubuntu, arch, i.release_version)
  os.mkdir(target_dir)
  for path in glob.glob(dirpath + "/vagrant_debian_build/prime-phylo*.deb"):
    shutil.move(path, target_dir)
