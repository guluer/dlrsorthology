esjolund@bigmem:/tmp/deb2$ debsign *.changes
 signfile prime-phylo_1.0.4-1.dsc Erik Sjolund <erik.sjolund@gmail.com>

You need a passphrase to unlock the secret key for
user: "Erik Sjolund <erik.sjolund@gmail.com>"
4096-bit RSA key, ID 603DA35A, created 2012-08-08

                  
 signfile prime-phylo_1.0.4-1_amd64.changes Erik Sjolund <erik.sjolund@gmail.com>

You need a passphrase to unlock the secret key for
user: "Erik Sjolund <erik.sjolund@gmail.com>"
4096-bit RSA key, ID 603DA35A, created 2012-08-08

                  
Successfully signed dsc and changes files
esjolund@bigmem:/tmp/deb2$



esjolund@bigmem:/tmp/deb2$ emacs ~/.dput.cf
esjolund@bigmem:/tmp/deb2$ cat ~/.dput.cf
[mentors]
fqdn = mentors.debian.net
incoming = /upload
method = http
allow_unsigned_uploads = 0
progress_indicator = 2
# Allow uploads for UNRELEASED packages
allowed_distributions = .*
esjolund@bigmem:/tmp/deb2$ dput mentors prime-phylo_1.0.4-1_amd64.changes 
Checking signature on .changes
gpg: Signature made 2012-10-15T15:44:11 CEST using RSA key ID 603DA35A
gpg: Good signature from "Erik Sjolund <erik.sjolund@gmail.com>"
Good signature on /tmp/deb2/prime-phylo_1.0.4-1_amd64.changes.
Checking signature on .dsc
gpg: Signature made 2012-10-15T15:44:09 CEST using RSA key ID 603DA35A
gpg: Good signature from "Erik Sjolund <erik.sjolund@gmail.com>"
Good signature on /tmp/deb2/prime-phylo_1.0.4-1.dsc.
Uploading to mentors (via http to mentors.debian.net):
  Uploading prime-phylo_1.0.4-1.dsc: done.
  Uploading prime-phylo_1.0.4.orig.tar.gz: done.    
  Uploading prime-phylo_1.0.4-1.debian.tar.gz: done.
  Uploading prime-phylo_1.0.4-1_amd64.deb: done.      
  Uploading prime-phylo_1.0.4-1_amd64.changes: done.
Successfully uploaded packages.
esjolund@bigmem:/tmp/deb2$ 
