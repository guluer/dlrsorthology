#!/bin/sh

set -e

if [ $# -ne 1 ]
then
  echo "error: wrong number of arguments. Usage: `basename $0` nr_of_threads"
  echo "For instance `basename $0` 2"
  exit 1
fi

nr_in_parallell=$1

cd "@CMAKE_BINARY_DIR@" && make -j${nr_in_parallell} package_source

deb_dir="@CMAKE_BINARY_DIR@/deb_build_dir"
mkdir "$deb_dir"

cd  "$deb_dir"

src_tar="@CMAKE_BINARY_DIR@/prime-phylo-@PACKAGE_VERSION@.tar.gz"
cp "$src_tar" "prime-phylo_@PACKAGE_VERSION@.orig.tar.gz"
tar xfz "$src_tar"
cd "prime-phylo-@PACKAGE_VERSION@"

svn export "@CMAKE_SOURCE_DIR@/excluded_from_release/debian" "$deb_dir/prime-phylo-@PACKAGE_VERSION@/debian"

echo $nr_in_parallell > "@vagrant_dir@/num_threads"

cd "@vagrant_dir@"
vagrant up


