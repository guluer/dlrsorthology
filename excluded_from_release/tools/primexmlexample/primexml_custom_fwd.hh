#ifndef PRIMEXML_CUSTOM_FWD_HH
#define PRIMEXML_CUSTOM_FWD_HH

template <typename base>
class NodeType_impl;

template <typename base>
class McmcType_impl;

template <typename base>
class BirthDeathMcmcType_impl;

template <typename base>
class EdgeRateMcmcType_impl;

#endif
