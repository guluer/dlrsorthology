#include "primexml.hh"


template <typename base>
NodeType_impl<base>::
NodeType_impl(const id_type &id)
    : base(id) {
}
   
template <typename base>
NodeType_impl<base>::
NodeType_impl(const xercesc::DOMElement &e,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(e, f, c) {
}

template <typename base>
NodeType_impl<base>::
NodeType_impl(const NodeType_impl &p,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(p, f, c) {
}

template <typename base>
NodeType_impl<base> *NodeType_impl<base>::
_clone(xml_schema::flags f, xml_schema::container *c) const {
  return new NodeType_impl(*this, f, c);
}

template class NodeType_impl<primexmlNs::NodeType_base>;



template <typename base>
McmcType_impl<base>::
McmcType_impl(const probability_type &probability,
              const iteration_type &iteration,
              const  nAcceptedStates_type &n_accepted_states)
    : base(probability, iteration, n_accepted_states) {
}
   
template <typename base>
McmcType_impl<base>::
McmcType_impl(const xercesc::DOMElement &e,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(e, f, c) {
}

template <typename base>
McmcType_impl<base>::
McmcType_impl(const McmcType_impl &p,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(p, f, c) {
}

template <typename base>
McmcType_impl<base> *McmcType_impl<base>::
_clone(xml_schema::flags f, xml_schema::container *c) const {
  return new McmcType_impl(*this, f, c);
}

template class McmcType_impl<primexmlNs::McmcType_base>;




template <typename base>
EdgeRateMcmcType_impl<base>::
EdgeRateMcmcType_impl(const probability_type &probability,
                      const iteration_type &iteration,
                      const nAcceptedStates_type &n_accepted_states,
                      const suggestionVariance_type &suggestionVariance)
    : base(probability, iteration, n_accepted_states, suggestionVariance) {
}
   
template <typename base>
EdgeRateMcmcType_impl<base>::
EdgeRateMcmcType_impl(const xercesc::DOMElement &e,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(e, f, c) {
}

template <typename base>
EdgeRateMcmcType_impl<base>::
EdgeRateMcmcType_impl(const EdgeRateMcmcType_impl &p,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(p, f, c) {
}

template <typename base>
EdgeRateMcmcType_impl<base> *EdgeRateMcmcType_impl<base>::
_clone(xml_schema::flags f, xml_schema::container *c) const {
  return new EdgeRateMcmcType_impl(*this, f, c);
}

template class EdgeRateMcmcType_impl<primexmlNs::EdgeRateMcmcType_base>;




template <typename base>
BirthDeathMcmcType_impl<base>::
BirthDeathMcmcType_impl(const probability_type &probability,
                    const iteration_type &iteration,
                        const nAcceptedStates_type &n_accepted_states,
                    const birthDeathParam_type &birth_death_param)
    : base(probability, iteration, n_accepted_states, birth_death_param) {
}
   
template <typename base>
BirthDeathMcmcType_impl<base>::
BirthDeathMcmcType_impl(const xercesc::DOMElement &e,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(e, f, c) {
}

template <typename base>
BirthDeathMcmcType_impl<base>::
BirthDeathMcmcType_impl(const BirthDeathMcmcType_impl &p,
              xml_schema::flags f,
              xml_schema::container *c)
    : base(p, f, c) {
}

template <typename base>
BirthDeathMcmcType_impl<base> *BirthDeathMcmcType_impl<base>::
_clone(xml_schema::flags f, xml_schema::container *c) const {
  return new BirthDeathMcmcType_impl(*this, f, c);
}

template class BirthDeathMcmcType_impl<primexmlNs::BirthDeathMcmcType_base>;
