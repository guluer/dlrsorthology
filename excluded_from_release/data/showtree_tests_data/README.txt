Some test cases in development

t1	Two leaves without time info
t1b	This version has its own node IDs and they should be honoured!
	
t2[abc]	The same host tree given in three different ways
t2_all	This is "cat t2? > t2_all" and we should be able to see them
	all.


R0* and R1*	Gene trees with reconciliation information.
