 2012-04-07 Erik Sjolund tried to run some of the commands from src/cxx/tools/chainsaw/RunTests.sh


 $ chainsaw r4b sp1		     
 Error:				     
     TreeIOTraits::enforceGuestTree:  
     no branch length info in tree    
 $ chainsaw  ex15.gt ex8.st	     
 Error:				     
     TreeIOTraits::enforceGuestTree:  
     no branch length info in tree    
 $ chainsaw  ex6.gt ex5.st 	     
 Error:				     
     TreeIOTraits::enforceGuestTree:  
     no branch length info in tree    
 $ chainsaw  ex7.gt ex5.st	     
 Error:				     
     TreeIOTraits::enforceGuestTree:  
     no branch length info in tree    


 2012-04-07 Erik Sjolund moved the test data files to example_data/data_not_in_source_package/chainsaw_testdata


