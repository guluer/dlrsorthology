#include <stdio.h>
#include "NHXnode.h"

extern struct NHXnode *root_node;

main () {
  root_node = read_tree(NULL);
  if (root_node) {
    print_tree(root_node, 0);
  } else {
    printf("Failure\n");
  }
}


print_tree(struct NHXnode *v, int indent) {
  if (v) {
    print_tree(v->left, indent + 3);
    print_indent(indent);
    if (v->name) {
      printf("%s\n", v->name);
    } else {
      printf("Node\n");
    }
    print_tree(v->right, indent + 3);
  }
}


print_indent(int indent) {
  int i;
  for (i=0; i<indent; i++) {
    putchar(' ');
  }
}
