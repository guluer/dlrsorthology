#include "../Probability.hh"
#include "../ChronTime.hh"
#include <stdio.h>
#include <iostream>
#include <assert.h>
#include "../AnError.hh"

void
main () 
{
  Probability A;
  Probability p;          //default constructor
  Probability q(10);      //default constructor with argument
  Probability r(-5);
  Probability s(4);
  p=q;                    //implicit copy constructor
  q=-2;                  //implicit assignment operator
  unsigned i=3;
  unsigned j=6;
  double n = 6.0;
  ChronTime c = 5.0;

  //Show parameter values
  //---------------------------------------------------------------------
  printf("New initial parameter values:\n");    
  printf("i = %d\n", i);
  printf("j = %d\n", j);
  printf("n = %f\n", n);
  printf("c = %f\n", c.val());
  printf("p = %f\n", p.val());
  printf("q = %f\n", q.val());
  printf("r = %f\n", r.val());
  printf("s = %f\n", s.val());
  printf("log(p) = %f\n", log(p.val()));
  printf("log(q) = %f\n", log(q.val()));
  printf("\n");

  //Check all operators
  //---------------------------------------------------------------------
  printf("arithmetic operations:\n");
  A = p + p;
  printf("p + p = %f\n", A.val());

  A = q + q;
  printf("q + q = %f\n", A.val());

  A = p + q;
  printf("p + q = %f\n", A.val());

  A = q + p;
  printf("q + p = %f\n", A.val());

  printf("\n");

  A = p - s;
  printf("p - s = %f\n", A.val());
  
  A = r - q;
  printf("r - q = %f\n", A.val());

  A = p - q;
  printf("p - q = %f\n", A.val());

  A = q - p;
  printf("q - p = %f\n", A.val());
  
  printf("\n");

  A = p * i;
  printf("p * i = %f\n", A.val());

  A = i * p;
  printf("i * p = %f\n", A.val());

  A = p * n;
  printf("p * n = %f\n", A.val());

  A = p * (-1 * n);
  printf("p * (-n) = %f\n", A.val());

  A = n * p;
  printf("n * p = %f\n", A.val());

  A = p * c;
  printf("p * c = %f\n", A.val());

  A = c * p;
  printf("c * p = %f\n", A.val());

  printf("\n");

  A = p * q;
  printf("p * q = %f\n", A.val());

  A = p/q;
  printf("p / q = %f\n", A.val());

  A = p.pow(i);
  printf("p ^ i = %e\n", A.val());

  A = p.pow(j);
  printf("p ^ j = %e\n", A.val());

  A = p.pow(-n);
  printf("p ^ - n = %e\n", A.val());

  A = probFact(i);
  printf("factorial(i) = probFact(i) = %e\n", A.val());

  A = probBinom(j,i);
  printf("binomial(^j _i) = probBinom(j, i) = %e\n", A.val());
  printf("\n");


  //Check const attributes
  //---------------------------------------------------------------------
  printf("arithmetic operations can handle constants?:\n");
  A = p * 5u;
  printf("p * 5 = %f\n", A.val());

  A = 5u * p;
  printf("5 * p = %f\n", A.val());

  A = p.pow(4);
  printf("p ^ 4 = %e\n", A.val());

  A = 5.0 - q;
  printf("5.0 - q = %f\n", A.val());

  A = probFact(3);
  printf("factorial(3) = probFact(i) = %e\n", A.val());
  printf("\n");

  //Check all comparisons
  //---------------------------------------------------------------------
  cout << "Comparisons:\n";
  if (q>r) {
    cout << "q > r\n";
  } else {
    cout << "q <= r\n";
  }

  if (s>r) {
    cout << "s > r\n";
  } else {
    cout << "s <= r\n";
  }

  if (q>=q) {
    cout << "q >= q, thankfully\n";
  } else {
    cout << "Oj d�.\n";
  }
  printf("\n");


  //Check chaining
  //---------------------------------------------------------------------
  printf("arithmetic operations handles chaining?:\n");
  A = q * p * i;
  printf("q * p * i = %f\n", A.val());

  A = q * p.pow(i);
  printf("q * p ^ i = %e\n", A.val());
  
  A = p/q - q;
  printf("p / q - q = %f\n", A.val());

  printf("\n");

  A = p - p/10;
  printf("p - p / 10 = %f\n", A.val());

  A = p - (p/10);
  printf("p - (p / 10) = %f\n", A.val());

  A = (p - p) /10;
  printf("(p - p) / 10 = %f\n", A.val());

  printf("\n");

  A = p/q - q/p;
  printf("p / q - q / p = %f\n", A.val());

  A = (p/q) - (q/p);
  printf("(p / q) - (q / p) = %f\n", A.val());

  printf("p / (q - q) / p = ");
  try
    {
      A = p/(q - q)/p;
      printf("%f\n", A.val());
    }
  catch(AnError e)
    {
      e.action();
    }

  printf("\n");

  printf("q - p / q = ");
  try
    {
      A = q - (p/q);
      printf("%f\n", A.val());
    }
  catch(AnError e)
    {
      e.action();
    }
  printf("That's all, folks\n");
}
