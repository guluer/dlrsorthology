
export MODULEPATH=$MODULEPATH:/afs/pdc.kth.se/projects/sbc/modules/system/

module add heimdal 
module add easy
module add afsws 
module add openmpi/1.3-gcc
#module add mpi
module add mkl
module add xsd
module add libxml2
module add cmake/2.8.0
module add boost
module add gengetopt
module add mkl


#module add cmake/2.8.2
#module add boost/1.41.0
#module add xerces-c/3.0.1
#module add openmpi/1.3-gcc
#module add gengetopt/2.22.3
#module add heimdal/standard
#module add mkl


module add mpi


# some problem with libimf.so and the libxml2 module ( that was built with the intel compiler )
# made me add i-compilers and i-fce
module add i-compilers
module add i-fce


# somehow the PDC version cmake/2.6.4 is marked as latest
module add cmake/2.8.0
#export CC=icc ; export CXX=icpc ; export F9X=ifort
export CC=mpicc ; export CXX=mpicxx ; export F9X=mpif90


