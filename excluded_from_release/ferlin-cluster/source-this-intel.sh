# Spent a lot of time trying to build PrIME with the openmpi/1.3-intel or  openmpi/1.4.1 (also intel)
# A lot of confusing errors.....
# 
# Instead of this script use : source-this-gcc.sh

echo "error: this does not work"

export MODULEPATH=$MODULEPATH:/afs/pdc.kth.se/projects/sbc/modules/system/

module add heimdal 
module add easy
module add afsws 
# tried this one too:
# module add openmpi/1.3-intel
module add openmpi/1.4.1
module add mkl
module add xsd
module add libxml2
module add cmake/2.8.0
module add boost
module add xerces-c
module add i-compilers/latest
module add i-fce/11.0.074
module add gengetopt

# somehow the PDC version cmake/2.6.4 is marked as latest
module add cmake/2.8.0
#export CC=icc ; export CXX=icpc ; export F9X=ifort
export CC=mpicc ; export CXX=mpicxx ; export F9X=mpif90


