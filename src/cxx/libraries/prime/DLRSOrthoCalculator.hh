/* 
 * File:   DLRSOrthoCalculator.h
 * Author: ikramu
 *
 * Created on October 18, 2012, 4:23 PM
 */

#ifndef DLRSORTHOCALCULATOR_H
#define	DLRSORTHOCALCULATOR_H

#include <iostream>
#include <cstdlib>
#include <map>
#include <boost/algorithm/string.hpp>
#include <fstream>

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "GammaDensity.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "SetOfNodes.hh"
#include "Probability.hh"
#include "Tokenizer.hh"

/* Namespaces used */
using namespace std;
using namespace beep;
using namespace boost::algorithm;

class DLRSOrthoCalculator {
public:
    DLRSOrthoCalculator();
    DLRSOrthoCalculator(string gnt, string spt, double mean,
        double var, double birth, double death, bool read_tree_from_string=false);
    DLRSOrthoCalculator(const DLRSOrthoCalculator& orig);
    virtual ~DLRSOrthoCalculator();
    
    void computeAndWriteOrthologies(string geneFileName);
    
    map<string, double> getOrthoEstimates();

private:
    /**
     * read_specie_tree
     * Reads species tree.
     *
     * @param spfile The specie tree
     */
    void read_species_tree(string spfile);
    
    vector<string> get_gene_pairs_from_lca(Node* node);
    
    /**
     * read_gene_tree
     * Reads a gene tree.
     *
     * @param gnfile The gene tree file
     */
    void read_gene_tree(string g_tree, bool from_string);

    /**
     * rescale_specie_tree
     * Rescales a species tree to [0,1].
     *
     * @param S The specie tree
     */
    void rescale_specie_tree();

    /**
     * create_disc_tree
     * Creates a discretized species tree from a species tree.
     *
     * @param species_tree The species tree
     * @param DS At return: Pointer to the discretized species tree
     * @param args_info Program arguments
     *
     */
    void create_disc_tree();    

    /**
     * find_lca
     * Finds and returns the lca node of a set of nodes.
     *
     * @param nodes A set of nodes
     * @param T Tree in which the nodes are found.
     *
     */
    Node* find_lca(SetOfNodes &nodes, Tree &T);

    std::vector<Node*> getDescendentNodes(Node* n);

    std::vector<Node*> getDescendentNodeRecursive(Node* n);

    /**
     * create_lookup_tables
     * Create an id for each gene in the range 0,..., gs_map.size()-1.
     * Then created lookup tables for gene name to id mapping and vice versa.
     *
     * @param ID Storage of id to gene map upon return.
     * @param inv_ID Storage of gene to id map upon return.
     * @param gs_map Gene to species map
     */
    void create_lookup_tables();
    
    std::vector<std::string> &split_str(const std::string &s, char delim, std::vector<std::string> &elems);
    
    std::vector<std::string> split_str(const std::string &s, char delim);

    void calc_speciation_single(char *outfile);

    void printVector(vector<Node*> v);

    vector<unsigned> getIdsFromNodes(vector<Node*> lnodes);

    bool isObligateDuplication(Node *lca, LambdaMap sigma);

    /**
     * 
     * @param gtree Gene Tree
     * @param map Map object to be populated
     */
    void populateGsMap(string spfile);

    std::vector<std::string> split(const std::string &s, char delim);

    void read_leaves_from_file(string path, vector<string> &msn);
    
    bool not_same_specie(string left_gene, string right_gene);
    
    string get_specie_from_gene_name(string gene);

    Tree species_tree;
    Tree gene_tree;
    //TreeIO tree_reader;
    StrStrMap *gsMap;
    GammaDensity *gamma;
    EdgeDiscBDProbs *bd_probs;
    EdgeDiscTree *DS;
    TreeIO tio;
    EdgeDiscGSR *gsr;
    map<int, string> ID; //Lookup table for id-gene mapping
    map<string, int> inv_ID; //Lookup table for gene-id mapping
};

#endif	/* DLRSORTHOCALCULATOR_H */

