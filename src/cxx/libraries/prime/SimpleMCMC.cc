#include "SimpleMCMC.hh"

#include "AnError.hh"
#include "Hacks.hh"
#include "MCMCObject.hh"

#include <iostream>
#include <sstream>
#include <unistd.h>
#include <cmath>


// Author: Lars Arvestad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;
  
  //-------------------------------------------------------------
  //
  // Construct / Destruct / Assign
  //
  //-------------------------------------------------------------
  SimpleMCMC::SimpleMCMC(MCMCModel& M, unsigned thin)
    : model(M),
      R(M.getPRNG()),
      iteration(0),
      thinning(thin),
      cout_buf(NULL),
      localOptimumFound(false),
      show_diagnostics(true),
      m_first_iterate(true),
      m_last_iterate(true)
  {
    p = model.initStateProb();    // To set stateProbs
    localOptimum = p;
    bestState = model.strRepresentation();
    model.commitNewState();       // To set old_stateProbs, 
  }

  SimpleMCMC::~SimpleMCMC()
  {
    // If we have redirected cout, then we want to make sure to set it back!
    if (cout_buf != NULL) 
      {
	os.close();		 // Close output file
	cout.rdbuf(cout_buf);	 // Assign the old buffer
	cout_buf = NULL;	 // Reset temp buffer pointer
      }
  }

  void SimpleMCMC::setFirstIterate(bool b)
  {
    m_first_iterate = b;
  }

  void SimpleMCMC::setLastIterate(bool b)
  {
    m_last_iterate = b;
  }

  //---------------------------------------------------------------
  //
  // Standardized interface from MCMCModel
  // You should never really need to change this!
  // 
  //---------------------------------------------------------------
  void
  SimpleMCMC::iterate(unsigned n_iters, unsigned print_factor)
  {
    // These two variables should be deleted:
    //                                       when? /bens
    // When we no longer feel we need the diagnostic output to
    // check whether the chain is stuck or not! /arve
    //
    // I have now removed the code for a diagFile.out where bad
    // proposals were supposed to go. We have not used this in years
    // anyway. I'll leave the warnings however! /arve
    unsigned check_number_nonupdate = 0;

    start_time = time(NULL);


    // First print out the settings
    if(m_first_iterate)
      {
	cout << "#  Starting MCMC with the following settings:\n#  "
	     << n_iters
	     << print()
	     << "#\n";
	
	// and the mcmc-header
	cout << "# L N "
#ifdef DEBUG_PROPOSAL
	     << "Prop(logfloat)  "
#endif
	     << model.strHeader()
	     << endl;
      }
    
    // We want output to behave in two ways depending on whether
    // we are writing to a terminal or not. If it is TTY, then only
    // the state information (on format <likelihood> <iteration> <state>)
    // is written. If cout is actually a file, then the same info is 
    // going there, but we are also outputting the likelihood numbers 
    // to stderr for visual inspection of convergence unless it has been 
    // supressed!
    //
    // I cannot compile this under g++ 3.2.2, and do not find another 
    // way on the Net for now. /arve
    // 
    // OK, trying a solution found on the Net, see Hacks.hh!

    bool error_stream_is_a_terminal = true;//isatty(prime_fileno(cerr));     

    // filebuf *fb = dynamic_cast<filebuf*>(cout.rdbuf());
    // bool error_stream_is_a_terminal = isatty(fb->fd()); 
    // Check if file descriptor is a TTY

    // bool error_stream_is_a_terminal = true;

    // output a header to cerr if not quiet
    if (error_stream_is_a_terminal // Check if file descriptor is a TTY
	&& show_diagnostics)
      {
	cerr.width(15);
	cerr << "L";
	cerr.width(15);
	cerr << "N";
	cerr.width(15);
	cerr << "alpha";
	cerr.width(15);
	cerr << "time"
	     << endl;
      }
    
    // This variable controls output to cerr
    unsigned printing = thinning * print_factor;

    string output = model.strRepresentation();
    for (unsigned i = 0; i < n_iters; i++) 
      {
	try 
	  {
	    MCMCObject proposal = model.suggestNewState();
	    Probability alpha = 1.0;
	    if(p > 0)
	      {
		alpha = proposal.stateProb * proposal.propRatio / p;
	      }
	    Probability test = Probability(R.genrand_real1());
// 	    cerr << "\nPold = " << p
// 		 << "\nPnew = " << proposal.stateProb
// 		 << "\nPnew/Pold = " << (proposal.stateProb/p).val()
// 		 << "\npropratio = " << proposal.propRatio.val()
// 		 << "\nalpha = " << alpha
// 		 << "\ntest = " << test.val()
// 		 << endl << endl;
	    
	    if(proposal.stateProb > localOptimum)
	      {
		localOptimumFound = true;
		localOptimum = proposal.stateProb;
		bestState = model.strRepresentation();	
	      }
// 	    if(alpha >= 1.0 || Probability(R.genrand_real1()) <= alpha)
	    if(alpha >= 1.0 || test <= alpha)
	      {
		model.commitNewState();
		// The following line should be deleted:
		check_number_nonupdate = 0;
		p = proposal.stateProb;
		// We're committing, so update output parameter
		output = model.strRepresentation();
	      }
	    else
	      {
		model.discardNewState();
		// Note that since we are discarding we do NOT update output
		// but leave it as in last iteration
	      }

	    // 	  }
	  }
	catch (AnError& e)
	  {

	    cerr << "SimpleMCMC::iterate\nAt iteration " 
		 << i 
		 << ".\nState is "
		 << model.strRepresentation()
		 << endl;
	    e.action();
	  }
	
	//if ((iteration % thinning == 0) || (localOptimumFound == true))
	if (iteration % thinning == 0)
	  {
	    localOptimumFound = false;
	    if (error_stream_is_a_terminal // Check if file descriptor is a TTY
		&& show_diagnostics 
		&& iteration % printing == 0) 
	      {
		cerr.width(15);
		cerr << p;
		cerr.width(15);
		cerr << iteration;
		cerr.width(15);
		cerr << model.getAcceptanceRatio();
		cerr.width(15);
		cerr << estimateTimeLeft(i, n_iters);
		cerr << endl;
	      }
	    
	    cout << p
		 << "\t"
		 << iteration
		 << "\t"
  		 << output
		 << "\n";  // This enables buffering and should speed up the program. However, if the program exits prematurely, some iterations may be lost!
	    // 		 << endl;
	  }
	iteration = iteration + 1;
      }
    if(m_last_iterate)
      {
        cout << "# acceptance ratio = " << model.getAcceptanceRatio() << "\n";
        cout << "# local optimum = " << localOptimum << "\n";
        cout << "# best state " << bestState << "\n";
      }
  }



  //---------------------------------------------------------------
  //
  // Standardized interface from MCMCModel
  // You should never really need to change this!
  //
  //---------------------------------------------------------------
  void
  SimpleMCMC::advance(unsigned n_iters)
  {
    /*Perform iterations*/
    for (unsigned i = 0; i < n_iters; i++)
      {
	try
	  {
            //Suggest new state
	    MCMCObject proposal = model.suggestNewState();

            //Calculate acceptance ratio
            Probability alpha = 1.0;
	    if(p > 0)
	      {
                alpha = proposal.stateProb * proposal.propRatio / p;
	      }

            if(proposal.stateProb > localOptimum)
	      {
                localOptimumFound = true;
                localOptimum = proposal.stateProb;
                bestState = model.strRepresentation();
	      }

            //Accept new state with probability alpha
	    if(Probability(R.genrand_real1()) <= alpha)
	      {
		model.commitNewState();
		p = proposal.stateProb;
	      }
	    else
	      {
		model.discardNewState();
	      }
	  }
	catch (AnError& e)
	  {

	    cerr << "SimpleMCMC::advance\nAt iteration "
		 << i
		 << ".\nState is "
		 << model.strRepresentation()
		 << endl;
	    e.action();
	  }
	iteration = iteration + 1;
      }
  }


  void
  SimpleMCMC::setOutputFile(const char *filename)//, char *header)
  { 
    if (cout_buf)  // Already have a file open. Close and reassign.
      {
	os.close();
	cout.rdbuf(cout_buf);
      }

    os.open(filename);
    cout_buf = cout.rdbuf();
    cout.rdbuf(os.rdbuf());

  }

  void
  SimpleMCMC::setThinning(unsigned i)
  {
    thinning = i;
  }

  bool
  SimpleMCMC::setShowDiagnostics(bool yes_no)
  {
    bool old_val = show_diagnostics;
    show_diagnostics = yes_no;
    return old_val;
  }


  string
  SimpleMCMC::estimateTimeLeft(unsigned iteration, unsigned when_done)
  {
    if (iteration < 10) {
      return "";
    }

    unsigned cur_time = time(NULL);
    float d = cur_time - start_time; 
    float efficiency = d / iteration;
    unsigned to_go = lrint(efficiency * (when_done - iteration));

    return readableTime(to_go); 
  }

  Probability
  SimpleMCMC::getLocalOptimum()
  {
    return localOptimum;
  }

  string 
  SimpleMCMC::getBestState()
  {
    return bestState;
  }

  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const SimpleMCMC& A)
  {
    return o << A.print();
  }
  string 
  SimpleMCMC::print() const
  {
    ostringstream oss;
    oss << " MCMC iterations, saving every "
	<< thinning
	<< " iteration.\n"
	<< indentString(model.print(), "#  ")
      ;
    return oss.str();
  }


}//end namespace beep
