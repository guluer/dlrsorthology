/* 
 * File: MultiGSR.hh Author: bens
 *
 * Created on den 4 mars 2010, 18:56
 */

#ifndef _MULTIGSR_HH
#define	_MULTIGSR_HH

#include "DiscTree.hh"
#include "EdgeDiscBDMCMC.hh"
#include "Density2PMCMC.hh"
#include "SubstitutionMCMC.hh"
#include "TreeMCMC.hh"

#include <sstream>
#include <ostream>
namespace beep
{  
  class MultiGSR
    : public StdMCMCModel
  {
  public:
     // Construct/destruct/assign
    //---------------------------
    MultiGSR(MCMCModel& prior, EdgeDiscTree& DS,
	     const Real& suggestRatio = 1.0);
    virtual ~MultiGSR();

    // Interface
    //--------------------------
    void addGeneFamily(SubstitutionMCMC& like, TreeMCMC& genetree,
		       EdgeDiscBDMCMC& bdm, Density2PMCMC& dens);
    virtual MCMCObject suggestOwnState();
    virtual Probability updateDataProbability();
    virtual void commitOwnState ();
    virtual void discardOwnState ();
    virtual std::string ownStrRep() const;
    virtual std::string ownHeader () const;
    virtual Probability calcDataProbability(unsigned excludeGF);
    void update();
    std::string print() const;

    // Attributes
    //-----------------------------
//     Tree* S;
    EdgeDiscTree* DS;
    std::vector<SubstitutionMCMC*> geneFams;
    std::vector<TreeMCMC*> trees;
    std::vector<EdgeDiscBDMCMC*> bds;
    std::vector<Density2PMCMC*> rateDensities;
    
    unsigned subIdx;
  };
}// end name space beep

#endif	/* ddG_MPIINSIDEMCMC_HH */

