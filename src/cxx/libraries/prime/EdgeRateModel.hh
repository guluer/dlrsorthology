#ifndef EDGERATEMODEL_HH
#define EDGERATEMODEL_HH

#include "EdgeWeightModel.hh"

#include <iostream>
#include <sstream>

#include "Beep.hh"

namespace beep
{
  // Forward declarations
  class Density2P;
  class Node;
  class Probability;

//----------------------------------------------------------------------
//
// class EdgeRateModel
//! Base class defining a common interface and defaults for EdgeRateModels.
//
//! These classes model rate (average over positions) of each node in the
//! Tree T, with the underlying density function rateProbs. 
//!
//! Subclasses of type EdgeRateModel provides models for rates that are 
//! constant over edges, ConstRateModel, or that vary between edges, 
//! VarRateModel.
//!
//!  Authors Bengt Sennblad, 
//!  copyright the MCMC club, SBC
//
//----------------------------------------------------------------------

  class EdgeRateModel : public EdgeWeightModel //ProbabilityModel
  {
  public:
    //----------------------------------------------------------------------
    // Constructor/Destructor
    //----------------------------------------------------------------------
    virtual ~EdgeRateModel() 
    {};

    //----------------------------------------------------------------------
    //
    // Interface
    // Provides default return values for subclasses
    //
    //----------------------------------------------------------------------

    // Access to shared members 
    // These return const objects, as a reasonable encapsulation compromise
    //----------------------------------------------------------------------
    virtual const Density2P& getDensity() const = 0;
    virtual const Tree& getTree() const = 0;
    virtual RealVector& getRateVector() const = 0;

    // Access parameters
    //----------------------------------------------------------------------
    virtual unsigned nRates() const = 0;

    //! Returns mean of underlying density.
    virtual Real getMean() const = 0;

    //! Returns variance of underlying density. 
    virtual Real getVariance() const = 0;

    //! Returns rate for (incoming edge to) node. Pointer version
    //! precondition: node!=NULL
    virtual Real getRate(const Node* node) const = 0;

    //! Returns rate for (incoming edge to) node. Reference version
    virtual Real getRate(const Node& node) const = 0;

    //! Returns mean of underlying density. 
    // TODO: The operator versions should perhaps be deprecated /bens
    //! Returns rate for (incoming edge to) node. Reference version
    virtual Real operator[](const Node& node) const = 0;

    //! Returns rate for (incoming edge to) node. Pointer version
    virtual Real operator[](const Node* node) const = 0;

    // set parameters
    //----------------------------------------------------------------------

    //! Returns variance of underlying density.
    virtual void setMean(const Real& newValue) = 0;

    //! Returns variance of underlying density.
    virtual void setVariance(const Real& newValue) = 0;

    //! sets rate for (incoming edge to) node. Pointer version
    virtual void setRate(const Real& newRate, const Node* node) = 0;

    //! sets rate for (incoming edge to) node. Reference version
    virtual void setRate(const Real& newRate, const Node& node) = 0;

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    virtual std::string print() const = 0;    

    // Brief model description
    //----------------------------------------------------------------------
    virtual std::string type() const = 0;

  };

}//end namespace beep

#endif
