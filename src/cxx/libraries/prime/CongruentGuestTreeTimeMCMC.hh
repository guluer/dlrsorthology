#ifndef CONGRUENTGUESTTREETIMEMCMC_HH
#define CONGRUENTGUESTTREETIMEMCMC_HH

#include "StdMCMCModel.hh"
#include "LambdaMap.hh"
#include <string>

namespace beep
{
  // Forward declarations
  class LambdaMap;
  class MCMCObject;
  class Node;
  class Probability;
  class StrStrMap;
  class Tree;
  
  //! This class is a first attempt at a class that models the case
  //! where a guest tree exactly follows a host tree (or a subtree
  //! of a host tree), e.g., a plastid gene or a gene with cencerted 
  //! evolution. The class Currently DO NOT HANDLE CASES WITH CHANGING
  //! HOST TREE, simply because I haven't bothered to figure out how 
  //! to do that yet 
  class CongruentGuestTreeTimeMCMC : public StdMCMCModel
  {
  public:
    //-----------------------------------------------------------------
    // Construct/destruct/assign
    //-----------------------------------------------------------------
    CongruentGuestTreeTimeMCMC(MCMCModel& prior, Tree& S_in,
			       Tree& G_in, StrStrMap& gs);
    ~CongruentGuestTreeTimeMCMC();
    CongruentGuestTreeTimeMCMC(const CongruentGuestTreeTimeMCMC& m);
    CongruentGuestTreeTimeMCMC& operator=(const CongruentGuestTreeTimeMCMC& m);
    void update();

  protected:
    //-----------------------------------------------------------------
    // Interface
    //-----------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();

    std::string ownStrRep() const;
    std::string ownHeader() const;

    Probability updateDataProbability();

    std::string print() const;
  private:
    //-----------------------------------------------------------------
    // Implementation
    //-----------------------------------------------------------------
    void initG(Node& u, LambdaMap& sigma);

    //-----------------------------------------------------------------
    // Attributes
    //-----------------------------------------------------------------
    Tree* S;
    Tree* G;
    LambdaMap sigma;
  };

}//end namespace beep
#endif
