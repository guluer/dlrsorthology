#ifndef BIRTHDEATHINHYBRIDMCMC_HH
#define BIRTHDEATHINHYBRIDMCMC_HH

#include <string>
#include <iostream>

#include "BirthDeathInHybridProbs.hh"
#include "StdMCMCModel.hh"

namespace beep
{
  // Forward declarations
  class PRNG;
  class Tree;

  //-------------------------------------------------------------
  //
  //! Provides MCMC interface to BirthDeathProbs and perturbation
  //! of its parameters 
  //
  //-------------------------------------------------------------
  class BirthDeathInHybridMCMC : public StdMCMCModel,
			 public BirthDeathInHybridProbs
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    BirthDeathInHybridMCMC(MCMCModel& prior,HybridTree &S, Real birthRate, 
		   Real deathRate, Real* topTime = 0);
    ~BirthDeathInHybridMCMC();


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    void fixRates();
    void multiplySuggestionVariance(Real multiplier)
    {
      suggestion_variance = multiplier * (birth_rate+death_rate) / 2.0;
    }

    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const BirthDeathInHybridMCMC& A);
    std::string print() const;

  private:
//     //-------------------------------------------------------------
//     //
//     // Implementation
//     //
//     //-------------------------------------------------------------

//     // Utility function for changing birth/death rates.
//     // Returns a number in the interval [0.8, 1.25].
//     //-------------------------------------------------------------
//     Real rateChange();

    //-------------------------------------------------------------
    //
    // Attributes - none
    //
    //-------------------------------------------------------------
    Real old_birth_rate;
    Real old_death_rate;
    bool estimateRates;

    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;
  };
}//end namespace beep


#endif
