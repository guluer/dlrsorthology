#ifndef HYBRIDTREEIO_HH
#define HYBRIDTREEIO_HH

#include "TreeIO.hh"

namespace beep
{
  // Forward declarations
  class GammaMap;
  class HybridTree;
  class Node;
  class SetOfNodes;
  class StrStrMap;
  class Tree;
  
  //--------------------------------------------------------------------
  //
  // HybridTreeIO
  //
  //! extends TreeIO to HybridTrees
  //
  //--------------------------------------------------------------------
  class HybridTreeIO : public TreeIO
  {
  public:
    //--------------------------------------------------------------------
    //
    // Constructors
    // 
    //--------------------------------------------------------------------

  protected:
    HybridTreeIO(enum TreeSource src, const std::string s);

  public:
    HybridTreeIO();	  //! "Empty" constructor, allows reading from STDIN.
    HybridTreeIO(const TreeIO& io); //! Convert from a TreeIO
    HybridTreeIO(const HybridTreeIO& io);
    HybridTreeIO& operator=(const HybridTreeIO& io);
    virtual ~HybridTreeIO();


    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
  public:

    /// \name Named constructors! 
    //! Usage: 
    //!   HybridTreeIO io = fromFile("Nisse"); /* read from file Nisse */
    //!   HybridTreeIO io = fromString("(a,(b,c));"); /* read from this string */
    //!   HybridTreeIO io = fromFile("");      /* read from STDIN  */
    //!   HybrdiTreeIO io;                     /* read from STDIN  */
    //--------------------------------------------------------------------
    //@{
//     static HybridTreeIO fromFile(const std::string &filename);
//     static HybridTreeIO fromString(const std::string &treeString);
    //@}

    //----------------------------------------------------------------------
    //! \name Reading trees
    //----------------------------------------------------------------------
    //@{
    HybridTree readHybridTree(); 
    
    HybridTree readHybridTree(TreeIOTraits traits,
			      std::vector<SetOfNodes> *AC, StrStrMap *gs);

    // Convenience front to readAllBeepTrees(...)
    // Reads 'NW tags' as edge times and nothing more
    //----------------------------------------------------------------------
    std::vector<HybridTree> readAllHybridTrees(std::vector<StrStrMap>* gs = 0);

    //! Basic function for reading multiple trees in PRIME format
    //! ID and name of nodes are always read, optionally edge times
    //! (useET=true), node times (useNT=true), edge lengths (useBL=true)
    //! antichains (AC!=NULL) and gene species maps (gs!=NULL) may be
    //! read. NWIsET detemines whether the 'Newick Weight' tag, i.e., what
    //! is given after ':' in the newick tree, should be interpreted as edge
    //! times or edge lengths.
    //! precondition: (useET && useNT) != true
    //----------------------------------------------------------------------
    std::vector<HybridTree> readAllHybridTrees( TreeIOTraits traits,
					       std::vector<SetOfNodes> *AC,
					       std::vector<StrStrMap>* gs);
   
    //@}

    //----------------------------------------------------------------------
    //! \name Writing trees
    //----------------------------------------------------------------------
    //@{
    static std::string writeHybridTree(const HybridTree& T, 
				       TreeIOTraits traits,
				       const GammaMap* gamma);

    //! convenience front function for writeHybridTree(...) 
    //! writes tree S with edge times
    //----------------------------------------------------------------------
    static std::string writeHybridTree(const HybridTree& S);

  };
}//end namespace beep
#endif
