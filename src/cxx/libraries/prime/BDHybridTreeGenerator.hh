#ifndef BDHYBRIDTREEGENERATOR_HH
#define BDHYBRIDTREEGENERATOR_HH

#include "PRNG.hh"
#include "TreeGenerator.hh"

#include <map>
#include <vector>
#include <sstream>
#include <cstdlib>
#include <string>

namespace beep
{
  //! Forward declarations
  class GammaMap;
  class HybridTree;
  class StrStrMap;

  //-------------------------------------------------------------------
  //
  //! Generation of Hybrid networks under an extended linear Birth-death 
  //! model with parameters:
  //! birth_rate = lambda + rho and  death rate = mu.
  //! When a birth occurs at a lineage p it will be a hybridization with 
  //! probability rho/(lambda+rho). The lineage p will be one of the 
  //! parents of the hybrid child. The other parent op is picked uniformly 
  //! from the concurrent lineages (including p). The hybrid child will
  //! thus have two parents.
  //! If a parent lineage of a hybrid child goes extinct, this is not 
  //! pruned, but represented by an extinction node. This is to retain
  //! information about the hybrid event.
  //
  //-------------------------------------------------------------------
  class BDHybridTreeGenerator : public TreeGenerator
  {
  public:
    //-------------------------------------------------------------------
    //
    // Construct/destruct
    //
    //-------------------------------------------------------------------
    BDHybridTreeGenerator(Real birthRate, Real deathRate, Real hybridRate);
    ~BDHybridTreeGenerator();

    //-------------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------------
    void allowAutoPloidy();
    void setTopTime(Real time);
    Real getTopTime();
    bool generateTree(Tree& G_in, bool noTopTime);
    bool generateHybridTree(HybridTree& G_in);
    StrStrMap exportGS();   
    GammaMap exportGamma();

    
    //-------------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const BDHybridTreeGenerator& BDG);

    //-------------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------------
    void generateV(unsigned k);
    void generateX(unsigned k, Real maxT);
    //! createTrueGamma
    //! reads gamma and converts to a GammaMap stored in tmpGamma
    //---------------------------------------------------------------------
    void createTrueGamma(GammaMap& tmpGamma) const;

    //-------------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------------
    Real lambda;
    Real mu;
    Real rho;
    Real toptime;
    HybridTree* G; 
    std::vector<Node*> leaves;
    std::map<Node*, Real> times;
    PRNG rand;
    bool allowAuto;
  };
}//end namespace beep

#endif
