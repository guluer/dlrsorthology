#include "MCMCSampler.hh"

using namespace beep;

template <class ProbabilityModel_T>
void MCMCSampler<ProbabilityModel_T>::setThinning(int thinning)
{
    m_thinning = thinning;
}

template <class ProbabilityModel_T>
void MCMCSampler<ProbabilityModel_T>::setPrintInterval(int printInterval)
{
    m_printInterval = printInterval;
}
