//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   PosteriorSampler.cc
// * Author: fmattias
// *
// * Created on January 25, 2010, 7:27 PM
// */
//
//#include "PosteriorSampler.hh"
//
//using namespace beep;
//using namespace std;
//
//PosteriorSampler::PosteriorSampler(SequenceData &sequenceData,
//                                   Tree &speciesTree,
//                                   StrStrMap &geneSpeciesMap,
//                                   MatrixTransitionHandler &Q,
//                                   Density2P &edgeRateDensity,
//                                   int minIntervals,
//                                   float timeStep
//                                  ) :
//                                  m_sequenceData(sequenceData),
//                                  m_Q(Q),
//                                  m_gsrSampler(speciesTree,
//                                               geneSpeciesMap,
//                                               edgeRateDensity,
//                                               minIntervals,
//                                               timeStep)
//
//{
//    setup();
//}
//
//PosteriorSampler::PosteriorSampler(SequenceData &sequenceData,
//                                   Tree &speciesTree,
//                                   StrStrMap &geneSpeciesMap,
//                                   MatrixTransitionHandler &Q,
//                                   Density2P &edgeRateDensity,
//                                   EdgeDiscTree &discretizedTree,
//                                   LSDProbs lsd
//                                  ) :
//                                  m_sequenceData(sequenceData),
//                                  m_Q(Q),
//                                  m_gsrSampler(speciesTree,
//                                               geneSpeciesMap,
//                                               edgeRateDensity,
//                                               discretizedTree,
//                                               lsd)
//{
//    setup();
//}
//
//PosteriorSampler::~PosteriorSampler()
//{
//    delete m_edgeWeightHandler;
//    delete m_siteRateDensity;
//    delete m_mcmcSiteRates;
//    delete m_siteRateHandler;
//    delete m_partList;
//    delete m_substitutionMCMC;
//    delete m_posteriorMCMC;
//}
//
//void PosteriorSampler::setup()
//{
//    // Set up edge weights, equating weights with lengths.
//    m_edgeWeightHandler = new EdgeWeightHandler(*m_gsrSampler.currentState());
//
//    // MCMC: Disc. gamma substitution rate variation among sites.
//    m_siteRateDensity = new UniformDensity(UNIFORM_SITE_RATE_START, UNIFORM_SITE_RATE_END, true);
//    m_mcmcSiteRates = new ConstRateMCMC(*m_gsrSampler.getWeightMCMC(),
//                                        *m_siteRateDensity,
//                                        *m_gsrSampler.getGeneTree(),
//                                        "Alpha");
//
//    // Set up site rate handler.
//    // TODO: Should rates vary between sites?
//    m_siteRateHandler = new SiteRateHandler(1, *m_mcmcSiteRates);
//    m_mcmcSiteRates->fixRates();
//
//    // MCMC: Substitution.
//    // TODO: Add partList to class members
//    m_partList = new vector<string>();
//    m_partList->push_back(string("all"));
//    m_substitutionMCMC = new SubstitutionMCMC(*m_mcmcSiteRates,
//                                               m_sequenceData,
//                                              *m_gsrSampler.getGeneTree(),
//                                              *m_siteRateHandler,
//                                               m_Q,
//                                              *m_edgeWeightHandler,
//                                              *m_partList);
//
//    // Create mcmc handler that respects sequences or not
//    m_posteriorMCMC = new SimpleMCMC(*m_substitutionMCMC, 1);
//    m_gsrSampler.setMCMC(m_posteriorMCMC);
//
//    MCMCSampler<SubstitutionMCMC>::initialize(m_substitutionMCMC, m_posteriorMCMC);
//}
//
//GSRSampler *PosteriorSampler::getGSRSampler()
//{
//    return &m_gsrSampler;
//}

