#ifndef SUBSTITUTIONMCMC_HH
#define SUBSTITUTIONMCMC_HH

#ifdef PERTURBED_TREE
#include "FastCacheSubstitutionModel.hh"
#else
#include "CacheSubstitutionModel.hh"
#endif
#include "StdMCMCModel.hh"

#include <iostream>


namespace beep
{

  //---------------------------------------------------------------------
  //
  // Class SubstitutionMCMC MCMCinterface to SubstitutionModel
  // handels the perturbation of parameters. 
  //
  // This should actually contain a SequenceModel which in turn should 
  // provide some info on what parameters to change.
  //
  //---------------------------------------------------------------------
#ifdef PERTURBED_NODE
#ifdef PERTURBED_TREE
  class SubstitutionMCMC : public FastCacheSubstitutionModel, public StdMCMCModel
#else
  class SubstitutionMCMC : public CacheSubstitutionModel, public StdMCMCModel
#endif
#else
  class SubstitutionMCMC : public SubstitutionModel, public StdMCMCModel
#endif
  {
    //---------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //---------------------------------------------------------------------
  public:
    SubstitutionMCMC(MCMCModel& prior,
		     const SequenceData& Data, 
		     const Tree& T,
		     SiteRateHandler& siteRates,
		     const TransitionHandler& Q,
		     EdgeWeightHandler& edgeWeights,
		     const std::vector<std::string>& partitionsList);
    SubstitutionMCMC(SubstitutionMCMC& SM);
    ~SubstitutionMCMC();
  
    SubstitutionMCMC& operator=(const SubstitutionMCMC& SM);

    //---------------------------------------------------------------------
    //
    // Interface from StdMCMCModel
    //
    //---------------------------------------------------------------------
    // We do not have any parameters to perturb
    //---------------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    std::string getAcceptanceInfo() const;
    Probability updateDataProbability();

    //----------------------------------------------------------------------
    //
    // I/O
    // I have not converged on a consensus for this yet, but the 
    // following functions should suffice for the final product
    //
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const SubstitutionMCMC& A);
    std::string print() const;

  protected:
    std::pair<unsigned, unsigned> accPropCnt; //!< Acc. and total prop. count.
  };  
}//end namespace beep
#endif
