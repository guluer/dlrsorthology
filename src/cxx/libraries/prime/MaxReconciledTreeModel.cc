#include "MaxReconciledTreeModel.hh"

#include <sstream>

#include "AnError.hh"
#include "BirthDeathProbs.hh"

namespace beep
{
  using namespace std;
  //------------------------------------------------------------
  //
  // Construct / destruct / Assign
  //
  //------------------------------------------------------------
  MaxReconciledTreeModel::MaxReconciledTreeModel(Tree& G, StrStrMap& gs, 
						 BirthDeathProbs& bdp)
    :ReconciledTreeModel(G,gs,bdp),
     MA(*S, G),
     MX(*S, G)
  {}

  MaxReconciledTreeModel::MaxReconciledTreeModel(const ReconciliationModel& m)
    :ReconciledTreeModel(m),
     MA(*S, *G),
     MX(*S, *G)
  {}
    
  MaxReconciledTreeModel::~MaxReconciledTreeModel()
  {}
  

  //------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------
  Probability 
  MaxReconciledTreeModel::getMLReconciliation()
  {
    // Clear old values
    MA = NodeNodeMap<ProbRanksMap>(*S, *G); 
    MX = NodeNodeMap<std::map<unsigned, ProbRanksMap> >(*S,*G);

    Node& rG = *G->getRootNode();
    Node& rS = *S->getRootNode();
    computeMA(rS,rG,1);
    Probability ml = MA(rS,rG).begin()->first;
    gA(rS, rG, 1);
    inits();
    return ml;
  }
      
  //------------------------------------------------------------
  //
  // Implementation
  //
  //------------------------------------------------------------
  void
  MaxReconciledTreeModel::gA(Node& x, Node& u, unsigned i)
  {
    // Find the right parameters
    ProbRanksMap::const_iterator mait = MA(x,u).begin();
    advance(mait, i-1);
    const Utriple& mai = mait->second; 
    // Pass recursion on     
    gX(x, u, mai.first, mai.second.first);
    return;
  }
  
  void
  MaxReconciledTreeModel::gX(Node& x, Node& u, unsigned k, unsigned i)
  {
    assert(x.dominates(*sigma[u]));

    // get the right parameters
    ProbRanksMap::const_iterator mxit = MX(x,u)[k].begin();
    advance(mxit, i-1);
    const Utriple& mxi = mxit->second;
    
    if(k == 1)
      {
	if(u.isLeaf())
	  {
	    if(x.isLeaf())
	      {
		assert(sigma[u] == &x);
		gamma.addToSet(&x,&u); // add u to gamma(x)
		return;
	      }
	    else
	      {
		// First pass on recursion
		Node& y = *x.getDominatingChild(sigma[u]);
		gA(y,u,1); // One reconc has all Probability
		gamma.addToSet(&x,&u); // add u to gamma(x)
		return;
	      }
	  }
	else
	  {
	    assert(x.isLeaf() == false);

	    if(sigma[u] == &x)
	      {
		assert(gamma_star.isInGamma(&u, &x));
		Node& v = *u.getLeftChild();
		Node& w = *u.getRightChild();
		Node& y = *x.getDominatingChild(sigma[v]);
		Node& z = *x.getDominatingChild(sigma[w]);
		
		assert(y.getSibling() == &z);
		// pass recursion on
		gA(y, v, mxi.second.first);
		gA(z, w, mxi.second.second);
		
		gamma.addToSet(&x,&u); // add u to gamma(x)
		return;
	      }
	    else
	      {
		Node& y = *x.getDominatingChild(sigma[u]);
		gA(y,u,mxi.second.first);
		gamma.addToSet(&x,&u); // add u to gamma(x)
		return;
	      }
	  }
      }
    else
      {
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();
	  
	gX(x,v,mxi.first, mxi.second.first);
	gX(x,w,k-mxi.first, mxi.second.second);
	return;
      }
  };
	  



  //! This function computes \f$ m_V^{(i)}(x,u) \f$ and saves it in MA(x,u), 
  //! together with the ranks of the \f $m_A \f$ that xielded it.
  //! Precondition: x.dominates(*sigma[u])
  //!               x.isLeaf() == u.isLeaf && sigma[u] ==x 
  //------------------------------------------------------------
  void 
  MaxReconciledTreeModel::computeMV(Node& x, Node& u, unsigned i)
  {
    if(x.isLeaf())
      {
	// Check preconditions
	assert(u.isLeaf());
	assert(sigma[u] == &x);

	// Figure out where to start. By looking at the size of MX(x,u)[k], we
	// know how many we have made before; add 1 and we know where to start
	unsigned start = MX(x,u)[1].size() + 1;

	// Currently this is a stupid literal-minded implementation, so...
	if(start == 1)
	  {
	    MX(x,u)[1].insert
	      (std::make_pair(1.0, std::make_pair(1, std::make_pair(1,0))));
	    start++;
	  }
	for(unsigned j = start; j <= i;j++)
	  {
	    MX(x,u)[1].insert
	      (std::make_pair(0, std::make_pair(1,std::make_pair(0,0))));
	  }
      }
    else if(x.strictlyDominates(*sigma[u]))  // This also checks precondition
      {
	// First pass recursion on
	Node& y = gamma.getDominatingChild(x,u);
	Node& z = *y.getSibling();
	if(MA(y,u).size() < i)
	  {
	    computeMA(y,u,i);
	  }

	// then set MX[1], i.e.,  m_V(x,u) = m_A(y,u) * X_A(z) is used as 
	// index and the ranks are i for m_A(y,u), and a dummy 0 for m_A(z,u)
	Probability X_A = bdp->partialProbOfCopies(z,0); 
	ProbRanksMap::const_iterator maj = MA(y,u).begin();
	// Figure out where to start. By looking at the size of MX(x,u)[k], we
	// know how many we have made before; add 1 and we know where to start
	unsigned start = MX(x,u)[1].size() + 1;
	// forward the iterators
	advance(maj, start-1);
	for(unsigned j = start; j <= i; j++, maj++)
	  {
	    MX(x,u)[1].insert
	      (std::make_pair(maj->first * X_A,
			      std::make_pair(1, std::make_pair(j,0))));
	  }
      }
    else
      {
	assert(sigma[u] == &x);      //Check precondition

	// First pass recursion on
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();
	Node& y = gamma.getDominatingChild(x,v);
	Node& z = gamma.getDominatingChild(x,w);
	
	assert(y.getSibling() == &z); // double-check sanity

	if(MA(y,v).size() < i)
	  {
	    computeMA(y,v,i);
	  }
	if(MA(z,w).size() < i)
	  {
	    computeMA(z,w,i);
	  }

	// then set MX[1], i.e.,  m_V(x,u) = m_A(y,v)*m_A(z,w) is used as 
	// index and the ranks are j for m_A(y,u), and h for m_A(z,u)
	ProbRanksMap::const_iterator maj = MA(y,v).begin();
	ProbRanksMap::const_iterator mah = MA(z,w).begin();

	// Figure out where to start. By looking at the size of MX(x,u)[k], we
	// know how many we have made before; add 1 and we know where to start
	unsigned start = MX(x,u)[1].size() + 1;
	// forward the iterators
	advance(maj, start-1);
	advance(mah, start-1);

	for(unsigned j = start; j <= i; j++, maj++)
	  {
	    for(unsigned h = start; h <= i; h++, mah++)
	      {
		MX(x,u)[1].insert
		  (std::make_pair(maj->first * mah->first,
				  std::make_pair(1,std::make_pair(j,h))));
	      }
	  }
      }
    
#ifdef DEBUG_DP
    if(x.isLeaf()) i = 1;
    ProbRanksMap::const_iterator it = MX(x,u)[1].begin();
    for(unsigned j = 1;j <= i; j++, it++)
	cerr << "computeMV:\tMX(" << x.getNumber() << ", " 
	     << u.getNumber() <<  ")[" << 1 << "][" << j << "] = " 
	     << it->first.val() << endl;
#endif
    return;
  }

  //! Computes \f$ m_A^{(i)}(x,u) \f$ and saves it in MA(x,u), 
  //! together with the rank of the \f $m_x \f$ that yielded it.
  //! Precondition: x.domiantes(*sigma[u])
  //!               x.isLeaf() == u.isLeaf && sigma[u] ==x 
  //------------------------------------------------------------
  void 
  MaxReconciledTreeModel::computeMA(Node& x, Node& u, unsigned i)
  {
    assert(x.dominates(*sigma[u])); // Check precondition
      
    // iterate over possible number of leaves
    for(unsigned k = slice_L(x,u); k <= slice_U[u]; k++)
      {
	// Pass recursion on
	if(MX(x,u)[k].size() < i)
	  {
	    computeMX(x,u,k,i);
	  }
	// then set MA, i.e.,  m_A(x,u) = Q_x(k) m_X(x,u, k) is used as index
	// and the ranks are j for m_X(x,u), and a dummy 0.
	Probability Q_X = bdp->partialProbOfCopies(x, k); 
	ProbRanksMap::const_iterator mxj = MX(x,u)[k].begin();
	// Figure out where to start. By looking at the size of MA(x,u), we
	// know how many we have made before; add 1 and we know where to start
	unsigned start = MA(x,u).size() + 1;
	// forward the iterators
	advance(mxj, start-1);
	for (unsigned j = start; j <= i; j++, mxj++)
	  {
	    MA(x,u).insert
	      (std::make_pair(Q_X * mxj->first,
			      std::make_pair(k,std::make_pair(j,0))));

#ifdef DEBUG_DP
	    ProbRanksMap::const_iterator it = MA(x,u).begin();
	    for(unsigned j = 1;j <= i; j++, it++)
		cerr << "computeMA:\tMA(" << x.getNumber() << ", " 
		     << u.getNumber() <<  ")[" << j << "] = " 
		     << it->first.val() << endl;
#endif
	  }
      }

    return;
  }


  //! This function computes \f$ m_X^{(i)}(x,u,k) \f$ and saves it in 
  //! MX(x,u), together with the ranks of the \f $m_X \f$ that yielded it.
  //! Precondition: x.dominates(*sigma[u])
  //!               k <= slice_U]u]
  //!               x.isLeaf() == u.isLeaf() && sigma[u] == x 
  //------------------------------------------------------------
  void 
  MaxReconciledTreeModel::computeMX(Node& x, Node& u, unsigned k, unsigned i)
  {
    assert(k <= slice_U[u]);    // Check precondition

    if(k == 1)                  // We're at a speciation
      {
	if(MX(x,u)[1].empty())
	  {
	    // pass recursion on MX[1] will be filled by computeMV
	    computeMV(x,u,i);
	  }
      }
    else
      {
	assert(k>1);
	assert(x.dominates(*sigma[u])); //Check precondition

	// Prepare for passing on recursion
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();

	//Determine limits of l
	unsigned min = slice_L(x,v);
	unsigned max = std::min(k-slice_L(x,w), slice_U[v]);
	if(k > slice_U[w] + slice_L(x,v))
	  {
	    min = k - slice_U[w];
	  }
	assert(min >= slice_L(x,v));
	assert(max <= slice_U[v]);
	assert(k-min <= slice_U[w]);
	assert(k-max >= slice_L(x,w));

	for(unsigned l = min; l <= max; l++)
	  {
	    // First pass recursion on
	    // if u is symmetric, then the combinatorics demands that 
	    // we iterate over i+1 (see Arvestad et al.2007)
	    if(isomorphy[u]) 
	      {
		i++;
	      }
	    if(MX(x,v)[l].size() < i)
	      {
		computeMX(x, v, l, i);
	      }
	    if(MX(x,w)[k-l].size() < i)
	      {
		computeMX(x, w, k-l, i);
	      }
	    	
	    // then set MX, i.e.,  m_X(x,u) = m_X(x,v)*m_X(x,w) is used as 
	    // index and the ranks are j for m_X(x,v), and h for m_X(x,w)
	    ProbRanksMap::const_iterator mxj = MX(x,v)[l].begin();
	    ProbRanksMap::const_iterator mxh = MX(x,w)[k-l].begin();

	    // Figure out where to start. By looking at the size of MX(x,u)[k], we
	    // know how many we have made before; add 1 and we know where to start
	    unsigned start = MX(x,u)[k].size() + 1;
	    // forward the iterators
	    advance(mxj, start-1);
	    advance(mxh, start-1);

 	    for(unsigned j = start; j <= i; j++, mxj++)
	      {
 		for(unsigned h = start; h <= i; h++, mxh++)
		  {
		    MX(x,u)[k].insert
		      (std::make_pair(computeI(u,j,h,k,l)/(static_cast<Real>(k)-1) *
				      mxj->first * mxh->first,
				      std::make_pair(l,std::make_pair(j,h))));

#ifdef DEBUG_DP
		    ProbRanksMap::const_iterator it = MX(x,u)[k].begin();
		    for(unsigned j = 1;j <= i; j++, it++)
		      cerr << "computeMX:\tMX(" << x.getNumber() << ", " 
			   << u.getNumber() <<  ")[" << k << "][" << j 
			   << "] = " << it->first.val() << endl;
#endif
		  }
	      }
	  }
      }
    return;
  }

  unsigned
  MaxReconciledTreeModel::computeI(Node& u, unsigned i, unsigned j, 
				   unsigned k, unsigned l)
  {
    if(isomorphy[u])
      {
	if(l < k/2)
	  {
	    return 0;
	  }
	if(l == k/2)
	  {
	    if(i < j)
	      {
		return 0;
	      }
	    if(i == j)
	      {
		return 1;
	      }
	  }
      }
    return 2;
  }

}// end namespace beep
