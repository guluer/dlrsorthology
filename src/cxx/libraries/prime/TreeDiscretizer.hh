#ifndef TreeDiscretizerOld_HH
#define TreeDiscretizerOld_HH

#include <string>
#include <utility>
#include <vector>

#include "BeepVector.hh"
#include "Node.hh"
#include "Tree.hh"

namespace beep
{

/**
 * joelgs: This class is old and probably out of use. Don't confuse this
 * with the contents of 'TreeDiscretizers'.
 *
 * Encapsulates a tree DS resulting from augmenting a Tree S with discretization nodes. In the
 * future, this class may become a subclass of Tree.
 * 
 * More info to come (hopefully).
 */
class TreeDiscretizerOld
{	
	
public:

	/**
	 * Discretization points ("pure" or node corresponding) of an edge are
	 * retrieved using node-index pairs. The point on the actual node has index 0.
	 */
	typedef std::pair<const Node*, unsigned> Point;
	
	/**
	 * Constructor. Divides every edge with as few points as possible although not
	 * exceeding a specified timestep limit. One may also specify a minimum number
	 * of points for every edge.
	 * @param S the original underlying tree.
	 * @param maxTimestep the approximate time step between points, not exceeded.
	 * @param minNoOfPtsPerEdge minimum numbre of points per edge.
	 */
	TreeDiscretizerOld(Tree& S, Real maxTimestep, unsigned minNoOfPtsPerEdge);
	
	/**
	 * Constructor. Divides every edge using equally many points.
	 * @param S the original underlying tree.
	 * @param noOfPtsPerEdge the number of points per edge.
	 */
	TreeDiscretizerOld(Tree& S, unsigned noOfPtsPerEdge);
	
	/**
	 * Destructor.
	 */
	virtual ~TreeDiscretizerOld();
	
	/**
	 * Updates the entire discretized tree based on the underlying Tree.
	 */
	void update();
			
	/**
	 * Returns the underlying Tree.
	 * @return the Tree.
	 */
	Tree& getOrigTree() const;
	
	/**
	 * Returns the underlying Tree's root node.
	 * @return the root.
	 */
	const Node* getOrigRootNode() const;
	
	/**
	 * Returns the underlying Tree's node for a certain index.
	 * @param index the node's index.
	 * @return the node.
	 */
	const Node* getOrigNode(unsigned index) const;
	
	/**
	 * Returns the total number of points in the discretized tree.
	 * @return the number of points.
	 */
	unsigned getTotalNoOfPts() const;
	
	/**
	 * Returns the number of points for an edge.
	 * @param node the end node of the edge.
	 * @return the number of points.
	 */
	unsigned getNoOfPts(const Node* node) const;
	
	/**
	 * Returns the timestep size between points on an edge.
	 * @param node the end node of the edge.
	 * @return the 
	 */
	Real getTimestep(const Node* node) const;
	
	/**
	 * Returns the time length of the top time edge.
	 * @return the time.
	 */
	Real getTopTime() const;
	
	/**
	 * Returns the absolute toptime time value, i.e. root time + top time edge time. 
	 * @return the absolute toptime time.
	 */
	Real getTopToLeafTime() const;
	
	/**
	 * Returns the time length of an edge.
	 * @param node the lower node of the edge.
	 * @return the time length of the edge.
	 */
	Real getEdgeTime(const Node* node) const;
	
	/**
	 * Returns the points for an edge.
	 * @param node the end node of the edge.
	 * @return the points in a vector.
	 */
	const std::vector<Real>* getPts(const Node* node) const;
	
	/**
	 * Returns the time of a certain discretization point.
	 * No bounds checking is made.
	 * @param pt the point.
	 * @return the time of the point.
	 */
	Real getPtTime(Point pt) const;
	
	/**
	 * Returns the time of a point corresponding to a node.
	 * @param node the node.
	 * @return the time of the point.
	 */
	Real getPtTime(const Node* node) const;
	
	/**
	 * Returns the time difference between two discretization points.
	 * @param ptOne the first point.
	 * @param ptTwo the second point.
	 * @return time(ptOne) - time(ptTwo).
	 */
	Real getPtTimeDiff(Point ptOne, Point ptTwo) const;
	
	/**
	 * Returns the point directly above a specified point.
	 * @param pt the point.
	 * @return the point directly above.
	 */
	Point getParentPt(Point pt) const;
	
	/**
	 * Returns the left point directly below a specified node.
	 * @param the node.
	 * @return the left point below.
	 */
	Point getLeftChildPt(const Node* node) const;
	
	/**
	 * Returns the right point directly below a specified node.
	 * @param the node.
	 * @return the right point below.
	 */
	Point getRightChildPt(const Node* node) const;
	
	/**
	 * Returns the topmost point of the entire augmented tree.
	 * @return the point.
	 */
	Point getTopmostPt() const;

	/**
	 * Returns the topmost point of an edge.
	 * @param the node defining the edge.
	 * @return the point.
	 */
	Point getTopmostPt(const Node* node) const;
	
	/**
	 * Returns the number of point steps between two points.
	 * @param xPt the ancestral point.
	 * @param yPt the descendant point.
	 * @return the number of steps between the points.
	 */
	unsigned getNoOfStepsBetweenPts(Point xPt, Point yPt) const;
	
	/**
	 * Returns the smallest and largest edge times for all edges in the tree
	 * excluding the top time. The latter is returned alongside instead.
	 */
	void getMinMaxEdgeTime(Real& min, Real& max, Real& topTime) const;
	
	/**
	 * Returns the smallest and largest time steps for all edges in the tree
	 * excluding the top time edge. The time step for the latter is returned
	 * alongside instead.
	 */
	void getMinMaxTimestep(Real& min, Real& max, Real& topTimeVal) const;
	
	/**
	 * Returns the minimum and maximum number of points on each edge in the
	 * tree excluding the top time edge. The number for the latter is returned
	 * alongside instead.
	 */
	void getMinMaxNoOfPts(unsigned& min, unsigned& max, unsigned& topTimeVal) const;
	
	/**
	 * Prints miscellaneous information about the TreeDiscretizerOld to cerr.
	 * @param maxPrintedPtsPerEdge the max number of points to be listed per edge.
	 */
	void debugInfo(bool printNodeInfo=true) const;
	
private:

public:
	
private:
	
	/**
	 * The underlying original Tree.
	 */
	Tree& m_S;
	
	/**
	 * If true states that each edge has been divided by equally many points,
	 * namely the value stored in m_minNoOfPts. If false, an approximate target timestep
	 * will be used beteween points.
	 */
	bool m_equidivision;
	
	/**
	 * Only applicable when m_equidivision==false. Represents the target time step when
	 * an edge is divided by points. The actual value will never be exceeded.
	 * Note however, that each edge will always be forced to contain at least m_minNoOfPts points.
	 */
	Real m_maxTimestep;
	
	/**
	 * The minimum number of points each edge is required to have (excluding possibly a null
	 * top time edge). If m_equidivision==true, this refers to the number of points of each
	 * edge.
	 */
	unsigned m_minNoOfPts;
	
	/** Discretization time step sizes for each edge. */
	RealVector m_timesteps;
	
	/**
	 * The points of the discretized tree stored in vectors for each edge. The point
	 * corresponding to the end node is stored at index 0 of the vector and is followed by
	 * pure disc. points (with ascending time values).
	 */
	BeepVector< std::vector<Real>* > m_pts;
	
};

} // end namespace beep

#endif /*TreeDiscretizerOld_HH*/

