#include "ConstRateModel.hh"

#include "AnError.hh"
#include "Density2P.hh"
#include "Node.hh"
#include "Tree.hh"

#include <sstream>

//----------------------------------------------------------------------
//
// class ConstRateModel
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //public:
  //----------------------------------------------------------------------
  //
  // Constructor/Destructor/Assignment
  //
  //----------------------------------------------------------------------
  ConstRateModel::ConstRateModel(Density2P& rateProb, const Tree& T,
		  EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      EdgeRateModel_common(rateProb, T, rwp)
  {
    // All rates will have same value, simulate this by  edgeRates.size() = 1
    edgeRates = RealVector(1, rateProb.getMean());
  }
    
  // With a specified value for rate
  ConstRateModel::ConstRateModel(Density2P& rateProb, const Tree& T,
		  const Real& rate, EdgeWeightModel::RootWeightPerturbation rwp)
    : EdgeRateModel(),
      EdgeRateModel_common(rateProb, T, rwp)
  {
    // All rates will have same value, simulate this by  edgeRates.size() = 1
    edgeRates = RealVector(1, rate);
  }
    
  ConstRateModel::ConstRateModel(const ConstRateModel& crm)
    : EdgeRateModel(crm),
      EdgeRateModel_common(crm)
  {
  }
    
  ConstRateModel::~ConstRateModel() {};
  
  ConstRateModel&
  ConstRateModel::operator=(const ConstRateModel& crm)
  {
    if(this != &crm)
      {
	EdgeRateModel::operator=(crm);
	EdgeRateModel_common::operator=(crm);
      }
    return *this;
  }
    
  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  // Interface inherited from ProbabilityModel
  //----------------------------------------------------------------------

  // Probability of the (average) rate
  //----------------------------------------------------------------------
  Probability 
  ConstRateModel::calculateDataProbability()
  {
    return rateProb->operator()(edgeRates[0u]); //TODO: make nicer /bens
  };

  // Nothing to update
  //----------------------------------------------------------------------
  void  
  ConstRateModel::update()
  {
  };

  //----------------------------------------------------------------------
  // Interface inherited from EdgeRateModel
  //----------------------------------------------------------------------

  // Description of model
  //----------------------------------------------------------------------
  std::string 
  ConstRateModel::type() const
  {
    return "a const model ";
  }


  // Returns value of rate - which is the same for all nodes. 
  //----------------------------------------------------------------------
  Real  
  ConstRateModel::getRate(const Node* n) const
  {
    return edgeRates[0u]; //TODO: make nicer /bens
  };

  Real  
  ConstRateModel::getRate(const Node& n) const
  {
    return getRate(&n);
  };

  // Sets all edge rates to newRate - pointer version. Parameter node 
  // allows dynamic calls from EdgeRateModel.
  void 
  ConstRateModel::setRate(const Real& newRate, const Node* n)
  {
    if(rateProb->isInRange(newRate))
      {
	edgeRates[0u] = newRate; //TODO: make nicer /bens
	return;
      }
    else
      {
	ostringstream oss;
	oss << "ConstRatemodel::setRate(r): r = " 
	    << newRate
	    << " out of range!";
	throw AnError(oss.str());
      }
    }

  // sets all edge rates to newRate - reference version.
  void 
  ConstRateModel::setRate(const Real& newRate, const Node& n)
  {
    return setRate(newRate); // calls pointer version!
  }

  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
  
  // Always define an ostream operator!
  //------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const ConstRateModel& crm)
  {
    return o << crm.print();
  }

  std::string ConstRateModel::print() const 
  {
    return "The rates are constant over the tree (Mol. clock).\n" + 
      EdgeRateModel_common::print();
  };

  unsigned   
  ConstRateModel::nRates() const
  {
    return 1;
  }
}//end namespace beep
