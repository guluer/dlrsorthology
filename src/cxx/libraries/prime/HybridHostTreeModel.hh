#ifndef HYBRIDHOSTTREEMODEL_HH
#define HYBRIDHOSTTREEMODEL_HH

#include "ProbabilityModel.hh"

#include <map>
#include <vector>

namespace beep
{
  // Forward declarations
  class Node;
  class HybridTree;

  //----------------------------------------------------
  //
  //! Computes the probability of a hybrid network 
  //! under the species hybrid model
  //
  //----------------------------------------------------
  class HybridHostTreeModel : public ProbabilityModel
  {
  public:
    //----------------------------------------------------
    //
    //! \name Construct/destruct/assign
    //@{
    //----------------------------------------------------
    HybridHostTreeModel(HybridTree& S_in, Real speciation_rate,
			Real extinction_rate, Real hybridization_rate,
			unsigned maxGhost= 250);
    ~HybridHostTreeModel();
    HybridHostTreeModel(const HybridHostTreeModel& hhtm);
    HybridHostTreeModel& operator=(const HybridHostTreeModel& hhtm);
    // @}

    //----------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------
    void update(); 
    Probability calculateDataProbability();    
    std::string print() const;
    //! \name change model parameters
    //@{
    void setLambda(const Real& newValue);
    void setMu(const Real& newValue);
    void setRho(const Real& newValue);
    //@}

    //! Change # ghosts
    void setMaxGhosts(unsigned n);

    //----------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------

    //! Orders internal nodes using a std::map with node times 
    //! as keys, also record the number of concurrent lineages
    //! (Notice that, except for hybridizations, all node times are unique
    void initNodeOrder();    
    //! This should probably be moved to a subclass of BirthDeathProbs later
    void computeProbabilities(Real& qD, Real& qL, 
			      Real& qX, Real& qU, const Real& t);
    //! Precomputes values of K(i,j) combinatorial term accounting
    //! the number of ways to choose visible lineages at event times
    void fillKTable();

  protected:
    //----------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------
    HybridTree* S;
    Real lambda;   // Speciation rate
    Real mu;       // Extinction rate
    Real rho;      // Hybridization rate

    //! The maximum allowed number of ghosts at any time in the tree
    unsigned ghostMax;
    //! keeps track of order of internal nodes/events using nodet time t as key,
    //! as well as the number of concurrent lineages (L) at t
    std::map<Real, std::pair< Node*, unsigned> ,std::less<Real> > nodeOrder;
    //! Stores values of K(i,j) combinatorial term accounting 
    //! for the number of ways to choose visible linages at event times
    std::vector< std::vector<Real> > K;

    //! \name{structures saving temporary values during likelihood calculation}
    //!@{
    std::vector<Real> Phi1;
    std::vector<Real> Phi2;
    std::vector<Real> E1;
    std::vector<Real> E2;
    //!@}

  };

}// end namespace beep
#endif
