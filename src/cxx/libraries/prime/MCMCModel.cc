#include "MCMCModel.hh"

// Author: Lars Arvestad, Bengt Sennblad, � the MCMC-club, SBC, 
// all rights reserved
namespace beep
{

  // Initiate static PRNG rand
  PRNG MCMCModel::R(getpid());

  MCMCModel::MCMCModel()
    : MCMC_iteration(0),
      nAcceptedStates(0)
  {
  }

  MCMCModel::~MCMCModel()
  {};

  MCMCModel&
  MCMCModel::operator=(const MCMCModel& mm)
  {
    if(&mm != this)
      {
	MCMC_iteration = 0;
	nAcceptedStates = 0;
      }
    return *this;
  }
  

  void
  MCMCModel::registerCommit()
  {
    nAcceptedStates++;
    incIterationNumber();
  }


  void
  MCMCModel::registerDiscard()
  {
    incIterationNumber();
  }



  // Calculates the acceptance rate.
  //----------------------------------------------------------------------
  Real
  MCMCModel::getAcceptanceRatio() const
  {
    if (MCMC_iteration > 0)
      {
	return (Real)nAcceptedStates / (Real)MCMC_iteration;
      }
    else
      {
	return 0.0;
      }
  }

  void
  MCMCModel::incIterationNumber()
  {
    MCMC_iteration++;
  }


  unsigned long
  MCMCModel::iterationNumber() const
  {
    return MCMC_iteration;
  }

  PRNG &
  MCMCModel::getPRNG() const
  {
    return R;
  }

}//end namespace beep
