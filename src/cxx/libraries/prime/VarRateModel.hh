#ifndef VARRATEMODEL_HH
#define VARRATEMODEL_HH

#include "EdgeRateModel_common.hh"

#include <iostream>
#include <sstream>

namespace beep
{
  // Forward declarations
  class Node;
  class Tree;

  //TODO: Some of the pure virtual functions in VarRateModel could be removed
  // since it now inherits EdgeRateModel

  //----------------------------------------------------------------------
  //
  // class VarRateModel, inherits from EdgeRateModel
  //! Pure virtual base class for model of rates that vary over a tree T.
  //! These classes model the rate of each node or edge in the Tree T,
  //! with the underlying density function rateProbs.
  //!
  //! Precondition: T.getNumberOfLeaves() > 1
  //!
  //!  Authors Martin Linder, adapted by Bengt Sennblad,
  //!  copyright the MCMC club, SBC
  // Note that subclasses of this class is also declared in this file:
  //
  //----------------------------------------------------------------------
  class VarRateModel : public EdgeRateModel_common
  {

  public:

    //----------------------------------------------------------------------
    //
    // Constructor/Destructor
    //
    //----------------------------------------------------------------------
    //! Precondition: T,getNumberOf Nodes > 1
    VarRateModel(Density2P& rateProb, const Tree& T, EdgeWeightModel::RootWeightPerturbation rwp);

    //! with specified values for edgerates
    //! Precondition: T,getNumberOf Nodes > 1
    //! precondition: edgeRates_in == T.getNumberOfNodes()
    VarRateModel(Density2P& rateProb, const Tree& T,
		 const RealVector& edgeRates_in, EdgeWeightModel::RootWeightPerturbation rwp);
    VarRateModel(const VarRateModel& vrm);
    virtual ~VarRateModel();
    VarRateModel& operator=(const VarRateModel& vrm);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    virtual unsigned nRates() const;
    // Interface inherited from ProbabilityModel
    //----------------------------------------------------------------------
    //! Calculates joint probability of edge rates.
    //! Pr(rates) = \f$ \prod_{e\in E(T)}f(r(e)) \f$ according to model
    //----------------------------------------------------------------------
    virtual Probability calculateDataProbability()=0;
    virtual void update();

    // returns value of rate for node
    // However, we return the full rate value including the mean rates!
    //----------------------------------------------------------------------
    //     virtual Real getRate(const Node& node) const = 0;
    //     virtual Real getRate(const Node* node) const = 0;

    virtual void setRate(const Real& newRate, const Node& node);
    virtual void setRate(const Real& newRate, const Node* node);

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    // Always define an ostream operator!
    // This should provide a neat output describing the model and its
    // current settings for output to the user. It should list current
    // parameter values by li nking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const VarRateModel& erm);
    virtual std::string print() const;
    //! Returns the subtype of VarRateModel
    virtual std::string type() const;

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    //     RealVector edgeRates; //!< Keeps rates for all nodes/edges in tree T

  };




  //----------------------------------------------------------------------
  //
  // subclass iidRateModel
  //! All rates are iid with mean and variance given by Density2P
  //! rateProb. Rate of root edge is not modeled, while the child edges
  //! behaviour may be set in constructor.
  //
  //----------------------------------------------------------------------
  class iidRateModel : public VarRateModel
  {
  public:
    //----------------------------------------------------------------------
    //
    // Constructor/Destructor
    //
    //----------------------------------------------------------------------
    iidRateModel(Density2P& rateProb, const Tree& T,
		 EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    iidRateModel(Density2P& rateProb, const Tree& T,
		 const RealVector& edgeRates,
		 EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    iidRateModel(const iidRateModel& vrm);
    ~iidRateModel();
    iidRateModel& operator=(const iidRateModel& vrm);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Interface inherited from ProbabilityModel
    //----------------------------------------------------------------------
    virtual Probability calculateDataProbability();

    //! Returns value of rate for incoming edge to node - reference version.
    //----------------------------------------------------------------------
    virtual Real getRate(const Node& n) const;
    //! Returns value of rate for incoming edge to node - pointer version.
    //----------------------------------------------------------------------
    virtual Real getRate(const Node* n) const;
    virtual void setRate(const Real& newRate, const Node& node);
    virtual void setRate(const Real& newRate, const Node* node);

    //------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------

    // Always define an ostream operator!
    // This should provide a neat output describing the model and its
    // current settings for output to the user. It should list current
    // parameter values by linking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const iidRateModel& erm);
    virtual std::string print() const;

    virtual std::string type() const;

  protected:
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
    //! Helper function for calculateDataProbability()
    Probability recursiveDataProb(Node* n);

  };





  //----------------------------------------------------------------------
  //
  // subclass gbmRateModel -
  //! Geometric brownian motion, or evolving rate model � la Kishino &
  //! Thorne, 2001. Rates evolve over edges of the tree, in a time-
  //! dependent manner. Node rates are modeled and edge rate are
  //! approximated as the average of the two connected edges.
  //!
  //! For a node, n, the rate, r(n)\f$\sim D(\mbox{a,b})\f$, where r(n)
  //! is the rate of node n, a is set to the rate of the parent node,
  //! and b = variance * t(n), where t(n) is the time of the incoming
  //! edge to n.
  //! Due to over-parameterization, the root node's rate is not modeled.
  //! One of the children of the root (arbitrary choice is left child)
  //! is set as the starting point, and the two edges from the root to the
  //! children is considered a single edge. Addendum: this behaviour may be
  //! altered.
  //
  //----------------------------------------------------------------------
  class gbmRateModel : public VarRateModel
  {
  public:
    //----------------------------------------------------------------------
    //
    // Constructor/Destructor
    //
    //----------------------------------------------------------------------
    gbmRateModel(Density2P& rateProb, const Tree& T,
		 EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    gbmRateModel(Density2P& rateProb, const Tree& T, const RealVector& edgeRates,
		 EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    gbmRateModel(const gbmRateModel& vrm);
    ~gbmRateModel();

    gbmRateModel& operator=(const gbmRateModel& vrm);


    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    // Interface inherited from ProbabilityModel
    //----------------------------------------------------------------------
    virtual Probability calculateDataProbability();
    virtual std::string type() const;

    //! Returns mean of underlying density.
    virtual Real getMean() const;
    //! Returns variance of underlying density.
    virtual Real getVariance() const;

    //! Returns mean of underlying density.
    virtual void setMean(const Real& newValue);
    //! Returns variance of underlying density.
    virtual void setVariance(const Real& newValue);

    //! Returns edge rate as average of current and parent's rate
    //----------------------------------------------------------------------
    virtual Real getRate(const Node& n) const;
    //! Returns value of rate for incoming edge to node - pointer version.
    //----------------------------------------------------------------------
    virtual Real getRate(const Node* n) const;

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    // Always define an ostream operator!
    // This should provide a neat output describing the model and its
    // current settings for output to the user. It should list current
    // parameter values by linking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const gbmRateModel& erm);
    virtual std::string print()const
      ;
  protected:
    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------
    Probability recursiveDataProb(Node* n, Real parentRate);

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
    Real variance; //! Start 'variance' of gbm model
  };

}//end namespace beep
#endif
