#include "HybridHostTreeModel.hh"

#include "AnError.hh"
#include "HybridTree.hh"
#include "Node.hh"
#include "NormalDensity.hh"

#include <cassert>
#include <cmath>
#include <sstream>
#include <cassert>

namespace beep
{
  using namespace std;
  //----------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //----------------------------------------------------
  HybridHostTreeModel::HybridHostTreeModel(HybridTree& S_in, 
					   Real speciation_rate,
					   Real extinction_rate, 
					   Real hybridization_rate,
					   unsigned maxGhost)
    : ProbabilityModel(),
      S(&S_in),
      lambda(speciation_rate),
      mu(extinction_rate),
      rho(hybridization_rate),
      ghostMax(maxGhost+1),
      nodeOrder(),
      K(),//ghostMax, 0),
      Phi1(ghostMax,-1),
      Phi2(ghostMax,-1),
      E1(ghostMax,-1),
      E2(ghostMax,-1)
  {
    initNodeOrder();
    fillKTable();
  }

  HybridHostTreeModel::~HybridHostTreeModel()
  {}
  
  HybridHostTreeModel::HybridHostTreeModel(const HybridHostTreeModel& hhtm)
    : ProbabilityModel(hhtm),
      S(hhtm.S),
      lambda(hhtm.lambda),
      mu(hhtm.mu),
      rho(hhtm.rho),
      ghostMax(hhtm.ghostMax),
      nodeOrder(hhtm.nodeOrder),
      K(hhtm.K),
      Phi1(hhtm.Phi1),
      Phi2(hhtm.Phi2),
      E1(hhtm.E1),
      E2(hhtm.E2)
  {}

  HybridHostTreeModel&
  HybridHostTreeModel::operator=(const HybridHostTreeModel& hhtm)
  {
    if(&hhtm != this)
      {
	ProbabilityModel::operator=(hhtm);
	S = hhtm.S;
	lambda = hhtm.lambda;
	mu = hhtm.mu;
	rho = hhtm.rho;
	ghostMax = hhtm.ghostMax;
	nodeOrder = hhtm.nodeOrder;
	K = hhtm.K;
	Phi1 = hhtm.Phi1;
	Phi2 = hhtm.Phi2;
	E1 = hhtm.E1;
	E2 = hhtm.E2;
      }
    return *this;
  }
  
  //----------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------
  void 
  HybridHostTreeModel::update()
  {
    initNodeOrder();
  };
  
  //
  Probability 
  HybridHostTreeModel::calculateDataProbability()
  {
    using namespace std;
    // \phi(e_i, j), i.e., the probability that, given a maximunm of Kmax 
    // ghosts at any event, the hybrid model, starting at time t_i with 
    // l_i lineages and j ghosts gives the observed hybrid forest; 
    // t_i and l_i given by e_i.
    // It is stored in the Phi structures. The indexing by e_i is 
    // implicit, so that if e_i is the current event treated, then 
    // cPhi[j] stores the value of \phi(e_i, j), and pPhi[j] stores
    // the value of \phi(e_{i-1}, j)
    vector<Real>* pPhi = &Phi1;    // These are the names actually used
    vector<Real>* cPhi = &Phi2;    // p-previous, c-current
    vector<Real>* pE = &E1;        // These are the names actually used
    vector<Real>* cE = &E2;        // p-previous, c-current
    vector<Real>* tmp;             // for switching pE and cE

    // Partial probabilities
    Real qD = 0;  // 1-P(t)
    Real qL = 0;  // P(t)(1-u_t)
    Real qX = 0;  // e^(\lambda+rho+mu)t e^theta(X_t-X_0) =  
                  // e^(\lambda+rho+mu)t (e^(mu t) P(t))^theta
    Real qU = 0;  // u_t
    Real beta = 0;// prob of event

    Node* v = NULL;
    Real t = 0;


    //
    fill(cPhi->begin(), cPhi->end(),0); // set Phi structures to 'leaf-state',
    fill(pPhi->begin(), pPhi->end(),1.0); // i.e., 1.0 for j=0 and 0 else
    (*pPhi)[0] = 1.0;
    for(map<Real, pair<Node*, unsigned> >::iterator event = nodeOrder.begin(); 
	event != nodeOrder.end(); event++)
      {	
	// reset data structure E
	fill(pE->begin(), pE->end(),1.0);
	// get the parameters specific to event
	v = (*event).second.first;               // event = internal node
	unsigned L = (*event).second.second + 1; // 1 extra lineage after event

	Node* hybrid = S->getHybridChild(*v);    // NULL if v is not a hybrid
	if(hybrid)
	  {
	    // Event is a hybridization
	    // Determine number of lineages
	    if(S->isExtinct(*hybrid->getSibling())) 
	      {
		L--;
	      }
	    if(S->isExtinct(*S->getOtherSibling(*hybrid))) 
	      {
		L--;
	      }
	    assert(L > 0);
	    beta = 2*rho; // This will later be divided by (L+j) where j=#ghosts
	  }
	else
	  {
	    //Event is a speciation
	    beta = lambda;
	  }

	t = S->getTime(*v) - t;
	computeProbabilities(qD, qL, qX, qU, t);
	
	// Case j = 0
	*pE = K[L];

	(*cPhi)[0] = (*pPhi)[0] = (*pE)[0];
	// Case j > 0
	for(unsigned j = 1; j < ghostMax; j++)
	  {
	    (*cE)[0] = (*pE)[0] * qD;
	    (*cPhi)[j]  = 0;
	    for(unsigned k = 1; k < ghostMax; k++)
	      {
		(*cE)[k] = (*pE)[k] * qD;
		for(unsigned m = 1; m < k; m++)
		  {
		    (*cE)[k] += (*pE)[k-m] * std::pow(qL, static_cast<int>(m));
		  }
		(*cPhi)[j] += (*pPhi)[k] * (*cE)[k] * pow(qU,k);
	      }
	    (*cPhi)[j] *= beta * pow(qX, L);
	    if(hybrid)
	      {
		(*cPhi)[j] /= (L + j);
	      }
	    tmp = pE;
	    pE = cE;
	    cE = tmp;
	  }    
	tmp = pPhi;
	pPhi = cPhi;
	cPhi = tmp;
      }

    // We are now at the root sum Phi over number of ghosts and return result
    Real ret = 0;
    computeProbabilities(qD, qL, qX, qU, S->rootToLeafTime());
    for(unsigned k = 0; k < ghostMax; k++)
      {
	ret += (*pPhi)[k];// * std::pow(qU, 10*static_cast<int>(k));
      }

    return ret;
  }
    
  std::string 
  HybridHostTreeModel::print() const
  {
    std::ostringstream oss;
    oss << "HybridHostTreeModel:\n"
	<< "Computes probability of a host tree with given node times\n"
	<< "and potentially with (non-binary) hybridizations.\n"
	<< "Parental lineages that participate in a hybridization, but\n"
	<< "later goes extinct must be present in HybridTree (but may be\n"
	<< "pruned from binary tree)\n"
	<< "Parameters:\n"
	<< "Host tree is as follows:\n"
	<< S->print()
	<< "Speciation rate, lambda = " << lambda << "\n"
	<< "Extinction rate, mu = " << mu << "\n"
	<< "Hybridization rate, rho = " << rho << "\n"
	<< "\n";
    return oss.str();
  }

  void 
  HybridHostTreeModel::setLambda(const Real& newValue)
  {
    assert(newValue > 0);
    lambda = newValue;
  }

  void 
  HybridHostTreeModel::setMu(const Real& newValue)
  {
    assert(newValue > 0);
    mu = newValue;
  }

  void 
  HybridHostTreeModel::setRho(const Real& newValue)
  {
    assert(newValue > 0);
    rho = newValue;
  }

  void 
  HybridHostTreeModel::setMaxGhosts(unsigned n)
  {
    ghostMax = n + 1;
    K.clear();
    Phi1.resize(ghostMax,-1);
    Phi2.resize(ghostMax,-1);
    E1.resize(ghostMax,-1);
    E2.resize(ghostMax,-1);
    fillKTable();
  }

  //----------------------------------------------------
  //
  // Implementation
  //
  //----------------------------------------------------

  //! Orders internal nodes using a std::map with node times 
  //! as keys, also record the number of concurrent lineages
  //! (Notice that, except for hybridizations, all node times are unique
  void 
  HybridHostTreeModel::initNodeOrder()
  {
    nodeOrder.clear();
    for(unsigned i = 0; i < S->getNumberOfNodes(); i++)
      {
	Node* u = S->getNode(i);

	// Ignore leaves and nodes that represents other parents
	// (to avoid that hybridization events are counted twice)
	if(u->isLeaf() == false && 
	   (S->getOtherParent(*u->getLeftChild()) != u &&
	    S->getOtherParent(*u->getRightChild()) != u))
	  {
	    Real t = S->getTime(*u);
	      
	    if(u->isRoot() == false && 
	       t - S->getTime(*u->getLeftChild()) == 0 &&
	       t - S->getTime(*u->getRightChild()) == 0)
	      {
		// This indicates  autoploidy -- ignore
		// but check sanity first
		assert(S->getHybridChild(*u->getLeftChild()) ==
		       S->getHybridChild(*u->getRightChild()));
		assert(S->isExtinct(*S->getHybridChild(*u->getLeftChild())
				    ->getSibling())||
		       S->isExtinct(*S->getOtherSibling(*S->getHybridChild
						       (*u->getLeftChild()))));
	      }
	    else
	      {
		// Now find out how many lineages there are at time t
		unsigned n = 1;
		for(unsigned j = 0; j < S->getNumberOfNodes(); j++)
		  {
		    Node* v = S->getNode(j);
		    if(S->getTime(*v) < t &&
		       (v->isRoot() || S->getTime(*v->getParent()) > t))
		      {
			n++;
		      }
		  }
		nodeOrder[t] = std::make_pair(u,n);
	      }
	  }
      }
//     for(std::map<Real, std::pair< Node*, unsigned> ,std::less<Real> >::iterator i = nodeOrder.begin(); i != nodeOrder.end(); i++)
//       {
// 	cerr << i->second.first->getNumber() << endl;
//       }
//     cerr << *S;
    return;
  }
  
  // This should probably be moved to a subclass of BirthDeathProbs later
  // qU = u_t, qL = P(t)(1-u_t), qD = 1-P(t), 
  // qX = exp{-(\lambda+rho+mu)t+\theta(X_{t1}-X_{0})} 
  // t = t1-t2, theta = (lambda+rho)/(2(lambda+rho))
  void 
  HybridHostTreeModel::computeProbabilities(Real& qD, Real& qL, 
					    Real& qX, Real& qU, const Real& t)
  {
    using namespace std;
    Real diff = lambda + rho - mu; // + mu eller hur
    if(diff == 0)
      {
	Real denom(1.0 + (mu * t)); 
	assert(denom > 0);
	// P_t = 1.0 / denom; u_t = (mu * t) / denom;
	qD = qU = (mu * t) / denom;
	qL = 1.0 / (denom * denom);
	qX = std::exp(-2*mu) * std::pow(mu * t - std::log(1+mu*t), 1 - rho / (2*(lambda +rho)));//?????
	assert(qL <1.0);
	assert(qD <1.0);
	assert(qU <1.0);
	assert(qX <1.0);
      }
    else if(mu == 0)
      {
	// P_t = 1.0; u_t = 1.0 - std::exp(-diff * t);
	qD = 0;
	qL = std::exp(-diff * t);
	qU = 1.0 - qL;
	
	throw AnError("This does not work?", 1);
      }
    else
      {
	Real E = std::exp(-diff * t);
	Real denom = lambda + rho - mu * E;
	assert(denom != 0);
	assert(E > 0);
	  
	// P_t = diff / denom; u_t = 1.0 - P_t * E;
	qD = 1.0 - diff/denom;
	qL = std::pow(diff / denom, 2) * E;
	qU = 1.0 - diff / denom * E;

	E = std::exp(-(lambda+rho+mu));
	Real E2 = std::exp(mu * t);
	qX = E * std::pow(diff / denom * E2, 1 - rho / (2*(lambda +rho)));
	assert(qL <1.0);
	assert(qD <1.0);
	assert(qU <1.0);
	assert(qX <1.0);
      }
    assert(qD > 0);
    assert(qL > 0);
    assert(qX > 0);
    assert(qU > 0);
    return;
  }

  // Precomputes values of K(k)
  void 
  HybridHostTreeModel::fillKTable()
  {
    vector<Real> preK(ghostMax,0);
    vector<Real> A(ghostMax,0);    // Actual data structures with dummy names
    vector<Real> B(ghostMax,0);
    vector<Real>* cK = &A;  // holds prepreK[i][j] for 'current' value of i
    vector<Real>* pK = &B;  // holds prepreK[i-1][j], i.e., for 'previous' value of i
    vector<Real>* tmp; // helper pointer to switch pK and cK

    Real beta = (rho + 2 * lambda) / (2 * (rho + lambda));

    // Case j = 1
    preK[0] = 1.0;
    for(unsigned k = 1; k < ghostMax; k++)
      { 
	(*pK)[k] = 1.0 / static_cast<double>(k);
	preK[k] += beta * (*pK)[k];
      }
    //Case j > 1
    for(unsigned j = 2; j < ghostMax; j++)
      {
// 	assert(preK[0] > 0);
	for(unsigned k = j; k < ghostMax; k++)
	  {
	    for(unsigned m = 1; m < k-1; m++)
	      { 
		(*cK)[k] += ((*pK)[m] / static_cast<double>(k));
	      }
 	    preK[k] += (std::pow(beta, static_cast<int>(j)) * (*cK)[k]);
	    assert(preK[k] >0 && preK[k] < 1.0);
	  }
	// Save cK as pK and set a fresh new cK
	tmp = pK;
	pK = cK;
	cK = tmp;
	fill(cK->begin(),cK->end(), 0);
      }

    // Note preK now holds the result for K(ghostMax)
    // Now compute K'
    K.push_back(vector<Real>());
    K.push_back(preK);
//     for(unsigned k = 0; k < ghostMax; k++)
//       {
//  	cerr <<"preK[" <<k<<"] = "<<preK[k] << endl;
//       }
//     for(unsigned k = 0; k < ghostMax; k++)
//       {
//  	cerr <<"K[" <<1 << "][" <<k<<"] = "<<K[1][k] << endl;
//       }
    
//     for(unsigned l = 2; l < ghostMax; l++)
    for(unsigned l = 2; l < S->getNumberOfLeaves(); l++)
      {
	K.push_back(vector<Real>(ghostMax, 0));
	K[l][0] = 1.0;
	for(unsigned k = 1; k < ghostMax; k++)
	  {
	    for(unsigned m = 0; m < k; m++)
	      {
		K[l][k] += K[l-1][k-m] * preK[m];
	      }
	    assert(K[l][k] > 0);
//    	    cerr <<"K[" <<l << "][" <<k<<"] = "<<K[l][k] << endl;
	  }
      }
    
    return;
  }

}// end namespace beep
