#ifndef LAMBDAMAP_H
#define LAMBDAMAP_H

#include "BeepVector.hh"

#include <string>

namespace beep
{
  // Forward declarations
  class Node;
  class StrStrMap;
  class Tree;


  //--------------------------------------------------------------------
  //
  //! LambdaMap implements the map between gene node and species node,
  //! called \sigma in our JACM paper. For historical reasons, however,
  //! the class is unfortunately named LambdaMap.
  //!
  //! lambda is defined as follows
  //! 1. If g \in leaves(G) then \lambda(g) = s \in leaves(S), in the 
  //!    natural way.
  //! 2. Otherwise, \lambda(g) = MRCA(lambda(left(g)), lambda(right(g))).
  //!
  //! LambdaMap is a specialization of BeepVector<Node*> (NodeVector)
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  //
  //--------------------------------------------------------------------
  class LambdaMap : public NodeVector
  {
  public:
    LambdaMap(const Tree& G, const Tree& S, const StrStrMap &gs);
    virtual ~LambdaMap();
    LambdaMap(const LambdaMap& l);
    LambdaMap& operator=(const LambdaMap& l);

    void update(const Tree& G, const Tree& S, StrStrMap* gs = 0);

  protected:
    // Preliminary -- for use with UserLambdaMap subclass
    LambdaMap(const Tree& G, const Tree& S); 

  public:
    friend std::ostream& operator<<(std::ostream &o, const LambdaMap& l);
    std::string print() const;

    //--------------------------------------------------------------------
    //
    // Implementation
    //
    //--------------------------------------------------------------------
  protected:

    //! Set up lambda map
    //! Species nodes returned
    //! gs maps gene names to species names
    Node* compLeafLambda(Node *g, const Tree& S, const StrStrMap &gs); 

    //! Compute the rest of lambda
    Node* recursiveLambda(Node *g, const Tree& S, const StrStrMap& gs); 

    //! Compute the new lambda using existing lambda
    Node* recursiveLambda(Node *g, const Tree& S); 

    //--------------------------------------------------------------------
    //
    // Attirbutes
    //
    //--------------------------------------------------------------------
    std::string description;
  };

}// end namespace beep


#endif
