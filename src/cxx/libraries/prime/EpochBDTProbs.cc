#include <cassert>
#include <cmath>
#include <numeric>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "EpochBDTProbs.hh"

namespace beep
{

using namespace std;

EpochBDTProbs::EpochBDTProbs(
		const EpochTree& ES,
		Real birthRate,
		Real deathRate,
		Real transferRate,
		unsigned noOfTransferCounts) :
	ODESolver(REL_TOL, ABS_TOL, true, true),
	PerturbationObservable(),
	m_ES(ES),
	m_birthRate(0.0),
	m_birthRateOld(0.0),
	m_deathRate(0.0),
	m_deathRateOld(0.0),
	m_transferRate(0.0),
	m_transferRateOld(0.0),
	m_rateSum(0.0),
	m_rateSumOld(0.0),
	m_Qe(ES, 0.0),
	m_Qef(ES, 0.0),
	m_counts(0),
	m_Qefk(noOfTransferCounts, RealEpochPtPtMap(ES, 0.0)),
	wi(0),
	wt(0),
	ws(0),
	wlast(0),
	wn(0),
	wnorm(0)
{
	Real mr = 0.95 * getMaxAllowedRate();
	if (birthRate > mr)
	{
		birthRate = mr;
		cout << "# High initial birth rate; changing it to " << mr
			<< " (95% of max limit)." << endl;
	}
	if (deathRate > mr)
	{
		deathRate = mr;
		cout << "# High initial death rate; changing it to " << mr
			<< " (95% of max limit)." << endl;
	}
	if (transferRate > mr)
	{
		transferRate = mr;
		cout << "# High initial transfer rate; changing it to " << mr
			<< " (95% of max limit)." << endl;
	}

	// Should suffice ('update()' is slightly slower).
	setRates(birthRate, deathRate, transferRate);
}


EpochBDTProbs::~EpochBDTProbs()
{
}


void
EpochBDTProbs::update()
{
	// Reinitialize storage structures in case tree
	// topology or discretization has changed.
	m_Qe = RealEpochPtMap(m_ES, 0);
	m_Qef = RealEpochPtPtMap(m_ES, 0);
	unsigned noOfTransferCounts = m_Qefk.size();
	m_Qefk = vector<RealEpochPtPtMap>(noOfTransferCounts, RealEpochPtPtMap(m_ES, 0));
	
	// Recompute probabilities.
	calcProbsWithinEpochs();
	calcProbsBetweenEpochs();
}


Real
EpochBDTProbs::getBirthRate() const
{
	return m_birthRate;
}


Real
EpochBDTProbs::getDeathRate() const
{
	return m_deathRate;
}


Real
EpochBDTProbs::getTransferRate() const
{
	return m_transferRate;
}


Real
EpochBDTProbs::getRateSum() const
{
	return m_rateSum;
}


void
EpochBDTProbs::getRates(Real& birthRate, Real& deathRate, Real& transferRate) const
{
	birthRate = m_birthRate;
	deathRate = m_deathRate;
	transferRate = m_transferRate;
}


void
EpochBDTProbs::setRates(Real newBirthRate, Real newDeathRate, Real newTransferRate)
{
	if (newBirthRate < 0 || newDeathRate < 0 || newTransferRate < 0)
	{ throw AnError("Cannot have negative rate in EpochBDTProbs.", 1); }

	m_birthRate = newBirthRate;
	m_deathRate = newDeathRate;
	m_transferRate = newTransferRate;
	m_rateSum = m_birthRate + m_deathRate + m_transferRate;
	
	// Note: Calling update() instead is slightly slower due to
	// reinitialization of map data structures with respect to host tree
	// discretization topology. This should just overwrite previous values.
	calcProbsWithinEpochs();
	calcProbsBetweenEpochs();
}


Real
EpochBDTProbs::getMaxAllowedRate() const
{
	// Use root-to-leaf time as indicator of scale.
	// Discard top time since it may vary dramatically
	// (unless it refers to the only edge).
	Real t = m_ES.getRootToLeafTime();
	if (t == 0)
	{
		t = m_ES.getTopTime();
	}
	const Real MAX_INTENSITY = 10.0;
	return (MAX_INTENSITY / t);
}


const Tree&
EpochBDTProbs::getTree() const
{
	return m_ES.getOrigTree();
}


string
EpochBDTProbs::getTreeName() const
{
	return m_ES.getOrigTree().getName();
}


Real
EpochBDTProbs::getRootToLeafTime() const
{
	return m_ES.getRootToLeafTime();
}
	
	
Real
EpochBDTProbs::getTopTime() const
{
	return m_ES.getTopTime();
}
	
	
Real
EpochBDTProbs::getTopToLeafTime() const
{
	return m_ES.getTopToLeafTime();
}


void
EpochBDTProbs::cache()
{
	m_birthRateOld = m_birthRate;
	m_deathRateOld = m_deathRate;
	m_transferRateOld = m_transferRate;
	m_rateSumOld = m_rateSum;
	m_Qe.cache();
	m_Qef.cache();
}


void
EpochBDTProbs::restoreCache()
{
	m_birthRate = m_birthRateOld;
	m_deathRate = m_deathRateOld;
	m_transferRate = m_transferRateOld;
	m_rateSum = m_rateSumOld;
	m_Qe.restoreCache();
	m_Qef.restoreCache();
}


const vector<RealEpochPtPtMap>*
EpochBDTProbs::getOneToOneProbsForCounts()
{
	// m_counts is used as flag for transfer
	// count computations: 0 in normal case,
	// >0 when to perform such calculations.
	m_counts = m_Qefk.size();
	if (m_counts > 0)
	{
		calcProbsWithinEpochs();
		calcProbsBetweenEpochs();
	}
	m_counts = 0;
	return (&m_Qefk);
}


void
EpochBDTProbs::appendInitVals(realvec& Q) const
{
	Q.insert(Q.end(), (1 + m_counts) * wn * wn, 0.0);
	realvec::iterator it = Q.begin() + wn;
	
	// ID matrix for Qef.
	for (unsigned e = 0; e < wn; ++e)
	{
		it[e * wn + e] = 1.0;
	}
	
	// ...and for Qef0, if doing counts. Only zeros for Qef1, Qef2, ...
	if (m_counts > 0)
	{
		it += (wn * wn);
		for (unsigned e = 0; e < wn; ++e)
		{
			it[e * wn + e] = 1.0;
		}
	}
}

		
void
EpochBDTProbs::fcn(Real t, const realvec& Q, realvec& dQdt)
{
	// Defines the organization of solver's concatenated vectors.
	// First wn elements are reserved for extinction probs, Qe.
	// Next wn*wn elements are one-to-one probs, Qef.
	// When doing counts, there are more elements, see below.
	realvec::const_iterator Qef = Q.begin() + wn;
	realvec::iterator dQefdt = dQdt.begin() + wn;
	
	// Compute sum of Qe.
	Real sumqe = accumulate(Q.begin(), Qef, Real(0.0));
	
	// For each f (sic!), compute sum of Qef.
	realvec sumqxf(wn, 0.0);
	for (unsigned e = 0; e < wn; ++e)
	{
		for (unsigned f = 0, ef = e * wn; f < wn; ++f, ++ef)
		{
			sumqxf[f] += Qef[ef];
		}
	}
	
	// Compute derivatives.
	for (unsigned e = 0; e < wn; ++e)
	{
		Real qe = Q[e];
		Real sumqg = sumqe - qe;
		
		// dQedt = delta*Qe(t)^2 + tau/(n-1)*Qe*sum_{f in E\e}Qf(t) + mu - phi*Qe(t).
		dQdt[e] = m_birthRate * qe * qe + wnorm * qe * sumqg +
			m_deathRate - m_rateSum * qe;
		
		// dQefdt = 2*delta*Qe(t)*Qef(t,t0) + tau/(n-1)*(Qe(t)*sum_{g in E\e}Qgf(t,t0) +
		// Qef(t,t0)*sum_{g in E\e}Qg(t)) - phi*Qef(t,t0).
		for (unsigned f = 0, ef = e * wn; f < wn; ++f, ++ef)
		{
			Real qef = Qef[ef];
			dQefdt[ef] = 2 * m_birthRate * qe * qef +
				wnorm * (qe * (sumqxf[f] - qef) + qef * sumqg) - m_rateSum * qef;
		}
	}
	
	// If the transfer count flag is set, we should solve with respect to
	// transfer counts too. That means that the next m_counts*wn*wn elements
	// contain the one-to-one probs for exactly k transfers, Qefk, for 0<=k<=m_counts-1.
	if (m_counts > 0)
	{
		fcnForCounts(Q, dQdt, sumqe);
	}
}


void
EpochBDTProbs::fcnForCounts(const realvec& Q, realvec& dQdt, Real sumqe)
{
	// Indexing in Q:
	// First wn elements are extinction probs Qe.
	// Next wn*wn elements are Qef.
	// Next wn*wn elements are Qef0, next wn*wn are Qef1, etc.
	unsigned wn2 = wn * wn;
	realvec::const_iterator Qefk = Q.begin() + (wn + wn2);
	realvec::iterator dQefkdt = dQdt.begin() + (wn + wn2);
	
	// For each (f,k), compute sum of Qefk.
	vector<realvec> sumqxfk(m_counts, realvec(wn, 0.0));
	for (unsigned k = 0; k < m_counts; k++, Qefk += wn2)
	{
		for (unsigned e = 0; e < wn; ++e)
		{
			for (unsigned f = 0, ef = e * wn; f < wn; ++f, ++ef)
			{
				sumqxfk[k][f] += Qefk[ef];
			}
		}
	}
		
	// Reset, and let z denote the k preceding the current.
	Qefk = Q.begin() + (wn + wn2);
	realvec::const_iterator Qefz = Qefk - wn2;
	
	// Compute derivatives for k=0,1,2...
	for (unsigned k = 0; k < m_counts; ++k, Qefk += wn2, dQefkdt += wn2, Qefz += wn2)
	{
		for (unsigned e = 0; e < wn; ++e)
		{
			Real qe = Q[e];
			Real sumqg = sumqe - qe;
			
			// dQefkdt = 2*delta*Qe(t)*Qefk(t,t0) + tau/(n-1)*(Qe(t)*sum_{g in E\e}Qgfz(t,t0) +
			// 			 Qefk(t,t0)*sum_{g in E\e}Qg(t)) - phi*Qefk(t,t0).
			for (unsigned f = 0, ef = e * wn; f < wn; ++f, ++ef)
			{
				Real sumqgfz = (k == 0) ?  0 : (sumqxfk[k-1][f] - Qefz[ef]);
				Real qef = Qefk[ef];
				dQefkdt[ef] = 2 * m_birthRate * qe * qef +
					wnorm * (qe * sumqgfz + qef * sumqg) - m_rateSum * qef;
			}
		}
	}
}


ODESolver::ExtSolResult
EpochBDTProbs::solout(unsigned no, Real told, Real t, realvec& Q)
{	
	// Store probabilities for discretized times the solver has passed.
	// Extinction probs. need only be stored once for an epoch, and are
	// reused in later iterations.
	// Since solver may in rare cases return negative values v=0-eps,
	// we always store max(v,0), without altering solver's current solution.
	
	realvec Qint;  // Note: Must be valid during iterator lifespan.
	while (ws <= wlast && m_ES[wi].getTime(ws) < t + 1e-8)
	{
		realvec::const_iterator it = Q.begin();
		if (abs(t - m_ES[wi].getTime(ws)) > 1e-8)
		{
			// If not on a discretization time, interpolate.
			contd5(Qint, m_ES[wi].getTime(ws));
			it = Qint.begin();
		}
		
		// Store values for time-tuple (s,t).
		if (wt == 0) { m_Qe.setWithMin(wi, ws, it, 0.0); }
		it += wn;
		m_Qef.setWithMin(wi, ws, wi, wt, it, 0.0);
		
		// If counting transfers, store such values too.
		for (unsigned k = 0; k < m_counts; ++k)
		{
			it += (wn * wn);
			m_Qefk[k].setWithMin(wi, ws, wi, wt, it, 0.0);
		}
		
		++ws;
	}
	return ODESolver::SOLUTION_NOT_CHANGED;
}


void
EpochBDTProbs::calcProbsWithinEpochs()
{
	// We start iterating at leaf epoch.
	wi = 0;
	wlast = m_ES[wi].getNoOfTimes() - 1;
	wn = m_ES[wi].getNoOfEdges();
	wnorm = m_transferRate / (wn - 1);
	
	// The vector of components used in ODE solving, Q, is concatenated this way
	// (all with respect to the single current epoch):
	// First wn elements are Qe(t), corresponding to prob. of extinction for
	// a single lineage at time t in edge e.
	// Next wn*wn elements are Qef(s,t), denoting prob. of single surviving mortal
	// at time t in edge f when a single lineage starts at time s in edge e.
	// Conditionally, next m_counts*wn*wn elements are similar to Qef(s,t), but
	// when exactly k transfers have occurred along the way, k=0,...,m_counts-1.
	realvec Q;
	Q.reserve(wn + (1 + m_counts) * wn * wn);
	
	// Initial values at t=0 for leaf epoch:
	// Qe=0, while Qee=1 and Qef=0 where e<>f.
	Q.assign(wn, 0.0);
	appendInitVals(Q);
	
	// For each epoch i strictly below top time epoch.
	while (wn > 1)
	{
		// For each lower discretized time wt.
		for (wt = 0; wt <= wlast; ++wt)
		{
			// Initial values. Already set for wt==0.
			if (wt > 0)
			{
				Q = m_Qe(wi, wt);
				appendInitVals(Q);
			}
			
			// For each upper discretized time ws >= wt.
			ws = wt;
			Real t = m_ES[wi].getTime(ws);
			
			if (ws == wlast)
			{
				// Explicitly store probs. for ws==wt==wlast.
				solout(0, t, t, Q);
			}
			else
			{
				// Solve ODE system from wt to up to wlast.
				// Probs. for ws = 0...wlast are implicitly stored
				// in solout() callbacks.
				Real h = 0;
				dopri5(t, m_ES[wi].getUpperTime(), Q, h);
			}
		}
		
		// Update Q for next epoch by merging values of the two edges
		// that joined going upwards.
		++wi;
		unsigned split = m_ES.getSplitIndex(wi);
		Q[split] = Q[split] * Q[split + 1];
		Q.resize(wn);
		Q.erase(Q.begin() + split + 1);
		wlast = m_ES[wi].getNoOfTimes() - 1;
		--wn;
		wnorm = m_transferRate / (wn - 1);
		appendInitVals(Q);
	}
	
	// Compute probabilities for top time edge "Kendall way"
	// since no transfers may take place here.
	assert(Q.size() == 1 + 1 + m_counts);
	Real D = Q[0];
	Real o2o = 1.0;
	Real PtFull, utFull, PtHalf, utHalf;
	calcPtAndUt(m_ES[wi].getTimestep(), PtFull, utFull);
	calcPtAndUt(m_ES[wi].getTimestep()/2.0, PtHalf, utHalf);
	for (wt = 0; wt <= wlast; ++wt)
	{
		if (wt > 0)
		{
			D = m_Qe(wi, wt, 0);
			o2o = 1.0;
		}
		for (ws = wt; ws <= wlast; ++ws)
		{
			// Store values more less same way as in solout().
			if (wt == 0) { m_Qe(wi, ws, 0) = D; }
			m_Qef(wi, ws, 0, wi, wt, 0) = o2o;
			
			// When counting, we can only have 0 transfers.
			if (m_counts > 0)
			{
				m_Qefk[0](wi, ws, 0, wi, wt, 0) = o2o;
				for (unsigned k = 1; k < m_counts; ++k)
				{
					m_Qefk[k](wi, ws, 0, wi, wt, 0) = 0.0;
				}
			}
			// Update for next upper endpoint. Only half timestep at epoch boundaries.
			bool halfTimestep = (ws == 0 && wt == 0) || (ws + 1 == wlast);
			Real Pt = halfTimestep ? PtHalf : PtFull;
			Real ut = halfTimestep ? utHalf : utFull;
			o2o = o2o * Pt * (1.0 - ut) / ((1.0 - ut * D) * (1.0 - ut * D));
			D = 1.0 - Pt * (1.0 - D) / (1.0 - ut * D);
		}
	}
}


void
EpochBDTProbs::calcProbsBetweenEpochs()
{
	// For every upper epoch i.
	for (unsigned i = 1; i < m_ES.getNoOfEpochs(); ++i)
	{
		// For every lower epoch j strictly beneath i.
		for (unsigned j = 0; j < i; ++j)
		{
			calcProbsBetweenEpochs(i, j);
		}
	}
}


void
EpochBDTProbs::calcProbsBetweenEpochs(unsigned i, unsigned j)
{
	const EpochPtSet& epi = m_ES[i];
	const EpochPtSet& epj = m_ES[j];
	unsigned lastti = epi.getNoOfTimes() - 1;
	unsigned lastei = epi.getNoOfEdges() - 1;
	unsigned lasttj = epj.getNoOfTimes() - 1;
	unsigned lastej = epj.getNoOfEdges() - 1;
	
	// Let z refer to epoch just below i.
	unsigned z = i - 1;
	const EpochPtSet& epz = m_ES[z];
	unsigned lastzt = epz.getNoOfTimes() - 1;
	
	// Edge number g in epoch i split into edges number
	// g and g+1 in epoch z.
	unsigned g = m_ES.getSplitIndex(i);
	Real Dgp = m_Qe.getForLastTime(z, g);
	Real Dgb = m_Qe.getForLastTime(z, g + 1);
	
	// For every upper point time s.
	for (unsigned s = 0; s <= lastti; ++s)
	{
		// For every upper point edge e.
		for (unsigned e = 0; e <= lastei; ++e)
		{
			// For every lower point time t.
			for (unsigned t = 0; t <= lasttj; ++t)
			{
				// For every lower point edge f.
				for (unsigned f = 0; f <= lastej; ++f)
				{
					// COMPUTE PROBABILITY Qef(i,s,e,j,t,f).
					Real& qef = m_Qef(i, s, e, j, t, f);
					
					// First treat case with edge g.
					qef = m_Qef(i, s, e, i, 0, g) *
						(m_Qef(z, lastzt, g, j, t, f) * Dgb + m_Qef(z, lastzt, g + 1, j, t, f) * Dgp);
										
					// For every edge h in epoch i besides g. hb refers to same edge, but below.
					for (unsigned h = 0, hb = 0; h <= lastei; ++h, ++hb)
					{
						if (h == g) { ++hb; continue; }
						qef += m_Qef(i, s, e, i, 0, h) * m_Qef(z, lastzt, hb, j, t, f);
					}
					
					// COMPUTE PROBABILITY Qefk(i,s,e,j,t,f) FOR EVERY k.
					// IN MANY RESPECTS ANALOGOUS TO ABOVE.
					for (unsigned k = 0; k < m_counts; ++k)
					{
						Real& qefk = m_Qefk[k](i, s, e, j, t, f);
						qefk = 0.0;  // Must reset first.
							
						// For every kp+kb=k.
						for (unsigned kp = 0, kb = k - kp; kp <= k; ++kp, --kb)
						{
							qefk += m_Qefk[kp](i, s, e, i, 0, g) *
								(m_Qefk[kb](z, lastzt, g, j, t, f) * Dgb + m_Qefk[kb](z, lastzt, g + 1, j, t, f) * Dgp);
												
							for (unsigned h = 0, hb = 0; h <= lastei; ++h, ++hb)
							{
								if (h == g) { ++hb; continue; }
								qefk += m_Qefk[kp](i, s, e, i, 0, h) * m_Qefk[kb](z, lastzt, hb, j, t, f);
							}
						}
					}
				}
			}
		}
	}
}


void
EpochBDTProbs::calcPtAndUt(Real t, Real& Pt, Real& ut) const
{
	if (std::abs(m_birthRate - m_deathRate) < 1e-9)
	{
		Real denom = 1.0 + (m_deathRate * t);
		Pt = 1.0 / denom;
		ut = (m_deathRate * t) / denom;
	}
	else if (m_deathRate < 1e-9)
	{
		Pt = 1.0;
		ut = 1.0 - std::exp(-m_birthRate * t);
	}
	else
	{
		Real dbDiff = m_deathRate - m_birthRate;
		Real E = std::exp(dbDiff * t);
		Real denom = m_birthRate - (m_deathRate * E);
		Pt = -dbDiff / denom; 
		ut = (m_birthRate * (1.0 - E)) / denom;
	}
}


string
EpochBDTProbs::getDebugInfo(bool inclExtinc, bool inclOneToOne, bool inclCounted) const
{
	ostringstream oss;
	oss << "# =================================== EPOCHBDTPROBS ===================================" << endl;
	oss << "# Rates: duplication + loss + transfer = "
		<< m_birthRate << " + " << m_deathRate << " + " << m_transferRate << " = " << m_rateSum
		<< endl;
	unsigned cnts = getNoOfTransferCounts();
	if (cnts > 0)
	{
		oss << "# Transfer counts: 0..." << (cnts - 1) << "." << endl;
	}
	else
	{
		oss << "# No transfer counts." << endl;
	}
	
	if (inclExtinc)
	{
		oss << "# Extinction probs Qe:" << endl << m_Qe;
	}
	if (inclOneToOne)
	{
		oss << "# One-to-one probs Qef:" << endl << m_Qef;
	}
	if (inclCounted)
	{
		for (unsigned k = 0; k < m_Qefk.size(); ++k)
		{
			oss << "# Counted transfer one-to-one probs Qef" << k << ":" << endl << m_Qefk[k];
		}
	}
	oss << "# =====================================================================================" << endl;
	return oss.str();
}

} // end namespace beep.

