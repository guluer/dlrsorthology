/* 
 * File:   Tokenizer.hh
 * Author: peter9
 *
 * String tokenizer. Splits a string in several substrings at a user specified
 * character.
 * 
 * Example:
 * 
 * Tokenizer tok(";");
 * tok.setString("Written;by;Peter");
 * while( tok.hasMoreTokens() ){
 *      cout << tok.getNext() << endl;
 * }
 * 
 * This gives the following output:
 *
 * Written
 * by
 * Peter
 *
 * Created on November 26, 2009, 4:46 PM
 */


/**
 * Overview of members:
 * public:
 *  Tokenizer(const string& delimiters);
 *   string getNextToken();
 *   bool hasMoreTokens();
 *   void setString(string &str);
 * private:
 *   void advance();
 *   bool m_more_tokens;
 *   string m_string;
 *   size_t m_offset;
 *   string m_token;
 *   string m_delimiters;
 */



#ifndef _TOKENIZER_HH
#define	_TOKENIZER_HH
#include <stdlib.h>
#include <string>
namespace beep{
    using namespace std;
    class Tokenizer
    {
        public:
            /**
             * Tokenizer
             *
             * Constructor.
             * 
             * @param delimiters A vector of delimiters.
             */
            Tokenizer(const string& delimiters);

            /**
             * getNextToken
             *
             * Returns the next token in the string.
             */
            string getNextToken();

            /**
             * hasMoreToken
             *
             * Returns true if there are more tokens to be found in the string.
             */
            bool hasMoreTokens();

            /**
             * setString
             *
             * Set the string to tokenize.
             * @param str The string to tokenize
             */
            void setString(string &str);

        private:
            /**
             * advance
             *
             * Find next token.
             */
            void advance();

            //Flag for more token
            bool m_more_tokens;

            //String to tokenize
            string m_string;

            //Offset in string for current token
            size_t m_offset;

            //Current token
            string m_token;

            //Delimiters
            string m_delimiters;
    };

};
#endif	/* _TOKENIZER_HH */