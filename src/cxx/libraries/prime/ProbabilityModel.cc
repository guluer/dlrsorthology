#include <sstream>

#include "ProbabilityModel.hh"

// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  //------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //------------------------------------------------------------------
  ProbabilityModel::ProbabilityModel()
  { 
  }
  
  ProbabilityModel::ProbabilityModel(std::string name)
  {
  }

  ProbabilityModel::~ProbabilityModel()
  {};

  ProbabilityModel::ProbabilityModel(const ProbabilityModel& pm)
    :like(pm.like)
  { 
  }

  ProbabilityModel&
  ProbabilityModel::operator=(const ProbabilityModel& pm)
  { 
    if(&pm != this)
      {
	like = pm.like;
      }
    return *this;
  }


  //------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------

  //------------------------------------------------------------------
  //
  // I/O
  //
  //------------------------------------------------------------------
  
  // Always define an ostream operator!
  // This should provide a neat output describing the model and its 
  // current settings for output to the user. It should list current 
  // parameter values by linking to their respective ostream operator
  //------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const ProbabilityModel& pm)
  {
    return o << pm.print();
  }

  std::string 
  ProbabilityModel::print() const
  {
    std::ostringstream oss;
    oss << "   ProbabilityModel, a virtual template class.\n"
	<< "   Subclasses of this class calculates probabilities\n"
	<< "   for data given a model of evolution. It defines a\n"
	<< "   standard interface to classes handling perturbation of\n"
	<< "   the evolutionary model's parameters, e.g., MCMCModel.\n"
	<< "      Parameters of the model are as follows:\n"
	<< "         - None"
	<< std::endl;
    return oss.str();
  };
  
}//end namespace beep

