#ifndef INVMRCA_HH
#define INVMRCA_HH

#include <sstream>

#include "BeepVector.hh"

namespace beep
{
  //! Implements an inverse most recent common ancestor map used 
  //! for orthology output
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  class InvMRCA
  {
    // Typedefs:
    typedef std::pair< std::vector<unsigned>, std::vector<unsigned> > NodeSetPairs;
  public:
    //----------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //----------------------------------------------------------
    InvMRCA(Tree& T_in);
    InvMRCA(const InvMRCA& m);
    virtual ~InvMRCA();

    InvMRCA operator=(InvMRCA m);

    //----------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------
    std::string getStrRep(Node& u, Probability p) const;

    void update();

    //! returns two sets of nodes, left and right, such that for any 
    //! a in left and b in right, u=MRCA(a,b)
    //----------------------------------------------------------------------
    void getSubtreeLeaves(Node* u, std::vector<unsigned>& v) const;

  protected:
    //----------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------
    Tree* T;
    BeepVector<NodeSetPairs> invMRCA;
  };
} //end namespace beep

#endif

