#ifndef MCMCMODEL_HH
#define MCMCMODEL_HH

#include <iostream>
#include <string>
#include <unistd.h>

#include "Beep.hh"
#include "PRNG.hh"

namespace beep
{
class MCMCObject;
class Probability;
class ProbabilityModel;

//----------------------------------------------------------------------
//
//! class MCMCModel
//! Abstract base-class defining an interface for MCMC-implementations
//!
//! The expected use of this interface is as follows (in pseudo code)
//! 1. Set up a model (subclass to MCMCModel) with associated 
//!    ProbabilityModel, LM, for the Likelihood and a nested MCMCModel 
//!    for the prior probabilities (the prior could maybe be empty)
//! 2. Call suggestNewState() and record the probability of the new
//!    state. 
//! 3. If the probability makes you want to keep the state, use
//!    commitNewState()!. 
//! 4. Otherwise, use discardNewState() to "go back" to the old state. 
//! 5. Goto 2, unless you want to quit.
//! 
//! For very simple priors, the prior model may be kept empty (null 
//! pointer). There should then be implemented a default prior model, 
//! which typically is the uniform distribution over a parameter range.
//!
//! In addition, strRepresentation() returns a string encoding the
//! current state. This is probably wanted if you want to do something
//! else than merely study the fit of the data to your model.
//!
//! currentStateProb() returns the probability of the current state,
//! but since suggestNewState() also returns a probability, this
//! function is only really useful on the first state, before any calls
//! to suggestNewState() has been made.
//! Author: Bengt Sennblad, Lars Arvestad, � the MCMC-club, SBC, 
//!all rights reserved
//
//----------------------------------------------------------------------
  class MCMCModel
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    // Constructor should probably best take 
    // 1) a ProbabilityModel, LM, for handling likelihoods and its 
    //    (directly) associated attributes (which are handled and perturbed 
    //    by the present MCMCModel, and
    // 2) a MCMCModel for prior probabilities with all its attributes set 
    //    (these attributes are not handled by the present MCMCModel). 
    // Model 2), the prior, may possibly be missing and a default prior 
    // should then be used

    //
    //----------------------------------------------------------------------
    //MCMCModel(ProbabilityModel LM, 
    //          'AttributeType' attribute,
    //          ..., 
    //          MCMCModel Prior);

    MCMCModel();
    virtual ~MCMCModel();

    MCMCModel& operator=(const MCMCModel& mm);

    //----------------------------------------------------------------------
    //
    // Interface towards the acting MCMC class (e.g., SimplaeMCMC)
    //
    //----------------------------------------------------------------------

    // Perturbs model parameters and calls LM.calculateDataProbability() 
    // to get Likelihood and/or call PM.suggestNewState() to get Prior
    // probability. Typically returns simply the product of the Prior 
    // and the Likelihod
    //----------------------------------------------------------------------
    virtual MCMCObject suggestNewState() = 0; 
    virtual MCMCObject suggestNewState(unsigned x) = 0;

    // Return probability of current state 
    //----------------------------------------------------------------------
    virtual Probability initStateProb() = 0; 
    virtual Probability currentStateProb() = 0; 

    virtual void        commitNewState() = 0;
    virtual void        commitNewState(unsigned x) = 0;
    virtual void        discardNewState() = 0;
    virtual void        discardNewState(unsigned x) = 0;
    // 

    // Represent current state as a string - calls PM.strRepresentation()
    // for states of nested priors
    //----------------------------------------------------------------------
    virtual std::string strRepresentation() const = 0; // For writing to file

    // Print a header corresponding to this string
    //----------------------------------------------------------------------
    virtual std::string strHeader() const = 0;

    // Get acceptance info.
    virtual std::string getAcceptanceInfo() const = 0;

    // Return number of states in prior (probably only used first time!) 
    //----------------------------------------------------------------------
    virtual unsigned   nParams() const = 0;

    //------------------------------------------------------------------
    //
    // Statistics support
    //
    void          registerCommit();
    void          registerDiscard();
    Real          getAcceptanceRatio() const;
    unsigned long iterationNumber()    const;
    PRNG&         getPRNG()            const;

  protected:
    void          incIterationNumber();


  public:
    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------

    // Always define an ostream operator!
    // This should provide a neat output describing the model and its 
    // current settings for output to the user. It should list current 
    // parameter values by li nking to their respective ostream operator
    //------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const MCMCModel& m)
    {
      return o << m.print();
    };

    virtual std::string print() const
    {
      return std::string("MCMCModel, an abstract baseclass.\n"
		    "Subclasses of this class handles parameters, \n"
		    "ProbabilityModels and nested MCMCModels for\n"
		    "mcmc-implementation\n "
		    "It defines a standard interface to classes performing\n"
		    "mcmc, e.g., SimpleMCMC.\n");
    };

  protected:
    //----------------------------------------------------------------------
    // 
    // Attributes
    // The recommended attributes are :
    // 1) Model for the calculation of the Likelihood, i.e., 
    //    P(Data|current state),this is of type ProbabilityModel
    // 2) Parameters relating to the LikelihoodModel, these are the
    //    parameters that are directly perturbed by this model itself
    // 3) Model for the calculation of the prior probability of these 
    //    parameters. This is of type MCMCModel. In this way hierarchical
    //    priors can be handled. Could be an empty pointer (a default prior
    //    should then be implemented).
    //
    //----------------------------------------------------------------------

    // 'ParameterType parameter' // The parameters realting to LM below.
    // ...                       // more parameters 
    // ProbabilityModel& LM      // LikelihoodModel  
    // MCMCModel* PM             // PriorModel can be empty


    //----------------------------------------------------------------------
    //
    // Common attributes.
    //
    // arve believes these will be useful in all occurances.
    //

    //
    // The pseudo-random number generator is used for generating new
    // parameter values and choosing which parameter to perturb.
    //
    static PRNG R;

    //
    // We keep track of the number of MCMC iterations through this variable.
    //
    unsigned long MCMC_iteration;

    //
    // The number of accepted state changes
    //
    unsigned long nAcceptedStates;
  };

}//end namespace beep

#endif
