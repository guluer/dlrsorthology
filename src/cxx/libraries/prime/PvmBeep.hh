#ifndef PVMBEEP_HH
#define PVMBEEP_HH

//!
//!  This file defines variables and constants common to different 
//!  PVM-aware parts of beep.
//!

namespace PvmBeep {
  //! This number is probably too high! 
  //! The communication burden increases severly with more clients.
  const int MAX_N_CLIENTS = 100; 

  //! Conversion factor.
  //! We don't want the clients to compute too large batches. It is
  //! better to break it up in pieces so we don't have to wait for slow
  //! hosts et.c. As a rult of thumb, the batch size for a client is
  //!   n_samples / (n_clients * BatchReductionFactor)
  const int BatchReductionFactor = 10;


  //! The following tokens will tell what type the next message is
  enum MessageTypes {
    // Administrative messages
    NoMessage,			// 0 A possible return code
    AnyMessage,			// 1 For recieving without selection
    WorkOrder,			// 2 String describing the next work order
    PleaseStartWorking,		// 3 End of params: All params are supposed to be there
    YouAreDone,			// 4 Done with sub-project
    
    // General data messages
    Tree,			// 5
    Probability,		// 6
    ProbVector,			// 7 First an integer (size), then floats of log probs.
    String,			// 8
    
    // Specific data messages
    GeneTree,			// 9
    SpeciesTree,		//10 
    G2S,			//11 String-to-string map of leaf names
    BirthRate,			//12
    DeathRate,			//13 
    SubstRate,			//14
    SequenceData,		//15
    BatchSize,			//16 The number of likelihood calculations in client before reporting
    BatchNumber,		//17 An ID to separate computations from different parameter sets
    Orthology,			//18 Orthology predictions
    AnErrorString,		//19 For error reporting, client to master
    
    SendStatstics,		//20 Statistics
    Client2Master,		//21 Compound message
    
    ProcessIsDead		//22 Notifications upon inexpected process death
    
};

} // End of beep namespace
  

#endif
