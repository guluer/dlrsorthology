#ifndef BEEP2BLAS_H
#define BEEP2BLAS_H

// Copyright: Modifications to the original file by Bengt Sennblad <bengt.sennblad@ki.se> 2007-2012

/*
 This file has been somewhat modifed from the original file
 mtl-2.1.2-22/mtl/blas_interface.h
 found in this tar gz package:
 http://osl.iu.edu/download/research/mtl/mtl-2.1.2-22.tar.gz


 The file has this copyright information:                                                                                                                                            
 //
 // Software License for MTL
 // 
 // Copyright (c) 2001-2005 The Trustees of Indiana University. All rights reserved.
 // Copyright (c) 1998-2001 University of Notre Dame. All rights reserved.
 // Authors: Andrew Lumsdaine, Jeremy G. Siek, Lie-Quan Lee
 // 
 // This file is part of the Matrix Template Library
 // 
 // See also license.mtl.txt in the distribution.

 The content of the file
 mtl-2.1.2-22/license.mtl.txt
 and can be read here:

---------------------------------------------------------------------------------

Software License for MTL

Copyright (c) 2001-2005 The Trustees of Indiana University. All rights reserved.
Copyright (c) 1998-2001 University of Notre Dame. All rights reserved.
Authors: Andrew Lumsdaine, Jeremy G. Siek, Lie-Quan Lee

This file is part of the Matrix Template Library

Indiana University has the exclusive rights to license this product under the following license.

Redistribution and use in source and binary forms, with or without modification, are permitted 
provided that the following conditions are met:
        1.      All redistributions of source code must retain the above  copyright notice, 
                the list of authors in the original source code,  this list of conditions and 
                the disclaimer listed in this license;
        2.      All redistributions in binary form must reproduce the above  copyright notice, 
                this list of conditions and the disclaimer listed  in this license in the 
                documentation and/or other materials  provided with the distribution;
        3.      Any documentation included with all redistributions must include  the 
                following acknowledgement:
                "This product includes software developed at the University of Notre Dame and  
                 the Pervasive Technology Labs at Indiana University. For technical information  
                 contact Andrew Lumsdaine at the Pervasive Technology Labs at Indiana University. 
                 For administrative and license questions contact the  Advanced Research and 
                 Technology Institute at 1100 Waterway  Blvd. Indianapolis, Indiana 46202, 
                 phone 317-274-5905, fax  317-274-5902."
                 Alternatively, this acknowledgement may appear in the software itself, and 
                 wherever such third-party acknowledgments normally appear.
        4.      The name "MTL" shall not be used to endorse or promote  products derived from 
                this software without prior written  permission from Indiana University. For 
                written permission, please  contact Indiana University Advanced Research 
                & Technology  Institute.
        5.      Products derived from this software may not be called "MTL", nor may "MTL" 
                appear in their name, without  prior written permission of Indiana University 
                Advanced Research & Technology Institute.

Indiana University provides no reassurances that the source code provided does not infringe the 
patent or any other intellectual property rights of any other entity. Indiana University disclaims 
any liability to any recipient for claims brought by any other entity based on infringement of 
intellectual property rights or otherwise.

LICENSEE UNDERSTANDS THAT SOFTWARE IS PROVIDED "AS IS" FOR WHICH NO WARRANTIES AS TO CAPABILITIES 
OR ACCURACY ARE MADE. INDIANA UNIVERSITY GIVES NO WARRANTIES AND MAKES NO REPRESENTATION THAT 
SOFTWARE IS FREE OF INFRINGEMENT OF THIRD PARTY PATENT, COPYRIGHT, OR OTHER PROPRIETARY RIGHTS. 
INDIANA UNIVERSITY MAKES NO WARRANTIES THAT SOFTWARE IS FREE FROM "BUGS", "VIRUSES", "TROJAN 
HORSES", "TRAP DOORS", "WORMS", OR OTHER HARMFUL CODE. LICENSEE ASSUMES THE ENTIRE RISK AS TO THE 
PERFORMANCE OF SOFTWARE AND/OR ASSOCIATED MATERIALS, AND TO THE PERFORMANCE AND VALIDITY OF 
INFORMATION GENERATED USING SOFTWARE.

-------------------------------------------
 */


namespace beep
{

  /*--------------------------------------------------------
    Basic Linear Algebra Subprograms for C/C++
    Version 1.0
    Matthew E. Gaston
    May 6, 1998
    ----------------------------------------------------------*/

#ifdef __cplusplus
  extern "C" {
#endif

    /*---------------------------------------------------------
      Level 1 BLAS
      -----------------------------------------------------------*/


    /*
    //  Dot product functions: return <- transpose(a) * y
    */
    double ddot_(const int& dim, const double* a, const int& stridex,
		 const double* y, const int& stridey);
    float sdot_(const int& dim, const float* a, const int& stridex,
		 const float* y, const int& stridey);

    /*
      AXPY: y <- alpha* x + y
    */
    // modified by bens
    void daxpy_(const int& dim, const double& alpha , 
		const double* x, const int& stridex, 
		double* y, const int& stridey);
    void saxpy_(const int& dim, const float& alpha , 
		const float* x, const int& stridex, 
		float* y, const int& stridey);

    /*
      Copy: y <- x
    */
    // modified by bens
    void dcopy_(const int& dim, const double* x, const int& stridex, 
		double* y, const int& stridey);
    void scopy_(const int& dim, const float* x, const int& stridex, 
		float* y, const int& stridey);

    /*
      Sum of Absolute Values: return <- sum of elements in a
    */
    // modified by bens
    double dasum_(const int& dim, const double* a, const int& stridex);
    float sasum_(const int& dim, const float* a, const int& stridex);

    /*
      Scale: x <- alpha * x
    */
    // modified by bens
    void dscal_(const int& dim, const double& alpha, 
		double* x, const int& stridex);
    void sscal_(const int& dim, const float& alpha, 
		float* x, const int& stridex);


    /*---------------------------------------------------------
      Level 2 BLAS
      -----------------------------------------------------------*/

    // modified by bens: y <- alpha * opA * x + beta * y
    // transa = : 'N' => opA = A, 'T' => opA = tranpose(A)
    void dgemv_(const char& trans, const int& cols, const int& rows, 
		const double& alpha, const double dA[], const int& lda,
		const double x[], const int& stridex,
		const double& beta, double y[], const int& stridey);
    void sgemv_(const char& trans, const int& cols, const int& rows, 
		const float& alpha, const float dA[], const int& lda,
		const float x[], const int& stridex,
		const float& beta, float y[], const int& stridey);


    /*---------------------------------------------------------
      Level 3 BLAS
      -----------------------------------------------------------*/
    //Matrix multiplications: C<-alpha * opA * B + beta * C
    // transa = : 'N' => opA = A, 'T' => opA = tranpose(A)
    void dgemm_(const char& transA, const char& transB, 
		const int& m, const int& n, const int& k,
		const double& alpha,  const double dA[],  const int& lda,
		const double dB[], const int& ldb, 
		const double& beta, double dC[], const int& ldc);
    void sgemm_(const char& transA, const char& transB, 
		const int& m, const int& n, const int& k,
		const float& alpha,  const float dA[],  const int& lda,
		const float dB[], const int& ldb, 
		const float& beta, float dC[], const int& ldc);



    /*---------------------------------------------------------
      LAPACK
      -----------------------------------------------------------*/

    //eigenvalues
    void dgeev_(const char& jobvl, const char& jobvr,
		const int& n, double da[],
		const int& lda, double dwr[],
		double dwi[], double dvl[],
		const int& ldvl, double dvr[],
		const int& ldvr, double dwork[],
		const int& ldwork, int& info);
    void sgeev_(const char& jobvl, const char& jobvr,
		const int& n, float da[],
		const int& lda, float dwr[],
		float dwi[], float dvl[],
		const int& ldvl, float dvr[],
		const int& ldvr, float dwork[],
		const int& ldwork, int& info);

    void dgetrf_ (const int& cols,
		  const int& rows,
		  double da[],
		  const int& lda,
		  int ipivot[],
		  int& info);
    void sgetrf_ (const int& cols,
		  const int& rows,
		  float da[],
		  const int& lda,
		  int ipivot[],
		  int& info);

    void dgetri_(const int& n, double dA[], const int& ldA, 
		 int dI[], double dW[], const int& ldW, int& info);
    void sgetri_(const int& n, float dA[], const int& ldA, 
		 int dI[], float dW[], const int& ldW, int& info);



#ifdef __cplusplus
  } // extern "C"
#endif
}//end namespace beep
#endif
