#ifndef BEEP_HH
#define BEEP_HH

#include <limits>
#include <map>
#include <string>

#include <libxml/xmlstring.h>
// Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved

//-------------------------------------------------------
//
// This file contains various typedefs and other junk that 
// are often used in the namespace beep
//
//-------------------------------------------------------



#define LENGTH_OF_PRINTF_TMP_BUF 20 // should be able to fit both one float or one double

namespace beep
{

  // typedef that will simplify migration to a type with
  // better precision in beep maths, if we ever decide to do that
  // Please, use this instead of double

#ifdef LA_SINGLE_PRECISION
  // this makes us use a lower precision, the idea was that it might
  // give faster program, but currently iy does not seem so worth while
  typedef float Real; 
  typedef std::numeric_limits<float> Real_limits; 

  #define sprintfReal  sprintfFloat
#define   xmlReadReal xmlReadFloat

#else
  #define sprintfReal  sprintfDouble
#define   xmlReadReal  xmlReadDouble 



  typedef double Real;
  typedef std::numeric_limits<double> Real_limits; 
#endif

  double xmlReadDouble( const xmlChar *str);
  float xmlReadFloat( const xmlChar *str);
  int xmlReadInt( const xmlChar *str);

  void sprintfDouble(char *str, size_t size, double d);
  void sprintfFloat(char *str, size_t size,  float f);

  void capitalize(std::string& s);
  void capitalize(char* s);

  // This function add an indent space and is used in different 
  // classes' ouput/print() function
  //----------------------------------------------------------
  std::string indentString(std::string s, const std::string& indent = "    ");

  // I am tired of the stupid std::pow that can't handle unsigned exponent
  Real pow(const Real& p, const unsigned& n);


  std::string typeid2typestring(std::string t);

}//end namespace beep
#endif
