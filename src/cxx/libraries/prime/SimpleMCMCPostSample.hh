#ifndef SIMPLEMCMCPOSTSAMPLE_HH
#define SIMPLEMCMCPOSTSAMPLE_HH

#include <fstream>

#include "MCMCModel.hh"
#include "PRNG.hh"
#include "Probability.hh"
#include "SimpleMCMC.hh"


namespace beep
{

/**
 * A variant of SimpleMCMC where the samples are drawn from the next committed
 * iteration rather than from the last previous committed iteration. (Note: If
 * the thinning-th iteration itself happens to be committed, then, of course,
 * that is used.)
 * 
 * The motivation for this is that models which perform computationally heavy
 * extra work when sampling may then avoid doing unnecessary work at each
 * non-sampled iteration. (If one pre-samples as in the ordinary SimpleMCMC, one
 * does not know before-hand if the current iteration will be a sample or not,
 * and must therefore always do the extra work.)
 * 
 * Methods getBestState() and getLocalOptimum() are not implemented, and
 * the superclass versions should not be used in their place.
 *  
 * For more info, see SimpleMCMC.
 * 
 * @author Joel Sjöstrand, � the MCMC-club, SBC, all rights reserved

 */
class SimpleMCMCPostSample : public SimpleMCMC
{
	
public:
	
	/**
	 * Constructor.
	 * @param M end-of-chain MCMC model.
	 * @thinning sample every i-th iteration.
	 */
	SimpleMCMCPostSample(MCMCModel &M, unsigned thinning = 1);
	
	/**
	 * Destructor.
	 */
	virtual ~SimpleMCMCPostSample();

	/**
	 * Runs one or several iterations of the Metropolis algorithm.
	 * @param n_iters number of iterations.
	 * @print_factor print every i-th sample (note: not every i-th iteration).
	 */
	virtual void iterate(unsigned n_iters=1, unsigned print_factor = 1);

	/**
	 * Invokes print().
	 */
	friend std::ostream& operator<<(std::ostream &o, const SimpleMCMCPostSample& A);
	
	/**
	 * Returns string iteration of MCMC and intrinsic models.
	 * @param info string.
	 */
	virtual std::string print() const;

protected:

	/**
	 * Prints information before iterating.
	 * @param n_iters number of iterations.
	 */
	virtual void printPreamble(unsigned n_iters) const;
	
	/**
	 * Samples the current (committed) iteration.
	 * @param doPrint true to print state to cerr too.
	 * @param proposal MCMC-object of current state.
	 * @param i iteration.
	 * @param n_iters number of iterations.
	 */
	virtual void sample(bool doPrint, const MCMCObject& proposal,
			unsigned i, unsigned n_iters);
	
};

}//end namespace beep

#endif //SIMPLEMCMCPOSTSAMPLE_HH
