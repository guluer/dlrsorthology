#include "ReconciledTreeTimeMCMC.hh"

#include "LogNormDensity.hh"
#include "MCMCObject.hh"

#include <limits>
#include <cmath>


namespace beep
{
  using namespace std;
  //-------------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //-------------------------------------------------------------------
  ReconciledTreeTimeMCMC::ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G_in, 
						 StrStrMap& gs_in, 
						 BirthDeathProbs& bdp_in, 
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves()-1, suggestRatio),
      ReconciledTreeTimeModel(G_in, gs_in, bdp_in), 
      estimateTimes(true),
      rootFixed(false),
      minEdgeTime(0),
      oldTime(0),
      IdxNode(NULL)
  {
    update();
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
	// uniform and recursive sampling	
	sampleTimes();
      }
  }

  ReconciledTreeTimeMCMC::ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G_in, 
						 StrStrMap& gs_in, 
						 BirthDeathProbs& bdp_in, 
						 string name,
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves() -1, name, suggestRatio),
      ReconciledTreeTimeModel(G_in, gs_in, bdp_in), 
      estimateTimes(true),
      rootFixed(false),
      minEdgeTime(0),
      oldTime(0),
      IdxNode(NULL)
  {
    update();
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
	// uniform and recursive sampling
	sampleTimes();
      }
  }
  //!\todo{Update all constructors to take minEdgeTime and fixRoot  
  ReconciledTreeTimeMCMC::ReconciledTreeTimeMCMC(MCMCModel& prior, Tree& G_in, 
						 StrStrMap& gs_in, 
						 BirthDeathProbs& bdp_in, 
						 Real minEdgeTime,
						 bool fixRoot,
						 string name,
						 Real suggestRatio)
    : StdMCMCModel(prior, G_in.getNumberOfLeaves() -1, name, suggestRatio),
      ReconciledTreeTimeModel(G_in, gs_in, bdp_in), 
      estimateTimes(true),
      rootFixed(fixRoot),
      minEdgeTime(minEdgeTime),
      oldTime(0),
      IdxNode(NULL)
  {
    update();
    if(G->hasTimes() == false)
      {
	G->setTimes(*new RealVector(*G));
	// uniform and recursive sampling
	unsigned i = 0;
	while(i < 1000)
	  {
	    try
	      {
		cerr << "generating times !  ";
		sampleTimes();
		break;
	      }
	    catch(AnError& e)
	      {
		cerr << "Caught "
		     << e.what()
		     << " trying again!  ";
	      }
	    i++;
	    if(i >= 1000)
	      throw AnError("ReconciledTreeTimeMCMC:"
			    "Failed to generate times", 1);
	  }
	cerr << "i = " << i << endl;
      }
  }
  
  ReconciledTreeTimeMCMC::~ReconciledTreeTimeMCMC()
  {}
  
  ReconciledTreeTimeMCMC::
  ReconciledTreeTimeMCMC(const ReconciledTreeTimeMCMC& rtm)
    : StdMCMCModel(rtm),
      ReconciledTreeTimeModel(rtm), 
      estimateTimes(rtm.estimateTimes),
      oldTime(rtm.oldTime),
      IdxNode(rtm.IdxNode)
  {}

  ReconciledTreeTimeMCMC& 
  ReconciledTreeTimeMCMC::operator=(const ReconciledTreeTimeMCMC& rtm)
  {
    if(&rtm != this)
      {
	StdMCMCModel::operator=(rtm);
	ReconciledTreeTimeModel::operator=(rtm);
	estimateTimes = rtm.estimateTimes;
	oldTime = rtm.oldTime;
	IdxNode = rtm.IdxNode;
      }
    return *this;
  }

  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  void
  ReconciledTreeTimeMCMC::sampleTimes()
  {
    if(rootFixed)
      {
	Node& root = *G->getRootNode();
	G->setTime(root, S->rootToLeafTime() + bdp->getTopTime());
	sampleTimes(root.getLeftChild(), 
		    S->rootToLeafTime() + bdp->getTopTime()); 
	sampleTimes(root.getRightChild(), 
		    S->rootToLeafTime() + bdp->getTopTime()); 
      }
    else
      {
	sampleTimes(G->getRootNode(), S->rootToLeafTime()+bdp->getTopTime()); 
      }
  }

  void 
  ReconciledTreeTimeMCMC::fixTimes()
  {
    n_params = 0;
    updateParamIdx();    // tell StdMCMCModel
    return;
  }


  // Interface inherited from StdMCMCModel
  MCMCObject 
  ReconciledTreeTimeMCMC::suggestOwnState()
  {
    // Randomly select a valid node to perturb
    do
      {
	unsigned n = R.genrand_modulo(G->getNumberOfNodes());
	IdxNode = G->getNode(n);
      }
    while(IdxNode->isLeaf() ||                              // leaf times fixed
	  (IdxNode->isRoot() && S->rootToLeafTime() == 0) );// BD interval  
    oldTime = G->getTime(*IdxNode);
    //     if(IdxNode->isRoot())
    //       {
    // 	throw AnError("Perturbing species root time\n", 1);
    //       }
#ifdef PERTURBED_NODE
    if(!G->perturbedNode())                 // Flag perturbation in tree
      G->perturbedNode(IdxNode); 
    else
      G->perturbedNode(G->getRootNode()); 
#endif
    // Turn off notifications just to be sure.
    bool notifStat = G->setPertNotificationStatus(false);

    MCMCObject MOb =  perturbTime(*IdxNode);  // delegate actual perturb

    // Notify listeners.
    G->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::PERTURBATION);
    G->notifyPertObservers(&pe);

    return MOb;
  }

  void 
  ReconciledTreeTimeMCMC::commitOwnState()
  {
  }

  void 
  ReconciledTreeTimeMCMC::discardOwnState()
  {
    // Turn off notifications just to be sure.
    bool notifStat = G->setPertNotificationStatus(false);

    G->setTime(*IdxNode, oldTime);

    // Notify listeners.
    G->setPertNotificationStatus(notifStat);
    PerturbationEvent pe(PerturbationEvent::RESTORATION);
    G->notifyPertObservers(&pe);
    
#ifdef PERTURBED_NODE
    // We need to fix this!!!
    if(G->perturbedNode())                 // Flag perturbation in tree
      G->perturbedNode(G->getRootNode()); 
    else
      G->perturbedNode(IdxNode); 
#endif
  }

  string 
  ReconciledTreeTimeMCMC::ownStrRep() const
  {
    ostringstream oss;
    if(estimateTimes)
      {
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node* u = G->getNode(i);
	    if(u->isLeaf() == false)
	      {
		oss << u->getNodeTime() << ";\t";
	      }
	  }
      }
    return oss.str();
  }

  string 
  ReconciledTreeTimeMCMC::ownHeader() const
  {
    ostringstream oss;
    if(estimateTimes)
      {
	for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
	  {
	    Node* u = G->getNode(i);
	    if(u->isLeaf() == false)
	      {
		oss << G->getName() <<".nodeTime[" << i << "](float);\t";
	      }
	  }
      }
    return oss.str();
  }
  
  Probability 
  ReconciledTreeTimeMCMC::updateDataProbability()
  {
    update();
    return ReconciledTreeTimeModel::calculateDataProbability();
  }

//   void 
//   ReconciledTreeTimeMCMC::updateToExternalPerturb(const Tree& newT)
//   {
//     // Turn off notifications just to be sure.
//     bool notifStat = G->setPertNotificationStatus(false);

//     MCMCObject MOb =  perturbTime(*IdxNode);  // delegate actual perturb

//     // Notify listeners.
//     G->setPertNotificationStatus(notifStat);
//     PerturbationEvent pe(PerturbationEvent::PERTURBATION);
//     G->notifyPertObservers(&pe);
//   }
  //-------------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------------
  ostream& 
  operator<<(ostream &o, const ReconciledTreeTimeMCMC& A)
  {
    return o << "ReconciledTreeTimeMCMC "<< A.print();
  }

  string 
  ReconciledTreeTimeMCMC::print() const
  {
    return "ReconciledTreeTimeMCMC " + ReconciledTreeTimeModel::print()
      +  StdMCMCModel::print();

  }
  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------------
 
  // The function perturbs the node time at 'currentNode' and returns the
  // proposal ratio. The use of changeTime() and changeNodeTime
  // the proposal ratio q_before / q_after is taken from Martin Linder
  // Pre: u.isLeaf() == false 
  //---------------------------------------------------------------------------
  MCMCObject
  ReconciledTreeTimeMCMC::perturbTime(Node& u)
  {
    // Check preconditions
    assert(u.isLeaf() == false);

    MCMCObject MOb(1.0,1.0);
    Real suggestion_variance = G->rootToLeafTime() / 100.0;
    Real pSpec = 0.2;

    Real oldValue = G->getTime(u);   
    Real maxT = S->rootToLeafTime() + bdp->getTopTime();
    if(u.isRoot() == false)
      { 
	maxT = G->getTime(*u.getParent());
      }
    Real minT = max(G->getTime(*u.getLeftChild()),
		    G->getTime(*u.getRightChild()));
    Real sigmaT = S->getTime(*sigma[u]);
    
    // Check sanity
    assert(oldValue < maxT);
    assert(minT < oldValue);
    assert(sigmaT <= oldValue);

    if(oldValue == sigmaT) // i.e., this is currently a speciation
      {
	// 	oldValue *= (1.0 + 1e-10);
	minT = sigmaT;
      }
    else if(minT < sigmaT) // this is allowed to be a speciation
      {
	if(R.genrand_real1() < pSpec) // Make it a speciation
	  {
	    G->setTime(u,sigmaT);
	    MOb.stateProb = updateDataProbability();

	    // Now compute the proposal ratio q(s|s')/q(s'|s)
	    // q(s'|s)=pSpec, q(s|s') = LogN(s) where LogN has mean s'
	    static LogNormDensity nd(1.0,1.0);
	    nd.setEmbeddedParameters(std::log(sigmaT + Real_limits::min())
				     - 0.5 * suggestion_variance,
				     suggestion_variance);
	    MOb.propRatio = nd(oldValue) / pSpec;
	    return MOb;
	  }
	else
	  {
	    minT = sigmaT;
	  }
      }
    if(maxT -minT <= 2 * minEdgeTime)
      {
	MOb.stateProb = 0;
        return MOb;
      }
    Real newTime = 0;
    unsigned i = 1;
    do
      {
	// Use utility function in StdMCMCModel to get new time
	newTime = perturbLogNormal(oldValue, suggestion_variance, 
				   minT, maxT, MOb.propRatio, 0);//2);
	i++;
	if(i > 10000)
	  throw AnError("ReconciledTreeTimeMCMC:\n"
			"  Failed to change time of node " 
			+ u.getNumber(), 1);
      }
    while(maxT - newTime < minEdgeTime || newTime -minT < minEdgeTime);
    
    
    assert(minT <= newTime);
    assert(sigmaT <= newTime);
    assert(newTime < maxT);
    G->setTime(u, newTime);

    MOb.stateProb = updateDataProbability();
    
    return MOb;
  }

  void
  ReconciledTreeTimeMCMC::sampleTimes(Node* u, Real maxT)
  {
    assert(u != 0);
    assert(maxT > 0);
    if(u->isLeaf())
      {
	if(maxT < 2 * minEdgeTime)
	  {
	    ostringstream oss;
	    oss << "Edge time < min EdgeTime ("
		<< minEdgeTime 
		<< ") generated";
	    throw AnError(oss.str());
	  }
	G->setTime(*u, 0);
      }
    else //if(u->isRoot() == false)
      {
	Real t;
	Real minT = S->getTime(*sigma[u]);
	if(maxT - minT < 2 * minEdgeTime)
	  {
	    ostringstream oss;
	    oss << "Edge time < min EdgeTime ("
		<< minEdgeTime 
		<< ") generated";
	    throw AnError(oss.str());
	  }
	if(minT == 0)
	  {
	    t = maxT * R.genrand_real3();
	  }
	else
	  {
	    Real tmp = R.genrand_real2();
	    if(tmp == 0)
	      t = minT;
	    else
	      t = minT + (maxT - minT) * tmp;
	  }
	if(maxT - t < minEdgeTime)
	  t = maxT - minEdgeTime;
	assert(t >= minT);
	assert(t < maxT);

	// Pass recursion on
	sampleTimes(u->getLeftChild(), t);
	sampleTimes(u->getRightChild(), t);

	RealVector& times = G->getTimes();
	times[u] = t;
      }
    return;
  }

}// end namespace beep
