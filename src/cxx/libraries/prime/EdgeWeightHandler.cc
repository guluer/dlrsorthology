#include "EdgeWeightHandler.hh"
#include "AnError.hh"

#include <sstream>

namespace beep
{
  using namespace std;
  //-------------------------------------------------------------------
  //
  // Class EdgeWeightHandler
  //
  //-------------------------------------------------------------------

  //-------------------------------------------------------------------
  //
  // Constructor, Destructor, Assignment
  //
  //-------------------------------------------------------------------
  EdgeWeightHandler::EdgeWeightHandler(EdgeWeightModel& erm)
    : T(&erm.getTree()),
      lengths(0)
  {
    init(erm);
  }

  EdgeWeightHandler::EdgeWeightHandler(const EdgeWeightHandler& ewh)
    : T(ewh.T),
      lengths(ewh.lengths)
  {}

  EdgeWeightHandler::~EdgeWeightHandler()
  {}

  EdgeWeightHandler& 
  EdgeWeightHandler::operator=(const EdgeWeightHandler& ewh)
  {
    if(this != &ewh)
      {
	T = ewh.T;
	lengths = ewh.lengths;
      }
    return *this;
  }

  //-------------------------------------------------------------------
  //
  // interface
  //
  //-------------------------------------------------------------------
//   const EdgeRateModel&  
//   EdgeWeightHandler::getRateModel()
//   {
//     return *edgeRates;
//   }

  const Tree&  
  EdgeWeightHandler::getTree()
  {
    return *T;
  }

  // Returns the length of incomimg edge to n 
  //! \todo{change name to getLength and fix calls in SubstModel}
  Real  
  EdgeWeightHandler::getWeight(const Node& n) const
  {
    if(lengths->size() == 1)
      {
	return (*lengths)[0u];
      }
    else
      {
	return (*lengths)[n];
      }
  }
  
  //! \todo{change name to getLength and fix calls in SubstModel}
  Real  
  EdgeWeightHandler::getWeight(const Node* n) const 
  { 
    assert(n != 0);
    return getWeight(*n);
  }

  Real  
  EdgeWeightHandler::operator[](const Node& n) const
  {  
    return getWeight(n);
  }

  Real  
  EdgeWeightHandler::operator[](const Node* n) const
  {  
    assert (n != 0);
    return getWeight(*n); 
  }

  //! Updates edgelengths. Base class has nothing to update.
  void
  EdgeWeightHandler::update()
  {}

  //-------------------------------------------------------------------
  // IO
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const EdgeWeightHandler& ewh)
  {
    return o << ewh.print()
      ;
  }
  std::string  
  EdgeWeightHandler::print() const
  {
    std::ostringstream oss;
    oss
      << "EdgeWeights reads edgeWeights directly from the tree\n" 
      << T->getName() 
      << "\n";
    return oss.str();
  }

  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------------
  void
  EdgeWeightHandler::init(EdgeWeightModel& erm)
  {
    if(T->hasLengths())
      {
	if(&T->getLengths() != &erm.getWeightVector())
	  {
	    throw AnError("EdgeWeightHandler::EdgeWeightHandler\n"
			  "conflict: T->lengths already exists",1);
	  }
	lengths = &T->getLengths();
      }
    else
      {
	T->setLengths(erm.getWeightVector());
	lengths = &T->getLengths();
      }
  }
    

  //-------------------------------------------------------------------
  //
  // Class EdgeTimeRateHandler
  //
  //-------------------------------------------------------------------

  //-------------------------------------------------------------------
  //
  // Constructor, Destructor, Assignment
  //
  //-------------------------------------------------------------------
  EdgeTimeRateHandler::EdgeTimeRateHandler(EdgeRateModel& erm)
    : EdgeWeightHandler(erm)
  {
    init(erm),
    update();
  }
    
  EdgeTimeRateHandler::EdgeTimeRateHandler(const EdgeTimeRateHandler& erh)
    : EdgeWeightHandler(erh)
  {
    // Is this an incomplete copy constructor?? fix!!!
  }

  EdgeTimeRateHandler::~EdgeTimeRateHandler()
  {}

  EdgeTimeRateHandler& 
  EdgeTimeRateHandler::operator=(const EdgeTimeRateHandler& ewh)
  {
    if(this != & ewh)
      {
	EdgeWeightHandler::operator=(ewh);
      }
    return *this;
  }

  //! Updates edgelengths. Checks if either rates or times of any edge is 
  //! perturbed and update the corresponding edge weight
  // TODO: Two alternatives: 
  // Either, update always update all edgelengths (possibly only if any 
  // perturbation is indicated), or 
  // we could skip update() and edgelengths, and let operator[x] always  
  // return the multiplication edgeRate[x] * x.getTime(). 
  void 
  EdgeTimeRateHandler::update()
  {
#ifdef PERTURBED_NODE
    Node* t = T->perturbedNode();
    if(t != 0)
      {
	if(t == T->getRootNode()) // update all
	  {
#endif
	    for(unsigned i = 0; i < T->getNumberOfNodes(); i++)
	      {
		Node* n = T->getNode(i);
		if(!n->isRoot())
		  {
		    (*lengths)[n] = T->getRate(*n) * T->getEdgeTime(*n);
		  }
	      }
#ifdef PERTURBED_NODE
	  }	      
	else
	  {
	    if(!t->isLeaf())
	      {

		Node* l = t->getLeftChild();
		Node* r = t->getRightChild();
		(*lengths)[l] = T->getRate(*l) * T->getEdgeTime(*l);
		(*lengths)[r] = T->getRate(*r) * T->getEdgeTime(*r);
	      }
	    (*lengths)[t] = T->getRate(*t) * T->getEdgeTime(*t);
	  }
      }	    
#endif
    return;
  }

  //-------------------------------------------------------------------
  // IO
  //-------------------------------------------------------------------
  std::string 
  EdgeTimeRateHandler::print() const
  {
    std::ostringstream oss;
    oss
      << "Edgelengths is modeled as the edgeTimes * rates from Tree "
      << T->getName() 
      << "\n";
    return oss.str();

  }


  //-------------------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------------------
  void
  EdgeTimeRateHandler::init(EdgeRateModel& erm)
  {
    if(T->hasRates())
      {
	if(&T->getRates() != &erm.getRateVector())
	  {
	    throw AnError("EdgeTimeRateHandler::EdgeTimeRateHandler\n"
			  "conflict: T->rates already exists",1);
	  }
      }
    else
      {
	T->setRates(erm.getRateVector());
      }
    T->setLengths(*new RealVector(*T,0));
    lengths = &T->getLengths(); 
  }

}//end namespace beep

