#ifndef EDGEDISCBDMCMC_HH
#define EDGEDISCBDMCMC_HH

#include <string>
#include <iostream>

#include "EdgeDiscBDProbs.hh"
#include "StdMCMCModel.hh"

namespace beep
{

  /**
   * MCMC model for altering birth and death rates for an "edge discretized tree"
   * (see documentation for EdgeDiscBDProbs for more info).
   * 
   * The birth and the death rates are perturbed with equal chance according
   * to a log normal distribution on the suggestion variance.
   */
  class EdgeDiscBDMCMC : public StdMCMCModel
  {

  public:

	/**
	 * Constructor.
	 * @param prior the prior in the MCMC-chain.
	 * @param BDProbs the discretized birth-death probabilities, encapsulating
	 * the birth and death rates.
	 * @param suggestRatio the suggestion ratio.
	 */
	EdgeDiscBDMCMC(MCMCModel& prior, EdgeDiscBDProbs* BDProbs, const Real& suggestRatio);
	
    /**
     * Destructor.
     */
    virtual ~EdgeDiscBDMCMC();

    //! Access to member 
    virtual EdgeDiscBDProbs& getModel()
    {
      return *m_BDProbs;
    }
    /**
     * Fixes birth and death rates to their current values.
     */
    void fixRates();

    /**
     * Sets the scale factor for the suggestion variance.
     * @param multiplier a linear scale factor.
     */
    void multiplySuggestionVariance(Real multiplier);

    /**
     * Perturbs the birth and death rates with equal probability.
     * Caches values before perturbing.
     * @return the MCMC object associated with the state.
     */
    MCMCObject suggestOwnState();
	
    /**
     * Commits the state. Resets the perturbation status of 
     * the object with BD parameters.
     */
    void commitOwnState();
	
    /**
     * Restores previous birth and death rates.
     */
    void discardOwnState();
	
    /**
     * Returns a string with parameter values in accordance with MCMC model guidelines.
     * @return the current parameter values in a string.
     */
    std::string ownStrRep() const;
	
    /**
     * Returns a string with parameter names in accordance with MCMC model guidelines.
     * @return the parameter names in a string.
     */
    std::string ownHeader() const;
	

    //! Used in parallelization, spec. MpiMultiGSR
    void updateToExternalPerturb(Real newLambda, Real newMu);

    /**
     * No data probability of its own, returns 1.0.
     * @return 1.
     */
    Probability updateDataProbability()
    {
      return Probability(1.0);
    }
    
    /**
     * Returns a string with acceptance related info.
     * @return a string with acceptance info.
     */
     virtual std::string getAcceptanceInfo() const;

    /**
     * Friend helper for printing the object. Uses member method print() for the task.
     * @param o the ostream reference.
     * @param A the object to print.
     * @return the appended ostream reference.
     */
    friend std::ostream& operator<<(std::ostream& o, const EdgeDiscBDMCMC& A);
	
    /**
     * Returns an information string for the object.
     * @return the info string.
     */
    std::string print() const;

  private:

  public:

  private:
	
	/** The birth and death rates (including precalculated values). */
	EdgeDiscBDProbs* m_BDProbs;
	
    /** Set to true if the birth and death rate should be kept fixed. */
    bool m_fixRates;

    unsigned m_which;                             /*< Flag for curr. perturbed param. */
    std::pair<unsigned,unsigned> m_bAccPropCnt;   /*< Birth acc. and tot. prop. count. */
    std::pair<unsigned,unsigned> m_dAccPropCnt;   /*< Death acc. and tot. prop. count. */
  };

}//end namespace beep

#endif /*EDGEDISCBDMCMC_HH*/

