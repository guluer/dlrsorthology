#include <cassert>

#include "AnError.hh"
#include "EdgeDiscPtMaps.hh"
#include "EdgeDiscTree.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscPtKeyIterator.hh"

namespace beep
{

  using namespace std;


  ////////////////////////// 1-D MAP /////////////////////////

  template<typename T>
  EdgeDiscPtMap<T>::EdgeDiscPtMap(Tree& S) :
    m_DS(NULL),
    m_vals(S),
    m_cache(S),
    m_cacheIsValid(false)
  {
  }
	

  template<typename T>
  EdgeDiscPtMap<T>::EdgeDiscPtMap(EdgeDiscTree* DS, const T& defaultVal) :
    m_DS(DS),
    m_vals(DS->getTree()),
    m_cache(DS->getTree()),
    m_cacheIsValid(false)
  {
    rediscretize(defaultVal);
  }


  template<typename T>
  EdgeDiscPtMap<T>::EdgeDiscPtMap(const EdgeDiscPtMap<T>& ptMap) :
    m_DS(ptMap.m_DS),
    m_vals(ptMap.m_vals),
    m_cache(ptMap.m_cache),
    m_cacheIsValid(ptMap.m_cacheIsValid)
  {
  }


  template<typename T>
  EdgeDiscPtMap<T>::~EdgeDiscPtMap()
  {
  }


  template<typename T>
  EdgeDiscPtMap<T>& 
  EdgeDiscPtMap<T>::operator=(const EdgeDiscPtMap<T>& ptMap)
  {
    if (this != &ptMap)
      {
	m_DS = ptMap.m_DS;
	m_vals = ptMap.m_vals;
	m_cache = ptMap.m_cache;
	m_cacheIsValid = ptMap.m_cacheIsValid;
      }
    return *this;
  }


  template<typename T>
  Tree& 
  EdgeDiscPtMap<T>::getTree() const
  {
    return (m_DS->getTree());
  }


  template<typename T>
  T& EdgeDiscPtMap<T>::getTopmost()
  {
    return m_vals[m_DS->getRootNode()].back();
  }


  template<typename T>
  const T& EdgeDiscPtMap<T>::getTopmost() const
  {
    return m_vals[m_DS->getRootNode()].back();
  }


  template<typename T>
  EdgeDiscretizer::Point EdgeDiscPtMap<T>::getTopmostPt() const
  {
    return EdgeDiscretizer::Point(m_DS->getRootNode(),
				  m_vals[m_DS->getRootNode()].size() - 1);
  }


  template<typename T>
  EdgeDiscPtKeyIterator<T> EdgeDiscPtMap<T>::beginKey()
  {
    return EdgeDiscPtKeyIterator<T>(this);
  }


  template<typename T>
  EdgeDiscPtKeyIterator<T> EdgeDiscPtMap<T>::endKey()
  {
    return EdgeDiscPtKeyIterator<T>(this, true);
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> EdgeDiscPtMap<T>::begin(const Node* node)
  {
    return EdgeDiscPtMapIterator<T>(this, 
				    EdgeDiscretizer::Point(node, 0));
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> 
  EdgeDiscPtMap<T>::begin(const EdgeDiscretizer::Point& pt)
  {
    return EdgeDiscPtMapIterator<T>(this, EdgeDiscretizer::Point(pt));
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> EdgeDiscPtMap<T>::end()
  {
    return EdgeDiscPtMapIterator<T>(this, getTopmostPt());
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> EdgeDiscPtMap<T>::endPlus()
  {
    return EdgeDiscPtMapIterator<T>(this, EdgeDiscretizer::Point(NULL, 0));
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> EdgeDiscPtMap<T>::end(const Node* node)
  {
    return EdgeDiscPtMapIterator<T>(this, node->isRoot() ?
				    getTopmostPt() : EdgeDiscretizer::Point(node->getParent(), 0));
  }


  template<typename T>
  EdgeDiscPtMapIterator<T> EdgeDiscPtMap<T>::endPlus(const Node* node)
  {
    return (node->isRoot() ? endPlus() :
	    EdgeDiscPtMapIterator<T>(this, EdgeDiscretizer::Point(node->getParent(), 0)));
  }


  template<typename T>
  void EdgeDiscPtMap<T>::rediscretize(const T& defaultVal)
  {
    Tree& S = m_DS->getTree();
    for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
      {
	m_vals[*it].assign(m_DS->getNoOfPts(*it), defaultVal);
      }
  }


  template<typename T>
  void EdgeDiscPtMap<T>::reset(const T& defaultVal)
  {
    for (unsigned i = 0; i < m_vals.size(); ++i)
      {
	m_vals[i].assign(m_vals[i].size(), defaultVal);
      }
  }


  template<typename T>
  void EdgeDiscPtMap<T>::cache()
  {
    m_cache = m_vals;
    m_cacheIsValid = true;
  }


  template<typename T>
  void EdgeDiscPtMap<T>::cachePath(const Node* node)
  {
    while (node != NULL)
      {
	m_cache[node] = m_vals[node];
	node = node->getParent();
      }
    m_cacheIsValid = true;
  }


  template<typename T>
  void EdgeDiscPtMap<T>::restoreCache()
  {
    if (m_cacheIsValid)
      {
	m_vals = m_cache;
	m_cacheIsValid = false;
      }
  }


  template<typename T>
  void EdgeDiscPtMap<T>::restoreCachePath(const Node* node)
  {
    if (m_cacheIsValid)
      {
	while (node != NULL)
	  {
	    m_vals[node] = m_cache[node];
	    node = node->getParent();
	  }
	m_cacheIsValid = false;
      }
  }


  template<typename T>
  void EdgeDiscPtMap<T>::invalidateCache()
  {
    m_cacheIsValid = false;
  }


  template<typename T>
  string EdgeDiscPtMap<T>::print() const
  {
    ostringstream oss;
    oss << "# (node,ptIndex): value" << endl;
    Tree& S = m_DS->getTree();
    for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
      {
	oss << "# ";
	unsigned nn = (*it)->getNumber();
	const vector<T>& v = m_vals[*it];
	for (unsigned j = 0; j < v.size(); ++j)
	  {
	    oss << '(' << nn << ',' << j << "): " << v[j] << '\t';
	  }
	oss << endl;
      }
    return oss.str();
  }


  // Specialization printing non-log values.
  template<>
  string EdgeDiscPtMap<Probability>::print() const
  {
    ostringstream oss;
    oss << "# (node,ptIndex): value" << endl;
    Tree& S = m_DS->getTree();
    for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
      {
	oss << "# ";
	unsigned nn = (*it)->getNumber();
	const vector<Probability>& v = m_vals[*it];
	for (unsigned j = 0; j < v.size(); ++j)
	  {
	    oss << '(' << nn << ',' << j << "): " << v[j].val() << '\t';
	  }
	oss << endl;
      }
    return oss.str();
  }

  template<typename T>
  T EdgeDiscPtMap<T>::normalizeToProbabilities(const Node* node)
  {
      ostringstream oss;
      oss << "Before Normalization" << endl;
      
      //Node *old_node = node;
      //cout << "node is"
      // first get the sum
      T sum = 0;
      while (node != NULL)
      {		
	const vector<T>& v = m_vals[node];        
	for (unsigned j = 0; j < v.size(); ++j)
	  {
            sum += v[j];
            cout << v[j] << "\t";
	    //oss << '(' << nn << ',' << j << "): " << v[j] << '\t';
	  }
	oss << endl;
	node = node->getParent();
      }
      
      if(sum > 0.0)
        cout << "sum is " << sum << endl;    
      
      while (node != NULL)
      {		
	const vector<T>& v = m_vals[node];        
	for (unsigned j = 0; j < v.size(); ++j)
	  {
            T val = v[j]/sum;
            cout << val << "\t";
	    //oss << '(' << nn << ',' << j << "): " << v[j] << '\t';
	  }
	oss << endl;
	node = node->getParent();
      }
      
      cout << oss.str();
      return sum;
  }

  template<typename T>
  string EdgeDiscPtMap<T>::printPath(const Node* node) const
  {
    ostringstream oss;
    oss << "# (node,ptIndex): value" << endl;
    while (node != NULL)
      {
	oss << "# ";
	unsigned nn = node->getNumber();
	const vector<T>& v = m_vals[node];
	for (unsigned j = 0; j < v.size(); ++j)
	  {
	    oss << '(' << nn << ',' << j << "): " << v[j] << '\t';
	  }
	oss << endl;
	node = node->getParent();
      }
    return oss.str();
  }


  // Specialization printing non-log values.
  template<>
  string EdgeDiscPtMap<Probability>::printPath(const Node* node) const
  {
    ostringstream oss;
    oss << "# (node,ptIndex): value" << endl;
    while (node != NULL)
      {
	oss << "# ";
	unsigned nn = node->getNumber();
	const vector<Probability>& v = m_vals[node];
	for (unsigned j = 0; j < v.size(); ++j)
	  {
	    oss << '(' << nn << ',' << j << "): " << v[j].val() << '\t';
	  }
	oss << endl;
	node = node->getParent();
      }
    return oss.str();
  }


  ////////////////////////// 2-D MAP /////////////////////////


  template<typename T>
  EdgeDiscPtPtMap<T>::EdgeDiscPtPtMap(EdgeDiscTree& DS, 
				      const T& defaultVal,
				      bool subtreeOnly) :
    m_DS(DS),
    m_subtreeOnly(subtreeOnly),
    m_noOfPts(DS.getTree()),
    m_vals(DS.getTree().getNumberOfNodes(), 
	   DS.getTree().getNumberOfNodes()),
    m_cache(DS.getTree().getNumberOfNodes(), 
	    DS.getTree().getNumberOfNodes()),
    m_cacheIsValid(false)
  {
    rediscretize(defaultVal);
  }


  template<typename T>
  EdgeDiscPtPtMap<T>::EdgeDiscPtPtMap(const EdgeDiscPtPtMap<T>& ptPtMap)
    : m_DS(ptPtMap.m_DS),
      m_subtreeOnly(ptPtMap.m_subtreeOnly),
      m_noOfPts(ptPtMap.m_noOfPts),
      m_vals(ptPtMap.m_vals),
      m_cache(ptPtMap.m_cache),
      m_cacheIsValid(ptPtMap.m_cacheIsValid)
  {
  }


  template<typename T>
  EdgeDiscPtPtMap<T>::~EdgeDiscPtPtMap()
  {
  }


  template<typename T>
  EdgeDiscPtPtMap<T>& 
  EdgeDiscPtPtMap<T>::operator=(const EdgeDiscPtPtMap<T>& ptPtMap)
  {
    if (this != &ptPtMap)
      {
	m_DS = ptPtMap.m_DS;
	m_subtreeOnly = ptPtMap.m_subtreeOnly;
	m_noOfPts = ptPtMap.m_noOfPts;
	m_vals = ptPtMap.m_vals;
	m_cache = ptPtMap.m_cache;
	m_cacheIsValid = ptPtMap.m_cacheIsValid;
      }
    return *this;
  }


  template<typename T>
  void EdgeDiscPtPtMap<T>::rediscretize(const T& defaultVal)
  {
    Tree& S = m_DS.getTree();
    Tree::const_iterator it, jt;

    // Retrieve number of points per edge.
    for (it = S.begin(); it != S.end(); ++it)
      {
	m_noOfPts[*it] = m_DS.getNoOfPts(*it);
      }
	
    if(m_subtreeOnly)
      {
	// For each "to" edge, create point-to-point values 
	// to each edge on its root path.
	for (jt = S.begin(); jt != S.end(); ++jt)
	  {
	    unsigned jID = (*jt)->getNumber();
	    unsigned jSz = m_noOfPts[*jt];
	    const Node* i = (*jt);
	    while (i != NULL)
	      {
		unsigned iID = i->getNumber();
		unsigned iSz = m_noOfPts[i];
		m_vals(iID, jID).assign(iSz * jSz, defaultVal);
		i = i->getParent();
	      }
	  }
      }
    else
      {
	// Fill all possible point-to-point values.
	for (it = S.begin(); it != S.end(); ++it)
	  {
	    unsigned iID = (*it)->getNumber();
	    unsigned iSz = m_noOfPts[*it];
	    for (jt = S.begin(); jt != S.end(); ++jt)
	      {
		unsigned jID = (*jt)->getNumber();
		unsigned jSz = m_noOfPts[*jt];
		m_vals(iID, jID).assign(iSz * jSz, defaultVal);
	      }
	  }
      }
  }


  template<typename T>
  void EdgeDiscPtPtMap<T>::reset(const T& defaultVal)
  {
    for (unsigned i = 0; i < m_vals.nrows(); ++i)
      {
	for (unsigned j = 0; j < m_vals.ncols(); ++j)
	  {
	    m_vals(i,j).assign(m_vals(i,j).size(), defaultVal);
	  }
      }
  }


  template<typename T>
  void EdgeDiscPtPtMap<T>::cache()
  {
    m_cache = m_vals;
    m_cacheIsValid = true;
  }


  template<typename T>
  void EdgeDiscPtPtMap<T>::restoreCache()
  {
    if (m_cacheIsValid)
      {
	m_vals = m_cache;
	m_cacheIsValid = false;
      }
  }


  template<typename T>
  void EdgeDiscPtPtMap<T>::invalidateCache()
  {
    m_cacheIsValid = false;
  }


  ///////////////////////// Templates ////////////////////////

  template class EdgeDiscPtMap<Real>;
  template class EdgeDiscPtMap<Probability>;
  template class EdgeDiscPtPtMap<Real>;
  template class EdgeDiscPtPtMap<Probability>;


} // end namespace beep.

