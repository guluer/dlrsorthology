#ifndef LENGTHRATEMODEL_HH
#define LENGTHRATEMODEL_HH

#include "VarRateModel.hh"

namespace beep
{
  // forward declaration
  class CongruentGuestTreeTimeMCMC;

  class LengthRateModel : public EdgeWeightModel
  {
  public:
    //--------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //--------------------------------------------------------
    LengthRateModel(Density2P& rateProb_in, Tree& T_in,
    		EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    ~LengthRateModel();

    //--------------------------------------------------------
    //
    // Interface
    //
    //--------------------------------------------------------
    // In addition to the standard ProbabilityModel interface, we have:
    void setTimeListener(CongruentGuestTreeTimeMCMC& m);
    // Access parameters
    //------------------------------------------------------------------
    //! Returns the tree on which edge weights are modeled
    const Tree& getTree() const;

    //! number of weights that are modeled
    unsigned nWeights() const;

    //! Returns reference to vector of weight for (incoming edge to) Node u. 
    RealVector& getWeightVector() const;

    //! Returns weight for (incoming edge to) Node u. 
    Real getWeight(const Node& u) const;

    //! set the weight for (incoming edge to) Node u to 'weight'.
    void setWeight(const Real& weight, const Node& u);

    //! set the weight for (incoming edge to) Node u to 'weight'.
    void setWeightVector(RealVector& weights);

    //! get legitimate range of weights
    void getRange(Real& low, Real& high);

    //! get the behaviour of how root edges are handled.
    virtual RootWeightPerturbation getRootWeightPerturbation() const
    {
    	return perturbedRootEdges;
    }

    //! set the behaviour of how root edges are handled.
    virtual void setRootWeightPerturbation(RootWeightPerturbation rwp)
    {
    	perturbedRootEdges = rwp;
    }

    void update();

    Probability calculateDataProbability();

    //-------------------------------------------------------------------
    //
    // IO
    //
    //-------------------------------------------------------------------
    virtual std::string print() const;

  protected:
    //--------------------------------------------------------
    //
    // Attribute
    //
    //--------------------------------------------------------
    iidRateModel rateModel;;
    RealVector* weights;

    // Spezial for CongruentTreeTimeMMCC used in waladen
    CongruentGuestTreeTimeMCMC* cttm;
    EdgeWeightModel::RootWeightPerturbation perturbedRootEdges; //! Guides perturbation of edges around root.
  };

}//end namespace beep

#endif 
