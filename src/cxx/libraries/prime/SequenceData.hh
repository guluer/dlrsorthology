#ifndef SEQUENCE_HH
#define SEQUENCE_HH

#include <vector>
#include <string>
#include <algorithm>
#include <cassert>
#include <iostream>
#include <map>

#include "SequenceType.hh"

//----------------------------------------------------------------------
//
// Class SequenceData
// This handles sequence data matrix of a specified sequence type (see 
// class SequenceType). This matrix has rows corresponding to genes and 
// columns corresponding to aligned positions of these genes. 
// the class provides functions for accesing the data and 
// associated attributes, and also for converting the data into a hash
// where a pattern is the key and the number of occurrences of this 
// pattern is the value.User defined partitions of data is prepared for,
// but are currently not used.
//
//----------------------------------------------------------------------
namespace beep
{
  // Global typedefs in namespace beep
  //! vector of columns in data  
  typedef std::vector<unsigned>              PositionVec; 

  //! maps state pattern to columns (in a partition of data) that displays it 
  typedef std::map<std::string, PositionVec> PatternMap;  
  typedef std::vector<std::pair<unsigned, unsigned> > PatternVec;  

  //! vector of partitions of data
  typedef std::vector<PatternVec>            PartitionVec;

class SequenceData
{
      //! \todo Maybe a name for SequenceData as well?

public:
  //----------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //----------------------------------------------------------------------

  // Constructor
  //----------------------------------------------------------------------
  SequenceData(const beep::SequenceType& dt);

  // Copy Constructor
  //----------------------------------------------------------------------
  SequenceData(const SequenceData& D);

  // Destructor 
  //----------------------------------------------------------------------
  ~SequenceData();

  // Assignment operator
  //----------------------------------------------------------------------
  SequenceData& operator=(const SequenceData& D);


public:
  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------


  //----------------------------------------------------------------------
  //
  // Accessors
  //
  //----------------------------------------------------------------------

  // Access sequence type
  //----------------------------------------------------------------------
  beep::SequenceType getSequenceType() const;

  // Get the number of positions
  //----------------------------------------------------------------------
  unsigned getNumberOfPositions() const;

  // Get the number of sequences
  //----------------------------------------------------------------------
  unsigned getNumberOfSequences() const;

  // Size of largest sequence name. Weird function, but used in PVM code!
  //----------------------------------------------------------------------
  unsigned getNameMaxSize() const;

  // Access a sequence position
  //----------------------------------------------------------------------
  unsigned operator()(const std::string& name, unsigned pos) const;
  const std::string operator[](const std::string& name) const;
  const beep::LA_Vector leafLike(const std::string& name, unsigned pos) const;


  // General sequence access
  //----------------------------------------------------------------------
  std::string getSequenceName(unsigned idx) const;

  //!Returns a vector with sequence names
  //----------------------------------------------------------------------
  std::vector<std::string> getAllSequenceNames() const
  {
    std::vector<std::string> ret;
    for(std::map<std::string, std::string>::const_iterator i = data.begin();
	i != data.end(); i++)
      {
	ret.push_back(i->first);
      }
    return ret;
  };


  // Sort data into a table with character patterns present in the 
  // interval [first,last], ordered according to nameOrder, and their 
  // associated number of occurrences in the interval. If no interval
  // is given all data are sorted
  // Should probably be completely replaced by getSortedData(), below, 
  // but might be interesting when outputting statistic of data.
  //----------------------------------------------------------------------
  PatternMap sortData() const;
  PatternMap sortData(const std::string& partition) const;

  // SubstitutionModle actually never uses PatternMap as a map. Thus, 
  // it is better to return a PatternVec instead.
  // TODO: These maybe should return const?
  PatternVec getSortedData() const;
  PatternVec getSortedData(const std::string& partition) const;
  
  //----------------------------------------------------------------------
  //
  // Manipulators
  //
  //----------------------------------------------------------------------

  void changeType(beep::SequenceType newtype);

  // Adding data read from file
  //----------------------------------------------------------------------
  void addData(const std::string& name, const std::string& sequence);

    //----------------------------------------------------------------------
    //
    // I/O
    //
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, const SequenceData& D);
    std::string print() const;
    
    std::map<char, double> getBaseFrequencies();

    //----------------------------------------------------------------------
    //
    // Implementation
    //
    //----------------------------------------------------------------------

  public:
    // Helper function of operator<<
    //----------------------------------------------------------------------
    const std::string data4os() const;
    const std::string data4fasta() const;

  
  private:
    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
  beep::SequenceType seqType;
 

    std::map<std::string, std::string> data;
};

}//end namespace beep
#endif
    
  
