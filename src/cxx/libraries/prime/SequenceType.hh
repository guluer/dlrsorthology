#ifndef SEQUENCETYPE_HH
#define SEQUENCETYPE_HH

#include <climits>   
#include <vector>
#include <map>
#include <string>

#include "Probability.hh"
#include "LA_Vector.hh"

namespace beep
{
/**
 *  A base class for the different sequence types.
 */


#define MAXTYPELEN 20

  class SequenceType
  {
  public: //TODO: redundant/bens
    static SequenceType getType(const std::string &s);
    
  protected:
    SequenceType(const std::string &alpha, const std::string &amb);
    
  public:
    SequenceType(const SequenceType &dt);
    SequenceType();
    virtual ~SequenceType(){};
    
    SequenceType& operator=(const SequenceType &dt);
    bool operator==(const SequenceType& dt) const;   

    static SequenceType getSequenceType(std::string s);

    std::string getType() const;
    
    unsigned int alphabetSize() const;
    
    unsigned int operator()(const char& state) const;

    virtual const beep::LA_Vector& getLeafLike(const char& state) const;

    unsigned int char2uint(const char c) const;
    char     uint2char(const unsigned i) const;
    std::vector<unsigned> stringTranslate(const std::string &s) const;


    Probability typeLikelihood(const std::string &s) const;

    bool checkValidity(const std::vector<unsigned int> &v) const;

    friend std::ostream& operator<<(std::ostream& os, const SequenceType& dt);
    std::string print() const;

  protected:
    /// A string describing the type.
    std::string type;

    /// All states in the alphabet represented as chars.
    std::string alphabet;

    /// The alternative alphabet.
    std::string ambiguityAlphabet;

    /// The probabilities for alphabet states for any given state. 
    std::vector<beep::LA_Vector> leafLike;

    // The following two probs are used for guessing sequence type
    /// The probability of observing an alphabet character in a seq.
    Probability alphProb;	
    /// Same, but for ambiguity symbols.
    Probability ambiguityProb;	
  };

/**
 * Class DNA is used for DNA sequences. The alphabet has 4 states:
 * a=0, c=1, g=2, t=3.
 * The alternative alphabet has 13 states and represents
 * different combination of the 4 basic nucleotides.
 * E.g. m is a or c with 50% on each.
 * m(ac)=01, r(ag)=02, w(at)=03, s(cg)=12, y(ct)=13, k(gt)=23, 
 * v(acg)=012, h(act)=013, d(agt)=023, b(cgt)=123, 
 * x(acgt)=n(acgt)=-(acgt)=0123
 * See http://www.hgu.mrc.ac.uk/Softdata/Misc/ambcode.htm for more info.
 */
  class DNA : public SequenceType
  {
  public:
    DNA();
  };    

/**
 * Class AminoAcid is used for AA sequences. 
 * The alphabet has 20 states that represents the different aminoacids:
 * a=1, r=2, n=3, d=4, c=5, q=6, e=7, g=8, h=9, i=10, l=11, k=12, 
 * m=13, f=14, p=15, s=16, t=17, w=18, y=19, v=20.
 * The alternative alphabet has 4 states and represents
 * different combination of the 20 basic AA.
 * b=21, z=22, x=23, -=24
 * b=dn z=eq x=acdefghiklmnpqrstvwy.
 * See http://www.hgu.mrc.ac.uk/Softdata/Misc/ambcode.htm for more info.
 */
  class AminoAcid : public SequenceType
  {
  public:
    AminoAcid();
  };

/**
 * Class Codon is used for Codon sequences. 
 * The alphabet has 61 states, all three letter combination of
 * the nucleotides except for the most common stop codons (4^3-3=61).
 * Instead of using three letter symbols we invent a char representation.
 */
  class Codon : public SequenceType
  {
  public:
    Codon();

    unsigned int str2uint(const std::string& codon_str);
    std::string uint2str(const unsigned int& codon);
  };    

  extern DNA myDNA;
  extern AminoAcid myAminoAcid;
  extern Codon myCodon;
}     // End of namespace beep

#endif
    
