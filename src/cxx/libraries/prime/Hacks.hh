#ifndef HACKS_HH
#define HACKS_HH

/// The purpose of this file is to gather strange code
///
/// Our first contestant to the Coding Annoyances Price is "How to
/// detect if a stream is connected to a TTY".  There used to be a
/// simple way doing this using regular library code, where you simply
/// accessed a stream's file descriptor and then used the C stdlib
/// function isatty. However, support for accessing the file
/// descriptor went away. Fortunately, not only we need this type of
/// functionality, so I could eventually find some code on the Net for
/// this. More or less. /arve
///
/// The function 'fileno' returns a file descriptor.
//! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved

#include <iosfwd>

  template <typename charT, typename traits>
  int prime_fileno(const std::basic_ios<charT, traits>& stream);
  
  
  /// Produce nicely formatted time strings
  ///
  /// Input is a number of seconds and output is a string, such as 2d4h
  /// (meaning "2 days and 4 hours), 4h3m (for "four hours and 3
  /// minutes"), or 3m1s (meaning "3 minutes and 1 second").
  std::string readableTime(unsigned seconds);
  
  //! Convert a number into a string
  //!
  //! This is something I've been needed in so many cases of debugging
  //! that I decided to make it "more systematic". /arve
  std::string int2str(int x);
#endif
