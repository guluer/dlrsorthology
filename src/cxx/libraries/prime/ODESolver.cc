#include <cmath>
#include <iostream>

#include "ODESolver.hh"

namespace beep
{

using namespace std;

ODESolver::ODESolver(Real rtol, Real atol, bool hasSolout, bool doDense) :
	m_hasSolout(hasSolout),
	m_doDense(doDense && hasSolout),
	m_cont(),
	m_rtol(rtol),
	m_atol(atol),
	m_nmax(100000),
	m_nstiff(1000),
	m_uround(2.3e-16),
	m_safe(0.9),
	m_fac1(0.2),
	m_fac2(10),
	m_beta(0.04),
	m_hmax(0.0),
	m_nfcn(0),
	m_nstep(0),
	m_naccpt(0),
	m_nrejct(0),
	m_n(0),
	m_xold(0.0),
	m_hout(0.0),
	m_y1(),
	m_k1(),
	m_k2(),
	m_k3(),
	m_k4(),
	m_k5(),
	m_k6(),
	m_ysti()
{
}

		
ODESolver::~ODESolver()
{
}
		

void ODESolver::getStatistics(unsigned& nfcn, unsigned& nstep, unsigned& naccpt, unsigned& nrejct) const
{
	nfcn = m_nfcn;
	nstep = m_nstep;
	naccpt = m_naccpt;
	nrejct = m_nrejct;
}


Real ODESolver::getRelativeTolerance() const
{
	return m_rtol;
}


Real ODESolver::getAbsoluteTolerance() const
{
	return m_atol;
}


void ODESolver::setTolerance(Real rtol, Real atol)
{
	m_rtol = rtol;
	m_atol = atol;
}


bool ODESolver::getHasSolout() const
{
	return m_hasSolout;
}
	

void ODESolver::setHasSolout(bool hasSolout)
{
	m_hasSolout = hasSolout;
	if (!hasSolout) { m_doDense = false; }
}


bool ODESolver::getDenseOuput() const
{
	return m_doDense;
}


void ODESolver::setDenseOutput(bool doDense)
{
	m_doDense = (doDense && m_hasSolout);
}


unsigned ODESolver::getMaxNoOfSteps() const
{
	return m_nmax;
}


void ODESolver::setMaxNoOfSteps(unsigned maxNoOfSteps)
{
	if (maxNoOfSteps <= 0)
		throw AnError("Must specify maximum no of steps greater than 0.");
	m_nmax = maxNoOfSteps;
}


unsigned ODESolver::getStiffDetectFactor() const
{
	return (m_nstiff == numeric_limits<unsigned>::max() ? 0 : m_nstiff);
}


void ODESolver::setStiffDetectFactor(unsigned factor)
{
	// Value 0 means no detection at all.
	m_nstiff = (factor == 0) ? numeric_limits<unsigned>::max() : factor;
}


Real ODESolver::getRoundingUnit() const
{
	return m_uround;
}


void ODESolver::setRoundingUnit(Real roundingUnit)
{
	if (roundingUnit <= 1e-35 || roundingUnit >= 1)
		throw AnError("Must have rounding unit in range (1e-35, 1).");
	m_uround = roundingUnit;
}


Real ODESolver::getSafetyFactor() const
{
	return m_safe;
}


void ODESolver::setSafetyFactor(Real factor)
{
	if (m_safe <= 1e-4 || m_safe >= 1)
		throw AnError("Must have safety factor in range (1e-4, 1).");
	m_safe = factor;
}


void ODESolver::getStepSizeParams(Real& param1, Real& param2) const
{
	param1 = m_fac1;
	param2 = m_fac2;
}


void ODESolver::setStepSizeParams(Real param1, Real param2)
{
	m_fac1 = param1;
	m_fac2 = param2;
}


Real ODESolver::getStepSizeStabilizationParam() const
{
	return m_beta;
}


void ODESolver::setStepSizeStabilizationParam(Real beta)
{
	if (beta < 0 || beta > 0.2)
		throw AnError("Step size stabilizer must be in range [0, 0.2]");
	m_beta = beta;
}


Real ODESolver::getMaxStepSize() const
{
	return m_hmax;
}


void ODESolver::setMaxStepSize(Real maxStepSize)
{
	m_hmax = std::abs(maxStepSize);
}


void ODESolver::initialize()
{
	// Init. working vectors.
	m_y1.assign(m_n, 0.0);
	m_k1.assign(m_n, 0.0);
	m_k2.assign(m_n, 0.0);
	m_k3.assign(m_n, 0.0);
	m_k4.assign(m_n, 0.0);
	m_k5.assign(m_n, 0.0);
	m_k6.assign(m_n, 0.0);
	m_ysti.assign(m_n, 0.0);
	m_cont.assign(m_n * 5, 0.0);  // All 5 coefficient rows concatenated.

	// Reset statistics counters.
	m_nfcn = 0;
	m_nstep = 0;
	m_naccpt = 0;
	m_nrejct = 0;
}


ODESolver::SolverResult ODESolver::dopri5(
		Real& x,
		Real xend,
		vector<Real>& y,
		Real& h,
		const vector<Real>* rtol,
		const vector<Real>* atol)
{
	m_n = y.size();
	initialize();

	if (rtol != NULL && rtol->size() < m_n) { throw AnError("Too small rel. tol. vector."); }
	if (atol != NULL && atol->size() < m_n) { throw AnError("Too small abs. tol. vector."); }
	
	// Calculate some more convenient settings values based on members.
	Real hmax = (m_hmax == 0.0) ? std::abs(xend - x) : m_hmax;
	Real expo1 = 0.2 - m_beta * 0.75;
	Real facc1 = 1.0 / m_fac1;
	Real facc2 = 1.0 / m_fac2;
	int posneg = (x <= xend) ? 1 : -1;

	// Err from previous step.
	Real facold = 1e-4;

	// Suggested new step size. Recomputed if rejected.
	Real hnew;

	// Number of hypothetically stiff samples detected within small proximity.
	unsigned iasti = 0;

	// Number of consecutive samples deemed non-stiff after detecting a stiff one.
	unsigned nonsti = 0;

	// Step size parameter during stiffness detection.
	Real hlamb = 0.0;
	
	// Last iteration flag.
	bool last = false;

	// First evaluation at initial x.       
	fcn(x, y, m_k1);
	m_nfcn++;

	// An initial step size set to 0 implies that an auto estimation should be made.
	if (h == 0.0)
	{
		h = hinit(x, y, posneg, hmax, rtol, atol);
		m_nfcn++;
	}

	// Rejection of step.
	bool reject = false;
	
	m_xold = x;
	m_hout = h;
	
	// Return-code from external solution provider.
	ExtSolResult irtrn;

	// Call external solution provider for first time if such exists.
	if (m_hasSolout)
	{
		irtrn = solout(m_naccpt + 1, m_xold, x, y);
		if (irtrn == INTERRUPT_SOLVER)
		{
			return SUCCESSFUL_INTERRUPTED;
		}
	}
	else
	{
		irtrn = NOT_INVOKED;
	}

	// BASIC INTEGRATION STEP.
	while (true)
	{
		// When unable to retrieve answer within max allowed steps.
		if (m_nstep > m_nmax) { return INSUFFICIENT_NMAX; }

		// When step size is too small to continue.
		if (0.1 * std::abs(h) <= std::abs(x) * m_uround) { return TOO_SMALL_GEN_STEP_SIZE; }

		// If we will reach xend, we're at the last iteration.
		if ((x + 1.01 * h - xend) * posneg > 0.0)
		{
			h = xend - x;
			last = true;
		}

		m_nstep++;

		// The first 6 stages.
		if (irtrn == SOLUTION_CHANGED)
		{
			// Recompute, since solution externally changed.
			fcn(x, y, m_k1);
		}
		for (unsigned i = 0; i < m_n; ++i)
		{ m_y1[i] = y[i] + h * A21 * m_k1[i]; }
		fcn(x + C2 * h, m_y1, m_k2);
		for (unsigned i = 0; i < m_n; ++i)
		{ m_y1[i] = y[i] + h * (A31 * m_k1[i] + A32 * m_k2[i]); }
		fcn(x + C3 * h, m_y1, m_k3);
		for (unsigned i = 0; i < m_n; ++i)
		{ m_y1[i] = y[i] + h * (A41 * m_k1[i] + A42 * m_k2[i] + A43 * m_k3[i]); }
		fcn(x + C4 * h, m_y1, m_k4);
		for (unsigned i = 0; i < m_n; ++i)
		{ m_y1[i] = y[i] + h * (A51 * m_k1[i] + A52 * m_k2[i] + A53 * m_k3[i] + A54 * m_k4[i]); }
		fcn(x + C5 * h, m_y1, m_k5);
		for (unsigned i = 0; i < m_n; ++i)
		{ m_ysti[i] = y[i] + h * (A61 * m_k1[i] + A62 * m_k2[i] + A63 * m_k3[i] + A64 * m_k4[i] + A65 * m_k5[i]); }
		Real xph = x + h;
		fcn(xph, m_ysti, m_k6);
		for (unsigned i = 0; i < m_n; ++i)
		{ m_y1[i] = y[i] + h * (A71 * m_k1[i] + A73 * m_k3[i] + A74 * m_k4[i] + A75 * m_k5[i] + A76 * m_k6[i]); }
		fcn(xph, m_y1, m_k2);

		// Store dense output coefficients if specified.
		if (m_doDense)
		{
			for (unsigned i=0; i<m_n; ++i)
			{
				m_cont[4 * m_n + i] = h * (D1 * m_k1[i] + D3 * m_k3[i] + D4 * m_k4[i]
				       + D5 * m_k5[i] + D6 * m_k6[i] + D7 * m_k2[i]);
			}
		}
		
		for (unsigned i = 0; i < m_n; ++i)
		{
			m_k4[i] = (E1 * m_k1[i] + E3 * m_k3[i] + E4 * m_k4[i] + E5 * m_k5[i] + E6 * m_k6[i] + E7 * m_k2[i]) * h;
		}
		m_nfcn += 6;

		// Error estimation. 
		Real err = 0.0;
		if (rtol == NULL)
		{
			// Scalar tolerance.
			for (unsigned i = 0; i < m_n; ++i)
			{
				Real sk = m_atol + m_rtol * std::max(std::abs(y[i]), std::abs(m_y1[i]));
				err += (m_k4[i] / sk) * (m_k4[i] / sk);
			}
		}
		else
		{
			// Per-component tolerances.
			for (unsigned i = 0; i < m_n; ++i)
			{
				Real sk = (*atol)[i] + (*rtol)[i] * std::max(std::abs(y[i]), std::abs(m_y1[i]));
				err += (m_k4[i] / sk) * (m_k4[i] / sk);
			}
		}
		err = sqrt(err / m_n);

		// Computation of hnew.
		Real fac11 = std::pow(err, expo1);
		Real fac = fac11 / std::pow(facold, m_beta);      // Lund-stabilization.
		fac = std::max(facc2, std::min(facc1, fac / m_safe));  // We require fac1 <= hnew/h <= fac2.
		hnew = h / fac;
		
		if (err <= 1.0)
		{
			// STEP IS ACCEPTED.
			facold = std::max(err, 1e-4);
			m_naccpt++;

			// Stiffness detection test. Sample every m_nstiff-th sample, or every time
			// after a sample was positive.
			if (m_naccpt % m_nstiff == 0 || iasti > 0)
			{
				Real stnum = 0.0;
				Real stden = 0.0;
				for (unsigned i = 0; i < m_n; ++i)
				{
					stnum += (m_k2[i] - m_k6[i]) * (m_k2[i] - m_k6[i]);
					stden += (m_y1[i] - m_ysti[i]) * (m_y1[i] - m_ysti[i]); 
				}
				if (stden > 0.0) { hlamb = h * sqrt(stnum / stden); }
				if (hlamb > 3.25)
				{
					// Reset conditional non-stiff counter. Increase stiff counter. If too
					// many stiff samples in a row (interspersed with a few non-stiff), abort.
					nonsti = 0;
					iasti++;
					if (iasti == 15) { return PROBABLY_STIFF; }
				}
				else
				{
					// Increase conditional non-stiff counter, if enough, reset stiff counter.
					nonsti++;
					if (nonsti == 6) { iasti = 0; }
				}
			}

			// Calculate dense output.
			if (m_doDense)
			{
				for (unsigned i = 0; i < m_n; ++i)
				{
					Real yd0 = y[i];
					Real ydiff = m_y1[i] - yd0;
					Real bspl = h * m_k1[i] - ydiff;
					m_cont[i] = y[i];
					m_cont[m_n + i] = ydiff;
					m_cont[2 * m_n + i] = bspl;
					m_cont[3 * m_n + i] = -h * m_k2[i] + ydiff - bspl;
				}
			}
			
			// Move on to next step.
			for (unsigned i = 0; i < m_n; ++i)
			{
				m_k1[i] = m_k2[i];
				y[i] = m_y1[i];
			}
			m_xold = x;
			m_hout = h;
			x = xph;
			if (m_hasSolout)
			{
				irtrn = solout(m_naccpt + 1, m_xold, x, y);
				if (irtrn == INTERRUPT_SOLVER)
				{
					return SUCCESSFUL_INTERRUPTED;
				}
			}
			if (last)
			{
				// Normal exit.
				h = hnew;
				return SUCCESSFUL;
			}
			if (std::abs(hnew) > hmax) { hnew = posneg * hmax; }
			if (reject) { hnew = posneg * std::min(std::abs(hnew), std::abs(h)); }
			reject = false;
		}
		else
		{
			// STEP IS REJECTED. Recompute hnew.
			hnew = h / std::min(facc1, fac11 / m_safe);
			reject = true;
			if (m_naccpt >= 1) { m_nrejct++; }
			last = false;
		}
		h = hnew;
	}
}


Real ODESolver::hinit(
		const Real& x,
		const vector<Real>& y,
		const int& posneg,
		const Real& hmax,
		const std::vector<Real>* rtol,
		const std::vector<Real>* atol)
{
	vector<Real>& f0 = m_k1;
	vector<Real>& f1 = m_k2;
	vector<Real>& y1 = m_k3;

	Real dnf = 0.0;
	Real dny = 0.0;

	if (rtol == NULL)
	{
		// Scalar tolerance.
		for (unsigned i = 0; i < m_n; ++i)
		{
			Real sk = m_atol + m_rtol * std::abs(y[i]);
			dnf += (f0[i] / sk) * (f0[i] / sk);
			dny += (y[i] / sk) * (y[i] / sk);
		}
	}
	else
	{
		// Per-component tolerances.
		for (unsigned i = 0; i < m_n; ++i)
		{
			Real sk = (*atol)[i] + (*rtol)[i] * std::abs(y[i]);
			dnf += (f0[i] / sk) * (f0[i] / sk);
			dny += (y[i] / sk) * (y[i] / sk);
		}
	}

	Real h = (dnf <= 1e-10 || dny <= 1e-10) ? 1e-6 : sqrt(dny / dnf) * 0.01;
	h = posneg * std::min(h, hmax);

	// Perform an exlicit Euler step.
	for (unsigned i = 0; i < m_n; ++i)
	{
		y1[i] = y[i] + h * f0[i];
	}
	fcn(x + h, y1, f1);

	// Estimate the second derivative of the solution.
	// As earlier, use scalar or per-component tolerances.
	Real der2 = 0.0;
	if (rtol == NULL)
	{
		for (unsigned i = 0; i < m_n; ++i)
		{
			Real sk = m_atol + m_rtol * std::abs(y[i]);
			der2 += ((f1[i] - f0[i]) / sk) * ((f1[i] - f0[i]) / sk);
		}
	}
	else
	{
		for (unsigned i = 0; i < m_n; ++i)
		{
			Real sk = (*atol)[i] + (*rtol)[i] * std::abs(y[i]);
			der2 += ((f1[i] - f0[i]) / sk) * ((f1[i] - f0[i]) / sk);
		}
	}
	der2 = sqrt(der2) / h;

	// Step size is computed such that 
	// h^5 * max(norm(f0), norm(der2)) == 0.01.
	Real der12 = std::max(std::abs(der2), sqrt(dnf));
	Real h1 = (der12 <= 1e-15) ?
			std::max(1e-6, std::abs(h) * 1.0e-3) : std::pow(0.01 / der12, 1.0 / IORD);
	h = posneg * std::min(std::min(100 * std::abs(h), h1), hmax);
	return h;
} 


Real ODESolver::contd5(unsigned i, Real xCont)
{
	Real theta = (xCont - m_xold) / m_hout;
	Real theta1 = 1.0 - theta;
	return (m_cont[i] + theta * (m_cont[m_n + i] + theta1 * (m_cont[2 * m_n + i] +
			theta * (m_cont[3 * m_n + i] + theta1 * m_cont[4 * m_n + i]))));
}


void ODESolver::contd5(vector<Real>& yIpl, Real xCont)
{
	yIpl.resize(m_n);
	Real theta = (xCont - m_xold) / m_hout;
	Real theta1 = 1.0 - theta;
	for (unsigned i = 0; i < m_n; ++i)
	{
		yIpl[i] = m_cont[i] + theta * (m_cont[m_n + i] + theta1 * (m_cont[2 * m_n + i] +
				theta * (m_cont[3 * m_n + i] + theta1 * m_cont[4 * m_n + i])));
	}
}

} // end namespace beep
