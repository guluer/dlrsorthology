#ifndef BEEPOPTION_HH
#define BEEPOPTION_HH

#include <algorithm>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include "AnError.hh"

namespace beep
{
namespace option
{

using namespace std;

/** Enumerates valid beep option types. */
enum BeepOptionType
{
	EMPTY,            /**> Empty option. */ 
	BOOL,             /**> Single bool option. */
	UNSIGNED,         /**> Single unsigned int option. */
	INT,              /**> Single int option. */
	DOUBLE,           /**> Single double option. */
	STRING,           /**> Single string option. */
	INT_X2,           /**> Twin ints option. */
	DOUBLE_X2,        /**> Twin doubles option. */
	DOUBLE_X3,        /**> Triple doubles option. */
	STRING_ALT,       /**> Option for string in set of predefined alternatives. */
	USER_SUBST_MODEL  /**> Highly customized option for user defined subst. model. */
};

/** Defines letter case for BeepOptions containing strings. */
enum StringCase
{
	MIXED,		/**> Both upper case and lower case. */
	UPPER,		/**> Upper case only. */
	LOWER		/**> Lower case only. */
};

// Forward declarations.
struct BeepOption;
struct BoolOption;
struct UnsignedOption;
struct IntOption;
struct DoubleOption;
struct StringOption;
struct IntX2Option;
struct DoubleX2Option;
struct DoubleX3Option;
struct StringAltOption;
struct UserSubstModelOption;

/**
 * Convenience container class for handling options in PrIME programs.
 * 
 * Handles options of various types, where each option has an ID (the one 
 * used as user input to the program, e.g. "Abc" for option "-Abc") and a
 * value. Moreover, one may set a customized error message other than the
 * default one if desired, and also inspect whether the option has been
 * touched (i.e. parsed) or not.
 * 
 * Options are retrieved from the map using string names, but may also be 
 * retrieved using the IDs.
 * 
 * Typical usage:
 * \code
 *    ...
 *    using namespace beep::option;
 *    ...
 *    BeepOptionMap bom;
 *    bom.addIntOption("Opt1", "A", 123, ...);    // 123 is default val.
 *    bom.addBoolOption("Opt2", "B", true, ...);
 *    bom.addUnsignedOption("Opt3", "Cm", 555, ...);
 *    bom.addStringOption("Opt4", "Cn", "Hooray!", ...);
 *    bom.addStringAltOption("Opt5", "D", "Knave", "Knight,Knave,Spy", ...);
 *    ...
 *    try
 *    {
 *       int argIndex = 1;
 *       if (!bom.parseOptions(argIndex, argv, argc))
 *       {
 *          cerr << "These are the options:" << endl;
 *          cerr << bom;      // Prints help messages of stored options.
 *          exit(0);
 *       }
 *    }
 *    catch (AnError& e)
 *    {
 *       showErrorMessage(e);
 *       exit(1);
 *    }
 *    ...
 *    unsigned myCmVal = bom.getUnsigned("Opt3");   // Retrieves option.
 *    ...
 * \endcode
 * 
 * As can be seen, the class includes e.g. a method for parsing user input 
 * and an overloaded ostream operator '<<' to allow for the map to be printed.
 * The output in this case is all of the help messages of the options 
 * concatenated in the order in which they were inserted.
 */
class BeepOptionMap
{
public:
	
	/**
	 * Constructor.
	 * @param helpIds special IDs reserved for help options in
	 *  comma-separated list, see parseOptions() for more info.
	 * @param unknownOptionErrMsg string thrown when an unknown 
	 * option is parsed.
	 */
	BeepOptionMap(string helpIds = "h,u,?", 
		      string unknownOptionErrMsg = "Unknown option");
	
	/**
	 * Destructor.
	 */
	virtual ~BeepOptionMap();
	
	/**
	 * Parses options from user input as long as correct options 
	 * starting with character '-' can be found. If an unknown option 
	 * is encountered, 'AnError' stating this is thrown.
	 * Similarly, if a parsing error is encountered, 'AnError' with 
	 * the error message of that particular option is thrown. If one 
	 * of the reserved help option IDs are encountered, the parsing 
	 * is aborted and false is returned, otherwise true is returned.
	 * @param argIndex the current index in the user input array. 
	 * Incremented with one for each parsed option.
	 * @param argc the size of the user input array.
	 * @param argv the user input array.
	 * @return false if an help option ID was encountered, otherwise true.
	 */
	bool parseOptions(int& argIndex, int argc, char **argv);
	
	/**
	 * Returns the option with the specified name.
	 * @param name the option name.
	 * @return the option.
	 */
	BeepOption* getOption(string name);
	
	/**
	 * Returns the option with the specified ID.
	 * @param id the option ID.
	 * @return the option.
	 */
	BeepOption* getOptionById(string id);
	
	/**
	 * Can be used to find out whether an option has been parsed or not.
	 * Just returns the corresponding flag stored in the option itself.
	 * @param name the option's name.
	 * @return true if the option was read when parsing user input.
	 */
	bool hasBeenParsed(string name);
	
	/**
	 * Adds a bool option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value (is inverted if option 
	 * is found in user input).
	 * @param helpMsg the option's help message.
	 */
	void addBoolOption(string name, string id, bool defaultVal,
			   string helpMsg);
	
	/**
	 * Returns the value of a bool option.
	 * @param name the option name.
	 * @return the option value.
	 */
	bool getBool(string name);
	
	/**
	 * Returns a bool option.
	 * @param name the option name.
	 * @return the option.
	 */
	BoolOption* getBoolOption(string name);
	
	/**
	 * Adds an unsigned option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addUnsignedOption(string name, string id, 
			       unsigned defaultVal, string helpMsg);
	
	/**
	 * Returns the value of an unsigned option.
	 * @param name the option name.
	 * @return the option value.
	 */
	unsigned getUnsigned(string name);
	
	/**
	 * Returns an unsigned option.
	 * @param name the option name.
	 * @return the option.
	 */
	UnsignedOption* getUnsignedOption(string name);
	
	/**
	 * Adds an int option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addIntOption(string name, string id, int defaultVal, 
			  string helpMsg);
	
	/**
	 * Returns the value of an int option.
	 * @param name the option name.
	 * @return the option value.
	 */
	int getInt(string name);
	
	/**
	 * Returns an int option.
	 * @param name the option name.
	 * @return the option.
	 */
	IntOption* getIntOption(string name);
	
	/**
	 * Adds a double option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addDoubleOption(string name, string id, 
			     double defaultVal, string helpMsg);

	/**
	 * Returns the value of a double option.
	 * @param name the option name.
	 * @return the option value.
	 */
	double getDouble(string name);
	
	/**
	 * Returns a double option.
	 * @param name the option name.
	 * @return the option.
	 */
	DoubleOption* getDoubleOption(string name);

	/**
	 * Adds a string option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 * @param valCase the string value's letter format.
	 */
	void addStringOption(string name, string id, string defaultVal, 
			     string helpMsg, StringCase valCase=MIXED);
	
	/**
	 * Returns the value of a string option.
	 * @param name the option name.
	 * @return the option value.
	 */
	string getString(string name);
	
	/**
	 * Returns a String option.
	 * @param name the option name.
	 * @return the option.
	 */
	StringOption* getStringOption(string name);
	
	/**
	 * Adds a twin int option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addIntX2Option(string name, string id, pair<int,int> defaultVal,
			     string helpMsg);
	
	/**
	 * Returns the value of a twin int option.
	 * @param name the option name.
	 * @return the option value.
	 */
	pair<int,int> getIntX2(string name);
	
	/**
	 * Returns a twin int option.
	 * @param name the option name.
	 * @return the option.
	 */
	IntX2Option* getIntX2Option(string name);
	
	/**
	 * Adds a twin double option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addDoubleX2Option(string name, string id,
			pair<double,double> defaultVal, string helpMsg);
	
	/**
	 * Returns the value of a twin double option.
	 * @param name the option name.
	 * @return the option value.
	 */
	pair<double,double> getDoubleX2(string name);
	
	/**
	 * Returns a twin double option.
	 * @param name the option name.
	 * @return the option.
	 */
	DoubleX2Option* getDoubleX2Option(string name);
	
	/**
	 * Adds a triple double option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param helpMsg the option's help message.
	 */
	void addDoubleX3Option(string name, string id, double defaultVal,
			double defaultVal2, double defaultVal3, string helpMsg);
	
	/**
	 * Returns the value of a triple double option.
	 * @param name the option name.
	 * @return the option value.
	 */
	std::vector<double> getDoubleX3(string name);
	
	/**
	 * Returns a triple double option.
	 * @param name the option name.
	 * @return the option.
	 */
	DoubleX3Option* getDoubleX3Option(string name);
	
	/**
	 * Adds a string alternative option, i.e. a string constrained by a
	 * predefined set of values.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param defaultVal the default value.
	 * @param validVals the valid alternatives in a comma-separated list
	 * (no extra spaces!).
	 * @param helpMsg the option's help message.
	 * @param valCase the string value's letter format.
	 * @param ignoreCase if false, makes exact letter case matching a 
	 * requirement.
	 */
	void addStringAltOption(string name, string id, string defaultVal, 
				string validVals, string helpMsg,
				StringCase valCase=MIXED, 
				bool ignoreCase=true);
	
	/**
	 * Returns the value of a string alternative option, i.e. an option
	 * where the value is constrained by a set of values.
	 * @param name the option name.
	 * @return the option value.
	 */
	string getStringAlt(string name);
	
	/**
	 * Returns a string alternative option, i.e. an option
	 * where the value is constrained by a set of values.
	 * @param name the option name.
	 * @return the option.
	 */
	StringAltOption* getStringAltOption(string name);
	
	/**
	 * Adds a user substitution model option.
	 * @param name the option's name.
	 * @param id the option's ID in user input (excluding dash).
	 * @param helpMsg the option's help message.
	 * @param ignoreCase if false, makes exact letter case matching 
	 * a requirement.
	 */
	void addUserSubstModelOption(string name, string id, string helpMsg,
				     bool ignoreCase=true);
	
	/**
	 * Returns a user substitution model option.
	 * @param name the option name.
	 * @return the option.
	 */
	UserSubstModelOption* getUserSubstModelOption(string name);
	
	/**
	 * Prints the option map as a string consisting of concatenated 
	 * help messages of all stored options (in the order they were 
	 * inserted).
	 * @param o the stream to append.
	 * @param bom the option map to print.
	 * @return the appended stream.
	 */
	friend ostream& operator<<(ostream& o, const BeepOptionMap &bom);
	
	/**
	 * Prints the option map as a string consisting of concatenated 
	 * help messages of all stored options (in the order they were 
	 * inserted).
	 * @param o the stream to append.
	 * @param bom the option map to print.
	 * @return the appended stream.
	 */
	friend ostream& operator<<(ostream& o, const BeepOptionMap* bom);
	
	/**
	 * Helper. Parses a char array to an int.
	 * @param str the string to be parsed.
	 * @param result the variable in which to store the value.
	 * @return false if the parsing failed.
	 */ 
	static bool toInt(char* str, int& result);
	
	/**
	 * Helper. Parses a char array to an unsigned int.
	 * @param str the string to be parsed.
	 * @param result the variable in which to store the value.
	 * @return false if the parsing failed.
	 */
	static bool toUnsigned(char* str, unsigned& result);
	
	/**
	 * Helper. Parses a char array to a double.
	 * @param str the string to be parsed.
	 * @param result the variable in which to store the value.
	 * @return false if the parsing failed.
	 */
	static bool toDouble(char* str, double& result);
	
private:
	
	void addOption(string name, BeepOption* option);
	
	void parseBool(BoolOption* bo, int& argIndex, int argc, char **argv);
	void parseUnsigned(UnsignedOption* bo, int& argIndex, int argc, 
			   char **argv);
	void parseInt(IntOption* bo, int& argIndex, int argc, char **argv);
	void parseDouble(DoubleOption* bo, int& argIndex, int argc, char **argv);
	void parseString(StringOption* bo, int& argIndex, int argc, char **argv);
	void parseIntX2(IntX2Option* bo, int& argIndex, int argc, char **argv);
	void parseDoubleX2(DoubleX2Option* bo, int& argIndex, int argc, char **argv);
	void parseDoubleX3(DoubleX3Option* bo, int& argIndex, int argc, char **argv);
	void parseStringAlt(StringAltOption* bo, int& argIndex, int argc, char **argv);
	void parseUserSubstModel(UserSubstModelOption* bo, int& argIndex, int argc, char **argv);
	
public:
	
private:
	
	/** IDs reserved for help options. */
	set<string> m_helpIds;
	
	/** Error message thrown when trying to parse unrecognized option. */
	string m_unknownOptionErrMsg;
	
	/** Options stored in map accessed by names. */
	map<string, BeepOption*> m_options;
	
	/** Extra map for accessing options by IDs. */
	map<string, BeepOption*> m_optionsById;
	
	/** Extra vector of options for printing them in order of insertion. */
	vector<BeepOption*> m_optionsInOrderOfIns;
};


/**
 * Base struct for option parameters in Beep/PrIME programs. Every option
 * has an ID, a help message, a parse error message, and a flag indicating
 * whether the option has been touched or not when parsing user input.
 * It's up to subclasses to implement value holder, etc. See BeepOptionMap
 * for usage.
 */
struct BeepOption
{
	string id;			/**< Identifier, e.g. 'Bi' for 
                                             parameter '-Bi'. */
	string helpMsg;			/**< Help message for printing. */
	string parseErrMsg;		/**< Error message when failed to   
                                             parse value. */
	bool hasBeenParsed;		/**< Initially false, set to true 
                                             if the option was parsed. */
	
	/** Constructor. */
	BeepOption(string id_, string helpMsg_, string parseErrMsg_) 
	  : id(id_), helpMsg(helpMsg_), parseErrMsg(parseErrMsg_), 
	    hasBeenParsed(false)
	{
	};
	
	/** Returns the option type. */
	virtual BeepOptionType getType() const
	{
		return EMPTY;
	};
	
	/** Destructor. */
	virtual ~BeepOption()
	{
	};
	
	/** Prints the help message of the option. */
	friend ostream& operator<<(ostream& o, const BeepOptionMap &bo);
	
	/** Prints the help message of the option. */
	friend ostream& operator<<(ostream& o, const BeepOptionMap* bo);
};


/** BeepOption for bool value. See BeepOptionMap for useage. */
struct BoolOption : BeepOption
{
	bool val;	/**< Option value. Initialized to default value. 
                             Inverted when option parsed. */
	
	BoolOption(string id_, bool defaultVal, string helpMsg_) 
	  : BeepOption(id_, helpMsg_, ""), 
	    val(defaultVal)
	{
	};
			
	BeepOptionType getType() const
	{
		return BOOL;
	};
};


/** BeepOption for unsigned int value. See BeepOptionMap for usage. */
struct UnsignedOption : BeepOption
{
	unsigned val;		/**< Option value. */
	
	UnsignedOption(string id_, unsigned defaultVal, string helpMsg_) 
	  : BeepOption(id_, helpMsg_, 
		       "Expected unsigned integer after option -"+id_+'.'), 
	    val(defaultVal)
	{
	};
	
	BeepOptionType getType() const
	{
		return UNSIGNED;
	};
};


/** BeepOption for int value. See BeepOptionMap for usage. */
struct IntOption : BeepOption
{
	int val;		/**< Option value. */
	
	IntOption(string id_, int defaultVal, string helpMsg_) 
	  : BeepOption(id_, helpMsg_, 
		       "Expected integer after option -"+id_+'.'), 
	    val(defaultVal)
	{
	};
	
	BeepOptionType getType() const
	{
		return INT;
	};
};


/** BeepOption for double value. See BeepOptionMap for usage. */
struct DoubleOption : BeepOption
{
	double val;		/**< Option value. */
	
	DoubleOption(string id_, double defaultVal, string helpMsg_) 
	  : BeepOption(id_, helpMsg_, 
		       "Expected float after option -"+id_+'.'),
	    val(defaultVal)
	{
	};
	
	BeepOptionType getType() const
	{
		return DOUBLE;
	};
};


/** BeepOption for string value. See BeepOptionMap for usage. */
struct StringOption	: BeepOption
{
	string val;			/**< Option value. */
	StringCase valCase;	/**< Defines value's letter format. */
	
	StringOption(string id_, string defaultVal, string helpMsg_, 
		     StringCase valCase_) 
	  : BeepOption(id_, helpMsg_, 
		       "Expected string after option -"+id_+'.'), 
	    val(defaultVal), 
	    valCase(valCase_)
	{
		if      (valCase == UPPER) 
		  {
		    transform(val.begin(), val.end(), val.begin(), 
			      (int(*)(int))toupper); 
		  }
		else if (valCase == LOWER) 
		  { 
		    transform(val.begin(), val.end(), val.begin(), 
			      (int(*)(int))tolower); 
		  }
	};
	
	BeepOptionType getType() const
	{
		return STRING;
	};
};


/** BeepOption for twin int values. See BeepOptionMap for usage. */
struct IntX2Option : BeepOption
{
	std::pair<int,int> val;		/**< Option value. */
	
	IntX2Option(string id_, std::pair<int,int> defaultVal, string helpMsg_) :
		BeepOption(id_, helpMsg_, "Expected pair of integers after option -"+id_+'.'), val(defaultVal)
	{
	};
	
	BeepOptionType getType() const
	{
		return INT_X2;
	};
};


/** BeepOption for twin double values. See BeepOptionMap for usage. */
struct DoubleX2Option : BeepOption
{
	std::pair<double,double> val;		/**< Option value. */
	
	DoubleX2Option(string id_, std::pair<double,double> defaultVal, string helpMsg_) :
		BeepOption(id_, helpMsg_, "Expected pair of doubles after option -"+id_+'.'), val(defaultVal)
	{
	};
	
	BeepOptionType getType() const
	{
		return DOUBLE_X2;
	};
};


/** BeepOption for triple double values. See BeepOptionMap for usage. */
struct DoubleX3Option : BeepOption
{
	double val;		/**< Option value. */
	double val2;	/**< Option value. */
	double val3;	/**< Option value. */
	
	DoubleX3Option(string id_, double defaultVal, double defaultVal2,
			double defaultVal3, string helpMsg_) :
		BeepOption(id_, helpMsg_, "Expected triplet of doubles after option -"+id_+'.'),
		val(defaultVal),
		val2(defaultVal2),
		val3(defaultVal3)
	{
	};
	
	BeepOptionType getType() const
	{
		return DOUBLE_X3;
	};
};


/**
 * BeepOption for string constrained by predefined alternatives. The valid 
 * alternatives must be provided in a comma-separated string. Case insensitive
 * by default.
 * See BeepOptionMap for usage.
 */
struct StringAltOption : BeepOption
{
	string val;	       		/**< Option value. */
	set<string> validVals;		/**< Set with valid alternatives. */
	StringCase valCase;		/**< Defines value's letter format. */
	bool ignoreCase;		/**< Ignores case when comparing value
                                             to valid alternatives. */
	
	StringAltOption(string id_, string defaultVal, 
			string validVals_, string helpMsg_, 
			StringCase valCase_, bool ignoreCase_);
	
	BeepOptionType getType() const
	{
		return STRING_ALT;
	};
};


/**
 * Highly customized option for parsing user defined substitution model.
 * Currently supports types 'DNA', 'AMINOACID' and 'CODON'.
 * Parses user input according to "-OptionId <type> <Pi> <R>".
 * Case insensitive by default. See BeepOptionMap for usage.
 */
struct UserSubstModelOption : BeepOption
{
	string type;		 /**< User substitution model string 
                                      identifier, e.g. "DNA". */
	vector<double> pi;	 /**< Stationary values. */
	vector<double> r;        /**< Model transition probabilities as
                                     flattened upper triangular matrix. */
	string sizeParseErrMsg;	 /**< Error message when lacking parameters
			              to Pi or R. */
	string piParseErrMsg;	 /**< Error message when failing to parse Pi.*/
	string rParseErrMsg;     /**< Error message when failing to parse R. */
	bool ignoreCase;         /**< Ignores case when comparing type to 
				      recognized alternatives. */
	
	UserSubstModelOption(string id_, string helpMsg_, bool ignoreCase_) :
		BeepOption(id_, helpMsg_, 
			   "Expected 'DNA'/'AminoAcid'/'Codon' after option "
			   +id_+'.'),
		type("UNDEFINED"),
		pi(),
		r(),
		sizeParseErrMsg("Too few parameters for Pi and R in user "
				"substitution model."),
		piParseErrMsg("Failed to parse Pi in user substitution model."),
		rParseErrMsg("Failed to parse R in user substitution model."),
		ignoreCase(ignoreCase_)
	{
	};
	
	BeepOptionType getType() const
	{
		return USER_SUBST_MODEL;
	};
};

} // end namespace option.
} // end namespace beep.

#endif /*BEEPOPTION_HH*/
