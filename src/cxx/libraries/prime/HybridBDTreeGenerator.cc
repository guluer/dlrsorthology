#include "HybridBDTreeGenerator.hh"
#include "AnError.hh"

#include <cmath>
#include <sstream>

namespace beep
{
  //-------------------------------------------------------------------
  //
  // Construct/destruct
  //
  //-------------------------------------------------------------------
  HybridBDTreeGenerator::HybridBDTreeGenerator(HybridTree& S_in, 
					       Real birthRate, Real deathRate)
    : BDTreeGenerator(S_in.getBinaryTree(), birthRate, deathRate),
      H(&S_in)
  {}
    
  HybridBDTreeGenerator::~HybridBDTreeGenerator()
  {}


  //-------------------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------------------
  StrStrMap 
  HybridBDTreeGenerator::exportGS()
  {
    if(gs.size() == 0)
      {
	throw AnError("No gs has been generated to return");
      }
    StrStrMap hgs;
    for(unsigned i = 0; i < G->getNumberOfNodes(); i++)
      {
	std::string u = G->getNode(i)->getName();
	Node* bx = S->findNode(gs.find(u));
	Node* hx = H->getCorrespondingHybridNode(bx);
	hgs.insert(u, hx->getName());
      }
    return hgs;
  }
    
  // Please note, this gamma maps the generated gene tree to the 
  // binary tree corresponding to S
  GammaMap 
  HybridBDTreeGenerator::exportGamma()
  {
    return BDTreeGenerator::exportGamma();
  }
              
  //-------------------------------------------------------------------
  // I/O
  //-------------------------------------------------------------------
  //     friend std::ostream& operator<<(std::ostream &o, 
  // 				    const HybridBDTreeGenerator& BDG);

}//end namespace beep
