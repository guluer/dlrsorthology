#ifndef UNIFORMDENSITY_HH
#define UNIFORMDENSITY_HH

#include "Density2P_common.hh"

#include "Probability.hh"

namespace beep
{
  
  //----------------------------------------------------------------------
  //
  // Class UniformDensity 
  //! Implements the uniform density distribution
  //
  //! \f[ 
  //!  f(x) = 
  //!  \begin{cases}
  //!   1/(\beta - \alpha), &  x\in [\alpha, \beta]\\	0,&\mbox{else}, 
  //!  \end{cases}
  //! \f],
  //! where \f$ \alpha = mean - \sqrt{3 variance} \f$, 
  //!   \f$ \beta = mean + \sqrt{3 variance} \f$  
  //! (unless bounds = true, see constructor). 
  //! Equivalently: \f$ mean  = \frac{\alpha+\beta}{2}\f$ and 
  //! \f$ variance = \frac{(\beta -\alpha)^2}{12} \f$
  //!
  //! Invariants: variance > 0
  //!             alpha < beta
  //!
  //! Author: Bengt Sennblad
  //! copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------
  class UniformDensity : public Density2P_common
  {
  public:
    //----------------------------------------------------------------------
    //
    // Construct/Destruct
    //
    //----------------------------------------------------------------------

    //! If bounds = true, mean and variance are taken to be
    //! lower and upper bound
    UniformDensity(Real mean, Real variance, bool embedded = false);
    UniformDensity(const UniformDensity& df);
    ~UniformDensity();

    UniformDensity& operator=(const UniformDensity& df);

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------

    // Density-, distribution, and inverse functions
    //----------------------------------------------------------------------

    //! The density function f(), returns \f[ f(x) = \begin{cases}
    //! 1 / (2 * variance) & \alpha \leq x \leq \beta \\ 0& else\end{cases}\f]
    Probability operator()(const Real& x) const;
 
    //! samples from distribution function, F(x), returns x: F(x) = p
    //! Precondition: 0 <= p <= 1.0.
    //! Postcondition: isInRange(sampleValue(p).
    Real sampleValue(const Real& p) const;

    // Access parameters
    //----------------------------------------------------------------------
    //! Returns \f$ mean = \frac{\beta + \alpha}{2}\f$
    Real getMean() const;

    //! Returns \f$ variance = \frac{\beta - \alpha}{12}\f$
    Real getVariance() const;



    // Sets parameters
    //----------------------------------------------------------------------
    //! Set mean and variance of density
    //! Precondition: newVariance >= 0.
    //! Postcondition: getMean()==mean and getVariance()==variance
    void setParameters(const Real& mean, const Real& variance);    

    //! Sets new mean of density
    //! Postcondition: getMean()==mean and getVariance()==variance
    void setMean(const Real& newMean);    

    //! Sets new variance of density
    //! Precondition: newVariance >= 0.
    //! Postcondition: getMean()==mean and getVariance()==variance
    void setVariance(const Real& newVariance);


//     // tests and information on valid ranges
//     //------------------------------------------------------------------

//     //! Check if value has a non-zero probability.
//     //! returns true if \f$ f(x)\neq 0 \Leftrightarrow 
//     //! \alpha \geq x \geq \beta \f$
//     //  i.e., if x is in interval [alpha, beta]
//     bool isInRange(const Real& x) const; 

//     //! Returns range [alpha, beta] in which f(x) has a non-zero probability.
//     void getRange(Real& min, Real& max) const;

    //------------------------------------------------------------------
    //
    // I/O
    //
    //------------------------------------------------------------------
    virtual std::string print() const;

  private:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------

    //! The attribute p is the constant value of f(x), the density function
    //------------------------------------------------------------------
    Probability p;

  };

}//end namespace beep
#endif
