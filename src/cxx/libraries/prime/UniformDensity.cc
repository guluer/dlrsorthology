#include "UniformDensity.hh"

#include "AnError.hh"

#include <assert.h>
#include <cmath>
#include <sstream>

namespace beep
{
  using namespace std;

  //----------------------------------------------------------------------
  //
  // Class UniformDensity 
  // Implements the uniform density distribution
  //
  // Invariants: variance > 0
  //             alpha < beta
  //
  // Author: Bengt Sennblad
  // copyright the MCMC-club, SBC
  //
  //----------------------------------------------------------------------

  //----------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //----------------------------------------------------------------------
  // If bounds = true, mean and variance are taken to be
  // lower and upper bound
  //----------------------------------------------------------------------
  UniformDensity::UniformDensity(Real mean, Real variance, bool embedded)
    : Density2P_common(mean, variance, "Uniform"),
      p()
  {
    if(embedded)
      {
	setEmbeddedParameters(mean, variance);
      }
    else
      {
	setParameters(mean, variance);
      }
    setRange(alpha, beta);
  }

  UniformDensity::UniformDensity(const UniformDensity& df)
    : Density2P_common(df),
      p(df.p)
  {}

  UniformDensity::~UniformDensity() 
  {}

  UniformDensity& 
  UniformDensity::operator=(const UniformDensity& df)
  {
    if(&df != this)
      {
	Density2P_common::operator=(df);
	p = df.p;
      }
    return *this;
  }


  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  // Density-, distribution, and inverse functions
  //----------------------------------------------------------------------
  
  // The density function f(), returns f(x)
  Probability 
  UniformDensity::operator()(const Real& x) const
  {
    if( x < alpha || x > beta)
      {
	return Probability(0);
      }
    else
      {
	return p;
      }
  }

  // samples from distribution function, F(x), returns x: F(x) = p
  // Precondition: 0 <= p <= 1.0.
  // Postcondition: isInRange(sampleValue(p)).
  Real
  UniformDensity::sampleValue(const Real& p_in) const 
  {
    assert(0 < p_in && p_in < 1.0); //Check precondition
    return alpha + ((beta - alpha) * p_in);
  }


  // Access parameters
  //----------------------------------------------------------------------
  Real
  UniformDensity::getMean() const 
  {
    return (alpha + beta) / 2;
  }
    
  Real 
  UniformDensity::getVariance() const 
  {
    return std::pow((beta - alpha), 2) / 12;
  }
    
  // Sets parameters
  //----------------------------------------------------------------------
  void
  UniformDensity::setParameters(const Real& mean, const Real& variance)
  {
    assert(variance >= 0); //Check precondition.

    Real diff = std::sqrt(3.0 * variance);
    alpha = mean - diff;
    beta = mean + diff;
    p = 1.0 / (beta - alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  UniformDensity::setMean(const Real& mean)
  {
    Real variance = getVariance();
    assert(variance >= 0); // Check invariant?

    Real diff = std::sqrt(3.0 * variance);
    alpha = mean - diff;
    beta = mean + diff;
    p = 1.0 / (beta - alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  UniformDensity::setVariance(const Real& variance)
  {
    assert(variance >= 0); //Check precondition.

    Real mean = getMean();
    Real diff = std::sqrt(3.0 * variance);
    alpha = mean - diff;
    beta = mean + diff;
    p = 1.0 / (beta - alpha);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }

  // tests and information on valid ranges
  //------------------------------------------------------------------

//   bool 
//   UniformDensity::isInRange(const Real& x) const
//   {
//     if(x < alpha || x > beta) // x is outside allowed interval
//       return false;
//     else 
//       return true;
//   }

//   void
//   UniformDensity::getRange(Real& min, Real& max) const
//   {
//     min = alpha;
//     max = beta; 
//     return;
//   }


  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
  std::string UniformDensity::print() const
  {
    std::ostringstream oss;
    oss << "Uniform distribution in the interval ["
	<< alpha
	<< ", "
	<< beta
	<< "].\n"
	<< "p =  " <<p<<"\n";
      ;
    return oss.str();
  }
    
}//end namespace beep
