#ifndef CONSTRATEMODEL_HH
#define CONSTRATEMODEL_HH

#include "EdgeRateModel_common.hh"

#include <iostream>

namespace beep
{
  // Forward declarations
  class Node;
  class Tree;

  //----------------------------------------------------------------------
  //
  // class ConstRateModel
  //! subclass of EdgeRateModel that models a stationary rate over edges 
  //! in the Tree T. This class model the mean relative rate (to an optional 
  //! external meanRates) for the Tree T, with the underlying density 
  //! function rateProb. 
  //!
  //!
  //!  Author Bengt Sennblad, 
  //!  copyright the MCMC club, SBC
  //
  //----------------------------------------------------------------------

  class ConstRateModel : public EdgeRateModel_common
  {
  public: 
    //----------------------------------------------------------------------
    //
    // Constructor/Destructor
    //
    //----------------------------------------------------------------------
    ConstRateModel(Density2P& rateProb, const Tree& T,
    		EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    ConstRateModel(Density2P& rateProb, const Tree& T, const Real& rate,
    		EdgeWeightModel::RootWeightPerturbation rwp = EdgeWeightModel::RIGHT_ONLY);
    ConstRateModel(const ConstRateModel& crm);  
    ~ConstRateModel();
    ConstRateModel& operator=(const ConstRateModel& crm);    
    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    virtual unsigned nRates() const;

    //----------------------------------------------------------------------
    // Interface inherited from ProbabilityModel
    //----------------------------------------------------------------------
    //! Returns f(rate) under the density rateProb
    //----------------------------------------------------------------------
    Probability calculateDataProbability();

    //! Nothing to update. 
    //----------------------------------------------------------------------
    void update();

    // Brief model description
    //----------------------------------------------------------------------
    virtual std::string type() const;

    // Pragmatic parameter access
    //! (Will be) Deprecated. 
    //! Should be replaced by an EdgeRateModel::getVectorOfRates
    //! that should be overloaded in ConstRateModel and VarRateModel so
    //! that it can be accessed through an EdgeRateModel reference or pointer.
    //  TODO: Implement above:. Q: Should it return a RealVector, where the
    // ConstRateModel impl. returns all elements filled with value of rate,
    // or should it return a vector so that CRM can return a single rate.
    //  (VarRateModel is easy) /bens
    // TODO: Does getRate and SetRate 
    //----------------------------------------------------------------------
    Real getRateParam() { return edgeRates[0u]; };


    //----------------------------------------------------------------------
    // Interface inherited from RateModel
    //----------------------------------------------------------------------

    //! returns value of rate - pointer version
    //----------------------------------------------------------------------
    virtual Real getRate(const Node* n = 0) const;
    //! returns value of rate - reference version
    //----------------------------------------------------------------------
    virtual Real getRate(const Node& n) const;

    //! Sets all edge rates to newRate - pointer version. Parameter node 
    //! allows dynamic calls from EdgeRateModel. Default parameter allows
    //! calls setRate(rate) from explicit ConstRateModel.
    virtual void setRate(const Real& newRate, const Node* n = 0);
    //! sets all edge rates to newRate - reference version.
    virtual void setRate(const Real& newRate, const Node& n);

    //------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const ConstRateModel& erm);
    virtual std::string print() const;

  };
}//end namespace beep

#endif

