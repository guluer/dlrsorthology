#include <cassert>
#include <cmath>

#include "AnError.hh"
#include "EpochPtSet.hh"

namespace beep
{

using namespace std;


EpochPtSet::EpochPtSet(std::vector<const Node*> edges, Real loTime, Real upTime, unsigned noOfIvs) :
	m_edges(edges),
	m_times(),
	m_timestep((upTime - loTime) / noOfIvs)
{
	assert(upTime > loTime);
	
	// Add times. Treat endpoints separately.
	m_times.reserve(noOfIvs + 2);
	m_times.push_back(loTime);
	for (unsigned i = 0; i < noOfIvs; ++i)
	{
		m_times.push_back(loTime + m_timestep / 2.0 + i * m_timestep);
	}
	m_times.push_back(upTime);
}


EpochPtSet::~EpochPtSet()
{
}


const vector<Real>&
EpochPtSet::getTimes() const
{
	return m_times;
}


Real
EpochPtSet::getTime(unsigned index) const
{
	return m_times[index];
}


Real
EpochPtSet::getLowerTime() const
{
	return m_times.front();
}


Real
EpochPtSet::getUpperTime() const
{
	return m_times.back();
}


unsigned
EpochPtSet::getNoOfTimes() const
{
	return m_times.size();
}


unsigned
EpochPtSet::getNoOfIntervals() const
{
	return (m_times.size() - 2);
}


Real
EpochPtSet::getTimestep() const
{
	return m_timestep;
}


Real
EpochPtSet::getTimeSpan() const
{
	return (m_times.back() - m_times.front());
}


const vector<const Node*>&
EpochPtSet::getEdges() const
{
	return m_edges;
}


const Node*
EpochPtSet::getEdge(unsigned index) const
{
	return m_edges[index];
}


unsigned
EpochPtSet::getNoOfEdges() const
{
	return m_edges.size();
}


unsigned
EpochPtSet::getNoOfPoints() const
{
	return (m_times.size() * m_edges.size());
}


} // end namespace beep.
