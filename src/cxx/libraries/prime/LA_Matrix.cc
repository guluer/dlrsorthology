#include <cassert>
#include <string>
#include <sstream>


#include "LA_Matrix.hh" 
#include "LA_DiagonalMatrix.hh" 

namespace beep
{
  
  //  public:
  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

    LA_Matrix::LA_Matrix()
      : dim(dim),
	data(new Real[dim*dim])
    {
      std::cerr <<"default constructor called\n";
      for(unsigned i = 0; i < dim*dim; i++)
	{
	  data[i] = 0;
	}
    };


  LA_Matrix::LA_Matrix(const unsigned& dim, const Real in_data[])
    : dim(dim),
      data(new Real[dim*dim])
  {
    for(unsigned i = 0; i < dim; i++)
      {
	// Using blas strided vector copy
	ACC_PREFIX(copy_)(dim, &in_data[i], dim, &data[i*dim], 1);
      }
  };

  // Create empty matrix
  //------------------------------------------------------------------------
  LA_Matrix::LA_Matrix(const unsigned& dim)
    : dim(dim),
      data(new Real[dim*dim])
  {
    for(unsigned i = 0; i < dim*dim; i++)
      {
	data[i] = 0;
      }
  };

  // Copy constructor
  //------------------------------------------------------------------------
  LA_Matrix::LA_Matrix(const LA_Matrix& B)
    : dim(B.dim),
      data(new Real[dim*dim])
  {
    // Using blas vector copy
    ACC_PREFIX(copy_)(dim*dim, B.data, 1, data, 1);
  };

  // Destructor
  //------------------------------------------------------------------------
  LA_Matrix::~LA_Matrix()
  {
    delete[] data;
  };


  // Assignment operator 
  //------------------------------------------------------------------------
  LA_Matrix& 
  LA_Matrix::operator=(const LA_Matrix& B)
  {
    if(this != &B)
      {
	assert(dim == B.dim);
	// Using blas vector copy
	ACC_PREFIX(copy_)(dim*dim, B.data, 1, data, 1);
      }
    return *this;
  };




  //public:
  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------
  
  // Dimension of matrix
  //------------------------------------------------------------------------
  const unsigned& 
  LA_Matrix::getDim() const
  {
    return dim;
  };

  // Access to individual elements
  //------------------------------------------------------------------------
  Real& 
  LA_Matrix::operator()(const unsigned& row, 
			const unsigned& column)
  {
    return data[column * dim + row];     
  };
    
  Real 
  LA_Matrix::operator()(const unsigned& row, 
			const unsigned& column) const
  {
    return data[column * dim + row];     
  };
    

  // Transpose of dense matrix
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::transpose() const
  {
    LA_Matrix C(dim);
    for(unsigned i = 0; i < dim; i++)
      {
	ACC_PREFIX(copy_)(dim, &data[i*dim], 1, &C.data[i], dim);	  
      }
    return C;
  };
    

  // Inverse of Matrix using Lapack function
  //   void dgetri_(const int& dim, double dA[], const int& ldA,
  //                int dI[], double dW[], const int&ldW, int& info);
  // which performs A <- inv(A)
  // where matrix A (= copy of this, will also hold the answer) has 
  // dimension ldA (here always=dim) by dim, 
  // dW (a workspace) is an array and has dimension ldW(>= 4*dim)
  // dI (holds indices of pivots) is an array of ints and has dimension dim
  // info is an error message and is 0 on success
  //
  // Should be overloaded in derived class, but can not virtual because 
  // different type is returned
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::inverse() const
  {
    LA_Matrix A(*this);
    int dI[dim];
    unsigned ldW = dim + 1;
    // this dim is what mtl uses, it might possibly be more effective if 
    // the dimension of dW is bigger according to LAPACK documentation
    // The optimal value of ldW is given (as sW[0]
    Real dW[ldW];
    int info;

    ACC_PREFIX(getrf_)(dim, dim, A.data, dim, dI, info);
    if(info != 0)
      {
	throw AnError("LA_Matrix::inverse():"
		      "blas::dgetrf failed");
      }

    ACC_PREFIX(getri_)(dim, A.data, dim, dI, dW, ldW, info);
    if(info != 0)
      {
	throw AnError("LA_Matrix::inverse():"
		      "blas::dgetri failed");
      }
    return A;
  };

  // Trace (sum of diagonal elements of Matrix A using BLAS function
  //   void dasum_(const int& n, const double x, const int& stridex)
  // which performs d = x_1 + x_2 + ... + x_dim,
  // where d is a scalar that is returned by the function,
  // Vector x (this) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = dim always).
  //------------------------------------------------------------------------
  Real 
  LA_Matrix::trace() const
  {
    const LA_Matrix& Q = *this;
    Real trace = 0;
    for(unsigned i = 0; i < dim; i++)
      {
	trace += Q(i,i);
      }
    return trace;//beep::dasum_(dim*dim, data, dim);//FELFELFEL!
  };


  // Addition of matrices A and B using BLAS function
  //   void daxpy_(const int& dim, const double& alpha,  
  //               const double x[],  const int& stridex,
  // 	             const double y[], const int& stridy)
  // which performs y = alpha * x + y,
  // where alpha is a scalar that multiplies x,
  // Vector x (the argument) and y (a copy of this that also will 
  // hold the answer) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Matrix
  LA_Matrix::operator+(const LA_Matrix& B) const
  {
    assert(B.dim == dim);
    LA_Matrix C(B);

    beep::ACC_PREFIX(axpy_)(dim*dim, 1, data, 1, C.data, 1);
    return C;
  };


  // Multiplication of matrix A and a scalar using BLAS function
  //   void dscal_(const int& dim, const double alpha, 
  //               const double x[],  const int& stridex)
  // which performs x = alpha * x,
  // alpha is a scalar
  // Vector x (a copy of this that also will hold the answer) has 
  // dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::operator*(const Real& alpha) const
  {
    LA_Matrix A(*this);
    beep::ACC_PREFIX(scal_)(dim*dim, alpha, A.data, 1);
    return A;
  };

  LA_Matrix 
  operator*(const Real& alpha, const LA_Matrix& A)
  {
    LA_Matrix B(A);
    beep::ACC_PREFIX(scal_)(B.getDim()*B.getDim(), alpha, B.data, 1);
    return B;
  };

  // Division of matrix A and a scalar using BLAS function
  //   void dscal_(const int& dim, const double alpha, 
  //               const double x[],  const int& stridex)
  // which performs x = alpha * x,
  // alpha is a scalar
  // Vector x (a copy of this that also will hold the answer) has 
  // dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::operator/(const Real& alpha) const 
  {
    LA_Matrix A(*this);
    beep::ACC_PREFIX(scal_)(dim*dim, 1/alpha, A.data, 1);
    return A;
  };

  LA_Vector 
  LA_Matrix::col(const unsigned& col) const
  {
      LA_Vector l(dim, data + col * dim);
      return l;
  };
  
  // Multiplication of matrix A and a vector where element pos equals alpha
  // and the remaining elements are zero
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Matrix::col_mult(const Real& alpha, const unsigned& col) const
  {
    assert(col < dim);
    LA_Vector l(dim, data + col * dim);
    beep::ACC_PREFIX(scal_)(dim, alpha, l.data, dim);
    return l;
  };

  bool
  LA_Matrix::col_mult(LA_Vector& result, const Real& alpha, 
		      const unsigned& col) const
  {
    if(col >= dim)
      return false;
    ACC_PREFIX(copy_)(dim, data + col * dim, 1, result.data, 1);
    beep::ACC_PREFIX(scal_)(dim, alpha, result.data, 1);
    return true;
  };


  // Multiplication of matrix A and vector x using BLAS function
  //   void dgemv_(const char& transA, 
  // 	      const int& cols, const int& rows,
  // 	      const double& alpha,  const double dA[],  const int& lda,
  // 	      const double x, const int& stridex, 
  // 	      const double& beta, double y[], const int& stridey);
  // which performs C = alpha * A * x + beta * C,
  // where transX indicates the transposition to be performed on matrix X 
  // (here always no transposition = 'N'). alpha is a scalar that multiplies
  // A and beta is a scalar that multiplies C before performing addition.
  // A (=this) and C (the result has dimension m by n (here m = n always).
  // dX is the data of matrix X stored contiguously in memory, and ldX is
  // the dimension of this storage (here ldX = m always)
  // Vector x has dimension n, stridex is the stride between elements of x 
  // if not stored completely continuosly (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Matrix::operator*(const LA_Vector& x) const
  {
    assert(x.getDim() == dim);
    LA_Vector y(dim);
      
    ACC_PREFIX(gemv_)('N', dim, dim, 1, data, dim, x.data, 1, 0, y.data, 1);
    return y;
  };

  void
  LA_Matrix::mult(const LA_Vector& x, LA_Vector& result) const
  {
    assert(x.getDim() == dim && result.getDim() == dim);
      
    ACC_PREFIX(gemv_)('N', dim, dim, 1, data, dim, x.data, 1, 0, 
		      result.data, 1);
    return;
  };

  // Multiplication of matrices A and B using BLAS function
  //   void dgemm_(const char& transA, const char& transB, 
  // 	      const int& m, const int& n, const int& k,
  // 	      const double& alpha,  const double dA[],  const int& lda,
  // 	      const double dB[], const int& ldb, 
  // 	      const double& beta, double dC[], const int& ldc);
  // which performs C = alpha * A *B + beta * C,
  // where transX indicates the transposition to be performed on matrix X 
  // (here always no transposition = 'N'). alpha is a scalar that multiplies
  // A and beta is a scalar that multiplies C before performing addition.
  // A (=this) has dimension m by k, B (the argument) has dimension k by n, 
  // and C (=the answer) has dimension m by n (here m = n = k always).
  // dX is the data of matrix X stored contiguously in memory, and ldX is
  // the dimension of this storage (here ldX = m always)
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::operator*(const LA_Matrix& B) const
  {
    assert(B.dim == dim);
    LA_Matrix C(dim);

    ACC_PREFIX(gemm_)('N', 'N', dim, dim, dim, 
		      1, data, dim, B.data, dim, 0, C.data, dim);
    return C;
  }

  void
  LA_Matrix::mult(const LA_Matrix& B, LA_Matrix& result) const
  {
    assert(B.dim == dim && result.dim == dim);

    ACC_PREFIX(gemm_)('N', 'N', dim, dim, dim, 
		      1, data, dim, B.data, dim, 0, result.data, dim);
    return;
  }

  // Multiplication of a dense matrix and a diagonal matrix
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Matrix::operator*(const LA_DiagonalMatrix& D) const
  {
    assert(D.getDim() == dim);
    LA_Matrix C(*this);
    for(unsigned i = 0; i < dim; i++)
      {
	beep::ACC_PREFIX(scal_)(dim, D.data[i], &C.data[i*dim], 1);
      }
    return C;
  }

  // Elementwise multiplication of two matrices  
  //------------------------------------------------------------------------
  LA_Matrix
  LA_Matrix::ele_mult(const LA_Matrix& B) const
  {
    assert(B.dim == dim);
    LA_Matrix C(dim);
    for(unsigned i = 0; i < dim*dim; i++)
      {
	C.data[i] = data[i] * B.data[i];
      }
    return C;
  }

  // Sum of elements  of Matrix A using BLAS function
  //   void dasum_(const int& dim, const double x, const int& stridex)
  // which performs d = x_1 + x_2 + ... + x_dim,
  // where d is a scalar that is returned by the function,
  // Vector x (this) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  Real 
  LA_Matrix::sum() const
  {
    return beep::ACC_PREFIX(asum_)(dim*dim, data, 1);
  };

  // Eigensystem of dense matrix A (=this) using LAPACK function
  //   void dgeev_(const char& jobl, jobr, 
  // 	         const int& m, double dA[],  const int& lda,
  // 	         double dER[], const double dEZ[],
  //               double dL[], const int& ldL,
  //               double dR[], const int& ldR,
  //               double dW[], const int& ldW, int& info)
  // which performs 
  // ER/EZ = matrices with real/imaginary parts of eigenvalues
  // of A, L/R = matrices with left/right eigenvectors of A
  // A, ER, EZ, L, R all have dimension m by m 
  // dW is a workspace array for BLAS, it has dimension ldw >= 4*m 
  // (optimal value of ldW can be queried by setting ldW=-1, on return W[1] 
  // always holds the optimal value of ldW.
  // dX is the data of matrix X stored contiguously in memory, and ldX is
  // the dimension of this storage (here ldX = m always)
  //------------------------------------------------------------------------
  void 
  LA_Matrix::eigen(LA_DiagonalMatrix& E, LA_Matrix& V, LA_Matrix& iV)
  {
    assert(E.getDim() == dim && V.getDim() == dim && iV.getDim() == dim);
    LA_Matrix Q(*this);
    Real e[dim]; 
    Real ez[dim];    // dummy for compleximaginary part of eigenvalues

    unsigned ldW = 4 * dim;
    Real dW[ldW];
    int info;
	  
    ACC_PREFIX(geev_)('N', 'V', dim, Q.data, dim, e, ez, 
		      0, dim, V.data, dim, dW, ldW, info);
    if(info != 0) 
      {
	throw AnError("eigen failed");
      }
  
    ACC_PREFIX(copy_)(dim, e, 1, E.data, 1);
  
    iV = V.inverse();
 
    return;
  };
    
  //------------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------------
  std::ostream& operator<<(std::ostream& os, 
				 const beep::LA_Matrix& A)
  {
    std::ostringstream oss;
    unsigned dim = A.getDim();
    oss << "dimension: " << dim << "\n";
    for(unsigned i = 0; i < dim; i++)
      {
	for(unsigned j = 0; j < dim; j++)
	  {
	    oss << "\t" << A(i,j) << ",";
	  }
	oss << "\n";
      }
    return os << oss.str();
  };


  // LA_Vector::col_row_product defined here beacuse it needs to 
  // know about LA_MAtrix
  //
  // Column row product of vectors x and y 
  // Vector x (the argument) and y has the same dimension dim, and the result
  // is saved in a matrix with dimension (dim, dim)
  //------------------------------------------------------------------------
  LA_Matrix 
  LA_Vector::col_row_product(const LA_Vector& x) const
  {
    assert(x.dim == dim);
    LA_Matrix M(x.dim);
    for(unsigned i = 0; i < dim; i++)
      {
	for(unsigned j = 0; j < dim; j++)
	  {
	    M(i,j) = operator[](i) * x[j];
	  }
      }
    //    return beep::ACC_PREFIX(dot_)(dim, data, 1, x.data, 1);
    return M;
  };
  


}// end namespace beep


