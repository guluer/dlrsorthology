#ifndef PRIMEOPTIONMAP_HH
#define PRIMEOPTIONMAP_HH

#include "Beep.hh"

#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <typeinfo>
#include <vector>
#include <cxxabi.h>

namespace beep
{
  // 
  extern const unsigned MAXPARAMS;
  //Forward declaration
  class PrimeOption;

  // Helper class holding parameters of UserUSbstMatrix
  class UserSubstMatrixParams
  {
  public:
    std::string seqtype;
    std::vector<Real> Pi;
    std::vector<Real> R;
  };


  //! Convenience container class for handling options in PrIME programs.
  //!
  //! Handles options of various types, where each option has a descriptive name,
  //!  an ID (the one used as user input to the program, e.g. "Ab" for option
  //! "-Ab") and a value. Moreover, a customized error message (other than the
  //! default one) may be set. 
  //! 
  //! Typical usage:
  //! \code
  //!    ...
  //!    using namespace beep::option;
  //!    ...
  //!    PrimeOptionMap pom;
  //!    pom.addIntOption("Opt1", "A", 123, ...);    // 123 is default val.
  //!    pom.addBoolOption("Opt2", "B", true, ...);
  //!    pom.addUnsignedOption("Opt3", "Cm", 555, ...);
  //!    pom.addStringOption("Opt4", "Cn", "Hooray!", ...);
  //!    pom.addStringAltOption("Opt5", "D", "Knave", "Knight,Knave,Spy", ...);
  //!    ...
  //!    try
  //!    {
  //!       int argIndex = 1;
  //!       if (!bom.parseOptions(argIndex, argv, argc))
  //!       {
  //!          cerr << "These are the options:" << endl;
  //!          cerr << bom;      // Prints help messages of stored options.
  //!          exit(0);
  //!       }
  //!    }
  //!    catch (AnError& e)
  //!    {
  //!       showErrorMessage(e);
  //!       exit(1);
  //!    }
  //!    ...
  //!    unsigned myCmVal = bom.getUnsigned("Opt3");   // Retrieves option.
  //!    ...
  //! \endcode
  //! 
  //! As can be seen, the class includes e.g. a method for parsing user input 
  //! and an overloaded ostream operator '<<' to allow for the map to be printed.
  //! The output in this case is all of the help messages of the options 
  //! concatenated in the order in which they were inserted.
  //---------------------------------------------------------------------
  class PrimeOptionMap
  {
  public:
	
    //! \nameConstruct/destruct/assign
    //@{
    //-------------------------------
    //! @param helpIds are special IDs reserved for help options in
    //!  comma-separated list, see parseOptions() for more info.
    //! @param unknownOptionErrMsg string thrown when an unknown 
    //!  option is parsed.
    PrimeOptionMap(std::string helpIds = "h,u,?", 
		   std::string unknownOptionErrMsg = "Unknown option");
    virtual ~PrimeOptionMap();
    //@}
	
    //! \name Interface
    //@{
    //----------------------------------------------------------------
    //! Add a usage text that describes the program
    //! @param the Name of the program
    //! @param the compulsory parameters to the program
    //! @param text describing the program
    void addUsageText(std::string progName, std::string parameters, 
		      std::string usageText);
    std::string getUsage() const;

    //! \name Adds an option of various types.
    //! @param name the option's name.
    //! @param id the option's ID in user input (excluding dash).
    //! @param defaultVal the default value.
    //! @param helpMsg the option's help message.
    //@{
    void addBoolOption(std::string name, std::string id, unsigned nParameters, 
		       std::string defaultValues, std::string validValues,
		       std::string helpMessage);
    void addIntOption(std::string name, std::string id, unsigned nParameters, 
		      std::string defaultValues, std::string validValues, 
		      std::string helpMessage);
    void addUnsignedOption(std::string name, std::string id, 
			   unsigned nParameters, std::string defaultValues, 
			   std::string validValues, std::string helpMessage);
    void addRealOption(std::string name, std::string id, unsigned nParameters,
		       std::string defaultValues, std::string validValues,
		       std::string helpMessage);
    void addStringOption(std::string name, std::string id, unsigned nParameters, 
			 std::string defaultValues,std::string validValues,
			 std::string helpMessage);
    void addUserSubstMatrixOption(std::string name, std::string id, 
				  unsigned nParameters, 
				  std::string defaultValues,
				  std::string validValues,
				  std::string helpMessage);
    //@}

    //! \name Returns the value of an option of various types.
    //! @param name the option name.
    //! @return the option value.
    //@{
    std::vector<bool>        getBool(std::string name);
    std::vector<int>         getInt(std::string name);
    std::vector<unsigned>    getUnsigned(std::string name);
    std::vector<Real>        getReal(std::string name);
    std::vector<std::string> getString(std::string name);
    std::vector<UserSubstMatrixParams> 
    getUserSubstitutionMatrix(std::string name);
    //@}
    //! Check if option hasbeen treated already
    bool hasBeenParsed(std::string name);

    //! Parses options from user input as long as correct options 
    //! starting with character '-' can be found. If an unknown option 
    //! is encountered, 'AnError' stating this is thrown.
    //! Similarly, if a parsing error is encountered, 'AnError' with 
    //! the error message of that particular option is thrown. If one 
    //! of the reserved help option IDs are encountered, the parsing 
    //! is aborted and false is returned, otherwise true is returned.
    //! @param argIndex the current index in the user input array. 
    //! Incremented with one for each parsed option.
    //! @param argc the size of the user input array.
    //! @param argv the user input array.
    //! @return false if an help option ID was encountered, otherwise true.
    bool parseOptions(int& argIndex, int argc, char **argv);

    //! IO
    friend std::ostream& beep::operator<<(std::ostream& o, 
					  const PrimeOptionMap& pom);
    friend std::ostream& beep::operator<<(std::ostream& o, 
					  const PrimeOption& po);
    static std::string formatMessage(std::string opt, std::string usageText);

  protected:
    //! \name Implementation
    //@{
    //-----------------------
    //! \name Helpers for access functions
    //@{
    void addOption(std::string name, std::string id, PrimeOption& option);
    PrimeOption& getOption(std::string name);
    PrimeOption& getOptionById(std::string id);

    //@}
    //@}


  private:
    //! \name Attributes
    //@{
    //------------------------	
    std::set<std::string> helpIds;             //!< reserved help option IDs 
    std::string usage;                         //!< usage text
    std::string unknownOptionErrMsg;           //!< 'Unrecognized error' message
    std::map<std::string, PrimeOption*> options;    //!< Options access by name
    std::map<std::string, PrimeOption*> optionsById;//!< �ptions access by IDs. 
    std::vector<PrimeOption*> optionsInOrderOfIns;  //! Options in insert order

    //! \name default help measures for usage text
    //@{
    static unsigned defIndent;
    static unsigned defTab;
    static unsigned maxLength; 
    //@}
    //@}
  };
  
} // end namespace beep.

#endif ///!PRIMEOPTIONMAP_HH//!/
