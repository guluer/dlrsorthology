#ifndef EPOCHPTMAPS_HH
#define EPOCHPTMAPS_HH

#include <sstream>
#include <string>
#include <utility>
#include <vector>

#include "EpochTree.hh"
#include "GenericMatrix.hh"
#include "Node.hh"
#include "Probability.hh"

namespace beep
{

/**
 * Map for storing data for each point of a discretized epoch tree.
 * 
 * Values are stored as follows:
 * For each discretized time (of which there may be two since adjacent
 * epochs each have points on the border), there is a vector of values
 * corresponding to each edge at that time.
 * These vectors are stored in an enclosing vector which covers the times
 * of all epochs appended after each other.
 * 
 * If the discretization topology of the epochs on which the map is based
 * changes, the map is invalidated (and a new map must be created).
 * 
 * Points are referenced by triplets: epoch number, time index in epoch
 * and edge index in epoch.
 * 
 * The map includes functionality for caching and restoring values.
 * 
 * @author Joel Sjöstrand.
 */
template<typename T>
class EpochPtMap
{
	typedef typename std::vector<T>::iterator iterator;
	typedef typename std::vector<T>::const_iterator const_iterator;
	
public:
	
	/**
	 * Constructor. Creates a map filled with default elements.
	 * @param ES the discretized tree.
	 * @param defaultVal default value for all points in map.
	 */
	EpochPtMap(const EpochTree& ES, const T& defaultVal);
	
	/**
	 * Copy-constructor. Cached values are not copied.
	 */
	EpochPtMap(const EpochPtMap& ptMap);
	
	/**
	 * Destructor.
	 */
	virtual ~EpochPtMap();
	
	/**
	 * Assignment operator. Cached values are not copied, neither
	 * the underlying tree (the latter is assumed to be the same).
	 */
	EpochPtMap& operator=(const EpochPtMap& ptMap);
	
	/**
	 * Returns the value at a certain point.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @param k the edge index in epoch.
	 * @return the value at the point.
	 */
	inline T& operator() (unsigned i, unsigned j, unsigned k)
	{
		return (m_vals[m_offsets[i] + j][k]);
	}
    
	/**
	 * Returns the value at a certain point.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @param k the edge index in epoch.
	 * @return the value at the point. 
	 */
    inline const T& operator() (unsigned i, unsigned j, unsigned k) const
    {
    	return (m_vals[m_offsets[i] + j][k]);
    }
	
    /**
	 * Returns the value at a certain point.
	 * @param tm the discretized time.
	 * @param k the edge index in epoch.
	 * @return the value at the point.
	 */
	inline T& operator() (const EpochTime& tm, unsigned k)
	{
		return (m_vals[m_offsets[tm.first] + tm.second][k]);
	}
    
	/**
	 * Returns the value at a certain point.
	 * @param tm the discretized time.
	 * @param k the edge index in epoch.
	 * @return the value at the point.
	 */
	inline const T& operator() (const EpochTime& tm, unsigned k) const
	{
		return (m_vals[m_offsets[tm.first] + tm.second][k]);
	}
	
    /**
	 * Returns all values for a certain time in a certain epoch.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @return the values for all concerned edges.
	 */
	inline std::vector<T>& operator() (unsigned i, unsigned j)
	{
		return (m_vals[m_offsets[i] + j]);
	}
    
	/**
	 * Returns all values for a certain time in a certain epoch.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @return the values for all concerned edges.
	 */
    inline const std::vector<T>& operator() (unsigned i, unsigned j) const
    {
    	return (m_vals[m_offsets[i] + j]);
    }
    
    /**
	 * Returns all values for a certain time in a certain epoch.
	 * @param tm the discretized time.
	 * @return the values for all concerned edges.
	 */
	inline std::vector<T>& operator() (const EpochTime& tm)
	{
		return (m_vals[m_offsets[tm.first] + tm.second]);
	}
	
	 /**
	 * Returns all values for a certain time in a certain epoch.
	 * @param tm the discretized time.
	 * @return the values for all concerned edges.
	 */
	inline const std::vector<T>& operator() (const EpochTime& tm) const
	{
		return (m_vals[m_offsets[tm.first] + tm.second]);
	}
    
	/**
	 * Sets values for all points for a certain time in an epoch.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @param start the beginning of vector to copy from.
	 */
	inline void set(unsigned i, unsigned j, const_iterator start)
	{
		std::vector<T>& v = m_vals[m_offsets[i] + j];
		v.assign(start, start + v.size());
	}
    
	/**
	 * Sets values for all points for a certain time in an epoch.
	 * @param tm the discretized time.
	 * @param start the beginning of vector to copy from.
	 */
	inline void set(const EpochTime& tm, const_iterator start)
	{
		std::vector<T>& v = m_vals[m_offsets[tm.first] + tm.second];
		v.assign(start, start + v.size());
	}
	
	/**
	 * Sets values for all points for a certain time in an epoch.
	 * If a value to be set is smaller than a specified bound, the
	 * bound is used instead.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @param start the beginning of vector to copy from.
	 * @param lowerBound the lower bound.
	 */
	inline void setWithMin(unsigned i, unsigned j, const_iterator start, const T& lowerBound)
	{
		std::vector<T>& v = m_vals[m_offsets[i] + j];
		for (iterator it = v.begin(); it != v.end(); ++it, ++start)
		{
			(*it) = std::max(*start, lowerBound);
		}
	}
	
	/**
	 * Sets values for all points for a certain time in an epoch.
	 * If a value to be set is greater than a specified bound, the
	 * bound is used instead.
	 * @param i the epoch index.
	 * @param j the time index in epoch.
	 * @param start the beginning of vector to copy from.
	 * @param upperBound the upper bound.
	 */
	inline void setWithMax(unsigned i, unsigned j, const_iterator start, const T& upperBound)
	{
		std::vector<T>& v = m_vals[m_offsets[i] + j];
		for (iterator it = v.begin(); it != v.end(); ++it, ++start)
		{
			(*it) = std::min(*start, upperBound);
		}
	}
	
	/**
	 * Returns the value for a specified edge for
	 * the last time of a specified epoch.
	 * @param i the epoch index.
	 * @param k the edge index in epoch.
	 * @return the value.
	 */
	inline T& getForLastTime(unsigned i, unsigned k)
	{
		return (m_vals[m_offsets[i+1] - 1][k]);
	}
	
	/**
	 * Returns the value for a specified edge for
	 * the last time of a specified epoch.
	 * @param i the epoch index.
	 * @param k the edge index in epoch.
	 * @return the value.
	 */
	inline const T& getForLastTime(unsigned i, unsigned k) const
	{
		return (m_vals[m_offsets[i+1] - 1][k]);
    }
	
	/**
	 * Returns the values for a time below a specified one.
	 * @param tm the time identifier.
	 * @return the values.
	 */
	inline std::vector<T>& getBelow(const EpochTime& tm)
	{
		return (m_vals[m_offsets[tm.first] + tm.second - 1]);
	}
	
	/**
	 * Returns the values for a time below a specified one.
	 * @param tm the time identifier.
	 * @return the values.
	 */
	inline const std::vector<T>& getBelow(const EpochTime& tm) const
	{
		return (m_vals[m_offsets[tm.first] + tm.second - 1]);
    }
    
	/**
	 * Returns the topmost value, i.e. the value at
	 * the very last time of the last epoch.
	 * @return the topmost value.
	 */
	inline T& getTopmost()
	{
		return m_vals.back().front();
	}
	
	/**
	 * Returns the topmost value, i.e. the value at
	 * the very last time of the last epoch.
	 * @return the topmost value.
	 */
	inline const T& getTopmost() const
	{
		return m_vals.back().front();
    }
	
	/**
	 * Resets all values in entire map to the specified value.
	 * @param defaultVal the value to be set.
	 */
	void reset(const T& defaultVal);
	
	/**
	 * Saves current values in a cache. Cached values can be
	 * restored with a call to restoreCache(), or be disabled
	 * via call to invalidateCache().
	 */
	void cache();
	
	/**
	 * Restores cached values. Has no effect if
	 * invalidateCache() has been invoked since call to cache().
	 */
	void restoreCache();
	
	/**
	 * Disables last made cache.
	 */
	void invalidateCache();
	
    /**
     * Returns a string representation of the map.
     * Index 0 is printed last.
     * @return a string representation.
     */
    std::string print() const;
    
    /**
     * Prints the map. Index 0 is printed last, i.e. leaves are printed
     * at the bottom.
     */
	friend std::ostream& operator<<(std::ostream &o, const EpochPtMap<T>& map)
    {
    	return o << map.print();
    }
    
private:
	
	/** Pointer to tree on which map is based. */
	const EpochTree& m_ES;
	
	/** For each epoch, the offset in value vector with regard to times. */
	std::vector<unsigned> m_offsets;
	
	/**
	 * Values for all points. Each element in outer vector
	 * corresponds to a time.
	 */
	std::vector< std::vector<T> > m_vals;
	
	/** Cached values. */
	std::vector< std::vector<T> > m_cache;
	
	/** Indicates cache validity. */
	bool m_cacheIsValid;
};


/**
 * Consider the point set of a discretized epoch tree. This map
 * stores data for the Cartesian product of two such sets,
 * i.e. one value for every pair of points.
 * 
 * Data is stored in a matrix where element (i,j) contains all map values
 * from time i to time j.
 * Such an element is in turn a matrix (concatenated into a vector)
 * containing the values from all points at time i to all points at
 * time j.
 * 
 * If the discretization topology of the epochs on which the map is
 * based changes, the map is invalidated and a new instance replacing
 * it must be created.
 * 
 * Points are referenced by triplets: epoch number, time index in epoch
 * and edge index in epoch. Values are, naturally, retrieved using
 * two points.
 * 
 * The map includes functionality for caching and restoring values.
 */
template<typename T>
class EpochPtPtMap
{
	typedef typename std::vector<T>::iterator iterator;
	typedef typename std::vector<T>::const_iterator const_iterator;
	
public:

	/**
	 * Constructor. Fills the map with default elements.
	 * @param ES the discretized epoch tree.
	 * @param defaultVal the default value for all point pairs.
	 */
	EpochPtPtMap(const EpochTree& ES, const T& defaultVal);
	
	/**
	 * Copy-constructor. Cached values are not copied.
	 */
	EpochPtPtMap(const EpochPtPtMap& ptPtMap);
	
	/**
	 * Destructor.
	 */
	virtual ~EpochPtPtMap();
	
	/**
	 * Assignment operator. Cached values are not copied, neither
	 * the underlying tree (the latter is assumed to be the same).
	 */
	EpochPtPtMap& operator=(const EpochPtPtMap& ptPtMap);
	
	/**
	 * Sets all values from points of a certain time to points
	 * of a certain time. The input matrix should be provided
	 * in form of a vector where matrix rows have been concatenated.
	 * @param i epoch index of time 1.
	 * @param j time index in epoch of time 1.
	 * @param p epoch index of time 2.
	 * @param q time index in epoch of time 2.
	 * @param start beginning of vector to copy from.
	 */
	inline void set(unsigned i, unsigned j, unsigned p, unsigned q,
			const_iterator start)
	{
		std::vector<T>& v = m_vals(m_offsets[i] + j, m_offsets[p] + q);
		v.assign(start, start + v.size());
	}
	
	/**
	 * Sets all values from points of a certain time to points
	 * of a certain time. The input matrix should be provided
	 * in form of a vector where matrix rows have been concatenated.
	 * @param tm1 the first discretized time.
	 * @param tm2 the second discretized time.
	 * @param start beginning of vector to copy from.
	 */
	inline void set(EpochTime tm1, EpochTime tm2, const_iterator start)
	{
		std::vector<T>& v = m_vals(m_offsets[tm1.first] + tm1.second,
				m_offsets[tm2.first] + tm2.second);
		v.assign(start, start + v.size());
	}
	
	/**
	 * Sets all values from points of a certain time to points
	 * of a certain time. The input matrix should be provided
	 * in form of a vector where matrix rows have been concatenated.
	 * If a value to be set is smaller than a specified bound, the
	 * bound is used instead.
	 * @param i epoch index of time 1.
	 * @param j time index in epoch of time 1.
	 * @param p epoch index of time 2.
	 * @param q time index in epoch of time 2.
	 * @param start beginning of vector to copy from.
	 * @param lowerBound the lower bound.
	 */
	inline void setWithMin(unsigned i, unsigned j, unsigned p, unsigned q,
			const_iterator start, const T& lowerBound)
	{
		std::vector<T>& v = m_vals(m_offsets[i] + j, m_offsets[p] + q);
		for (iterator it = v.begin(); it != v.end(); ++it, ++start)
		{
			(*it) = std::max(*start, lowerBound);
		}
	}
	
	/**
	 * Sets all values from points of a certain time to points
	 * of a certain time. The input matrix should be provided
	 * in form of a vector where matrix rows have been concatenated.
	 * If a value to be set is greater than a specified bound, the
	 * bound is used instead.
	 * @param i epoch index of time 1.
	 * @param j time index in epoch of time 1.
	 * @param p epoch index of time 2.
	 * @param q time index in epoch of time 2.
	 * @param start beginning of vector to copy from.
	 * @param upperBound the upper bound.
	 */
	inline void setWithMax(unsigned i, unsigned j, unsigned p, unsigned q,
			const_iterator start, const T& upperBound)
	{
		std::vector<T>& v = m_vals(m_offsets[i] + j, m_offsets[p] + q);
		for (iterator it = v.begin(); it != v.end(); ++it, ++start)
		{
			(*it) = std::min(*start, upperBound);
		}
	}
	
	/**
	 * Resets all values in entire map to the specified value.
	 * @param defaultVal the value to be set.
	 */
	void reset(const T& defaultVal);
	
	/**
	 * Returns a certain point-to-point value.
	 * @param i epoch index of time 1.
	 * @param j time index in epoch of time 1.
	 * @param k edge index in epoch of time 1.
	 * @param p epoch index of time 2.
	 * @param q time index in epoch of time 2.
	 * @param r edge index in epoch of time 2.
	 * @return the value.
	 */
    inline T& operator() (unsigned i, unsigned j, unsigned k,
    		unsigned p, unsigned q, unsigned r)
    {
    	std::vector<T>& v = m_vals(m_offsets[i] + j, m_offsets[p] + q);
    	return v[k * m_ES[p].getNoOfEdges() + r];
    }
    
    /**
	 * Returns a certain point-to-point value.
	 * @param i epoch index for time 1.
	 * @param j time index in epoch of time 1.
	 * @param k edge index in epoch of time 1.
	 * @param p epoch index of time 2.
	 * @param q time index in epoch of time 2.
	 * @param r edge index in epoch of time 2.
	 * @return the value.
	 */
    inline const T& operator() (unsigned i, unsigned j, unsigned k,
    		unsigned p, unsigned q, unsigned r) const
    {
    	const std::vector<T>& v = m_vals(m_offsets[i] + j, m_offsets[p] + q);
    	return v[k * m_ES[p].getNoOfEdges() + r];
    }
	
    /**
	 * Returns a certain point-to-point value.
	 * @param tm1 the first discretized time.
	 * @param k edge index with respect to tm1.
	 * @param tm2 the second discretized time.
	 * @param r edge index with respect to tm2.
	 * @return the value.
	 */
    inline T& operator() (EpochTime tm1, unsigned k,
    		EpochTime tm2, unsigned r)
    {
    	std::vector<T>& v = m_vals(m_offsets[tm1.first] + tm1.second,
    			m_offsets[tm2.first] + tm2.second);
    	return v[k * m_ES[tm2.first].getNoOfEdges() + r];
    }
    
    /**
	 * Returns a certain point-to-point value.
	 * @param tm1 the first discretized time.
	 * @param k edge index with respect to tm1.
	 * @param tm2 the second discretized time.
	 * @param r edge index with respect to tm2.
	 * @return the value.
	 */
    inline const T& operator() (EpochTime tm1, unsigned k,
    		EpochTime tm2, unsigned r) const
    {
    	const std::vector<T>& v = m_vals(m_offsets[tm1.first] + tm1.second,
    			m_offsets[tm2.first] + tm2.second);
    	return v[k * m_ES[tm2.first].getNoOfEdges() + r];
    }
    
    /**
     * Returns a matrix (concatenated to a vector) of point-to-point
     * values for two specified times.
	 * @param i epoch index for time 1.
	 * @param j time index in epoch for time 1.
	 * @param p epoch index for time 2.
	 * @param q time index in epoch for time 2.
	 * @return the values.
	 */
	inline std::vector<T>& operator() (unsigned i, unsigned j,
			unsigned p, unsigned q)
	{
		return m_vals(m_offsets[i] + j, m_offsets[p] + q);
	}
    
	/**
     * Returns a matrix (concatenated to a vector) of point-to-point
     * values for two specified times.
	 * @param i epoch index for time 1.
	 * @param j time index in epoch for time 1.
	 * @param p epoch index for time 2.
	 * @param q time index in epoch for time 2.
	 * @return the values.
	 */
	inline const std::vector<T>& operator() (unsigned i, unsigned j,
			unsigned p, unsigned q) const
	{
		return m_vals(m_offsets[i] + j, m_offsets[p] + q);
	}
	
	/**
     * Returns a matrix (concatenated to a vector) of point-to-point
     * values for two specified times.
     * @param tm1 the first discretized time.
	 * @param tm2 the second discretized time.
	 * @return the values.
	 */
	inline std::vector<T>& operator() (EpochTime tm1, EpochTime tm2)
	{
		return m_vals(m_offsets[tm1.first] + tm1.second,
				m_offsets[tm2.first] + tm2.second);
	}
	
	/**
     * Returns a matrix (concatenated to a vector) of point-to-point
     * values for two specified times.
     * @param tm1 the first discretized time.
	 * @param tm2 the second discretized time.
	 * @return the values.
	 */
	inline const std::vector<T>& operator() (EpochTime tm1, EpochTime tm2) const
	{
		return m_vals(m_offsets[tm1.first] + tm1.second,
				m_offsets[tm2.first] + tm2.second);
	}
	
	/**
	 * Saves current values in a cache. Cached values can be
	 * restored with a call to restoreCache(), or be disabled
	 * via call to invalidateCache().
	 */
	void cache();
	
	/**
	 * Restores cached values. Has no effect if
	 * invalidateCache() has been invoked since call to cache().
	 */
	void restoreCache();
	
	/**
	 * Disables last made cache.
	 */
	void invalidateCache();
	
	/**
     * Returns a string representation of the map.
     * @return a string representation.
     */
    std::string print() const;
    
    /**
     * Prints the map.
     */
	friend std::ostream& operator<<(std::ostream &o, const EpochPtPtMap<T>& map)
    {
    	return o << map.print();
    }
	
private:
	
	/** Pointer to tree on which map is based. */
	const EpochTree& m_ES;
	
	/** For each epoch, the offset in value matrix with regard to times. */
	std::vector<unsigned> m_offsets;
	
	/** For each time-to-time, the values from points in a concat. matrix. */
	GenericMatrix< std::vector<T> > m_vals;
	
	/** Cached values. */
	GenericMatrix< std::vector<T> > m_cache;
	
	/** Indicates cache validity. */
	bool m_cacheIsValid;
};

// Typedefs. Make sure to 'templatize' them in source file.
typedef EpochPtMap<Real> RealEpochPtMap;
typedef EpochPtPtMap<Real> RealEpochPtPtMap;
typedef EpochPtMap<Probability> ProbabilityEpochPtMap;
typedef EpochPtPtMap<Probability> ProbabilityEpochPtPtMap;

} // end namespace beep

#endif /* EPOCHPTMAPS_HH */
