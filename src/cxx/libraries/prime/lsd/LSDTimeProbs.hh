/* 
 * File:   LSDTimeProbs.hh
 * Author: fmattias
 *
 * Created on December 7, 2009, 12:13 PM
 */

#ifndef _LSDTIMEPROBS_HH
#define	_LSDTIMEPROBS_HH

#include <istream>
#include <utility>

#include "Tree.hh"
#include "Probability.hh"


namespace beep {

class LSDTimeProbs {
   
public:
    typedef std::pair<int, Real> TimePoint;
    typedef std::map<int, Real>::const_iterator LSDTimePointsIterator;

    /**
     * Constructor
     *
     * Creates an object that holds LSDs. LSDs are seen as a map from an edge
     * to a probability and a time point. Currently only one LSD on each edge
     * is supported.
     */
    LSDTimeProbs();

    /**
     * Copy constructor
     *
     * Creates a new object with the same properties as other.
     *
     * other - The new object will have the same properties as other.
     */
    LSDTimeProbs(const LSDTimeProbs& other);

    /**
     * Destructor
     */
    ~LSDTimeProbs();

    /**
     * addTime
     *
     * Adds the possibility of a LSD to the edge.
     *
     * edge - The edge where a LSD will be added.
     * time - The time when the LSD will occur. The time is relative
     *        to the bottom of the edge.
     * lsdProbability - Probability of the LSD occurring.
     */
    void addTime(const Node *edge, Real time, Probability lsdProbability);

    /**
     * Returns true if there is a possible LSD on the specified edge.
     *
     * edge - The edge which will be looked for LSDs.
     */
    bool hasTime(const Node *edge);

    /**
     * Returns the time of the LSD on the specified edge. If no time exists on
     * the edge, a negative number is returned.
     *
     * edge - An edge with a LSD.
     */
    Real getTime(const Node *edge);

    /**
     * Returns the probability of the LSD on the specified edge. If no LSD
     * exists 0 will be returned.
     *
     * edge - An edge with a LSD.
     */
    Probability getProbability(const Node *edge);

    /**
     * Sets the probability of each LSD occurring to p.
     *
     * p - Probability of each LSD occurring.
     */
    void setAll(Probability p);

    /**
     * Returns true if there is a LSD on the specified edge between the
     * relative times (t1, t2). The time is relative from the 'bottom' of the
     * edge, i.e. 0 at the bottom and edge time at the top. The order of t1
     * and t2 does not matter, the interval will be (min(t1,t2), max(t1,t2)).
     *
     * edge - The edge that might have a LSD.
     * t1 - Relative start time of the interval.
     * t2 - Relative end time of the interval.
     */
    bool lsdInInterval(const Node *edge, Real t1, Real t2);

    /**
     * Returns an iterator for the time points of the LSDs.
     */
    LSDTimePointsIterator begin();

    /**
     * Returns the end of an iterator for the time points of the LSDs.
     */
    LSDTimePointsIterator end();

    /**
     * Reads times and probabilities for an LSDTimeProbs object and stores
     * them in the specified LSDTimeProbs.
     *
     * input - Stream to read the times and probabilities from.
     * speciesTree - The species tree in which the times are specified.
     * lsdTimeProbs - The object where the times will be stored.
     */
    static void readFromFile(std::istream &input,
                             const Tree &speciesTree,
                             LSDTimeProbs &lsdTimeProbs);

private:
    // Map from an edge to a time point
    std::map<int, Real> m_lsdTimes;

    // Map from an edge to a probability
    std::map<int, Probability> m_lsdProbs;
};

}

#endif	/* _LSDTIMEPROBS_HH */

