/* Interface class for large scale duplication (LSD) estimators. The purpose
 * of derived classes is to estimate discretization points in an EdgeDiscTree
 * where large scale duplications may have occurred.
 * 
 * File:   LSDEstimator.hh
 * Author: fmattias
 *
 * Created on November 23, 2009, 2:06 PM
 */

#ifndef _LSD_ESTIMATOR_HH
#define	_LSD_ESTIMATOR_HH

#include <vector>

#include "EdgeDiscGSR.hh"
#include "EdgeDiscTree.hh"
#include "TreeDiscretizers.hh"

namespace beep {

class LSDEstimator {
public:

    /**
     * Computes estimations of large scale duplications. The estimates are
     * stored in the supplied LSDestimates vector.
     *
     * integratedModel - The model for computing realization probabilities in a
     *                   discretized species tree.
     * DS - Discretized species tree, must be the same as the one used in
     *      integratedModel.
     * lsdEstimates - An empty vector where the estimates will be stored.
     * iterations - The number of iterations performed by an underlying mcmc.
     */
    virtual void getLSDs(LSDProbs &lsdEstimates, int iterations) = 0;

    virtual ~LSDEstimator()
    {
        
    }
private:

};

}
#endif	/* _LSD_ESTIMATOR_HH */

