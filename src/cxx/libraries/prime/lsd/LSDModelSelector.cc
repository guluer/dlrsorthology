//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   LSDModelSelector.cc
// * Author: fmattias
// *
// * Created on December 1, 2009, 3:53 PM
// */
//#include <cassert>
//
//#include "lsd/LSDModelSelector.hh"
//#include "mcmc/PosteriorSampler.hh"
//namespace beep {
//
//LSDModelSelector::LSDModelSelector()
//{
//
//}
//
//LSDModelSelector::~LSDModelSelector()
//{
//
//}
//
//Probability LSDModelSelector::computeModelProbability(PosteriorSampler &treeLengthThetaMCMC, int iterations, int skip)
//{
//    EdgeDiscGSR *state;
//    SubstitutionMCMC *substitutionMCMC = treeLengthThetaMCMC.currentState();
//
//    Probability modelProbability(0.0);
//    Probability denominator = 1.0/iterations;
//
//    for(int iteration = 0; iteration < iterations; iteration++){
//        std::cout << "Iteration: " << iteration << std::endl;
//        state = treeLengthThetaMCMC.getGSRSampler()->nextState(skip);
//        std::cout << treeLengthThetaMCMC.currentState()->getTree() << std::endl;
//        modelProbability += denominator*substitutionMCMC->calculateDataProbability();
//        std::cout << "Model probability: " << modelProbability << std::endl;
//        std::cout << "Tree probability: " << state->calculateDataProbability() << " " << state->calculateDataProbability().val() << std::endl;
//        std::cout << "Birth rate: " << state->getBDProbs().getBirthRate() << " Death rate: " << state->getBDProbs().getDeathRate() << std::endl;
//        std::cout << "Variance: " << treeLengthThetaMCMC.getGSRSampler()->getDensity()->getVariance() << std::endl;
//        //Real diff = std::abs(state->getTotalPlacementProbability(state->getTree().getRootNode()).val() - state->calculateDataProbability().val());
//        //assert(diff < 1e-9);
//    }
//
//    return modelProbability;
//}
//Real LSDModelSelector::computeBayesFactor(Probability modelZero, Probability modelAlternative)
//{
//    Probability bayesFactor = modelAlternative / modelZero;
//    return bayesFactor.val();
//}
//}
