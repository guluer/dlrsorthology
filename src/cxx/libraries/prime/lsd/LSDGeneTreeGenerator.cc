/* 
 * File:   LSDGeneTreeGenerator.cc
 * Author: fmattias
 * 
 * Created on November 27, 2009, 4:02 PM
 */

#include <cmath>
#include <algorithm>

#include "lsd/LSDGeneTreeGenerator.hh"
#include "PRNG.hh"

namespace beep {

using namespace std;

LSDGeneTreeGenerator::LSDGeneTreeGenerator(Tree &speciesTree,
                                           Real birthRate,
                                           Real deathRate,
                                           LSDTimeProbs &lsdTimes)
                                           : m_birthDeathModel(speciesTree, 
                                                               birthRate,
                                                               deathRate
                                                              ),
                                             m_lsdTimes(lsdTimes),
                                             m_maxTries(DEFAULT_MAX_TRIES),
                                             m_minNumberOfLeaves(DEFAULT_MIN_NUMBER_OF_LEAVES),
                                             m_maxNumberOfLeaves(DEFAULT_MAX_NUMBER_OF_LEAVES)
{
    if(speciesTree.getNumberOfLeaves() < 2) {
        throw AnError("LSDGeneTreeGenerator.constructor: Species tree needs at least 2 leaves.");
    }
}

LSDGeneTreeGenerator::LSDGeneTreeGenerator(const LSDGeneTreeGenerator& other) :
                                            m_randomGenerator(other.m_randomGenerator),
                                            m_birthDeathModel(other.m_birthDeathModel),
                                            m_lsdTimes(other.m_lsdTimes),
                                            m_maxTries(other.m_maxTries),
                                            m_minNumberOfLeaves(other.m_minNumberOfLeaves),
                                            m_maxNumberOfLeaves(other.m_maxNumberOfLeaves)

{
    m_currentGeneID = other.m_currentGeneID;
    m_randomGenerator.setSeed(other.m_randomGenerator.getSeed());
}

void LSDGeneTreeGenerator::generateTree(Tree &geneTree, Real topTime)
{
    Tree &speciesTree = m_birthDeathModel.getStree();
    if(topTime > speciesTree.getTopTime()){
        throw AnError("LSDGeneTreeGenerator.generateTree: Top time of gene "
                      "tree larger than top time of species tree.");
    }

    /* Create tree */
    generateTreeTopology(geneTree, speciesTree, topTime);

    /* Set node times */
    setNodeTimes(geneTree);
}

void LSDGeneTreeGenerator::generateTree(Tree &geneTree)
{
    /* Generate a time for the top edge */
    Real topTime = sampleTimeToDuplication();

    /* If top time is later than the time of the top edge in the species tree,
     * generate gene tree from the root by setting topTime to zero. */
    Tree &speciesTree = m_birthDeathModel.getStree();
    if(topTime >= speciesTree.getTopTime()) {
        topTime = 0.0;
    }

    /* Generate gene tree */
    generateTree(geneTree, topTime);
}

void
LSDGeneTreeGenerator::generateRatesAndLengths(Tree &geneTree, Density2P &rateDensity)
{
    /* Set up rate and length vectors */
    RealVector *rateVector = new RealVector(geneTree);
    RealVector *lengthVector = new RealVector(geneTree);

    /* Intialize iterator */
    Tree::iterator geneTree_it, geneTree_itEnd;
    geneTree_it = geneTree.begin();
    geneTree_itEnd = geneTree.end();

    /* Generate a rate for each node */
    for(; geneTree_it != geneTree_itEnd; ++geneTree_it){
        Node *currentNode = *geneTree_it;

        /* Sample new rate */
        Real uniformRandom = m_randomGenerator.genrand_real1();
        Real rate = rateDensity.sampleValue(uniformRandom);

        (*rateVector)[currentNode] = rate;
        (*lengthVector)[currentNode] = rate*currentNode->getTime();
    }

    /* Associate tree with rates */
    geneTree.setRates(*rateVector, true);
    geneTree.setLengths(*lengthVector, true);
}

StrStrMap
LSDGeneTreeGenerator::exportGeneSpeciesMap()
{
    return m_geneSpeciesMap;
}

vector<Node *>
LSDGeneTreeGenerator::getSpeciations()
{
    return m_speciations;
}

void LSDGeneTreeGenerator::setSeed(unsigned long seed)
{
    m_randomGenerator.setSeed(seed);
}

void LSDGeneTreeGenerator::setMaxNumberOfTries(unsigned int maxNumberOfTries)
{
    m_maxTries = maxNumberOfTries;
}

void LSDGeneTreeGenerator::setMinNumberOfLeaves(unsigned int minNumberOfLeaves)
{
    if(minNumberOfLeaves < 2) {
        throw AnError("LSDGeneTreeGenerator::setMinNumberOfLeaves: Number of "
                      "leaves must be greater than 2.");
    }
    else {
        m_minNumberOfLeaves = minNumberOfLeaves;
    }
}

void LSDGeneTreeGenerator::setMaxNumberOfLeaves(unsigned int maxNumberOfLeaves)
{
    if(maxNumberOfLeaves < m_minNumberOfLeaves) {
        throw AnError("LSDGeneTreeGenerator::setMaxNumberOfLeaves: Maximum "
                      "number of leaves must be greater than minimum number"
                      " of leaves.");
    }
    else {
        m_maxNumberOfLeaves = maxNumberOfLeaves;
    }
}

void
LSDGeneTreeGenerator::resetState()
{
    m_nodeTimes.clear();
    m_geneSpeciesMap.clearMap();
    m_speciations.clear();
    m_currentGeneID = 0;
}

void
LSDGeneTreeGenerator::generateTreeTopology(Tree &geneTree, Tree &speciesTree, Real topTime)
{
    Node *speciesTreeRoot = speciesTree.getRootNode();
    Real timeLeft = speciesTreeRoot->getTime() - topTime;
    assert(timeLeft >= 0);

    Node *leftSubTree = 0;
    Node *rightSubTree = 0;

    /* Loop until we get a gene tree with at least two leaves */
    unsigned int tries;
    for(tries = 0; tries < m_maxTries; tries++){
        /* Delete old sub trees */
        if(leftSubTree != EXTINCT_SUBTREE) {
            leftSubTree->deleteSubtree();
            delete leftSubTree;
        }
        if(rightSubTree != EXTINCT_SUBTREE) {
            rightSubTree->deleteSubtree();
            delete rightSubTree;
        }

        /* Reset class state and remove previous gene topology (if any) */
        resetState();
        geneTree.clear();

        /* Evolve tree */
        if(topTime > 0.0) {
            leftSubTree = evolveInEdge(geneTree, speciesTreeRoot, timeLeft);
            rightSubTree = evolveInEdge(geneTree, speciesTreeRoot, timeLeft);  
        }
        else {
            Node *leftRootChild = speciesTreeRoot->getLeftChild();
            Node *rightRootChild = speciesTreeRoot->getRightChild();

            leftSubTree = evolveInEdge(geneTree, leftRootChild, leftRootChild->getTime());
            rightSubTree = evolveInEdge(geneTree, rightRootChild, rightRootChild->getTime());
        }
        
        bool notExtinct = leftSubTree != EXTINCT_SUBTREE &&
                          rightSubTree != EXTINCT_SUBTREE;

        unsigned int numberOfLeaves = 0;
        if(notExtinct) {
            numberOfLeaves = leftSubTree->getNumberOfLeaves() +
                             rightSubTree->getNumberOfLeaves();
        }
        
        bool correctNumberOfLeaves = numberOfLeaves >= m_minNumberOfLeaves &&
                                     numberOfLeaves <= m_maxNumberOfLeaves;

        /* If both sub trees are alive and we got an allowed number of leaves
         * then we are done. */
        if(notExtinct && correctNumberOfLeaves) {
            break;
        }
    }

    if(tries < m_maxTries) {
        /* A gene tree was generated in less than m_maxTries */
        Node *root = geneTree.addNode(leftSubTree, rightSubTree);
        if(topTime > 0.0) {
            setNodeTime(root, speciesTreeRoot->getNodeTime() + speciesTreeRoot->getTime() - topTime);
        }
        else {
            setNodeTime(root, speciesTreeRoot->getNodeTime());
        }
        geneTree.setRootNode(root);
    }
    else {
        /* No gene tree was generated in m_maxTries */
        ostringstream errorMessage;
        errorMessage << "LSDGeneTreeGenerator.generateTreeTopology(): "
                      "No gene tree with more than two leaves was generated in "
                      << m_maxTries << " tries, please try to change the "
                      "birth-death, leaves and tries parameters.";
        throw AnError(errorMessage.str());
    }
}

Node *
LSDGeneTreeGenerator::evolveInEdge(Tree &geneTree, Node *x, Real edgeTimeLeft)
{
    /* Generate edge time */
    Real edgeTime = sampleTimeToDuplication();
    assert(edgeTime > 0);

    /* Determine if gene survives to the next event (duplication or speciation) */
    Real survivalTime = std::min<Real>(edgeTime, edgeTimeLeft);

    /* Does our new edge stretch over a duplication site */
    if(overDuplication(x, edgeTimeLeft, survivalTime) && generateLSD(x)){
        edgeTime = timeToLSD(x, edgeTimeLeft);
        survivalTime = timeToLSD(x, edgeTimeLeft);
    }

    if(becomesExtinctIn(survivalTime)) {
        return EXTINCT_SUBTREE;
    }

    Node *leftSubTree = 0;
    Node *rightSubTree = 0;
    Node *subTreeRoot = 0;
    Real nodeTime = 0.0;

    /* If time does not exceed max time */
    if(edgeTime < edgeTimeLeft) {
        /* Recursively create children and add them if result is not zero */
        leftSubTree = evolveInEdge(geneTree, x, edgeTimeLeft - edgeTime);
        rightSubTree = evolveInEdge(geneTree, x, edgeTimeLeft - edgeTime);

        /* Time of the duplication */
        nodeTime = x->getNodeTime() + edgeTimeLeft - edgeTime;
        subTreeRoot = createSubTree(geneTree, leftSubTree, rightSubTree, nodeTime);
    }
    else {
        /* Gene survies to speciation */
        if(!x->isLeaf()) {
            /* Create new processes in the two child edges */
            Node *xLeftChild = x->getLeftChild();
            Node *xRightChild = x->getRightChild();

            leftSubTree = evolveInEdge(geneTree, xLeftChild, xLeftChild->getTime());
            rightSubTree = evolveInEdge(geneTree, xRightChild, xRightChild->getTime());

            /* If both lineages are alive, it is a speciation, so the node time
             * is the same as the speciation time */
            nodeTime = x->getNodeTime();
            subTreeRoot = createSubTree(geneTree, leftSubTree, rightSubTree, nodeTime);

            /* Add speciation */
            if(leftSubTree != EXTINCT_SUBTREE && rightSubTree != EXTINCT_SUBTREE) {
                m_speciations.push_back(subTreeRoot);
            }
        }
        else {
            /* Return a new leaf node */
            string leafName = getLeafName();
            Node *leaf = geneTree.addNode(NULL, NULL, leafName);
            m_geneSpeciesMap.insert(leafName, x->getName());

            /* Node time is the same as the corresponding speciation */
            setNodeTime(leaf, x->getNodeTime());

            subTreeRoot = leaf;
        }
    }

    /* Return the root of the created sub tree */
    return subTreeRoot;
}

Node *
LSDGeneTreeGenerator::createSubTree(Tree &geneTree, Node *leftSubTree, Node *rightSubTree, Real nodeTime)
{
    Node *returnNode;

    if(leftSubTree != EXTINCT_SUBTREE && rightSubTree != EXTINCT_SUBTREE) {
        /* Both sub trees are alive, create their parent */
        Node *subTreeRoot = geneTree.addNode(leftSubTree, rightSubTree);
        setNodeTime(subTreeRoot, nodeTime);
        returnNode = subTreeRoot;
    }
    else if(leftSubTree != EXTINCT_SUBTREE) {
        /* Right sub tree is extinct, ignore it */
        returnNode = leftSubTree;
    }
    else if(rightSubTree != EXTINCT_SUBTREE) {
        /* Left sub tree is extinct, ignore it */
        returnNode = rightSubTree;
    }
    else {
        /* Both sub trees are extinct */
        returnNode = EXTINCT_SUBTREE;
    }
    return returnNode;
}

void
LSDGeneTreeGenerator::setNodeTime(Node *u, Real nodeTime)
{
    assert(nodeTime >= 0);
    m_nodeTimes[u] = nodeTime;
}

Real
LSDGeneTreeGenerator::getNodeTime(Node *u)
{
    return m_nodeTimes[u];
}

void
LSDGeneTreeGenerator::setNodeTimes(Tree &geneTree)
{
    RealVector *timeVector = new RealVector(geneTree);

    /* Intialize iterator */
    Tree::iterator geneTree_it, geneTree_itEnd;
    geneTree_it = geneTree.begin();
    geneTree_itEnd = geneTree.end();

    /* Assign time to each node in a time vector */
    for(; geneTree_it != geneTree_itEnd; ++geneTree_it){
        Node *currentNode = *geneTree_it;

        (*timeVector)[currentNode] = m_nodeTimes[currentNode];
    }
    
    geneTree.setTimes(*timeVector, true);
}

string
LSDGeneTreeGenerator::getLeafName()
{
    stringstream leafName;
    leafName << "Gene" << m_currentGeneID;
    m_currentGeneID++;

    return leafName.str();
}

Real
LSDGeneTreeGenerator::sampleTimeToDuplication()
{
    Real bdDiff = m_birthDeathModel.getBirthRate();

    // Sample from exp(\lambda - \mu) by the inverse sampling method
    Real p = m_randomGenerator.genrand_real1();
    Real edgeTime = -std::log(1 - p) /  bdDiff;
    return edgeTime;
}

bool
LSDGeneTreeGenerator::becomesExtinctIn(Real edgeTime)
{
    Probability p = m_randomGenerator.genrand_real1();

    // Pr[X < edgeTime] where X \in Exp(\mu), i.e. the probability of the
    // death occuring before the birth.
    Probability extinctionProbability = 1 - std::exp(-edgeTime*m_birthDeathModel.getDeathRate());
    //return p <= m_birthDeathModel.extinctionProbability(edgeTime);
    return p <= extinctionProbability;
}

bool
LSDGeneTreeGenerator::overDuplication(Node *x, Real timeLeft, Real edgeTime)
{
    return m_lsdTimes.lsdInInterval(x, timeLeft, timeLeft - edgeTime);
}

Real LSDGeneTreeGenerator::timeToLSD(Node *x, Real timeLeft)
{
    if(m_lsdTimes.hasTime(x)){
        return timeLeft - m_lsdTimes.getTime(x);
    }
    else {
        throw AnError(" LSDGeneTreeGenerator::timeToDuplication(): "
                      "Duplication not found.");
    }
}

bool
LSDGeneTreeGenerator::generateLSD(Node *x)
{
    Real p = m_randomGenerator.genrand_real1();
    return p <= m_lsdTimes.getProbability(x).val();
}

LSDGeneTreeGenerator::~LSDGeneTreeGenerator() {
}

}

