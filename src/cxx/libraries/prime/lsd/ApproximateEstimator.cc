//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   ApproximateEstimator.cc
// * Author: fmattias
// *
// * Created on November 23, 2009, 2:39 PM
// */
//#include <ostream>
//#include <time.h>
//
//#include "Hacks.hh"
//#include "lsd/ApproximateEstimator.hh"
//#include "EdgeDiscPtMapIterator.hh"
//#include "TreeIO.hh"
//#include "EdgeDiscPtKeyIterator.hh"
//#include "io/LSDEstimatesFormat.hxx"
//#include "TimeEstimator.hh"
//#include "mcmc/PosteriorSampler.hh"
//#include "LogFilePrinter.hh"
//
//namespace beep {
//
//using namespace std;
//
//ApproximateEstimator::ApproximateEstimator(EdgeDiscTree &speciesTree,
//                                           PosteriorSampler &mcmc,
//                                           int burnInIterations,
//                                           int fixedIterations,
//                                           int mapIterations) :
//                                           m_speciesTree(speciesTree),
//                                           m_mcmc(mcmc),
//                                           m_discPointScore(speciesTree, 0),
//                                           m_doBurnIn(burnInIterations > 0),
//                                           m_burnInIterations(burnInIterations),
//                                           m_fixedIterations(fixedIterations),
//                                           m_mapIterations(mapIterations)
//{
//    // Empty
//}
//
//ApproximateEstimator::~ApproximateEstimator()
//{
//
//}
//
//void ApproximateEstimator::getLSDs(LSDProbs &lsdEstimates, int integrationIterations)
//{
//    /* Set up vector for storing score of each discretization point */
//    m_discPointScore.reset(0);
//
//    /* Check wether to do mcmc burn in first */
//    Tree mapTree;
//    if(m_doBurnIn){
//        std::cout << "Burn in" << std::endl;
//        m_mcmc.nextStateWithTimeLeft(m_burnInIterations);
//
//        /* Fix gene tree since we will be integrating over the lengths */
//        std::cout << "Finding MAP" << std::endl;
//        getBestGeneTree(m_mcmc.getMCMC(), mapTree);
////        findMAPTree(mapTree, m_mapIterations);
////        std::cout << "ALERT NOT Fixing tree" << std::endl;
//
//        m_mcmc.getGSRSampler()->fixGeneTree(mapTree);
//        m_mcmc.currentState()->update();
//        m_mcmc.currentState()->updateDataProbability();
////        m_mcmc.fixBDParameters();
//
//        /* Do some extra burn in iterations for the fixed tree */
////        m_mcmc.nextStateWithTimeLeft(m_fixedIterations);
//        m_doBurnIn = false;
//    }
//
//    ostringstream oss;
//    oss << "# L N " << m_mcmc.currentState()->strHeader() << endl;
//    LogFilePrinter::getLogFilePrinter()->printEntry(oss.str());
//
//    /* Integrate out gene tree for all discretization points */
//    std::cout << "Integrating out branch lengths" << std::endl;
//    for(int i = 0; i < integrationIterations; i++) {
//        EdgeDiscGSR *state = m_mcmc.getGSRSampler()->nextStateWithTimeLeft(SAMPLE_INTERVAL);
//        Real convergence = updatePointScore(m_discPointScore, *state, i+1);
//
//        /* Log state */
//        LogFilePrinter::getLogFilePrinter()->printEntry(m_mcmc.currentState()->strRepresentation());
//        ostringstream oss;
//        oss << "Total convergence: " << convergence << endl;
//        LogFilePrinter::getLogFilePrinter()->printEntry(oss.str());
//    }
//
//    // Set up iterator for discretization points
//    EdgeDiscPtKeyIterator<Real> speciesTree_it = m_speciesTree.beginKey();
//    EdgeDiscPtKeyIterator<Real> speciesTree_itEnd = m_speciesTree.endKey();
//    EdgeDiscretizer::Point x;
//
//    Real max = 0.0;
//    EdgeDiscretizer::Point maxPoint;
//
//    /* Find discretization point with the highest score and take the mean discpoint score */
//    for(; speciesTree_it != speciesTree_itEnd; speciesTree_it++){
//        x = *speciesTree_it;
//        if(m_discPointScore(x) > max) {
//            max = m_discPointScore(x);
//            maxPoint = x;
//        }
//
//        /* Take the mean score */
//        m_discPointScore(x) = m_discPointScore(x) / integrationIterations;
//    }
//
//    /* Save element with the highest score */
//    lsdEstimates.addPoint(maxPoint, 0.0);
//}
//
//Real ApproximateEstimator::updatePointScore(EdgeDiscPtMap<Real> &discPointScore,
//                                            EdgeDiscGSR &state,
//                                            int iteration)
//{
//    EdgeDiscPtKeyIterator<Real> speciesTree_it = m_speciesTree.beginKey();
//    EdgeDiscPtKeyIterator<Real> speciesTree_itEnd = m_speciesTree.endKey();
//
//    EdgeDiscretizer::Point x;
//
//    Real totalConvergence = 0.0;
//
//    for(; speciesTree_it != speciesTree_itEnd; speciesTree_it++)
//    {
//        x = *speciesTree_it;
//
//        /* Ignore last point of the root edge */
//        if(x.first->isRoot() && x.second == m_speciesTree.getNoOfPts(x.first) - 1) {
//            continue;
//        }
//
//        /* We are only interested in duplications */
//        if(!m_speciesTree.isSpeciation(x)){
//            // sum_{u \in G} Pr[ u on x | G, l]
//            Probability uxProbability = state.getTotalPlacementSum(&x);
//
//            // d(x) = sum_{i = 1}^n sum_{u \in G} Pr[ u on x | G, l_i]
//            discPointScore(x) += uxProbability.val();
//            if(iteration > 1) {
//                totalConvergence += std::abs(discPointScore(x) / (iteration - 1) - (discPointScore(x) + uxProbability.val()) / iteration);
//            }
//        }
//    }
//
//    return totalConvergence;
//}
//
//void ApproximateEstimator::findMAPTree(Tree &geneTree, int map_iterations){
//
//    Probability map(0.0);
//    Tree mapTree;
//
//    // Find MAP estimate by sampling gene trees
//    for(int i = 0; i < map_iterations; i++) {
//        m_mcmc.nextState(1);
//        EdgeDiscGSR *currentState = m_mcmc.getGSRSampler()->currentState();
//        Probability stateProbability = m_mcmc.currentState()->calculateDataProbability();
//
//        // Current state has a larger probability than map.
//        if(stateProbability > map) {
//            map = stateProbability;
//            mapTree = currentState->getTree();
//        }
//
//
//    }
//    TimeEstimator::instance()->update(map_iterations);
//    geneTree = mapTree;
//}
//
//void ApproximateEstimator::getBestGeneTree(SimpleMCMC *mcmc, Tree &geneTree) {
//    string bestState = mcmc->getBestState();
//
//    /* The gene tree is the second parameter, so we will try to find */
//    int treeStart = -1;
//    int treeLength = -1;
//    int occurence = 0;
//    for(unsigned int position = 0; position < bestState.size(); position++) {
//        if(bestState.at(position) == ';') {
//            occurence++;
//
//            /* Genee tree is the second parameter */
//            if(occurence == 2) {
//                treeStart = position + 1;
//            }
//            if(occurence == 3) {
//                treeLength = position - treeStart;
//                break;
//            }
//        }
//    }
//
//    /* Find the tree sub string */
//    string bestTree = bestState.substr(treeStart, treeLength);
//
//    /* Read the gene tree */
//    TreeIOTraits traits;
//    traits.setBL(true);
//    TreeIO geneTreeStringReader(TreeIO::fromString(bestTree));
//    geneTree = geneTreeStringReader.readBeepTree(NULL, NULL);
//}
//
//EdgeDiscPtMap<Real> *
//ApproximateEstimator::getDUPDensity()
//{
//    return &m_discPointScore;
//}
//
//void ApproximateEstimator::writeToXML(ostream &output)
//{
//    auto_ptr<LSDEstimatesType> xmlOutput(new LSDEstimatesType());
//
//    Tree::const_iterator speciesTree_it = m_speciesTree.getTree().begin();
//    Tree::const_iterator speciesTree_itEnd = m_speciesTree.getTree().end();
//
//    for(; speciesTree_it != speciesTree_itEnd; speciesTree_it++)
//    {
//        Node *currentEdge = *speciesTree_it;
//
//        Real edgeTime = currentEdge->getTime();
//        Real startTime = currentEdge->getNodeTime();
//
//        edgeType xmlEdge(currentEdge->getNumber(), edgeTime, startTime);
//
//        int numberOfPoints = m_speciesTree.getNoOfPts(currentEdge);
//        for(int discPoint = 0; discPoint < numberOfPoints; discPoint++){
//            EdgeDiscretizer::Point x(currentEdge, discPoint);
//
//            exDupType xmlExDup(m_discPointScore(x),
//                               discPoint,
//                               m_speciesTree(x) - startTime);
//            xmlEdge.exDup().push_back(xmlExDup);
//        }
//        xmlOutput->edge().push_back(xmlEdge);
//    }
//
//    xml_schema::namespace_infomap map;
//    map[""].name = "";
//    map[""].schema = "http://www.nada.kth.se/~fmattias/LSDEstimatesFormat.xsd";
//
//    LSDEstimates(output, *xmlOutput, map);
//}
//
//} /* Namespace end */

