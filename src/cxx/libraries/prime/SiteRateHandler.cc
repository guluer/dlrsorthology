#include <cassert>
#include <cassert>
#include <sstream>


#include "DiscreteGamma.hh"
#include "SiteRateHandler.hh"

//----------------------------------------------------------------------
//
// SiteRateHandler
//
//----------------------------------------------------------------------


namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Formal defs of static const members
  // I haven't found where this is stated in the standard, but
  // apparently we now need to do this? /bens
  //
  //----------------------------------------------------------------------
  const double SiteRateHandler::MAX;

  //----------------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //----------------------------------------------------------------------
    
  // Assumes no rate variation
  //----------------------------------------------------------------------
  SiteRateHandler::SiteRateHandler(unsigned ncats, EdgeRateModel& alpha_in)
    : alpha(&alpha_in), 
      siteRates(ncats)
  {
    update();
  }
    
  SiteRateHandler::SiteRateHandler(const SiteRateHandler& srm)
    : alpha(srm.alpha),
      siteRates(srm.siteRates)
  {
  }
    
  SiteRateHandler::~SiteRateHandler() 
  {
  };
    
  SiteRateHandler&
  SiteRateHandler::operator=(const SiteRateHandler& srm)
  {
    if(this != &srm)
      {
	alpha = srm.alpha;
	siteRates = srm.siteRates;
      }
    return *this;
  }
    
  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
  void 
  SiteRateHandler::update()
  {
    // Currently, we only use a ConstRateModel, but for generality to come
    // we simulate using node-specific alpha.
    // TODO: fix this nicer! /bens
    Real a = alpha->getRate(0);
    siteRates = getDiscreteGammaClasses(siteRates.size(), a, a);
  }

  // returns number of rate for categories
  //----------------------------------------------------------------------
  unsigned 
  SiteRateHandler::nCat() const
  {
    return siteRates.size();
  }

  // returns value of rate for category rate_cat
  // TODO: Should eventually take a node argument as well /bens
  //----------------------------------------------------------------------
  Real 
  SiteRateHandler::getRate(const unsigned& rate_cat) const
  {
    assert(rate_cat < siteRates.size());
    return siteRates[rate_cat];
  }

  // TODO: NOT a bool! Should also take a node argument eventually /bens
  bool 
  SiteRateHandler::setAlpha(const Real& new_alpha)
  {
    if(new_alpha > MAX)
      return false;

    // Currently, we only use a ConstRateModel, but for generality to come
    // we simulate using node-specific alpha.
    const Node* dummy = alpha->getTree().getRootNode(); 
    alpha->setRate(new_alpha, dummy);
    update();
    return true;
  }

  // TODO: Should also take a node argument eventually /bens
  Real 
  SiteRateHandler::getAlpha()
  {
    return alpha->getRate(0);
  }

  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
    
  // Always define an ostream operator!
  //------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const SiteRateHandler& srm)
  {
    return o << srm.print()
      ;
  }

  std::string SiteRateHandler::print() const 
  {
    std::ostringstream oss;
    oss 

      << "Site specific rates are modeled over "
      << nCat()
      << " categories by an \n"
      << "underlying Gamma distribution with a shape parameter \n";
    // TODO: Fix this nicer! /bens
    const Node* dummy = alpha->getTree().getRootNode();
    oss << "fixed to "
	  << alpha->getRate(dummy)
	  << ".\n";
    return oss.str();
  };

}//end namespace beep
