#ifndef DENSITY2PMCMC_HH
#define DENSITY2PMCMC_HH

#include "StdMCMCModel.hh"
#include "Density2P.hh"


namespace beep
{
  //----------------------------------------------------------------
  //
  //! Subclass to StdMCMCModel - responsible for perturbing mean and
  //! variance of Density2P. By setting the flag perturbCV=true one
  //! will perturb the coefficient of variation (CV) instead of the
  //! variance.
  //
  //----------------------------------------------------------------
  class Density2PMCMC : public StdMCMCModel
  {
  public:
    //----------------------------------------------------------------
    //
    //! Construct/destruct/assign
    //
    //----------------------------------------------------------------
    Density2PMCMC(MCMCModel& prior, Density2P& d, bool doPerturbCV = true);
    Density2PMCMC(const Density2PMCMC& dm);
    ~Density2PMCMC();
    Density2PMCMC& operator=(const Density2PMCMC& dm);

    //----------------------------------------------------------------
    //
    //! Interface
    //
    //----------------------------------------------------------------
    //! \name
    //! @{
    //! Fix parameters -- note that when building a chain of 
    //! MCMC-classes, these functions must be called before 
    //! constructing the next MCMC class
    //----------------------------------------------------------------
    void fixMean();
    void fixVariance();
    //! @}      
    Density2P& getModel();

    //----------------------------------------------------------------------
    // Inherited from StdMCMCModel
    //----------------------------------------------------------------------

    // Perturbs with equal probability either the mean or the variance of 
    // underlying density or one of the edge rates.
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    std::string getAcceptanceInfo() const;
    Probability updateDataProbability();
    //! Used in parallel implementation of mpiwaladen, when updating 
    //! slave chains to perturbations in master chain
    void updateToExternalPerturb(Real newMean, Real newVariance);

    //----------------------------------------------------------------------
    //
    // I/O
    //
    //----------------------------------------------------------------------
    std::string print() const;

    //----------------------------------------------------------------------
    //
    // Attributes
    //
    //----------------------------------------------------------------------
  protected:
    Density2P* density;
    bool perturbCV;     //!< True if to perturb coefficient of variation (CV) rather than variance.
    Real oldValue;
    Real idx_limits;
    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do 
    //! not know the location/scale of the distribution ahead of time. As a
    //! fix, we store a variance initiated from the start values of dupl/loss
    //! rates.
    Real suggestion_variance;

    unsigned whichParam;                         //!< Flag for curr. perturbed param.
    std::pair<unsigned,unsigned> p1AccPropCnt;   //!< First param acc. and tot. prop. count.
    std::pair<unsigned,unsigned> p2AccPropCnt;   //!< Second param acc. and tot. prop. count.
  };
} // end namespace beep

#endif
