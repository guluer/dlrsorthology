#include "HybridTreeIO.hh"

#include "AnError.hh"
#include "GammaMap.hh"
#include "HybridTree.hh"
#include "NHXannotation.h"
#include "Node.hh"
#include "StrStrMap.hh"


#include <cassert>		// For early bug detection

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <vector>
#include <cmath>
#include <algorithm>

namespace beep
{
  using namespace std;

  //--------------------------------------------------------------------
  //
  // Constructors
  // 
  //--------------------------------------------------------------------

  // Only create TreeIO objects through *named constructors* (see C++ FAQ 
  // lite). Actual constructor is protected, and does actually not do 
  // anything!
  HybridTreeIO::HybridTreeIO(enum TreeSource source, const string s)
    : TreeIO(source, s)
  {}

  HybridTreeIO::HybridTreeIO(const TreeIO& io)
    : TreeIO(io)
  {}

  // Update: Since I sometimes want to output trees without reading 
  // anything, I will now allow instantiating the empty object.
  //--------------------------------------------------------------------
  HybridTreeIO::HybridTreeIO()
    : TreeIO()
  {}

  HybridTreeIO::~HybridTreeIO()
  {}

  HybridTreeIO::HybridTreeIO(const HybridTreeIO& io)
    : TreeIO(io)
  {}

  HybridTreeIO&
  HybridTreeIO::operator=(const HybridTreeIO& io)
  {
    if(&io != this)
      {
	TreeIO::operator=(io);
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------
 
  // Named constructors! 
  // Usage: 
  //   TreeIO io = fromFile("Nisse");
  //--------------------------------------------------------------------
//   HybridTreeIO
//   HybridTreeIO::fromFile(const string &f)
//   {
//     return TreeIO::fromFile(f);
//   }


//   HybridTreeIO
//   HybridTreeIO::fromString(const string &s)
//   {
//     return TreeIO::fromString(s);
//   }


  //----------------------------------------------------------------------
  // Reading trees:
  //----------------------------------------------------------------------

  HybridTree
  HybridTreeIO::readHybridTree(TreeIOTraits traits,
			 vector<SetOfNodes> *AC, 
			 StrStrMap *gs)
  {
    struct NHXtree* t = readTree();
    HybridTree T;
    // Create BeepVectors to hold required 'tag' info
    traits.setHY(true);
    if(traits.hasET() || traits.hasNT())
      {
	T.setTimes(*new RealVector(treeSize(t)), true);
      }
    if(traits.hasBL())
      {
	T.setLengths(*new RealVector(treeSize(t)), true);
      }
    
    // Convert it into our preferred C++ data structure
    Node *r = TreeIO::extendBeepTree(T, t->root, traits, AC, gs, 
				     T.getOPAttribute(), T.getEXAttribute());
    if (r == NULL) 
      {
	throw AnError("The input tree was empty!");
      }

    // Trees are allowed to have a name annotation at the root.
    // So a name attribute at the root node is taken as the name
    // attribute of the tree. Other name attributes are at this 
    // point ignored.
    if (struct NHXannotation *a = find_annotation(t->root, "NAME"))
      {
	string str = a->arg.str;
	T.setName(str);
      }

    // Set Top Time if requested and available
    // if useET then ET handles topTime
    if(traits.hasNT())
      {
	if(struct NHXannotation *a = find_annotation(t->root, "TT"))
	  {
	    Real toptime = a->arg.t;
	    T.setTopTime(toptime);
	  }
      }

    // Loose temp structure and hand the root in a good place.
    delete_trees(t);
    T.setRootNode(r);
    if(T.IDnumbersAreSane(*r) == false)
      {
	throw AnError("There are higher ID-numbers than there are nodes in tree",
		      "TreeIO::readHybridTree");
      }

    return T;
  }
 
  HybridTree
  HybridTreeIO::readHybridTree()
  {
    TreeIOTraits traits;
    checkTagsForTree(traits);	// Hey, you are reading the tree here as well!!! /arve
    if(traits.containsTimeInformation() == false)
      {
	throw AnError("Host tree lacks time information for some of it nodes", 1);
      }
    traits.enforceStandardSanity();

    return readHybridTree(traits, 0, 0);
  }


  vector<HybridTree>
  HybridTreeIO::readAllHybridTrees(TreeIOTraits traits,
			     std::vector<SetOfNodes> *AC, //meaningless?
			     std::vector<StrStrMap>* gs)
  {
    assert(AC == 0 && gs == 0); // disable uncertain functionality
    vector<HybridTree> GV;	       
    struct NHXtree* T = readTree();
    struct NHXtree* Ti = T;
    traits.setHY(true);

    if (T == 0)
      { 
	throw AnError("The input gene tree was empty!");
      }

    int i=0;
    while (Ti) 
      {
	HybridTree G;
	StrStrMap* gsi = NULL; // disable uncertain functionality
	
	// Create BeepVectors to hold required 'tag' info
	if(traits.hasET() || traits.hasNT())
	  {
	    G.setTimes(*new RealVector(treeSize(Ti)),true);
	  }
	if(traits.hasBL())
	  {
	    G.setLengths(*new RealVector(treeSize(Ti)),true);
	  }

	Node* r;
	r = TreeIO::extendBeepTree(G, Ti->root, traits,
 				   AC, gsi, 
				   G.getOPAttribute(), G.getEXAttribute());

	if (struct NHXannotation *a = find_annotation(Ti->root, "NAME"))
	  {
	    string s(a->arg.str);
	    G.setName(s);
	  }
	else
	  {
	    ostringstream oss;
	    oss << "G" << i;
	    G.setName(oss.str());
	  }

	// Set Top Time if requested and available
	// if useET then ET handles topTime
	if(traits.hasNT())
	  {
	    if(struct NHXannotation *a = find_annotation(Ti->root, "TT"))
	      {
		Real toptime = a->arg.t;
		G.setTopTime(toptime);
	      }
	  }
	
	G.setRootNode(r);
	if(G.IDnumbersAreSane(*r) == false)
	  {
	    throw AnError("There are higher ID-numbers than there are nodes in tree",
			  "TreeIO::ReadBeepTree");
	  }
// 	cerr << "New Treee:\n" << G <<endl;
	GV.push_back(G);
	if(gs)
	  {
	    gs->push_back(*gsi);
	  }
	Ti = Ti->next;
	i++;
      }

    delete_trees(T);
    // Trees are in reversed order in NHXtree, we need to fix that
    reverse(GV.begin(), GV.end());
    return GV;


//     vector<HybridTree> GV2;
//     for(unsigned i = 1; i <= GV.size(); i++)
//       {
// 	GV2.push_back(GV[GV.size()-i]);
//       }
//     return GV2;
  }


  // Convenience front to readAllBeepTrees(...)
  // Reads 'NW tags' as edge times and nothing more
  //----------------------------------------------------------------------
  vector<HybridTree>
  HybridTreeIO::readAllHybridTrees(std::vector<StrStrMap>* gs)
  {
    TreeIOTraits traits;
    traits.setHY(true);
    traits.setNT(true);
    return readAllHybridTrees(traits, 0, gs);
  }

  //----------------------------------------------------------------------
  // Writing trees
  //----------------------------------------------------------------------

  string 
  HybridTreeIO::writeHybridTree(const HybridTree& T, 
			  const TreeIOTraits traits,
			  const GammaMap* gamma)
  {
    TreeIOTraits localtraits(traits);
    localtraits.setID(false);
    ostringstream name;

    if (localtraits.hasName()) {
      name << "[&&PRIME NAME=" << T.getName();
      if(T.getRootNode() == NULL)
	{
	  name << "] [empty tree]";
	  return name.str();
	}
      else 
	{
	  if(localtraits.hasNT())
	    {
	      name << " TT=" << T.getTopTime();
	    }
	  name << "]";
	}
    }
    map<unsigned, unsigned> id;
//     string least = "";
    map<Node*, string> least;
    return recursivelyWriteBeepTree(*T.getRootNode(), least, localtraits,
				    gamma, T.getOPAttribute(), 
				    T.getEXAttribute(), &id) + name.str();
  }

  // convenience front function for writeHybridTree(...) 
  // writes tree T with edge times
  //----------------------------------------------------------------------
  string 
  HybridTreeIO::writeHybridTree(const HybridTree& S)
  {
    TreeIOTraits traits;
    traits.setID(true);
    traits.setET(true);
    return writeHybridTree(S, traits, 0);
  }


}//end namespace beep
