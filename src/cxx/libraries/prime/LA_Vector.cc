#include <cassert>
#include <string>
#include <sstream>

#include "AnError.hh"
#include "beep2blas.hh" 
#include "LA_Vector.hh" 

namespace beep {
  
  //public:
  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------
  LA_Vector::LA_Vector(const unsigned& dim)
    : dim(dim),
      data(new Real[dim])
  {
    for(unsigned i = 0; i < dim; i++)
      {
	data[i] = 0;
      }
  };

  // All elements are intitiated to init
  LA_Vector:: LA_Vector(const unsigned& dim, const Real& init)
    : dim(dim),
      data(new Real[dim])
    {
      for(unsigned i = 0; i < dim; i++)
	{
	  data[i] = init;
	}
    };


  LA_Vector::LA_Vector(const unsigned& dim, const Real in_data[])
    : dim(dim),
      data(new Real[dim])
  {
    // Using blas vector copy
    ACC_PREFIX(copy_)(dim, in_data, 1, data, 1);
  };


  LA_Vector::LA_Vector(const LA_Vector& B)
    : dim(B.dim),
      data(new Real[B.dim])
  {
    // Using blas vector copy
    ACC_PREFIX(copy_)(dim, B.data, 1, data, 1);
  };


  LA_Vector::~LA_Vector()
  {
    delete[] data;
  };


  LA_Vector& 
  LA_Vector::operator=(const LA_Vector& B)
  {
    if(this != &B)
      {
	if(dim != B.dim)
	  {
	    throw AnError("LA_MAtrix::operator=:"
			  "dimension don't fit between matrices");;	 
	  }
	// Using blas vector copy
	ACC_PREFIX(copy_)(dim, B.data, 1, data, 1);
      }
    return *this;
  };


  bool
  LA_Vector::operator!=(const LA_Vector& B) const 
  {
    if(this != &B)
      {
	for(unsigned i = 0; i < dim; i++)
	  {
	    if(data[i] != B.data[i])
	      return true;
	  }
      }
    return false;
  };


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------
  
  // Dimension of Vector
  //------------------------------------------------------------------------
  const unsigned& 
  LA_Vector::getDim() const
  {
    return dim;
  };

  // Access to individual elements
  //------------------------------------------------------------------------
  Real& 
  LA_Vector::operator[](const unsigned& elem)
  {
    return data[elem];
  }

  const Real 
  LA_Vector::operator[](const unsigned& elem) const
  {
    return data[elem];
  }

  // (Re)set all elements
  //------------------------------------------------------------------------
  void 
  LA_Vector::setAllElements(Real value)
  {
    for(unsigned i = 0; i < dim; i++)
      {
	data[i] = value;
      }
  };    

  // Addition of vectors x and y using BLAS function
  //   void daxpy_(const int& dim, const double& alpha,  
  //               const double x[],  const int& stridex,
  // 	             const double y[], const int& stridy)
  // which performs y = alpha * x + y,
  // where alpha is a scalar that multiplies x,
  // Vector x (the argument) and y (a copy of this that also will 
  // hold the answer) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Vector::operator+(const LA_Vector& x) const
  {
    assert(x.dim == dim);
    LA_Vector y(x);

    beep::ACC_PREFIX(axpy_)(dim, 1, data, 1, y.data, 1);
    return y;
  };


  // dot? product of vectors x and scalar alpha using BLAS function
  //   void dscal_(const int& dim, const double alpha, 
  //               const double x[],  const int& stridex)
  // which performs x = alpha * x,
  // alpha is a scalar
  // Vector x (a copy of this that also will hold the answer) has 
  // dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Vector::operator*(const Real& alpha) const
  {
    LA_Vector x(*this);
    beep::ACC_PREFIX(scal_)(dim, alpha, x.data, 1);
    return x;
  };

  //friend function
  LA_Vector 
  operator*(const Real& alpha, const LA_Vector& x)
  {
    LA_Vector y(x);
    beep::ACC_PREFIX(scal_)(y.getDim(), alpha, y.data, 1);
    return y;
  };


  // dot? ratio of vectors x and scalar alpha using BLAS function
  //   void dscal_(const int& dim, const double 1/alpha, 
  //               const double x[],  const int& stridex)
  // which performs x = alpha * 1/x,
  // alpha is a scalar
  // Vector x (a copy of this that also will hold the answer) has 
  // dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Vector::operator/(const Real& alpha) const
  {
    LA_Vector x(*this);
    beep::ACC_PREFIX(scal_)(dim, 1/alpha, x.data, 1);
    return x;
  };


  LA_Vector 
  operator/(const Real& alpha, const LA_Vector& x)
  {
    LA_Vector y(x);
    beep::ACC_PREFIX(scal_)(y.getDim(), 1/alpha, y.data, 1);
    return y;
  };


  // dot product of vectors x and y using BLAS function
  //   void ddot_(const int& dim,  
  //              const double x[],  const int& stridex,
  // 	            const double y[], const int& stridy)
  // which performs y = atranspose(x) * y,
  // Vector x (the argument) and y (a copy of this that also will 
  // hold the answer) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  Real 
  LA_Vector::operator*(const LA_Vector& x) const
  {
    assert(x.dim == dim);
    return beep::ACC_PREFIX(dot_)(dim, data, 1, x.data, 1);
  };

  // Sum of elements  of vector x using BLAS function
  //   void dasum_(const int& dim, const double x, const int& stridex)
  // which performs d = x_1 + x_2 + ... + x_dim,
  // where d is a scalar that is returned by the function,
  // Vector x (this) has dimension n, stridex is the stride between 
  // elements of the vector if not stored completely continuosly 
  // (here stridex = 1 always).
  //------------------------------------------------------------------------
  Real 
  LA_Vector::sum() const
  {
    return beep::ACC_PREFIX(asum_)(dim, data, 1);
  };


  // Elementwise multiplication of two vectors    
  //------------------------------------------------------------------------
  LA_Vector 
  LA_Vector::ele_mult(const LA_Vector& x) const
  {
    assert(x.dim == dim);
    LA_Vector y(dim);
    for(unsigned i = 0; i < dim; i++)
      {
	y.data[i] = data[i] * x.data[i];
      }
    return y;
  }

  void
  LA_Vector::ele_mult(const LA_Vector& x, LA_Vector& result) const
  {
    assert(x.dim == dim && result.dim == dim);
    for(unsigned i = 0; i < dim; i++)
      {
	result.data[i] = data[i] * x.data[i];
      }
    return;
  }

  //------------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream& os, const beep::LA_Vector& x)
  {
    return os << x.print();
  };

  std::string 
  LA_Vector::print() const
  {
    std::ostringstream oss;
    oss << "dimension: " << dim << "\n";
    for(unsigned i = 0; i < dim; i++)
      {
	oss << "\t" << data[i] << "\n";
      }
    return oss.str();
  }
 
} /* namespace beep */





