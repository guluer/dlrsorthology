#include <iostream>
#include <cmath>
#include <string>
#include "SimpleObserver.hh"
#include "MCMCModel.hh"


#include <sstream>
#include "Hacks.hh"

namespace beep {
  bool SimpleObserver::afterEachStep(MCMCModel & model, unsigned int iterationsPerformed, bool stateWasChanged, std::string & stdoutStr, std::string & stderrStr ) {

    Probability curProb = model.currentStateProb();
    if ( notRunYet || curProb > localOptimum ) {
       localOptimum = curProb;
       bestState = model.strRepresentation();
       notRunYet = false;
    }

    std::ostringstream ostreamStdout;
    std::ostringstream ostreamStderr;
    /*
    ostreamStdout << "in  SimpleObserver::afterEachStep iterationsPerformed=" << iterationsPerformed 
	    << " currentStateProb()=" <<  model.currentStateProb();
    */
    unsigned printing = thinning * print_factor;
	if (iterationsPerformed % thinning == 0)
	  {
	    if ( show_diagnostics && iterationsPerformed % printing == 0) 
	      {
		ostreamStderr.width(15);
		ostreamStderr << model.currentStateProb();
		ostreamStderr.width(15);
		ostreamStderr << iteration;
		ostreamStderr.width(15);
		ostreamStderr << model.getAcceptanceRatio();
		ostreamStderr.width(15);
		ostreamStderr << estimateTimeLeft(iterationsPerformed, m_iter_total);
		ostreamStderr << std::endl;
	      }
	    
	    ostreamStdout << model.currentStateProb()
		 << "\t"
		 << iterationsPerformed
		 << "\t"
  		 << model.strRepresentation()
		 << "\n";  // This enables buffering and should speed up the program. However, if the program exits prematurely, some iterations may be lost!
// 		 << endl;
	  }
    stdoutStr = ostreamStdout.str();
    stderrStr = ostreamStderr.str();
    return true;
  };

  void
  SimpleObserver::setOutputFile(const char *filename)//, char *header)
  {
    if (cout_buf)  // Already have a file open. Close and reassign.
      {
	os.close();
	std::cout.rdbuf(cout_buf);
      }

    os.open(filename);
    cout_buf = std::cout.rdbuf();
    std::cout.rdbuf(os.rdbuf());

//     if (header) 
//       {
// 	cout << header << endl;
//       }
  }

  void
  SimpleObserver::setThinning(unsigned i)
  {
    thinning = i;
  }

  void
  SimpleObserver::setPrintFactor(unsigned i)
  {
    print_factor = i;
  }

  bool
  SimpleObserver::setShowDiagnostics(bool yes_no)
  {
    bool old_val = show_diagnostics;
    show_diagnostics = yes_no;
    return old_val;
  }


  std::string
  SimpleObserver::estimateTimeLeft(unsigned iteration, unsigned when_done)
  {
    if (iteration < 10) {
      return "";
    }
    unsigned cur_time = time(NULL);
    float d = cur_time - start_time; 
    float efficiency = d / iteration;
    unsigned to_go = lrint(efficiency * (when_done - iteration));

    return readableTime(to_go); 
  }

  Probability
  SimpleObserver::getLocalOptimum()
  {
    assert(!notRunYet);
    return localOptimum;
  }

  std::string 
  SimpleObserver::getBestState()
  {
    assert(!notRunYet);
    return bestState;
  }

  //----------------------------------------------------------------------
  // I/O
  //----------------------------------------------------------------------


  /*
  ostream& 
  operator<<(ostream &o, const SimpleObserver& A)
  {
    return o << A.print();
  }

  string 
  SimpleObserver::print() const
  {
    ostringstream oss;
    oss << " MCMC iterations, saving every "
	<< thinning
	<< " iteration.\n"
	<< indentString(model.print(), "#  ")
      ;
    return oss.str();
  }
  */

}
