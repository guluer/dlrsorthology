#include "EnumHybridGuestTreeModel.hh"

#include "AnError.hh"
#include "HybridTree.hh"

namespace beep
{
  using namespace std;
  //-------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //-------------------------------------------------------
  EnumHybridGuestTreeModel::EnumHybridGuestTreeModel(Tree& G_in, HybridTree& S_in, 
		       StrStrMap& gs_in, BirthDeathProbs& bdp_in)
    : ProbabilityModel(),
      G(&G_in),
      S(&S_in),
      gs(gs_in),
      bdp(&bdp_in),
      maps(),
      useDivTimes(false),
      models(),
      divmodels()
  {
    inits();
  }

  EnumHybridGuestTreeModel::~EnumHybridGuestTreeModel()
  {}

  EnumHybridGuestTreeModel::EnumHybridGuestTreeModel(const EnumHybridGuestTreeModel& hgm)
    : ProbabilityModel(hgm),
      G(hgm.G),
      S(hgm.S),
      gs(hgm.gs),
      bdp(hgm.bdp),
      maps(hgm.maps),
      useDivTimes(hgm.useDivTimes),
      models(hgm.models),
      divmodels(hgm.divmodels)
  {}

  EnumHybridGuestTreeModel&
  EnumHybridGuestTreeModel::operator=(const EnumHybridGuestTreeModel& hgm)
  {
    if(&hgm != this)
      {
	ProbabilityModel::operator=(hgm);
	G = hgm.G;
	S = hgm.S;
	gs = hgm.gs;
	bdp = hgm.bdp;
	maps = hgm.maps;
	useDivTimes = hgm.useDivTimes;
	models = hgm.models;
	divmodels = hgm.divmodels;
      }
    return *this;
  }

  //-------------------------------------------------------
  //
  // Interface
  //
  //-------------------------------------------------------
  void 
  EnumHybridGuestTreeModel::update()
  {
    // update needed if there has been any changes in G or S
    inits();
   }

  Probability 
  EnumHybridGuestTreeModel::calculateDataProbability()
  {
    Probability sum = 0;
    if(useDivTimes)
      {
	for(std::vector<ReconciledTreeTimeModel>::iterator i = divmodels.begin();
	    i != divmodels.end(); i++)
	  {
	    Probability tmp= i->calculateDataProbability();
	    cerr << i->getGTree() << endl;
	    cerr << "probability of first tree is " << tmp << endl;
	    sum += tmp;
// 	    sum += i->calculateDataProbability();
	  }
	return sum;/// divmodels.size();
      }
    else
      {
	for(std::vector<GuestTreeModel>::iterator i = models.begin();
	    i != models.end(); i++)
	  {
	    Probability tmp= i->calculateDataProbability();
	    cerr << i->getGTree() << endl;
	    cerr << "probability of first tree is " << tmp << endl;
	    sum += tmp;
// 	    sum += i->calculateDataProbability();
	  }
	return sum;// / models.size();
      }
  }

  void 
  EnumHybridGuestTreeModel::useDivergenceTimes()
  {
    useDivTimes = true;
    models.clear();
    update();
  }

  //-------------------------------------------------------
  //
  // Implementation
  //
  //-------------------------------------------------------
  void 
  EnumHybridGuestTreeModel::inits()
  {
    maps.clear();
    maps.push_back(gs);
    fillMaps(G->getNode(0), 0);//maps.back());
    if(useDivTimes)
      {
	divmodels.clear();
	for(std::vector<StrStrMap>::iterator i = maps.begin();
	    i != maps.end(); i++)
	  {
	    divmodels.push_back(ReconciledTreeTimeModel(*G, *i, *bdp));
	  }
      }
    else
      {
	models.clear();
	for(std::vector<StrStrMap>::iterator i = maps.begin();
	    i != maps.end(); i++)
	  {
	    models.push_back(GuestTreeModel(*G, *i, *bdp));
	  }
      }
    bdp->update();
    return;
  }

  // PRE: There is at least one leaf-to-leaf map in maps. 
  // argument pos indicates current template map
  void 
  EnumHybridGuestTreeModel::fillMaps(Node* n, unsigned pos)
  {
    if(n == NULL)
      {
	return;
      }
    Node* nextNode = G->getNode(n->getNumber() + 1);

    if(n->isLeaf())
      {
	std::string gname = n->getName();
	std::string sname = gs.find(gname);
	// Check sanity
	assert(S->isExtinct(*S->findNode(sname)) == false);
	assert(S->findNode(sname) != NULL);

	// Check if sname (and thus gname) are affected by a hybridization
	// if so, we must add at least on more leaf-to-leaf map
	Node* snode = S->findNode(sname);
	if(S->hybrid2Binary.find(snode) != S->hybrid2Binary.end())
	  {
	    unsigned next; // indicates which map to use as template
	    // We need one new map per corresponding homeolog leaf
	    for(unsigned i = 0; i < S->hybrid2Binary[snode].size(); i++)
	      {
		if(i ==  0)
		  {
		    next = pos; // the current map is template for the first new map
		  }
		else
		  {
		    // A new map is needed make this the new template map
		    maps.push_back(maps[pos]);
		    next = maps.size() - 1; 
		  }
		// Update the new map for homeolog leaf	
		sname = S->hybrid2Binary[snode][i]->getName();
		maps[next].change(gname, sname);
		// pass on recursion
		fillMaps(nextNode, next);
	      }
	  }
	else
	  {
	    throw PROGRAMMING_ERROR("We should never come here");
	  }
	return;
      }
    else
      {
	fillMaps(nextNode, pos);
	return;
      }
  }
  
} //end namespace beep
