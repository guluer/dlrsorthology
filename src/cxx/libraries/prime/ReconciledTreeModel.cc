#include <cassert>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "ReconciledTreeModel.hh"
#include "TreeAnalysis.hh"


namespace beep
{
  using namespace std;

  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  ReconciledTreeModel::ReconciledTreeModel(Tree &G_in, 
					   StrStrMap &gs_in,
					   BirthDeathProbs &bdp_in)
    : ReconciliationModel(G_in, gs_in, bdp_in)
  {
    inits();		   // Compute important helper params
  }

  ReconciledTreeModel::ReconciledTreeModel(Tree &G_in, 
					   StrStrMap &gs_in,
					   BirthDeathProbs &bdp_in,
					   vector<SetOfNodes>& AC)
    : ReconciliationModel(G_in, gs_in, bdp_in, AC)
  {
    inits();		   // Compute important helper params
  }

  ReconciledTreeModel::ReconciledTreeModel(const ReconciliationModel& rs)
    : ReconciliationModel(rs)
  {
    inits();		   // Compute important helper params
  }


  ReconciledTreeModel::ReconciledTreeModel(const ReconciledTreeModel &M)
    : ReconciliationModel(*M.G, *M.gs, *M.bdp)
  {
    //  WARNING1("ReconciledTreeModel copy constructor used. Probably buggy!");
  }

 
  ReconciledTreeModel::~ReconciledTreeModel()
  {
    //  delete gamma;
  }


  ReconciledTreeModel &
  ReconciledTreeModel::operator=(const ReconciledTreeModel &M)
  {
    if (this != &M)
      {
	ReconciliationModel::operator=(M);
      }

    return *this;
  }


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------

  //------------------------------------------------------------------------
  // Calculations and computations
  //------------------------------------------------------------------------
  Probability
  ReconciledTreeModel::calculateDataProbability()
  {
    assert(gamma.empty() == false);
    Node *rootS = S->getRootNode();
    Node *rootG = G->getRootNode();

//     unsigned leaves = 0; // will hold |L(G_{u,\gamma(x)})|
//     Probability ret;

//      ret = computeE_X(rootS, rootG, leaves);     // e_X(x,u)
//      ret *= bdp->topPartialProbOfCopies(leaves); // e_a(x,u)=Q_x(leaves)*e_X(x, u)

//      return ret;
    return computeE_A(rootS, rootG);
  }

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const ReconciledTreeModel& pm)
  {
    return o << pm.print();
  };
  
  string 
  ReconciledTreeModel::print() const
  {
    std::ostringstream oss;
    oss << "ReconciledTreeModel: Computes the probability of\n"
	<< "a reconciled tree (G, gamma), where gamma is a\n"
	<< "reonciliation of the guest tree G to a host tree S\n"
	<< indentString(ReconciliationModel::print());
    return oss.str();
  }
  
 

  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------

  // Debugging support macros
  //-------------------------------------------------------------------
#ifdef DEBUG_DP
#define DEBUG_EV(S,X,U,R) {cerr << S<< ":\tEV(" << X->getNumber() <<"," << U->getNumber() << ") = " << R.val() << endl;}
#define DEBUG_EA(S,X,U,R,K) {cerr << S << ":\tEA(" << X->getNumber() <<"," << U->getNumber() << ") = " << R.val() << " for #leaves = " << K << endl;}
#define DEBUG_EX(S,X,U,R,K) {cerr << S << ":\tEX(" << X->getNumber() <<"," << U->getNumber() << ") = " << R.val() << ", passing up #leaves  = " << K << endl;}
#else
#define DEBUG_EV(S,X,U,R)
#define DEBUG_EA(S,X,U,R,K)
#define DEBUG_EX(S,X,U,R,K)
#endif

  //Initialize some basic structures.
  void 
  ReconciledTreeModel::inits()
  {
    TreeAnalysis TA(*G);
    isomorphy_rec = TA.isomorphicSubTrees(gamma);

    ReconciliationModel::inits();
  }

  // Computes e_V(x,u) by recursing through $\gamma^{-1}(u)$
  // Notice that the argument x is not really needed, but is 
  // used as an error-check with the asserts.
  // Precondition: u != 0
  //-----------------------------------------------------------------------
  Probability
  ReconciledTreeModel::computeE_V(Node* x, Node* u)
  {
    assert(u != 0);
    assert(gamma.numberOfGammaPaths(*u) > 0);      
    
    Probability ret = 0;

    if(x->isLeaf())
      {
	assert(u->isLeaf());
	assert(gamma.isInGamma(u, x));
	assert(sigma[u] == x); //triple-check
	ret = 1.0;
      }
    else if(x == gamma.getLowestGammaPath(*u) && gamma.isSpeciation(*u))
      {
	Node* v = u->getLeftChild();
	Node* w = u->getRightChild();
	// Get the right subtree of S_x for v and w
	Node *y = x->getDominatingChild(sigma[v]);
	Node *z = x->getDominatingChild(sigma[w]);

	assert(z == y->getSibling()); // Check sanity of recursion
	
	ret = computeE_A(y,v) * computeE_A(z,w);
      }
    else
      {
	// Get the right subtree of S_x for u
	Node* y = x->getDominatingChild(sigma[u]);
	//Now get y's sibling; notice that $u\not\in\gamma(z)$	
	Node* z = y->getSibling();
	// e_V(x,u) = e_A(y,v) X_A(z)
	ret = computeE_A(y,u) * bdp->partialProbOfCopies(*z,0); 

	DEBUG_EA("computeE_V",z,u,bdp->partialProbOfCopies(*z,0),0);
      }
    
    DEBUG_EV("computeE_V",x,u,ret);

    return ret;
  }

  // Computes e_A(x,u)
  // Precondition: u!=0, x!=0, u.isLeaf()==false, x-isLeaf()==false,
  //               gamma.isInGamma(u,x)||gamma.isInGamma(u,x.getParent())||
  //               x->isRoot()
  //----------------------------------------------------------------------
  Probability
  ReconciledTreeModel::computeE_A(Node* x, Node* u)
  {
    assert(u != 0);
    assert(x != 0);
    assert(x->isRoot()||gamma.isInGamma(u,x->getParent())||
	   gamma.isInGamma(u->getParent(),x->getParent()));

    unsigned leaves = 0; // will hold |L(G_{u,\gamma(x)})|

    Probability ret = computeE_X(x, u, leaves); // e_X(x,u)
    ret *= bdp->partialProbOfCopies(*x, leaves); // e_a(x,u)=Q_x(leaves)*e_X(x, u)

    DEBUG_EA("computeE_A",x,u,ret,leaves);

    return  ret;
  }	

  // Computes e_X(x,u)
  // precondition: x!=0, u!=0
  //----------------------------------------------------------------------
  Probability
  ReconciledTreeModel::computeE_X(Node *x, Node *u, unsigned& leaves)
  {
    assert(x != NULL);
    assert(u != NULL);

    Probability ret = 0;
    if(gamma.isInGamma(u,x)) // e_X(x,u) = e_V(x,u)
      {
	leaves = 1; //Add another leaf
	ret = computeE_V(x,u);
      }
    else
      {
	Node *v  = u->getLeftChild();
	Node *w = u->getRightChild();
	// Pass recursion on to set leaves and get e_X(x,v) * e_X(x,w)
	unsigned vleaves = 0;
	unsigned wleaves = 0;
	ret =  computeE_X(x,v,vleaves) * computeE_X(x,w,wleaves);
	leaves = vleaves + wleaves;

	// compute the factor 
	// $\frac{2^{\delta(G_{u,\gamma(x)})+\delta{\gamma,x}(u)}}{leaves-1}$
	Probability factor(1.0 / (leaves - 1));
	adjustFactor(factor, *u);
	// e_X(x,u) = factor * e_X(x,left) * e_X(x,right)
	ret *= factor;
      }

    DEBUG_EX("computeE_X",x,u,ret,leaves);

    return ret;
  }
  

  void
  ReconciledTreeModel::adjustFactor(Probability& factor, Node& u)
  {
    if (isomorphy_rec[u] == false)
      {
	factor *= 2;
      }
  }



  //-----------------------------------------------------------------
  //
  // subclass LabeledReconciledTreeModel.
  //
  //-----------------------------------------------------------------


  LabeledReconciledTreeModel::LabeledReconciledTreeModel(Tree &G_in, 
					   StrStrMap &gs_in,
					   BirthDeathProbs &bdp_in)
    : ReconciledTreeModel(G_in, gs_in, bdp_in)
  {}

  LabeledReconciledTreeModel::LabeledReconciledTreeModel(Tree &G_in, 
					   StrStrMap &gs_in,
					   BirthDeathProbs &bdp_in,
					   vector<SetOfNodes>& AC)
    : ReconciledTreeModel(G_in, gs_in, bdp_in)
  {}

  LabeledReconciledTreeModel::LabeledReconciledTreeModel(const ReconciliationModel& rs)
    : ReconciledTreeModel(rs)
  {}


  LabeledReconciledTreeModel::LabeledReconciledTreeModel(const ReconciledTreeModel &M)
    : ReconciledTreeModel(M)
  {}

 
  LabeledReconciledTreeModel::~LabeledReconciledTreeModel()
  {}


  LabeledReconciledTreeModel &
  LabeledReconciledTreeModel::operator=(const ReconciledTreeModel &M)
  {
    if (this != &M)
      {
	ReconciledTreeModel::operator=(M);
      }

    return *this;
  }

  void
  LabeledReconciledTreeModel::adjustFactor(Probability& factor, Node& u)
  {
    factor *= 2;
  }



}// end namespace beep
