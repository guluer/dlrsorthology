#ifndef RECONCILEDTREEMCMC_HH
#define RECONCILEDTREEMCMC_HH

#include "ReconciledTreeModel.hh"
#include "MCMCObject.hh"
#include "ReconciliationTimeSampler.hh"
#include "ReconciliationSampler.hh"
#include "StdMCMCModel.hh"
#include "TreeIO.hh"


namespace beep
{
  //------------------------------------------------------------
  //
  //! MCMC class for perturbing reconciliations, GammaMaps.
  //! Currently this is done by simply picking new gamma
  //! uniformly among the available ones, and then sampling 
  //! new divergence times from the distribution implied
  //! by the new gamma
  //
  //------------------------------------------------------------
  class ReconciledTreeMCMC : public StdMCMCModel, public ReconciledTreeModel
  {
  public:
    //------------------------------------------------------------
    //
    // Construct/destruct/assign
    //
    //------------------------------------------------------------
    ReconciledTreeMCMC(MCMCModel& prior, Tree& G_in, 
		       StrStrMap& gs_in, BirthDeathProbs& bdp_in,
		       Real suggestRatio = 1.0)
      : StdMCMCModel(prior, 1, "Gamma", suggestRatio),
	ReconciledTreeModel(G_in, gs_in, bdp_in),
	old_gamma(gamma),
	old_times(0),
	psamplet(1.0),
	old_psamplet(0)
    {
      deduceGamma();
      updateDataProbability();
      if(G->hasTimes())
	{
	  ReconciliationTimeSampler rts(*this);
	  old_psamplet = rts.sampleTimes();
	  old_times = G->getTimes();
	}
      ReconciliationSampler tmp(*this);
      tmp.sampleReconciliation();
    }

    ReconciledTreeMCMC(const ReconciledTreeMCMC& rtm)
      : StdMCMCModel(rtm), 
	ReconciledTreeModel(rtm),
	old_gamma(rtm.old_gamma),
	old_times(rtm.old_times),
	psamplet(rtm.psamplet),
	old_psamplet(rtm.old_psamplet)
    {}

    
    ReconciledTreeMCMC& operator=(const ReconciledTreeMCMC& rtm)
    {
      if(&rtm != this)
	{
	  StdMCMCModel::operator=(rtm);
	  ReconciledTreeModel::operator=(rtm);
	  old_gamma = rtm.old_gamma;
	  old_times = rtm.old_times;
	  psamplet = rtm.psamplet;
	  old_psamplet = rtm.old_psamplet;
	}
      return *this;
    }
      
  public:
    //------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------
    void fixGamma()
    {
      if(n_params != 0)
	{
	  n_params--;
	  updateParamIdx();  // tell StdMCMCModel
	}
    }

    
    MCMCObject suggestOwnState()
    {
      old_gamma = gamma;
      do
	{
	  gamma.perturbation(gamma_star);
	}
      while(gamma == old_gamma);

      old_psamplet = psamplet;
      if(G->hasTimes())
	{
	  old_times = G->getTimes();
	  ReconciliationTimeSampler rts(*this);
	  psamplet = rts.sampleTimes(true);
	  G->perturbedTree(true);
	}
      return MCMCObject(updateDataProbability(), old_psamplet / psamplet);
    }
    
    void commitOwnState()
    {}

    void discardOwnState()
    {
      gamma = old_gamma;
      if(G->hasTimes())
	{
	  G->setTimes(old_times);
	  G->perturbedTree(true);
	}
      psamplet = old_psamplet;
    }

    std::string ownStrRep() const
    {
      std::ostringstream oss;
      if(n_params != 0)
	{
	  oss << TreeIO::writeGuestTree(*G, &gamma) << ";\t";
	}
      return oss.str();
    }
    
    std:: string ownHeader() const
    {
      if(n_params != 0)
	{
	  return "gamma(reconciliation);\t";
	}
      return "";
    }

    Probability updateDataProbability()
    {
      update();
      return calculateDataProbability();
    }

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, 
				    const ReconciledTreeMCMC& A)
    {
      return o << A.print();
    }
    
    std::string 
    print() const
    {
      std::ostringstream oss;
      oss << "ReconciledTreeMCMC " << name << ":\n";
      if(n_params == 0)
	{
	  oss << "The reconciliation "
	      << "is fixed to:\n"
	      << gamma.print();
	}
      else
	{
	  oss << "The reconciliation is perturbed during MCMC-analysis.\n"
	      << "When perturbed a new reconciliation is chosen uniformly\n"
	      << "from the set of all reconciliations\n";
	}
      oss << StdMCMCModel::print();

      return oss.str();
    }
  
    
  protected:
    //------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------
    void deduceGamma()
    {
      gamma.reset();
      if(G->hasTimes())
	{
	  deduceGamma(G->getRootNode());
	}
      else
	{
	  setGamma(gamma_star);
	}
      gamma.checkGamma(G->getRootNode());
    }

    Node* deduceGamma(Node* u)
    {
      Node* x;
      if(u->isLeaf())
	{
	  x = sigma[u];
	  gamma.addToSet(x,u);
	}
      else
	{
	  Node* left  = deduceGamma(u->getLeftChild());
	  Node* right = deduceGamma(u->getRightChild());
	  x = S->mostRecentCommonAncestor(left,right);
	  if(left != right)
	    {
	      gamma.addToSet(x,u);
	    }
	}
      while(u->isRoot() == false && x->isRoot() == false &&
	    G->getTime(*u->getParent()) > S->getTime(*x->getParent()))
	{
	  x = x->getParent();
	  gamma.addToSet(x,u);
	}
      if(u->isRoot())
	{
	  while(x->isRoot() == false && G->getTime(*u) < S->getTime(*x->getParent()))
	    {
	      x = x->getParent();	
	      gamma.addToSet(x,u);
	    }
	}      
    
      return x;
    }

    
  protected:
    //------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------
    GammaMap old_gamma;
    RealVector old_times;
    Probability psamplet;
    Probability old_psamplet;
  };

}//end namespace beep

#endif
