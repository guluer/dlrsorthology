#include "util.h"
#include <stdio.h>
//#include <libc.h>   // for random and srandom, but on linux srandom is in stdlib.h
#include <stdlib.h>

void checkpoint(char *location)
{
    FILE *dfile;

    if ((dfile = fopen("debugFile", "a+")) == NULL) {
        printf("checkpoint error: Couldn't open debugFile for appending \n");
        return;
    }

    fprintf(dfile, "%s\n", location);
    fclose(dfile);

}

void checkpointOneInt(char *location, int theInt)
{
    FILE *dfile;

    if ((dfile = fopen("debugFile", "a+")) == NULL) {
        printf("checkpoint error: Couldn't open debugFile for appending \n");
        return;
    }

    fprintf(dfile, "%s %i\n", location, theInt);
    fclose(dfile);

}

double ranDoubleUpToOne(void)
{
	return ((double)random()) / ((double)((long)RAND_MAX)) ;
}

void setBigQFromRMatrixDotPiVec(double **theBigQ, double **theRMatrix, double *piVec, int dim)
{
    int	row, col;
    double	sum;

 	
	for(col = 0; col < dim; col++){
		for(row = 0; row < dim; row++){
			theBigQ[row][col] = theRMatrix[row][col] * piVec[col];
		}
	}

    // set theBigQ diagonals
    for(row = 0; row < dim; row++) {
        sum = 0.0;
        for(col = 0; col < dim; col++) {
            if(row != col) sum = sum + theBigQ[row][col];
        }
        theBigQ[row][row] = -sum;
    }


}	

void normalizeBigQ(double **theBigQ, double *piVec, int dim)
{
	double  sumOfPiVecElements;
	double	sumODE;	// sum of off-diagonal elements of bigPi . bigQ
	int	row, col;

	// first check whether piVec has been set, by summing them all.  Equals 1?
		sumOfPiVecElements = 0.0;
		for(row = 0; row < dim; row++) {
			sumOfPiVecElements = sumOfPiVecElements + piVec[row];
		}
		if(sumOfPiVecElements < 0.999 || sumOfPiVecElements > 1.001) {
			printf("Model: normalizeBigQ: Something wrong with the piVec\n");
			printf("    sumOfPiVecElements is %f\n", sumOfPiVecElements);
			abort(); // was exit(1);
		}
	
	// get sum of off-diag elements of bigPi . bigQ 
		sumODE = 0.0;
		for(row = 0; row < dim; row++) {
			for(col = 0; col < dim; col++) {
				if(row != col) {
					sumODE = sumODE + (piVec[row] * theBigQ[row][col]);
				}
			}
		}
	
	// now multiply each element of theBigQ by the inverse of sumODE
		sumODE = 1.0 / sumODE;
		for(row = 0; row < dim; row++) {
			for(col = 0; col < dim; col++) {
				theBigQ[row][col] = theBigQ[row][col] * sumODE;
			}
		}
		return;
}


int indexOfIntInArray(int theInt, int *theIntArray, int arrayLength)
{
	int i;

	for(i = 0; i < arrayLength; i++) {
		if(theInt == theIntArray[i]) {
			return i;
		}
	}
	return -1;
}

