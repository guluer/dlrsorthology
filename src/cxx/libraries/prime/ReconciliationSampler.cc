#include <cassert>
#include <algorithm>		// For lower_bound

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "ReconciliationSampler.hh"

namespace beep
{
  using namespace std;

  //-------------------------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------------------

  ReconciliationSampler::ReconciliationSampler(Tree& G_in, StrStrMap& gs_in, 
					       BirthDeathProbs& bdp_in)
  //    : GuestTreeModel(G_in, gs_in, bdp_in),
    : LabeledGuestTreeModel(G_in, gs_in, bdp_in),
      R(),
      C_A(*S, G_in),
      C_X(*S, G_in),
      D_A(*S, G_in),
      D_X(*S, G_in),
      posteriorsComputed(false)
  {
    gamma = gamma_star;
    GuestTreeModel::inits();
  }

  ReconciliationSampler::ReconciliationSampler(ReconciliationModel& rm)
  //    : GuestTreeModel(rm),
    : LabeledGuestTreeModel(rm),
      R(),
      C_A(*S, *G),
      C_X(*S, *G),
      D_A(*S, *G),
      D_X(*S, *G),
      posteriorsComputed(false)
  {
    GuestTreeModel::inits();
  }

  ReconciliationSampler::ReconciliationSampler(const ReconciliationSampler &M)
  //    : GuestTreeModel(M),
    : LabeledGuestTreeModel(M),
      R(),
      C_A(M.C_A),
      C_X(M.C_X),
      D_A(M.D_A),
      D_X(M.D_X),
      posteriorsComputed(M.posteriorsComputed)
  {
  
  }

  ReconciliationSampler::~ReconciliationSampler()
  {
  }

  ReconciliationSampler &
  ReconciliationSampler::operator=(const ReconciliationSampler &rs)
  {
    if(this != &rs)
      {
	GuestTreeModel::operator=(rs);
	R = rs.R;
	C_A = rs.C_A;
	C_X = rs.C_X;
	D_A = rs.D_A;
	D_X = rs.D_X;
	posteriorsComputed = rs.posteriorsComputed;
      }
  
    return *this;
  }


  //-------------------------------------------------------------------------
  //
  // Interface
  //
  //--------------------------------------------------------------------

  // Updating stuff when the gene tree has changed /this is cheap so O.K. /bens
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::update()
  {
    GuestTreeModel::update();
    posteriorsComputed = false;	// Invalidate C_A, etc.
  }

  // Main method: sampling of reconciliations!
  //-------------------------------------------------------------------------

  // Generates and returns a new \f[\gamma\f], also stores it in 
  // attribute gamma
  GammaMap
  ReconciliationSampler::sampleReconciliation()
  {
    generateReconciliation();  
    return gamma;
  }
  
  // Generates and returns a new \f[\gamma\f] and its probability, 
  // \f[Pr(\gamma | G, \lambda, \mu)\f], also stores it in attribute gamma
  pair<GammaMap, Probability>
  ReconciliationSampler::sampleReconciliationAndProb()
  {
    // First make sure we are initialized
    if(posteriorsComputed == false)
      {
	setAttributesAndProbabilities();
      }

    gamma.reset(); // Reset gamma
    Probability p = beginSlice(S->getRootNode(), G->getRootNode());

    return pair<GammaMap,Probability>(gamma, p);
  }

  // Generates a new \f[\gamma\f] and stores it in attribute gamma
  void
  ReconciliationSampler::generateReconciliation()
  {
    // First make sure we are initialized
    if(posteriorsComputed == false)
      {
	setAttributesAndProbabilities();
      }
    
    gamma.reset(); // Reset gamma
    beginSlice(S->getRootNode(), G->getRootNode());
    return;
  }


  //-------------------------------------------------------------------------
  //
  // Implementation
  //
  //--------------------------------------------------------------------
  // Debugging support macros
  //-------------------------------------------------------------------------
#ifdef DEBUG_SAMPLING
#define DEBUG_C_A(X,U,K) {cerr << "C_A(" << X->getNumber() << ", " << U->getNumber() << ", " << K << ") = " << C_A(X,U)[K-1].val() << "\tS_A(" << X->getNumber() << ", " << U->getNumber() << ") = " << S_A(X,U).val() <<endl;}
#define DEBUG_C_X(X,U,K,K1) {cerr << "C_X(" << X->getNumber() << ", " << U->getNumber() << ", " << K << ", " << K1 <<  ") = " << C_X(X,U)[K-1][K1-1].val() << endl;}
#else
#define DEBUG_C_A(X,U,K)
#define DEBUG_C_X(X,U,K,K1)
#endif

  // Reset all probabilities including those of base class
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::setAttributesAndProbabilities()
  {
    Probability p = calculateDataProbability();
    computePosteriors();
  }


  // Front end to parameterized method below
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::computePosteriors()

  {
    Node *rootG = G->getRootNode();
    computePosteriors(rootG);

    // Finally update C_A at the top. This is slightly different from the
    // other slice, please see GuestTreeModel::calculateDataProbability()
    // for details.
    Node *rootS = S->getRootNode();
    C_A(rootS, rootG).resize(slice_U[rootG]);
    D_A(rootS, rootG).resize(slice_U[rootG]);
    Probability sum = 0.0;

    for (unsigned k = slice_L(rootS, rootG); k <= slice_U[rootG]; k++) 
      {
	Probability term = bdp->topPartialProbOfCopies(k) * 
	  S_X(rootS, rootG)[k - 1];
	sum = sum + term;
	C_A(rootS, rootG)[k-1] = sum  / S_A(rootS, rootG);
	D_A(rootS, rootG)[k-1] = term / S_A(rootS, rootG);
	DEBUG_C_A(rootS, rootG, k);
      }

    posteriorsComputed = true;
  }


  /// Traverses the gene tree and computes the $C_A$ and $C_X$ values.
  // At every gene  node, we start with the lowest possible species node
  // and traverses up to the root in the species tree, filling in values
  // as we go.
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::computePosteriors(Node *u)
  {
    Node *y = sigma[u];
    if(u->isLeaf())
      {
	while(y->isRoot() == false)
	  {
	    updateC_A(y, u);
	    y = y->getParent();
	  }
      }
    else
      {
	computePosteriors(u->getLeftChild());
	computePosteriors(u->getRightChild());
	while(y->isRoot() == false)
	  {
	    updateC_A(y, u);
	    updateC_X(y, u);
	    y = y->getParent();
	  }
	updateC_X(y, u);
      }
  }

  // Actual computation of C_A and D_A given gene and species nodes.
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::updateC_A(Node *y, Node *u)
  {
    C_A(y, u).resize(slice_U[u]);
    D_A(y, u).resize(slice_U[u]);
    if(u->isLeaf())
      {
	C_A(y, u)[0] = 1.0;
	D_A(y, u)[0] = 1.0;
	DEBUG_C_A(y,u,1);
      }
    else
      {
	Probability sum = 0.0;
	for (unsigned k = slice_L(y, u); k <= slice_U[u]; k++) 
	  {
	    Probability term = bdp->partialProbOfCopies(*y, k) * 
	      S_X(y, u)[k - 1]; 
	    sum += term;
	    C_A(y, u)[k-1] = sum  / S_A(y, u); // Distribution function
	    D_A(y, u)[k-1] = term / S_A(y, u); // probability function
	    DEBUG_C_A(y,u,k);
	    assert( C_A(y, u)[k - 1] <= 1.0);
	  }
      }
  }

  // Actual computation of C_X and D_X given gene and species nodes.
  //-------------------------------------------------------------------------
  void
  ReconciliationSampler::updateC_X(Node *y, Node *u)
  {
    assert(u->isLeaf() == false);

    unsigned maxK = slice_U[u]; 
    C_X(y, u).resize(maxK);
    D_X(y, u).resize(maxK);

    for (unsigned k = slice_L(y, u); k <= maxK; k++) 
      {
	Node *left = u->getLeftChild();
	Node *right = u->getRightChild();

	// Notice that we actually save the probabilities relating
	// to u's left child, thus size of vector depends on left

	unsigned maxKleft = slice_U[left];
	C_X(y, u)[k-1].resize(maxKleft);
	D_X(y, u)[k-1].resize(maxKleft);
      
	// First sum the contributions
	Probability sum = 0.0;
	for (unsigned k1 = slice_L(y, left); k1 <= maxKleft; k1++) 
	  {
	    unsigned k2 = k - k1;
	    if(k2 <= slice_U[right] && k2 >= slice_L(y, right)) 
	      {
 		Probability term = S_X(y, left)[k1 - 1] * 
		  S_X(y, right)[k2 - 1];
		sum = sum + term;
		C_X(y, u)[k - 1][k1 - 1] =  sum; // these are not 
		D_X(y, u)[k - 1][k1 - 1] =  term;// normalized yet
	      }
	  }

	// Then scale the elements -- sum now holds normalizing factor
	for (unsigned k1 = slice_L(y, left); k1 <= maxKleft; k1++) 
	  {
	    unsigned k2 = k - k1;
	    if(k2 == 0)
	      {
		DEBUG_C_X(y,u,k,k1);
	      }
	    if(k2 <= slice_U[right] && k2 >= slice_L(y, right)) 
	      {
		C_X(y, u)[k - 1][k1 - 1] = C_X(y, u)[k - 1][k1 - 1] / sum;
		D_X(y, u)[k - 1][k1 - 1] = D_X(y, u)[k - 1][k1 - 1] / sum;
		DEBUG_C_X(y,u,k,k1);
		assert(C_X(y, u)[k - 1][k1 - 1] <= 1.0);
	      }
	  }
      }
  }



  // At the beginning of a slice, we have to choose which size it should be
  // Then send on recursion
  // Corresponds to sampleA in paper_1
  //-------------------------------------------------------------------------
  Probability
  ReconciliationSampler::beginSlice(Node *y, Node *u)
  {
#ifdef DEBUG_SAMPLING
    cerr << "Entering beginSlice(" << y->getNumber() << "," << u->getNumber() << ")\n";
#endif
    assert(y != NULL);
    assert(u != NULL);

    unsigned k = chooseElement(C_A(y, u), slice_L(y,u), slice_U[u]);
    Probability p_k = D_A(y, u)[k - 1];
    return p_k * recurseSlice(y, u, k);
  }


  // Now that we know how big a slice, or subslice starting at the current 
  // gene node, is we will have to decide what the slice looks like by at 
  // each gene node randomly decide how many nodes are put in the left part 
  // of the tree. When we get to k==1, we know that it is time to start a 
  // new slice using beginSlice().
  //-------------------------------------------------------------------------
  Probability
  ReconciliationSampler::recurseSlice(Node *x, Node *u, 
				      unsigned k) 
  {
#ifdef DEBUG_SAMPLING
    cerr << "Entering recurseSlice(" << x->getNumber() << "," << u->getNumber() << "," << k <<")\n";
#endif
    assert(x != NULL);
    assert(u != NULL);
    assert(k > 0);

    if(k == 1)
      {
#ifdef DEBUG_SAMPLING
	cerr << "recurseSlice k = 1\n";
#endif
	if(u->isLeaf() == true)	// Found bottom of gene tree...
	  {
#ifdef DEBUG_SAMPLING
	    cerr << "recurseSlice: node "<< u->getNumber() << " is a Leaf\n";
#endif
	    if(x->isLeaf() == true) // ...and the bottom of the species tree:
	      //Time to stop
	      {
		gamma.addToSet(x, u); // add $u$ to $\gammamin(x)$
		return Probability(1.0);
	      }
	    else// Here, there are more species nodes, which means
	      {	// that u is acting as a representative for unseen speciations
		Probability p = beginSlice(x->getDominatingChild(sigma[u]), u);
		gamma.addToSet(x, u); // Order is important here! 
		//First recursion, or gamma might break
		return p;
	      }
	  }
	else// If u is not a leaf...
	  {
#ifdef DEBUG_SAMPLING
	    cerr << "recurseSlice: node "<< u->getNumber() << " is not a Leaf\n";
#endif
	    // TODO: Change this to an assert?/bens
	    if(x->isLeaf() == true) // Ooh, this is truly bad! Should not happen
	      {
		throw AnError("Bad programming!", 1);
	      }
	    else			
	      // This is the normal path! There are more 
	      // nodes to recurse on in both trees.
	      // If u is a speciation node, then recurse 
	      //both left and right
	      {

//  		if(gamma_star.isInGamma(u, x))
		if(x == gamma_star.getLowestGammaPath(*u) && gamma_star.isSpeciation(*u))
//  		if(gamma_star.isSpeciationInGamma(u, x))
		  {
#ifdef DEBUG_SAMPLING
		    cerr << "recurseSlice: node "<< u->getNumber() << " is in gamma*(" << x->getNumber() << ")\n";
#endif
		    Node *uLeft = u->getLeftChild();
		    Node *uRight = u->getRightChild();
		    Node *xLeft = x->getDominatingChild(sigma[uLeft]);
		    Node *xRight = x->getDominatingChild(sigma[uRight]);
		    Probability p_left  = beginSlice(xLeft, uLeft);
		    Probability p_right = beginSlice(xRight, uRight);

		    // Order is important here! First recursion, or gamma
		    // might break
		    gamma.addToSet(x, u); 

		    return p_left * p_right;
		  }
		else  // If not a speciation, there has been a loss, 
		  // avoid that lineage!
		  {
#ifdef DEBUG_SAMPLING
		    cerr << "recurseSlice: node "<< u->getNumber() << " is not in gamma*(" << x->getNumber() << ")\n";
#endif
		    Probability p = beginSlice(x->getDominatingChild(sigma[u])
					       , u);
		    // Order is important here! First recursion, or gamma 
		    // might break
		    gamma.addToSet(x, u); 

		    return p;
		  }
	      }
	  }
      }
    else    // k > 1, must keep on recursing through the slice
      {
#ifdef DEBUG_SAMPLING
	cerr << "recurseSlice k > 1\n";
#endif
	Node *uLeft = u->getLeftChild();
	Node *uRight = u->getRightChild();

	// Compute the allowed range for k1
	unsigned k1_min = slice_L(x, uLeft);
	unsigned k1_max = slice_U[uLeft];
	unsigned k2_min = slice_L(x, uRight);
	unsigned k2_max = slice_U[uRight];

	if(k1_min + k2_max < k)	
	  // It used to be k1_min < k - k2_max, but we 
	  // are working with unsigneds here, so k - k2_max
	  // could become humongous if k2_max > k!
	  {
	    k1_min = k - k2_max;
	  }
	if(k1_max + k2_min > k)	// Same problem here! Hmm unsigned ints 
	  // can be dangerous!
	  {
	    k1_max = k - k2_min;
	  }

	// Go pick the sub-slice size, i.e., $l^\gamma(x,left)$
	// Notice that $l^\gamma(x,right) = k - l^\gamma(x,left)$
	unsigned k1 = chooseElement(C_X(x, u)[k - 1], k1_min, k1_max);

	assert(k1 < k);
	assert(k1 > 0);

	Probability p_k1 = D_X(x, u)[k - 1][k1 - 1];
	Probability p_left = recurseSlice(x, uLeft,  k1);
	Probability p_right = recurseSlice(x, uRight, k - k1);
	return p_k1 * p_left * p_right;
      }
  }


  // In:  A vector of probabilities: v[k] is the probability of element k
  // Out: The index of a randomly chosen element, distribution from v.
  //
  // Woohoo! I am using STL. It took me three times the time to implement this
  // compared to if I had done the binary search myself. But I did learn stuff
  // about STL. I wonder how efficient it really is though, since the code 
  // seems a little bit convoluted!   /arve
  //-------------------------------------------------------------------------
  unsigned
  ReconciliationSampler::chooseElement(vector<Probability> &v, 
				       unsigned L, unsigned U)
  {
    if(L == U)
      {
	return L;
      }
    else
      {
	Real r = R.genrand_real2();
#ifdef DEBUG_SAMPLING
	cerr << "chooseElement uses r = " << r << endl;
#endif
	// Note that if v.begin() and lower_bound... are not random access 
	// iterators (as they are here) distance can be very ineffective
	unsigned c =  1 + distance(v.begin(), 
		   lower_bound(v.begin() + L - 1, v.begin() + U - 1, r));

	assert(L <= c);
	assert(c <= U);
 
	return c;
      }
  }

}//end namespace beep
