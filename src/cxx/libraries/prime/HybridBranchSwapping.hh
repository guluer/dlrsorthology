#ifndef HYBRIDBRANCHSWAPPING_HH
#define HYBRIDBRANCHSWAPPING_HH

#include "PRNG.hh"

namespace beep
{
  // Forward declarations
  class HybridTree;
  class Node;

  class HybridBranchSwapping
  {
    //--------------------------------------------------------------
    //
    // Construct destruct assign
    //
    //--------------------------------------------------------------
  public:
    HybridBranchSwapping(HybridTree& H_in);
    ~HybridBranchSwapping();
    HybridBranchSwapping(const HybridBranchSwapping& hbs);
    HybridBranchSwapping& operator=(const HybridBranchSwapping& hbs);

    //--------------------------------------------------------------
    //
    // Interface
    //
    //--------------------------------------------------------------
    

    //--------------------------------------------------------------
    //
    // Implementation
    //
    //--------------------------------------------------------------
    Node* addHybrid();
    Node* rmHybrid();
    Node* mvHybrid();
    Node* addExtinct(Node& p, Node& u);
    Node* rmExtinct(Node& e);
    void suppress(Node& u);
    //--------------------------------------------------------------
    //
    // Attributes
    //
    //--------------------------------------------------------------
    HybridTree* H;
    PRNG R;

  };

}//end namespace beep
#endif
