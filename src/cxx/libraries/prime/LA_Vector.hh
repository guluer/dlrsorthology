#ifndef LA_VECTOR_H
#define LA_VECTOR_H

#include "Beep.hh"

#include <iostream>

#ifdef LA_SINGLE_PRECISION
#define ACC_PREFIX(x) s##x
#else
#define ACC_PREFIX(x) d##x
#endif

namespace beep {
  // Forward declarations
  class LA_Matrix;
  class LA_DiagonalMatrix;
  class LA_Vector;
  LA_Vector operator*(const Real& alpha, const LA_Vector& x);
  LA_Vector operator/(const Real& alpha, const LA_Vector& x);
  
  //! Linear Algebra vector help class used in conjunction with LAPACK,
  //! e.g., for computation of substitution matrix probabilities
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  class LA_Vector
  {
    friend class beep::LA_Matrix;
    friend class beep::LA_DiagonalMatrix;
  public:
    //------------------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //------------------------------------------------------------------------
    LA_Vector(const unsigned& dim);

    // All elements are intitiated to init
    LA_Vector(const unsigned& dim, const Real& init);

    LA_Vector(const unsigned& dim, const Real in_data[]);

    LA_Vector(const LA_Vector& B);

    virtual ~LA_Vector();

    LA_Vector& operator=(const LA_Vector& B);

    bool operator!=(const LA_Vector& B) const;

    //------------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------------
  
    // Dimension of Vector
    //------------------------------------------------------------------------
    const unsigned& getDim() const;

    // Access to individual elements
    //------------------------------------------------------------------------
    Real& operator[](const unsigned& elem);

    const Real operator[](const unsigned& elem) const;

    // (Re)set all elements
    //------------------------------------------------------------------------
    void setAllElements(Real value);

    // Addition of vectors x and y 
    //------------------------------------------------------------------------
    LA_Vector operator+(const LA_Vector& x) const ;

    // product of vectors x and scalar alpha 
    //------------------------------------------------------------------------
    LA_Vector operator*(const Real& alpha) const;

    friend LA_Vector operator*(const Real& alpha, const LA_Vector& x);

    // Column row product of vectors x and y yields a matrix
    // Defined in LA_Matrix!
    //------------------------------------------------------------------------
    LA_Matrix col_row_product(const LA_Vector& x) const;


    // ratio of vectors x and scalar alpha 
    //------------------------------------------------------------------------
    LA_Vector operator/(const Real& alpha) const;

    friend LA_Vector operator/(const Real& alpha, const LA_Vector& x);

    // dot product of vectors x and y
    //------------------------------------------------------------------------
    Real operator*(const LA_Vector& x) const;

    // Sum of elements  of vector x 
    //------------------------------------------------------------------------
    Real sum() const;

    // Elementwise multiplication of two vectors    
    //------------------------------------------------------------------------
    LA_Vector ele_mult(const LA_Vector& x) const;
    void ele_mult(const LA_Vector& x, LA_Vector& result) const;

    //------------------------------------------------------------------------
    // I/O
    //------------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream& os, 
				    const beep::LA_Vector& x);

    virtual std::string print() const;

  private:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
  
    unsigned dim;
    Real* data; //This is an array
  };
 
} /* namespace beep */

#endif




