#ifndef RECONCILIATIONMODEL_HH
#define RECONCILIATIONMODEL_HH

#include <iostream>
#include <sstream>

#include "Beep.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "NodeMap.hh"
#include "NodeNodeMap.hh"
#include "ProbabilityModel.hh"

namespace beep
{
  // Forward declarations
  class BirthDeathProbs;
  class Node;
  class StrStrMap;
  class Tree;

  //------------------------------------------------------------------------
  //
  //! Base class for reconciliation classes
  //! Author: Lars Arvestad, SBC, � the MCMC-club, SBC, all rights reserved
  // 
  //------------------------------------------------------------------------
  class ReconciliationModel : public ProbabilityModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    ReconciliationModel(Tree &G_in, StrStrMap &gs_in, BirthDeathProbs &bdp_in);
    ReconciliationModel(Tree &G_in, StrStrMap &gs_in, BirthDeathProbs &bdp_in, 
			std::vector<SetOfNodes>& AC);
    ReconciliationModel(const ReconciliationModel &M);
    virtual ~ReconciliationModel();
    ReconciliationModel & operator=(const ReconciliationModel &M);

    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------

    // Access and manipulation
    //-------------------------------------------------------------------
    virtual Tree& getGTree() const;                //!< returns G
    virtual Tree& getSTree() const;                //!< returns S
    virtual BirthDeathProbs& getBirthDeathProbs(); //!< returns bdp

    //! Returns \f$\gamma\f$ curently saved in attribute gamma
    virtual const GammaMap& getGamma();

    //! Sets \f$\gamma\f$ curently saved in attribute gamma to copy 
    //! argument
    virtual void setGamma(const GammaMap& newGamma);

    //! Get automatic support for choosing rates
    //! We make a quick calculation of a suitable starting point with
    //! chooseStartingRates(). Here several values for the rates are
    //! tested and the best one is chosen. No artificial intelligence
    //! here, simply rough estimates!
    //-------------------------------------------------------------------
    virtual void chooseStartingRates();
    virtual void chooseStartingRates(Real &birthRate, Real &deathRate);

    //------------------------------------------------------------------
    // Interface to MCMCModel
    //------------------------------------------------------------------
    virtual Probability calculateDataProbability(); 
    virtual void        update();

    //-------------------------------------------------------------------
    //
    // I/O
    //
    //-------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const ReconciliationModel& rm);
    std::string print() const;

  protected:
    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
    virtual void inits();
    
    //! Fill lower bound on \f$ |L(G_{u,\gamma(x)})|\f$ in the slice_L 
    //! structure (see attributes below).
    //------------------------------------------------------------------
    virtual void computeSliceSizeLowerBound(Node *v);
    
  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------

    //------------------------------------------------------------------
    // externally handled attributes
    //------------------------------------------------------------------
    Tree* G;
    Tree* S;  
    StrStrMap* gs;
    BirthDeathProbs* bdp; //!< Handle all gene-tree slice probabilities 		
  
    //------------------------------------------------------------------
    // Internal attribute that need to be initiated before gamma
    //------------------------------------------------------------------
    LambdaMap sigma; //!< Implements the connection  \f$ \sigma(u)=x \f$

    //! Stores the current reconciliation if any
    GammaMap gamma;		



    //!\todo{ Some of the following attributes could be placed in a 
    //! middle class, as all subclasses may not use them \bens}

    //! Bounds the anti-chains. This could be the most parsimonious 
    //! reconciliation or a user-defined reconciliation
    GammaMap gamma_star;		

    //! Indexed by node id. Elem is true if the subtrees of the node are
    //! isomorphic.
    NodeMap<bool> isomorphy;	

    //! Element i stores the number of leaves in the subtree rooted at
    //! node with ID = i. This is an upper bound on gene tree slices.
    NodeMap<unsigned> slice_U;

    //! Like slice_U, but for lower bounds. This bound depends your
    //! reconciliation, or rather, where in the species tree you want a
    //! gene node.
    NodeNodeMap<unsigned> slice_L;
  };

}//end namespace beep

#endif
