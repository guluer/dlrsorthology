#include <cassert>
#include <cmath>
#include <sstream>

#include "BirthDeathProbs.hh"

#include "AnError.hh"
//#include "Integrals.hh"
#include "Probability.hh"
#include "Tree.hh"

using namespace std;

// Author: Lars Arvestad, Bengt Sennblad, � the MCMC-club, SBC,
// all rights reserved
namespace beep
{
  class Tree;

  //---------------------------------------------------------------
  // 
  // Construct/Destruct
  //
  //---------------------------------------------------------------
  BirthDeathProbs::BirthDeathProbs(Tree &S, 
				   const Real& birth_rate,
				   const Real& death_rate,
				   Real* top_time)
    : S(S),
      topTime(top_time?top_time:&S.getTopTime()),
      birth_rate(birth_rate),
      death_rate(death_rate),
      db_diff(death_rate - birth_rate),
      BD_const(S.getNumberOfNodes()),
      BD_var(S.getNumberOfNodes()),
      BD_zero(S.getNumberOfNodes()), 
      generalBirthRate(S.getNumberOfNodes()),
      generalDeathRate(S.getNumberOfNodes()) 
  {
    if(*topTime == 0)
      {
	S.setTopTime(1.0);
      }
    // If badly chosen rates, then protest!
    if (birth_rate == 0.0)
      {
	throw AnError("Cannot have birth rate = 0.0!");
      }
    if (death_rate == 0.0)
      {
	throw AnError("Cannot have death rate = 0.0!");
      }
    if (birth_rate < 0.0)
      {
	throw AnError("Negative birth rate suggested!", 1);
      } 
    if (death_rate < 0.0)
      {
	throw AnError("Negative death rate suggested!", 1);
      } 

    update();
  }

  BirthDeathProbs::BirthDeathProbs(const BirthDeathProbs &BDP)
    : S(BDP.S),
      topTime(BDP.topTime),
      birth_rate(BDP.birth_rate),
      death_rate(BDP.death_rate),
      db_diff(death_rate - birth_rate),
      BD_const(BDP.BD_const),
      BD_var(BDP.BD_var),
      BD_zero(BDP.BD_zero),
      generalBirthRate(BDP.generalBirthRate),
      generalDeathRate(BDP.generalDeathRate) 
  {
  }

  //virtual 
  //---------------------------------------------------------------
  BirthDeathProbs::~BirthDeathProbs()
  {
  }


  // Assignment
  //---------------------------------------------------------------
  BirthDeathProbs&
  BirthDeathProbs::operator=(const BirthDeathProbs &BDP)
  {
    if (this != &BDP)
      {
	S = BDP.S;
	birth_rate = BDP.birth_rate;
	death_rate = BDP.death_rate;
	topTime = BDP.topTime;
	db_diff = death_rate - birth_rate;
	BD_const = BDP.BD_const;
	BD_var = BDP.BD_var;
	BD_zero = BDP.BD_zero;
	generalBirthRate= BDP.generalBirthRate;
	generalDeathRate = BDP.generalDeathRate;
      }

    return *this;
  }



  //---------------------------------------------------------------------
  //
  // Interface
  //
  //---------------------------------------------------------------------

  // Access to Tree S and topTime
  // This could be used when asserting that, e.g., a BDP and a GammaMap
  // relates to the same host tree. It is also used by 
  // ReconciliationTimeSampler to access the host tree's root node
  //---------------------------------------------------------------------
  Tree& 
  BirthDeathProbs::getStree() const
  {
    return S;
  }
  
  Real 
  BirthDeathProbs::getTopTime() const
  {
    return *topTime;
  }
  // Access parameters
  //---------------------------------------------------------------------
  void 
  BirthDeathProbs::getRates(Real &birthRate, Real &deathRate) const
  {
    birthRate = birth_rate;
    deathRate = death_rate;
  }

  Real&
  BirthDeathProbs::getBirthRate()
  {
    return birth_rate;
  }

  Real&
  BirthDeathProbs::getDeathRate()
  {
    return death_rate;
  }

  // Parameter update
  //---------------------------------------------------------------------
  void
  BirthDeathProbs::setRates(Real new_birth_rate, Real new_death_rate, 
			    bool do_update)
  {
    birth_rate = new_birth_rate;
    death_rate = new_death_rate;  
    db_diff = death_rate - birth_rate;

    if (do_update == true) 
      {
	update();
      }
  }

  // Force an update of precomputed values
  //---------------------------------------------------------------------
  void
  BirthDeathProbs::update()
  {
    if(BD_const.size() != S.getNumberOfNodes())
      {
	BD_const = ProbVector(S.getNumberOfNodes());
	BD_var = ProbVector(S.getNumberOfNodes());
	BD_zero = ProbVector(S.getNumberOfNodes()); 
	generalBirthRate = RealVector(S.getNumberOfNodes());
	generalDeathRate = RealVector(S.getNumberOfNodes());
      }
    calcBirthDeathProbs(*S.getRootNode());
  }


  // The conditional (partial)probability of n gene copies over an arc (x,y)
  // Remember kids: This is not a probability, since we are cancelling a 
  // factor $(1-D)^{c+1}$ also occuring elsewhere! That is why it has *Partial* 
  // in its name. 
  //---------------------------------------------------------------------
  Probability
  BirthDeathProbs::partialProbOfCopies(const Node &y, unsigned n_kids) const
  {
    if (n_kids == 0) 
      {
	assert(BD_zero[y] > 0.0);
	return BD_zero[y];
      }
    else 
      {
	assert(BD_const[y] > 0.0);
	return BD_const[y] * pow(BD_var[y], (int)n_kids - 1);
      }
  }


  // We don't treat the top slice differently here, but might be doing so 
  // in derived classes.
  // Remember kids: This is not a probability, since we are cancelling a 
  // factor $(1-D)^{c+1}$ also occuring elsewhere! That is why it has *Partial* 
  // in its name. 
  //******TO BEOVERLOADED IN INTEGRALDUPSPECPROBS*****
  //---------------------------------------------------------------------
  Probability
  BirthDeathProbs::topPartialProbOfCopies(unsigned n_kids) const
  {
    return partialProbOfCopies(*S.getRootNode(), n_kids); 
  }

  Probability
  BirthDeathProbs::extinctionProbability(Real t) const
  {
      // Probability of going extinct in time t due to Kendall.
      Real E = std::exp((birth_rate - death_rate)*t);
      return (death_rate*(E - 1)) / (birth_rate*E - death_rate);
  }

  // The probability of extinction below a node v. 
  Probability
  BirthDeathProbs::extinctionProbability(Node *v) const
  {
    assert(v != NULL);
    return BD_zero[*(v->getLeftChild())] * BD_zero[*(v->getRightChild())];
  }


  // Given a probability, P, and a vertex sn, sample c, the number of childs
  // from the cumulative probability function for c
  // The probability function is:
  //    f_X(c = 0) = 1 - \tilde{P}(t)(1-D_y)
  //    f_X(c | c > 0) = \tilde{P}(t)(1-D_y)(1-\tilde{u}_t)\tilde{u}_t^{c-1}
  // The distribution function is:
  //    F_X(c) = 1 - \tilde{P}(t)(1-D_y)\tilde{u}_t^c
  // Sample by drawing P from U(0,1) and set c so that 
  //    0 < P < F_X(c)         for c = 0  or 
  //    F_X(c-1) < P < F_X(c)  for c > 0
  // Note that this is conditioned on that all c children survives below t
  // You need to ascertain that in recursive sampling!
  //----------------------------------------------------------------------
  unsigned
  BirthDeathProbs::sampleNumberOfChildren(Node& y,
					  const Real& P) const
  {
    assert(y.getTime() > 0.0);

    // Note that  BD_zero[y] = 1 - \tilde{P}(t)(1-D_y)
    // and        BD_var[y]  = \tilde{u}_t / (1 - D_y)

    if(P <= BD_zero[y].val())
      {
	return 0;
      }
    Real x = (1 - P) / (1 - BD_zero[y].val()); // = \tilde{u}_t^c

    if(y.isLeaf()) 
      {
	return static_cast<unsigned>(ceil(std::log(x) / std::log(BD_var[y].val())));
      }
    Probability D_y = BD_zero[y.getLeftChild()] * BD_zero[y.getRightChild()];


    // Return c = ln(x / \tilde{u}_t) = ln( x / BD_var[y] (1 - D_y))
    // Convert 
    return static_cast<unsigned>(ceil(std::log(x) / 
				      std::log((BD_var[y] * (1-D_y)).val())));
  }

  // The calculation of 
  // (c-1)\tilde{\lmbade}\tilde{P}(t)(1-\tilde{u}_t)/\tilde{u}_T, in a 
  // slice, i.e., partial probabilities for each edge time in a slice.
  // The full edgetimeprob of the slice is the product of partialEdgeTimeProbs
  // for all edges an the (c-1)th power of EdgeTimeProbFactor  (see above)
  // used, e.g., in ReconciliationTimeModel
  // NOTE!! Despite the name, what is really sent as 't' is really the
  // 'remaining time' ijn the slice, i.e., if 'u' is the nide such that 
  // |L(G_u)| = 'leaves', then 't' = u.getNodeTime() -y.getNodeTime() 
  //----------------------------------------------------------------------
  Probability
  BirthDeathProbs::partialEdgeTimeProb(Node& y,
				       const unsigned& leaves, 
				       const Real& t) const
  {

//     Probability D = 0;
//     if(!y.isLeaf())
//       {
// 	D = BD_zero[y.getLeftChild()] * BD_zero[y.getRightChild()];
//       }
#ifdef THOMPSON
    // If called recursively, and with THOMPSON defined, and finally 
    // multiplied by the function getThompsons_p1()this function will 
    // yield Thompson's probability Pr[t,G,l|lambda, mu, S], where G is a
    // labeled history, t is the divergence times and l the labels of G, 
    // and S is a single-leaved host tree. 
    // Notice that S must be single-leaved!
    Probability factor = 2*birth_rate;
#else
    //    Probability factor = (leaves - 1) * birth_rate / (BD_var[y] * (1 - D));
    Probability factor = (leaves - 1) * birth_rate / BD_var[y];
#endif 
   if(t == 0)
      {
	throw AnError("BirthDeathProbs::partialEdgeTimeProb: "
		      "time t <= 0, currently not handled!\n",1);
      }
    else if(db_diff == 0)
      {
	return factor / std::pow(1 + generalDeathRate[y] * t, 2);
      }
    else
      {
	Real E = std::exp(db_diff * t);
	Real P_t = -db_diff / (generalBirthRate[y] - generalDeathRate[y] * E);
	// This is actually unused because P_t *E ) u_t
// 	Real u_t = birth_rate * (1 - E) / (generalBirthRate[y] 
// 					   - generalDeathRate[y] * E);
   	return factor * std::pow(P_t, 2) * E;
      }
  };

  // Computes the probability that a birth occur time div_time before
  // Node x and produces a single lineage at x that survives to the 
  // leaves of S. Used in ReconciledTreeTimeModel
  Probability 
  BirthDeathProbs::bornLineageProbability(Node& x, Real div_time)
  {
    Probability Pt;
    Probability ut;
    calcPt_Ut(div_time, Pt, ut);
    
    Probability p = birth_rate * Pt * (1-ut);
    if(x.isLeaf() == false)
      {
	Node& y = *x.getLeftChild();
	Node& z = *x.getRightChild();
	p/= pow(1-ut * BD_zero[y] * BD_zero[z],2);
      }
    return p;
  }

    // The calculation of edge time
 
  // The process starts at maxT, and should yield leaves leaves, sample 
  // the time for the first birth, i.e., of the top binary vertex
  //----------------------------------------------------------------------
  Real
  BirthDeathProbs::generateEdgeTime(Node& y,
				    const unsigned& leaves, 
				    const Real& P,
				    Real maxT) const
  {
    if(maxT < 0)
      {
	maxT = y.getTime();
      }
    unsigned c = leaves - 1;
    if(db_diff == 0)
      {
	Real E = generalDeathRate[y] * maxT;
	Real r = std::pow(P, Real(1.0 / c)) * E / (1 + E);
	return r / (generalDeathRate[y] * (1 - r));
      }
    else
      {
	Real E = std::exp(db_diff * maxT);
	Real r = std::pow(P, Real(1.0 / c)) *
	  (1 - E) / (generalBirthRate[y] - generalDeathRate[y] * E);
	return std::log( (generalBirthRate[y] * r - 1) / 
			 (generalDeathRate[y] * r - 1)) / (db_diff);
      }
  };

  //---------------------------------------------------------------------
  // I/O
  //---------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, 
	     const BirthDeathProbs& tsm)
  {
    return o << "BirthDeathProbs.\n"
	     << "A class for handling parameters and probabilities\n"
	     << "relating to the birth-death model used in reconciliations.\n"
	     << "Also handles sampling from probability distributions\n"
      //Add indentation here, e.g., <<indent = original_indent + 3
	     << tsm.print();
  };


  // print function for parameters used in operator<<
  //--------------------------------------------------------------------
  std::string 
  BirthDeathProbs::print() const
  {
    //I must update this! /bens
    std::ostringstream os;
    os     << "Parameters:\n"
	   << S.getName()
	   << " (species tree):\n"
      //here should be added a S.tree4os statement when this becomes available
	   << "\n"
	   << "birth_rate, death rate, db_diff (their negative difference):\n"
	   << birth_rate << "\t" << death_rate << "\t" << db_diff << "\n"
	   << "\n"
	   << "BD_zero, BD_const, BD_var, generalBirthRate, generalDeathRate\n"
	   << "are  variables derived from birth_rate and death_rate specific\n"
	   << "to vertices in the host tree, and are used in the probability\n"
	   << " calculations.\n"
	   << "\n"
	   << "\n";
      
    return os.str();
  }



  //---------------------------------------------------------------------
  //
  // Implementation
  //
  //---------------------------------------------------------------------

  // calcPt_Ut()
  // Used for the calculation the Pt-value and Ut-value of a child Node.
  // Ut is known as u_t in the paper, and Pt is P(t) in the paper.
  //---------------------------------------------------------------------
  void
  BirthDeathProbs::calcPt_Ut(const Real t, Probability & Pt, Probability & u_t) const
  {
    assert(t >= 0);
    assert(death_rate >= 0);
    assert(birth_rate > 0);
    if (death_rate == birth_rate)
      {
	Probability denominator(1.0 + (death_rate * t)); // I hope birth_rate * t is not too small... - it shouldn't matter as long as it's positive
	Pt = Probability(1.0) / denominator;
	u_t = Probability(death_rate * t) / denominator;
      }
    else if(death_rate == 0)
      {
	Pt = 1.0; 
	u_t = 1.0 - exp(-birth_rate * t);
	assert(u_t != 1.0);
      }
    else
      {
	Probability E = std::exp(db_diff * t);
	Probability denominator = birth_rate - (death_rate * E);
	Pt = -db_diff / denominator; 

	Probability u_t_numerator = birth_rate * (1.0 - E);
	u_t = u_t_numerator / denominator; 
	assert(u_t != 1.0);
      }
    assert(Pt > 0.0);
//     assert(u_t > 0.0); //!\todo{Allowing zero length edges -- problem?}
  }




  // calcBirthDeathProbs()
  //******TO BEOVERLOADED IN INTEGRALDUPSPECPROBS*****
  //----------------------------------------------------------------------
  void
  BirthDeathProbs::calcBirthDeathProbs(Node &root)
  {
    //   if (root.isLeaf() == true) //This should be changed!!!
    //     {
    //       throw AnError("This program cannot handle single species trees just yet. Lasse simply does not have the head to figure it out at the moment.");
    //     }
    //   else
    //     {
//     assert(root.getTime() > 0.0);
    assert(*topTime > 0.0);
    calcBirthDeathProbs_recursive(root);
    //     }
  }

  // calcBirthDeathProbs_recursive()
  // This function is called for nodes below the root.
  //----------------------------------------------------------------------
  void 
  BirthDeathProbs::calcBirthDeathProbs_recursive(Node &y)
  {
    Probability Pt; 
    Probability Ut;
    Real t = 0;
    if(y.isRoot())
      {
	t = *topTime;
      }
    else
      {
	t = y.getTime();
      }
    calcPt_Ut(t, Pt, Ut);

    assert(Pt > 0.0);
//     assert(Ut > 0.0); //!\todo{Allowing zero edge times -- OK?}
    assert(Ut != 1.0);

    if (y.isLeaf() == true)
      {
	BD_const[y] = Pt * (1.0 - Ut);
	BD_var[y]   = Ut;
	BD_zero[y]  = 1.0 - Pt; // Simple expression for leaves

	// General birth and death rates used when sampling edge times
	generalBirthRate[y] = birth_rate;
	generalDeathRate[y] = death_rate;

      }
    else 
      {
	Node &left = *(y.getLeftChild());
	Node &right = *(y.getRightChild());
      
	// Probability of extinction below y is prob of extinction along both 
	// child lineages, given by BD_zeroL and BD_zeroR:
	calcBirthDeathProbs_recursive(left);
	calcBirthDeathProbs_recursive(right);
      
	Probability D = BD_zero[left] * BD_zero[right];
	Probability tmp = 1.0 - (Ut * D);
      
	// Compute the probability of extinction over arc (x, y):
	BD_zero[y] = 1.0 - (Pt * (1.0 - D) / tmp);
      
	// The following prepares for computing e_A(x, y, u). 
	//See also Node.hh!
	BD_const[y] = Pt * (1.0 - Ut) / (tmp * tmp);
	BD_var[y]   = Ut / tmp;
      
	// General birth and death rates used when sampling edge times
	generalBirthRate[y] = birth_rate *(1 - D.val());
	generalDeathRate[y] = death_rate - birth_rate * D.val();
      }
  }



  // Expected number of genes if starting with one gene copy at the root of
  // the species tree.
  //---------------------------------------------------------------------
  Probability
  BirthDeathProbs::expectedNumGenes() const
  {
    Node *r = S.getRootNode();

    // Since there is no time/divergence/distance information associated
    // with the root node, we have to do one call for each child of r and
    // add the two lineages up.

    Node *left = r->getLeftChild();
    Node *right = r->getRightChild();

    // We simply cannot handle too simple trees
    if (left == NULL || right == NULL)
      {
	throw AnError("Species trees are required to have at least two leaves. Sorry!", 1);  // We are probably capable of handling this given that we have a root time!/bens
      }

    Probability el(expectedNumGenes(left));
    Probability er(expectedNumGenes(right));
    return  el + er;
  }

  // How many genes would you expect, according to this model, if you
  // followed only one lineage?
  //---------------------------------------------------------------------
  Probability
  BirthDeathProbs::expectedNumGenesLineage(Real t) const
  {
    Probability Pt;
    Probability u_t;

    calcPt_Ut(t, Pt, u_t);
    return Pt / (1.0 - u_t);
  }


  // Draws t = \tau - waiting time from Distribution function 
  // F(t) = \tilde{\lambda}(1-\tilde{u}_\tau) \tilde{P}(t)/(1-\tilde{u}_t)
  // where \tau = startTime and F(t) = p, this conditioned on 
  // that the process survives, i.e., \tilde{P}(\tau)(1-X)
  // Note! returns startTime - waiting time (a divergence time!)
  Real
  BirthDeathProbs::sampleWaitingTime(Node& x, Real startTime, Probability p)
  {
    assert(p < 1.0);
    assert(p>0);
    assert(startTime > 0);

    // Compute qX = u_\tau, probability of at least one more birth
    Real E = std::exp(db_diff * startTime);
    Probability qX = (generalBirthRate[x] *(1-E)/
		      (generalBirthRate[x]-generalDeathRate[x]*E));
    
    if(p > qX)       // No more births
      {
	return 0;
      }
    else             // There is at least one more birth
      {
	p = p/qX;    // normalize to conditioned on qX

	Real time;
	if(db_diff == 0)
	  {
	    time = startTime * p.val();
	  }
	else
	  {
	    time = -std::log(p.val() * E / (1-E))/db_diff;
	  }
	assert(time != startTime);
	assert(time > 0.0);
	return time;
      }
  }
    
  // The expected number of gene copies after time $t$ is 
  // $P(t)/(1-u_t)$. Thanks to linearity of expectation, 
  // we can simply add up the number of copies below the current node
  // and multiply with what we should have got over the edge down
  // to the current node. 
  //---------------------------------------------------------------------
  Probability
  BirthDeathProbs::expectedNumGenes(Node *v) const
  {
    Probability Pt;
    Probability u_t;

    calcPt_Ut(v->getTime(), Pt, u_t);

    Probability over_edge(Pt  / (1.0 - u_t));
    if (v->isLeaf() == true) 
      {
	return over_edge;
      } 
    else 
      {
	Probability el(expectedNumGenes(v->getLeftChild()));
	Probability er(expectedNumGenes(v->getRightChild()));
	return over_edge * (el + er);
      }
  }



}//end namespace beep
