#include "LogNormDensity.hh"

#include <assert.h>
#include "AnError.hh"
#include "DiscreteGamma.hh"
#include "Probability.hh"

#include <cmath>
#include <sstream>

//----------------------------------------------------------------------
//
// Class LogNormDensity
// implements the logNormal density distribution
//
// Note! alpha = mean and beta = variance of underlying normal distribution
//
// Author: Martin Linder, copyright the MCMC-club, SBC
//
//----------------------------------------------------------------------

namespace beep
{
  using namespace std;
  //----------------------------------------------------------------------
  //
  // Formal defs of static const members
  // I haven't found where this is stated in the standard, but
  // apparently we now need to do this? /bens
  //
  //----------------------------------------------------------------------
  const double LogNormDensity::pi;

  //----------------------------------------------------------------------
  //
  // Construct/Destruct
  //
  //----------------------------------------------------------------------
  LogNormDensity::LogNormDensity(Real mean, Real variance, bool embedded)
    : Density2P_positive(mean, variance, "LogNorm"),
      c()
  {
    if(embedded)
      {
	setEmbeddedParameters(mean, variance);
      }
    else
      {
	setParameters(mean, variance);
      }
  };

  LogNormDensity::LogNormDensity(const LogNormDensity& df)
    : Density2P_positive(df),
      c(df.c)
  {
  };

  LogNormDensity::~LogNormDensity() 
  {};

  LogNormDensity& 
  LogNormDensity::operator=(const LogNormDensity& df)
  {
    if(&df != this)
      {
	Density2P_positive::operator=(df);
	c = df.c;
      }
    return *this;
  }

  //----------------------------------------------------------------------
  //
  // Interface
  //
  //----------------------------------------------------------------------

  // Density-, distribution-, and sampling functions
  //----------------------------------------------------------------------
  Probability 
  LogNormDensity::operator()(const Real& x) const
  {
    if(x <= 0)
      return 0.0;
    else
      {
	Real logx = std::log(x);
	return Probability::setLogProb( -0.5 * std::pow(logx - alpha, 2)/beta
					- logx + c, 1);
      }
  }

  Probability 
  LogNormDensity::operator()(const Real& x, const Real& interval) const
  {
    return (alnorm((std::log(x+interval/2)-alpha)/std::sqrt(beta), false) - 
	    alnorm((std::log(x-interval/2)-alpha)/std::sqrt(beta), false));
  }
  
  Real
  LogNormDensity::sampleValue(const Real& p) const
  {
    assert(0 < p && p < 1.0); // check precondition

    return std::exp(std::pow(beta, 0.5) * gauinv(p) + alpha);
  }

  // Access parameters
  //----------------------------------------------------------------------
  Real
  LogNormDensity::getMean() const 
  {
    return std::exp(alpha + 0.5 * beta);
  }
    
  Real
  LogNormDensity::getVariance() const 
  {
    Real ebeta = std::exp(beta);
    Real e2alpha = std::exp(2 * alpha);
    return e2alpha * ebeta *(ebeta -1);
  }
    
  // Set Embedded Parameters
  // Precondition: isInRange(first) && isInRange(second)
  //----------------------------------------------------------------------
  void 
  LogNormDensity::setEmbeddedParameters(const Real& first, const Real& second)
  {
    assert(std::abs(first) < Real_limits::max() && isInRange(second)); // check precondition

    alpha = first;
    beta = second;
    c = -0.5 * std::log(2 * pi * beta);
    return;
  }
  void 
  LogNormDensity::setParameters(const Real& mean, const Real& variance)
  {
    assert(isInRange(mean) && isInRange(variance)); // check precondition

    Real lnm = std::log(mean);
    beta = std::log(variance / std::pow(mean, 2) + 1);
    alpha = lnm - 0.5 * beta;
    c = -0.5 * std::log(2 * pi * beta);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  LogNormDensity::setMean(const Real& mean)
  {
    assert(isInRange(mean)); // check precondition

    Real variance = getVariance();
    beta = std::log(variance / std::pow(mean, 2) + 1);
    alpha = std::log(mean) - 0.5 * beta;
    c = -0.5 * std::log(2 * pi * beta);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }
    
  void 
  LogNormDensity::setVariance(const Real& variance)
  {
    assert(isInRange(variance)); // cheak precondition

    Real mean = getMean();
    Real lnm = std::log(mean);
    beta = std::log(variance / std::pow(mean, 2) + 1);
    alpha = lnm - 0.5 * beta;
    c = -0.5 * std::log(2 * pi * beta);

    //Check postcondition - note that we may have precision error
    assert(2 * std::abs(getMean() - mean) / (getMean() + mean) < 1e-5);
    assert(2*std::abs(getVariance()-variance)/(getVariance()+variance)<1e-5); 
    return;
  }


  //------------------------------------------------------------------
  // I/O
  //------------------------------------------------------------------
  std::string
  LogNormDensity::print() const
  {
    std::ostringstream oss;
    oss << "Log Normal distribution LogN("
	<< alpha
	<< ", "
	<< beta
	<< ")\n" 
      ;
    return oss.str();
  }

}//end namespace beep

