#include "SeqIO.hh"

#include <fstream>

#include "AnError.hh"
#include "SequenceType.hh"


namespace beep
{
using namespace std;

SeqIO::SeqIO()
: slist(NULL),
guessedType(NULL),
DNA_likelihood(0.0),
AA_likelihood(0.0)
{
}

SeqIO::~SeqIO() {
	seq_free(slist);
}

SequenceData
SeqIO::readSequences(const string &filename)
{
	SeqIO sio;
	try
	{
		sio.importData(filename);
	}
	catch( AnError& a )
	{
		if(!sio.importDataFormat2(filename))
			throw a;
	}

	//
	// Now do actual conversion
	//
	SequenceData SD(*sio.guessedType);
	if(sio.data.empty())
	{
		for (seq* sl = sio.slist; sl != NULL; sl = sl->next)
		{
			SD.addData(seq_locus(sl), sl->sequence);
		}
	}
	else
	{
		for (vector<pair<string,string> >::iterator it = sio.data.begin(); it != sio.data.end(); it++)
		{
			SD.addData((*it).first, (*it).second);
		}
	}        
	return SD;
}

//
// Read sequences and impose the given sequence type. 
// You can not force a sequence type with zero probability, because
// that implies a sequence symbol which is not in the type's alphabet.
//
SequenceData
SeqIO::readSequences(const string &filename, const string &ststr)
{
	SequenceType st = SequenceType::getSequenceType(ststr);
	return readSequences(filename, st);
}

SequenceData
SeqIO::readSequences(const string &filename, const SequenceType &st)
{
	SeqIO sio;
	try
	{
		sio.importData(filename);
	}
	catch( AnError& a )
	{
		if(!sio.importDataFormat2(filename))
			throw a;
	}



	if (st == myAminoAcid && sio.AA_likelihood == 0.0) {
		throw AnError("The read sequence cannot be of type AminoAcid, which was required.");
	} else if ((st == myDNA || st == myCodon) && sio.DNA_likelihood == 0.0) {
		throw AnError("The read sequence cannot be DNA, which was required.");
	}

	SequenceData SD(st);

	if(sio.data.empty())
	{
		for (seq* sl = sio.slist; sl != NULL; sl = sl->next)
		{
			SD.addData(seq_locus(sl), sl->sequence);
		}
	}
	else
	{
		for (vector<pair<string,string> >::iterator it = sio.data.begin(); it != sio.data.end(); it++)
		{
			SD.addData((*it).first, (*it).second);
		}
	}        
	return SD;
}

void
SeqIO::importData(const string &filename)
{
	// joelgs: Had to change below to be able to compile on v. 4.3.
	//char *str = strdup(filename.c_str());
	//sfile * sf = seq_open(str, "r");
	//delete str;
	vector<char> str(filename.begin(), filename.end());
	str.push_back('\0');
	char rMode[] = { 'r', '\0' };
	sfile* sf = seq_open(&str[0], &rMode[0]);
	
	if (sf)
	{
		int n_seqs = 0;
		slist = seq_read_all(sf, &n_seqs);
		seq_close(sf);

		if (n_seqs == 0){
			throw AnError("No parseable sequences found in given file.", filename);
		}

		// A priori, no preference
		Probability dna_prob = 0.5;
		Probability protein_prob = 0.5;
                //std::cout << n_seqs << endl;
                
		for (seq* sl = slist; sl != NULL; sl = sl->next)
		{
                    //std::cout << sl->seqlen << "," << sl->sequence << endl;
			dna_prob *= beep::myDNA.typeLikelihood(sl->sequence);
			protein_prob *= beep::myAminoAcid.typeLikelihood(sl->sequence);
			if (dna_prob == 0.0 && protein_prob == 0.0)
			{
				break;
			}
		}
		DNA_likelihood = dna_prob;
		AA_likelihood = protein_prob;

		if (dna_prob == 0.0 && protein_prob == 0.0)
		{
			throw AnError("Does not recognize sequences as either DNA or protein.");
		}
		else if (dna_prob > protein_prob)
		{
			guessedType = &beep::myDNA;
		}
		else
		{
			guessedType = &beep::myAminoAcid;
		}

	}
	else
	{
		throw AnError("Could not open sequence file.", filename);
	}				
}

bool
SeqIO::importDataFormat2(const string &filename)
{
	std::ifstream  in(filename.c_str());
	unsigned int noOfSeqs;
	unsigned int seqLengths;

	if(!in)
		throw AnError("Could not open sequence file.", filename);


	if(!(in >> noOfSeqs) || !(in >> seqLengths))
		return false;

	string locus;
	Probability dna_prob = 0.5;
	Probability protein_prob = 0.5;

	while( in >> locus )
	{
		string sequence;
		in >> sequence;
		data.push_back(pair<string,string>(locus,sequence));
	}


	for (vector<pair<string,string> >::iterator it = data.begin(); it != data.end(); it++)
	{
		dna_prob *= beep::myDNA.typeLikelihood((*it).second);
		protein_prob *= beep::myAminoAcid.typeLikelihood((*it).second);
		if (dna_prob == 0.0 && protein_prob == 0.0)
		{
			break;
		}
	}
	DNA_likelihood = dna_prob;
	AA_likelihood = protein_prob;

	if (dna_prob == 0.0 && protein_prob == 0.0)
	{
		throw AnError("Does not recognize sequences as either DNA or protein.");
	}
	else if (dna_prob > protein_prob)
	{
		guessedType = &beep::myDNA;
	}
	else
	{
		guessedType = &beep::myAminoAcid;
	}
	return true;
}

}//end namespace beep

