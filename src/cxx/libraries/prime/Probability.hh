#ifndef PROBABILITY_HH
#define PROBABILITY_HH

#include <boost/mpi/environment.hpp> 
#include <boost/mpi/communicator.hpp> 
#include <boost/mpi.hpp>
namespace mpi = boost::mpi; 

// include headers that implement a archive in simple text format
// #include <boost/archive/text_oarchive.hpp>
// #include <boost/archive/text_iarchive.hpp>

#include <iostream>
#include "Beep.hh"

//#ifdef PROBABILITY_CHEAT
//typedef double Probability;
//#else

//----------------------------------------------------------------------
//Author: Bengt Sennblad
//�the MCMC-club, Stockholm Bioinformatics Center, 2002
//----------------------------------------------------------------------


//----------------------------------------------------------------------
//
// Member functions for Class Probability:
// This class holds the logProbabilities of a number, and defines 
// overloaded operators for two Probability variables:
//         +=, -=, *=, /=, +,-,*,/, <, >, <=, >=
// and the following functions for a Probability and an int variable
//         power, factorial, binomial
//
// Note that this overrides the std::pow/log, so be explicit if you want
// to use that.
//
// All functions return a Probability and none alters the original 
// parameter values
//
// This is somewhat old and could probably be better implemented, but 
// does what it should anyway.
//
//----------------------------------------------------------------------

namespace beep
{
  class Probability;
  // Forward declarations of friend functions
     Probability pow(const Probability& p, const double& n); // Why refs?
     Probability exp(const Probability& p);
     Probability log(const Probability& p);
     Probability probFact(unsigned u);
     Probability probBinom(unsigned u1, unsigned u2);



  class Probability 
  {
  public:
    Probability();      
    Probability(const Real& d);      
    //! \todo{ there is a problem with constructing Probabilities from unsigned
    //! including using Probaility::operator- with unsigned. Current 
    //! workaround is to use static_cast<Real> on the unsigend, but
    //! this must be fixed presently}
    Probability(const Probability& P);

    // Named constructor for low lever communication etc.
    static Probability setLogProb(double logProb, int sign);

    //---------------------------------------------------------------------
    //
    //Functions to access the log probability directly
    //
    //---------------------------------------------------------------------
    double getLogProb() const;
    int    getSign() const;

    //---------------------------------------------------------------------
    //
    //Household function - getting the exponentiated value
    //
    //---------------------------------------------------------------------
    double val() const;
    //  operator Real();

    //---------------------------------------------------------------------
    //
    //Assignment operators
    //
    //---------------------------------------------------------------------
    Probability& operator=(const Probability& q);
    //  Probability& operator=(const double& d);
    Probability& operator+=(const Probability& q);
    Probability& operator-=(const Probability& q);
    Probability& operator*=(const Probability& q);
    Probability& operator/=(const Probability& q);

    //---------------------------------------------------------------------
    //
    //Logical operators
    //
    //---------------------------------------------------------------------

    //Logical operators for two Probaility variables
    //---------------------------------------------------------------------
    bool operator>(const Probability& q) const;
    bool operator<(const Probability& q) const;
    bool operator>=(const Probability& q) const; 
    bool operator<=(const Probability& q) const; 
    bool operator==(const Probability& q) const; 
    bool operator!=(const Probability& q) const; 

    //---------------------------------------------------------------------
    //
    //Log Arithemtics and operators for two Probability variables
    //
    //---------------------------------------------------------------------
    friend Probability 
    operator+(const Probability& p, const Probability& q);
    friend Probability 
    operator-(const Probability& p, const Probability& q);
    friend Probability 
    operator*(const Probability& p, const Probability& q);
    friend Probability 
    operator/(const Probability& p, const Probability& q); 

    //---------------------------------------------------------------------
    //
    //Power and exp of a Probability and unary negation operator
    //
    //---------------------------------------------------------------------
    //   friend Probability pow(const Probability& p, const unsigned& n);
    //   friend Probability pow(const Probability& p, const int& n);
    friend Probability pow(const Probability& p, const double& n); // Why refs?
    friend Probability exp(const Probability& p);
    friend Probability log(const Probability& p);
    Probability operator-() const;

    //---------------------------------------------------------------------
    //
    //Factorial and binomial ("u1 choose u2") of sunsigneds  
    //using logarithms - returniong Probabilities
    //
    //---------------------------------------------------------------------
    friend Probability probFact(unsigned u);
    friend Probability probBinom(unsigned u1, unsigned u2);

    //--- I/O -------------------------------------------------------------
    //
    // Write probabilities to a stream
    //
    //---------------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const Probability& p);

  public:
    //---------------------------------------------------------------------
    // mpi serialization functions
    //---------------------------------------------------------------------
    friend class boost::serialization::access; 
  
    template<class Archive> 
    void serialize(Archive & ar, const unsigned int version)
    {
      ar & p;
      ar & sign;
    }

  private:
    //----------------------------------------------------------------------
    //
    //Helper arithmetics finctions
    //
    //----------------------------------------------------------------------
  
    //Helper for addition and subtraction of two Probabilities
    //---------------------------------------------------------------------
    void add(const Probability& q);
    void subtract(const Probability& q);


  private:
  public:
    //---------------------------------------------------------------------
    //
    //Class variable - contains the log probability
    //
    //---------------------------------------------------------------------

    long double p;
    int sign;   //-1 = negative, 0 = zero, 1 = positive

  };
}//end namespace beep

namespace boost { 
  namespace mpi { 
    template<> 
    struct is_mpi_datatype<beep::Probability> : mpl::true_ { }; 
  }
} 

//#endif

#endif




  
