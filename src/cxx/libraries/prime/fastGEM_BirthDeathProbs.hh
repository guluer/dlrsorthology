#ifndef FASTGEM_BIRTHDEATHPROBS_HH
#define FASTGEM_BIRTHDEATHPROBS_HH

#include <algorithm>
#include <vector>

#include "BirthDeathProbs.hh"
#include "GenericMatrix.hh"
//#include "Generic3DMatrix.hh"
#include "Probability.hh"

namespace beep
{

//! This class
//
  class fastGEM_BirthDeathProbs : public BirthDeathProbs 
  {
  public:
    //----------------------------------------------------------------------
    //
    //Constructors and Destructors and Assignment
    //
    //----------------------------------------------------------------------
    fastGEM_BirthDeathProbs(Tree& S, unsigned noOfDiscrPoints, std::vector<double>* discrPoints, const Real& birthRate, const Real& deathRate);
    virtual ~fastGEM_BirthDeathProbs();

    //----------------------------------------------------------------------
    //
    // Interface
    //
    //----------------------------------------------------------------------
    void update();
    void calcP11();
    Probability getP11dupValue(unsigned Sindex,unsigned xIndex);
    Probability getP11specValue(unsigned Sindex);
    Probability getLossValue(unsigned Sindex);
    Real getPxTime(unsigned Sindex, unsigned xIndex);
    std::vector<double>* getDiscrPoints();

    //---------------------------------------------------------------------
    // I/O
    //---------------------------------------------------------------------

    //---------------------------------------------------------------------
    //
    // Implementation
    //
    //---------------------------------------------------------------------
  private:
    Probability calcP11item(const Real pxTime, const Real xTime, Node &Snode) const;
    void setP11dupValue(unsigned Sindex,unsigned xIndex, Probability p);
    void setP11specValue(unsigned Sindex, Probability p);
    void setLossValue(unsigned Sindex, Probability p);
    void fillPxTimeTable();

    //------------------------------------------------------------------------
    // Attributes
    //
    //------------------------------------------------------------------------
    unsigned noOfDiscrPoints;
    std::vector<double>* discrPoints; 
    GenericMatrix<Probability> P11dup;
    std::vector<Probability> P11spec; 
    std::vector<Probability> loss; 
    Real timeStep;
    GenericMatrix<Real> pxTimes;
  };

}//end namespace beep

#endif
