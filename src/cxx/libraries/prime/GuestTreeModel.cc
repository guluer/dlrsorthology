#include <cassert>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "GuestTreeModel.hh"
#include "TreeAnalysis.hh"


namespace beep
{
  using namespace std;

  //------------------------------------------------------------------------
  //
  //
  // class GUESTTREEMODEL
  // Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved
  //
  //
  //------------------------------------------------------------------------

  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  GuestTreeModel::GuestTreeModel(Tree &G_in, 
				 StrStrMap &gs_in,
				 BirthDeathProbs &bdp_in)
    : ReconciliationModel(G_in, gs_in, bdp_in),
      S_A(*S, G_in),
      S_X(*S, G_in),
      doneSA(*S, G_in),
      doneSX(*S, G_in),
      orthoNode(0)
  {
    inits();		   // Compute important helper params
  }

  GuestTreeModel::GuestTreeModel(ReconciliationModel& rs)
    : ReconciliationModel(rs),
      S_A(*S, *G),
      S_X(*S, *G),
      doneSA(*S, *G),
      doneSX(*S, *G),
      orthoNode(0)
  {
     inits();		   // Compute important helper params
  }


  GuestTreeModel::GuestTreeModel(const GuestTreeModel &M)
    : ReconciliationModel(M),
      S_A(M.S_A),
      S_X(M.S_X),
      doneSA(M.doneSA),
      doneSX(M.doneSX),
      orthoNode(0)
  {
    inits();

  }

 
  GuestTreeModel::~GuestTreeModel()
  {
    //  delete gamma;
  }


  GuestTreeModel &
  GuestTreeModel::operator=(const GuestTreeModel &M)
  {
    if (this != &M)
      {
	ReconciliationModel::operator=(M);
	S_A = M.S_A;
	S_X = M.S_X;
	doneSA = M.doneSA;
	doneSX = M.doneSX;
	orthoNode = 0;
      }
    inits();
    return *this;
  }


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------

  // Updating stuff when the gene tree has changed /this is cheap so O.K. /bens
  //-------------------------------------------------------------------------
  void
  GuestTreeModel::update()
  {
    ReconciliationModel::update();
    inits();
  }


  //------------------------------------------------------------------------
  // Calculations and computations
  //------------------------------------------------------------------------

  // If u=NULL, then calculateData Probability() returns Pr[G], else if, 
  // e.g., u is the LCA of v and w, calculateDataProbability() returns 
  // Pr[G and v and w are orthologs].
  //------------------------------------------------------------------------
  void 
  GuestTreeModel::setOrthoNode(const Node* u)
  {
    orthoNode = u;
  };
  
  // Since calculateDataProbability often is called both in e.g., 
  // ReconciliationSampler and in, e.g., GeneTreeMCMC, we get redundant 
  // calculations done in a single MCMC-iteartions. Thus, maybe we should 
  // include a flag showing if the function needs to be recalculated. 
  // I leave it for now, though. /bens
  //------------------------------------------------------------------------
  Probability
  GuestTreeModel::calculateDataProbability()
  {
    // Clear old values - Stupid way of doing it?
    doneSA = doneSX = NodeNodeMap<unsigned>(*S,*G, 1);

    Node& rootS = *S->getRootNode();
    Node& rootG = *G->getRootNode();
    
    // Compute the S_A and S_X structures
    computeSA(rootS,rootG); 
    return S_A(rootS, rootG);
  }

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const GuestTreeModel& pm)
  {
    return o << pm.print();
  };
  
  string 
  GuestTreeModel::print() const
  {
    std::ostringstream oss;
    oss << "GuestTreeModel: Computes the likelihood of a gene.\n"
	<< "tree given a species tree, by summing over all \n"
	<< "reconciliations.\n"
	<< indentString(G->getName() + " (guest tree)\n");
    return oss.str();
  }
  
  
  // Debugging support macros
  //-------------------------------------------------------------------
#ifdef DEBUG_DP
#define DEBUG_SX(S,X,U,K) {cerr << S << ":\tS_X(" << X.getNumber() << ", " << U.getNumber() << ", " << K <<  ") = " << S_X(X,U)[K-1].val() << endl;}
#define DEBUG_SA(S,X,U) {cerr << S << ":\tS_A(" << X.getNumber() << ", " << U.getNumber() << ") = " << S_A(X,U).val() << endl;}

#define DEBUG_S_X(S,X,U,K) {cerr << S << ":\tS_X(" << X->getNumber() << ", " << U->getNumber() << ", " << K <<  ") = " << S_X(X,U)[K-1].val() << endl;}
#define DEBUG_S_A(S,X,U) {cerr << S << ":\tS_A(" << X->getNumber() << ", " << U->getNumber() << ") = " << S_A(X,U).val() << endl;}
#else
#define DEBUG_SX(S,X,U,K)
#define DEBUG_SA(S,X,U)

#define DEBUG_S_X(S,X,U,K)
#define DEBUG_S_A(S,X,U)
#endif



  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  // computes \f$ s_V(x,u) \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  void 
  GuestTreeModel::computeSV(Node& x, Node& u)
  {
    assert(x.dominates(*sigma[u])); //check precondition

    if(sigma[u] == &x)
      {
	if(x.isLeaf())
	  {
	    assert(u.isLeaf());
	    S_X(x,u)[0] = 1.0;
	  }
	else
	  {
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    Node& y = *x.getDominatingChild(sigma[v]);
	    Node& z = *x.getDominatingChild(sigma[w]);
	    
	    assert(y.getSibling() == &z); // Check sanity of recursion
	    computeSA(y,v);
	    computeSA(z,w);
	    S_X(x,u)[0] = S_A(y,v) * S_A(z,w);	
	  }
      }
    else
      {
	Node& y = *x.getDominatingChild(sigma[u]);
	Node& z = *y.getSibling();
	computeSA(y,u);
	computeSA(z,u);
	
	S_X(x,u)[0] = S_A(y,u) * S_A(z,u);  //bdp->partialProbOfCopies(z, 0);
      }
    DEBUG_SX("ComputeSX", x, u, 1);
  }

  // computes \f$ s_A(x,u) \f$
  //-----------------------------------------------------------------
  void 
  GuestTreeModel::computeSA(Node& x, Node& u)
  {
    // Check if S_A(x,u) is already filled in...
    if(doneSA(x,u) == 0)
      {
	return;
      }
    // ...else fill it in and set flag correspondingly
    doneSA(x,u) = 0;

    Probability p = 0;

    // if u \in S_x, then s_A(x,u) = \sum_k\in[|L(G:U)|}Q_x(k)s_X(x,u,k)
    if(x.dominates(*sigma[u]))
      {
	computeSX(x,u);
	
	for(unsigned k = slice_L(x, u); k <= slice_U[u]; k++) 
	  {
	    if(x.isRoot())
	      {
		p += S_X(x,u)[k-1] * bdp->topPartialProbOfCopies(k);	  
	      }
	    else
	      {
		p += S_X(x,u)[k-1] * bdp->partialProbOfCopies(x, k);	  
	      }
	  }
      }
    // if u \not\in S_x, then s_A(x,u) = X_A(x)
    else
      {
	p = bdp->partialProbOfCopies(x,0); 
      }
    
    S_A(x,u) = p;
    DEBUG_SA("ComputeSA", x, u);
  }
  
  // computes \f$ s_X(x,u,k), k\in[|L(G_u)|] \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  void 
  GuestTreeModel::computeSX(Node& x, Node& u)
  {
    assert(x.dominates(*sigma[u])); // check precondition
    
    // Check if S_A(x,u) is already filled in...
    if(doneSX(x,u) == 0)
      {
	return;
      }
    // ...else fill it in and set flag correspondingly
    doneSX(x,u) = 0;

    unsigned U = slice_U[u];
    unsigned L = slice_L(x, u);
    S_X(x, u).assign(U, 0); // all elements initiated to 0
    
    if(L == 1)
      {
	computeSV(x,u);
      }
    if(&u != orthoNode)
      {
	for(unsigned k = std::max(2u, L); k <= U; k++) 
	  {	
	    Probability sum(0.0);
	    
	    Probability factor(1.0 / (k-1));

	    // Multiply factor with 2 if needed
	    adjustFactor(factor, u);

	    // Loop through all valid solutions to k1 + k2 = k and sum
	    // $S_X(x, left, k1)S_X(x, right, k2)$
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    computeSX(x,v);
	    computeSX(x,w);
	    
	    unsigned Lv  = slice_L(&x,&v);
	    unsigned Uv  = slice_U[&v];
	    unsigned Lw = slice_L(&x,&w);
	    unsigned Uw = slice_U[&w];
	    assert(Lv > 0);
	    assert(Lw > 0);
	    for (unsigned k1 = Lv; k1 <= Uv; k1++)
	      {
		unsigned k2 = k - k1;
		if (k2 <= Uw && k2 >= Lw) 
		  {
		    sum +=  S_X(x,v)[k1-1] * S_X(x,w)[k2-1]; 
		  }
	      }
	    S_X(x, u)[k - 1] = factor * sum;
	    DEBUG_SX("ComputeSX", x, u, k);
	  }
      }
    return;
  }

  //! multiplies with two if subtrees are non-isomorphic
  //------------------------------------------------------------------------
  void 
  GuestTreeModel::adjustFactor(Probability& factor, Node& u)
  {
    if(isomorphy[u] == false)
      {
	factor *= 2;
      }
    return;
  }	      










  //------------------------------------------------------------------------
  //
  //
  // class LabeledGUESTTREEMODEL
  //
  //
  //------------------------------------------------------------------------
  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  LabeledGuestTreeModel::LabeledGuestTreeModel(Tree &G_in, 
					       StrStrMap &gs_in,
					       BirthDeathProbs &bdp_in)
    : GuestTreeModel(G_in, gs_in, bdp_in),
      nLabeling(probFact(G->getNumberOfLeaves()))
  {
    inits();		   // Compute important helper params
  }

  LabeledGuestTreeModel::LabeledGuestTreeModel(ReconciliationModel& rs)
    : GuestTreeModel(rs),
      nLabeling(probFact(G->getNumberOfLeaves()))
  {
     inits();		   // Compute important helper params
  }


  LabeledGuestTreeModel::LabeledGuestTreeModel(const LabeledGuestTreeModel &M)
    : GuestTreeModel(M),
      nLabeling(M.nLabeling)
  {
    //  WARNING1("LabeledGuestTreeModel copy constructor used. Probably buggy!");
    inits();
  }

 
  LabeledGuestTreeModel::~LabeledGuestTreeModel()
  {
    //  delete gamma;
  }


  LabeledGuestTreeModel &
  LabeledGuestTreeModel::operator=(const LabeledGuestTreeModel &M)
  {
    if (this != &M)
      {
	GuestTreeModel::operator=(M);
	nLabeling = M.nLabeling;
      }
    inits();
    return *this;
  }

  //-----------------------------------------------------------------------
  //
  // Interfacec
  //
  //-----------------------------------------------------------------------

  // normalize with number of possible labelings
  //------------------------------------------------------------------------
  Probability
  LabeledGuestTreeModel::calculateDataProbability()
  {
    return GuestTreeModel::calculateDataProbability();
  }

  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  //! multiplies with two always
  //------------------------------------------------------------------------
  void 
  LabeledGuestTreeModel::adjustFactor(Probability& factor, Node& u)
  {
    factor *= 2;
    return;
  }	      

}// end namespace beep
