#include "HybridTree.hh"
#include "Node.hh"
#include "AnError.hh"
#include "BeepVector.hh"

#include <sstream>
#include <stdlib.h>

namespace beep
{
  using namespace std;
  //-----------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //-----------------------------------------------------------
  HybridTree::HybridTree()
    : Tree(),
      otherParent(),
      extinct(),
      hybrid2Binary(),
      binary2Hybrid(),
      bTree()
  {};

  HybridTree::HybridTree(const Tree& T)
    : Tree(),
      otherParent(),
      extinct(),
      hybrid2Binary(),
      binary2Hybrid(),
      bTree()
  {
    if(T.getRootNode())
      {
	setRootNode(buildFromBinaryTree(T.getRootNode()));
      }
    else
      {
	rootNode = 0;
      }
    if(T.hasTimes())
      {
	times = new RealVector(T.getTimes());
      }
    updateBinaryTree();
  };

  HybridTree::~HybridTree()
  {
    clearTree();
  };

  HybridTree::HybridTree(const HybridTree& T)
    : Tree(),
      otherParent(),
      extinct(),
      hybrid2Binary(),
      binary2Hybrid(),
      bTree()
  {
    noOfNodes = T.noOfNodes;
    noOfLeaves = T.noOfLeaves;
    if(noOfNodes > all_nodes.size())
      {
	all_nodes.resize(noOfNodes,NULL);
      }
    name = T.name;
    if(T.getRootNode())
      {
	setRootNode(copyAllNodes(T.getRootNode()));
	perturbed_node = rootNode;
      }
    for(map<const Node*, unsigned>::const_iterator i = T.extinct.begin(); 
	i != T.extinct.end(); i++)
      {
	const Node* thisNode = getNode(i->first->getNumber());
	extinct[thisNode] = 1;
      }
    if(T.times)
      {
	setTimes(*new RealVector(*T.times), true);
      }
    if(T.rates)
      {
	setRates(*new RealVector(*T.rates),true);
      }
    if(T.lengths)
      {
	setLengths(*new RealVector(*T.lengths),true);
      }
    setTopTime(T.topTime);
    updateBinaryTree();
  };

  HybridTree&
  HybridTree::operator=(const HybridTree& T)
  {
    if(this != &T)
      {
	clear();
	noOfNodes = T.noOfNodes;
	noOfLeaves = T.noOfLeaves;
	if(noOfNodes > all_nodes.size())
	  {
	    all_nodes.resize(noOfNodes,NULL);
	  }
	name = T.name;
	if(T.getRootNode())
	  {
	    setRootNode(copyAllNodes(T.getRootNode()));
	    perturbedTree(true);
	  }
	for(map<const Node*, unsigned>::const_iterator i = T.extinct.begin(); 
	    i != T.extinct.end(); i++)
	  {
	    const Node* thisNode = getNode(i->first->getNumber());
	    extinct[thisNode] = 1;
	  }
	if(T.times)
	  {
	    setTimes(*new RealVector(*T.times), true);
	  }
	if(T.rates)
	  {
 	    setRates(*new RealVector(*T.rates),true);
	  }
	if(T.lengths)
	  {
 	    setLengths(*new RealVector(*T.lengths),true);
	  }	
	setTopTime(T.topTime);
	updateBinaryTree();
      }
    return *this;
  };
    
  //-----------------------------------------------------------
  //
  // Interface
  //
  //-----------------------------------------------------------
  //Overloading selected functions from Tree
  //---------------------------------------------------------------
  unsigned 
  HybridTree::getNumberOfLeaves() const
  {
    return Tree::getNumberOfLeaves() - extinct.size();
  }

  Node* 
  HybridTree::addNode(Node* leftChild, Node* rightChild, unsigned id,
		      std::string name, bool extinctNode)
  {
    assert(extinctNode == false || (leftChild == NULL && rightChild == NULL));

    Node* v = Tree::addNode(leftChild, rightChild, id, name);
    if(extinctNode)
      {
	extinct[v] = 1;
      }
    else if(isExtinct(*v))
      {
	extinct.erase(v);
      }
    assert(v != 0);
    
    return v;
  }
    
  void
  HybridTree::setTime(const Node& n, Real t) const
  {
    assert(n.isLeaf() || n.isRoot() ||
	   (t >= getTime(*n.getLeftChild()) && 
	    t >= getTime(*n.getRightChild()) && 
	    t <= getTime(*n.getParent())));

    Node* h = getHybridChild(n);    
    (*times)[&n] = t;
    if(h)
      {
	Node* op = h->getParent();
	if(&n == op)
	  {
	    op = getOtherParent(*h);
	  }
	if(op == NULL)
	  {
	    throw AnError("HybridTree::setTime():\n"
			  "op is NULL for hybridNode",1);
	  }
	assert(op->isLeaf() || op->isRoot() ||
	       (t >= getTime(*op->getLeftChild()) && 
		t >= getTime(*op->getRightChild()) && 
		t <= getTime(*op->getParent())));
	(*times)[op] = t;
      }
    return;
  }

  void
  HybridTree::setEdgeTime(const Node& v, Real time) const
  {
    if(v.isRoot())
      {
	topTime = time;
      }
    else
      {
        (*times)[v] = (*times)[v.getParent()] - time;

	assert((*times)[v] > (*times)[v.getLeftChild()]);
	assert((*times)[v] > (*times)[v.getRightChild()]);

	Node* h = getHybridChild(v);    
	if(h)
	  {
	    // Hybrids parent must have same time
	    Node* op = h->getParent();
	    if(&v == op)
	      {
		op = getOtherParent(*h);
	      }
	    assert((*times)[v] > getTime(*op->getLeftChild())); 
	    assert((*times)[v] > getTime(*op->getRightChild()));
	    // If we have an extinction node then this might be a 
	    // autopolyploidy, which can yield a zero edge time
	    if(isExtinct(*h->getSibling()) || 
	       isExtinct(*getOtherSibling(*h)))
	      {
		assert((*times)[v] <= getTime(*op->getParent()));
	      }
	    else
	      {
		assert((*times)[v] < getTime(*op->getParent()));
	      }

	    (*times)[op] = (*times)[v];
	  }
      }
  }

  // Access and test of to hybrid-specific relations
  //---------------------------------------------------------------
  Node*
  HybridTree::getOtherParent(const Node& n) const
  {
    if(isHybridNode(n))
      {
	return otherParent.find(&n)->second;
      }
    else
      {
	return 0;
      }
  }
  
  void
  HybridTree::setOtherParent(const Node& child, Node* parent) // parent can be NULL
  {
    if(parent)
      {
	otherParent[&child] = parent;
      }
    else
      {
	otherParent.erase(&child);
      }
  }
  
  void 
  HybridTree::switchParents(Node& child)
  {
    Node* p = child.getParent();
    Node* op = getOtherParent(child);

    // check sanity
    assert(p->getLeftChild() == &child || p->getRightChild() == &child);
    assert(op->getLeftChild() == &child || op->getRightChild() == &child);

    setOtherParent(child, p);
    child.setParent(op);
  }

  Node*
  HybridTree::getOtherSibling(const Node& n) const
  {
    if(isHybridNode(n))
      {
	Node& op = *otherParent.find(&n)->second;
	if(op.getLeftChild() == &n)
	  {
	    return op.getRightChild();
	  }
	else
	  {
	    return op.getLeftChild();
	  }
      }
    else
      {
	return 0;
      }
  }

  Node*
  HybridTree::getHybridChild(const Node& u) const
  {
    Node* h = 0;
    if(u.isLeaf() == false)
      {
	if(isHybridNode(*u.getLeftChild()))
	  {
	    h = u.getLeftChild();
	  }
	if(isHybridNode(*u.getRightChild()))
	  {
	    h = u.getRightChild();
	  }
      }
    return h;
  }
  
  bool
  HybridTree::isBinaryNode(const Node& n) const
  {
    if(n.isLeaf() || isHybridNode(n) || isExtinct(n))
      {
        return false;
      }
    else
      {
        return true;
      }
  }

  bool
  HybridTree::isHybridNode(const Node& n) const
  {
    if(otherParent.find(&n) == otherParent.end())
      {
	return false;
      }
    else
      {
	return true;
      }
  };

  bool 
  HybridTree::isExtinct(const Node& n) const
  {
    if(extinct.find(&n) == extinct.end())
      {
	return false;
      }
    else
      {
	return true;
      }
  }

  Tree&
  HybridTree::getBinaryTree() const
  {
    if(rootNode && bTree.getRootNode() == false)
      {
	updateBinaryTree();
      }
    return bTree;
  }

  vector<Node*> 
  HybridTree::getCorrespondingBinaryNodes(Node* h)
  {
    if(hybrid2Binary.find(h) == hybrid2Binary.end())
      {
	throw AnError("HybridTree::getCorrespondingBinaryNodes::"
		      "hybrid2Binary is not initiated", 1);
      }
    return hybrid2Binary[h];
  }
  Node*
  HybridTree::getCorrespondingHybridNode(Node* b)
  {
    if(binary2Hybrid.find(b) == binary2Hybrid.end())
      {
	throw AnError("HybridTree::getCorrespondingHybridNode::"
		      "binary2Hybrid is not initiated", 1);
      }
    return binary2Hybrid[b];
  }

  unsigned 
  HybridTree::countHybrids()
  {
    return otherParent.size();
  }
  unsigned 
  HybridTree::countExtinct()
  {
    return extinct.size();
  };

  //---------------------------------------------------------------
  // Access to hybrid specific attributes, used by TreeIO
  //---------------------------------------------------------------
  map<const Node*, Node*>* 
  HybridTree::getOPAttribute() const
  {
    return &otherParent;
  }
  map<const Node*, unsigned>* 
  HybridTree::getEXAttribute() const
  {
    return &extinct;
  }

  //---------------------------------------------------------------
  // IO
  //---------------------------------------------------------------
  std::string
  HybridTree::print(bool useET, bool useNT, bool useBL, bool useER) const
  {
    ostringstream oss;
    string namestr = getName();
    if (namestr.length() > 0) 
      {
	oss << "HybridTree " << getName() << ":\n";
      }
    else 
      {
	oss << "HybridTree:\n";
      }
    oss << subtree4os(getRootNode(), "", "", useET, useNT, useBL);
    return oss.str();
  }

  std::string
  HybridTree::print() const
  {
    return print(true, true, true, false);
  }

  std::string 
  HybridTree::printBinary2Hybrid() const
  {
    std::ostringstream oss;
    for(unsigned i = 0; i < bTree.getNumberOfNodes(); i++)
      {
	oss << i 
	    << "\t" 
	    << binary2Hybrid[bTree.getNode(i)]->getNumber()
	    << "\n";
      }
    return oss.str();
  }

  std::string 
  HybridTree::printHybrid2Binary() const
  {
    std::ostringstream oss;
    for(unsigned i = 0; i < getNumberOfNodes(); i++)
      {
	map<const Node*, std::vector<Node*> >::const_iterator ci = 
	  hybrid2Binary.find(getNode(i));
	if(ci != hybrid2Binary.end())
	  {
	    oss << i << "\t" ;
	    std::vector<Node*> v =  ci->second;
	    for(unsigned j = 0; j < v.size(); j++)
	      oss << v[j]->getNumber() << "\t";
	    oss << "\n";
	  }
      }
    return oss.str();
  }

  // These functions are not defined for class HybridTree
  unsigned 
  HybridTree::getHeight() const
  {
    std::cerr << "getHeight not implemented for HybridTree\n";
    abort(); // was exit(1);
    return 0;
  }
  
  Real 
  HybridTree::imbalance() const
  {
    std::cerr << "Imbalance not implemented for HybridTree\n";
    abort(); // was exit(1);
    return 0;
  }

  Node* 
  HybridTree::mostRecentCommonAncestor(Node* a, Node* b) const
  {
    throw AnError("mostRecentCommonAncestor not implemented for HybridTree",1);
    return 0;
  }

  //---------------------------------------------------------------
  //
  // Implementation
  //
  //---------------------------------------------------------------
  void
  HybridTree::clearTree()
  {
    if(rootNode != 0)
      {
	deleteHybridSubtree(getRootNode());    
	delete rootNode;
	rootNode = 0;
      }
    assert(rootNode == 0);
    otherParent.clear();
    extinct.clear();
    noOfNodes = noOfLeaves = 0;
    name2node.clear();
    all_nodes = std::vector<Node*>(DEF_NODE_VEC_SIZE, NULL);
    bTree.clear();
    hybrid2Binary.clear();
    binary2Hybrid.clear();
    Tree::clearTree();
  }

  Node*
  HybridTree::copyAllNodes(const Node* v)
  {
    assert(v != 0);
    Node* u = getNode(v->getNumber());
    if(u)
      {
	setOtherParent(*u, u->getParent());
	return u;
      }
    
    return Tree::copyAllNodes(v);
  }

  string
  HybridTree::subtree4os(Node *v, string indent_left, string indent_right,
		   bool useET, bool useNT, bool useBL) const
  {
    std::ostringstream oss;
    if(v) 
      {
	oss << subtree4os(v->getRightChild(), indent_right +"    |", 
			  indent_right + "     ", useET, useNT, useBL)
	    << indent_right.substr(0,indent_right.size()-1) + "+";
// 	if (!v->getName().empty()) 
	if (v->getName() != "") 
	  {
	    oss << "--- "
		<< v->getNumber()
		<< ", "
		<< v->getName();
	  }
	else 
	  {
	    oss << "--- "
		<< v->getNumber();
	  }
	if(v->isLeaf())
	  {
	    if(hybrid2Binary.find(v) != hybrid2Binary.end())
	      {
		const vector<Node*> bNames = hybrid2Binary[v];
		  if(bNames.size() > 1)
		    {
		      oss << " ( ";
		      for(unsigned i = 1; i < bNames.size(); i++)
			{
			  oss << bNames[i]->getName() << " " ;
			}
		      oss << ")";
		    }
	      }
	  }
	if(useET)
	  {
	    oss << ",   ET: "
		<< v->getTime();
	  } 
	if(useNT)
	  {
	    oss << ",   NT: "
		<< v->getNodeTime();
	  }
	if(useBL)
	  {
	    oss << ",   BL :"
		<< v->getLength();
	  }
	if(isHybridNode(*v))
	  {
	    oss << ",   HYBRID: ";
	    if(getOtherParent(*v))
	      oss << getOtherParent(*v)->getNumber()
		  << " X "
		  << v->getParent()->getNumber();
	    else
	      throw AnError("HybridTree::subtree4os:\n"
			    "Hybrid node lacks other parent",1);
	  
	  }
	if(isExtinct(*v))
	  {
	    oss << ",   EXTINCT!";
	  }
#ifdef DEBUG_HYBRIDTREE 
	if(v->getParent())
	  {
	    oss << ", parent : " << v->getParent()->getNumber();
	  }
	else
	  {
	    oss << ", parent : NULL";
	  }
#endif
	oss << "\n"
	    << subtree4os(v->getLeftChild(), indent_left + "     ", 
			  indent_left + "    |", useET, useNT, useBL);
      }
    return oss.str();
  }

  Node* 
  HybridTree::buildFromBinaryTree(const Node* u)
  {
    assert(u != 0);
    Node* left = 0;
    Node* right = 0;
    if(u->isLeaf() == false)
      {
	left = buildFromBinaryTree(u->getLeftChild());
	right = buildFromBinaryTree(u->getRightChild());
      }
    return addNode(left, right, u->getNumber(), u->getName(), false);
  }

  void
  HybridTree::updateBinaryTree() const
  {
    if(perturbedTree())
      {
	bTree.clear();
	hybrid2Binary.clear();
	binary2Hybrid.clear();
	
	if(rootNode)
	  {
	    bTree.setRootNode(copyAllHybridNodes(rootNode));
	    bTree.perturbedTree(true);
	    if(times)
	      {
		RealVector& bTimes = *new RealVector(bTree);
		for(unsigned i = 0; i < bTree.getNumberOfNodes(); i++)
		  {
		    Node* h = binary2Hybrid[bTree.getNode(i)];
		    bTimes[i] = (*times)[h];
		  }
		bTree.setTimes(bTimes, true);
		assert(rootToLeafTime() == bTree.rootToLeafTime());
		bTree.setTopTime(topTime);
	      }
	    bTree.setName(name + "b");
	    //! \todo{Do similar for lengths and rates -- maybe?}
	  }
      }
  }

  Node*
  HybridTree::copyAllHybridNodes(Node* v) const
  {
    assert(v != NULL);
    if(isExtinct(*v))
      {
	assert(v->isLeaf());
	return NULL;
      }

    if(hybrid2Binary.find(v) != hybrid2Binary.end())
      {
	assert(isHybridNode(*v));
	
	Node* v1 = hybrid2Binary[v].front();
	assert(v1 != 0);
	Node* v2 = bTree.copySubtree(v1);
	renameLeaves(*v1, *v2);
	return v2;
      }

    Node* l = NULL;
    Node* r = NULL;
	
    if(v->isLeaf() == false)
      {
 	l = copyAllHybridNodes(v->getLeftChild());
	r = copyAllHybridNodes(v->getRightChild());
	
	if(l == NULL)
	  {
	    assert(r != NULL);
	    return r;
	  }
	else if(r == NULL)
	  {
	    assert(l != NULL);
	    return l;
	  }
      }
    Node *u = bTree.addNode(l, r, v->getName());//new Node(*v);
    binary2Hybrid[u] = v;
    hybrid2Binary[v].push_back(u);

    return u;
  }

  // Delete all nodes lower in the tree. The current node is not deleted.
  //----------------------------------------------------------------------
  void
  HybridTree::deleteHybridSubtree(Node* n)
  {
    Node* leftChild = n->getLeftChild();

    if(leftChild)
      {
	deleteHybridSubtree(leftChild);
	Node* op = getOtherParent(*leftChild);
	if(op == n)
	  {
	    switchParents(*leftChild);
	    op = getOtherParent(*leftChild);
	    assert(op != n);
	  }
	deleteHybridNode(leftChild, op);
	
	Node* r = n->getRightChild();
	if(r && getOtherParent(*r) == n)
	  {
	    switchParents(*r);
	  }
	n->setChildren(NULL, r); 
	leftChild = NULL;
      }
    assert(n->getLeftChild() == NULL);

    Node* rightChild = n->getRightChild(); 
    if(rightChild)
      {
	deleteHybridSubtree(rightChild);
	Node* op = getOtherParent(*rightChild);
	if(op == n)
	  {
	    switchParents(*rightChild);
	    op = getOtherParent(*rightChild);
	    assert(op != n);
	  }
	deleteHybridNode(rightChild, op);
	n->setChildren(NULL, NULL);
	rightChild = NULL;
      }
    assert(n->getRightChild() == NULL);
  }
  
  void 
  HybridTree::deleteHybridNode(Node* n, Node* op)  
  {
    if(op)
      {
	if(op->getLeftChild() == n)
	  {
	    op->setChildren(NULL, op->getRightChild());
	    assert(op->getLeftChild() == NULL);
	  }
	else
	  {
	    assert(op->getRightChild() == n);
	    op->setChildren(op->getLeftChild(), NULL);
	    assert(op->getRightChild() == NULL);
	  }
	otherParent.erase(n);
	assert(getOtherParent(*n) == NULL);
      }
    
    removeNode(n);//    all_nodes[n->getNumber()] = NULL;
    n = NULL;
//     assert(noOfNodes > 0);
//     noOfNodes--;
//     delete n;
  }
    
  void
  HybridTree::renameLeaves(const Node& v, Node& v2) const
  {
    if(v.isLeaf() == false)
      {
	renameLeaves(*v.getLeftChild(), *v2.getLeftChild());
	renameLeaves(*v.getRightChild(), *v2.getRightChild());
      }
    assert(binary2Hybrid.find(&v) != binary2Hybrid.end());
    Node* h = binary2Hybrid[&v];

    hybrid2Binary[h].push_back(&v2);
    binary2Hybrid[&v2] = h;
  }

}//end namespace beep
