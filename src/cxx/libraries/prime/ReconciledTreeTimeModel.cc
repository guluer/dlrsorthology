#include "ReconciledTreeTimeModel.hh"

namespace beep
{
  using namespace std;
  //------------------------------------------------------------
  //
  // Construct/destruct/assign
  //
  //------------------------------------------------------------
  ReconciledTreeTimeModel::ReconciledTreeTimeModel(Tree& G, StrStrMap& gs, 
						   BirthDeathProbs& bdp_in)
    : ReconciliationModel(G, gs, bdp_in)
  {}
  
  ReconciledTreeTimeModel::~ReconciledTreeTimeModel()
  {}
  
  ReconciledTreeTimeModel::
  ReconciledTreeTimeModel(const ReconciledTreeTimeModel& rtt)
    : ReconciliationModel(rtt)
  {}
  
  ReconciledTreeTimeModel& 
  ReconciledTreeTimeModel::operator=(const ReconciledTreeTimeModel& rtt)
  {
    if(&rtt != this)
      {
	ReconciliationModel::operator=(rtt);
      }
    return *this;
  }

  //------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------
  void 
  ReconciledTreeTimeModel::update()
  {
#ifdef PERTURBED_TREE
    if(S->perturbedTree())
      {
	sigma.update(*G, *S, gs);
	gamma_star = GammaMap::MostParsimonious(*G, *S, sigma); 
	gamma = gamma_star;
      }
    else if(G->perturbedTree())
      {
	sigma.update(*G, *S);
	gamma_star = GammaMap::MostParsimonious(*G, *S, sigma); 
	gamma = gamma_star;
      }
#else
    sigma.update(*G, *S);
    gamma_star = GammaMap::MostParsimonious(*G, *S, sigma); 
    gamma = gamma_star;
#endif
  }
  
  Probability 
  ReconciledTreeTimeModel::calculateDataProbability()
  {
    Node& rootS = *S->getRootNode();
    Node& rootG = *G->getRootNode();
    return computeRA(rootS, rootG);
  }
  
  //------------------------------------------------------------
  // I/O
  //------------------------------------------------------------
  std::string 
  ReconciledTreeTimeModel::print() const
  {
    return "ReconciliationTreeTimeModel  " + ReconciliationModel::print();
  }
  
  //------------------------------------------------------------
  //
  // Implementation
  //
  //------------------------------------------------------------
  // Debugging support macros
  //-------------------------------------------------------------------
#ifdef DEBUG_DP
#define DEBUG_R(S,X,U,P) {std::cerr << S << "(" << X.getNumber() << ", " << U.getNumber() << ") = " << P.val() << std::endl;}
#else
#define DEBUG_R(S,X,U,K)
#endif
  
  
  
  // computes \f$ r_V(x,u) \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  Probability 
  ReconciledTreeTimeModel::computeRV(Node& x, Node& u)
  {
    assert(x.dominates(*sigma[u])); //check precondition
    Probability p;
    if(sigma[u] == &x)
      {
	assert(gamma_star.isInGamma(&u, &x));
	if(x.isLeaf())
	  {
	    assert(u.isLeaf());
	    return 1.0;
	  }
	else
	  {
	    Node& v = *u.getLeftChild();
	    Node& w = *u.getRightChild();
	    Node& y = *x.getDominatingChild(sigma[v]);
	    Node& z = *x.getDominatingChild(sigma[w]);
	    
	    assert(y.getSibling() == &z); // Check sanity of recursion
	    p = computeRA(y,v) * computeRA(z,w);
	  }
      }
    else
      {
	Node& y = *x.getDominatingChild(sigma[u]);
	Node& z = *y.getSibling();
	
	p = computeRA(y,u) * computeRA(z,u);
      }

     DEBUG_R("ComputeRV", x, u, p);
     return p;
  }
  
  // computes \f$ r_A(x,u) \f$
  //-----------------------------------------------------------------
  Probability 
  ReconciledTreeTimeModel::computeRA(Node& x, Node& u)
  {
    Probability p;
    // if u \in S_x, then s_A(x,u) = \sum_k\in[|L(G:U)|}Q_x(k)s_X(x,u,k)
    if(x.dominates(*sigma[u]))
      {
	p = computeRX(x,u);
	if(x.isRoot())
	  {
	    p *= bdp->topPartialProbOfCopies(1u);	  
	  }
	else
	  {
	    p *= bdp->partialProbOfCopies(x,1u);	  
	  }
      }
    // if u \not\in S_x, then s_A(x,u) = X_A(x)
    else
      {
	p = bdp->partialProbOfCopies(x,0);	  
      }
    DEBUG_R("ComputeRA", x, u, p);
    return p;
  }
  
  // computes \f$ r_X(x,u,k), k\in[|L(G_u)|] \f$
  // precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
  //-----------------------------------------------------------------
  Probability 
  ReconciledTreeTimeModel::computeRX(Node& x, Node& u)
  {
    if(x.dominates(*sigma[u]) == false)
      cerr << "u = " << u.getNumber() 
	   << "   sigma[u] = "
	   <<sigma[u]->getNumber() 
	   << "    x = " << x.getNumber() 
	   << endl ;
    assert(x.dominates(*sigma[u])); // check precondition
    
    Probability p;
    if(G->getTime(u) <= S->getTime(x))
      {
	p = computeRV(x,u);
      }
    else
      {
	Node& v = *u.getLeftChild();
	Node& w = *u.getRightChild();
	
	p =  computeRX(x,v) * computeRX(x,w);
	
	Real tu = G->getTime(u) - S->getTime(x);
	p *= bdp->bornLineageProbability(x,tu);
	
	// compute the factor 
	// $2^{\delta(G_{u,\gamma(x)})+\delta{\gamma,x}(u)}
	// r_X(x,u) = factor * ~P(tu)(1-~u_tu) * r_X(x,left) * r_X(x,right)
	p *= 2;
      }
    DEBUG_R("ComputeRX", x, u, p);
    return p;
  }
  
} // end namespace beep
