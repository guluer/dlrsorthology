#ifndef RECONCILIATIONTIMEMCMC_HH
#define RECONCILIATIONTIMEMCMC_HH

#include <iostream>
#include <string>

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "MCMCModel.hh"
#include "Node.hh"
#include "ReconciliationModel.hh"
#include "ReconciliationTimeModel.hh"
#include "ReconciliationTimeSampler.hh"
#include "StdMCMCModel.hh"

namespace beep
{
  
  //-------------------------------------------------------------
  //
  //! MCMC-interface to ReconciliationTimeModel
  //
  //-------------------------------------------------------------
  class ReconciliationTimeMCMC : public StdMCMCModel, 
				 public ReconciliationTimeModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------

    // number of parameters equal number of internal nodes
    //-------------------------------------------------------------
    ReconciliationTimeMCMC(MCMCModel& prior, Tree &G, BirthDeathProbs& nee, 
			   GammaMap& gamma, Real suggestRatio = 1.0);

    ReconciliationTimeMCMC(MCMCModel& prior, Tree &G, BirthDeathProbs& nee, 
			   GammaMap& gamma, bool include_root_time, 
			   Real suggestRatio = 1.0);

    // Construct using an existing ReconciliationModel (subclass)
    ReconciliationTimeMCMC(MCMCModel& prior, ReconciliationModel& rs, 
			   Real suggestRatio=1.0);

    // Construct using an existing ReconciliationModel (subclass)
    ReconciliationTimeMCMC(MCMCModel& prior, ReconciliationModel& rs, 
			   bool include_root_time, Real suggestRatio=1.0);

    //With name
    ReconciliationTimeMCMC(MCMCModel& prior, Tree &G, BirthDeathProbs& nee, 
			   GammaMap& gamma, const std::string& name,
			   Real suggestRatio = 1.0);

    // Construct using an existing ReconciliationModel (subclass)
    ReconciliationTimeMCMC(MCMCModel& prior, ReconciliationModel& rs, 
			   const std::string& name, Real suggestRatio);
    ReconciliationTimeMCMC(const ReconciliationTimeMCMC& rts);
    ~ReconciliationTimeMCMC();
    ReconciliationTimeMCMC& operator=(const ReconciliationTimeMCMC& rts);

    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    const GammaMap& getGamma();
    void fixTimes();
    void multiplySuggestionVariance(Real multiplier)
    {
      suggestion_variance = multiplier * G->rootToLeafTime() / 
	G->getRootNode()->getMaxPathToLeaf();
    }

    MCMCObject suggestOwnState();
    MCMCObject suggestOwnState(unsigned x);
    void commitOwnState();
    void commitOwnState(unsigned x);
    void discardOwnState();
    void discardOwnState(unsigned x);
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const ReconciliationTimeMCMC& A);
    std::string print() const;

    //-------------------------------------------------------------
    //
    // Implementation
    //
    //-------------------------------------------------------------

    //The function perturbs the node time at 'currentNode' and returns the
    //proposal ratio.
    //-------------------------------------------------------------
    MCMCObject perturbTime(Node& gn);

  protected:
    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    unsigned Idx;
    bool estimateTimes;
    Real oldValue;
    Probability like;
    Probability oldLike;
    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;

  };

}//end namespace beep
#endif
