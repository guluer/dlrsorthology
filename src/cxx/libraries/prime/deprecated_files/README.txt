The directory
src/cxx/libraries/prime/deprecated_files/manually_picked
contains files that were manually sorted out as being deprecated (Mostly done by Lars Arvestad).

The directory
src/cxx/libraries/prime/deprecated_files/filtered
contain files that were detected from the grep command as not being used. For details see the commands below.


esjolund@ubuntu:src/cxx/libraries/prime$ for i in `ls *cc`; do if ! grep -qr $i . ; then echo $i; fi ; done 
EdgeDiscGSR_PeterMattias.cc
EdgeDiscPtKeyIterator.cc
MultiFamReconMCMC.cc
MultiFamReconModel.cc
PvmClient.cc
PvmCommunication.cc
PvmMaster.cc
SplicedDiscTree.cc
TreeModel_common.cc
UserLambdaMap.cc
aladen.cc
analyze_L_ratio.cc
collectTrees.cc
distr_likelihood.cc
full_jarden.cc
gtl.cc
ortho_parse.cc
setBLonTree.cc
slow_jarden.cc
spez_generateTree.cc
treestats.cc
understand_multiple_inheritance.cc
understand_templates.cc

esjolund@ubuntu:src/cxx/libraries/prime$ for i in `ls *cc`; do a=${i/cc/hh} ; if [ -e $a ] ; then if  ! grep -qr $a /tmp/e/prime-phylo-1.0.2/src  ; then echo $a; fi ; fi; done 
EdgeDiscGSR_PeterMattias.hh




I am wondering about EdgeDiscPtKeyIterator.cc and EdgeDiscPtKeyIterator.hh. The first file is not used but the second is used...
Probably they should be moved to deprecated_files.... /Erik Sjolund 2012-04-04
