
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cstdlib>
#include <exception>

#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PRNG.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SimpleMCMC.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"


// Global options with default settings
//------------------------------------
int nParams = 1;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 100;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

bool fixed_times = false;
bool fixed_rates = false;
double birthRate = 1.0;
double deathRate = 1.0;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      // tell the user we've started
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      int opt = readOptions(argc, argv);
      if(opt + 1 > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	  exit(1);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string treefile(argv[opt++]);
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G;
      G = io.readGuestTree();  
      G.setName("G");
      cerr << "intree:\n"
	   << G;

      //Set up "end MCMCModel"
      //---------------------------------------------------------

      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

      // Set up priors
      //---------------------------------------------------------
      Tree S = Tree::EmptyTree();
      S.setName("S");
      //set up a gamma
      StrStrMap gs;
      // Map all leaves of G to the single leaf in S
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  gs.insert(G.getNode(i)->getName(), S.getRootNode()->getName());
	}
      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda); 
      BirthDeathMCMC bdm(dm, S, birthRate, deathRate, !fixed_rates); 
      ReconciliationTimeMCMC rtm(bdm, G, bdm, gamma, "EdgeTimes");

      if(true) //Create a temporary object to initiate times of G 
	{
	  BirthDeathMCMC bdm2(dm, S, 1.0, 1.0, !fixed_rates);
	  ReconciliationTimeSampler sampler(G, bdm, gamma);
	  sampler.sampleTimes(false);
	}

      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(rtm, Thinning);

      if (do_likelihood)
	{
	  cout << rtm.currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  


      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cout << "#Running: ";
	  for(int i = 0; i < argc; i++)
	    {
	      cout << argv[i] << " ";
	    }
	  cout << " in directory"
	       << getenv("PWD")
	       << "\n#\n";
	  cout << "#Start MCMC (Seed = " << RSeed << ")\n";
	}


      // Perform and time the Likelihood calculation
      time_t t0 = time(0);
      clock_t ct0 = clock();
      
      iterator.iterate(MaxIter, printFactor);
      // //       Probability p = rsa.suggestNewState().stateProb;
      // //       //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

      if (!quiet)
	{
	  cerr << rtm.getAcceptanceRatio()
	       << " = acceptance ratio\n";
	}
      
      if (rtm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
  catch(exception e)
    {
      cout <<" error\n";
      cerr << e.what();
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         a string\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -c <float>            Output to cerr <float> times less often\n"
    << "                         than to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth and death rates of the birth-\n"
    << "                         death model \n"
    << "     -Bp <float>         start value of the birth rate parameter\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'c':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_rates = true;
		    // Don't break here, because we want to fall through to 'r' to set the
		    // rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  
