#ifndef DISCGEM_HH
#define DISCGEM_HH

#include <list>
#include <time.h>
#include <vector>

#include "Beep.hh"
#include "BeepVector.hh"
#include "EdgeWeightModel.hh"
#include "GenericMatrix.hh"
#include "LambdaMap.hh"
#include "PerturbationObservable.hh"

namespace beep
{

  // Forward declarations.
  class Density2P;
  class DiscBirthDeathProbs;
  class DiscTree;
  class Node;
  class Probability;
  class StrStrMap;
  class TreePerturbationEvent;
  
  /**
   * Model based on the Gene Evolution Model (GEM) which uses a discretization
   * of the host tree to numerically integrate over all possible reconciliations
   * of the current guest tree topology (as constrained by the discretization).
   * Rates are handled as length divided by time, the first of which corresponds 
   * to edge lengths in the guest tree, the second to edge times in the host tree.
   * For a node u of the guest tree which can be placed on a node X of the host tree, 
   * X acts similarly to a discretization point in case sigma(u) != X.
   * At the moment, perturbation of the host tree is not permitted.
   * 
   * Moreover, as an EdgeWeightModel, this class holds the edge lengths of the guest 
   * tree, and is thus the model for an MCMC perturbing the lengths.
   * 
   * This class listens for perturbation updates on the birth-death parameters, 
   * the guest tree, and the rate density function, so make sure that any change 
   * results in a notification.
   */
  class DiscGEM : public EdgeWeightModel, public PerturbationObserver
  {
    
    typedef std::vector<Probability> ProbVector;
    
  public:
    
    /**
     * Constructor.  * @param G the guest tree.  * @param DS the
     * discretized version of host tree S.  * @param GSMap the G-to-S
     * leaf mapping.  * @param rateDensFunc the rate density function.
     * * @param BDProbs the birth and death probs. for the
     * duplication-loss process.  * @param sharedRootChildEdges flag
     * indicating whether the edges leading to the * root of the guest
     * tree should be considered shared (as in unrooted tree).  * May
     * have a major performance impact.  * @param fixedGNodes a map
     * stating known speciations u of G that should be * kept fixed at
     * sigma(u). Used as if containing bools. May only be set * when not
     * perturbing the guest tree.
     */
    DiscGEM(Tree& G,
	    DiscTree& DS,
	    StrStrMap& GSMap,
	    Density2P& rateDensFunc,
	    DiscBirthDeathProbs& BDProbs,
	    bool sharedRootChildEdges = false,
	    BeepVector<unsigned>* fixedGNodes = NULL);
    
    /**
     * Destructor.
     */
    virtual ~DiscGEM();
    
    /**
     * Returns the guest tree.
     * @return the guest tree.
     */
    virtual const Tree& getTree() const;
    
    /**
     * Returns the number of weights, i.e. (roughly) the number of
     * edges in the guest tree.
     * @return the number of perturbable lengths.
     */
    virtual unsigned nWeights() const;
    
    /**
     * Returns the edge lengths of the guest tree.
     * @return the lengths of the guest tree.
     */
    virtual RealVector& getWeightVector() const;
    
    /**
     * Returns the length of an edge in the guest tree.
     * @param node the lower node of the edge.
     * @return the length of the edge. 
     */
    virtual Real getWeight(const Node& node) const;
    
    /**
     * Sets the length of an edge in the guest tree.
     * @param weight the length to be set.
     * @param u the lower node of the edge.
     */
    virtual void setWeight(const Real& weight, const Node& u);
    
    /**
     * Returns the valid range of the underlying rate density function.
     * Note: is this sound?  @param low the lower value of the range.  *
     * @param high the upper value of the range.
     */
    virtual void getRange(Real& low, Real& high);
    
    /**
     * Returns info on how root edge weights are perturbed.
     * @return perturbation technique.
     */
    virtual RootWeightPerturbation getRootWeightPerturbation() const
    {
      return EdgeWeightModel::BOTH;
    }
    
    /**
     * Has no effect.
     * @param rwp
     */
    virtual void setRootWeightPerturbation(RootWeightPerturbation rwp)
    {}
    
    /**
     * Returns a string with information about the rate
     * probabilities of the guest tree.
     * @return an info string.
     */
    virtual std::string print() const;
    
    /**
     * Callback method for notifying this object about changes to the
     * underlying parameter holders (birth-death rates, guest tree, and
     * rate density function). Updates or restores the internal
     * probabilities depending on event. For changes to the guest tree,
     * there is support for making an optimized partial update in case
     * detailed information (a TreePerturbationEvent) is specified.
     * @param sender the address of the parameter holder which has been
     *        perturbed.
     * @param event the kind of perturbation. Null value results in full
     *        update.
     */
    virtual void perturbationUpdate(const PerturbationObservable* sender,
				    const PerturbationEvent* event);
    
    /**
     * Currently does nothing. All updates are made in (callbacks to)
     * perturbationUpdate().
     */
    virtual void update();
    
    /**
     * Computes the data probability with respect to possible
     * reconciliations given that the lineage starts at the top.
     * Requires that help and probability data structures are up-to-date.
     * @return the data probability.
     */
    virtual Probability calculateDataProbability();
    
    /**
     * Turns on/off writing of speciation errors to stderr. Such errors
     * are reported when a speciation cannot take place due to too few
     * discretization steps below in host tree. Off by default.
     * @param showSpecError if true, turns on error reporting.
     */
    void setSpecErrorVisible(bool showSpecError);
    
    /**
     * Prints debugging information to stderr.
     */
    void debugInfo(bool printNodeInfo=true) const;
    
  private:
    
    /**
     * Helper. Updates help data structures. Invoke before computing 
     * (probabilities partial as well as full).
     */
    void updateHelpStructures();
    
    /**
     * Recursive helper. Finds and stores the lowest possible point in DS
     * on which a node u of G can be placed. Recursively processes all nodes
     * in the subtree below (and including) the specified node.
     * @param u the root of the subtree of G.
     */
    void updateLoGridIndices(const Node* u);
    
    /**
     * Recursive helper. Finds and stores the highest possible point in DS
     * on which a node u of G can be placed. Recursively processes all nodes
     * in the subtree below (and including) the specified node. Must be
     * invoked after updateLoGridIndices(u).
     * @param u the root of the subtree of G.
     */
    void updateUpGridIndices(const Node* u);
    
    /**
     * Makes a clean update of probability data structures. Requires
     * that help data structures are up-to-date.
     */
    void updateProbsFull();
    
    /**
     * Helper. Makes a partial update of probability data structures based on
     * information on guest tree changes. Requires that help data
     * structures are up-to-date.
     * @param details info about guest tree changes. Must not be null.
     */
    void updateProbsPartial(const TreePerturbationEvent* details);
  
    /**
     * Helper. Updates all probabilities for the specified node u of G.
     * Requires that help structures are up-to-date, and children probabilities
     * in case of a non-recursive call.
     * @param u the node of G.
     * @param doRecurse true to process children recursively first.
     * @param cleanUpdate true to recalculate all probabilities, false to
     *        only recalculate needed parts. In the latter case, existing
     *        values must -- naturally -- be known to be valid.
     */
    void updateNodeProbs(const Node* u, bool doRecurse, bool cleanUpdate);
    
    /**
     * Helper. Appends additional node probabilites for a node u
     * of G for a specified interval of points. Used to add missing points
     * in dirty updates.
     * @param u the node of G for which to append probabilities.
     * @param loIndex the lowest point in the host tree for which to append 
     * probability.
     * @param upIndex the highest point in the host tree for which to append
     *        probability.
     */
    void appendNodeProbs(const Node* u, DiscTree::Point loIndex, unsigned upIndex);
    
    /**
     * Helper. Calculates a vector of lineage-event probabilities for a lineage
     * starting at a specified point and the event of v of G being placed at '
     * points below. Lineages are stored in decreasing lengths, with index 0 
     * corresponding to v being placed at its lowermost valid point, and so 
     * forth.
     * @param lins the (initially empty) vector of lineage-event probabilities.
     * @param v the node of G for which probabilities are computed.
     * @param linStart the fixed point where all lineages start.
     * @param singleLin if true, calculates only a single lineage. Typically used
     *        when v can only take place at a fixed point below.
     */
    const Node* calcLinProbs(ProbVector& lins, const Node* v,
			     DiscTree::Point linStart, bool singleLin) const;
    
    /**
     * Helper. Computes the overall probability of two vectors of lineage-event
     * probabilities for two sibling nodes v and w of G. Includes rates along the way.
     * @param vLins the first vector of lineage-event probabilities (excl. rates).
     * @param wLins the second vector of lineage-event probabilities (excl. rates).
     * @param vLength the length of all lineages in the first vector. Used to calculate rates.
     * @param wLength analagous with above.
     * @param vTimesteps the number of timesteps in the discretized host tree which the
     *        first (longest) lineage in the first vector spans. Each ensuing lineage is
     *        assumed to be one timestep shorter.
     * @param wTimesteps analagous with above.
     * @param sharedEdge if true, considers the edge of the two guest tree nodes to be shared
     *        (typically because they are in the unrooted tree). The default would be
     *        false. True has a negative performance impact.
     */
    Probability calcRateSumProb(ProbVector& vLins, ProbVector& wLins, 
				Real vLength, Real wLength,
				unsigned vTimesteps, unsigned wTimesteps, 
				bool sharedEdge) const;
    
    /**
     * Helper Extends lineages by multiplying each element with the specified
     * probability.
     * @param the lineage-event probabilities.
     * @param linSeg the segment which to "extend" each element with. 
     */
    static void extendLins(ProbVector& lins, Probability linSeg);
    
    /**
     * Helper. Stores current probabilities for the entire guest tree, or just parts of
     * it based on specified information. To cache entire tree, pass along null.
     * @param details information about guest tree changes. Null causes caching
   * of entire tree.
   */
    void cacheProbs(const TreePerturbationEvent* details);
    
    /**
     * Helper. Stores the current probabilities for a node u of G.
     * @param u the node of the guest tree.
     * @param doRecurse true to cache all of subtree rooted at u.
     */
    void cacheNodeProbs(const Node* u, bool doRecurse);
    
    /**
     * Helper. Restores cached probabilities. These may concern parts of
     * or all of the guest tree G.
     */
    void restoreCachedProbs();
    
    /**
     * Helper. Simple access function which returns the stored probability
     * of a certain node corrsponding to a specified grid index in the host
     * tree.
     * @param u the node of G for which to retrieve probability.
     * @param gridIndex the grid index of the point in DS for which to
     *        retrieve the probability.
     * @return the probability.
     */
    Probability& getProb(const Node* u, unsigned gridIndex) const;
    
  public:
    
  private:
  
    /** The guest tree G. */
    Tree& m_G;
    
    /** The discretized host tree S, coined DS. */
    DiscTree& m_DS;
    
    /** Guest-to-host tree leaf map. */
    /*const*/ StrStrMap& m_GSMap;
  
    /** Rate density function. */
    Density2P& m_rateDensFunc;
    
    /** Birth and death probabilities for the discretized host tree. */
    DiscBirthDeathProbs& m_BDProbs;
    
    /** Guest-to-host tree sigma map. */
    LambdaMap m_sigma;
    
    /** The lengths of G. Stored here, but normally a handler sets G 
	to point to them as well. */
    //! These must be a pointer so we can use anyl engths already in the tree
    mutable RealVector* m_lengths;
    
    /** Flag indicating that rates for root's child edges in G are 
      treated as one. */
    bool m_sharedRootChildEdges;
    
    /**
     * For each node u of G, contains a map stating if the node should be
     * fixed or not at sigma(u). If a node u has a child v so that 
     * sigma(u)=sigma(v),
     * this instruction is ignored. Vector is used as if consisting of bools.
     * G topology must not be perturbed if this has been set. Normally null.
     */
    BeepVector<unsigned>* m_fixedGNodes;
    
    /** Shows error messages related to too few disc. steps for spec. */
    bool m_showSpecError;
    
    /**
     * For each node u of G, contains the grid index of the lowermost placement
     * in DS where u can be placed, and also the lower node of the edge in question.
     */
    BeepVector<DiscTree::Point> m_loGridIndices;
    
    /**
     * For each node u of G, contains the grid index of the uppermost placement
     * in DS where u can be placed.
     */
    BeepVector<unsigned> m_upGridIndices;
  
    /**
     * For each node u of G, contains a vector with elements corresponding 
     * to each valid placement of u in the discretized host tree DS. Each 
     * element stores what can be seen as the (discretized) probability of 
     * u taking place at that position, with element 0 being the position 
     * closest to the leaves and so forth. Access to this array using
     * grid indices instead of relative indices is greatly simplified 
     * with 'getProb()'.
     */
    BeepVector<ProbVector*> m_probs;
    
    /**
     * Cached probabilities. Sometimes contains only parts of tree, 
     * where non-cached
     * vectors must be null.
     */
    BeepVector<ProbVector*> m_probs_old;
    
  };
  
}  // end namespace beep

#endif /* DISCGEM_HH */

