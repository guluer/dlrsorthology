#ifndef ORTHOLOGYMATRIX_HH
#define ORTHOLOGYMATRIX_HH

// Since the orthology code needs to be access in both
// ReconSeqApproximator and ClientReconSeqApprox, I am now moving this
// logic into its own class. OrthologyMatrix is a matrix of
// Probability that records the probability of two genes being
// orthologous. Only the subdiagonal part is used.
//

#include "GammaMap.hh"
#include "GenericMatrix.hh"
#include "Probability.hh"

namespace beep {
  // forward declarations
  class OrthologyMatrix;
  OrthologyMatrix operator+(const OrthologyMatrix& om1, const OrthologyMatrix& om2);

  class OrthologyMatrix : public GenericMatrix<Probability>
  {
  public:
    OrthologyMatrix(const Tree& G);
    OrthologyMatrix(const OrthologyMatrix& om);	// Copy constructor
    ~OrthologyMatrix();

    // Assignment and related
    OrthologyMatrix& operator=(const OrthologyMatrix& om);
    OrthologyMatrix& operator+=(const OrthologyMatrix& om);

    friend OrthologyMatrix operator+(const OrthologyMatrix& om1, const OrthologyMatrix& om2);

    Probability  operator()(unsigned i, unsigned j) const; // Access one element
    Probability& operator()(unsigned i, unsigned j);
    void testing_set_elem();

    void reset();		// Put zeros in the orthology matrix

    void recordOrthology(const GammaMap &gamma, const Probability &p);

    // For printing
    std::string strRepresentation() const;

  private:
    Tree G;
  };
}


#endif
