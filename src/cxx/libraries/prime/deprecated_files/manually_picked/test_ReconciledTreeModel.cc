#include "AnError.hh"
#include "BranchSwapping.hh"
#include "GuestTreeModel.hh"
#include "ReconciledTreeModel.hh"
#include "ReconciliationSampler.hh"
#include "TreeIO.hh"

void usage(char* cmsd);
int readOptions(int argc, char **argv);
int nParams = 2;

double topTime           = -1.0;
double birthRate         = 1.0;
double deathRate         = 1.0;

// Main program
//-------------------------------------------------------------
int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc <= nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = readOptions(argc, argv);
      if(opt + nParams > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      vector<SetOfNodes> AC;
      StrStrMap gs;
      Tree G = io.readGuestTree(&AC, &gs);
      G.setName("G");
      cout << "\nThe guest tree is\n"
	   << G;

      string host(argv[opt++]);
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");
      if(topTime >= 0)
	{
	  S.setTopTime(topTime);
	}
      cerr << "\n The host tree is\n " << S;

      cout << "\nThe guest-leaf-to-host-leaf map (gs) is\n" <<gs;

      cout << "\nThe reconciliation (gamma) is\n";
      LambdaMap lambda(G,S,gs);
      GammaMap gamma(G,S,lambda, AC);
      cout << gamma;
      cout << "------------------------------------------------\n\n";

      BirthDeathProbs bdp(S, birthRate, deathRate);

      ReconciledTreeModel a(G, gs, bdp);
      GuestTreeModel b(G,gs, bdp);
      a.setGamma(gamma);
      Probability P = a.calculateDataProbability() / b.calculateDataProbability();
      cout << "\nThe probability of the reconciled tree is:\n"
	   << "P[G, gamma] = " << P.val() << " (= exp(" << P << "))\n\n";
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
};




void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << "[<optinons>] <reconciled guesttree> <hosttree>"
    << "\n"
    << "Outputs probability of input reconciliation under GEM model\n."
    << "\n"
    << "Parameters:\n"
    << "   <reconciled guesttree>  (string) name of file with guest tree in\n"
    << "                           PrIME format including reconciliation tags\n"
    << "   <hosttree>              (string) name of file with guest tree in\n"
    << "                           PrIMEor Newick format\n"

    << "Options:\n"
    << "  -u, -h          This text.\n"
    << "  -B<option>      Options relating to the bith death process\n"
    << "   -Bp <float> <float>\n"
    << "                  Set BD parameters to these values.\n"
    << "   -Bt <float>    Fix top time to this value\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'p':
		{
		  if (opt + 1 < argc)
		    {		  
		      birthRate = atof(argv[++opt]);
		      if (opt + 1 < argc)
			{		  
			  deathRate = atof(argv[++opt]);
			}
		      else
			{
			  cerr << "Expected birth and death param (float) after -Bp/f\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected birth and death param (float) after -Bp/f\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      case 't':
		{
		  if (opt + 1 < argc)
		    {		  
		      topTime = atof(argv[++opt]);
		    }
		  else
		    {
		      cerr << "Expected top time param (float) after -Bt\n";
		      usage(argv[0]);
		      exit(1);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

