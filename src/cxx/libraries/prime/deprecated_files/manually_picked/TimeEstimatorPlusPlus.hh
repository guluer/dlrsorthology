/**
 * A class for estimating the time left given that we are to perform a certain
 * number of iterations.
 *
 * The time estimator uses the singleton pattern for easy access.
 *
 *
 * File:   TimeEstimatorPlusPlus.hh
 * Author: fmattias
 *
 * Created on January 15, 2010, 3:20 PM
 */

#include <iostream>

#ifndef _TIMEESTIMATORPLUSPLUS_HH
#define	_TIMEESTIMATORPLUSPLUS_HH

class TimeEstimatorPlusPlus {
public:
    TimeEstimatorPlusPlus(int totalNumberOfIterations);
    TimeEstimatorPlusPlus(const TimeEstimatorPlusPlus& orig);
    virtual ~TimeEstimatorPlusPlus();


    /**
     * Start the timer, i.e. create a reference time point.
     */
    void start();

    /**
     * Restart the timer.
     *
     */
    void reset(int totalNumberOfIterations);


    /**
     * Specify the number of iterations that has been performed since the
     * last update. Last update can either be the last call to start() or the
     * last call to update().
     *
     * numberOfIteration - The number of iterations that has been performed
     *                     since the last call to either start or
     *                     numberOfIterations.
     */
    void update(int numberOfIterations);

    /**
     * Returns the estimated number of seconds left.
     *
     */
    double getEstimatedTimeLeft();

    /**
     * Prints the estimated time left in the following format:
     *   "Estimated time left: HH hours, MM minutes, SS seconds"
     */
    void printEstiamtedTimeLeft();

    /**
     * Sets the output stream for printing estimated time left.
     * DEFAULT: std::cout.
     */
    void setOutputStream(std::ostream &output);

    /**
     * Returns the singleton time estimator instance.
     *
     */
    static TimeEstimatorPlusPlus *instance(int totalNumberOfIterations = 0);

private:

    // The total number of iterations that will be performed
    int m_numberOfIterationsLeft;

    // The number of iterations that has been performed
    int m_performedIterations;

    // Total time passed so far
    double m_totalTime;

    // Time stamp of last update to the time estimator
    clock_t m_lastUpdate;

    std::ostream *m_output;
};

#endif	/* _TIMEESTIMATORPLUSPLUS_HH */

