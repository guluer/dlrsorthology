#ifndef INTERGRALBIRTHDEATHPROBS_HH
#define INTERGRALBIRTHDEATHPROBS_HH
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "Probability.hh"
#include "BeepVector.hh"
#include "Tree.hh"

namespace beep
{

  class IntegralBirthDeathProbs : public BirthDeathProbs
  {
  public:
    //------------------------------------------------------------------------
    //
    // construct/destruct/assign
    //
    //------------------------------------------------------------------------
    //  beta is the Expected mean of the exponential distribution for toptime!
    IntegralBirthDeathProbs(Tree &S, Real birth_rate, Real death_rate, Real &beta);
    IntegralBirthDeathProbs(const IntegralBirthDeathProbs &M);
    virtual ~IntegralBirthDeathProbs();

    //------------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------------
    virtual IntegralBirthDeathProbs& operator=(const IntegralBirthDeathProbs& dsp);

    // User can set number Intervals in numerical integration
    void setNumberOfIntegrationIntervals(unsigned n);


    // We treat the top slice differently since we actually don't know the
    // time scale of the top slice. Therefore, we are adapting an exponential
    // (is it?) prior on the time before the species root.
    virtual Probability topPartialProbOfCopies(unsigned n_kids) const;

 
  protected:
    //------------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------------

    void calcBirthDeathProbs(Node &sn);

  protected:
    //------------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------------
    Real beta;
    unsigned nIntervals;
  };



  class DensityFunctor 
  {
  public:
    DensityFunctor(const IntegralBirthDeathProbs &model, 
		   const Real &bRate, const Real &dRate, const Real &beta, 
		   const unsigned nkids, const Probability &D);
    Real operator() (Real t);

  private:
    const IntegralBirthDeathProbs &M;
    Real bRate;
    Real dRate;
    Real beta;
    unsigned c;			// number of children
    Probability D;

    Real alfa;			// Avoid recomputing difference bRate - dRate
    Probability bD;		// Avoid stupid multiplication bRate * D
                                // When is this used?
  };

} //end namespace beep

#endif
 
