#ifndef RECONCILIATIONAPPROXIMATOR_HH
#define RECONCILIATIONAPPROXIMATOR_HH
/*********************************************************************

  ReconciliationApproximator

  The purpose of this class is to approximate $\Pr(Q, G| \lambda, \mu)$
  by sampling reconciliations and nodes times and compute an average.
  The node time sampling is delegated to another sub class of 
  ProbabilityModel: SeqProbApproximator.

**********************************************************************/
#include "Probability.hh"
#include "ProbabilityModel.hh"
#include "ReconciliationSampler.hh"
#include <mtl/mtl.h>

typedef mtl::matrix< Probability, 
		     mtl::rectangle<>, 
		     mtl::dense<>, 
		     mtl::column_major>::type OrthologyMatrix;

//! \todo{This class is probably going to be deleted /bens}
class ReconciliationApproximator : public ProbabilityModel
{
public:
  //
  // Construct / Destruct
  //
  ReconciliationApproximator(ReconciliationSampler &sampler,
		       Tree &G,
		       unsigned n_gamma_samples);

  ReconciliationApproximator(const ReconciliationApproximator &rsa); // Copy constuctor

 ~ReconciliationApproximator();


  //
  // Assignment
  //
  ReconciliationApproximator& operator=(const ReconciliationApproximator& rsa);


  //-----------------------------------------------------------------------
  //
  // Accessors
  //
  //-----------------------------------------------------------------------
  ReconciliationSampler& getReconciliationSampler();


  //
  // Get a string representation of a lower triangular matrix containing
  // orthology predictions.
  //
  string strOrthology();

  //
  // Reset orthology matrix
  //
  void resetOrthology();

  //
  // Update the orthology matrix;
  void recordOrthology(const GammaMap &gamma, const Probability p);

  //-----------------------------------------------------------------------
  //
  // ProbabilityModel interface
  //
  virtual Probability calculateDataProbability();
  virtual void update();


  //-----------------------------------------------------------------------
  //
  // Private support for ProbabilityModel interface (see code for comments)
  //
private:
  //  Probability cached_calculateDataProbability();
  Probability simple_calculateDataProbability();

  //----------------------------------------------------------------------
  //
  // Attributes
  //  
private:
  Tree                  &G;	// The gene tree
  ReconciliationSampler& sampler;
  unsigned               n_gamma_samples; // This tells us how many 
                                          // reconciliations to sample.

  OrthologyMatrix orthology;	// Collect orthology information in an lower triangular matrix

  //
  // An error is generated if you try to get more samples that this:
  //
  static const unsigned  MAX_N_SAMPLES=10000000; 

};

#endif
