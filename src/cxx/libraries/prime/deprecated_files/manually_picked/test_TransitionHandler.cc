#include "TransitionHandler.hh"
#include "MatrixTransitionHandler.hh"
#include "SequenceData.hh"
#include "SequenceType.hh"

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc != 4) 
    {
      cerr << "Usage: test_transitionHandler <model> <edge weight> <pos>\n";
      exit(1);
    }
  try
    {
      string model = argv[1];
      Real w = atof(argv[2]);
      unsigned pos = atoi(argv[3]);
      
      cout << "Testing named constructor 'create(<model>)'\n"
	   << "-------------------------------------------\n";
      MatrixTransitionHandler mth = MatrixTransitionHandler::create(model);
      cout << mth <<endl;

      cout << "Testing copy constructor 'create(<model>)'\n"
	   << "-------------------------------------------\n";
      MatrixTransitionHandler copy(mth);
      cout << copy << endl;

      cout << "Testing assignment operator(<model>)\n"
	   << "-------------------------------------------\n";
      MatrixTransitionHandler assign = mth;
      cout << assign << endl;


      cout << "All function below accesed with dynamic \n"
	   << "binding through TransitionHandler&\n"
	   << "-------------------------------------------\n";
      TransitionHandler& th = mth;
      cout << mth <<endl;

      cout << "Testing iscompatible()\n"
	   << "-------------------------------------------\n";
      cout << "against DNA data  = " 
	   << (th.isCompatible(SequenceData(SequenceType::getSequenceType("DNA")))
	       ?"true":"false")
	   << ",\nAminoAcid data = "
	   << (th.isCompatible(SequenceData(SequenceType::getSequenceType("protein")))
	       ?"true":"false")
	    <<"\nand Codon data = "
	    << (th.isCompatible(SequenceData(SequenceType::getSequenceType("Codon")))
	       ?"true":"false")
	   << endl << endl;


      cout << "Testing resetP(<w>)\n"
	   << "-------------------------------------------\n";
      mth.resetP(w);

      cout << "Testing col_mult(<pos>)\n"
	   << "-------------------------------------------\n";
      LA_Vector result1(mth.getAlphabetSize());
      mth.col_mult(result1, pos);
      cout << "col_mult(result1, 0) => result1 = " << result1 << endl;

      cout << "Testing mult()\n"
	   << "-------------------------------------------\n";
      LA_Vector result2(mth.getAlphabetSize());
      mth.mult(result1, result2);
      cout << "col_mult(result1, result2) => result2 = " << result2 << endl;

      cout << "Testing multWithPi()\n"
	   << "-------------------------------------------\n";
      mth.multWithPi(result2, result1);
      cout << "multWithPi(result2, result1) => result1 = " << result1 << endl;

      
    }
  catch(AnError e)
    {
      cout << "Error: ";
      e.action();
    }

  return 0;
}
