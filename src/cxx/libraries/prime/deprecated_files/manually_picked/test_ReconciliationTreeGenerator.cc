#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "PRNG.hh"
#include "ReconciliationTreeGenerator.hh"
#include "Tree.hh"
#include "TreeIO.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <birthrate> <deathrate> <number of leaves> [<outfile prefix>]\n"
       << "\n"
       << "Parameters:\n"
       << "   <birthrate>         a double\n"
       << "   <deathrate>         a double\n"
       << "   <number of leaves>  int, if <0 a random number is used\n"
       << "   <outfile prefix>    Optional. The generated trees, gammas\n"
       << "                       and gs are output to files named\n"
       << "                       <prefix>.species, <prefix>.gene and \n"
       << "                       <prefix>.gs, respectively\n";
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc < 4|| argc > 5) 
    {
      usage(argv[0]);
    }

  try
    {
      // get parameter from arguments
      //---------------------------------------------------------
      Real lambda = atof(argv[1]);
      Real mu = atof(argv[2]);

      PRNG R;			// Seeded by process id
      unsigned leaves;
      if(atoi(argv[3]) < 1)
	{
	  leaves = R.genrand_int32();
	}
      else
	{
	  leaves = atoi(argv[3]);
	}

      //Create a dummy Species tree
      //---------------------------------------------------------
      cout<<"Creating dummy tree\n"
	  <<"---------------------------------------------\n";
      Tree T = Tree::EmptyTree();

      // Set up the model for the species tree
      //---------------------------------------------------------
      cout<<"Set up a model for the species tree\n"
	  <<"---------------------------------------------\n";
      BirthDeathProbs BDP(T, lambda, mu);
      ReconciliationTreeGenerator RTG_S(BDP);
//       cout << "RTG_S, the model for generating the species tree\n";
//       cout << RTG_S;
      

      cout<<"simulating Speciestree\n"
	  <<"---------------------------------------------\n";
      RTG_S.generateGammaTree(leaves, true);
      Tree S = RTG_S.exportGtree();
      cout << "S, the species tree\n";
      cout << S << endl;

      cout << "Getting gamma for species tree\n"
	  <<"---------------------------------------------\n";
      GammaMap gamma_S = RTG_S.exportGamma();
//       cout << "gamma_S, the gamma from T to S\n";
//       cout << gamma_S;
//       cout << endl;

      cout << "Getting gs\n"
	  <<"---------------------------------------------\n";
//       cout << "gs_S, the leaf map from T to S\n";
//       cout << RTG_S.gs4os();
//       cout << endl;


      // Set up a model for the gene tree
      //---------------------------------------------------------
      cout<<"Set up a model for the gene tree\n"
	  <<"---------------------------------------------\n";
      BirthDeathProbs BDP_G(S, 0.3, 0.2);
      ReconciliationTreeGenerator RTG_G(BDP_G);
//       cout << "RTG_G, the model for generatirng the gene tree\n";
//       cout << RTG_G;

      cout<<"simulate a Genetree\n"
	  <<"---------------------------------------------\n";
      RTG_G.generateGammaTree(true);
      Tree G1 = RTG_G.exportGtree();

      cout << "Getting gamma\n"
	  <<"---------------------------------------------\n";
      GammaMap gamma_G1 = RTG_G.exportGamma();
      cout << gamma_G1;
      cout << "gamma_G1, the gamma from S to G\n";
      cout << endl;

      cout << "Getting gs\n"
	  <<"---------------------------------------------\n";
      cout << "gs_G1, the leaf map from S to G\n";
      cout << RTG_G.gs4os();
      cout << endl;

      cout<<"simulate another Genetree\n"
	  <<"---------------------------------------------\n";
      RTG_G.generateGammaTree(leaves, true);
      Tree G2 = RTG_G.exportGtree();
      cout << "G2, the gene tree\n";
      cout << G2 << endl;

      cout << "Getting gamma for G2\n"
	  <<"---------------------------------------------\n";
      GammaMap gamma_G2 = RTG_G.exportGamma();
      cout << gamma_G2;
      cout << "gamma_G2, the gamma from S to G\n";
      cout << endl;

      cout << "Getting gs\n"
	  <<"---------------------------------------------\n";
      cout << "gs_G2, the leaf map from S to G\n";
      cout << RTG_G.gs4os();
      cout << endl;

      if(argc == 5)
	{
	  string prefix(argv[4]);
	  TreeIO io;
	  ofstream species((prefix + ".species").data());
	  species << io.newickSpeciesStringTree(S) << "\n";

	  ofstream gene((prefix + ".gene").data());
	  gene << io.newickSpeciesStringTree(G2) << "\n";

	  ofstream gs((prefix + ".gs").data());
	  gs << RTG_G.gs4os() << "\n";
 	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";
      e.action();
    }
}
