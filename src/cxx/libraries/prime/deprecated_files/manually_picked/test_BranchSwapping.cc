#include "AnError.hh"
#include "BranchSwapping.hh"
#include "Tree.hh"
#include "TreeIO.hh"

bool useNT = true;
bool useBL = true;

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 2)
    {
      cerr << "usage: test_BranchSwapping <Guesttree>\n";
      exit(1);
    }

  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
      //      Tree G = io.readBeepTree(false, true, true, false, 0, 0);
      TreeIOTraits traits;
      traits.setNT(true);
      traits.setBL(true);
      Tree G = io.readBeepTree(traits, 0, 0);
      G.setName("G");

      traits.setNT(false);
      traits.setBL(false);
      string correctTree = io.writeBeepTree(G,traits,0);
      if(io.writeBeepTree(G,traits,0) == correctTree)//"(Leaf_1_0 ,((((Leaf_2_0 ,(Leaf_3_0 ,Leaf_3_1 )),(((Leaf_2_1 ,Leaf_2_2 ),Leaf_2_3 ),Leaf_3_2 )),((Leaf_2_4 ,Leaf_3_3 ),(Leaf_2_5 ,Leaf_3_4 ))),Leaf_4_0 ))[&&PRIME NAME=G]")
	{
	  cerr << "Start_tree correct\n";
	}

      BranchSwapping bs;
      cout << G;
      bs.doReRoot(G, true, true);
      cout << G;
      bs.doNNI(G, true, true);
      cout << G;



      exit(0);

      vector<double> old_rates;
      vector<double> old_times;
      vector<double> old_lengths;

      unsigned correct_counter = 0;
      unsigned opt_counter = 0;
      for (unsigned perturb_iter = 0; perturb_iter <= 0; perturb_iter++)
	{
	  for (unsigned perturb_type = 0; perturb_type <= 0; perturb_type++)
	    {
	      Tree old_G = G;
 	      cout << "G before: \n"
 		   << G.print(true,true,true,true);
// 	      cout << "G before: \n"
// 		   << G.print(false,false,false,false);
	      cout << io.writeBeepTree(G,traits,0) << "\n";
	      
	      double old_length_sum = 0.0;
	      for(unsigned n_iter = 0; n_iter < G.getNumberOfNodes(); n_iter++)
		{
		  Node* n = G.getNode(n_iter);
		  double n_time = G.getEdgeTime(*n);
		  old_times.push_back(n_time);
		  double n_length = G.getLength(*n);
		  old_lengths.push_back(n_length);
		  old_length_sum += n_length;
		  double n_rates = n_length/n_time;
		  old_rates.push_back(n_rates);
		}
	      
	      BranchSwapping bs;
	      
	      if (perturb_type == 0)
		{
// 		  cout << "doReRoot(G,true,true)\n";
// 		  bs.doReRoot(G,true,true);
		  cout << "doReRoot(G,true,false)\n";
		  bs.doReRoot(G,true,false);
		}
	      else if (perturb_type == 1)
		{
// 		  cout << "doNNI(G,true,true)\n";
// 		  bs.doNNI(G,true,true);
		  cout << "doNNI(G,true,false)\n";
		  bs.doNNI(G,true,false);
		}
	      else if (perturb_type == 2)
		{
// 		  cout << "doSPR(G,true,true)\n";
// 		  bs.doSPR(G,true,true);
		  cout << "doSPR(G,true,false)\n";
		  bs.doSPR(G,true,false);
		}
	      
// 	      double length_sum = 0.0;
// 	      for(unsigned n_iter = 0; n_iter < G.getNumberOfNodes(); n_iter++)
// 		{
// 		  Node* n = G.getNode(n_iter);
// 		  double n_length = G.getLength(*n);
// 		  length_sum += n_length;
// 		}
// 	      cout << "old_length_sum: " << old_length_sum << " length_sum: " << length_sum << "\n";
	      
 	      cout << "G after:\n"
 		   << G.print(true,true,true,true);
//	      cout << "G after: \n"
//		   << G.print(false,false,false,false);
	      cout << io.writeBeepTree(G,traits,0) << "\n\n";

 	      if (io.writeBeepTree(G,traits,0) == "(Leaf_1_0 ,((((Leaf_2_0 ,(Leaf_3_0 ,Leaf_3_1 )),(((Leaf_2_1 ,Leaf_2_2 ),Leaf_2_3 ),Leaf_3_2 )),((Leaf_2_4 ,Leaf_3_3 ),(Leaf_2_5 ,Leaf_3_4 ))),Leaf_4_0 ))[&&PRIME NAME=G]")
 		{
		  correct_counter++;
 		  cerr << perturb_iter << " G = correctG !\n";
 		}
 	      if (io.writeBeepTree(G,traits,0) == "(((Leaf_1_0 ,Leaf_4_0 ),((Leaf_2_4 ,Leaf_3_3 ),(Leaf_2_5 ,Leaf_3_4 ))),((Leaf_2_0 ,(Leaf_3_0 ,Leaf_3_1 )),(((Leaf_2_1 ,Leaf_2_2 ),Leaf_2_3 ),Leaf_3_2 )))[&&PRIME NAME=G]")
 		{
		  opt_counter++;
 		  cerr << perturb_iter << " G = optG !\n";
 		}
	      //	      G = old_G;	    
	    }
	}
      cerr << "correct_counter: " << correct_counter << " opt_counter: " << opt_counter << "\n";
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
}
