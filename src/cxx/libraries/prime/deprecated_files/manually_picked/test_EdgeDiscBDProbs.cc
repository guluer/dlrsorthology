#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "BirthDeathProbs.hh"
#include "EdgeDiscBDProbs.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscTree.hh"
#include "TreeDiscretizers.hh"
#include "TreeIO.hh"

static const int NO_OF_PARAMS = 4;

std::string ParamTreeFile = "";
beep::Real ParamToptime = 1.0;
beep::Real ParamBirthRate = 0.0;
beep::Real ParamDeathRate = 0.0;

/**
 * Prints current status.
 */
void printProbs(const beep::EdgeDiscBDProbs& p, beep::EdgeDiscTree* DS, beep::Tree& S)
{
	using namespace std;
	using namespace beep;
	
	// For comparison.
	BirthDeathProbs q(S, p.getBirthRate(), p.getDeathRate(), &ParamToptime);
	
	cout << "Birth & death rates: " << p.getBirthRate() << " & " << p.getDeathRate() << endl;
	cout << "Edge probabilities:" << endl;
	cout << "Edge no:\tDS one-to-one:\tS one-to-one:\tDS extinction:\tS extinction:" << endl;
	for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
	{
		const Node* n = (*it);
		cout
			<< n->getNumber() << '\t'
			<< p.getOneToOneProb(n) << '\t'
			<< q.partialProbOfCopies(*n, 1).val() << '\t' 
			<< p.getExtinctionProb(n) << '\t'
			<< q.partialProbOfCopies(*n, 0).val() << endl;
	}
	cout << "For each disc pt y, partial one-to-one probs from ancestral points x:" << endl;
	RealEdgeDiscPtMapIterator xend = DS->endPlus();
	for (Tree::const_iterator it = S.begin(); it != S.end(); ++it)
	{
		RealEdgeDiscPtMapIterator yend = DS->endPlus(*it);
		for (RealEdgeDiscPtMapIterator y = DS->begin(*it); y != yend; ++y)
		{
			cout << "y=(" << y.getPt().first->getNumber() << ',' << y.getPt().second << "):  ";
			for (RealEdgeDiscPtMapIterator x = DS->begin(y); x != xend; ++x)
			{
				cout << p.getOneToOneProb(x, y) << ", ";
			}
			cout << endl;
		}
	} 
}


/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS)
		{
			throw AnError("Wrong number of input parameters!");
		}
		
		// Read parameters.
		ParamTreeFile = argv[argIndex++];
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamToptime))
			throw AnError("Invalid top time value!");
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamBirthRate))
			throw AnError("Invalid birth rate!");
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamDeathRate))
			throw AnError("Invalid death rate!");

		// Create S from file, followed by discretization.
		Tree S(TreeIO::fromFile(ParamTreeFile).readHostTree());
		S.setTopTime(ParamToptime);
		cout << "**** Creating discretized tree with every edge splitted in three intervals. ****" << endl;
		EquiSplitEdgeDiscretizer disc(3, 3);
		EdgeDiscTree* DS = disc.getDiscretization(S);
		
		// Create BD-probs.
		cout << "**** Creating EdgeDiscBDProbs with specified rates. ****" << endl;
		EdgeDiscBDProbs DSprobs(*DS, ParamBirthRate, ParamDeathRate);
		printProbs(DSprobs, DS, S);
		cout << "**** Caching current values. ****" << endl;
		DSprobs.cache();
		cout << "**** Doubling birth rate, reducing death rate to half. ****" << endl;
		DSprobs.setRates(ParamBirthRate * 2.0, ParamDeathRate / 2.0);
		printProbs(DSprobs, DS, S);
		cout << "**** Restoring cache. ****" << endl;
		DSprobs.restoreCache();
		printProbs(DSprobs, DS, S);
		delete DS;
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <tree file> <top time> <birth rate> <death rate>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		cout << "Usage: " << argv[0] << " <tree file> <top time> <birth rate> <death rate>" << endl;
	}
	cout << endl;
};
