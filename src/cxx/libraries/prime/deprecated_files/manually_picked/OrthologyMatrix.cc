#include "OrthologyMatrix.hh"

using namespace std;

namespace beep {
  OrthologyMatrix::OrthologyMatrix(const Tree& G)
    : GenericMatrix<Probability>(G.getNumberOfNodes(), G.getNumberOfNodes()),
      G(G)
  {
    reset();
  }

  OrthologyMatrix::OrthologyMatrix(const OrthologyMatrix& om)
    : GenericMatrix<Probability>(om),
      G(om.G)
  {
  }

  OrthologyMatrix::~OrthologyMatrix()
  {
  }
  
  OrthologyMatrix&
  OrthologyMatrix::operator=(const OrthologyMatrix& om)
  {
    if (&om != this) {
      GenericMatrix<Probability>::operator=(om);
      G = om.G;
    }
    return *this;
  }

  OrthologyMatrix&
  OrthologyMatrix::operator+=(const OrthologyMatrix& om)
  {
    GenericMatrix<Probability>::operator+=(om);
    return *this;
  }
 
  OrthologyMatrix
  operator+(const OrthologyMatrix& om1, const OrthologyMatrix& om2)
  {
    OrthologyMatrix tmp(om1);
    return tmp += om2;
  }


  Probability
  OrthologyMatrix::operator()(unsigned i, unsigned j) const
  {
    if (i < j)
      {
	return get_element(j, i);
      }
    else
      {
	return get_element(i, j);
      }
  }

  Probability&
  OrthologyMatrix::operator()(unsigned i, unsigned j)
  {
    if (i < j)
      {
	return get_element(j, i);
      }
    else
      {
	return get_element(i, j);
      }
  }


  void
  OrthologyMatrix::reset()
  {
    // Reset the orthology matrix. We only use the lower half
    for (unsigned i = 1; i < nrows(); i++)
      {
	for (unsigned j = 0; j < i; j++) 
	  {
	    get_element(i, j) = Probability(0.0);
	  }
      }
  
  }

  void
  OrthologyMatrix::recordOrthology(const GammaMap &gamma, 
				   const Probability &p)
  {
    multimap<int, int> ortho_list = gamma.getOrthology();
    for(multimap<int,int>::iterator iter = ortho_list.begin();
	iter != ortho_list.end();
	iter++) 
      {
	//	cerr << (*iter).first << "\t" << (*iter).second << endl;
	if ((*iter).first > (*iter).second) // Remember: Only lower half
	  {
// 	    Probability tmp = 	    get_element((*iter).first, (*iter).second) + p;
// 	    cerr << tmp - p << endl;
// 	    get_element((*iter).first, (*iter).second) = tmp;
// 	    get_element((*iter).first, (*iter).second) += 1.0;
// 	    cerr << 	    get_element((*iter).first, (*iter).second) << endl;
	    get_element((*iter).first, (*iter).second) += p;
	  }
	else
	  {
// 	    Probability tmp = 	    get_element((*iter).second, (*iter).first) + p;
// 	    cerr << tmp - p << endl;
// 	    get_element((*iter).second, (*iter).first) = p;
// 	    get_element((*iter).second, (*iter).first) += 1.0;
// 	    cerr << 	    get_element((*iter).second, (*iter).first) << endl;
	    get_element((*iter).second, (*iter).first) += p;
	  }
      }
  }


  string
  OrthologyMatrix::strRepresentation() const
  {
    int size = nrows(); // Needs changing if own OrthologyMatrix 
    ostringstream buf;
    buf.str().reserve(size * (size + 1) * 5); // Allocate some space

    // Return a string representation of the orthology matrix.
    // Two version available: Matlab and list of pairs. The Matlab version 
    // does not play well with our script mcmc_analysis which want semi-colon 
    // delimited fields.
#ifdef MATLAB_ORTHOLOGS
    // Using Matlab/Octave format, but with truncated rows. For instance, 
    // instead of 
    //    [[ 0 0 0]; [0.5 0 0]; [1 0 0]]
    // we write
    //    [[ 0]; [0.5 0]; [1 0 0]]
    // which is everything up to and including the diagonal element and 
    // nothing more.
    //-------------------------------------------------------------------
    buf << "[";
    for (int row=0; row < size; row++)
      {
	buf << "[";
	for (int col=0; col < row; col++) 
	  {
	    buf << get_element(row,col).val() << " ";
	  }
	buf << " 0];";
      }
    buf << "]";
#else
    // A list of pairs. Format: 
    // orthology := '[' + ortholist + ']'
    // ortholist := empty
    //            | orthopair + ' ' + ortholist
    // orthopair := '[' + id1 + ',' + id2 + '] = ' + probability
    //-------------------------------------------------------------------
    buf << "[";
    for (int row=1; row < size; row++)
      {
	Node *v1 = G.getNode(row);
	for (int col=0; col < row; col++) 
	  {
	    if (row > col && get_element(row, col) > 0.0)
	      {
		Node *v2 = G.getNode(col);
		buf << '[' 
		  +  v1->getName() 
		  +  ',' 
		  +  v2->getName()
		  +  "]="
		    << get_element(row,col).val();
	      }
	  }
      }
    buf << "]";
#endif
    return buf.str();
  }


  void
  OrthologyMatrix::testing_set_elem()
  {
    get_element(1, 0) = 0.5; 
  };
}
