#include "PRNG.hh"
#include <iostream>

int main()
{
  using namespace std;
  PRNG rand;

  double sum1 = 0;
  double sum2 = 0;
  double sum3 = 0;

  double min1 = 0;
  double min2 = 0;
  double min3 = 0;

  double max1 = 0;
  double max2 = 0;
  double max3 = 0;

  double med1 = 0;
  double med2 = 0;
  double med3 = 0;

  double max = 100000;

  cerr << "Seed: " << rand.getSeed() << endl;
  for (double i = 0; i < max; i+=1.0)
    {
      double r1 = rand.genrand_real1(); 
      double r2 = rand.genrand_real2();      
      double r3 = rand.genrand_real3();

      //      cout << r3;

      sum1 += r1;
      sum2 += r3;
      sum3 += r3;
      
      if(r1 < 0.5)
	min1 += 0.5 - r1;
      else if(r1 > 0.5)
	max1 += r1 - 0.5;
      else
	med1 += r1;

      if(r2 < 0.5)
	min2 += 0.5 - r2;
      else if(r2 > 0.5)
	max2 += r2 - 0.5;
      else
	med2 += r2;

      if(r3 < 0.5)
	min3 += 0.5 - r3;
      else if(r3 > 0.5)
	max3 += r3 - 0.5;
      else
	med3 += r3;
    }
  cerr << "Mean for genrand_real1 = " << sum1 / max << endl;
  cerr << "Mean for genrand_real2 = " << sum2 / max << endl;
  cerr << "Mean for genrand_real3 = " << sum3 / max << endl;

  cerr << "AverageMin for genrand_real1 = " << min1 / max << endl;
  cerr << "AverageMax for genrand_real1 = " << max1 / max << endl;
  cerr << "AverageMed for genrand_real1 = " << med1 / max << endl;

  cerr << "AverageMin for genrand_real2 = " << min2 / max << endl;
  cerr << "AverageMax for genrand_real2 = " << max2 / max << endl;
  cerr << "AverageMed for genrand_real2 = " << med2 / max << endl;

  cerr << "AverageMin for genrand_real3 = " << min3 / max << endl;
  cerr << "AverageMax for genrand_real3 = " << max3 / max << endl;
  cerr << "AverageMed for genrand_real3 = " << med3 / max << endl;

  return 0;
}

