#include "HybridHostTreeModel.hh"

#include "Node.hh"
#include "AnError.hh"
#include "BeepVector.hh"
#include "HybridTreeIO.hh"
#include "HybridTree.hh"
#include "PRNG.hh"

#include <iostream>

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  try
    {
      if(argc < 3)
	{
	  cerr << "Usage: "
	       << argv[0]
	       << " <hybrid network> <useTree>\n";
	  exit(0);
	}
     //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      int Rseed = rand.getSeed();
      cerr << "seed = " << Rseed << endl;

      unsigned opt = 1;
      HybridTreeIO io_in = HybridTreeIO::fromFile(string(argv[opt++]));
      TreeIOTraits tot;
      tot.setNT(true);
      vector<HybridTree> Tvec = io_in.readAllHybridTrees(tot, 0, 0);
      HybridTree& T = Tvec[atoi(argv[opt++])];
      if(T.getName() == "Tree")
	{
	  T.setName("Host");
	}
      
//       cout << "Hybrid Tree:\n" << T.print() << endl;
//       cout << "Update binary tree\n";
//       cout << T.getBinaryTree();

      
      cout << "First construct a HybridHostTreeModel with tree First\n"
	   << "-----------------------------------------------------\n";      

      HybridHostTreeModel A(T, 4, 0.3, 0.1, 10);
//       cout << A;
      // Perform and time the Likelihood calculation
      Probability oldP = 0;
      unsigned Max = 1000;
      unsigned diff = Max/10;
//       for(unsigned i = Max; i > 1; i -= diff)
      for(unsigned i = Max; i > 1; i /= 2)
	{
	  if(i == diff)
	    diff = diff/10;
	  A.setMaxGhosts(i);
	  time_t t0 = time(0);
	  clock_t ct0 = clock();
	  
	  Probability P = A.calculateDataProbability();
	  assert(P > 0);
	  if(i == Max)
	    {
	      oldP = P;
	    }
	  time_t t1 = time(0);    
	  
	  clock_t ct1 = clock();
//       cerr << "Wall time: " << difftime(t1, t0) << " s"
// 	   << endl
// 	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
// 	   << endl;

      
	  cout << i  
	       << "\tP = " << P.val() << " = exp(" << P << ")" 
	       << "\t diff = " << ((oldP-P)/oldP).val() 
	       << "\tcomp.time = " << difftime(t1, t0) << " s\n";
	}

      exit(2);

      cout << "\n\nConstruct a new hybrid tree\n"
	   << "---------------------------------\n\n";
      HybridTree T2;
      T2.setName("First");
      
      // Leaves
      T2.addNode(0, 0, 0, "A", true);
      T2.addNode(0, 0, 1, "B");
      T2.addNode(0, 0, 2, "C");
      
      // Internal nodes
      T2.addNode(T2.getNode(0), T2.getNode(1), 3, "D");
      T2.addNode(T2.getNode(3), T2.getNode(2), 4, "E");
      
      T2.setRootNode(T2.getNode(4));
      
      cout << "\nThe result is\n"
	   << "Node\tparent\totherParent\n";
      for(unsigned i = 0; i < T2.getNumberOfNodes(); i++)
	{
	  Node* n = T2.getNode(i);
	  if(n)
	    {
	      cout << i << "\t";
	      if(n->isRoot())
		{
		  cout << "ROOT\t";
		}
	      else
		{
		  cout << n->getParent()->getNumber() << "\t";
		}
	      if(T2.isHybridNode(*n))
		{
		  cout << T2.getOtherParent(*n)->getNumber() << "\n";
		}
	      else
		{
		  cout << "NO HYBRID\n";
		}
	      
	    }
	  else
	    cerr << "FAILED\n";
	}
      
      RealVector times2(5);
      times2[0u] = 0;
      times2[1] = 0;
      times2[2] = 0;
      times2[3] = 0.5;
      times2[4] = 1.0;
      T2.setTimes(times2);
      
      cout << "Hybrid Tree:\n" << T2.print() << endl;
      cout << "Update binary tree\n";
      cout << T2.getBinaryTree();
      
      cout << "First construct a HybridHostTreeModel with tree First\n"
	   << "-----------------------------------------------------\n";      
      HybridHostTreeModel A2(T2, 0.4, 0.3, 0.01);
      cout << A2;
      Probability P2 = A2.calculateDataProbability();
      
      cout << "P2 = " << P2.val() << endl;
      

      cout << "Then copy construct B from A\n"
	   << "-----------------------------------------------------\n";      
      HybridHostTreeModel B(A);
      cout << B;
      cout << "Change the rates of B so lambda = 1.2, mu = 0.7 and rho = 1.0\n"
	   << "-----------------------------------------------------\n";      
      B.setLambda(1.2);
      B.setMu(0.7);
      B.setRho(1.0);
      cout << B;
      cout << "Lastly assign A to B\n"
	   << "-----------------------------------------------------\n";      
      B = A;
      cout << B;

      
//       Probability P = A.calculateDataProbability();
      
//       cout << "P = " << P.val() << endl;
      
    }
  catch(AnError e)
    {
      e.action();
    }
};
