#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "GammaMap.hh"
#include "LambdaMap.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "ReconciliationTimeModel.hh"
#include "ReconciliationTimeSampler.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <guest tree> <species tree> <gs> <birthrate> <deathrate> [<seed>]\n"
       << "\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc < 6 || argc > 7) 
    {
      usage(argv[0]);
    }

  try
    {
      // get parameter from arguments
      //---------------------------------------------------------
      string gfile = argv[1];
      string sfile = argv[2];
      string gsfile = argv[3];
      Real lambda = atof(argv[4]);
      Real mu = atof(argv[5]);
      PRNG R;			// Seeded by process id
      if(argc == 7)
	{
	  R.setSeed(atof(argv[6]));
	}

      TreeIO io = TreeIO::fromFile(gfile);
      Tree G = io.readGuestTree();
      io.setSourceFile(sfile);
      Tree S = io.readHostTree();
      StrStrMap gs = io.readGeneSpeciesInfo(gsfile);

      // Set up the model for the species tree
      //---------------------------------------------------------
      BirthDeathProbs BDP(S, lambda, mu);
      ReconciliationSampler RS(G, gs, BDP);
      ReconciliationTimeSampler RTS(RS);
      ReconciliationTimeModel rtm(G, BDP, RS.getGamma(), true); 
      for(unsigned i = 0; i < 5; i++)
	{
	  GammaMap gamma = RS.sampleReconciliation();
	  RTS.update();
// 	  ReconciliationTimeSampler RTS(G, BDP, gamma);
	  Probability P = RTS.sampleTimes();
	  rtm.update();
	  Probability Q = rtm.calculateDataProbability();
	  
	  cout << " P = " << P << "  Q = " << Q << endl << endl;
	}
    }
  catch (AnError e)
    {
      cerr << "error found\n";
      e.action();
    }
}
