/* 
 * File:   DiscretizedDensity.cc
 * Author: fmattias
 * 
 * Created on December 3, 2009, 2:26 PM
 */

#include "DiscretizedDensity.hh"

namespace beep {

using namespace std;

DiscretizedDensity::DiscretizedDensity(Density2P &continousDensity,
                                       float nonZeroStart,
                                       float nonZeroEnd,
                                       int intervals) :
                                       m_pdf(intervals),
                                       m_cdf(intervals),
                                       m_continousDensity(continousDensity),
                                       m_intervals(intervals),
                                       m_nonZeroStart(nonZeroStart),
                                       m_nonZeroEnd(nonZeroEnd)
                                       
{
    if(nonZeroStart > nonZeroEnd) {
        throw AnError("DiscretizedDensity::In constructor, nonZeroStart > nonZeroEnd.");
    }
    if(intervals < 1) {
        throw AnError("DiscretizedDensity::In constructor, at least 1 interval required.");
    }

    m_intervalLength = (nonZeroEnd - nonZeroStart) / intervals;
    
    createPDFAndCDF();
}


DiscretizedDensity::DiscretizedDensity(const DiscretizedDensity& other) :
                                m_pdf(other.m_pdf),
                                m_cdf(other.m_cdf),
                                m_continousDensity(other.m_continousDensity),
                                m_intervals(other.m_intervals),
                                m_intervalLength(other.m_intervalLength),
                                m_nonZeroStart(other.m_nonZeroStart),
                                m_nonZeroEnd(other.m_nonZeroEnd)

{
}


void DiscretizedDensity::createPDFAndCDF()
{
    Real normalizationFactor = 0.0;
    // Calculate pdf and cdf
    for(int interval = 0; interval < m_intervals; interval++) {
        Real start = m_nonZeroStart + interval*m_intervalLength;
        Real end = m_nonZeroStart + (interval + 1)*m_intervalLength;
        m_pdf[interval] = m_continousDensity.pdf((start + end)/2.0).val();
        normalizationFactor = normalizationFactor + m_pdf[interval];
    }

    // Protect against -inf and only having one interval with
    // non zero probability.
    normalizationFactor += 1e-10;

    // Normalize pdf
    Real cdfSum = 0.0;
    for(int interval = 0; interval < m_intervals; interval++){
        m_pdf[interval] = m_pdf[interval] / normalizationFactor;

        if(m_pdf[interval] > 1.0) {
            cerr << "An error in DiscretizedDensity::createPDFAndCDF: Probability larger than 1.0 " << endl;
            cerr << "m_pdf[" << interval << "] = " << m_pdf[interval] << endl;
            cerr << "normalizationFactor: " << normalizationFactor << endl;
            cerr << "gamma mean: " << m_continousDensity.getMean() << " variance: " << m_continousDensity.getVariance() << endl;
        }
        assert(m_pdf[interval] <= 1.0);
        cdfSum +=  m_pdf[interval];
        m_cdf[interval] = cdfSum;
    }
}

void DiscretizedDensity::reDiscretize()
{
    createPDFAndCDF();
}

int DiscretizedDensity::getInterval(Real x) const
{
    if(x < m_nonZeroStart || x > m_nonZeroEnd) {
        return -1;
    }
    
    if(x != m_nonZeroEnd) {
        return (x - m_nonZeroStart) / m_intervalLength;
    }
    else {
        return m_intervals - 1;
    }
}

Real DiscretizedDensity::pdf(Real x) const
{
    if(x >= m_nonZeroStart && x <= m_nonZeroEnd) {
        int interval = getInterval(x);
        return m_pdf[interval];  
    }
    else {
        return 0.0;
    }
}
Real DiscretizedDensity::cdf(Real x) const
{
    if(x >= m_nonZeroStart && x <= m_nonZeroEnd) {
        int interval = getInterval(x);
        return m_cdf[interval];
    }
    else if(x > m_nonZeroEnd) {
        return 1.0;
    }
    else {
        return 0.0;
    }
}

DiscretizedDensity::~DiscretizedDensity() {
}

}