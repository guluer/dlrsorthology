#include "HybridTree.hh"
#include "Node.hh"
#include "AnError.hh"
#include "BeepVector.hh"
#include "HybridTreeIO.hh"

#include <iostream>

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 3)
    {
      cerr << "usage: test_HybridTree <multiSTreefile> <useStree#> \n";
      exit(1);
    }
  try
    {
      unsigned opt = 1;
      string host(argv[opt++]);
      cerr << "host =  " << host << endl;
      HybridTreeIO io2 = HybridTreeIO::fromFile(host);
      vector<HybridTree> Svec = io2.readAllHybridTrees(0); 
      unsigned useS = atoi(argv[opt++]);
      cerr << Svec[useS];
      HybridTree T1 = Svec[useS];

      cout << "Print Hybrid Tree:\n"
	   << T1.print() 
	   << endl;
      cout << "NumberOf nodes = " << T1.getNumberOfNodes() << endl;

      cout << "Get binary tree\n";
      cout << T1.getBinaryTree();
      cout << "Number Of nodes = " << T1.getBinaryTree().getNumberOfNodes() << endl;

      cout << "binary2Hybrid\n"
	   << T1.printBinary2Hybrid()<<endl;
      cout << "hybrid2Binary\n" 
	   << T1.printHybrid2Binary() << endl;

      cout << "newick format\n"
	   << HybridTreeIO::writeHybridTree(T1) << endl;
      
      cout << "Test findNode -- access via leaf names\n";
      for(unsigned i = 0; i < T1.getNumberOfNodes(); i++)
	{
	  Node* n = T1.getNode(i);
	  if(n->isLeaf())
	    {
	      string name = n->getName();
	      cout << name << " -- \t" << T1.findNode(name)->getNumber() << "\n";
	    }
	}
      
      
      cout << "\nTest constructing from binary tree (derived from T1)\n"
	   << "------------------------------------------------------\n";
      HybridTree T2(T1.getBinaryTree());
      T2.setName("T2");
      cout << T2.print() << endl;
      cout << "NumberOf nodes = " << T2.getNumberOfNodes() << endl;
      cout << "Get binary tree\n";
      cout << T2.getBinaryTree();
      cout << "Number Of nodes = " << T2.getBinaryTree().getNumberOfNodes() 
	   << endl;

      cout << "Test Copyconstructor T3 from T2\n"
	   << "------------------------------------------------------\n";
      HybridTree T3(T2);
      cout << T3.print() << endl;
      T3.setName("T3");
      cout << "NumberOf nodes = " << T3.getNumberOfNodes() << endl;
      cout << "Get binary tree\n";
      cout << T2.getBinaryTree();
      cout << "Number Of nodes = " << T1.getBinaryTree().getNumberOfNodes() 
	   << endl;
      
      cout << "Finally test assignment T3 = T1\n"
	   << "------------------------------------------------------\n";
      T3 = T1;
      cout << T3.print() << endl;
      cout << "NumberOf nodes = " << T3.getNumberOfNodes()<< endl;
      cout << "NumberOf nodes = " << T3.getNumberOfNodes() << endl;
      cout << "Get binary tree\n";
      cout << T2.getBinaryTree();
      cout << "Number Of nodes = " << T3.getBinaryTree().getNumberOfNodes() 
	   << endl;

      cout << "Finally check that original tree is unaffected\n"
	   << "------------------------------------------------------\n";

      cout << "Print Hybrid Tree:\n"
	   << T1.print() 
	   << endl;
      cout << "NumberOf nodes = " << T1.getNumberOfNodes() << endl;

      cout << "Get binary tree\n";
      cout << T1.getBinaryTree();
      cout << "Number Of nodes = " << T1.getBinaryTree().getNumberOfNodes() << endl;
      cout << "end\n";
    }
  catch(AnError e)
    {
      e.action();
    }
};
