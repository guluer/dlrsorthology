#include "MCMCModel.hh"
#include "MCMCObject.hh"

namespace beep
{
  class test : public MCMCModel
  {
  public:
    // Use default constructors and destructors
    
    // define pure virtual functions
    MCMCObject suggestNewState()
    {
      return MCMCObject(0.3, 0.3);
    };
    
    Probability initStateProb()
    { return 1.0;};
    
    Probability currentStateProb()
    { return 0.5;};
    
    void commitNewState()
    { registerCommit();};
    
    void discardNewState()
    { registerDiscard();};
    
    std::string strRepresentation() const
    { return "STRING";};
    
    std::string strHeader() const
    { return "HEADER";};
    
    unsigned nParams() const
    { return 0;};
  };
}
  int main()
  {
    using namespace beep;
    using namespace std;
    cout << "Constructing test t\n";
    test t;

    MCMCObject MOb = t.suggestNewState();
    cout << "\nt.suggestNewState() produces: Mob(" 
	 << MOb.stateProb.val() << ", " 
	 << MOb.propRatio.val() << ")\n" << endl;

    cout << "t.initStateProb() = " << t.initStateProb().val() << endl << endl;
    cout << "t.currentStateProb = " << t.currentStateProb().val() << endl<< endl;

    cout << "t.ommitNewState \n";
    t.commitNewState();
    cout << "\nt.discardNewState\n";
    t.discardNewState();

    cout << "\nt.strHeader: " << t.strHeader() << endl;  
    cout << "\nt.strRepresentation: " << t.strRepresentation() << endl;  
    cout << "\nt.nParams() = " << t.nParams() << endl;

    cout << "\nt.getAcceptanceRatio = " << t.getAcceptanceRatio() << endl;

    PRNG& r = t.getPRNG();
    cout << "\nTesting t.getPRNG()->genrand_modulo(3) = " 
	 <<  r.genrand_modulo(3) << endl;

    cout << "\nTesting copy constructor: u(t) \n";
    test u(t);
    cout << "\tCommiting state\n";
    u.commitNewState();
    cout << "\tu.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;

    cout << "\nTesting assignment operator: t = u\n";
    t = u;
    cout << "\tt.getAcceptanceRatio = " << u.getAcceptanceRatio() << endl;

  
    return(0);
  }

