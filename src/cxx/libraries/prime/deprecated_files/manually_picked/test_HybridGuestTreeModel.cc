#include "AnError.hh"
#include "BirthDeathInHybridProbs.hh"
#include "BranchSwapping.hh"
#include "HybridGuestTreeModel.hh"
#include "HybridTree.hh"
#include "HybridTreeIO.hh"
#include "StrStrMap.hh"

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc < 7 || argc > 9)
    {
      cerr << "usage: test_HybridGuestTreeModel <multiSTreefile> <useStree#> <multiGTreefile> <useGTRee#>  [<multigsfile>] <birthrate> <deathrate> <topTime>\n";
      exit(1);
    }
  try
    {
      unsigned opt = 1;
      //Get trees
      //---------------------------------------------
      string host(argv[opt++]);
      cerr << "host =  " << host << endl;
      HybridTreeIO io2 = HybridTreeIO::fromFile(host);
      vector<HybridTree> Svec = 
	io2.readAllHybridTrees();//true, false, false, false,0,0); 
      unsigned useS = atoi(argv[opt++]);
      cerr << Svec[useS];
      HybridTree S = Svec[useS];
      if(S.getName() == "Tree")
	{
	  S.setName("Host");
	}

      cout << "Hybrid tree\n"
	   << S.print();
      cout << "Tree\n"
	   << Tree(S).print();

      string guest(argv[opt++]);
      TreeIO io = TreeIO::fromFile(guest);
      vector<StrStrMap> gsvec;

      vector<Tree> Gvec = io.readAllBeepTrees(0, &gsvec); 
      unsigned useG = atoi(argv[opt++]);
      Tree& G = Gvec[useG];
      G.setName("G");
      G.perturbedTree(false);
      cerr << G;

      if(gsvec.size() == 0)
	{
	  gsvec = TreeIO::readGeneSpeciesInfoVector(argv[opt++]);
	}
      StrStrMap& gs = gsvec[useG];
      cout << "gs = \n" << gs << endl;;

      Real brate = atof(argv[opt++]);
      Real drate = atof(argv[opt++]);

      Real topTime = 1.0;
      BirthDeathInHybridProbs bdp(S, brate, drate, &topTime);
      cout << "Testing constructors:"
	   << "------------------------------------------\n"
	   << "First construct HybridGuestTreeModel a with G as the tree.\n";
      HybridGuestTreeModel a(G, S, gs, bdp);
      cout << indentString(a.print());
      cout << "a: Probability of G is " ;
      cout <<  a.calculateDataProbability()
	   << "\n";
      exit(0);

      cout << "\nThen construct HybridGuestTreeModel b with S as the tree.\n";
      HybridTree S2 = S;
      StrStrMap gs2;
      for(unsigned i = 0; i < S.getBinaryTree().getNumberOfNodes(); i++)
	 {
	   Node* b = S.getBinaryTree().getNode(i);
	   gs2.insert(b->getName(), S.getCorrespondingHybridNode(b)->getName());
	 }
	   
      HybridGuestTreeModel b(S.getBinaryTree(), S, gs2, bdp);
      cout << indentString(b.print())
	   << "\nThen use copy constructor to create c from b\n";
      HybridGuestTreeModel c(b);
      try
	{
	  cout << indentString(c.print())
	       << "\nFinally use operator=, to set c = a:\n";
	  c = a;
	  cout << indentString(c.print());
	}
      catch(AnError e)
	{
	  cerr << "As expected, this did not work, but yielded AnError:\n";
	  cerr << indentString(e.message());
	  cerr << "I guess that we really ought to  fix the assignment\n"
	       << "operator of GammaMap, eventually\n\n";
	}
      bdp.update();
      a.update();
      cout << "a: Probability of G is " ;
      Probability prG =  a.calculateDataProbability();
      cout << prG
	   << "\n";
    }      
  catch(AnError e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }


  return(0);
};
