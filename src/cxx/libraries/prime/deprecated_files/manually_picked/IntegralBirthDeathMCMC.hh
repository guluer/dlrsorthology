#include <string>
#include <iostream>

#include "IntegralBirthDeathProbs.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "StdMCMCModel.hh"
#include "Tree.hh"

namespace beep
{

  class IntegralBirthDeathMCMC : public StdMCMCModel,
 				 public IntegralBirthDeathProbs
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    IntegralBirthDeathMCMC(MCMCModel& prior,
			   Tree &S, Real birthRate, Real deathRate,
			   Real beta,
			   bool estimateRates = true);
    ~IntegralBirthDeathMCMC();


    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const IntegralBirthDeathMCMC& A);
    std::string print() const;

  private:
//     //-------------------------------------------------------------
//     //
//     // Implementation
//     //
//     //-------------------------------------------------------------

//     // Utility function for changing birth/death rates.
//     // Returns a number in the interval [0.8, 1.25].
//     //-------------------------------------------------------------
//     Real rateChange();

    //-------------------------------------------------------------
    //
    // Attributes - none
    //
    //-------------------------------------------------------------
    Real old_birth_rate;
    Real old_death_rate;
    bool estimateRates;

    //! The suggestion function uses a normal distribution around the current
    //! value. For the proposal-ratios to function right, we need to keep the 
    //! variance to the distribution constant. This is a problem since we do not
    //! know the location/scale of the distribution ahead of time. As a fix, we
    //! store a variance initiated from the start values of dupl/loss rates.
    Real suggestion_variance;
  };
}//end namespace beep
