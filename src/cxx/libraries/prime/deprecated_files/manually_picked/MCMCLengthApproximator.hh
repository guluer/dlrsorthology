#ifndef MCMCLENGTHAPPROXIMATOR_HH
#define MCMCLENGTHAPPROXIMATOR_HH

#include "LengthRateModel.hh"
#include "PRNG.hh"
#include "ReconciledTreeTimeMCMC.hh"

namespace beep
{
  //---------------------------------------------------------
  //
  // MCMCLengthApproximator 
  //! Integrates probability over some parameter using MCMC
  //
  //---------------------------------------------------------  
  class MCMCLengthApproximator 
    : public LengthRateModel
  {
  public:
    //---------------------------------------------------------
    //
    //! Construct/destruct/assign
    //@{
    //---------------------------------------------------------  
    MCMCLengthApproximator(ReconciledTreeTimeMCMC& prior,
			   Density2P& rateProb_in, 
			   Tree& T_in);
    ~MCMCLengthApproximator();
    MCMCLengthApproximator(const MCMCLengthApproximator&r);
    MCMCLengthApproximator& 
    operator=(const MCMCLengthApproximator&r);
    //@}

    //---------------------------------------------------------
    //
    // Interface
    //
    //--------------------------------------------------------- 
    void setMCMCparams(unsigned iterations, unsigned thinning, 
		       unsigned burn_in, bool useML);
    //-------------------------------------------------------------------
    // ProbabilityModel interface
    //-------------------------------------------------------------------
    Probability calculateDataProbability();
    void update();

    //----------------------------------------------------------------------
    // I/O
    //----------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const MCMCLengthApproximator& A);
    std::string print() const;

  protected: 
    //----------------------------------------------------------------------
    // 
    // Implementation
    // 
    //----------------------------------------------------------------------
    Probability calcMCMC();
    Probability calcML();
    
  protected: 
    //---------------------------------------------------------
    //
    // Attributes
    //
    //--------------------------------------------------------- 
    ReconciledTreeTimeMCMC* model;
    unsigned nIter;
    unsigned thin;
    unsigned burnin;
    bool doML;
    PRNG R;
  };

}// end namespace beep
#endif
