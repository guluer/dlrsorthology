#include "Node.hh"
#include "Tree.hh"
#include "RandomTreeGenerator.hh"

#include <vector>
#include <string>
#include <sstream>

int
main()
{
  using namespace beep;
  using namespace std;

  unsigned nLeaves = 10; 
  vector<string> names;
  for(unsigned i = 0; i < nLeaves; i++)
    {
      ostringstream oss;
      oss << "Leaf_" << i;
      names.push_back(oss.str());
    }

  Tree T = RandomTreeGenerator::generateRandomTree(names);
  
  cout << T;

  return(0);
};
