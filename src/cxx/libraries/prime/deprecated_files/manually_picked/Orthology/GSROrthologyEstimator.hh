//**********************************************************************************************
// FOR REFERENCE, THIS CLASS CONTAINS A SNAPSHOT OF PETER/MATTIAS' EXTENDED GSR IMPLEMENTATION
// AS OF 2010-06-10.
// NO COMPATILIBILITY TESTS HAVE BEEN MADE, I.E. EXPECT COMPILATION ERRORS TO START WITH.
// HOWEVER, IT SHOULD EASE THE TRANSITION TO A MORE COHERENT STRUCTURE. NOTE THAT PETER/MATTIAS'
// EXTENDED VERSION MAY EXIST IN A MORE RECENT STATE IN A SEPARATE SVN-BRANCH.
//**********************************************************************************************

///*
// * File:   GSROrthologyEstimator.hh
// * Author: peter9
// *
// *
// * A class for estimating orthology probabilities based on the GSR model.
// * There are several ways to use this class based on the needs of the user.
// *
// * If only one orthology calculation is going to be performed, then it is
// * recommended to use 'estimateOrthologyFromMCMC' or
// * 'estimateOrthologyFromFile'. These options exist to improve
// * memory-efficiency.
// *
// * If several consecutive orthology estimations are going to be calculated,
// * it is recommended to first let GSROrthologyEstimator calculate all MCMC
// * states in advance by either reading them from file using 'readStatesFromFile'
// * or by using 'calculateStatesByMCMC'. Then for each new orthology estimation
// * use 'estimateOrthology'.
// *
// *
// * If the user already has calculated a state and just wishes to know the
// * orthology probability in that state for a pair of genes (or groups of genes),
// * then the static member 'estimateOrthologyFromState' is convenient to use.
// *
// *
// * Created on December 16, 2009, 3:25 PM
// */
//
//#ifndef _GSRORTHOLOGYESTIMATOR_HH
//#define	_GSRORTHOLOGYESTIMATOR_HH
//
//#include <utility>
//#include "SetOfNodes.hh"
//#include "EdgeDiscTree.hh"
//#include "StrStrMap.hh"
//#include "SequenceData.hh"
//#include "EdgeDiscGSR.hh"
//#include "Tree.hh"
//#include <vector>
//#include "CT/EdgeDiscGSRObjects.hh"
//#include "TreeIO.hh"
//#include <iostream>
//#include <fstream>
//#include "Tokenizer.hh"
//#include "MatrixTransitionHandler.hh"
//
//namespace beep{
//    using namespace std;
//
//
//
//    class GSROrthologyEstimator {
//    public:
//        //For ease of notation...
//        typedef vector< string > Group;
//        typedef pair< Group, Group > GroupPair;
//        typedef vector<GroupPair> GroupPairVector;
//        typedef pair<SetOfNodes, SetOfNodes> NodesPair;
//        typedef vector<NodesPair> NodesPairVector;
//
//        /**
//         * Construct object from a discretized tree and a gnee-species map.
//         */
//        GSROrthologyEstimator(  const EdgeDiscTree &DS,
//                                const StrStrMap &gs_map);
//
//        /**
//         * Copy contructor.
//         */
//        GSROrthologyEstimator(const GSROrthologyEstimator& orig);
//
//        /**
//         * Destructor.
//         */
//        virtual ~GSROrthologyEstimator();
//
//        /**
//         * isObligateDuplication
//         *
//         * Determine if a vertex is an obligate duplication.
//         * @param node The vertex.
//         * @param S A species tree
//         * @param gs_map gene-species map.
//         *
//         */
//        static bool isObligateDuplication(Node *node, Tree &S, StrStrMap &gs_map);
//
//        /**
//         * estimateOrthologyFromFile
//         *
//         * Estimate orthology probability by reading states from a file.
//         *
//         * @param filename File to read from.
//         * @param DS Discretized tree used to calculate probabilities from.
//         * @param gs_map gene-species map. Make sure this is consistent with
//         * gene trees in file and in the discretized species tree.
//         * @param g1 Group of genes.
//         * @param g2 Group of genes. Orthology is estimated between these groups.
//         *
//         */
//        static Probability estimateOrthologyFromFile(   const string &filename,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    const vector<string> &g1,
//                                                    const vector<string> &g2);
//        /**
//         * estimateOrthologyFromFile
//         *
//         * Estimate orthology probability by reading states from a file.
//         *
//         * @param filename File to read from.
//         * @param DS Discretized tree used to calculate probabilities from.
//         * @param gs_map gene-species map. Make sure this is consistent with
//         * gene trees in file and in the discretized species tree.
//         * @param g1 pairs. Pair of gene groups. Orthology for each pair is
//         * estimated.
//         *
//         */
//        static vector<Probability> estimateOrthologyFromFile(
//                                                    const string &filename,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    const GroupPairVector &pairs);
//        /**
//         * estimateOrthologyFromMCMC
//         *
//         * Estimate orthology probability between two groups of genes
//         * by performing MCMC sampling to find states.
//         *
//         * @param iterations Number of MCMC iterations.
//         * @param thinning Thinning parameter for MCMC.
//         * @param DS Discretized tree used to calculate probabilities from.
//         * @param gs_map gene-species map. Make sure this is consistent with
//         * gene trees in file and in the discretized species tree.
//         * @param Q Substitution model used.
//         * @param D Sequence data.
//         * @param birth_rate Birth rate.
//         * @param death_rate Death rate.
//         * @param g1 Group of gene names.
//         * @param g2 Group of genes. Orthology is estimated between these groups.
//         * @param diagnostics Progress is written to stdout.
//         * @param burn_in MCMC burn-in parameter.
//         *
//         */
//        static Probability estimateOrthologyFromMCMC(   const int iterations,
//                                                    const int thinning,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    MatrixTransitionHandler &Q,
//                                                    SequenceData &D,
//                                                    Density2P &density,
//                                                    const Real birth_rate,
//                                                    const Real death_rate,
//                                                    const vector<string> &g1,
//                                                    const vector<string> &g2,
//                                                    const bool diagnostics = true,
//                                                    unsigned int burn_in = 0);
//
//        /**
//         * estimateOrthologyFromMCMC
//         *
//         * Estimate orthology probability between several groups of genes
//         * by performing MCMC sampling to find states.
//         *
//         * @param iterations Number of MCMC iterations.
//         * @param thinning Thinning parameter for MCMC.
//         * @param DS Discretized tree used to calculate probabilities from.
//         * @param gs_map gene-species map. Make sure this is consistent with
//         * gene trees in file and in the discretized species tree.
//         * @param Q Substitution model used.
//         * @param D Sequence data.
//         * @param birth_rate Birth rate.
//         * @param death_rate Death rate.
//         * @param g1 pairs Pair of groups of genes to estimate orthology between.
//         * @param diagnostics Progress is written to stdout.
//         * @param burn_in MCMC burn-in parameter.
//         *
//         */
//        static vector<Probability> estimateOrthologyFromMCMC(   const int iterations,
//                                                    const int thinning,
//                                                    EdgeDiscTree &DS,
//                                                    StrStrMap &gs_map,
//                                                    MatrixTransitionHandler &Q,
//                                                    SequenceData &D,
//                                                    Density2P &density,
//                                                    const Real birth_rate,
//                                                    const Real death_rate,
//                                                    const GroupPairVector &pairs,
//                                                    const bool diagnostics = true,
//                                                    unsigned int burn_in = 0);
//
//        /**
//         * calculateStatesByMCMC
//         *
//         * Calculate states by MCMC sampling.
//         */
//        void calculateStatesByMCMC( const int iterations,
//                                    const int thinning,
//                                    MatrixTransitionHandler &Q,
//                                    SequenceData &D,
//                                    Density2P &density,
//                                    const Real start_birth_rate,
//                                    const Real start_death_rate);
//
//        /**
//         * readStatesFromFile
//         *
//         * Create states by reading from file.
//         *
//         */
//        void readStatesFromFile(const string filename,
//                                EdgeDiscTree &DS,
//                                StrStrMap &gs_map);
//
//
//        /**
//         * estimateOrthology
//         *
//         * Estimate the orthology between two groups of genes.
//         *
//         */
//        Probability estimateOrthology(  const vector<string> &g1,
//                                        const vector<string> &g2) const;
//
//        /**
//         * estimateOrthology
//         *
//         * Estimate the orthology between several groups of genes.
//         *
//         */
//        vector<Probability> estimateOrthology( const GroupPairVector &pairs) const;
//
//        /**
//         * estimateOrthologyFromState
//         *
//         * Estimates the orthology probability in one state between two
//         * groups of genes.
//         *
//         */
//        static Probability estimateOrthologyFromState(  EdgeDiscGSR &gsr,
//                                                        const SetOfNodes  &g,
//                                                        const SetOfNodes  &g2);
//        /**
//         * estimateOrthologyFromState
//         *
//         * Estimates the orthology probability in one state between several
//         * groups of genes.
//         *
//         */
//
//        static vector<Probability> estimateOrthologyFromState(  EdgeDiscGSR     &gsr,
//                                                        const NodesPairVector  &pairs);
//
//        /**
//         * Write progress of calculations on stdout.
//         *
//         */
//        void showDiagnostics(bool diag){m_DIAGNOSTICS = diag;}
//
//
//
//    private:
//        //State variables
//        Tree m_S;
//        EdgeDiscTree m_DS;
//        StrStrMap m_gs_map;
//        vector<EdgeDiscGSR*> m_gsr_states;
//        vector<EdgeDiscGSRObjects*> m_gsr_objects;
//        bool m_states_calculated;
//        bool m_DIAGNOSTICS;
//        //Helper functions
//
//        /**
//         * find_lca
//         *
//         * Finds the last common ancestor of a set of nodes in a tree.
//         * Assumes all nodes is found in the same tree, otherwise the result
//         * is undefined.
//         * Note: If there is only one node in the set, then that node itself is
//         * considered the lca.
//         *
//         * @param nodes A set of nodes.
//         *
//         */
//        static Node * find_lca(const SetOfNodes &nodes);
//
//        /**
//         * independent
//         *
//         * Decide if two groups of genes are independent.
//         *
//         */
//        static bool independent(const SetOfNodes &g1, const SetOfNodes &g2);
//
//        /**
//         * get_ortho_prob
//         *
//         * Finds the orthology probability of two sets of genes in a GSR state.
//         *
//         * @param g1 A set of genes.
//         * @param g2 A set of genes.
//         * @param species_leaves A set of species leaves in which the genes are found.
//         * @param gsr A GSR state.
//         */
//        static Probability get_ortho_prob( const SetOfNodes &g1, const SetOfNodes &g2,
//                                    const SetOfNodes &species_leaves,
//                                    EdgeDiscGSR &gsr);
//
//        /**
//         * find_gene_set
//         *
//         * Convert a set of gene names into a SetOfNodes object. The Node
//         * objects are taken from an EdgeDiscGSR object.
//         */
//        static SetOfNodes find_gene_set(EdgeDiscGSR &gsr, const vector<string> &gene_names);
//
//        /**
//         * convert_vector
//         *
//         * Converts a GroupPairVector into a NodesPairVector.
//         *
//         */
//        static NodesPairVector convert_vector(EdgeDiscGSR &gsr, const GroupPairVector &gpv);
//
//        /**
//         * insert_species_leaves
//         *
//         * Add Node objects to a SetOfNodes object. Node object are taken from
//         * a vector of gene names.
//         *
//         */
//        static void insert_species_leaves(SetOfNodes &species_leaves, const Tree &S, const StrStrMap &gs_map, const vector<string> &gn1, const vector<string> &gn2);
//        static void insert_species_leaves(vector<SetOfNodes> &S_vec, const Tree &S, const StrStrMap &gs_map, const GroupPairVector &gpv);
//
//};
//
//};
//#endif	/* _GSRORTHOLOGYESTIMATOR_HH */

