
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cstdlib>

#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "Density2P.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LambdaMap.hh"
#include "LengthRateMCMC.hh"
#include "LogNormDensity.hh"
#include "PRNG.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SimpleMCMC.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


// Global options with default settings
//------------------------------------
int nParams = 5;
bool mcmc = false;

char* outfile=NULL;
double mean = 1.0;
double variance = 1.0;

double birthRate = 1.0;
double deathRate = 1.0;

// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams || argc > nParams+1) 
    {
      cerr << "Wrong number of parameters\n"
	   << "Usage: test_LengthRateMCMC <tree with lengths> <#iters> [<do sampling>]\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string treefile(argv[1]);
      TreeIO io = TreeIO::fromFile(treefile);
//       Tree G = io.readBeepTree(true, false, true, false, 0,0);  
      StrStrMap gs;
      Tree G = io.readGuestTree(NULL, &gs);
      G.setName("G");
      cout << G.print(false, false, true,false);

      treefile = argv[2];
      io.setSourceFile(treefile);
      Tree S = io.readHostTree();
      S.setName("S");
      cout << S;

      if(gs.size() == 0)
	{
	  gs = TreeIO::readGeneSpeciesInfo(argv[3]);
	}

      unsigned N = atoi(argv[4]);

      //Set up MCMC classes
      //---------------------------------------------------------


      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

      // Set up priors
      //---------------------------------------------------------

      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda);
 
      BirthDeathMCMC bdm(dm, S, birthRate, deathRate, false);
      ReconciliationTimeMCMC rtm(bdm, G, bdm, gamma, "EdgeTimes");

      ReconciliationTimeSampler sampler(G, bdm, gamma);
      sampler.sampleTimes(false);

      //Set up mean and variance of substitution rates
      //---------------------------------------------------------

      // Set up Density function for rates
      //---------------------------------------------------------
      GammaDensity df(mean, variance);

      // Set up rates and LengthMCMC
      //---------------------------------------------------------
      LengthRateMCMC sm(rtm, df, G, "EdgeRates");      

      
      if(argc < nParams)
	{      
	  // Create MCMC handler
	  //---------------------------------------------
	  SimpleMCMC iterator(sm, 1);
	  
	  
	  iterator.iterate(N);
	}
      else // sample
	{
	  Probability sum = 0;
	  for(unsigned i = 0; i < N; i++)
	    {
	      sampler.sampleTimes();
	      sum += sm.updateDataProbability();
	      //cerr << sm.calculateDataProbability() << endl;
	    }
	  sum /= N;
	  cout << "sum = " << sum << " (" << sum.val() << ")" << endl;
	}
    }
  catch(AnError e)
    {
      cerr << " error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << " error\n"
	   << e.what();
    }
}

	      

	  
  
