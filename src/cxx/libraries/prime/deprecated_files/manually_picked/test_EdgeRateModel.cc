#include <iostream>
#include <iomanip>

#include "AnError.hh"
#include "ConstRateModel.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "PRNG.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"
#include "VarRateModel.hh"
#include "EdgeWeightHandler.hh"

void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <treefile> "
       << " <rate model> "
       << " <distribution> "
       << " <gene mean> "
       << " <gene variance>\n"
       << "\n"
       << "Parameters:\n"
       << "   <treefile>        file with a tree with >= 4 leaves\n"
       << "   <rate model>      A char: 'C' = molecular clock, 'I' = iid model, 'G' = gbm model\n"
       << "   <distribution>    A char: 'U' = Uniform, 'G' = Gamma, 'L' = LogNormal or 'I' = InverseGauss\n" 
       << "   <gene mean>       A double > 0\n"
       << "   <gene variance>   A double > 0\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 6) 
    {
      usage(argv[0]);
    }

  try
    {
      TreeIO treeIF = TreeIO::fromFile(string(argv[1]));
      Tree T = treeIF.readGuestTree();

      Density2P* df;
      switch(argv[3][0])
	{
	case 'U':
	  df = new UniformDensity(atof(argv[4]), atof(argv[5]),true);
	  break;
	case'G':
	  df = new GammaDensity(atof(argv[4]), atof(argv[5]));
	  break;
	case 'L':
	  df = new LogNormDensity(atof(argv[4]), atof(argv[5]));
	  break;
	case 'I':
	  df = new InvGaussDensity(atof(argv[4]), atof(argv[5]));
	  break;
	default:
	  cerr << argv[3] << " is not a valid density name\n";
	  exit(1);
	}

      cout << "Testing EdgeRateModel et al's constructor\n"
	   << "using sampleValue() from DensityFunctions2P\n"
	   << "-----------------------------------------\n";
      PRNG rand;
      EdgeRateModel* erm;
      switch(argv[2][0])
	{
	case 'C':
	  {
	    erm = new ConstRateModel(*df, T, 
				   df->sampleValue(rand.genrand_real1()));
	    break;
	  }
	case'I':
	  {
	    RealVector rates(T);
	    for(unsigned i = 0; i < T.getNumberOfNodes(); i++)
	      rates[T.getNode(i)] = df->sampleValue(rand.genrand_real1());
	    rates[T.getRootNode()] = df->getMean();
	    erm = new iidRateModel(*df, T, rates);
	    break;
	  }
	case'G':
	  {
	    RealVector rates(T);
	    for(unsigned i = 0; i < T.getNumberOfNodes(); i++)
	      rates[T.getNode(i)] = df->sampleValue(rand.genrand_real1());
	    rates[T.getRootNode()->getLeftChild()] = df->getMean();
	    erm = new gbmRateModel(*df, T, rates);
	    break;
	  }
	default:
	  {
	    cerr << argv[2] << " is not a valid density name\n";
	    exit(1);
	  }
	}

     
      cout << *erm << endl;

      cout << "Testing the getTree() function.\n"
	   << "The Tree edgeRateModel uses is"
	   << (&(erm->getTree()) == &T?" ":" not ")
	   << "the same as the input tree\n\n";

      cout << "Testing the getDensity() function. \n"
	   << "The Density edgeRateModel uses is"
	   << (&(erm->getDensity()) == df?" ":" not ")
	   << "the same as the input density\n\n";

      cout << "Testing EdgeRateModel's setRate(rate, node) and "
	   << "getRate(node) functions\n";
      cout << "setRate(3, 0) (pointer version)\n";
      erm->setRate(3, T.getNode(0));
      cout << "setRate(4, 1) (reference version)\n";
      erm->setRate(4, T.getNode(1));
      
      cout << "Checking with getRate()\n"
	   << "Node\tgetRate(*)\tgetRate(&)\toperator[*]\toperator[&]\n";
      for(unsigned i = 0; i < T.getNumberOfNodes(); i++)
	{
	  Node* n = T.getNode(i);
	  if(!n->isRoot())
	    cout << std::setw(1)
		 << i << "\t" 
		 << std::setw(9)
		 << erm->getRate(n) << "\t" 
		 << std::setw(10)
		 << erm->getRate(*n) << "\t" 
		 << std::setw(11)
		 << erm->operator[](n) << "\t" 
		 << std::setw(11)
		 << erm->operator[](*n) << "\n";
	  else
	    cout << "root rate is not modeled\n";
	}

      cout << "\n"
	   << "Testing calculateDataProbability\n"
	   << "--------------------------------\n";      
      cout << "Joint probability of rates is ln(" 
	   << erm->calculateDataProbability().val()
	   << ") = "
	   << erm->calculateDataProbability() 
	   << "\n\n";
      
      cout << "Testing the setMean and setVariance function\n"
	   << "Setting mean to 3.0";
      erm->setMean(3.0);
      cout << " and variance to 5.0\n\n";
      erm->setVariance(5.0);

      cout << "Testing the getMean and getVariance function\n"
	   << "mean = " << erm->getMean()
	   << ", variance = " << erm->getVariance() << ".\n";

      delete df;
      delete erm;
    }
  catch(AnError e)
    {
      e.action();
    }
  return 0;
}
      
 
