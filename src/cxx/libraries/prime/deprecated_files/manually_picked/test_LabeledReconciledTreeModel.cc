#include "AnError.hh"
#include "BranchSwapping.hh"
#include "ReconciledTreeModel.hh"
#include "ReconciliationSampler.hh"
#include "TreeIO.hh"



int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 6)
    {
      cerr << "usage: test_LabeledReconciledTreeModel <Guesttree> <HostTree> <gs> <birthrate> <deathrate>\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);
//       vector<SetOfNodes> AC;
//       StrStrMap gs;
      Tree G = io.readGuestTree(0,0);
      G.setName("G");
      cout << "G = \n"
	   << G;

      string host(argv[2]);
      cerr << "host =  " << host << endl;
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  // Reads times?
      S.setName("S");
      cout << "S = \n" << S;

      StrStrMap gs;
      gs = TreeIO::readGeneSpeciesInfo(argv[3]);
      cout << gs << endl;
//       unsigned j = 0;
//       for(vector<SetOfNodes>::const_iterator i = AC.begin(); i != AC.end(); i++, j++)
// 	{
// 	  if(i->size() != 0)
// 	    cout << j << i->set4os() << endl;
// 	}

      Real brate = atof(argv[4]);
      Real drate = atof(argv[5]);

      BirthDeathProbs bdp(S, brate, drate);


      cout << "Testing constructors:"
	   << "-------------------------------------------------\n"
	   << "First construct LabeledReconciledTreeModel a with Ggamma\n"
	   << "the tree and reconciliation.\n";
      LabeledReconciledTreeModel a(G, gs, bdp);
      LambdaMap lambda(G,S,gs);
      GammaMap gamma_star = GammaMap::MostParsimonious(G,S,lambda);
      a.setGamma(gamma_star);
//       LabeledReconciledTreeModel a(G, gs, bdp, AC);
//       cout << indentString(a.print());
//       cout << "Then construct LabeledReconciledTreeModel b with S as the\n"
// 	   << "tree without reconciliation.\n";
//       StrStrMap gs2;
//       for(unsigned i = 0; i < S.getNumberOfNodes(); i++)
// 	{
// 	  Node& n = *S.getNode(i);
// 	  if(n.isLeaf())
// 	    {
// 	      gs2.insert(n.getName(), n.getName());
// 	    }
// 	}
//       LabeledReconciledTreeModel b(S, gs2, bdp);
//       cout << indentString(b.print())
// 	   << "Then use copy constructor to create c from b\n";
//       LabeledReconciledTreeModel c(b);
//       cout << indentString(c.print())
// 	   << "\nFinally use operator=, to set c = a:\n";
//       try
// 	{
// 	  c = a;
// 	  cout << indentString(c.print());
// 	}
//       catch(AnError e)
// 	{
// 	  cout << "This failed,a s expected, with error message:\n"
// 	       << e.what()
// 	       << "\nWe really do need to fix the assignment operator"
// 	       << " of GammaMap\n"
// 	       << endl << endl;
// 	}
      cout << "Now test some probs\n"
// 	   << "a:\n"
// 	   << a
	   << "Probability of (G, gamma) is " 
	   << a.calculateDataProbability()
	   << "\n\n";

//       ReconciliationSampler sampler(G, gs, bdp);
//       Probability p = sampler.calculateDataProbability();
//       cerr << "Probability of G = " << p
// 	   << endl << endl;

//       std::pair<GammaMap, Probability> gp =sampler.sampleReconciliationAndProb();
//       for(unsigned i = 0; i < 10; i++)
// 	{
// 	  gp = sampler.sampleReconciliationAndProb();
// 	  if(io.writeGuestTree(G, &a.getGamma()) != io.writeGuestTree(G, &gp.first))
// 	    {
// 	      cout << endl 
// 		   << endl
// 		   << i
// 		   << "\nNow use ReconciliationSampler to sample a \n"
// 		   << "reconciliation that has probability "
// 		   <<  gp.second
// 		   << ".\nSampled reconciliation is: \n"
// 		   << gp.first.print(true)
// 		   << endl;
// 	      a.setGamma(gp.first);
// 	      cout << "\na.calculateDataProbability() gives "
// 		   << a.calculateDataProbability()/p
// 		   << "  ("
// 		   << a.calculateDataProbability()
// 		   << ")\n"
// 		   << "\n";
// 	    }
// 	}
//       cout << "To test update(), reroot G:\n";
//       BranchSwapping swap;
//       swap.reRoot(G);
//       //     swap.NNI(G);

      // This fails, which imply that we should throw AnError when tree is incorrect
//       try
// 	{
	  
// 	  cout << "This gives a probability of G " 
// 	       << a.calculateDataProbability() 
// 	       <<"\n";
// 	}
//       catch(AnError e)
// 	{
// 	  cout << "Without update, a.calculateDataProbability() fails, "
// 	       << "with error mesage:\n"
// 	       << e.what()
// 	       << endl;
// 	}
      
//        a.update();
//        cout << S << endl << G << endl << a.getGamma();

//       cout << "After a.update() a gives Probability of S is " 
// 	   << a.calculateDataProbability()
// 	   << "\n";
	  
    }
  catch(AnError e)
    {
      cerr << "Error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what();
    }
  return(0);
};
