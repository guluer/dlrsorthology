#include <iostream>

#include <sys/types.h>
#include <unistd.h>

#include "AnError.hh"
#include "BirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "EdgeRateMCMC_common.hh"
#include "EdgeWeightHandler.hh"
#include "TreeMCMC.hh"
#include "Hacks.hh"
#include "PRNG.hh"
#include "ReconSeqApproximator.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "SimpleMCMC.hh"
#include "TopTimeMCMC.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "RandomTreeGenerator.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"
#include "GammaDensity.hh"
#include "LogNormDensity.hh"
#include "InvGaussDensity.hh"

using namespace std;
using namespace beep;

// Global options
//-------------------------------------------------------------
int nParams = 2;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 10;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

unsigned nGammaTimeSamples = 1000;

// Substitution model related
std::string seqModel = "jc69";
std::string seqType;    // this and the following two variables are used in
std::vector<double> Pi; // user-defined substitution models
std::vector<double> R;
unsigned n_cats = 1;
double alpha = 1.0;
bool fixedAlpha = false;
// double pinvar = 0.0;

// EdgeRate model related
std::string rateModel = "Const";
std::string density = "Uniform";
std::map<unsigned, beep::Real> start_rates;
double mean = 1.0;
double variance = 1.0;
bool fixed_rateparams = false;
bool choose_rateparams = true;
double maxP = -1.0;

// Birth-death process related
std::map<unsigned, beep::Real> start_times;
bool fixed_bdrates = false;
double birthRate = 1.0;
double deathRate = 1.0;
double topTime = -1.0;
double Beta = -1.0;

bool MustChooseRates = true;

// tree related
char* tree_file = 0; //NULL
bool fixed_tree = false;
bool fixed_times = false;

// reconciliation related
bool outputGamma = false;
bool estimate_orthology = false;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  // Set up allowed models
  //! \todo If we have a factory, this won't be needed/bens
  //-------------------------------------------------------------
  //rate model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("ARVECODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("iid");
  rate_models.push_back("gbm");
  rate_models.push_back("const");
  
  //rate model
  rate_densities.push_back("Uniform");
  rate_densities.push_back("Gamma");
  rate_densities.push_back("LogN");
  rate_densities.push_back("InvG");

 
  if (argc < nParams) 
    {
      usage(argv[0]);
      exit(1);
    }

  try 
    {
      // tell the user we've started
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------
      int opt = readOptions(argc, argv);


      //Get input - tree and Data
      //---------------------------------------------
      if(opt + 2 > argc)
	{
	  cerr << "Arguments missing\n";
	  exit(2);
	}
      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile);

      string S_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(S_filename);
      Tree S = io.readHostTree();

      StrStrMap gs;
      Tree* G;
      if(tree_file)
	{
	  io.setSourceFile(tree_file);
// 	  G = new Tree(io.readGuestTree(NULL, &gs)); // no times or lengths read
	  // The new readBeepTree functions does not yet support providing
	  // gs when gs-info might not exist!
	  G = new Tree(io.readGuestTree(0, 0)); // no times or lengths read
	}
      else
	{
	  G = new 
	    Tree(RandomTreeGenerator::generateRandomTree(D.getAllSequenceNames()));
	}
      
      // Check for sanity of input and options:
      //----------------------------------------------------

      // We must not have a zero root time! We have to pretend we know
      // the time from the parent to the previous ancestor.
      //! \todo{ This actually represent a rather abstract variable 
      //! representing a time preceeding than the first branching point in 
      //! the guest tree. It is definitely not related to the host tree 
      //! (except that it must precede - or equal  - its root). Thus the 
      //! class should be renamed and the variable should not be stored in 
      //! S's root time. /bens}
      Node *sr = S.getRootNode();
      if (topTime <= 0.0) 
	{
	  if (sr->getTime() <= 0.0) 
	    {
	      Real height = S.rootToLeafTime();
	      sr->setTime(height / 10.0);
	    }
	}
      else
	{
	  sr->setTime(topTime);
	}


      // If gs is empty (no mapping info was found in the gene tree
      // or a random tree was created), 
      // we have to read a file with that info.
      if (gs.size() == 0  && opt < argc)
	{
	  gs = TreeIO::readGeneSpeciesInfo(string(argv[opt++]));
	}
      else
	{
	  cerr << "No mapping gene-name -> species-name found in the gene \n"
	       << "tree ("
	       << tree_file
	       << ") and no file with that mapping given.\n";
	  exit(2);
	}

      

      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------

       //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}
      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

      // Set up priors
      //-------------------------------------------------------
      if(Beta < 0)
	{
	  Beta = std::max(S.getRootNode()->getTime(), S.rootToLeafTime());
	}
      TopTimeMCMC stm(dm, S, Beta); //!< \todo{ change name of class \bens}
      BirthDeathMCMC bdm(stm, S, birthRate, deathRate, &stm.getTopTime());
      if(fixed_bdrates)
	{
	  bdm.fixRates();
	}

      // Set up mean and variance of substitution rates
      //---------------------------------------------------------
      if(choose_rateparams)
	{
 	  mean = 0.5/(S.rootToLeafTime() + S.getRootNode()->getTime()) 
	    * rand.genrand_real3();
 	  variance = mean * rand.genrand_real1()/10.0; 
	}

      if(maxP <= 0) // Check if user defined max exists <=> max >0
	{
	  // average branchlength should never be more than 5.0
	  
	  maxP = 5.0 / (S.rootToLeafTime() + S.getRootNode()->getTime());
	}

      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{ 
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else if(density == "UNIFORM")
	{
// 	  df = new UniformDensity(0, 5.0 / S.rootToLeafTime(), true);
	  df = new UniformDensity(0, 5.0, true);
	  cerr << "*******************************************************\n"
	       << "Note! mean and variance will always be fixed when using\n"
	       << "UniformDensity, the default interval will be (0,3)\n"
	       << "You might want to use the -Ef option\n"
	       << "*******************************************************\n";
	  fixed_rateparams = true;
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
	       << "for option -d\n";
	  usage(argv[0]);
	  exit(1);
	}

      // Set up rates 
      //---------------------------------------------------------
      EdgeRateMCMC* erm;
      if(rateModel == "gbm")
	{
	  erm = new gbmRateMCMC(bdm, *df, *G, "EdgeRates");
	}
      else if(rateModel == "iid")
	{
	  erm = new iidRateMCMC(bdm, *df, *G, "EdgeRates");
	}
      else
	{	 
	  erm = new ConstRateMCMC(bdm, *df, *G, "EdgeRates");
	  cerr << "Note!: When using a 'const' rate model, we really can't "
	       << "estimate the mean and variance of the underlying density. "
	       << "So it's recommended to use the -e option in this case. The "
	       << "defaults settings are thus a little stupid and should be "
	       << "changed\n";
	  erm->setRate(0.5/S.rootToLeafTime(), 0);
	}
      
      // Set any user defined rates
      if(start_rates.empty() == false)
	{
	  if(rateModel == "const")
	    {
	      if(start_rates.size() > 1)
		{
		  cerr << "\nWarning: Only the first rate, "
		       << start_rates.begin()->second
		       << " will be used, since rate\n is constant "
		       << "over tree. Remaining rates given are ignored\n\n";
		}
	      erm->setRate(start_rates.begin()->second, 0);
	    }
	  else
	    {
	      Node* n = 0; 
	      Node* rc = 0;
	      for(map<unsigned, Real>::const_iterator i = start_rates.begin();
		  i != start_rates.end(); i++)
		{
		  if(i->first >= G->getNumberOfNodes())
		    {
		      cerr << "\nError: Trying to set rate for vertex "
			   << i->first
			   << ", which do not exist in tree\n\n";
		      exit(23);
		    }
		  n = G->getNode(i->first);
		  if(n->isRoot())
		    {
		      cerr << "\nWarning: The rate submitted for root's "
			   << "incoming edge will be ignored!\n\n";
		    }
		  else
		    {
		      if(n->getParent()->isRoot())
			{
			  if(rc)
			    {
			      cerr << "\nWarning: The rate given for root's "
				   <<"child node "
				   << n->getNumber()
				   << "\nwill overwrite the rate of root's "
				   << "child node"
				   << rc->getNumber() << "!\n\n";
			    }
			  else
			    {
			      rc = n;
			    }
			}
		      erm->setRate(i->second, n);
		    }
		}
	    }
	}
      else
	{
	  erm->generateRates();  // Get random start values for rates 
	}

      if(fixed_rateparams)
	{
	  erm->fixMean();
	  erm->fixVariance();
	}


      // Set up Reconciliation handlers, rs is used both in 
      // GuestTreeMCMC and ReconSeqApproximator
      //---------------------------------------------------------
      ReconciliationSampler rs(*G, gs, bdm);
      if (MustChooseRates) 
	{
	  rs.chooseStartingRates(birthRate, deathRate);
	  cerr << "Birth- and Death rates are set to: "
	       << birthRate 
	       << " and "
	       << deathRate
	       << ", respectively.\n";
	}


      // MCMC class for tree perturbations
      //---------------------------------------------------------
      GuestTreeMCMC gtm(*erm, rs);

      // Start building the Monte Carlo approximat
      //---------------------------------------------------------
      EdgeTimeRateHandler ewh(*erm);

      UniformDensity uf(0, 3.0, true); // U[0,3]
      ConstRateMCMC alphaC(gtm, uf, *G, "Alpha"); 
      if(n_cats == 1 || fixedAlpha)
	{
	  alphaC.fixRates();
	  alphaC.setRate(alpha, 0);
	}
      SiteRateHandler srm(n_cats, alphaC);

      // Set up the SubstitutionMatrix
      //---------------------------------------------------------
      MatrixTransitionHandler* Q;
      if(seqModel == "USR")
	{
	  Q = new 
	    MatrixTransitionHandler(MatrixTransitionHandler::
				    userDefined(seqType, Pi, R)); 
	}
      else
	{
	  Q = new 
	    MatrixTransitionHandler(MatrixTransitionHandler::create(seqModel));
	}
      vector<string> partitionList ; 
      partitionList.push_back(string("all")); // We don't handlepartitions yet

      SubstitutionMCMC sm(dm, D, *G, srm, *Q, ewh, partitionList);

      ReconciliationTimeSampler rts(rs); // using info given in rs

      //! \todo{This looks like  weird practice\bens}
      unsigned nGammaSamples = nGammaTimeSamples;
      unsigned nTimeSamples = nGammaTimeSamples;  
#ifdef ONLY_ONE_TIMESAMPLE
      SeqProbApproximator spa(dm, rts, sm, 1, true);
#else
      SeqProbApproximator spa(dm, rts, sm, nTimeSamples, true);
#endif
      ReconSeqApproximator rsa(alphaC, spa, rs, nGammaSamples);
      if(outputGamma)
	{
	  rsa.outputReconciliations(true);
	}


      // Optional debug info
      //---------------------------------------------
      if (show_debug_info)
	{
	  cerr << G;
	  cerr << endl;
	  
	  cerr << S;
	  cerr << endl;
	}
      
      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(rsa, Thinning);

      if (do_likelihood)
	{
	  cout << gtm.currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  

      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
	}


      // Copy startup info to outfile (can be used to restart analysis)
      cout << "# Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}
      cout << " in directory"
	   << getenv("PWD")
	   << "\n";
      
      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------

      time_t t0 = time(0);
      clock_t ct0 = clock();


      iterator.iterate(MaxIter, printFactor);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << readableTime(t1- t0) 
	   << endl
	   << "CPU time: " << readableTime((ct1 - ct0)/CLOCKS_PER_SEC)
	   << endl;
      
      if (!quiet)
	{
	  cerr << gtm.getAcceptanceRatio()
	       << " = acceptance ratio   Wall time = "
	       << readableTime(t1-t0)
	       << "\n";
	}
      
      if (gtm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }
      delete G;
      delete df;
      delete erm;
      delete Q;

    }
  catch (AnError e) 
    {
      e.action();
    }
  catch (exception e) 
    {
      cerr << "Error: "
	   << e.what();
    }
}

void 
usage(char* cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <datafile> <species tree>"
    << " [<gene-species map<]\n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         Name of file containing sequences in FastA or\n"
    << "                      Genbank format.\n"
    << "   <species tree>     Species tree in Newick format. Branchlengths\n"
    << "                      are important and represent time.\n"
    << "   <gene-species map> Currently not Optional. This file contains lines with a gene\n"
    << "                      name in the first column and species name\n"
    << "                      as found in the species tree in the second.\n"
    << "                      You can also choose to associate the genes \n"
    << "                      with species in the gene tree. Please see\n"
    << "                      documentation.\n"


    << "Options:\n"
    << "   -u, -h             This text.\n"
    << "   -o <filename>      output file\n"
    << "   -i <float>         number of iterations\n"
    << "   -t <float>         thinning\n"  
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -p <int>           Precision: The number of iterations to \n"
    << "                      approximate the likelihood of reconciliation \n"
    << "                      and sequences. (" << nGammaTimeSamples << ")\n"
    << "   -s <int>           Seed for pseudo-random number generator. If \n"
    << "                      set to 0 (default), the process id is used as\n"
    << "                      seed.\n"
    << "   -q                 Do not output diagnostics to stderr.\n"
    << "   -g                 Debug info.\n"
    << "   -l                 Output likelihood. No MCMC.\n"
    << "   -S<option>            Options related to Substitution model\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
    << "                         the substitution model to use, (JC69 is the\n"
    << "                         default). Must fit data type \n"
    << "     -Su <datatype='DNA'/'AminoAcid'/'Codon'> <Pi=float1 float2 ... floatn>\n"
    << "                         <R=float1 float2 ...float(n*(n-1)/2)>\n"
    << "                         the user-defined substitution model to use.\n"
    << "                         The size of pi and R must fit data type (DNA: n=4\n"
    << "                         AminoAcid: n=20, Codon: n = 62), respectively. \n"
    << "                         If both -Su and -Sm is given (don't do this!),\n"
    << "                         only the last given is used\n"
    << "     -Sn <float>         nCats, the number of discrete rate\n"
    << "                         categories (default is one category)\n"
    << "     -Sa <float>         alpha, the shape parameter of the Gamma\n"
    << "                         distribution for site rates (default: 1)\n"
    //     << "     -p <float>         probability of a site being invarant\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ep <float> <float> start mean and variance of edge rate model\n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "     -Er { <node#=edgerate> <node#=edgerate> ... }\n"
    << "                         User defined edge rates for tree in tree\n"
    << "                         file. Root's rate is ignored. If rates are \n"
    << "                         given for both root'children, one is ignored.\n" 
    << "                         (note spaces after and before '}'\n"
    << "     -El <float>         User-defined (fixed) max value of rate params\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gg                 fix gene tree\n"
    << "     -Gf                 fix edge times. If not set, a birth-death\n"
    << "                         model is used as a prior of the edge rates\n"
    << "     -Gi <treefile>      use tree in file <treefile> as start tree\n"
    << "     -Gt { <node#=nodetime> <vertex#=nodetime> ... }\n"
    << "                         User defined nodetimes for tree in treefile\n"
    << "                         Times for all internal vertices except root/n"
    << "                         need to be set to avoid inconsistencies\n"
    << "                         (note spaces after and before '}'\n"
    << "   -R<option>            Options related to the gene tree\n"
    << "     -Rx                 Estimate reconciliation probabilities.\n"
//     << "     -Ro                 Estimate orthology. Default is discard \n"
//     << "                         orthology data.\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth and death rates of the birth-\n"
    << "                         death model to these values \n"
    << "     -Bp <float> <float> start value of the birth and death rate parameters\n"
    << "     -Bt <float>         Start value for 'top time', the time between first\n"
    << "                         duplication and root of S.\n"
    << "     -Bb <float>         The beta parameter for a prior distribution on\n"
    << "                         species root distance. (S.rootToLeaftime())\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  usage(argv[0]);
	  break;

	case 'o':
	  if (opt + 1 < argc)
	    {
	      outfile = argv[++opt];
	    }
	  else
	    {
	      cerr << "Expected filename after option '-o'\n";
	      usage(argv[0]);
	    }
	  break;
	case 'i':
	  if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	    {
	      cerr << "Expected integer after option '-i'\n";
	      usage(argv[0]);
	    }
	  break;

	case 't':
	  if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	    {
	      cerr << "Expected integer after option '-t'\n";
	      usage(argv[0]);
	    }
	  break;

	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-w'\n";
		usage(argv[0]);
	      }
	    break;
	  }

	case 'p':
	  if (sscanf(argv[++opt], "%d", &nGammaTimeSamples) == 0)
	    {
	      cerr << "Expected integer after option '-p'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  quiet = true;
	  break;
	
	case 'D':
	  show_debug_info = true;
	  break;
	  
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'S':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'u':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = "USR";
		      seqType = argv[++opt];
		      capitalize(seqType);
		      int dim = 0;
		      if(seqType == "DNA")
			dim = 4;
		      else if(seqType == "AMINOACID")
			dim = 20;
		      else if(seqType == "CODON")
			dim = 61;
		      else
			{
			  cerr << seqType
			       << " is not a valid data type for option -Su\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		      if(opt + (dim * (dim + 1) / 2) < argc)
			{
			  for(int i = 0; i < dim; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for Pi: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}
				    
			      Pi.push_back(atof(argv[opt]));
			    }
			  for(int i= 0; i < dim * (dim - 1) /2; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for R: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}

			      R.push_back(atof(argv[opt]));
			    }
			}
		      else
			{
			  cerr << "Too few parameters to -Su " 
			       << seqType
			       << "\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		      std::cerr << "Expected int (number of site rate\n"
				<< "classes) for option 'n'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		      fixedAlpha = true;
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-a'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	  // 	    case 'p':
	  // 	      	{
	  // 		  if (++opt < argc) 
	  // 		    {
	  // 		      pinv = atof(argv[opt]);
	  // 		    }
	  // 		  else
	  // 		    {
	  // 		      cerr << "Expected float (probability of a site being\n"
	  // 			   << "invariant) for option '-p'!\n";
	  // 		      usage(argv[0]);
	  // 		    }
	  // 		  break;
	  // 		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }	
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		      fixed_rateparams = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
		case 'r':
		  {
		    if (string(argv[++opt])  == "{")
		      {
			string s = argv[++opt];
			while(s != "}")
			  {
			    unsigned key = atoi(s.substr(0, s.find("=")).c_str());
			    Real data = atof(s.substr(s.find("=")+1).c_str());
			    start_rates[key] = data;
			    s = argv[++opt];
			  }
			if(start_rates.empty())
			  {
			    cerr << "Set of edge rates expected after "
				 << "option -Gu\n";
			    usage(argv[0]);
			    exit(1);
			  }
		      }
		    else
		      {
			cerr << "Expected edge rates as strings after -Er\n";
			usage(argv[0]);
			exit(1); //Check what error code should be used!
		      }
		    break;
		  }
	      case 'l':
		{
		  if (++opt < argc) 
		    {
		      maxP = atof(argv[opt]);
		      if(maxP <= 0)
			{
			  cerr << "Warning: the value given for -El was\n"
			       << "<= 0, and will be ignored\n";
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch(argv[opt][2])
	      {
	      case 'g':
		{
		  fixed_tree = true;
		  break;
		}
	      case 'f':
		{
		  fixed_times = true;
		  fixed_bdrates = true;
		  break;
		}
	      case 'i':
		{
		  if (opt + 1 < argc)
		    {		  
		      tree_file = argv[++opt];
		      break;
		    }
		  else
		    {
		      cerr << "Expected file name (string) after -Gi\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		}
	      case 't':
		{
		  if (string(argv[++opt])  == "{")
		    {
		      string s = argv[++opt];
		      while(s != "}")
			{
			  unsigned key = atoi(s.substr(0, s.find("=")).c_str());
			  Real data = atof(s.substr(s.find("=")+1).c_str());
			  start_times[key] = data;
			  s = argv[++opt];
			}
		      if(start_times.empty())
			{
			  cerr << "Set of edge times expected after "
			       << "option -Gt\n";
			  usage(argv[0]);
			  exit(1);
			}
		    }
		  else
		    {
		      cerr << "Expected edge times as strings after -Gt\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
			 << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'R':
	  {
	    switch(argv[opt][2])
	      {
	      case 'x':
		{
		  outputGamma = true;
		    break;
		}
	      case 'o':
		{
		  estimate_orthology = true;
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_bdrates = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  MustChooseRates = false;
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 't':
		{
		  if (++opt < argc)
		    {
		      topTime = atof(argv[opt]);
		    }
		  else
		    {
		      cerr << "Error: Expected a 'top time'!\n";
		      usage(argv[0]);
		    }
		  break;
		  }
	      case 'b':
		{
		  if (++opt < argc) 
		    {
		      if (sscanf(argv[opt], "%lf", &Beta) == 0)
			{
			  cerr << "Expected number after option '-b'\n";
			  usage(argv[0]);
			}
		    }
		  break;
		}
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  

	

