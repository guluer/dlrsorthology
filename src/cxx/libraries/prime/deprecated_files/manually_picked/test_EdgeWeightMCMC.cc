#include <iostream>

#include "AnError.hh"
#include "DummyMCMC.hh"
#include "EdgeWeightMCMC.hH"
#include "GammaDensity.hh"
#include "TreeIO.hh"
#include "VarRateModel.hh"
void
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <treefile> <# samples>"
       << "\n"
    ;
  exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 3) 
    {
      usage(argv[0]);
    }

  try
    {
      Real mean = 0.5;
      Real variance = 0.1;

      TreeIO io = TreeIO::fromFile(argv[1]);
      Tree T = io.readGuestTree();

      unsigned nSamples = atoi(argv[2]);

      DummyMCMC dm;

      GammaDensity gd(mean, variance);
      iidRateModel irm(gd, T);
      EdgeWeightMCMC ewm(dm,irm);
      ewm.generateWeights();
      cout << ewm;
      
      T.setLengths(irm.getWeightVector());
      cout << T.print(false,false,true,false);

      Probability p = 0;
      PRNG R;
      cout << "# L\tN\t" << ewm.strHeader() << endl;
      for(unsigned i = 0; i < nSamples; i++)
	{
	  MCMCObject m = ewm.suggestNewState();
	  if(m.propRatio * m.stateProb > p * R.genrand_real1())
	    {
	      p = m.stateProb;
	      ewm.commitNewState();
	    }
	  else
	    {
	      ewm.discardNewState();
	    }
	  cout << p << "\t" << i << "\t" << ewm.strRepresentation()<< endl;
	}

      cout << T.print(false,false,true,false);
    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action();
    }
}
