
#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cstdlib>


#include "AnError.hh"
#include "Beep.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeRateMCMC_common.hh"
#include "Density2P.hh"
#include "DummyMCMC.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "SimpleMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


// Global options with default settings
//------------------------------------
int nParams = 1;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 100;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;


std::string rateModel = "Const";
std::string density = "Uniform";
double mean = 1.0;
double variance = 1.0;
bool fixed_rateparams = false;
bool choose_rateparams = true;

bool fixed_times = false;
bool fixed_rates = false;
double birthRate = 1.0;
double deathRate = 1.0;

// allowed models
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //! \todo If we have a factory, this won't be needed/bens
  //-------------------------------------------------------------
  //rate model
  rate_models.push_back("iid");
  rate_models.push_back("gbm");
  rate_models.push_back("const");
  
  //rate model
  rate_densities.push_back("Uniform");
  rate_densities.push_back("Gamma");
  rate_densities.push_back("LogN");
  rate_densities.push_back("InvG");

  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      // tell the user we've started
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      int opt = readOptions(argc, argv);
      if(opt + 1 > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	  exit(1);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string treefile(argv[opt++]);
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G = io.readHostTree();  // Reads times?
      G.setName("G");
      cerr << "intree:\n"
	   << G;



      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}

      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

 
      //Set up mean and variance of substitution rates
      //---------------------------------------------------------
      if(choose_rateparams)
	{
 	  mean = 0.5;//G.rootToLeafTime();
 	  variance = mean * rand.genrand_real1()/10; 
	}


      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{ 
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else if(density == "UNIFORM")
	{
	  df = new UniformDensity(0, 10, true);
	  cerr << "*******************************************************\n"
	       << "Note! mean and variance will always be fixed when using\n"
	       << "UniformDensity, the default interval will be (0,10)\n"
	       << "You might want to use the -Ef option\n"
	       << "*******************************************************\n";
	  fixed_rateparams = true;
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
	       << "for option -d\n";
	  usage(argv[0]);
	  exit(1);
	}


      EdgeRateMCMC* erm;
      if(rateModel == "gbm")
	{
	  erm = new gbmRateMCMC(dm, *df, G, "EdgeRates");
	}
      else if(rateModel == "iid")
	{
	  erm = new iidRateMCMC(dm, *df, G, "EdgeRates");
	}
      else
	{	 
	  erm = new ConstRateMCMC(dm, *df, G, "EdgeRates");
	  cerr << "Note!: When using a 'const' rate model, we really can't "
	       << "estimate the mean and variance of the underlying density. "
	       << "So it's recommended to use the -e option in this case. The "
	       << "defaults settings are thus a little stupid and should be "
	       << "changed\n";
	}
      erm->generateRates();  // Get random start values for rates 

      if(fixed_rateparams)
	{
	  erm->fixMean();
	  erm->fixVariance();
	}


      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(*erm, Thinning);

      if (do_likelihood)
	{
	  cout << erm->currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  


      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cout << "#Running: ";
	  for(int i = 0; i < argc; i++)
	    {
	      cout << argv[i] << " ";
	    }
	  cout << " in directory"
	       << getenv("PWD")
	       << "\n#\n";
	  cout << "#Start MCMC (Seed = " << RSeed << ")\n";
	}


      // Perform and time the Likelihood calculation
      time_t t0 = time(0);
      clock_t ct0 = clock();
      
      iterator.iterate(MaxIter, printFactor);

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

      if (!quiet)
	{
	  cerr << erm->getAcceptanceRatio()
	       << " = acceptance ratio\n";
	}
      
      if (erm->getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         a string\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -p <float>            Output to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "     -Ep <float> <float> start mean and variance of edge rate model\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'p':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			  fixed_rateparams = true;
			  choose_rateparams = false;
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			  choose_rateparams = false;
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  
