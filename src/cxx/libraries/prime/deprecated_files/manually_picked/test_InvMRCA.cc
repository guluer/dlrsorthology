#include "AnError.hh"
#include "InvMRCA.hh"
#include "TreeIO.hh"



int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  if(argc != 2)
    {
      cerr << "usage: test_GuestTreeModel <Tree>\n";
      exit(1);
    }
  try
    {
      //Get tree and Data
      //---------------------------------------------
      string guest(argv[1]);
      TreeIO io = TreeIO::fromFile(guest);

      Tree T = io.readGuestTree(0, 0); 
      T.setName("G");

      cout << T;

      InvMRCA im(T);
      
      for(unsigned  i = 0; i < T.getNumberOfNodes(); i++)
	{
	  cout << im.getStrRep(T.getNode(i), i) << endl;
	}

    }      
  catch(AnError e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }


  return(0);
};
