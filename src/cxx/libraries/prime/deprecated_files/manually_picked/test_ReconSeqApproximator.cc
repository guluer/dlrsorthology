#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include "BirthDeathProbs.hh"
#include "ConstRateMCMC.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "PRNG.hh"
#include "ReconciliationTimeSampler.hh"
#include "ReconSeqApproximator.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SequenceModel.hh"
#include "StrStrMap.hh"
#include "SubstitutionMatrix.hh"
#include "SubstitutionMCMC.hh"
#include "SubstitutionModel.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <infileprefix> birth_rate death_rate n_samples model\n"
       << "\n"
       << "Parameters:\n"
       << "   <infileprefix>     a string, data file is <prefix>.fasta, \n"
       << "                      tree file is <prefix.gene>\n"
       << "   <birth_rate>       a double\n"
       << "   <death_rate>       a double\n"
       << "   <n-samples>        an unsigned, gives the number of Monte\n" 
       << "                      Carlo iterations to perform when esti-\n"
       << "                      mating likelihood of gene tree\n"
       << "   <model>            a string, JC69, UniformAA, JTT, \n"
       << "                      UniformCodon, ,ust ,atch datatype\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 6) 
    {
      usage(argv[0]);
    }

  try
    {
      SequenceModel* pmodel;
      string arg(argv[5]);
      if(arg == string("UniformAA"))
	{
	  pmodel =  new UniformAA();
	}
      else if(arg == ("JTT"))
	{
	  pmodel =  new JTT();
	}
      else if(arg == ("UniformCodon"))
	{
	  pmodel =  new UniformCodon();
	}
      else
	{
	  pmodel =  new JC69();
	}	  
      //      cout << *pmodel;

      double birthRate = atof(argv[2]);
      double deathRate = atof(argv[3]);
      unsigned n_samples = atoi(argv[4]);

      //Get tree and Data
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(string(argv[1])+".gene");
      Tree G = io.readSpeciesTree();

      io.setSourceFile(string(argv[1])+".species");
      Tree S = io.readSpeciesTree();

      StrStrMap gs = TreeIO::readGeneSpeciesInfo(string(argv[1])+".gs");
      SequenceData D = SeqIO::readSequences(string(argv[1])+".fasta");
      unsigned ntax = G.getNumberOfLeaves();
      unsigned nchar = D.getNumberOfPositions();

      //Create Q
      //---------------------------------------------
      beep::LA_Vector R = pmodel->R();
      beep::LA_DiagonalMatrix Pi = pmodel->Pi();
      beep::SubstitutionMatrix Q(R, Pi);
      //      cout << Q;

      //Create SubstitutionMCMC
      //---------------------------------------------
      UniformDensity gd(1.0, 0);
      PRNG rand;
      EdgeRateModel erm(gd, 1.0);
      DummyMCMC dm(rand);
      ConstRateMCMC RM(gd, 1.0, 1.0, erm, dm, 0.5 , true);
      vector<string> partitionList ;
      partitionList.push_back(string("all"));
      SubstitutionMCMC sm(G, D, RM, Q, partitionList, RM);
      
      cout << "\nTest av ReconSeqApproximator\n" 
	   << "-----------------------------\n";
      BirthDeathProbs bdp(S, birthRate, deathRate);
      ReconciliationTimeSampler rts(G, gs, bdp, rand);
      SeqProbApproximator spa(dm, rts, sm, 1);
      ReconSeqApproximator rsa(dm, spa, rts, n_samples);

      cout << rsa;

      // Perform and time the Likelihood calcualtion
      time_t t0 = time(0);
      clock_t ct0 = clock();

      Probability p = rsa.suggestNewState().stateProb;
      //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cout << "joint prob of tree is : "
	   << p
	   << endl
	   << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << double(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

