#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cstdlib>

#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeRateMCMC_common.hh"
#include "Density2P.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LambdaMap.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PRNG.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SimpleMCMC.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"


// Global options with default settings
//------------------------------------
int nParams = 2;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 100;
unsigned RSeed = 0;
unsigned samples = 1000;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

std::string seqModel = "jc69";
unsigned n_cats = 1;
double alpha = 1.0;
bool estimateAlpha = true;
// double pinvar = 0.0;

std::string rateModel = "Const";
std::string density = "Uniform";
double mean = 1.0;
double variance = 1.0;
bool fixed_rateparams = false;

bool fixed_times = false;
bool fixed_rates = false;
std::vector<double> substrates;
bool fixed_alpha = false;
double birthRate = 1.0;
double deathRate = 1.0;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //! \todo If we have a factory, this won't be needed/bens
  //-------------------------------------------------------------
  //rate model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("iid");
  rate_models.push_back("gbm");
  rate_models.push_back("const");
  
  //rate model
  rate_densities.push_back("Uniform");
  rate_densities.push_back("Gamma");
  rate_densities.push_back("LogN");
  rate_densities.push_back("InvG");

  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      // tell the user we've started
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      int opt = readOptions(argc, argv);
      
      //Get tree and Data
      //---------------------------------------------
      string treefile(argv[opt++]);
      TreeIO io = TreeIO::fromFile(treefile);
      Tree G = io.readSpeciesTree();  // Reads times?
      G.setName("G");
      cerr << G;

      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile);
      //! \todo Maybe a name for SequenceData as well?


      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}

      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

      // Set up priors
      //---------------------------------------------------------
      Tree S = Tree::EmptyTree();
      S.setName("S");
      //set up a gamma
      StrStrMap gs;
      // Map all leaves of G to the single leaf in S
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  gs.insert(G.getNode(i)->getName(), S.getRootNode()->getName());
	}
      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda); 
      BirthDeathMCMC bdm(dm, S, birthRate, deathRate, !fixed_rates);
      ReconciliationTimeSampler rts(G, gs, bdm, gamma, rand);


      // Set up the SubstitutionMatrix
      //---------------------------------------------------------
      MatrixTransitionHandler Q = MatrixTransitionHandler::create(seqModel);
 
      //Set up mean and variance of substitution rates
      //---------------------------------------------------------
      if(!fixed_rateparams)
	{
 	  mean = 0.5/G.rootToLeafTime();
 	  variance = mean * rand.genrand_real1()/10; 
	}


      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{ 
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else if(density == "UNIFORM")
	{
	  df = new UniformDensity(0, 5, true);
	  cerr << "*******************************************************\n"
	       << "Note! mean and variance will always be fixed when using\n"
	       << "UniformDensity, the default interval will be (0,5)\n"
	       << "You might want to use the -Ef option\n"
	       << "*******************************************************\n";
	  fixed_rateparams = true;
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
	       << "for option -d\n";
	  usage(argv[0]);
	}

      // Set up rates and SubstitutionMCMC
      //---------------------------------------------------------
      EdgeRateMCMC* erm;
      if(rateModel == "gbm")
	{
	  erm = new gbmRateMCMC(bdm, *df, G, "EdgeRates");
	}
      else if(rateModel == "iid")
	{
	  erm = new iidRateMCMC(bdm, *df, G, "EdgeRates");
	}
      else
	{	 
	  erm = new ConstRateMCMC(bdm, *df, G, "EdgeRates");
	  cerr << "Note!: When using a 'const' rate model, we really can't "
	       << "estimate the mean and variance of the underlying density. "
	       << "So it's recommended to use the -e option in this case. The "
	       << "defaults settings are thus a little stupid and should be "
	       << "changed\n";
	}
      
      if(fixed_rateparams)
	{
	  erm->fixMean();
	  erm->fixVariance();
	}
      if(!substrates.empty())
	{
	  for(unsigned i = 0; i < substrates.size(); i++)
	    {
	      Node* n = G.getNode(i);
	      erm->setRate(substrates[i], n);
	    }
	  erm->fixRates();
	}
      else
	{
	  erm->generateRates();  // Get random start values for rates 
	}


      EdgeTimeRateHandler ewh(*erm);

      UniformDensity uf(0, 3, true); // U[0,3]
      ConstRateMCMC alphaC(*erm, uf, G, "Alpha"); 
      SiteRateHandler srm(n_cats, alphaC);
      if(n_cats == 1|| estimateAlpha == false)
	{
	  alphaC.fixRates();
	}

      vector<string> partitionList ;  //Will we ever use this?
      partitionList.push_back(string("all"));

      SubstitutionMCMC sm(dm, D, G, srm, Q, ewh, partitionList);

      SeqProbApproximator spa(alphaC, rts, sm, samples, false);

      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(spa, Thinning);

      if (do_likelihood)
	{
	  cout << spa;
	  cout <<"\n\nNo MCMC has ben run\n"
	       << "The likehood of the current state using "
	       << samples
	       << " Monte Carlo samples is:\n";
	  cout << spa.currentStateProb() << endl;
	  exit(0);
	}      

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  


      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cout << "#Running: ";
	  for(int i = 0; i < argc; i++)
	    {
	      cout << argv[i] << " ";
	    }
	  cout << " in directory"
	       << getenv("PWD")
	       << "\n#\n";
	  cout << "#Start MCMC (Seed = " << RSeed << ")\n";
	}


      // Perform and time the Likelihood calculation
      time_t t0 = time(0);
      clock_t ct0 = clock();
      
      iterator.iterate(MaxIter);
      // //       Probability p = rsa.suggestNewState().stateProb;
      // //       //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

      if (!quiet)
	{
	  cerr << sm.getAcceptanceRatio()
	       << " = acceptance ratio\n";
	}
      
      if (sm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

      delete df;
      delete erm;
    }
  catch(AnError e)
    {
      cerr <<" error\n";
      e.action();
    }
  catch(exception e)
    {
      cerr << " exception" << e.what() << endl;
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile> <datafile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         a string\n"
    << "   <datafile>         a string\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -p <int>              number of samples in MonteCarlo integration\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -S<option>            Options related to Substitution model\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'>\n"
    << "                         the substitution model to use, (JC69 is the\n"
    << "                         default). Must fit data type \n"
    << "     -Sn <float>         nCats, the number of discrete rate\n"
    << "                         categories (default is one category)\n"
    << "     -Sa <float>         alpha, the shape parameter of the Gamma\n"
    << "                         distribution for site rates (default: 1)\n"
    //     << "     -p <float>         probability of a site being invarant\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "     -Er <unsigned n> <float1> <float2> ... <floatn>\n"
    << "                         fixed rates of edge rate model\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gg                 fix edge times. If not set, a birth-death\n"
    << "                         model is used as a prior of the edge rates\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth and death rates of the birth-\n"
    << "                         death model \n"
    << "     -Bp <float>         start value of the birth rate parameter\n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'p':
	  {
	    if (sscanf(argv[++opt], "%d", &samples) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'S':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		      std::cerr << "Expected int (number of site rate\n"
				<< "classes) for option 'n'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		      estimateAlpha = false;
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-a'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	  // 	    case 'p':
	  // 	      	{
	  // 		  if (++opt < argc) 
	  // 		    {
	  // 		      pinv = atof(argv[opt]);
	  // 		    }
	  // 		  else
	  // 		    {
	  // 		      cerr << "Expected float (probability of a site being\n"
	  // 			   << "invariant) for option '-p'!\n";
	  // 		      usage(argv[0]);
	  // 		    }
	  // 		  break;
	  // 		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }	
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			  fixed_rateparams = true;
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'r':
		{
		  if(++opt < argc) 
		    {
		      unsigned numberOfRates = atoi(argv[opt]);
		      for(unsigned i = 0; i < numberOfRates; i++)
			{
			  if(++opt < argc && argv[opt][0] != '-') 
			    {
			      substrates.push_back(atof(argv[opt]));
			    }
			  else
			    {
			      cerr << "Expected float (edge rate)\n";
			      usage(argv[0]);
			    }
			}
		    }
		  else
		    {
		      cerr << "Expected unsigned (number of Rates) followed\n"
			   << "by a sequence of floats (edge rates) for \n"
			   << "option '-Er'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch(argv[opt][2])
		{
		case 'g':
		  {
		    fixed_times = true;
		    fixed_rates = true;
		    break;
		  }
		default:
		  {
		    cerr << "Warning: Unknown option '" << argv[opt] 
			 << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		  }
		}
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_rates = true;
		    // Don't break here, because we want to fall through to 'r' to set the
		    // rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  
