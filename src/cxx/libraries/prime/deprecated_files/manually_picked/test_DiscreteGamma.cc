#include <iostream>
#include <cmath>

#include "AnError.hh"
#include "Beep.hh"
#include "DiscreteGamma.hh"


int
main()
{
  using namespace beep;
  using namespace std;

  try
    {
      using std::cout;
      using std::cerr;


      cout << "min = " << Real_limits::min()
	   << "   max = " << Real_limits::max()
	   << "   digits10 = " << Real_limits::digits10
	   << endl << endl;

      cout << "Try to find LTONE\n"
	   << "LTONE = " 
	   << (Real_limits::digits10+9)/3
	   << endl << endl;

      Real u = 37;
      for(;std::exp(-u*u/2)/(2*u*3.14159265358979) > Real_limits::min();
	  u += 0.00001);
      cout << "Try to find UTZERO\n"
	   << "UTZERO = "
	   << u
	   << endl<<endl;	

      cout << "x\tnormal percentage point\n";
      for(Real X = 0.1; X <1; X+=0.1)
	{
	  cout <<  X << "\t" << gauinv(X) << "\n";
	}
      cout << "\n\n";

      cout << "x\tstandard normal distribution function\n";
      for(Real X = -10; X <10; X+=1)
	{
	  cout <<  X << "\t" << alnorm(X, false) << "\n";
	}
      cout << "\n\n";
      exit(0);

      cout << "x\tGamma(x)\n";
      for(Real X = 1; X <=2; X+=0.1)
	{
	  cout << X << "\t " << loggamma_fn(X) << "\n";
	}
      cout << "\n\n";


      cout << "x\tv\tppchi2\n";
      for(Real i = 0.1; i <1; i+=0.3)
      	{
	  for(Real X = 0.25; X < 1.0; X += 0.25)
	    {
	      cout << X << "\t" << i << "\t" << ppchi2(X,i) << "\n";
	    }
	}
      cout << "\n\n";


      cout << "x\talpha\tGamma_in\n";
      for(Real i = 1; i <2; i+=1)
	{
	  for(Real X = 0.1; X <1; X+=0.1)
	    {
	      cout << X << "\t" << i << "\t" << gamma_in(X,i) << "\n";
	    }
	}
      cout << "\n\n";



      cerr << "Test of main function\n";
      cout << "ncat\talpha\trates\n";
      for(unsigned ncat = 4; ncat < 5; ncat++)
	{
	  for(Real alpha = 0.1; alpha <=1; alpha+=0.1)
	    {
	      cout << ncat << "\t" << alpha << "\t" ;
	      
	      std::vector<Real> rates =  getDiscreteGammaClasses(ncat, alpha, 
								 alpha);
	      for(std::vector<Real>::const_iterator i = rates.begin(); 
		  i < rates.end(); i++)
		{
		  std::cout << *i << "\t";
		}
	      std::cout << "\n";
	    }
	}

      unsigned k = 4;
      for(double x = 0.1; x < 1; x += 0.1)
	{
	  vector<Real> rK = getDiscreteGammaClasses(k, x, x);
	  cout << x <<"\t"; 
	  for(unsigned i = 0; i < k; i++)
	    {
	      std::cout << rK[i] << "\t";
	    }
	  std::cout << "\n";
	}
      for(double x = 2; x < 10; x += 1)
	{
	  vector<Real> rK = getDiscreteGammaClasses(k, x, x);
	  cout << x <<"\t"; 
	  for(unsigned i = 0; i < k; i++)
	    {
	      std::cout << rK[i] << "\t";
	    }
	  std::cout << "\n";
	}
      for(double x = 20; x < 110; x += 10)
	{
	  vector<Real> rK = getDiscreteGammaClasses(k, x, x);
	  cout << x <<"\t"; 
	  for(unsigned i = 0; i < k; i++)
	    {
	      std::cout << rK[i] << "\t";
	    }
	  std::cout << "\n";
	}
      for(double x = 100; x < 1100; x += 100)
	{
	  vector<Real> rK = getDiscreteGammaClasses(k, x, x);
	  cout << x <<"\t"; 
	  for(unsigned i = 0; i < k; i++)
	    {
	      std::cout << rK[i] << "\t";
	    }
	  std::cout << "\n";
	}
      return 0;
    }
  catch(AnError e)
    {
      e.action(); 
    }

}

