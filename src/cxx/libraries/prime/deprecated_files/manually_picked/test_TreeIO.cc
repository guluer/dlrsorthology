#include "AnError.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "BDTreeGenerator.hh"


bool useID = false;
bool useET = false;
bool useNT = false;
bool useBL = false;
bool NWisET = false;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  if (argc < 2) 
    {
      usage(argv[0]);
    }
  try
    {
      // Read options and check sanity
      //---------------------------------------------
      int opt = readOptions(argc, argv);

      // Read arguments
      //---------------------------------------------
      if (opt >= argc)
	{
	  cerr << "Expected a number-of-leaves argument\n";
	  usage(argv[0]);
	}
      unsigned leaves = atoi(argv[opt++]);
    
      Tree S;     //NULL;
      if (opt < argc)
	{
	  string infile = argv[opt++];
	  TreeIO io_in = TreeIO::fromFile(infile);
	  S = io_in.readHostTree();
      	}
      else
	{
	  //Create a dummy Species tree
	  //---------------------------------------------------------
	  S = Tree::EmptyTree();
	}
      // Set up the tree generator
      //---------------------------------------------------------
      PRNG R;
      BDTreeGenerator RTG(S, 0.5, 0.5);
      cout << RTG;

    
      // Generating evolutionary tree
      Tree G;
      unsigned limit = 0;
      while(G.getNumberOfLeaves() != leaves && limit < 100)
	{
	  RTG.generateTree(G);
	  limit++;
	}
      if(G.getNumberOfLeaves() != leaves)
	{
	  cerr << "failed to generate a tree with " << leaves << " leaves\n";
	  exit(1);
	}
      GammaMap gamma = RTG.exportGamma();
      cout << G.print(true,true, true, true) << endl;
    
      // write tree
      cout << "writing tree\n";
      TreeIOTraits traits;
      traits.setID(useID);
      traits.setET(useET);
      traits.setNT(useNT);
      traits.setBL(useBL);
      traits.setNWisET(NWisET);
      string tree = TreeIO::writeBeepTree(G, traits, &gamma);
      cout << tree << endl;
    
      TreeIO io = TreeIO::fromString(tree);
      StrStrMap gs;
      vector<SetOfNodes> gamma2;

      cout << "\n\nreadBeepTree (with the settings given in the input)\n"
	   << "yields the following tree:\n";
      Tree G2 = io.readBeepTree(&gamma2, &gs);
      cout << G2.print(true, true, true, true);
      cout << "the following gs:\n"
	   << gs;
      cout << "and the following AC:\n";
      unsigned j = 0;
      for(vector<SetOfNodes>::const_iterator i = gamma2.begin();
	  i != gamma2.end(); i++, j++)
	{
	  if(i->empty() == false)
	    {
	      cout << "Species node " << j << " has the following map:\n"
		   << indentString(i->set4os());
	    }
	}
      cout << endl;
      cout << "Test convenience functions\n"
	   << "--------------------------\n";
      cout << "writeHostTree():\n"
	   << TreeIO::writeHostTree(G)
	   << endl;

      cout << "writeGuestTree():\n"
	   << TreeIO::writeGuestTree(G)
	   << endl;

      cout << "writeNewickTree():\n"
	   << TreeIO::writeNewickTree(G)
	   << endl;
      try
	{ 
	  cout << "ReadHostTree():\n";
	  G2 = io.readHostTree();
	  cout << G2.print(true, true, true,true) << endl;
	}     
      catch(AnError e)
	{
	  cerr << "This failed as expected:\n"
	       << indentString(e.what())
	       << endl;
	}

      try
	{
	  cout << "ReadGuestTree():\n";
	  G2 = io.readGuestTree();
	  cout << G2.print(true, true, true,true) << endl;
	}
      catch(AnError e)
	{
	  cerr << "This failed as expected:\n"
	       << indentString(e.what())
	       << endl;	  
	}
      try	  
	{
	  cout << "ReadNewickTree():\n";
	  G2 = io.readNewickTree();
	  cout << G2.print(true, true, true,true) << endl;
	}
      catch(AnError e)
	{
	  cerr << "This failed:\n"
	       << indentString(e.what())
	       << endl;
	}
      
      if (argc > opt) {
      cout << "Try reading multiple trees" << endl;
      TreeIO io2= TreeIO::fromFile(argv[opt++]);
      std::vector<StrStrMap> gsvec;
//       vector<Tree> all_trees = io2.readAllBeepTrees(traits, NULL, &gsvec);
      vector<Tree> all_trees = io2.readAllBeepTrees(NULL, &gsvec);
      j = 0; 
      for(vector<Tree>::iterator i = all_trees.begin(); 
	  i != all_trees.end(); i++, j++)
	{
	  cout << "Tree " << j << ":\n"
	       << (*i).print()
	       << endl;
	}
      }

    }
  catch(AnError e)
    {
      cerr << "Error: ";
      e.action();
    }
  catch(exception e)
    {
      cerr << "Exception:"
	   << e.what();
    }
};

void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <nleaves> [<host tree>]\n"
    << "\n"
    << "Parameters:\n"
    << "   <nleaves>      Number of leaves in generated tree\n"
    << "   <host tree>    The host tree on which to generate tree\n"

    << "Options:\n"
    << "   -u, -h         This text.\n"
    << "   -I             Include 'ID' when writing trees\n"
    << "   -E             Include 'ET' when writing and reading trees\n"
    << "   -N             Include 'NT' when writing and reading trees\n"
    << "   -B             Include 'BL' when writing and reading trees\n"
    << "   -W             Number after ':' in tree is interpreted as ET\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'I':
	  {
	    useID = true;
	    break;
	  }
	case 'E':
	  {
	    useET = true;
	    break;
	  }
	case 'N':
	  {
	    useNT = true;
	    break;
	  }
	case 'B':
	  {
	    useBL = true;
	    break;
	  }
	case 'W':
	  {
	    NWisET = true;
	    break;
	  }
	default:
	  {
	    cerr << "option " << argv[opt] << " not recognized\n";
	    usage(argv[0]);
	    break;
	  }
	}      
      opt++;
    }
  return opt;
}
