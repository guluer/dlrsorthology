#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include <sys/types.h>
#include <unistd.h>

#include "BirthDeathMCMC.hh"
#include "BirthDeathProbs.hh"
#include "DensityFunction2P.hh"
#include "DummyMCMC.hh"
#include "GammaDensity.hh"
#include "GeneTreeMCMC.hh"
#include "InvGaussDensity.hh"
#include "LogNormDensity.hh"
#include "PRNG.hh"
#include "ReconciliationSampler.hh"
#include "ReconciliationTimeSampler.hh"
#include "ReconSeqApproximator.hh"
#include "StdMCMCModel.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SequenceModel.hh"
#include "SimpleMCMC.hh"
#include "SiteRateMCMC.hh"
#include "TopTimeMCMC.hh"
#include "SubstitutionMatrix.hh"
#include "SubstitutionMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "VarRateMCMC.hh"

// Global options with default settings
//------------------------------------
unsigned nParams = 4;


unsigned Thinning = 100;
unsigned MaxIter = 10000;
unsigned RSeed = 0;
char* outfile=NULL;
bool quiet = false;
bool do_likelihood = false;
bool fixed_rates = false;
bool fixed_tree = false;
bool show_debug_info = false;
 

double BirthRate = 1.0;
double DeathRate = 1.0;
double Beta = 1.0;
unsigned nGammaTimeSamples = 1000;
double alpha = 1.0;
bool estimateAlpha = true;
unsigned n_cats = 1;
// double pinvar = 0.0;
double mean = 1.0;
double variance = 1.0;
bool estimateMean = true;
std::string density = "Gamma";
bool gbm = false;
std::string seqModel = "jc69";


// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      int opt = readOptions(argc, argv);
      unsigned nGammaSamples = nGammaTimeSamples;
      unsigned nTimeSamples = nGammaTimeSamples;


      //Get tree and Data
      //---------------------------------------------
      if(opt + 2 > argc)
	{
	  cerr << "Arguments missing\n";
	  exit(2);
	}
      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile);
      
      StrStrMap gs;
      string G_filename(argv[opt++]);
      TreeIO io = TreeIO::fromFile(G_filename);
      Tree G = io.readGeneTree(NULL, &gs);
				   
      io.setSourceFile(argv[opt++]);
      Tree S = io.readSpeciesTree();

      // We must not have a zero root time! We have to pretend we now
      // the time from the parent to the previous ancestor.
      Node *sr = S.getRootNode();
      if (sr->getTime() <= 0.0) 
	{
	  Real height = S.rootToLeafTime();
	  sr->setTime(height / 10.0);
	}

      // If gs is empty (no mapping info was found in the gene tree), 
      // we have to read a file with that info.
      if (gs.size() == 0  && opt < argc)
	{
	  gs = TreeIO::readGeneSpeciesInfo(string(argv[opt++]));
	}
      else
	{
	  cerr << "No mapping gene-name -> species-name found in the gene \n"
	       << "tree ("
	       << G_filename
	       << ") and no file with that mapping given.\n";
	  exit(2);
	}



      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      if (RSeed == 0)
	{
	  RSeed = getpid();		// Seeded by process id
	}
      PRNG rand(RSeed);
      DummyMCMC dm(rand);


      // Set up the SubstitutionMatrix
      // how handle this in another way?
      //---------------------------------------------------------
      SequenceModel* pmodel;
      if(seqModel == string("UniformAA"))
	{
	  pmodel =  new UniformAA();
	}
      else if(seqModel == ("JTT"))
	{
	  pmodel =  new JTT();
	}
      else if(seqModel == ("UniformCodon"))
	{
	  pmodel =  new UniformCodon();
	}
      else if (seqModel == "JC69")
	{
	  pmodel =  new JC69(); 
	}	 
      else
	{
	  cerr << "Unknown sequence model: "<< seqModel << endl;
	  usage(argv[0]);
	}


 
  
      // Create Q, the substitution matrix
      //---------------------------------------------
      beep::LA_Vector R = pmodel->R();
      beep::LA_DiagonalMatrix Pi = pmodel->Pi();
      beep::SubstitutionMatrix Q(R, Pi);
 
      //Set up mean and variance of rates
      //---------------------------------------------------------
      if(estimateMean)
	{
	  // this should use some max pairwise distance from data instead of 1
	  mean = 1/G.rootToLeafTime(); 
	  variance = mean * rand.genrand_real1(); 
	}

      // Set up Density function for rates
      // how handle this in a neater way?
      //---------------------------------------------------------
      DensityFunction2P* df;
      if(density == "InvG")
	{
	  df = new InvGaussDensity(1.0, variance);
	}
      else if(density == "LogN")
	{ 
	  df = new LogNormDensity(1.0, variance);
	}
      else if(density == "Gamma")
	{
	  df = new GammaDensity(1.0, variance);
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN' or 'Gamma' for option -d\n";
	  usage(argv[0]);
	}

      // Set up rates and SubstitutionMCMC
      // How handle this in a neater way?
      //---------------------------------------------------------
      EdgeRateModel erm(*df, G, mean);

      vector<string> partitionList ;  //Will we ever use this?
      partitionList.push_back(string("all"));
      iidRateMCMC irm(*df, mean, G, erm, dm, 0.5, estimateMean);
      gbmRateMCMC grm(*df, mean, G, erm, dm, 0.5, estimateMean);
      EdgeRateModel* rm = &irm;
      StdMCMCModel* smm = &irm;
      if(gbm)
	{
	  rm = &grm;
	  smm = &grm;
	}
//       SiteRateMCMC srm(*rm, n_cats, alpha, *smm, 0.5, estimateAlpha);
      SiteRateMCMC srm(*rm, n_cats, alpha, dm, 0.5, estimateAlpha);

      SubstitutionMCMC sm(G, D, srm, Q, partitionList, srm, 0.5);


      BirthDeathProbs bdp(S, BirthRate, DeathRate);
      ReconciliationTimeSampler rts(G, gs, bdp, rand);
      SeqProbApproximator spa(dm, rts, sm, 1);
      ReconSeqApproximator rsa(dm, spa, rts, nGammaSamples);

      TopTimeMCMC stm(dm, S, Beta);
      BirthDeathMCMC bdm(stm, rts, fixed_rates);
      GeneTreeMCMC gtm(bdm, rsa, rts, fixed_gene_tree, fixed_gene_tree);

      if (do_likelihood)
	{
	  cout << gtm.currentStateProb() << endl;
	  exit(0);
	}      

      //
      // Optional debug info
      //
      if (show_debug_info)
	{
	  G.debug_print();
	  cerr << endl;
	  
	  S.debug_print();
	  cerr << endl;
	}
          

      // Create MCMC handler
      //---------------------------------------------
      SimpleMCMC iterator(gtm, rand, Thinning);

      if (outfile != NULL)
	{
	  try 
	    {
	      iterator.setOutputFile(outfile);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << outfile
		   << "') Using stdout instead.\n";
	    }
	}  

      if (quiet)
	{
	  iterator.setShowDiagnostics(false);
	}
      
      if (!quiet) 
	{
	  cerr << "Start MCMC\n";
	}

      // Perform and time the Likelihood calculation
      time_t t0 = time(0);
      clock_t ct0 = clock();

      iterator.iterate(MaxIter);
      // //       Probability p = rsa.suggestNewState().stateProb;
      // //       //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;

      if (gtm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }

      //       delete df;
      //       delete iterator;
      delete pmodel;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <datafile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <datafile>         Name of file containing sequences in FastA or\n"
    << "                      Genbank format.\n"
    << "   <gene tree>        Gene tree in Newick format.\n"
    << "   <species tree>     Species tree in Newick format. Branchlengths\n"
    << "                      are important and represent time.\n"
    << "   <gene-species map> Optional. This file contains lines with a gene\n"
    << "                      name in the first column and species name\n"
    << "                      as found in the species tree in the second.\n"
    << "                      You can also choose to associate the genes \n"
    << "                      with species in the gene tree. Please see\n"
    << "                      documentation.\n"


    << "Options:\n"
    << "   -u, -h             This text.\n"
    << "   -o <filename>      output file\n"
    << "   -i <float>         number of iterations\n"
    << "   -t <float>         thinning\n"  
    << "   -s <int>           Seed for pseudo-random number generator. If \n"
    << "                      set to 0 (default), the process id is used as\n"
    << "                      seed.\n"
    << "   -q                 Do not output diagnostics to stderr.\n"
    << "   -D                 Debug info.\n"
    //     << "   -L                 Output likelihood. No MCMC.\n"
    << "   -g                 Fix the input gene tree.\n"
    << "   -f <float> <float> Fix rates for duplication and loss. Rates are \n"
    << "                      given as floating point values.\n"
    << "   -r <float> <float> Start values for duplication and loss rate. If\n"
    << "                      no rates are given, sensible start values are\n"
    << "                      chosen automatically.\n"
    << "   -p <int>           Precision: The number of iterations to \n"
    << "                      approximate the likelihood of reconciliation \n"
    << "                      and sequences. (" << nGammaTimeSamples << ")\n"
    << "   -b <float>         The beta parameter for a prior distribution on}n"
    << "                      species root distance. (" << Beta << ")\n"
    << "   -a <float>         alpha, the shape parameter of the Gamma\n"
    << "                      distribution modeling site rates (default: 1)\n"
    << "   -n <float>         nCats, the number of discrete rate categories\n"
    << "                      (default is one category)\n"
    //     << "   - <float>         probability of a site being invarant\n"
    << "   -v <float> <float>(fixed) mean and variance of edge rate model\n"
    << "   -d <'Gamma'/'InvG'/'LogN'> \n"
    << "                      the density function to use for edge rates, \n"
    << "                      (Gamma is the default) \n"
    << "   -e <'iid'/'gbm'>   the rate model to use, (iid is the default) \n"
    << "   -m <'jc69'/'UniformAA'/'JTT'/'UniformCodon'>\n"
    << "                      the substitution model to use, (iid is the \n"
    << "                      default). Must fit data type \n"

    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while (opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  usage(argv[0]);
	  break;

	case 'o':
	  if (opt + 1 < argc)
	    {
	      outfile = argv[++opt];
	    }
	  else
	    {
	      cerr << "Expected filename after option '-o'\n";
	      usage(argv[0]);
	    }
	  break;
	case 'i':
	  if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	    {
	      cerr << "Expected integer after option '-i'\n";
	      usage(argv[0]);
	    }
	  break;

	case 't':
	  if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	    {
	      cerr << "Expected integer after option '-t'\n";
	      usage(argv[0]);
	    }
	  break;

	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  quiet = true;
	  break;
	
	case 'D':
	  show_debug_info = true;
	  break;
	  
	case 'L':
	  do_likelihood = true;
	  break;
	   
	case 'g':
	  fixed_tree = true;
	  break;

	case 'f':
	  fixed_rates = true;
	  // Don't break here, because we want to fall through to 'r' to set the
	  // rates that are arguments both to '-r' and '-f'!
	case 'r':
	  {
	    if (++opt < argc) 
	      {
		BirthRate = atof(argv[opt]);
		if (++opt < argc) 
		  {
		    DeathRate = atof(argv[opt]);
		  }
		else
		  {
		    cerr << "Error: Expected a gene loss rate (death rate)\n";
		    usage(argv[0]);
		  }
	      }
	    else
	      {
		cerr << "Expected gene duplication and loss rates (birth and death rates) for option '-r' or '-f'!\n";
		usage(argv[0]);
	      }
	  }
	  break;

	case 'p':
	  if (sscanf(argv[++opt], "%d", &nGammaTimeSamples) == 0)
	    {
	      cerr << "Expected integer after option '-p'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'b':
	  if (++opt < argc) 
	    {
	      if (sscanf(argv[opt], "%lf", &Beta) == 0)
		{
		  cerr << "Expected number after option '-b'\n";
		  usage(argv[0]);
		}
	    }
	  break;
	
	case 'a':
	  {
	    if (++opt < argc) 
	      {
		alpha = atof(argv[opt]);
		estimateAlpha = false;
	      }
	    else
	      {
		cerr << "Expected float (shape parameter for site rate\n"
		     << "variation) for option '-a'!\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'n':
	  if (++opt < argc) 
	    {
	      n_cats = atoi(argv[opt]);
	    }
	  else
	    {
	      std::cerr << "Expected int (number of site rate classes) for \n"
			<< "option 'n'\n";
	      usage(argv[0]);
	    }
	  break;
	  // 	    case 'q':
	  // 	      	{
	  // 		  if (++opt < argc) 
	  // 		    {
	  // 		      pinv = atof(argv[opt]);
	  // 		    }
	  // 		  else
	  // 		    {
	  // 		      cerr << "Expected float (probability of a site being\n"
	  // 			   << "invariant) for option '-p'!\n";
	  // 		      usage(argv[0]);
	  // 		    }
	  // 		  break;
	  // 		}
	case 'v':
	  {
	    if (++opt < argc) 
	      {
		mean = atof(argv[opt]);
		if (++opt < argc) 
		  {
		    variance = atof(argv[opt]);
		    estimateMean = false;
		  }
		else
		  {
		    cerr << "Expected float (variance for edge rates)\n";
		    usage(argv[0]);
		  }
	      }
	    else
	      {
		cerr << "Expected pair of floats (mean and variance for\n"
		     << "edge rates) for option '-r' or '-f'!\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'd':
	  if (opt + 1 < argc)
	    {
	      density = argv[++opt];
	    }
	  else
	    {
	      cerr << "Expected density after option '-d'\n";
	      usage(argv[0]);
	    }
	  break;
	case 'e':
	  if (opt + 1 < argc)
	    {
	      if(string(argv[++opt]) == "gbm")
		gbm = true;
	      else if(string(argv[opt]) != "iid")
		{
		  cerr << "Expected 'iid' or ''gbm' after option '-m'\n";
		  usage(argv[0]);
		}
	      // else we use default = iid
	    }
	  else
	    {
	      cerr << "Expected 'i' or ''g' after option '-m'\n";
	      usage(argv[0]);
	    }
	  break;
	case 'm':
	  if (opt + 1 < argc)
	    {
	      seqModel = argv[++opt];
	    }
	  else
	    {
	      cerr << "Expected seqModel after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	default:
	  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	  usage(argv[0]);
	}
      opt++;
    }
  return opt;
}

	  
