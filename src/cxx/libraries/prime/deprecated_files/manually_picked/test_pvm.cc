#include <iostream>

using namespace std;

#include "AnError.hh"
#include "PvmMaster.hh"
#include "Tree.hh"
#include "TreeIO.hh"

using namespace beep;


char *treestr = "((a, b), (c, d:500)):10;";

int
main(int argc, char **argv)
{

  try 
    {
      PvmMaster M(string("/home/theory/arve/test_client"), 1);

      //ints
      cerr << "Number of clients: " << M.getNumClients() << endl;
      M.createMessage(17);
      M.multiCast();
      cerr << "Broadcast '17'\n";

      M.waitForMessage();
      int j;
      M.receive(j);

      cerr << "   Recieved '" << j << "'\n";

      //strings
      string nisse("Nisse");
      cerr << "Sending string" << endl;
      M.createMessage(nisse);
      M.multiCast();

      M.waitForMessage();
      string bertil;
      M.receive(bertil);
      cerr << "   Recieved '" << bertil << "'\n";

      //trees
      TreeIO io = TreeIO::fromString(treestr);
      Tree T = io.readGeneTree();
      cerr << "Sending a tree: " << T.print() << endl;
      M.createMessage(T);
      M.multiCast();

      M.waitForMessage();
      Tree T2;
      M.receive(T2);

      cerr << "   " << io.newickSpeciesStringTree(T2) << endl;

      // 
      cerr << "Sending sequence data." << endl;
      SequenceData sd(myDNA);
      sd.addData("Lasse", "acgt");
      sd.addData("Miranda", "tcag");
      M.createMessage(sd);
      M.multiCast(PvmBeep::SequenceData);

      M.waitForMessage();
      SequenceData sd2(myDNA);
      M.receive(sd2);
      cerr << "   " <<sd2;

      // Orthology
      cerr << "Sending an OM." << endl;
      OrthologyMatrix om(T);
      om.testing_set_elem();
      cerr << om.strRepresentation() << endl;
      OrthologyMatrix om2(T);
      M.createMessage(om);
      M.multiCast(PvmBeep::Orthology);
      cerr << "   Matrix sent, waiting" << endl;
      M.waitForMessage();
      M.receive(om2);
      cerr << "   Received OM: " << om2.strRepresentation() << endl;

    }
  catch (AnError e)
    {
      e.action();
    }
}
