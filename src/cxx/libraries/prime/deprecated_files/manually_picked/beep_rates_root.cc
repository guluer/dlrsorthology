#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>
#include <cstdlib>

#include "Beep.hh"
#include "BirthDeathMCMC.hh"
#include "EdgeWeightHandler.hh"
#include "EdgeRateMCMC_common.hh"
#include "Density2P.hh"
#include "DummyMCMC.hh"
#include "GammaMap.hh"
#include "GammaDensity.hh"
#include "InvGaussDensity.hh"
#include "LambdaMap.hh"
#include "LogNormDensity.hh"
#include "MatrixTransitionHandler.hh"
#include "PRNG.hh"
#include "ReconciliationTimeMCMC.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SequenceData.hh"
#include "SimpleMCMC.hh"
#include "SiteRateHandler.hh"
#include "StrStrMap.hh"
#include "SubstitutionMCMC.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "TreeMCMC.hh"
#include "UniformDensity.hh"


// Global options with default settings
//------------------------------------
int nParams = 2;

char* outfile=NULL;
unsigned MaxIter = 10000;
unsigned Thinning = 100;
unsigned printFactor = 1;
unsigned RSeed = 0;
bool quiet = false;
bool show_debug_info = false;
bool do_likelihood = false;

std::string seqModel = "jc69";
std::string seqType;
std::vector<double> Pi;
std::vector<double> R;
unsigned n_cats = 1;
double alpha = 1.0;
bool fixedAlpha = false;

std::string rateModel = "Const";
std::string density = "Uniform";
std::map<unsigned, beep::Real> start_rates;
double mean = -1.0;
double variance = -1.0;
bool fixed_rateparams = false;
bool fixed_mean = false;
double maxP = -1.0;

bool fixed_times = false;
std::map<unsigned, beep::Real> start_times;
bool fixed_bdrates = false;
double birthRate = -1.0;
double deathRate = -1.0;

// allowed models
std::vector<std::string> subst_models;
std::vector<std::string> rate_models;
std::vector<std::string> rate_densities;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);


// Main program
//-------------------------------------------------------------
int
main(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  // Set up allowed models
  //! \todo If we have a factory, this won't be needed/bens
  //-------------------------------------------------------------
  //rate model
  subst_models.push_back("UNIFORMAA");
  subst_models.push_back("JTT");
  subst_models.push_back("UNIFORMCODON");
  subst_models.push_back("ARVECODON");
  subst_models.push_back("JC69");
  
  //rate model
  rate_models.push_back("iid");
  rate_models.push_back("gbm");
  rate_models.push_back("const");
  
  //rate model
  rate_densities.push_back("Uniform");
  rate_densities.push_back("Gamma");
  rate_densities.push_back("LogN");
  rate_densities.push_back("InvG");

  if (argc < nParams) 
    {
      usage(argv[0]);
    }
  try
    {
      // tell the user we've started
      cerr << "Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cerr << argv[i] << " ";
	}
      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
#ifdef FIX_ROOT_EDGES_RATE
      cerr << "NOTE! In this version of beep_rates, the edge rate of\n"
	   << " root's right child is forced to have the same value\n"
	   << "as the left child\n"
	   << "This should be used with the gbm model only!\n";
#endif

      int opt = readOptions(argc, argv);
      if(opt + 2 > argc)
	{
	  cerr << "Too few arguments\n";
	  usage(argv[0]);
	  exit(1);
	}
      
      //Get tree and Data
      //---------------------------------------------
      string treefile(argv[opt++]);
      TreeIO io = TreeIO::fromFile(treefile);
      bool readEdgeTimes = fixed_times && start_times.empty();
      Tree G = io.readBeepTree(readEdgeTimes, false, false, 
			       readEdgeTimes, 0, 0);
      G.setName("G");

      // Set up the SubstitutionMatrix already here to be able
      // to determine seqtype
      //---------------------------------------------------------
      MatrixTransitionHandler* Q;
      if(seqModel == "USR")
	{
	  //So Stupid way of creating an instance!!!
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::
					  userDefined(seqType, Pi, R)); 
	}
      else
	{
	  Q = new MatrixTransitionHandler(MatrixTransitionHandler::
					  create(seqModel));
	}
      string datafile(argv[opt++]);
      SequenceData D = SeqIO::readSequences(datafile, Q->getType());

      //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if (RSeed != 0)
	{
	  rand.setSeed(RSeed);
	}

      DummyMCMC dm; // dm's only function is to end the chain of MCMCModels

      // Set up priors
      //---------------------------------------------------------
      Tree S = Tree::EmptyTree();
      S.setName("S");

      //set up a gamma
      StrStrMap gs;
      // Map all leaves of G to the single leaf in S
      for(unsigned i = 0; i < G.getNumberOfNodes(); i++)
	{
	  gs.insert(G.getNode(i)->getName(), S.getRootNode()->getName());
	}
      LambdaMap lambda(G, S, gs);
      GammaMap gamma = GammaMap::MostParsimonious(G, S, lambda); 

      if(birthRate <= 0 || deathRate <= 0)
	{
	  Real max_rate = MAX_INTENSITY / S.getRootNode()->getTime();
	  birthRate = max_rate * rand.genrand_real3();
	  deathRate = max_rate * rand.genrand_real3();
	}
      BirthDeathMCMC bdm(dm, S, birthRate, deathRate, !fixed_bdrates);
//       GuestTreeMCMC gtm(bdm, G, gs, bdm);
//       gtm.fixRoot();
//       gtm.fixTree();

      ReconciliationTimeMCMC rtm(bdm, G, bdm, gamma, "EdgeTimes");
      ReconciliationTimeSampler rts(G, bdm, gamma);
      rts.sampleTimes();

      //Set up mean and variance of substitution rates
      //---------------------------------------------------------
      if(maxP <= 0) // Check if user defined max exists <=> max >0
	{
	  // max branchlength should never be more than 5.0
	  
	  maxP = 5.0 / G.rootToLeafTime();
	}
      if(mean <=0)
	{
 	  mean = maxP * rand.genrand_real3(); 
	}
      if(variance <=0)
	{
 	  variance = mean * rand.genrand_real3()/10; 
	}


      // Set up Density function for rates
      //---------------------------------------------------------
      Density2P* df;
      capitalize(density);
      if(density == "INVG")
	{
	  df = new InvGaussDensity(mean, variance);
	}
      else if(density == "LOGN")
	{ 
	  df = new LogNormDensity(mean, variance);
	}
      else if(density == "GAMMA")
	{
	  df = new GammaDensity(mean, variance);
	}
      else if(density == "UNIFORM")
	{
	  df = new UniformDensity(0, 10, true);
	  cerr << "*******************************************************\n"
	       << "Note! mean and variance will always be fixed when using\n"
	       << "UniformDensity, the default interval will be (0,10)\n"
	       << "You might want to use the -Ef option\n"
	       << "*******************************************************\n";
	  fixed_rateparams = true;
	}
      else
	{
	  cerr << "Expected 'InvG', 'LogN', 'Gamma' or 'Const' "
	       << "for option -d\n";
	  usage(argv[0]);
	  exit(1);
	}

      // Set up rates and SubstitutionMCMC
      //---------------------------------------------------------
      EdgeRateMCMC* erm;
      if(rateModel == "gbm")
	{
	  erm = new gbmRateMCMC(rtm, *df, G, "EdgeRates");
	  // gbm model models vertex rates and may possibly allow higher rates
	  maxP *= 2.0;
	}
      else if(rateModel == "iid")
	{
	  erm = new iidRateMCMC(rtm, *df, G, "EdgeRates");
	}
      else
	{	 
	  erm = new ConstRateMCMC(rtm, *df, G, "EdgeRates");
	  cerr << "Note!: When using a 'const' rate model, we really can't "
	       << "estimate the mean and variance of the underlying density. "
	       << "So it's recommended to use the -e option in this case. The "
	       << "defaults settings are thus a little stupid and should be "
	       << "changed\n";
	}
      
      if(fixed_rateparams)
	{
	  erm->fixMean();
	  erm->fixVariance();
	}

      if(fixed_mean)
	{
	  erm->fixMean();
	}

      erm->generateRates();  // Get random start values for rates 

      EdgeTimeRateHandler ewh(*erm);

      UniformDensity uf(0, 3, true); // U[0,3]
      ConstRateMCMC alphaC(*erm, uf, G, "Alpha"); 
      if(n_cats == 1 || fixedAlpha)
	{
	  alphaC.fixRates();
	  alphaC.setRate(alpha, 0);
	}
      SiteRateHandler srm(n_cats, alphaC);

      vector<string> partitionList ; 
      partitionList.push_back(string("all"));

      SubstitutionMCMC sm(alphaC, D, G, srm, *Q, ewh, partitionList);


      // Create MCMC handler
      //---------------------------------------------

      BranchSwapping bs;
      Tree G_orig = G;
      for(unsigned i = 0; i < G.getNumberOfNodes()-1; i++)
	{
	  G = G_orig;
	  bs.reRoot(G, *G.getNode(i));
	  gamma = GammaMap::MostParsimonious(G, S, lambda); 
	  Real max_rate = MAX_INTENSITY / S.getRootNode()->getTime();
	  birthRate = max_rate * rand.genrand_real3();
	  deathRate = max_rate * rand.genrand_real3();
	  bdm.setRates(birthRate, deathRate);
	  rts.sampleTimes();

	  Real mean = maxP * rand.genrand_real3(); 
	  erm->setMean(mean);
 	  erm->setVariance(mean * rand.genrand_real3()/10); 
	  erm->generateRates();  // Get random start values for rates 

	  G.perturbedTree(true);
	  SimpleMCMC iterator(sm, Thinning);
	  
	  if (do_likelihood)
	    {
	      cout << sm.currentStateProb() << endl;
	      exit(0);
	    }      
	  
	  if (outfile != NULL)
	    {
	      ostringstream oss;
	      oss << outfile << i << ".mcmc";
	      try 
		{
		  iterator.setOutputFile(oss.str().c_str());
		}
	      catch(AnError e)
		{
		  e.action();
		}
	      catch (int e)
		{
		  cerr << "Problems opening output file! ('"
		       << outfile
		       << "') Using stdout instead.\n";
		}
	    }  
	  
	  
	  if (quiet)
	    {
	      iterator.setShowDiagnostics(false);
	    }
	  
	  if (!quiet) 
	    {
	      cout << "#Running: ";
	      for(int i = 0; i < argc; i++)
		{
		  cout << argv[i] << " ";
		}
	      cout << " in directory"
		   << getenv("PWD")
		   << "\n#\n";
	      cout << "#Start MCMC (Seed = " << rand.getSeed() << ")\n";
	      cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
#ifdef FIX_ROOT_EDGES_RATE
	      cout << "NOTE! In this version of beep_rates, the edge rate of\n"
		   << "root's right children is forced to have the same value\n"
		   << "as the left child\n"
		   << "This should be used with the gbm model only!\n";
#endif
	      cout << "current G:\n"
		   << G.print(false,false,false);
	      
	    }
	  
	  // Perform and time the Likelihood calculation
	  time_t t0 = time(0);
	  clock_t ct0 = clock();
	  
	  iterator.iterate(MaxIter, printFactor);
	  
	  time_t t1 = time(0);    
	  
	  clock_t ct1 = clock();
	  cerr << "Wall time: " << difftime(t1, t0) << " s"
	       << endl
	       << "CPU time: " << Real(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	       << endl;
	  
	  if (!quiet)
	    {
	      cerr << sm.getAcceptanceRatio()
		   << " = acceptance ratio\n";
	    }
	  
	  if (sm.getAcceptanceRatio() == 0) 
	    {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
	    }
	}
	  
      delete Q;
      delete df;
      delete erm;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
  catch(exception e)
    {
      cout <<" error\n"
	   << e.what();
    }
}

	      
void 
usage(char *cmd)
{
  using std::cerr;
  cerr 
    << "Usage: "
    << cmd
    << " [<options>] <treefile> <datafile> \n"
    << "\n"
    << "Parameters:\n"
    << "   <treefile>         a string\n"
    << "   <datafile>         a string\n"


    << "Options:\n"
    << "   -u, -h                This text.\n"
    << "   -o <filename>         output file\n"
    << "   -i <float>            number of iterations\n"
    << "   -t <float>            thinning\n"  
    << "   -w <float>            Write to cerr <float> times less often than\n"
    << "                         to cout\n"  
    << "   -s <int>              Seed for pseudo-random number generator. If\n"
    << "   -q                    Do not output diagnostics to stderr.\n"
    << "   -d                    Debug info.\n"
    << "   -l                    Output likelihood. No MCMC.\n"
    << "   -S<option>            Options related to Substitution model\n"
    << "     -Sm <'JC69'/'UniformAA'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
    << "                         the substitution model to use, (JC69 is the\n"
    << "                         default). Must fit data type \n"
    << "     -Su <datatype='DNA'/'AminoAcid'/'Codon'> <Pi=float1 float2 ... floatn>\n"
    << "                         <R=float1 float2 ...float(n*(n-1)/2)>\n"
    << "                         the user-defined substitution model to use.\n"
    << "                         The size of pi and R must fit data type (DNA: n=4\n"
    << "                         AminoAcid: n=20, Codon: n = 62), respectively. \n"
    << "                         If both -Su and -Sm is given (don't do this!),\n"
    << "                         only the last given is used\n"
    << "     -Sn <float>         nCats, the number of discrete rate\n"
    << "                         categories (default is one category)\n"
    << "     -Sa <float>         alpha, the shape parameter of the Gamma\n"
    << "                         distribution for site rates (default: 1)\n"
    //     << "     -p <float>         probability of a site being invarant\n"
    << "   -E<option>            Options relating to edge rate model\n"
    << "     -Em <'iid'/'gbm'/'const'> \n"
    << "                         the edge rate model to use (default: const)\n"
    << "     -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'> \n"
    << "                         the density function to use for edge rates,\n"
    << "                         (Uniform is the default) \n"
    << "     -Ep <float> <float> start mean and variance of edge rate model\n"
    << "     -Ef <float> <float> fixed mean and variance of edge rate model\n"
    << "     -Ea <float>         fixed mean of edge rate model\n"
    << "     -El <float>         User-defined (fixed) max value of rate params\n"
    << "   -G<option>            Options related to the gene tree\n"
    << "     -Gf                 fix edge times. If not set, a birth-death\n"
    << "                         model is used as a prior of the edge rates\n"
    << "   -B<option>            Options related to the birth death process\n"
    << "     -Bf <float> <float> fix the birth and death rates of the birth-\n"
    << "                         death model to these values \n"
    << "     -Bp <float> <float> start value of the birth and death rate parameters\n"
    ;
  exit(1);
}

int
readOptions(int argc, char **argv) 
{
  using namespace beep;
  using namespace std;

  int opt=1;
  while(opt < argc && argv[opt][0] == '-') 
    {
      switch (argv[opt][1]) 
	{
	case 'h':
	case 'u':
	  {
	    usage(argv[0]);
	    break;
	  }
	case 'o':
	  {
	    if (opt + 1 < argc)
	      {
		outfile = argv[++opt];
	      }
	    else
	      {
		cerr << "Expected filename after option '-o'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'i':
	  {
	    if (sscanf(argv[++opt], "%d", &MaxIter) == 0)
	      {
		cerr << "Expected integer after option '-i'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 't':
	  {
	    if (sscanf(argv[++opt], "%d", &Thinning) == 0)
	      {
		cerr << "Expected integer after option '-t'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 'w':
	  {
	    if (sscanf(argv[++opt], "%d", &printFactor) == 0)
	      {
		cerr << "Expected integer after option '-p'\n";
		usage(argv[0]);
	      }
	    break;
	  }
	case 's':
	  if (opt + 1 < argc && sscanf(argv[++opt], "%d", &RSeed) == 0)
	    {
	      cerr << "Expected integer after option '-s'\n";
	      usage(argv[0]);
	    }
	  break;
	  
	case 'q':
	  {
	    quiet = true;
	    break;
	  }
	case 'd':
	  {
	    show_debug_info = true;
	    break;
	  }
	case 'l':
	  {
	    do_likelihood = true;
	    break;
	  }	   
	case 'S':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = argv[++opt];
		      capitalize(seqModel);
		      if(find(subst_models.begin(), subst_models.end(), seqModel) 
			 == subst_models.end())
			{
			  cerr << "Expected 'UniformAA', 'JTT', 'UniformCodon' "
			       << "or 'JC69' for option -Sm\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'u':
		{
		  if (opt + 1 < argc)
		    {
		      seqModel = "USR";
		      seqType = argv[++opt];
		      capitalize(seqType);
		      int dim = 0;
		      if(seqType == "DNA")
			dim = 4;
		      else if(seqType == "AMINOACID")
			dim = 20;
		      else if(seqType == "CODON")
			dim = 61;
		      else
			{
			  cerr << seqType
			       << " is not a valid data type for option -Su\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		      if(opt + (dim * (dim + 1) / 2) < argc)
			{
			  for(int i = 0; i < dim; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for Pi: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}
				    
			      Pi.push_back(atof(argv[opt]));
			    }
			  for(int i= 0; i < dim * (dim - 1) /2; i++)
			    {
			      if(argv[++opt][0] == '-')
				{
				  cerr << "Inappropriate state for R: "
				       << argv[opt]
				       << " in option -Su\n";
				  usage(argv[0]);
				  exit(1); // check which error code to use!
				}

			      R.push_back(atof(argv[opt]));
			    }
			}
		      else
			{
			  cerr << "Too few parameters to -Su " 
			       << seqType
			       << "\n";
			  usage(argv[0]);
			  exit(1); // check which error code to use!
			}
		    }
		  else
		    {
		      cerr << "Expected seqModel after option '-m'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'n':
		{
		  if (++opt < argc) 
		    {
		      n_cats = atoi(argv[opt]);
		    }
		  else
		    {
		      std::cerr << "Expected int (number of site rate\n"
				<< "classes) for option 'n'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      alpha = atof(argv[opt]);
		      fixedAlpha = true;
		    }
		  else
		    {
		      cerr << "Expected float (shape parameter for site rate\n"
			   << "variation) for option '-a'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	  // 	    case 'p':
	  // 	      	{
	  // 		  if (++opt < argc) 
	  // 		    {
	  // 		      pinv = atof(argv[opt]);
	  // 		    }
	  // 		  else
	  // 		    {
	  // 		      cerr << "Expected float (probability of a site being\n"
	  // 			   << "invariant) for option '-p'!\n";
	  // 		      usage(argv[0]);
	  // 		    }
	  // 		  break;
	  // 		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		  usage(argv[0]);
		  exit(1); //Check for correct error code
		}
		break;
	      }
	    break;
	  }	
	case 'E':
	  {
	    switch(argv[opt][2])
	      {
	      case 'm':
		{
		  if (opt + 1 < argc)
		    {
		      rateModel = argv[++opt];
		      if(find(rate_models.begin(), rate_models.end(), rateModel) 
			 == rate_models.end())
			{
			  cerr << "Model "
			       << argv[opt] 
			       << "does not exist\n" 
			       << "Expected 'iid', ''gbm' or 'const' "
			       << "after option '-Em'\n";
			  usage(argv[0]);
			  exit(1); //Check what error code should be used!
			}
		      // else we use default = const
		    }
		  else
		    {
		      cerr << "Expected 'iid', ''gbm' or 'const' after "
			   << "option '-Em'\n";
		      usage(argv[0]);
		      exit(1); //Check what error code should be used!
		    }
		  break;
		}
	      case 'd':
		{
		  if (opt + 1 < argc)
		    {
		      density = argv[++opt];
		    }
		  else
		    {
		      cerr << "Expected density after option '-d'\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'f':
		{
		      fixed_rateparams = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		}
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  variance = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Expected float (variance for "
			       << "edge rates)\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'a':
		{
		  if (++opt < argc) 
		    {
		      mean = atof(argv[opt]);
		      fixed_mean = true;
		    }
		  else
		    {
		      cerr << "Expected pair of floats (mean and variance\n"
			   << "for edge rates) for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      case 'l':
		{
		  if (++opt < argc) 
		    {
		      maxP = atof(argv[opt]);
		      if(maxP <= 0)
			{
			  cerr << "Warning: the value given for -El was\n"
			       << "<= 0, and will be ignored\n";
			}
		    }
		  break;
		}
	      default:
		{
		  cerr << "Warning: Unknown option '" << argv[opt] 
		       << "'!\n\n";
		  usage(argv[0]);
		  exit(1); // Check for correct error code
		}
	      }
	    break;
	  }
	case 'G':
	  {
	    switch(argv[opt][2])
		{
		case 'f':
		  {
		    fixed_times = true;
		    fixed_bdrates = true;
		    break;
		  }
		default:
		  {
		    cerr << "Warning: Unknown option '" << argv[opt] 
			 << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		  }
		}
	    break;
	  }
	case 'B':
	  {
	    switch(argv[opt][2])
	      {
	      case 'f':
		  {
		    fixed_bdrates = true;
		    // Don't break here, because we want to fall through to 'r'
		    // set the rates that are arguments both to '-r' and '-f'!
		  }
	      case 'p':
		{
		  if (++opt < argc) 
		    {
		      birthRate = atof(argv[opt]);
		      if (++opt < argc) 
			{
			  deathRate = atof(argv[opt]);
			}
		      else
			{
			  cerr << "Error: Expected a gene loss (death) rate\n";
			  usage(argv[0]);
			}
		    }
		  else
		    {
		      cerr << "Expected birth rate for option '-p' or '-f'!\n";
		      usage(argv[0]);
		    }
		  break;
		}
	      default:
		{
		    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
		    usage(argv[0]);
		    exit(1); // Check for correct error code
		}
	      }
	      break;
	  }
	default:
	  {
	    cerr << "Warning: Unknown option '" << argv[opt] << "'!\n\n";
	    usage(argv[0]);
	    exit(1); // Check for correct error code
	  }
	  break;
	}
      opt++;
    }
  return opt;
};

	  
  
