#include "AnError.hh"
#include "Beep.hh"
#include "BeepOption.hh"
#include "EdgeDiscPtMapIterator.hh"
#include "EdgeDiscTree.hh"
#include "TreeDiscretizers.hh"
#include "TreeIO.hh"

static const int NO_OF_PARAMS = 2;

std::string ParamTreeFile = "";   // Param 1: Name of file containing tree S.
beep::Real ParamToptime = 1.0;    // Param 2: Top time.

/************************************************************************
 * Main program.
 ************************************************************************/ 
int main(int argc, char **argv) 
{
	using namespace beep;
	using namespace beep::option;
	using namespace std;

	try
	{
		int argIndex = 1;
		if (argc - argIndex != NO_OF_PARAMS)
		{
			throw beep::AnError("Wrong number of input parameters!");
		}
		ParamTreeFile = argv[argIndex++];
		if (!BeepOptionMap::toDouble(argv[argIndex++], ParamToptime))
		{
			throw beep::AnError("Invalid top time value!");
		}

		// Create S from file.
		Tree S(TreeIO::fromFile(ParamTreeFile).readHostTree());
		S.setTopTime(ParamToptime);
		
		// Tree discretization.
		EdgeDiscTree* DS = NULL;
		
		// Create discretization according to user input.
		cout << "Please choose a discretization type:" << endl
			<< "1) EquiSplitEdgeDiscretizer" << endl
			<< "2) TimestepEdgeDiscretizer" << endl;
		string tmp, tmp2, tmp3;
		cin >> tmp;
		if (tmp[0] == '1')
		{
			cout << "Enter no. of intervals on each edge: ";
			cin >> tmp;
			cout << "Enter no. of intervals on top time edge: ";
			cin >> tmp2;
			EquiSplitEdgeDiscretizer disc(atoi(tmp.c_str()), atoi(tmp2.c_str()));
			DS = disc.getDiscretization(S);
		}
		else if (tmp[0] == '2')
		{
			cout << "Enter target timestep: ";
			cin >> tmp;
			cout << "Enter min. no of intervals: ";
			cin >> tmp2;
			cout << "Enter target timestep for top time edge: ";
			cin >> tmp3;
			StepSizeEdgeDiscretizer disc(atof(tmp.c_str()), atoi(tmp2.c_str()), atof(tmp3.c_str()));
			DS = disc.getDiscretization(S);
		}
		else {
			cout << "Invalid choice. Exiting." << endl;
			exit(0);
		}
		
		// Print facts.
		cout << "Top time: " << DS->getTopTime() << endl
			<< "Time of root: " << DS->getTime(DS->getRootNode()) << endl
			<< "Top-to-leaf time: " << DS->getTopToLeafTime() << endl
			<< "Root-to-leaf time: " << DS->getRootToLeafTime() << endl
			<< "Timespan of edge 0: " << DS->getEdgeTime(DS->getNode(0)) << endl;
		cout << "Printing entire discretization: " << endl << (*DS);
		cout << "Printing points on path from leaf 0 to topmost point: " << endl;
		EdgeDiscPtMapIterator<Real> it;
		EdgeDiscPtMapIterator<Real> endPlus = DS->endPlus();
		for (it = DS->begin(S.getNode(0)); it != endPlus; ++it)
		{
			cout << (*it) << ", ";
		}
		cout << endl;
		cout << "Printing path again, but excluding node-corresponding points: " << endl;
		for (it = DS->begin(S.getNode(0)); it != endPlus; it.pp())
		{
			cout << (*it) << ", ";
		}
		
		delete DS;
	}
	catch (AnError& e)
	{
		e.action();
		cout << "Usage: " << argv[0] << " <tree file> <top time>" << endl;
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		cout << "Usage: " << argv[0] << " <tree file> <top time>" << endl;
	}
	cout << endl;
};
