#include <cmath>
#include <iostream>

#include "AnError.hh"
#include "Beep.hh"
#include "IntegralBirthDeathProbs.hh"
#include "Integrals.hh"

using namespace std;

namespace beep
{

// 
// Construct/Destruct
// beta is the Expected mean of the exponential distribution for toptime!
//
IntegralBirthDeathProbs::IntegralBirthDeathProbs(Tree &S_in, 
						 Real birth_rate,
						 Real death_rate,
						 Real &beta)
  : BirthDeathProbs(S_in, birth_rate, death_rate),
    beta(beta),
    nIntervals(200)
{
}


IntegralBirthDeathProbs::IntegralBirthDeathProbs(const IntegralBirthDeathProbs &M)
  : BirthDeathProbs(M),
    beta(M.beta),
    nIntervals(M.nIntervals)
{
}

IntegralBirthDeathProbs::~IntegralBirthDeathProbs()
{

}


//
// Assignment
//
IntegralBirthDeathProbs&
IntegralBirthDeathProbs::operator=(const IntegralBirthDeathProbs &dsp)
{
  if (this != &dsp)
    {
      BirthDeathProbs::operator=(dsp),
      beta = dsp.beta;
      nIntervals = dsp.nIntervals;
      }

  return *this;
}



//---------------------------------------------------------------------
//
// Interface
//
//---------------------------------------------------------------------

// User can set number Intervals in numerical integration
void
IntegralBirthDeathProbs::setNumberOfIntegrationIntervals(unsigned n)
{
  nIntervals = n;
}


// Access to the beta parameter
//---------------------------------------------------------------------
// Real 
// IntegralBirthDeathProbs::getBeta() const
// {
//   return beta;
// }

// //---------------------------------------------------------------------
// Real
// IntegralBirthDeathProbs::setBeta(const Real newBeta)
// {
//   Real old = beta;
//   beta = newBeta;
//   return old;
// }

// We treat the top slice differently since we actually don't know the
// time scale of the top slice. Therefore, we are adapting an exponential
// (is it?) prior on the time before the species root.
//
// Remember kids: This is not a probability, since we are cancelling a 
// factor $(1-D)^{c+1}$ also occuring elsewhere! That is why it has *Partial* 
// in its name.
//---------------------------------------------------------------------
Probability
IntegralBirthDeathProbs::topPartialProbOfCopies(unsigned n_kids) const
{
//   int nIntervals = 200;
  Node *r = getStree().getRootNode();
  Real from = 0.0;
  Real to = 1.0;
  Probability D = extinctionProbability(r);
  Real b, d;

  getRates(b, d);
  DensityFunctor f_ct = DensityFunctor(*this, b, d, beta, n_kids, D);

  Probability P = simpsonRule<Real, DensityFunctor>(from, to, nIntervals, f_ct);

//   if(P<0.0 || P>1.0)
//     cerr << "P = " <<  P.val() << endl;
  assert(P >= 0.0);
  assert(P * pow(1.0-D, n_kids-1) <= 1.0);

  return(P);

}



//---------------------------------------------------------------------
//
// Implementation
//
//---------------------------------------------------------------------



// The root is a special case since we don't know the time before 
// first duplication or whatever.
//----------------------------------------------------------------------
void
IntegralBirthDeathProbs::calcBirthDeathProbs(Node &root)
{
  if (root.isLeaf() == true)
    {
      throw AnError("This program cannot handle single species trees just yet. Lasse simply does not have the head to figure it out at the moment.");
    }
  else
    {
      Node &left = *(root.getLeftChild());
      Node &right = *(root.getRightChild());
      calcBirthDeathProbs_recursive(left);
      calcBirthDeathProbs_recursive(right);

      Probability D = extinctionProbability(&root);

      // The first two values are not used.
       BD_const[root] = 1.0;
       BD_var[root] = 1.0;
       BD_zero[root] = D;
    }
}



//------------------------------------------------------------------------
//
// The functor class to aid with integration using a templated integrator
// Rechecked by Bengt. Notice integrand substitution x = exp(-t) yields
// \int_0^\ifnty (expr(t))dt = -\int_0^1 (expr(t->x))/x 
//
//------------------------------------------------------------------------
DensityFunctor::DensityFunctor(const IntegralBirthDeathProbs &model, 
			       const Real &birthR, const Real &deathR, 
			       const Real &b, 
			       const unsigned nkids, 
			       const Probability &Extinction)
  : M(model),
    bRate(birthR),
    dRate(deathR),
    beta(b),
    c(nkids),
    D(Extinction)
{
  alfa = bRate - dRate;
  bD = D * bRate;
}

Real
DensityFunctor::operator() (Real x)
{
  Real y = std::pow(x,alfa);
  Real t;
  Probability q;
  Real invBeta = 1.0/beta;

  if (x==1)
    {
      Probability f = 0;
      return(f.val());
    }
  // This is only for integration purpose, the function is really
  // undefined for x = 0.
  else if(x==0)
    {
      Probability f = 0;
      return(f.val());
    }


  // If birth_rate equal to death_rate:
  if (y == 1)
    {
      t = invBeta 
	* std::pow(-bRate * std::log(x), (int) c - 1) 
	* std::pow(x, invBeta - 1);
      q = bRate * ( - log(x)) * (1.0 - D)+1.0;
    }
  // y < 1, i.e., birth - rate < death_rate:
  else if (y < 1)
    {
      t = invBeta
	* y
	* std::pow(bRate, (int) c - 1) 
	* std::pow(alfa, 2.0)
	* std::pow(1.0 - y, (int) c - 1)
	* std::pow(x, invBeta - 1);
      //    if (bD == dRate){
      if (bD == dRate)
	{
	  q = bRate - bD;
	}
      else if (bRate - bD <(dRate - bD) * y)
	{
	  throw AnError("Nonvalid values of birth_rate and death_rate"
			"in Pr(c|t,birth_rate,death_rate)");
	}
      else
	{
	  q = bRate - bD+(bD - dRate) * y;
	}
    }
  // y > 1, i.e.,  birth - rate > death_rate:
  else
    {
      t = invBeta 
	* y 
	* std::pow(bRate, (int) c - 1) 
	* alfa * alfa		// pow(alfa,2.0) 
	* std::pow(y - 1, (int) c - 1) 
	* std::pow(x, invBeta - 1);
      if (bRate - bD >(dRate - bD) * y)
	{
	  throw AnError("Nonvalid values of birth_rate and death_rate"
			"in Pr(c|t,birth_rate,death_rate).");
	}
      else
	{
	  q = (dRate - bD) * y + bD - bRate;
	}
    }
  Probability n = pow(q, (int) c + 1);
  Probability f = t/n;
  return(f.val()); 
}

}//end namespace beep
