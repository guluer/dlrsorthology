#!/usr/bin/perl

use strict;
use Getopt::Long; # for parsing command line arguments
use Pod::Usage;

#Get options from command line
# Get command line options - if you are curious how this 
# works, check the Getopt::Long module documentation.

=head1 NAME

generateSFromPosterior.pl

=head1 SYNOPSIS

generateSFromPosterior.pl -m mcmcfile -t time -l low -h high -o outfile -s start -n number

=head1 ARGUMENTS

 -m mcmcfile     = Name of file containing mcmc output data
 -t time         = The node number of the subtree from wich
                   time should be taken
 -l low          = low limit on number of leaves
 -h high         = high limit on number of leaves
 -o outfile      = output file for generated trees
 -s start        = start at iteration <start<, discard burnin
 -n number       = sample <number> iterations
 -u usage        = brief help

=head1 DESCRIPTION

=cut


my ($infile, $tcol, $minleaves, $maxleaves, $outfile, $burnin, $number, $help);

GetOptions('usage'           => \$help,
	   'mcmcfile=s'      => \$infile,
	   'time=i'          => \$tcol,
	   'low=i'           => \$minleaves,
	   'high=i'          => \$maxleaves,
	   'start=i'         => \$burnin,
	   'number=i'        => \$number,
	   'outfile=s'	     => \$outfile,
	  );


if($help)  {
  pod2usage(-exitstatus=>0, -verbose=>2);
}
if (!$infile) {
    pod2usage(1);
  }

if (!$tcol) {
  pod2usage(1);
}

if (!$minleaves){
  pod2usage(1);
}

if (!$maxleaves){
  pod2usage(1);
}

if (!$burnin){
  pod2usage(1);
}

if (!$number){
  pod2usage(1);
}

if (!$outfile){
  pod2usage(1);
}

my @db;
my $birth;
my $death;
my $time;
my $bcol;
my $dcol;
#Check that outfile can be opened
if (open (INFILE, $infile)){
  my $inline;
  my @info;
  my $current = 0;
  while ($inline=<INFILE>){
    chomp $inline;	
    if(length($inline) != 0 && substr($inline,0,1) ne "#") {
      @info = split(/\t/, $inline);
      if($current >= $burnin) {
	$birth = $info[$bcol];
	$birth =~ s/;//;
	$death = $info[$dcol];
	$death =~ s/;//;
	$time = $info[$tcol];
	$time =~ s/;//;
	push(@db, [($birth, $death, $time)]);
      }
      $current++;
    }
    elsif(substr($inline,0, 3) eq "# L") {
      @info = split(/[\t ]/, $inline);
      for(my$i=0; $i < scalar(@info); $i++) {
	if($info[$i] =~ "birthRate") {
	  $bcol = $i-1;
	}
	elsif($info[$i] =~ "deathRate") {
	  $dcol = $i-1;
	}
	elsif($info[$i] =~ /nodeTime.$tcol/) {
	  print "$info[$i] \n";
	  $tcol = $i-1;
	}
      }
      if($bcol+1 >= scalar(@info) || $dcol+1 >= scalar(@info)) {
	print "Either the birth- or the death_column is too high\n";
	exit 0;
      }
      print "Extracting columnns, first col = $info[0]  bcol = $info[$bcol+1], dcol = $info[$dcol+1], time = $info[$tcol+1]\n";
    }
  }
}
else {
  print "could not open $infile\n";
  exit 0;
}


my $N = 0;
my $r;
my $truefile = $outfile.".true";
my $cmd;
my @l;
while($N < $number) {
  $r = int(rand(scalar(@db)));
  my$size = scalar(@db);
  $birth = $db[$r][0] / 130;
  $death = $db[$r][1] / 130;
  $time  = $db[$r][2] * 130;

  print "generating from iteration $r with birth rate $birth, death rate $death and time $time.\n";
  $cmd = "aladen -q -o tmp.G -l 100000 -Gs -To tmp.true -Gt -Gl $minleaves -Gh $maxleaves -Bp $birth $death -Bt $time";
  my $err = system("$cmd 2>&1 1>/dev/null"); #run aladen with generated tree saved to tmpfile
  if($err eq "0") {
    if($N == 0) {
      system("cat tmp.G |sed s/NAME=Tree/NAME=Tree$N/ > $outfile");
      system("cat tmp.true  > $truefile");
    }
    else {
      system("cat tmp.G |sed s/NAME=Tree/NAME=Tree$N/ >> $outfile");
      system("head -n 3 tmp.true |tail -n 1 >> $truefile");
    }
    $N += 1;
  }
  else{
    print "generation from iteration $r failed\n";
    print "$err\n";
  }
}

