#!/usr/bin/python

import getopt
import glob
import os
import re
import sys

HEADER_START_TAG = "# L N"
HEADER_SPLIT_PATTERN = "[^a-zA-Z0-9_\.\(\)]+|\(\w+\)\W+"
COMMENT_START_TAG = "#"
SAMPLE_SPLIT_PATTERN = ";*\t"


def ExtractParameters(params, burninratio, outfile, infiles):
    o = open(outfile, 'w')
    
    # For each parallel chain file.
    for fname in infiles:
        
        # Read number of samples.
        n = 0
        f = open(fname, 'r')
        for ln in f:
            if not ln.startswith(COMMENT_START_TAG): n += 1
        f.close()
        burnin = round(burninratio * n)
        
        # Open file again and process lines.
        f = open(fname, 'r')
        parammap = []
        bs = 0
        
        # For each line in chain file.
        for ln in f:
            
            # Read away preamble, then get parameter names and map.
            if ln.startswith(HEADER_START_TAG):
                fparams = re.split(HEADER_SPLIT_PATTERN, ln)
                if len(fparams[0]) == 0: fparams.pop(0)
                if len(fparams[-1]) == 0: fparams.pop()
                for i in range(len(params)):
                    for j in range(len(fparams)):
                        if params[i] == fparams[j]:
                            parammap.append(j)
                            break
                if len(parammap) != len(params):
                    raise Exception("Did not find all parameters in file!")
                continue
            
            # Ignore commented lines.
            if ln.startswith(COMMENT_START_TAG): continue
                
            # Read past burn-in part.
            if bs < burnin:
                bs += 1
                continue
            
            # Retrieve and print parameters.
            sparams = re.split(SAMPLE_SPLIT_PATTERN, ln)
            for i in range(len(parammap)-1):
                o.write(sparams[parammap[i]] + ',')
            o.write(sparams[parammap[-1]] + '\n')
        f.close()
    o.close()


# Shows error/help message, then exits.
def usage():
    print """
General PrIME script to extract parameters from multiple parallel MCMC chains.
Usage: ./ExtractParameters <Params> <Burn-in> <Outfile> <Infile1> [<Infile2> ...]
where <Params>  is a comma-separated list of parameter names (no spaces).
      <Burn-in> is e.g. 0.25 too discard first 25% samples of each input file.
"""


# Starter.
if __name__ == "__main__":
    # Read options and arguments.
    try:
        opts, args = getopt.getopt(sys.argv[1:], "h")
    except getopt.GetoptError:
        usage()
        sys.exit(2)
    for opt, optval in opts:
        if opt == "-h":
            usage()
            sys.exit(0)
    
    try:
        if len(args) < 4: raise Exception("Too few input arguments. Use -h to show help.")
        params = args[0].split(',')
        burninratio = float(args[1])
        outfile = args[2]
        infiles = args[3:]
        ExtractParameters(params, burninratio, outfile, infiles)

    except Exception, e:
        print "Error: ", e
        sys.exit(2)
    except:
        print "Unexpected error: ", sys.exc_info()[0]
        sys.exit(2)