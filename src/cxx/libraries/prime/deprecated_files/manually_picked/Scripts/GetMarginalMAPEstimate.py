#!/usr/bin/python

import getopt
import os
import random
import sys

# Bandwidth options.
BWS = {
'NRD'    : '\"nrd\"',
'NRD0'   : '\"nrd0\"',
'UCV'    : '\"ucv\"',
'BCV'    : '\"bcv\"',
'SJ_DPI' : 'bw.SJ(x=col,method=\"dpi\")',
'SJ_STE' : 'bw.SJ(x=col,method=\"ste\")'
}

# Kernel options.
KERNELS = {
'GAUSS'  : '\"gaussian\"',
'EPAN'   : '\"epanechnikov\"',
'RECT'   : '\"rectangular\"',
'TRI'    : '\"triangular\"',
'BI'     : '\"biweight\"',
'COS'    : '\"cosine\"',
'OPTCOS' : '\"optcosine\"'
}


# Input:
#   infiles   - list of input coda filenames.
#   params    - list of param indices for which to extract MAP.
#   negParams - subset of params for which samples can take on values < 0.
#   treeParam - parameter containing tree IDs, -1 if not available/used.
#   bw        - bandwidth: float or one of constants.
#   kernel    - kernel.
#   n         - no. of estimation points, power of 2 makes sense.
def GetMarginalMAPEstimate(infiles, params, negParams, treeParam,
                           bw, kernel, n):

    sz = len(infiles)
    
    # Do some parsing of input.
    if sz <= 0:
        raise Exception("Lacking input file.")
    if len(params) <= 0:
        raise Exception("Lacking parameters.")
    if treeParam != -1 and len(infiles) > 1:
        raise Exception("Cannot use option -t together with multiple infiles.")
    if (bw in BWS):
        bw = BWS[bw]
    else:
        bw = abs(float(bw))
    if kernel not in KERNELS:
        raise Exception("Invalid kernel.")
    kernel = KERNELS[kernel]
    n = abs(int(n))
    
    # Get random file names.
    rndno = random.sample(xrange(100000000), 1)[0]
    fr = 'margmap.%s.r' % rndno
    fdummy = 'margmap.%s.dummy' % rndno
    fout = 'margmap.%s.out' % rndno
    
    # Commence construction of R file.
    f = open(fr, 'w')
    
    # Input, chain files, etc.
    f.write('library(coda);\n\n')
    f.write('params=c(%s);\n' % ','.join(params));
    f.write('negparams=c(%s);\n' % ','.join(negParams));
    f.write('treeparam=%s;\n\n' % treeParam)
    tstr = ''
    mstr = ''
    ml = []
    for i in range(sz):
        tstr += ('t%s=read.table(\"%s\"); ' % (i, infiles[i]))
        mstr += ('m%s=mcmc(t%s); ' % (i, i))
        ml.append('m%s' % i)
    f.write(tstr + '\n')
    f.write(mstr + '\n')
    f.write('ml=mcmc.list(%s);\n' % ','.join(ml))
    f.write('mat=as.matrix(ml);\n')
    
    # Algorithm.
    f.write(
"""
n=dim(mat)[1];
nparams=length(params);
MAPs=rep(x=-1e100,times=nparams);          # Best MAP so far.
MAPdensities=rep(x=-1e100,times=nparams);  # Densities for best MAPs.

# If there is a tree parameter, we count number of unique
# trees in posterior with coverage > 0.1.
ntrees=-1;
if (treeparam > 0) {
   cnt=table(mat[,treeparam]);
   flt=cnt[which(cnt > 0.1*n)];
   ntrees=length(flt);
}

# If we have trees, we compute MAP w.r.t. parameter*tree space
# (by comparing densities for each of the trees).
if (ntrees > 0) {
   for (i in 1:ntrees) {
      # Extract samples pertaining to this tree.
      samples=mat[mat[,treeparam]==names(flt)[i],];
      # Compute density for each parameter.
      for (j in 1:nparams) {
         col=samples[,params[j],drop=TRUE];
         if (is.element(el=params[j],set=negparams)) {
            # Don't mirror the distribution.
            dens=density(x=col,bw=%s,kernel=%s,n=%s);
         } else {
            # Mirror the distribution, truncate at 0. Note: Bandwidth w.r.t. non-mirrored.
            dens=density(x=c(col,-col),from=0,bw=%s,kernel=%s,n=%s);
         }
         mapdens=max(dens$y)*flt[i]/n;
         if (mapdens > MAPdensities[j]) {
            MAPdensities[j] = mapdens;
            MAPs[j] = dens$x[which.max(dens$y)];
         }
      }
   }
} else {
   # Compute MAP w.r.t. parameter space (tree space not considered).
   for (j in 1:nparams) {
      col=mat[,params[j],drop=TRUE];
      if (is.element(el=params[j],set=negparams)) {
         # Don't mirror the distribution.
         dens=density(x=col,bw=%s,kernel=%s,n=%s);
      } else {
         # Mirror the distribution, truncate at 0. Note: Bandwidth w.r.t. non-mirrored.
         dens=density(x=c(col,-col),from=0,bw=%s,kernel=%s,n=%s);
      }
      MAPdensities[j]=max(dens$y);
      MAPs[j] = dens$x[which.max(dens$y)];
   }
}

""" % (bw,kernel,n,bw,kernel,n,bw,kernel,n,bw,kernel,n))
    
    # Print output and exit.
    f.write('write(MAPs,file=\"%s\",sep=\"\\t\");\n' % fout)
    f.write('q(save=\"no\");\n')
    f.close()
    
    # Fire away.
    os.system('R CMD BATCH --vanilla --slave %s %s' % (fr, fdummy))
    os.system('rm %s' % fr)
    os.system('rm %s' % fdummy)
    if os.path.isfile(fout):
        f = open(fout, 'r')
        print (f.readlines()[0].strip())
        f.close()
        os.system('rm %s' % fout)
    else:
        print "Error: No output was produced from R -- please check your input."


def usage():
    print \
"""
===============================================================================
Extracts the (maximum) mode from each marginal distribution of a set of desired
parameters. Input is one or several parallel MCMC chains in a Coda compliant
format. Uses R's kernel density function for the purpose. R with package Coda
must be installed. One has the ability to retrieve MAP values w.r.t.
parameter*tree space by specifying option -t.

Usage:
 
  ./GetMarginalMAPEstimate [options] <params> <infile> [<infile2> ...]

where <params> is a comma-separated list of parameter indices (NOT parameter
names!). Normally, parameter L corresponds to index 1.

Options are:
  -t <param>   specifies that one should retrieve MAPs w.r.t. the density of
               the parameter*tree space instead of just parameter space. The
               index of the parameter holding tree IDs must be provided. This
               setting may not be used in conjunction with multiple infiles.
               Only trees with posterior probability > 10% are considered, and
               if such are lacking, the normal 1-D density is utilised.
  -u <params>  specifies that negative sample values may exist for specified
               parameters. Important: default assumption is [0,inf). Should be
               a subset of <params>, and specified similarly.
  -w <bw>      changes the bandwidth selector. Valid choices are:
     <float>      for user specified bandwidth.
     NRD          for Scott's Gaussian "rule-of-thumb".
     NRD0         for Silverman's Gaussian "rule-of-thumb".
     UCV          for unbiased cross-validation.
     BCV          for biased cross-validation.
     SJ_DPI       for Sheather & Jones' "direct plug-in" (default).
     SJ_STE       for Sheather & Jones' "solve-the-equation".
  -k <kern>    changes the kernel. Usually less important than -w. Choices:
     GAUSS        for Gaussian.
     EPAN         for Epanechnikov (default).
     RECT         for rectangular.
     TRI          for triangular.
     BI           for biweight.
     COS          for cosine.
     OPTCOS       for optcosine.
  -n <res>     changes no. of est. points. Use power of 2. Defaults to 512.
===============================================================================
"""


# Starter.
if __name__ == "__main__":    
    
    # Defaults.
    treeParam = -1
    negParams = []
    bw = 'SJ_DPI'
    kernel = 'EPAN'
    n = 512
    
    # Read options.
    try:
        opts, args = getopt.getopt(sys.argv[1:], "ht:u:w:k:n:")
    except getopt.GetoptError:
        print "Invalid option."
        sys.exit(2)
    for opt, optval in opts:
        if opt == "-h":
            usage()
            sys.exit(0)
        elif opt == '-t':
            treeParam = optval
        elif opt == "-u":
            negParams = optval.split(',')
        elif opt == '-w':
            bw = optval
        elif opt == '-k':
            kernel = optval
        elif opt == '-n':
            n = optval
    
    try:
        # Get input.
        if len(args) < 2:
            raise Exception("Wrong number of input arguments. Use -h for help.")
        params = args[0].split(',')
        infiles = args[1:]
                    
        # Create and invoke R script.
        GetMarginalMAPEstimate(infiles, params, negParams, treeParam, bw, kernel, n)
        
    except Exception, e:
        print "Error: %s\n" % e
        sys.exit(2)
    except:
        print "Unexpected error: %s" % sys.exc_info()[0]
        sys.exit(2)
    