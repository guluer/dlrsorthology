#!/usr/bin/perl -w

my $usage = "Usage: restart_beeprates [<options>] <MCMC data>

This program reads MCMC output from a beep_rates analysis and
extracts data needed to restart the mcmc_analysis from the last
iteration in the input analysis. More precisely, it extracts
the options that was used to start the program and the variable
values in the last completed iteration (overrides original start
values). It then output a command for restarting the analysis,
changing the output file name to <original output filename>_2.
Optionally, new values for iterations and output file name can
be given.

Options:
   -i    additional iterations to run (overrides previous value)
   -o    new output file name (default: <old_name>_2.

In:  Name of MCMC output file from beep_rates

Out: A command that can be used to restart the analysis

";



my $program=undef;
my $data=undef;
my $tree=undef;

my %options = (
	       o => undef,
	       i => undef,
	       t => undef,
	       w => undef,
	       s => undef,
	       p => undef,
	       d => undef,
	       l => undef,
	       Sm => undef,
	       Su => undef,
	       Sn => undef,
	       Sa => undef,
	       Em => undef,
	       Ed => undef,
	       Ep => undef,
	       Ef => undef,
	       Er => undef,
	       Gf => undef,
	       Gt => undef,
	       Bf => undef,
	       Bp => undef
	      );


## Read options - options override settings in file
##-------------------------------------------------
if(scalar(@ARGV) > 0)
  {
    while($ARGV[0] =~ m/^-\S+/)
      {
	my $opt = shift @ARGV;
	if ($opt eq '-i')
	  {
	    if(@ARGV < 2)
	      {
		print STDERR "Too few arguments to restart_beep_rates!\n\n";
		print $usage;
		exit 7;
	      }
	    $opt = shift @ARGV;
	    if($opt =~ m/^(\d+)$/) 
	      {
		$options{i} = "-i $1 ";
	      }
	    else
	      {
		print STDERR "-i takes an integer as argument, found $opt\n\n";
		print $usage;
	      }
	  } 
	elsif ($opt eq '-o')
	  {
	    if (@ARGV < 2)
	      {
		print STDERR "Too few arguments to restart beep_rates!\n\n";
		print $usage;
		exit 7;
	      }
	    $opt = shift @ARGV;
	    $options{o} = "-o $opt ";
	  }
	else
	  {
	    print STDERR "Unknown option: '$opt'\n\n";
	    print $usage;
	    exit 8;
	  }
      }
  }




## Get the filename of the mcmc-file to extract from
##--------------------------------------------------
if (scalar(@ARGV) != 1)
  {
    print STDERR $usage;
    exit 1;
  }

my $filename = shift @ARGV;




## Pick out first line and extract options and arguments
##------------------------------------------------------
open(F, "head -1 $filename|") or die "restart_beep_rates: Could not open '$filename'";

while (<F>)
  {
    chomp;
    if (m/(?:^#Running:)\s+([\S\/]+beep_rates)\s+((?:-\S+(?:(?:\s+\{.+\})|(?:\s+\S+))*\s+)+)(\S+)\s+(\S+)\s+in directory.+/)
      {
	$program=$1;
	$tree = $3;
	$data = $4;
	my @parameters=split(/\s+-/, substr($2, 1));
	foreach $i (@parameters)
	  {
	    $i =~ m/(^\s*\w+)\s+.+$/;
	    my $key = $1;
	    if(exists $options{$key})
	      {
		if($options{$key})
		  {
		    next;
		  }
		else
		  {
		    $options{$key} = "-$i";
		    if($i =~ m/^o.+/)
		      {
			$options{$key} = "$options{$key}_2";
		      }
		  }
	      }
	    else
	      {
		print "Key $key is not part of Options \n";
	      }
	  }
      }
  }
close(F);




## Pick out header line and extract parameter names and positions
##---------------------------------------------------------------
open(F, "grep '# L N' $filename|") or die "restart_beep_rates: Could not open '$filename'";

my $mean = undef;
my $variance = undef;
my %rates;
my %times;
my $lambda = undef;
my $mu = undef;
while (<F>)
  {
    chomp;
    if (m/^#\s+(L\s+N\s+.+)/)
      {
	my @parameters = split(/[;\s]+/, $1);
	for($i = 0; $i < scalar(@parameters); $i++)
	  {
	    if($parameters[$i] =~ m/.+mean.+/)
	      {
		$mean = $i;
	      }
	    if($parameters[$i] =~ m/.+variance.+/)
	      {
		$variance = $i;
	      }
	    elsif($parameters[$i] =~ m/.+edgeRate\[(\d+)\].+/)
	      {
		$rates{$i} = $1;
	      }
	    elsif($parameters[$i] =~ m/.+nodeTime\[(\d+)\].+/)
	      {
		$times{$i} = $1;
	      }
	    if($parameters[$i] =~ m/.+birthRate.+/)
	      {
		$lambda = $i;
	      }
	    if($parameters[$i] =~ m/.+deathRate.+/)
	      {
		$mu = $i;
	      }
	  }
      }
  }
close(F);



## Pick out last data line and extract parameter values
# Note that last data line may be incomplete, therefore
# we grep the last two and determine which is the last
# complete line (has the full number of fields).
##------------------------------------------------------
open(F, "grep -v '#' $filename| tail -2|") or die "restart_beep_rates: Could not open '$filename'";

my @indata = undef;
my $size = scalar(@indata);
while (<F>)
  {
    chomp;
    m/(^.*)/;
    # If last line is incomplete, we use second last line
    # if line has a value the current line is the last
    # else (if it is undef) it is the second last line
    if(scalar(@indata) == 1)
      {
	@indata = split(/[;\s]+/, $1);
      }
    else
      {
	my @tmp = split(/[;\s]+/, $1);
	if(scalar(@indata) == scalar(@tmp))
	  {
	    @indata = @tmp;
	  }
      }
  }
close(F);




# Now connect the corresponding headers and data using
# the positions we determined when reading headers
#-----------------------------------------------------
if($mean and $variance)
  {
    # Check for saneness of input
    if($options{Ef})
      {
	print "This is strange, mean and variance is present in output, but -Ef is set\n";
	exit;
      }
    $options{Ep} = "-Ep $indata[$mean] $indata[$variance] ";
  }

if(%rates)
  {
    $options{Er} = "-Er { ";
    foreach $i (sort { $a <=> $b} keys(%rates))
      {
	$options{Er} = "$options{Er}$rates{$i}=$indata[$i] ";
      }
    $options{Er} = "$options{Er}}";
  }

if(%times)
  {
    if($options{Gf})
      {
	print "This is strange, node times are present in output, but -Gf is set\n";
	exit;
      }
    $options{Gt} = "-Gt { ";
    foreach $i (sort { $a <=> $b} keys(%times))
      {
	$options{Gt} = "$options{Gt}$times{$i}=$indata[$i] ";
      }
    $options{Gt} = "$options{Gt}}";
  }

if($lambda and $mu)
  {
    if($options{Bf})
      {
	print "This is strange, birth and death rates are present in output, but -Bf is set\n";
	exit;
      }
    $options{Bp} = "-Bp $indata[$lambda] $indata[$mu] ";
  }




## Finally join everything to the final restarting command
##--------------------------------------------------------
my $command = "$program ";
foreach $i (sort {$a cmp $b} keys(%options))
  {
    if($options{$i})
      {
	$command = "$command$options{$i} ";
      }
  }
$command = "$command$tree $data";

# We don't start the program, but just write the command
print "$command\n";
