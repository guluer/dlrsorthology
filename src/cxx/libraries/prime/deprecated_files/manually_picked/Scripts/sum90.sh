#! /bin/bash

USAGE="test90 <filenamebase> <iters>\n";

##Helper script for test.sh

#export PATH=/Users/bengtsennblad/MCMC/HEAD/mcmc-klubben/src/Scripts:$PATH
export PATH=/afs/pdc.kth.se/home/b/bens/bens00/MCMC_CLUSTER/HEAD/mcmc-klubben/src/Scripts/:$PATH


##change working directory here
#cd /afs/pdc.kth.se/home/b/bens/mcmc-klubben/src/CLUSTER

ITERS=$2  ##Change #replicates here
for ((i=0; i<$ITERS; i++)); do
    echo "Doing "$1$i
    test90_sum_test -b 0.5 $1$i".true" $1$i.mcmc > $1$i.sum
done




