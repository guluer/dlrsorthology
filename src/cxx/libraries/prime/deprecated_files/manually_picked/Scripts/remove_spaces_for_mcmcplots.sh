#!/bin/bash

# Use this to remove spaces between orthology probabilities in mcmc output 
# which offends cmcplots. It is suggested tht you redirect the outout to 
# a new file


if (test -z $1 || test -z $2) then
    echo "Usage: remover_sapces_for_mcmcplots <mcmcfile> <outfile_withoutspaces>";
    exit;
fi


cat $1 | sed s/\ ]/]/g | sed s/\ \\[/[/g|sed s/\;/\;\ /g >$2
