#! /usr/bin/perl -w

my $usage =" Usage: birth_death_params <burn-in> <MCMC data>

 In:  Output from MCMC iterations on the format
            <float>   <iter>    <float>;<float>;<annotated tree>
      The first float is a probability, the second and third are the birth and death rates.
      The annotated tree can be read by ortho_parse (which is also called by this script).

 Out: Expected birth and death parameters

";

my $psum = 0;		# Sum of all posterior probabilities
my $birth = 0;
my $death = 0;

if (@ARGV != 2) {
  print STDERR $usage;
  exit 1;
}

my $burnin = $ARGV[0];
if (not $burnin =~ /^\d+$/) {
  print STDERR "Did you provide a burn-in parameter? Found '$burnin' when
an integer parameter was expected.\n\n", $usage;
  exit 2;
}

my $fname = $ARGV[1];
open(F, "<$fname")
  or die "Could not open '$fname' for reading.\n";

while (<F>) {
  chomp;
  if (m/^([\d\.efg\-\+]+)\s+\d+\s+(\S+);\s*(\S+);.+$/) {
    $n_lines++;
    if ($n_lines > $burnin) {
      my $prob = $1;
      $psum += $prob;
      $birth += $prob * $2;
      $death += $prob * $3;
    }
  } else {
    die "Bad format of MCMC results on line $n_lines: '$_'";
  }
}
close(F);

if ($n_lines < $burnin) {
  print STDERR "The burn-in parameter ($burnin) was too large. Only $n_lines samples found in input.\n";
  exit 3;
}

print $birth / $psum, " = birth rate.\n";
print $death / $psum, " = death rate.\n";
