#include "TreeModel_common.hh"

namespace beep
{

  using namespace std;

  //-------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //-------------------------------------------------------------
  TreeModel_common::TreeModel_common(Tree& T)
    : T(&T)
  {}

  TreeModel_common::~TreeModel_common()
  {}

  TreeModel_common::TreeModel_common(const TreeModel_common& tm)
    : T(tm.T)
  {}

  TreeModel_common&
  TreeModel_common::operator=(const TreeModel_common& tm)
  {
    if(this != &tm)
      {
	T = tm.T;
      }
    return *this;
  }

  //-------------------------------------------------------------
  //
  // Interface 
  //
  //-------------------------------------------------------------
  Tree& 
  TreeModel_common::getTree()
  {
    return *T;
  }

  void 
  TreeModel_common::setTree(const Tree& T_in)
  {
    *T = T_in; 
    //! \todo{This should always be followed by a backup - user's 
    //! responsibility or should it be done here? (overhead)/bens}
    return;
  }

  
}//end namespace beep
