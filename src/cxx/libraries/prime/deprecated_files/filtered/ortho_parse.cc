#include <list>
#include <iostream>

#include "NHXnode.h"

using namespace std;


char *usage = "<filename>\
where <filename> contains a tree on extended Newick format. All nodes are considered \
to be speciations, unless they are annotated by 'D=Y'. For example, in this simple\
tree,\
\
   (a:0.1, b:0.2)[&&NHX:D=Y];\
\
the root node is marked as a duplication.\
";




list<char*> *
print_ortho_pairs(struct NHXnode *t)
{
  if (isLeaf(t))
    {
      return new list<char*>(1, t->name);
    }

  list<char*> *left = print_ortho_pairs(t->left);
  list<char*> *right = print_ortho_pairs(t->right);

  if (!isDuplication(t))
    {
      list<char*>::iterator i1;
      list<char*>::iterator i2;
      
      for (i1 = left->begin(); i1 != left->end(); i1++)
	for (i2 = right->begin(); i2 != right->end(); i2++)
	  {
	    cout << *i1 << "\t" << *i2 << endl;
	  }

    }
  left->splice(left->begin(), *right);
  delete right;
  return left;
}


int
main(int argc, char **argv)
{
  struct NHXnode *t = NULL;

  switch (argc) {
  case 1:
    t = read_tree(NULL);
    break;

  case 2:
    t = read_tree(argv[1]);
    break;

  default:
    cerr << argv[0] << ": " << usage;
    exit(1);
  }
    
  if (t)
    {
      delete print_ortho_pairs(t);
    }
  exit(0);
}

