#include <fstream>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>
#include <time.h>
#include <vector>

#include "BirthDeathMCMC.hh"
#include "ConstRateModel.hh"
#include "DistrReconSeqApprox.hh"
#include "DummyMCMC.hh"
#include "GammaDensity.hh"
#include "GammaMap.hh"
#include "PRNG.hh"
#include "ReconciliationTimeSampler.hh"
#include "SeqIO.hh"
#include "SeqProbApproximator.hh"
#include "SequenceData.hh"
#include "SequenceModel.hh"
#include "SimpleRealMCMC.hh"
#include "SpeciesTreeMCMC.hh"
#include "StrStrMap.hh"
#include "SubstitutionMatrix.hh"
#include "SubstitutionMCMC.hh"
#include "SubstitutionModel.hh"
#include "Tree.hh"
#include "TreeIO.hh"
#include "UniformDensity.hh"

#include "PvmMaster.hh"

void 
usage(char *cmd)
{
  using std::cerr;
  cerr << "Usage: "
       << cmd
       << " <infileprefix> birth_rate death_rate n_samples <model>\n"
       << "\n"
       << "Parameters:\n"
       << "   <infileprefix>     a string, data file is <prefix>.fasta, \n"
       << "                      tree file is <prefix.gene>\n"
       << "   <birth_rate>       a double\n"
       << "   <death_rate>       a double\n"
       << "   <n-samples>        an unsigned, gives the number of Monte\n" 
       << "                      Carlo iterations to perform when esti-\n"
       << "                      mating likelihood of gene tree\n"
       << "   <model>            a string, JC69, UniformAA, JTT, \n"
       << "                      UniformCodon, must match datatype\n"
    ;
    exit(1);
}

int
main (int argc, char **argv) 
{
  using namespace beep;
  using namespace std;
  
  if (argc != 6) 
    {
      usage(argv[0]);
    }

  try
    {
      Real birthRate = atof(argv[2]);
      Real deathRate = atof(argv[3]);
      Real alpha = 1.0;
      Real beta = 1.0;
      unsigned n_samples = atoi(argv[4]);
      string modelname(argv[5]);

      //Get tree and Data
      //---------------------------------------------
      TreeIO io = TreeIO::fromFile(string(argv[1])+".gene");
      Tree G = io.readGeneTree();

      io.setSourceFile(string(argv[1])+".species");
      Tree S = io.readSpeciesTree();

      StrStrMap gs = TreeIO::readGeneSpeciesInfo(string(argv[1])+".gs");
      SequenceData D = SeqIO::readSequences(string(argv[1])+".fasta");

      PRNG rand;
      DummyMCMC dm(rand);

      // Set up rates
      //---------------------------------------------------------
      UniformDensity df(0, 10000, true); //rate ~ U[0,10000]
      Real substRate = 1.00/S.rootToLeafTime(); // reasonable start substRate
      EdgeRateModel dummyRate(df, G); //dummy end-of-line ratemodel
      ConstRateMCMC molClock(df, G, substRate, dummyRate, dm, 0.5, false);
      
      SpeciesTreeMCMC     stm(alpha_param, S, beta);
      BirthDeathMCMC      bdp(stm, S, birthRate, deathRate);

      DistrReconSeqApproximator rsa("client_seqreconapprox",
				    1,   // # clients
				    bdp, // prior
				    bdp, 
				    alpha_param, // Substitution rate
				    stm, // Species tree
				    D,   // Sequence data
				    modelname, // Sequence model
				    G,   // Gene tree
				    gs,
				    100);

      cout << rsa.print();

      // Perform and time the Likelihood calcualtion
      time_t t0 = time(0);
      clock_t ct0 = clock();

      Probability p = rsa.suggestOwnState().stateProb;
      //Probability p = rsa.updateDataProbability();

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cout << "joint prob of tree is : "
	   << p
	   << endl
	   << "Wall time: " << difftime(t1, t0) << " s"
	   << endl
	   << "CPU time: " << double(ct1 - ct0)/CLOCKS_PER_SEC << " s"
	   << endl;
    }
  catch(AnError e)
    {
      cout <<" error\n";
      e.action();
    }
}


