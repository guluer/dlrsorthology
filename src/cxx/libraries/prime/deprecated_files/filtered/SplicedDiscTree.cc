#include <cassert>
#include <cmath>
#include <limits>

#include "AnError.hh"
#include "SplicedDiscTree.hh"

namespace beep
{

using namespace std;

SplicedDiscTree::SplicedDiscTree(Tree& S, unsigned minNoOfIvsPerSlice, Real maxTimestep) :
	m_S(S),
	m_minNoOfIvs(minNoOfIvsPerSlice),
	m_maxTimestep(maxTimestep),
	m_times(),
	m_splice2timestep(),
	m_splice2edges(),
	m_node2spliceAbove(S),
	m_node2spliceBelow(S)
{
	if (S.getTopTime() <= 0)
	{
		throw AnError("Cannot create spliced tree without top time edge.");
	}
	if (minNoOfIvsPerSlice == 0)
	{
		throw AnError("Cannot create spliced tree with 0 intervals per splice.");
	}
	unsigned noOfSplices = (S.getNumberOfNodes() + 1) / 2;
	m_times.reserve(noOfSplices);
	m_splice2timestep.reserve(noOfSplices);
	m_splice2edges.reserve(noOfSplices);
	update();
}


SplicedDiscTree::~SplicedDiscTree()
{
}


void
SplicedDiscTree::update()
{
	clearValues();
	
	// Lowermost splice contains all leaf edges. Use these as starting point.
	NodeList q = NodeList();
	for (Tree::iterator it = m_S.begin(); it != m_S.end(); ++it)
	{
		const Node* n = *it;
		if (n->isLeaf())
		{
			q.push_back(n);
			m_node2spliceAbove[n] = 0;  // Splice index of all leaves.
			m_node2spliceBelow[n] = 0;  // Undefined.
		}
	}
	
	const Node* nLo = q.front();            // Lower node of splice.
	const Node* nUp;                        // Upper node of splice.
	Real tLo = m_S.getTime(*(*q.begin()));  // Lower time of splice.
	Real tUp;                               // Upper time of splice.
		
	// Find splices.
	int i = 0;
	while (q.size() > 1)
	{
		nUp = NULL;
		tUp = numeric_limits<Real>::max();
		for (NodeList::iterator it = q.begin(); it != q.end(); ++it)
		{
			const Node* n = (*it)->getParent();
			if (m_S.getTime(*n) < tUp)
			{
				nUp = n;
				tUp = m_S.getTime(*n);
			}
		}
		
		// Make sure there is a at least a certain time span for each splice.
		if (tUp + MIN_SPLICE_DELTA < tLo) { tUp = tLo + MIN_SPLICE_DELTA; }
		
		// Add edges and time borders of current splice.
		m_times.push_back(vector<Real>());
		m_splice2timestep.push_back(createTimes(m_times.back(), tLo, tUp));
		m_splice2edges.push_back(q);
		m_node2spliceAbove[nLo] = i;
		m_node2spliceBelow[nUp] = i;
		
		// Update nodes for next set.
		q.remove(nUp->getLeftChild());
		q.remove(nUp->getRightChild());
		q.push_back(nUp);
		nLo = nUp;
		tLo = tUp;
		i++;
	}
	
	// The root should remain.
	assert(q.size() == 1 && nLo == q.front());
	assert(nLo == m_S.getRootNode());
	
	// Add root edge if not of zero length.
	tUp = m_S.getTopToLeafTime();
	if (tUp + MIN_SPLICE_DELTA < tLo) { tUp = tLo + MIN_SPLICE_DELTA; } 
	m_times.push_back(vector<Real>());
	m_splice2timestep.push_back(createTimes(m_times.back(), tLo, tUp));
	m_splice2edges.push_back(q);
	m_node2spliceAbove[nLo] = i;    // Actually undefined.
}


void
SplicedDiscTree::clearValues()
{
	m_times.clear();
	m_splice2timestep.clear();
	m_splice2edges.clear();
	m_node2spliceAbove.clearValues();
	m_node2spliceBelow.clearValues();
}


Real
SplicedDiscTree::createTimes(vector<Real>& v, Real tLo, Real tUp)
{
	assert(tLo < tUp);
	
	// If m_timestep<=0, just divide every splice in equally many parts.
	// Notice that both endpoints are appended.
	unsigned noOfIvs = (m_maxTimestep <= 0.0) ? m_minNoOfIvs :
			max(m_minNoOfIvs, static_cast<unsigned>(ceil((tUp - tLo) / m_maxTimestep  - 1e-6)));
	Real timestep = (tUp - tLo) / noOfIvs;
	for (unsigned i = 0; i <= noOfIvs; ++i)
	{
		v.push_back(tLo + i * timestep);
	}
	return timestep;
}


Real
SplicedDiscTree::getTopTime() const
{
	return (m_times.back().back() - m_times.back().front());
}


Real
SplicedDiscTree::getTopToLeafTime() const
{
	return m_times.back().back();
}


Real
SplicedDiscTree::getRootToLeafTime() const
{
	return m_times.back().front();
}


Real
SplicedDiscTree::getTime(const Node* node) const
{
	return m_times[m_node2spliceAbove[node]].front();
}


Real
SplicedDiscTree::getTime(unsigned spliceNo, unsigned index) const
{
	return m_times[spliceNo][index];
}


const vector<Real>&
SplicedDiscTree::getTimes(unsigned spliceNo) const
{
	return m_times[spliceNo];
}


Real
SplicedDiscTree::getTimestep(unsigned spliceNo) const
{
	return m_splice2timestep[spliceNo];
}


unsigned
SplicedDiscTree::getSpliceAbove(const Node* node) const
{
	return m_node2spliceAbove[node];
}


unsigned
SplicedDiscTree::getSpliceBelow(const Node* node) const
{
	return m_node2spliceBelow[node];
}


const SplicedDiscTree::NodeList&
SplicedDiscTree::getEdges(unsigned spliceNo) const
{
	return m_splice2edges[spliceNo];
}


unsigned
SplicedDiscTree::getNoOfSplices() const
{
	return m_times.size();
}

unsigned
SplicedDiscTree::getNoOfPoints(unsigned spliceNo) const
{
	return m_times[spliceNo].size();
}


unsigned
SplicedDiscTree::getTotalNoOfPoints(unsigned spliceNo) const
{
	return (m_splice2edges[spliceNo].size() * m_times[spliceNo].size());
}


unsigned
SplicedDiscTree::getNoOfPointsOnEdge(const Node* node, bool countOverlaps) const
{
	if (node->isRoot()) { return m_times.back().size(); }
	unsigned begin = m_node2spliceAbove[node];
	unsigned end = m_node2spliceBelow[node->getParent()];
	unsigned noOfPts = 0;
	for (unsigned i = begin; i <= end; ++i)
	{
		noOfPts += m_times[i].size();
	}
	if (!countOverlaps) { noOfPts -= end-begin; }
	return noOfPts;
}


unsigned
SplicedDiscTree::getTotalNoOfPoints() const
{
	unsigned noOfPts = 0;
	for (unsigned i = 0; i < m_times.size(); ++i)
	{
		noOfPts += m_times[i].size();
	}
	return noOfPts;
}


Tree&
SplicedDiscTree::getOrigTree() const
{
	return m_S;
}


const Node*
SplicedDiscTree::getOrigRootNode() const
{
	return m_S.getRootNode();
}
	

const Node*
SplicedDiscTree::getOrigNode(unsigned index) const
{
	return m_S.getNode(index);
}


void
SplicedDiscTree::debugInfo() const
{
	cerr << "# ================================= SPLICEDDISCTREE ===================================" << endl
		<< "# Discretization type: ";
	if (m_maxTimestep <= 0)
	{
		cerr << "Every splice comprises " << m_minNoOfIvs << " intervals" << endl;
	}
	else
	{
		cerr << "Approx. timestep is " << m_maxTimestep
			<< ", min no of intervals per splice is " << m_minNoOfIvs << endl;
	}
	unsigned sz = getNoOfSplices();
	cerr << "# No. of splices: " << sz
		<< ", no. of nodes: " << m_S.getNumberOfNodes()
		<< ", total no. of points: " << getTotalNoOfPoints()
		<< endl;
	cerr << "# Top time: " << getTopTime() << ", top-to-leaf time: " << getTopToLeafTime()
		<< ", root-to-leaf time: " << getRootToLeafTime() << endl;
	cerr << "# Splice:\tNo. of pts per section:\tTimestep:\tTime span:\tEdges:" << endl;
	for (unsigned i=0; i<sz; ++i)
	{
		const vector<Real>& v = getTimes(i);
		cerr
			<< "# " << i << '\t'
			<< getNoOfPoints(i) << '\t'
			<< getTimestep(i) << '\t'
			<< v.front() << "..." << v.back() << '\t'
			<< '{';
		NodeList::const_iterator it;
		for (it=m_splice2edges[i].begin(); it!=m_splice2edges[i].end(); ++it)
		{
			const Node* n = *it;
			cerr << n->getNumber() << ',';
		}
		cerr << '}' << endl;
	}
	
	// Prints node-based info.
	//for (unsigned i=0; i<m_S.getNumberOfNodes(); ++i)
	//{
	//	const Node* n = m_S.getNode(i);
	//	cerr
	//		<< "# Node: "
	//		<< i
	//		<< ", time: " << getTime(n)
	//		<< ", splice above: " << getSpliceAbove(n)
	//		<< ", splice below: " << getSpliceBelow(n)
	//		<< ", total non-coinciding pts on edge: " << getNoOfPointsOnEdge(n, false)
	//		<< endl;
	//}
	
	cerr << "# =====================================================================================" << endl;
}

} // end namespace beep.
