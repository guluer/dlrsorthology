#include <sstream>

#include "MultiFamReconMCMC.hh"
#include "TreeIO.hh"

namespace beep {

  MultiFamReconMCMC::MultiFamReconMCMC(MCMCModel &prior,
				       std::vector<Tree> &GV,
				       StrStrMap         &gs,
				       BirthDeathProbs   &kendall,
				       bool reroot_on)
    : StdMCMCModel(prior, 
		   (reroot_on ? GV.size() : 0),
		   (reroot_on ? (std::min(1.0, 10.0/GV.size())) : 1.0)),
      MultiFamReconModel(GV, gs, kendall),
      reroot_on(reroot_on),
      n_trees(GV.size())
  {
    
  }

  MultiFamReconMCMC::MultiFamReconMCMC(const MultiFamReconMCMC &M)
    : StdMCMCModel(M),
      MultiFamReconModel(M)
  {

  }

  MultiFamReconMCMC::~MultiFamReconMCMC()
  {
  }


  //---------------------------------------------------------------------
  //
  // Implementing the StdMCMCModel interface
  //
  // There are no parameters in this class since it acts as a
  // front-end to BirthDeathMCMC really. Therefore, the next three
  // methods should never be invoked.
  //
  MCMCObject
  MultiFamReconMCMC::suggestOwnState() // Should never be invoked!
  {
    MCMCObject mo(1.0, 1.0);

    if (reroot_on) 
      {
	PRNG R;
	ti = R.genrand_modulo(n_trees);
	Tree &G = RMV[ti].getGTree();
	oldG = G;
	mrGardener.reRoot(G);
	RMV[ti].update();

	mo.stateProb = calculateDataProbability();
      }
    return mo;//TODO: add reasonable default return! /bens
  }

  void        
  MultiFamReconMCMC::commitOwnState()
  {
  }

  void        
  MultiFamReconMCMC::discardOwnState()
  {
    if (reroot_on)
      {
	RMV[ti].getGTree() = oldG;
	RMV[ti].update();
      }
  }

  std::string
  MultiFamReconMCMC::ownStrRep() const
  {
    if (reroot_on)
      {
	TreeIO io;
	string s="";
	for (unsigned int i=0; i<n_trees; i++)
	  {
	    s += io.newickString(RMV[i].getGTree()) + "; ";
	  }
	return s;
      }
    else
      {
	return "";
      }
  }

  std::string
  MultiFamReconMCMC::ownHeader() const
  {
    if (reroot_on)
      {
	std::ostringstream oss;
	for (unsigned i=0; i<n_trees; i++)
	  {
	    string tname = RMV[i].getGTree().getName();
	    if (tname.length() > 0)
	      {
		oss << tname << "(tree); ";
	      } 
	    else
	      {
		oss << "G" << i << "(tree); ";
	      }
	  }
	return oss.str();
      }
    else
      {
	return "";
      }
  }


  Probability
  MultiFamReconMCMC::updateDataProbability()
  {
    update();
    return calculateDataProbability();
  }

} // Namespace beep
