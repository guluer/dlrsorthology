#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include "AnError.hh"
#include "PvmError.hh"
#include "PvmMaster.hh"

extern "C" {
#include "pvm3.h"
}

using namespace beep;
using namespace PvmBeep;

PvmMaster::PvmMaster()
  : clientProg(""),
    nClients(0),
    nAllocated(0)
{
}
    
PvmMaster::PvmMaster(const PvmMaster &m)
  : clientProg(m.clientProg),
    nClients(m.nClients),
    nAllocated(m.nAllocated),
    my_tid(m.my_tid)
{
  for (int i=0; i<nAllocated; i++) 
    {
      client_tid[i] = m.client_tid[i];
      wait_times[i] = m.wait_times[i];
      n_jobs[i] = m.n_jobs[i];
    }
}

PvmMaster::PvmMaster(const std::string prog, int n)
  : clientProg(prog),
    nClients(n)
{
  // Some sanity controls
  assert(n >= 0);
  assert(prog.length() > 0);

  // ... and some trivial error traps:
  struct stat file_data;
  if (stat(prog.c_str(), &file_data) != 0)
    {
      throw AnError("PvmMaster Could not access client program", prog);
    }
  
  // Convenience: Start as many clients processes as there are hosts computers
  if (nClients == 0) 
    {
      nClients = numHosts();
      if (nClients == 0)
	{
	  throw AnError("No hosts in PVM!", 1);
	}
    }

  // We have (1) the  client program to start, (2) no argument, (3) it can
  // be started on any host, (4) no host specification, (5) with a certain
  // number of clients, and (6) a vector to put client task ids in.
  char *s = strdup(clientProg.c_str());
  int info = pvm_spawn(s, NULL, 
		       PvmTaskDefault, NULL,
		       nClients, client_tid);
  delete s;
  nAllocated = info;
  if (nAllocated == 0) 
    {
      throw AnError("PVM: No clients were allocated!", 1);
    }

  for (int i=0; i<nAllocated; i++) 
    {
      wait_times[i] = 0;
      n_jobs[i] = 0;
    }

  // Request a message if clients would die
  pvm_notify(PvmTaskExit, PvmBeep::ProcessIsDead, nAllocated, client_tid);
  pvm_notify(PvmHostDelete, PvmBeep::ProcessIsDead, nAllocated, client_tid);

  // The return value is the number of clients _actually_ started.
  if (info < 0) {
    throw AnError("There is a PVM error.", 1);
  } else if (info == 0) {
    throw AnError("Could not start any PVM client programs!", 1);
  } else {
    if (nAllocated < nClients) {
      WARNING3("Could not allocate more than ", info, " PVM client programs");
    }
  }
}



//
// Destructor has to bring the client process down gracefully
//
PvmMaster::~PvmMaster()
{
				// Tell all clients to stop!
  for (int i=0; i<nAllocated; i++)
    {
      pvm_kill(client_tid[i]);
    }

  pvm_exit();			// We leave the PVM
}


//! \return The number of allocated PVM processes
int
PvmMaster::getNumClients() const
{
  return nAllocated;
}


void
PvmMaster::multiCast(enum MessageTypes mt)
{
#ifdef DISTR_DEBUG
  std::cerr << "Multicasting type " << mt << std::endl;
#endif
  int info = pvm_mcast(client_tid, nAllocated, static_cast<int>(mt));
  if (info < 0) 
    {
      throw PvmError(info);
    }
}


void
PvmMaster::sendToChain(int which_chain, enum MessageTypes mt)
{
  send(client_tid[which_chain-1], mt);
}

void
PvmMaster::swapClientTid(const int index1, const int index2)
{
  int tmp = client_tid[index1-1];

  client_tid[index1-1] = client_tid[index2-1];
  client_tid[index2-1] = tmp;
}


//! \return The PVM process (task) ID of the client with this index.
int
PvmMaster::getClientTID(int index)
{
  return client_tid[index-1];
}

//! \return The PVM process (task) id
int
PvmMaster::getMasterTID()
{
  return my_tid;
}

//
// Statistics
//
void 
PvmMaster::registerWaitTime(int sender, unsigned waiting)
{
  if (waiting == 0) 
    {
      return;			// No need for update!
    }
  else 
    {
      for (int i=0; i<nAllocated; i++) 
	{
	  if (client_tid[i] == sender) 
	    {
	      wait_times[i] = waiting; // Client sends an accumulated time
	      return;
	    }
	}
      PROGRAMMING_ERROR("No match of sender id with list of spawned processes!");
    }
}

void
PvmMaster::registerCompletion(int sender)
{
  //  std::cerr << "Complete jobs: " << sender << std::endl;
  for (int i=0; i<nAllocated; i++) 
    {
      if (client_tid[i] == sender) 
	{
	  n_jobs[i]++; // Number of times a batch has been received
	  return;
	}
    }
  PROGRAMMING_ERROR("No match of sender id with list of spawned processes!");
}

void
PvmMaster::writeStatistics()
{
  std::cout << "# Master wait: " << waitTime()
	    << "\n# Client   Task ID   Waiting   Jobs" << std::endl;
  for (int i=0; i<nAllocated; i++)
    {
      std::cout << "#  ";
      std::cout.width(3);
      std::cout << i;

      std::cout.width(12);
      std::cout << client_tid[i];
      std::cout.width(8);
      std::cout << wait_times[i];
      std::cout.width(9);
      std::cout << n_jobs[i] << std::endl;
    }
}
