#include "MultiFamReconModel.hh"

#include "AnError.hh"

using namespace std;

namespace beep {

  MultiFamReconModel::MultiFamReconModel(std::vector<Tree> &GV, StrStrMap &gs, BirthDeathProbs &nee)
  {
    RMV.reserve(GV.size());

    for (vector<Tree>::iterator i = GV.begin(); i != GV.end(); i++)
      {
	ReconciliationModel RM(*i, gs, nee);
	RMV.push_back(RM);
      }
  }

  //
  // Copy constructor
  //
  MultiFamReconModel::MultiFamReconModel(const MultiFamReconModel &M)
    : RMV(M.RMV)
  {
  
  }


  MultiFamReconModel::~MultiFamReconModel()
  {

  }


  MultiFamReconModel&
  MultiFamReconModel::operator=(const MultiFamReconModel &M)
  {
    if (this != &M)
      {
	RMV = M.RMV;
      }

    return *this;
  }


  Probability
  MultiFamReconModel::calculateDataProbability()
  {
    Probability p(1);

    for (vector<ReconciliationModel>::iterator i = RMV.begin(); 
	 i != RMV.end(); i++)
      {
	p *= i->calculateDataProbability();
      }

    return p;
  }

  void
  MultiFamReconModel::update()
  {
    for (vector<ReconciliationModel>::iterator i = RMV.begin(); 
	 i != RMV.end(); i++)
      {
	i->update();
      }
  }



  //
  // This is taken almost verbatim from ReconciliationModel::chooseStartingRates.
  //
  void
  MultiFamReconModel::chooseStartingRates()
  {
    BirthDeathProbs nee = RMV[0].getBirthDeathProbs();
    Tree S = RMV[0].getSTree();
    Real height = S.rootToLeafTime();

    if (height <= 0.0) 
      {
	throw AnError("Height of species tree is not a positive value!", 17);
      }

    Real rootTime = S.getRootNode()->getTime();
    if (rootTime == 0.0) {
      S.getRootNode()->setTime(height / 10.0); // A bit arbitrary, but OK.
      height *= 1.1;
    }

    Real rate = 0.001 / height;
    nee.setRates(rate, rate);
    Probability best_likelihood = calculateDataProbability();
    Real best_rate = rate;

    // The silly 0.9999 factor is there to remove the risk of having a 
    // precision problem. The stupid MAX_INTENSITY should cancel out in 
    // the sanity test performed in nee.setRates, but in some cases, we
    // are off by 10^-10 or something and the test fails for no good reason.
    for (Real factor = 0.999999*MAX_INTENSITY; factor > 0.001; factor /= 2)
      {
	rate = factor / height;
	nee.setRates(rate, rate);
	Probability L = calculateDataProbability();
	if (L > best_likelihood) 
	  {
	    best_likelihood = L;
	    best_rate = rate;
	  }
      }

    nee.setRates(best_rate, best_rate);
  }

}
