#ifndef PVMCOMMUNICATION_HH
#define PVMCOMMUNICATION_HH

#include <string>
#include <fstream>

#include "Beep.hh"
#include "PvmBeep.hh"

#include "OrthologyMatrix.hh"
#include "Probability.hh"
#include "Tree.hh"
#include "SequenceData.hh"
#include "StrStrMap.hh"

using namespace beep;

namespace beep {
class PvmCommunication 
{
public:
  PvmCommunication();
  ~PvmCommunication();

  //
  // Wait until a message is received
  //
  // Optional argument instructs to only look for certain messages.
  // The type of the messages is always returned, and is never 
  // 'NoMessage'.
  //
  enum PvmBeep::MessageTypes waitForMessage(enum PvmBeep::MessageTypes mt=PvmBeep::AnyMessage);

  enum PvmBeep::MessageTypes waitForAMessage(int TID, enum PvmBeep::MessageTypes mt=PvmBeep::AnyMessage);

  
  //
  // Check for new messages: This is really a non-blocking recieve.
  // 
  // Optional argument instructs to only look for certain messages.
  // The type of the messages is always returned, and if no message
  // is found 'NoMessage' is returned.
  //
  enum PvmBeep::MessageTypes checkMessages(enum PvmBeep::MessageTypes mt=PvmBeep::AnyMessage);

  enum PvmBeep::MessageTypes checkType() const;

  // Who send you the last received message? Value -1 if no message in buffer.
  int messageSender() const;

  //
  // Info
  //
  int numHosts() const;		// Return the number of host computers in the PVM

  // 
  // Receiving and creating messages
  //
  // Each data type will have a pair of receive/createMessage methods. In addition, some
  // complex data will have a packMessage method (e.g. Probability) which can be reused 
  // when creating heterogenous and even more complex messages. 
  //

  //
  // Receive a value
  //
  void receive(int &i);
  void receive(int *i, int length = 1);

  void receive(unsigned &u);
  void receive(unsigned *u, int length = 1);

  void receive(Real &x);
  void receive(Real *x, int length = 1);

  void receive(beep::Probability &p);
  void receive(beep::Tree &T, enum PvmBeep::MessageTypes mt=PvmBeep::GeneTree);
  void receive(OrthologyMatrix &OM); //! receives an orthologymatrix
  void receive(SequenceData &sd);
  void receive(StrStrMap &gs);
  void receive(std::string &st);
  void receive(beep::Probability &p, Real &temp, int &chain_id);
  void receive(beep::Probability &p, int &iter, std::string &state);

  //
  // Compose a new message
  //
  void createMessage();		// Empty message: Only the message type is valuable
  void createMessage(const int i);
  void createMessage(const int *i, const int length = 1);
  void createMessage(const unsigned u);
  void createMessage(const unsigned *u, const int length = 1);
  void createMessage(const Real x);
  void createMessage(const Real *x, const int length = 1);
  void createMessage(const Probability &p);
  void createMessage(const Tree &T);
  void createMessage(const SequenceData &sd);
  void createMessage(const StrStrMap &gs);
  void createMessage(const std::string &st);
  void createMessage(const OrthologyMatrix &OM);

  // 
  // Packing data
  //
  void packMessage(const Probability &p);
  void packMessage(const OrthologyMatrix &OM);

  //
  // Complex messages
  //
  // These should probably end up in a subclass. They are due to bad
  // planning by me (arve), since I had forgotten that the
  // createMessages above make atomic messages and a process
  // communicating with several others do not want data split up in
  // several messages. That makes it harder to keep data frmo one
  // process together.
  //
  void createMessage(const beep::Probability &p, const Real temp, const int chain_id);
  void createMessage(const beep::Probability &p, const int &iter, const std::string &state);
  void createMessage(int batch_id, int size, const beep::Probability &p, const OrthologyMatrix &OM, int waiting);

  //
  // Send a value
  //
  void send(int addressee, enum PvmBeep::MessageTypes mt);

#ifdef PIGS_FLY
// 
// Here comes some stuff that we probably won't need. Lotta used it
// during development. Should be deleted when we have a somewhat
// stable system!
//
  // Synchronizing:
  void join(const std::string &group);
  void wait(const std::string & group, const int members);
#endif

  //
  // General
  //
  int taskID();

  // How many seconds have this process been waiting for messages?
  unsigned waitTime() const;	

  //--------------------------------------------------------------------------------
  // Utilities
protected:
  void initSend();
  
  void tic();
  void toc();
  
  //--------------------------------------------------------------------------------
  // Attributes
  //
private:
  // The following attributes are related to pvm_bufinfo, pvm_recv, etc
  int buf;			// PVM buffer identifier
  int buf_len;			// Length of current message buffer
  enum PvmBeep::MessageTypes buf_type; // Type of last message
  int msg_tid;			// Sender task ID
  unsigned time_point;		// For measuring waiting time, see tic().
  unsigned wait_time;		// Counts the number of seconds

#ifdef DISTR_DEBUG
protected:
  std::ofstream debug_out;

#endif
public:
  void debug_msg(const std::string &s); // For debugging only! Only doing something when DISTR_DEBUG is defined.

};
}
#endif
