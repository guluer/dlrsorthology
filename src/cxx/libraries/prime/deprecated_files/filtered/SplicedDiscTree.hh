#ifndef SPLICEDDISCTREE_HH
#define SPLICEDDISCTREE_HH

#include <list>
#include <string>
#include <utility>
#include <vector>

#include "BeepVector.hh"
#include "Node.hh"
#include "Tree.hh"

namespace beep
{

/**
 * TBD.
 * 
 * Splices are accessed using numbers, 0 at leaves and so on. Such splice
 * identifiers can be retrieved using the nodes defining their endpoints.
 * 
 * 
 * 
 * Since, the discretizaton might slightly alter time values, make sure to access all node times,
 * top time, etc. from this class rather than underlying tree or the nodes themselves.
 */
class SplicedDiscTree
{
	
public:
	
	/**
	 * Minimum time span for a splice. If smaller than this, one of the "time-colliding"
	 * nodes is moved slightly. Should work for multiple colliding nodes too.
	 */
	static const Real MIN_SPLICE_DELTA = 0.0001;
	
	/**
	 * Convenience typedef for list of nodes or edges.
	 */
	typedef std::list<const Node*> NodeList;
	
	/**
	 * Constructor. Specify the minimum number of discretization intervals per
	 * per splice, and an approximate (never exceeded) timestep between points.
	 * Set the timestep to <=0 to create the same number of points on each splice.
	 * @param S the underlying tree.
	 * @param minNoOfIvsPerSlice the minimum number of discretization intervals
	 *        per splice, or exact number if maxTimestep<=0.
	 * @param maxTimestep the approximate desired timestep. Never exceeded.
	 *        Set to <=0 for minNoOfIvsPerSlice intervals on each splice.
	 */
	SplicedDiscTree(Tree& S, unsigned minNoOfIvsPerSlice, Real maxTimestep=0.0);
	
	/**
	 * Destructor.
	 */
	~SplicedDiscTree();
	
	/**
	 * Updates the discretization based on the underlying tree.
	 */
	void update();
	
	/**
	 * Returns the discretized top time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized top time.
	 */
	Real getTopTime() const;
	
	/**
	 * Returns the discretized top-to-leaf-time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized top-to-leaf time.
	 */
	Real getTopToLeafTime() const;
	
	/**
	 * Returns the discretized root-to-leaf-time. Note: Don't use the tree's own value
	 * directly, since it may not be exactly the same as the discretized value.
	 * @return the discretized root-to-leaf time.
	 */
	Real getRootToLeafTime() const;
	
	/**
	 * Returns the discretized time of a node. Note: Don't use the node's own time
	 * directly, since it may not be exactly the same as the discretized value.
	 * @param node the node.
	 * @return the discretized time of the node.
	 */
	Real getTime(const Node* node) const;
	
	/**
	 * Returns the time of a point, or more specifically the corresponding discretization
	 * "grid line".
	 * @param spliceNo the splice identifier.
	 * @param index the index of point on the splice.
	 * @return the time.
	 */
	Real getTime(unsigned spliceNo, unsigned index) const;
	
	/**
	 * Returns the point times of a certain splice. Please note that both ends are
	 * included, implying that end points coincide with endpoints of edges above/below.
	 * @param spliceNo the splice identifier.
	 * @return the times of the splice.
	 */
	const std::vector<Real>& getTimes(unsigned spliceNo) const;
	
	/**
	 * Returns the timestep between points in a splice.
	 * @param spliceNo the splice number.
	 * @return the timestep.
	 */
	Real getTimestep(unsigned spliceNo) const;
	
	/**
	 * Returns the splice number above a specified node.
	 * Undefined for root with no top time edge.
	 * @param node the lower end of the splice.
	 * @return the splice number.
	 */
	unsigned getSpliceAbove(const Node* node) const;

	/**
	 * Returns the splice number below a specified node.
	 * Undefined for leaves.
	 * @param node the upper end of the splice.
	 * @return the splice number.
	 */
	unsigned getSpliceBelow(const Node* node) const;
	
	/**
	 * Returns a list of edges which a splice covers.
	 * @param spliceNo the splice identifier.
	 * @return a list of edges defined through their lower nodes.
	 */
	const NodeList& getEdges(unsigned spliceNo) const;
	
	/**
	 * Returns the total number of splices.
	 * @return the number of splices. 
	 */
	unsigned getNoOfSplices() const;
	
	/**
	 * Returns the number of points per intersecting
	 * edge of a certain splice, both end points included.
	 * @param spliceNo the splice number.
	 * @return the number of points for each edge intersecting the splice.
	 */
	unsigned getNoOfPoints(unsigned spliceNo) const;
	
	/**
	 * Returns the total number of points in a slice, all endpoints included.
	 * @param spliceNo the splice number.
	 * @return the number of points in the slice, all endpoints included.
	 */
	unsigned getTotalNoOfPoints(unsigned spliceNo) const;
	
	/**
	 * Returns the total number of points of an edge (over all
	 * covered splices), end point excluded. Undefined for root
	 * with no top time edge.
	 * @param node the lower node of the edge.
	 * @param countOverlaps if true, counts coinciding slice
	 *        endpoints as separate points.
	 * @return the number of points.
	 */
	unsigned getNoOfPointsOnEdge(const Node* node, bool countOverlaps) const;
	
	/**
	 * Returns the total number of points in the entire tree, all
	 * endpoints included, even coinciding ones.
	 * @return overall number of points.
	 */
	unsigned getTotalNoOfPoints() const;
	
	/**
	 * Returns the underlying tree.
	 * @return the tree.
	 */
	Tree& getOrigTree() const;
	
	/**
	 * Returns the underlying tree's root node.
	 * @return the root node.
	 */
	const Node* getOrigRootNode() const;
	
	/**
	 * Returns the underlying tree's node with the specified index.
	 * @return the node.
	 */
	const Node* getOrigNode(unsigned index) const;
	
	/**
	 * Prints some info to stderr.
	 */
	void debugInfo() const;
	
private:
	
	/**
	 * Clears all internal vectors.
	 */
	void clearValues();
	
	/**
	 * Appends discretized times to a vector according to current settings.
	 * Both endpoints are included.
	 * @param v the vector to which times are appended.
	 * @param tLo the lower time value boundary.
	 * @param tUp the upper time value boundary.
	 * @return the timestep used.
	 */
	Real createTimes(std::vector<Real>& v, Real tLo, Real tUp);
	
public:
	
private:
	
	/** The underlying original Tree. */
	Tree& m_S;
	
	/**
	 * The minimum number of discretization intervals per splice.
	 * If maxTimestep<=0, this is the exact number of intervals of each splice.
	 */
	unsigned m_minNoOfIvs;
	
	/**
	 * The approximate (but never exceeded) timestep between discretization points.
	 * Set to <=0 to divide every splice in equally many parts (see minNoOfIvs). 
	 */
	Real m_maxTimestep;
	
	/** For each splice, the time values, both endpoints included. */
	std::vector< std::vector<Real> > m_times;
	
	/** For each splice, the timestep between points on the splice. */
	std::vector<Real> m_splice2timestep;
	
	/** For each splice, the intersecting edges. */
	std::vector<NodeList> m_splice2edges;
	
	/** For each node, the index of the splice which the node is the lower end of. */
	UnsignedVector m_node2spliceAbove;
	
	/** For each node, the index of the splice which the node is the upper end of. */
	UnsignedVector m_node2spliceBelow;
};

} // end namespace beep

#endif // SPLICEDDISCTREE_HH

