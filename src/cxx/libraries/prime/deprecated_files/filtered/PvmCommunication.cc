#include <fstream>
#include <ctime>
#include <unistd.h>

using namespace std;

#include "PvmCommunication.hh"
#include "PvmDeadClientError.hh"
#include "PvmError.hh"

using namespace PvmBeep;

#include "SequenceData.hh"
#include "SequenceType.hh"
#include "TreeIO.hh"

extern "C" {
#include <pvm3.h>
}

//
// Convenience macro: The code gets unreadable if we try 
// to catch all return codes from PVM.
//
#define PVMCALL(CALL) {int returncode = CALL; if (returncode < 0) {throw PvmError(returncode);}}

// Debug tricks:
#define WRITEVAR(VAR) {cerr << "" #VAR "" << ": " << VAR << endl; }

#ifdef DISTR_DEBUG
#define DISMSG(A) { debug_out << A << endl; }
//! Provide some debug support
void
PvmCommunication::debug_msg(const std::string &s)
{
  debug_out << s << endl;
}
#else
#define DISMSG(A)
void
PvmCommunication::debug_msg(const std::string &s)
{
  // Empty!
}
#endif

PvmCommunication::PvmCommunication()
  : buf(0),
    buf_len(0),
    buf_type(NoMessage),
    msg_tid(-1),
    time_point(0),
    wait_time(0)
{
#ifdef DISTR_DEBUG
  char buf[100];
  int res = gethostname(buf, 99);
  ostringstream os;

  os << "/Users/bengtsennblad/MCMC/HEAD/mcmc-klubben/src/debug_" //"/afs/nada.kth.se/home/theory/arve/Slasken/debug_"  
     << buf 
     << "_"
     << taskID();
  debug_out.open(os.str().c_str());

#endif    

}


PvmCommunication::~PvmCommunication()
{
#ifdef DISTR_DEBUG
  debug_out << "Done." << endl;
  debug_out.close();
#endif
  pvm_exit();			// Very important!
}


//! \return The task ID from of the process as given by PVM.
int
PvmCommunication::taskID()
{
  return pvm_mytid();
}


//! \return Number of hosts in the PVM
int
PvmCommunication::numHosts() const
{
  int nhost, narch;
  struct pvmhostinfo *hostp;

  PVMCALL(pvm_config(&nhost, &narch, &hostp));

  return nhost;
}

//! Wait, in blocked mode, for a message from the PVM system
//!
//! Checks for errors in PVM is performed, in which case exceptions are thrown.
//! For pure PVM errors, a PvmError object is created an thrown.
//! If a ProcessIsDead message is received, which is not really a
//! PVM error, a PvmDeadClientError is thrown.
//! 
//! \param mt Not used for some reason! Bug?  
//!
//! \return The message type of the recieved message.
enum MessageTypes
PvmCommunication::waitForMessage(enum MessageTypes mt) // TODO: Why is there a parameter here? It is not used /arve
{
  tic();			// Start timing...
  int info = pvm_recv(-1, -1);

  if (info < 0)
    {
      throw PvmError(info);
      toc();			// ... and stop timing!
      return NoMessage;
    }
  else 
    {
      buf = info;
      int msg_type;
      info = pvm_bufinfo(buf, &buf_len, &msg_type, &msg_tid);
      DISMSG("* PvmComm Bufinfo: Length=" << buf_len << ", type=" << msg_type);

      if (msg_type == AnErrorString) 
	{
	  string estr;
	  receive(estr);
	  throw AnError(estr);
	}
      if (msg_type == ProcessIsDead)
	{
	  int task;
	  pvm_upkint(&task, 1, 1);
	  throw PvmDeadClientError(task); // Throw a non-vital error
	}

      buf_type=MessageTypes(msg_type);
      toc();			// ... and stop timing here too!
      if (info < 0) {
	throw PvmError(info);
	return NoMessage;
      } else {
	return MessageTypes(msg_type);
      }
    }

}

//! Wait, in blocked mode, for a particular message from a particular host
//! in the PVM system.
//! 
//! Notice that you can choose to accept \e any message and \e any host by
//! setting the TID/mt as -1.
//!
//! Checks for errors in PVM is performed, in which case exceptions are thrown.
//! For pure PVM errors, a PvmError object is created an thrown.
//! If a ProcessIsDead message is received, which is not really a
//! PVM error, a PvmDeadClientError is thrown.
//! 
//! \param mt Not used for some reason! Bug?  
//!
//! \return The message type of the recieved message.
enum MessageTypes
PvmCommunication::waitForAMessage(int TID, enum MessageTypes mt)
{
  tic();
  int info = pvm_recv(TID, mt);

  if (info < 0)
    {
      throw PvmError(info);
      toc();
      return NoMessage;
    }
  else 
    {
      buf = info;
      int msg_type;
      info = pvm_bufinfo(buf, &buf_len, &msg_type, &msg_tid);

      if (msg_type == AnErrorString) 
	{
	  string estr;
	  receive(estr);
	  throw AnError(estr);
	}

      if (msg_type == ProcessIsDead)
	{
	  int task;
	  pvm_upkint(&task, 1, 1);
	  ostringstream tid;
	  tid << task;
	  throw AnError("A process has died", tid.str()); // Throw a non-vital error
	}

      buf_type=MessageTypes(msg_type);
      toc();
      if (info < 0) {
	throw PvmError(info);
	return NoMessage;
      } else {
	return MessageTypes(msg_type);
      }
    }

}


//! Non-blocking check for messages in PVM.
//!
//! NOTE! No handling of dead processes etc is present in this function, 
//! in contrast to WaitForMessages.
//! 
//! \param mt The message type requested. Set to -1 for \e any message
//!
//! \return The message type of the recieved message.
enum MessageTypes
PvmCommunication::checkMessages(enum MessageTypes mt)
{
  int info = pvm_nrecv(-1, mt);
  if (info < 0)
    {
      throw PvmError(info);
      return NoMessage;
    }
  else if (info == 0) 
    {
      msg_tid = -1;		// Reset recent-sender info
      return NoMessage;
    }
  else
    {
      buf = info;
      int msg_type;
      info = pvm_bufinfo(buf, &buf_len, &msg_type, &msg_tid);
      buf_type=MessageTypes(msg_type);
      if (info < 0) {
	throw PvmError(info);
	return NoMessage;
      } else {
	return MessageTypes(buf_type);
      }
    }
}


//! \return Message type of last received message.
enum MessageTypes 
PvmCommunication::checkType() const
{
  return buf_type;
}


//! \return Sender ID (really task ID) of last received message.
int
PvmCommunication::messageSender() const
{
  return msg_tid;
}

//------------------------------------------------------------------------------
//
// Sending and receiving objects
//

// A generic empty message
void
PvmCommunication::createMessage()
{
  initSend();
}

//------------------------------------------------------------------------------
//
// Integers
//

void
PvmCommunication::receive(int &i)
{
  DISMSG("Receiving int");
  int info = pvm_upkint(&i, 1, 1);
  if (info < 0)
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::createMessage(const int i)
{
  int j = i;
  initSend();

  int info = pvm_pkint(&j, 1, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }
}


void
PvmCommunication::receive(int *i, int length)
{
  DISMSG("Receiving int array");
  int info = pvm_upkint(i, length, 1);
  if (info < 0)
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::createMessage(const int *i, const int length)
{
  int j[length];
  for (int iter = 0; iter < length; iter++)
    j[iter]=i[iter];
  
  initSend();

  int info = pvm_pkint(j, length, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }
}


void
PvmCommunication::receive(unsigned &u)
{
  DISMSG("Receiving unsigned");
  int info = pvm_upkuint(&u, 1, 1);
  if (info < 0)
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::createMessage(const unsigned u)
{
  unsigned j = u;
  initSend();

  int info = pvm_pkuint(&j,1, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::receive(unsigned *u, int length)
{
  DISMSG("Receiving unsigned array");
  int info = pvm_upkuint(u, length, 1);
  if (info < 0)
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::createMessage(const unsigned *u, const int length)
{
  unsigned j[length];
  for (int iter = 0; iter < length; iter++)
    j[iter]=u[iter];
  
  initSend();

  int info = pvm_pkuint(j, length, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }
}


//------------------------------------------------------------------------------
//
// Real, doubles
//
void
PvmCommunication::receive(Real &f)
{
  DISMSG("Receiving real");
  PVMCALL(pvm_upkdouble(&f, 1, 1));
}

void
PvmCommunication::createMessage(const Real x)
{
  double y = x;
  initSend();
  PVMCALL(pvm_pkdouble(&y, 1, 1));

}

void
PvmCommunication::receive(Real *f, int length)
{
  DISMSG("Receiving real array");
  int info = pvm_upkdouble(f, length, 1);
  if (info < 0)
    {
      throw PvmError(info);
    }
}

void
PvmCommunication::createMessage(const Real *f, const int length)
{
  double g[length];
  for (int iter = 0; iter < length; iter++)
    g[iter]=f[iter];
  
  initSend();

  int info = pvm_pkdouble(g, length, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }
}

//------------------------------------------------------------------------------
//
// Probabilities
//
void
PvmCommunication::receive(Probability &p)
{
  DISMSG("Receiving Probability");
  double f;
  int s;
  PVMCALL(pvm_upkdouble(&f, 1, 1));
  PVMCALL(pvm_upkint(&s, 1, 1));
//   ostringstream oss;
//   oss << "Recieve Prob: s = "  << s;
  DISMSG(s);

  p = Probability::setLogProb(f, s);
}


void
PvmCommunication::createMessage(const Probability &p)
{
  initSend();
  packMessage(p);
}

void
PvmCommunication::packMessage(const Probability &p)
{
  double x = p.getLogProb();
  int    s = p.getSign();

  PVMCALL(pvm_pkdouble(&x, 1, 1));
  PVMCALL(pvm_pkint(&s, 1, 1));
}

//------------------------------------------------------------------------------
//
// OrthologyMatrix
//
void 
PvmCommunication::receive(OrthologyMatrix &OM)
{
  DISMSG("Receiving OrthologyMatrix");
  unsigned nrows;    // This is matrix dimensions, not number of elements!
  PVMCALL(pvm_upkuint(&nrows, 1, 1));

  if (nrows < 1) 
    {
      WARNING2("Orthology matrix dimension: ", nrows);
      throw AnError("No size on recieved OrthologyMatrix!", 1);
    } 
  else if (nrows != OM.nrows()) 
    {
      WARNING2("Orthology matrix dimension: ", nrows);
      WARNING2("Received:                   ", OM.nrows());
      throw AnError("Different dimensions on received and stored matrix!", 2);
    }
  else     
    {
      //
      // The vector p will store the part of the matrix OM that is found 
      // under the diagonal. Looking at the first k rows, there are
      // k*(k-1)/2 elements included. Row 1: No elements,
      // Row 2: One element. Row 3: 3 elements. Etc.
      // Rows and columns are however indexed from 0. I want to be able to 
      // store element (i, j) from the matrix in vector in a manner so that
      // there are no "unused" elements.
      // In the rows above row i, there are i*(i-1)/2 according to our
      // reasoning. So, we can store element (i, j) in position 
      // i*(i-1)/2 + j in the vector. 
      // The size of the vector is hence nrows * (nrows-1) / 2.
      //
      unsigned size = nrows * (nrows - 1) / 2;
      double p[size];
      int s[size];
      PVMCALL(pvm_upkdouble(p, size, 1));
      PVMCALL(pvm_upkint(s, size, 1));

      for (unsigned i=1; i<nrows; i++) 
	{
	  for (unsigned j=0; j<i; j++) 
	    {
	      unsigned position = i * (i-1) / 2 + j;
	      assert(position < size);
	      OM(i, j) = Probability::setLogProb(p[position], s[position]);
	    }
	}
    }
}

void
PvmCommunication::createMessage(const OrthologyMatrix &OM)
{
  initSend();
  packMessage(OM);
}

void
PvmCommunication::packMessage(const OrthologyMatrix &OM)
{
  unsigned nrows = OM.nrows(); assert(nrows < 100000); // Absurd limit, in lack of something precise.

  unsigned size = nrows * (nrows - 1) / 2; assert(size > 0);	// Check for overflow?
  double p[size];
  int s[size];

  for (unsigned i=1; i<nrows; i++)	// Start at 1, because the diagonal is not included
    {
      for (unsigned j=0; j<i; j++)	// Everything up until the diagonal
	{
	  unsigned position = i * (i-1) / 2 + j;  assert(position <size);
	  p[position] = OM(i, j).getLogProb();
	  s[position] = OM(i, j).getSign();
	}
    }

  PVMCALL(pvm_pkuint(&nrows, 1, 1));
  PVMCALL(pvm_pkdouble(p, size, 1));
  PVMCALL(pvm_pkint(s, size, 1));
}

//------------------------------------------------------------------------------
//
// Trees
//
void
PvmCommunication::receive(Tree &T, enum MessageTypes mt)
{
  DISMSG("Receiving Tree");
  int len;
  PVMCALL(pvm_upkint(&len, 1, 1));

  char s[len+1];
  PVMCALL(pvm_upkstr(s));

  DISMSG("Tree string: " << s);
  TreeIO io = TreeIO::fromString(s);
  if (mt == PvmBeep::GeneTree)
    {
      T = io.readGuestTree();	// Silly to have a specialized gene tree reader,
				// but this is the most general reader.
    }
  else
    {
      T = io.readHostTree();    //! \todo {We should use readBeepTree(...) here, but I don't know what the restrictions here are!}
    }
}

void
PvmCommunication::createMessage(const Tree &T)
{


  TreeIO io;
//   string tree_str = io.newickSpeciesStringTree(T); // Again, most general function, with node ID:s
// The above is the original stuff and seem to send a tree with ID and edgeLengths but without edgeLengths, so writeHostTree() should work
  string tree_str = io.writeHostTree(T); // Again, most general function, with node ID:s
  int    len      = tree_str.length();

  initSend();
  PVMCALL(pvm_pkint(&len, 1, 1));


  char * s = strdup(tree_str.c_str());
  PVMCALL(pvm_pkstr(s));
  delete(s);			// Possible memory leak here, if we get a lot of errors
}



//------------------------------------------------------------------------------
// 
// SequenceData
//

void
PvmCommunication::receive(SequenceData &sd)
{
  DISMSG("Receiving SequenceData");
  int nSeqs, nPos, name_bound;
  char str[MAXTYPELEN];

  PVMCALL(pvm_upkint(&nSeqs, 1, 1));
  PVMCALL(pvm_upkint(&nPos , 1, 1));
  PVMCALL(pvm_upkstr(str));
  PVMCALL(pvm_upkint(&name_bound, 1, 1));
  
  sd.changeType(SequenceType::getSequenceType(str));

  for(int i=0; i<nSeqs; i++)
    {
      char namebuf[1 + name_bound];
      char seqbuf[1 + nPos];

      PVMCALL(pvm_unpackf("%s", namebuf));
      PVMCALL(pvm_unpackf("%s", seqbuf));

      sd.addData(namebuf, seqbuf);
    }
}


void
PvmCommunication::createMessage(const SequenceData &sd)
{
  int nSeqs       = sd.getNumberOfSequences();
  int nPos        = 0;
  if (nSeqs > 0)
    {
      nPos        = sd.getNumberOfPositions();
    }
  SequenceType st = sd.getSequenceType();

  char*       cst = strdup(st.getType().c_str());
  assert(strlen(cst) < MAXTYPELEN);
  int  name_bound = sd.getNameMaxSize();

  initSend();

  PVMCALL(pvm_pkint(&nSeqs, 1, 1));
  PVMCALL(pvm_pkint(&nPos, 1, 1));
  PVMCALL(pvm_pkstr(cst));
  PVMCALL(pvm_pkint(&name_bound, 1, 1));

  delete cst;

  for (int i=0; i<nSeqs; i++) 
    {
      string name = sd.getSequenceName(i);
      PVMCALL(pvm_packf("%s", name.c_str()));
      string seq  = sd[name];
      PVMCALL(pvm_packf("%s", seq.c_str()));
    }
}


//------------------------------------------------------------------------------
// 
// gene2species map
//
void
PvmCommunication::receive(StrStrMap &gs)
{
  DISMSG("Receiving StrStrMap");
  int size;

  PVMCALL(pvm_upkint(&size, 1, 1));
  
  for (int i=0; i < size; i++)
    {
      int len;
      PVMCALL(pvm_upkint(&len, 1, 1));
      char buf1[len+1];		// Remember space for the null character!
      char buf2[len+1];

      PVMCALL(pvm_upkstr(buf1));
      PVMCALL(pvm_upkstr(buf2));

      gs.insert(buf1, buf2);
    }
}

void
PvmCommunication::createMessage(const StrStrMap &gs)
{
  int size = gs.size();
  initSend();

  PVMCALL(pvm_pkint(&size, 1, 1));

  for (int i = 0; i < size; i++)
    {
      string s1 = gs.getNthItem(i);
      string s2 = gs.find(s1);
      int len = max(s1.length(), s2.length());
      
      PVMCALL(pvm_pkint(&len, 1, 1));
      PVMCALL(pvm_packf("%s", s1.c_str()));
      PVMCALL(pvm_packf("%s", s2.c_str()));
    }
}

//------------------------------------------------------------------------------
// 
// Strings
//
void
PvmCommunication::receive(string &st)
{
  DISMSG("Receiving String");
  int len;
  int info = pvm_upkint(&len, 1, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }

  char s[len+1];
  info = pvm_upkstr(s);
  if (info < 0) 
    {
      throw PvmError(info);
    }

  st = s;
}

void
PvmCommunication::createMessage(const string &st)
{
  int len = st.length();

  initSend();
  int info = pvm_pkint(&len, 1, 1);
  if (info < 0) 
    {
      throw PvmError(info);
    }

  char * s = strdup(st.c_str());
  info = pvm_pkstr(s);
  delete(s);
  if (info < 0)
    {
      throw PvmError(info);
    }
}


//------------------------------------------------------------------------------
//
// Complex messages
//

// 
//! Client method, see ClientReconSeqApprox, returns data to master process
//
void 
PvmCommunication::createMessage(int batch_id,
				int size,
				const beep::Probability &sum,
				const OrthologyMatrix &OM,
				int waiting)
{
  initSend();

  PVMCALL(pvm_pkint(&batch_id, 1, 1));
  PVMCALL(pvm_pkint(&size, 1, 1));

  packMessage(sum);

  DISMSG("Crucial OM:");
  DISMSG(OM.strRepresentation());
  assert(OM.nrows() > 0);
  assert(OM.nrows() < 1000);
  packMessage(OM);
  PVMCALL(pvm_pkint(&waiting, 1, 1));
}


//------------------------------------------------------------------------------
//
// Chain information
//
void
PvmCommunication::receive(Probability &p, Real &temp, int &chain_id)
{ 
  // The probability of the chain's state:
  double f;
  int s;
  PVMCALL(pvm_upkdouble(&f, 1, 1));
  PVMCALL(pvm_upkint(&s, 1, 1));
  p = Probability::setLogProb(f, s);

  // The temperature of the chain:
  PVMCALL(pvm_upkdouble(&temp, 1, 1));
  
  // The id of the chain:
  PVMCALL(pvm_upkint(&chain_id, 1, 1));
}

void
PvmCommunication::createMessage(const Probability &p,const Real  temp, const int chain_id)
{

  initSend();

  // The probability of the chain's state:
  packMessage(p);

  // The temperature of the chain:
  double y = temp;
  PVMCALL(pvm_pkdouble(&y, 1, 1));

  // The id of the chain:
  int j = chain_id;
  
  PVMCALL(pvm_pkint(&j, 1, 1));
}

// State information

void
PvmCommunication::receive(Probability &p, int &iter, string &state)
{
  // The probability of the chain's state:
  double f;
  int s;
  PVMCALL(pvm_upkdouble(&f, 1, 1));
  PVMCALL(pvm_upkint(&s, 1, 1));

  p = Probability::setLogProb(f, s);

  // The iteration number of the chain:
  PVMCALL(pvm_upkint(&iter, 1, 1));

  // The state of the chain:
  int len;
  PVMCALL(pvm_upkint(&len, 1, 1));
  
  char str[len+1];
  PVMCALL(pvm_upkstr(str));

  state = str;
}



void
PvmCommunication::createMessage(const beep::Probability &p, const int &iter, const string &state)
{
  initSend();

  // The probability of the chain's state:
  packMessage(p);

  // The id of the chain:
  int j = iter;
  PVMCALL(pvm_pkint(&j, 1, 1));

  // The state of the chain:
  int len = state.length();
  PVMCALL(pvm_pkint(&len, 1, 1));
  
  char * str = strdup(state.c_str());
  PVMCALL(pvm_pkstr(str));
  delete(str);
}


//! Send off a message.
//!
//! It is assumed that a message (possibly empty) has been created using 
//! one of the createMessage methods. This method will send that off.
//!
//! \param addressee Task ID of receiver
//! \param mt        The type of the message. Useful especially when the message is empty.
void
PvmCommunication::send(int addressee, enum MessageTypes mt)
{
  PVMCALL(pvm_send(addressee, mt));
}

#ifdef PIGS_FLY
// 
// Here comes some stuff that we probably won't need. Lotta used it
// during development. Should be deleted when we have a somewhat
// stable system!
//
void
PvmCommunication::join(const string &group)
{
  char * g = strdup(group.c_str());
  int inum = pvm_joingroup(g);
  if (inum < 0) {
    throw PvmError(inum);
  }
}

void
PvmCommunication::wait(const string & group, const int members)
{
  char * g = strdup(group.c_str());
  int info = pvm_barrier(g,members);  
  if (info < 0) {
    throw PvmError(info);
  }
}
#endif

//------------------------------------------------------------------------------
// Utilities
//
void
PvmCommunication::initSend()
{
  PVMCALL(pvm_initsend(PvmDataDefault));
}

void
PvmCommunication::tic()
{
  time_point = time(NULL);
}

void
PvmCommunication::toc()
{
  wait_time += time(NULL) - time_point;
}


unsigned
PvmCommunication::waitTime() const
{
  return wait_time;
}
