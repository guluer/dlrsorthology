#include <string>

#include "AnError.hh"
#include "UserLambdaMap.hh"

namespace beep
{
  using namespace std;

  // Constructor
  //--------------------------------------------------------------------------
  UserLambdaMap::UserLambdaMap(Tree& G, Tree& S, const StrStrMap& gs)
    : LambdaMap(G, S),
      ags(G,0)
  {
    // Translate gs to ags
    for(unsigned i = 0; i < gs.size();i++)
      {
	Node* u = G.getNode(i);
	ags[u] = S.findNode(gs.find(u->getName()));
      }

    // Then fill lambda
    if(G.getRootNode() != NULL)
      {
	try 
	  {
	    recursiveLambda(G.getRootNode(), S, gs);
	  }
	catch (AnError err) 
	  {
	    err.action();
	  }
      }
  }

  UserLambdaMap::~UserLambdaMap()
  {}


  void 
  UserLambdaMap::update(Tree& G, Tree& S)
  {
    throw AnError("UserLambdaMap:: update\n"
		  "If G has changed, then ancestral info might not be valid,\n"
		  "so I invalidate update for now\n", 1);
  }


  //--------------------------------------------------------------------------
  // Computing the lambda map
  //
  // lambda is defined as follows
  // 1. If u \in leaves(G) then \lambda(u) = x \in leaves(S), in the natural 
  //    way.
  // 2. If user has mapped 
  // 3. Otherwise, \lambda(u) = MRCA(lambda(left(u)), lambda(right(u))).
  //
  //--------------------------------------------------------------------------

  // Compute lambda for inner node u and return usersigma(u).
  //--------------------------------------------------------------------------
  Node *
  UserLambdaMap::recursiveLambda(Node* u, Tree& S, const StrStrMap& gs)
  {
    Node* ret;

    // Usually the gs-map only maps leaves to leaves, so the following 
    // if-clause is equivalent to u.isLeaf(). However, if the user have 
    // mapped internal nodes of G as well, e.g., because there are 
    // additional evidence of the distribution of G on S, then the
    // if-clause consider also these internal nodes
    if (u->isLeaf())
      {
	ret = compLeafLambda(u, S, gs);
      }
    else
      {
	Node *ls = recursiveLambda(u->getLeftChild(), S, gs);
	Node *rs = recursiveLambda(u->getRightChild(), S, gs);

	// Possibly do this conditional that ags[u] == NULL
	ret = S.mostRecentCommonAncestor(ls, rs);
	pv[u->getNumber()] = ret;
#ifdef SHOW_LAMBDA
	cerr << "Gene " << u->getNumber() << ": " << *ret << endl;
#endif
      }

    // if there is no ancestral map on u, return ret, else return ags info
    if(ags[u] == 0)
      {
	return ret;
      }
    else
      {
	return ags[u];
      }
  }

  // Compute the new lambda using existing lambda
  Node *
  UserLambdaMap::recursiveLambda(Node *u, Tree &S)
  {
    Node* ret;
    if (u->isLeaf())
      {
	ret = pv[u->getNumber()];
      }
    else
      {
	Node *ls = recursiveLambda(u->getLeftChild(), S);
	Node *rs = recursiveLambda(u->getRightChild(), S);
	ret = S.mostRecentCommonAncestor(ls, rs);
	pv[u->getNumber()] = ret;
#ifdef SHOW_LAMBDA
	cerr << "Gene " << u->getNumber() << ": " << (*ret).getNumber() << endl;
#endif
      }

    // if there is no ancestral map on u, return ret, else return ags info
    if(ags[u] == 0)
      {
	return ret;
      }
    else
      {

	return ags[u];
      }
  }

  // Set up lambda map for leaf g. 
  // The lambda value is returned
  //--------------------------------------------------------------------------
  Node *
  UserLambdaMap::compLeafLambda(Node *g, Tree &S, const StrStrMap& gs)
  {
    string genename = g->getName();
    const string sp_name = gs.find(genename);
    if (sp_name.empty()) 
      {
	throw AnError("Input inconsistency: "
		      "Leaf name missing in gene-to-species data.", 
		      genename, 1);
      }
    
    
    try 
      {
	Node *s = S.findNode(sp_name);
	pv[g->getNumber()] = s;
	return s;
      }
    catch (AnError e) 
      {
	cerr << "An error occured when trying to map genes to species.\n"
	     << "Please verify that gene and species names are correct\n"
	     << "and complete!\n\n";
	e.action();
	return NULL;
      }
  }
  
}// end namespace beep
