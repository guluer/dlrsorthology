#ifndef GUESTTREEMODEL_HH
#define GUESTTREEMODEL_HH

#include "ReconciliationModel.hh"

#include <iostream>
#include <sstream>

namespace beep
{
  //------------------------------------------------------------------------
  //
  //! Implements methods necessary for computing reconciliation likelihoods
  //! and most likely reconciliation etc, by way of dynamic programming. 
  //! \todo{Improve class description /bens}
  //! Author: Bengt Sennblad, SBC, � the MCMC-club, SBC, all rights reserved
  //
  //------------------------------------------------------------------------
  class GuestTreeModel : public ReconciliationModel
  {
  public:
    //------------------------------------------------------------------
    //
    //!\name Construct / Destruct / Assign
    //@{
    //------------------------------------------------------------------
    GuestTreeModel(Tree &G, StrStrMap &gs, BirthDeathProbs &bdp);
    GuestTreeModel(ReconciliationModel& rs);
    GuestTreeModel(const GuestTreeModel &M);
    virtual ~GuestTreeModel();

    GuestTreeModel & operator=(const GuestTreeModel &M);
    //@}
    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------

    //! Sets orthoNode, see further calculateDataProbability()
    //------------------------------------------------------------------------
    void setOrthoNode(const Node* u);

    //------------------------------------------------------------------
    // Interface to MCMCModel
    //------------------------------------------------------------------
    void update();

    //! If orthoNode=NULL, then calculateData Probability() returns Pr[G], 
    //! else if, e.g., orthoNode is the LCA of v and w, calculateDataProbability() 
    //! returns Pr[G and v and w are orthologs].
    //------------------------------------------------------------------------
    virtual Probability calculateDataProbability(); 

    //-------------------------------------------------------------------
    //
    // I/O
    //
    //-------------------------------------------------------------------
    friend std::ostream& 
    operator<<(std::ostream &o, const GuestTreeModel& pm);
    std::string print() const;

    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
  protected:

    //! computes \f$ s_V(x,u) \f$
    //! precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    virtual void computeSV(Node& x, Node& u);

    //! computes \f$ s_A(x,u) \f$
    virtual void computeSA(Node& x, Node& u);

    //! computes \f$ s_X(x,u,k), k\in[|L(G_u)|] \f$
    //! precondition: sigma[u] < x     (note \sigma(u) = sigma[u]
    virtual void computeSX(Node& x, Node& u);

    //! multiplies with two if subtrees are non-isomorphic
    virtual void adjustFactor(Probability& factor, Node& u);
 
  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------

    // The following two structures are indexed by a species node and 
    // a gene node
    //! Holds \f$ S_A(x,u) \f$ for any \f$ x\in V(S), u\in V(G) \f$
    NodeNodeMap<Probability> S_A; 

    //! Holds \f$ S_X(x,u) \f$ for any \f$ x\in V(S), u\in V(G) \f$
    //! Notice that the S_X vectors are zero indexed!
    NodeNodeMap< std::vector<Probability> > S_X; 

    //! These keeps track if the corresponding value in S_X and S_A is computed 
    //!@{
    NodeNodeMap<unsigned> doneSA; 
    NodeNodeMap<unsigned> doneSX; 
    //!@}

    //! The value of this attribute determines what is computed by 
    //! calculateDataProbability(). If orthoNode = v, where v is the
    //! least common ancestor of some leaves l and l', then the prob 
    //! that l and l' are orthologs are computed, else if orthoNode = 0,
    //! Pr[G] is computed exactly as in GuestTreemodel
    const Node* orthoNode; 
  };


  //------------------------------------------------------------------------
  //
  //! Implements subclass for labeled guesttrees
  //
  //------------------------------------------------------------------------
  class LabeledGuestTreeModel : public GuestTreeModel
  {
  public:
    //------------------------------------------------------------------
    //
    // Construct / Destruct / Assign
    //
    //------------------------------------------------------------------
    LabeledGuestTreeModel(Tree &G, StrStrMap &gs, BirthDeathProbs &bdp);
    LabeledGuestTreeModel(ReconciliationModel& rs);
    LabeledGuestTreeModel(const LabeledGuestTreeModel &M);
    virtual ~LabeledGuestTreeModel();

    LabeledGuestTreeModel & operator=(const LabeledGuestTreeModel &M);

    //------------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------------
    // normailzes with number of labelings
    //------------------------------------------------------------------------
    virtual Probability calculateDataProbability(); 

  protected:
    //------------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------------
    //! multiplies with two always
    void adjustFactor(Probability& factor, Node& u);
  protected:
    //------------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------------
    Probability nLabeling;
  };
  
}//end namespace beep

#endif
