/* 
 * File:   Tokenizer.cc
 * Author: peter9
 * 
 * Created on November 26, 2009, 4:46 PM
 */

#include "Tokenizer.hh"

namespace beep{

Tokenizer::Tokenizer(const std::string& delims) :
    m_more_tokens(false),
    m_string(""),
    m_offset(0),
    m_delimiters(delims)
    
{
    //Find next token
    advance();
}




string
Tokenizer::getNextToken()
{
    if(m_more_tokens){
        string tmp = m_token;
        advance();
        return tmp;
    }
    return "";
}





void
Tokenizer::advance()
{
    //Find start of token
    size_t i = m_string.find_first_not_of(m_delimiters, m_offset);
    /*
     * If i == string::npos, then we did not find any starting point
     * hence, there are no more tokens
     */
    if (string::npos == i)
    {
        m_offset = m_string.length();
        m_more_tokens = false;
        return;
    }

    //Find end of token
    size_t j = m_string.find_first_of(m_delimiters, i);
    //If j == string::npos then we passed the end of the string
    if (string::npos == j)
    {
        //Set m_token will be m_string[i..end]
        m_token = m_string.substr(i);
        m_offset = m_string.length();
        m_more_tokens = true;
        return;
    }

    //m_token will be m_string[i..(j-1)]
    m_token = m_string.substr(i, j - i);
    m_offset = j;
    m_more_tokens = true;
}









bool Tokenizer::hasMoreTokens()
{
    return m_more_tokens;
}



void Tokenizer::setString(string &str)
{
    m_offset = 0;
    m_string = str;
    advance();
}

};//Namespace beep