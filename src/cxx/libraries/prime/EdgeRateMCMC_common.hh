
#ifndef EDGERATEMCMC_COMMON_HH
#define EDGERATEMCMC_COMMON_HH


#include <cmath>
#include <iostream>
#include <sstream>

#include "AnError.hh"
#include "Beep.hh"
#include "ConstRateModel.hh"
#include "Density2P.hh"
#include "EdgeRateMCMC.hh"
#include "EdgeRateModel.hh"
#include "VarRateModel.hh"



namespace beep
{

//----------------------------------------------------------------------
//
// class EdgeRateMCMC_common
// subclass of EdgeRateModel that implements a MCMCModel interface
// for preturbing the parameters, rate, mean and variance, of
// EdgeRateModel
//  Author Bengt Sennblad,
//  copyright the MCMC club, SBC
//
// This is templatized since a class iidRateMCMC would be identical
// to a class gbmRateMCMC, except for inheriting from iidRateModel
// instead of gbmRateModel.
//
// NOTE! that I have instatiated typedefs iidRateMCMC and gbmRateMCMC
// Use these instead of the template class!
//
//----------------------------------------------------------------------
template<class Templ_EdgeRateModel>
class EdgeRateMCMC_common : public Templ_EdgeRateModel, public EdgeRateMCMC
{
	// Some convenience alias
	using EdgeRateMCMC::oldValue;
	using EdgeRateMCMC::idx_limits;
	using EdgeRateMCMC::idx_node;
	using EdgeRateMCMC::min;
	using EdgeRateMCMC::max;
	using Templ_EdgeRateModel::rateProb;
	using Templ_EdgeRateModel::T;
	using Templ_EdgeRateModel::edgeRates;
	using StdMCMCModel::R;
	using EdgeRateMCMC::name;

public:

	//----------------------------------------------------------------------
	//
	// Constructor/Destructor/Assign
	//
	//----------------------------------------------------------------------
	EdgeRateMCMC_common(MCMCModel& prior,Density2P& rateProb_in,
			Tree& T, const Real& suggestRatio = 1.0) :
		EdgeRateModel(),
		Templ_EdgeRateModel(rateProb_in, T),
		EdgeRateMCMC(prior, Templ_EdgeRateModel::nRates() + 2, "EdgeRates", suggestRatio)
	{
		init();
		//       rateProb->getRange(min, max);
		//       if(rateProb->densityName() == "Uniform")
		// 	{
		// 	  fixMean();
		// 	  fixVariance();
		// 	}
		//       else
		// 	{
		// 	  max = 1000.0;
		// 	}
		//       update_idx_limits();
		// //       suggestion_variance = 0.1 * std::log(rateProb->getMean());
		//       suggestion_variance = 0.1 * rateProb->getMean();
	}

	EdgeRateMCMC_common(MCMCModel& prior, Density2P& rateProb_in, Tree& T_in,
			const std::string& name_in, const Real& suggestRatio = 1.0) :
		EdgeRateModel(),
		Templ_EdgeRateModel(rateProb_in, T_in),
		EdgeRateMCMC(prior, Templ_EdgeRateModel::nRates() + 2, "edgeRates",  suggestRatio)
	{
		init();
		//       rateProb->getRange(min, max);
		//       if(rateProb->densityName() == "Uniform")
		// 	{
		// 	  fixMean();
		// 	  fixVariance();
		// 	}
		//       else
		// 	{
		// 	  max = 1000.0; //!<\todo max should be set by user, include an access function /bens
		// 	}
		//       update_idx_limits();
		//       suggestion_variance = 0.1 * rateProb->getMean();
		name = name_in;
	}

	EdgeRateMCMC_common(const EdgeRateMCMC_common& erm) :
		EdgeRateModel(erm),
		Templ_EdgeRateModel(erm),
		EdgeRateMCMC(erm)
	{}

	~EdgeRateMCMC_common()
	{}

	EdgeRateMCMC_common& operator=(EdgeRateMCMC_common& erm)
	{
		EdgeRateModel::operator=(erm);
		Templ_EdgeRateModel::operator=(erm);
		EdgeRateMCMC::operator=(erm);
	}

public:
	//----------------------------------------------------------------------
	//
	// Interface
	//
	//----------------------------------------------------------------------

	//! \todo{General form suits non-rootperturbing RateModels}
	void fixRates()
	{
		if( idx_limits[2] != 0)                   // check if already fixed
		{
			idx_limits[2] = 0;                    // set perturb prob = 0
			n_params -= nRates();                 // less params to perturb
			update_idx_limits();                  // tell StdMCMCModel
		}
		return;
	}

	//! Generates a random sample of edgeRates. Base version
	// Works for, e.g., ConstRateModel
	//----------------------------------------------------------------------
	void generateRates()
	{
		for(unsigned i = 0; i < edgeRates.size(); i++)
		{
			Real newRate = rateProb->sampleValue(R.genrand_real3());
			setRate(newRate, T->getNode(i));
		}
		return;
	}

	//----------------------------------------------------------------------
	//
	// I/O
	//
	//----------------------------------------------------------------------
	friend std::ostream& operator<<(std::ostream &o, const EdgeRateMCMC_common& e)
	{
		return o << e.print();
	}

	std::string print() const
			{
		std::ostringstream oss;
		oss << Templ_EdgeRateModel::print();
		oss << "The mean ";
		if(idx_limits[0] == 0)
		{
			oss << " is fixed to "
					<< rateProb->getMean()
					<< ".\n";
		}
		else
		{
			oss << "is estimated during analysis.\n";
		}
		oss << "The variance ";
		if(idx_limits[1] == 0)
		{
			oss << "is fixed to "
					<< rateProb->getVariance()
					<< ".\n";
		}
		else
		{
			oss << " is estimated during analysis.\n";
		}
		oss << "The edge rates ";
		if(idx_limits[2] == 0)
		{
			oss << "are fixed to: \n";
			for(unsigned i = 0; i < edgeRates.size(); i++)
			{
				oss << "edgeRate[" << i << "]\t"<< edgeRates[i] << "\n";
			}
		}
		else
		{
			oss << " are estimated during analysis";
		}

		oss << ".\n";

		return name + ": " + oss.str() + StdMCMCModel::print();//Templ_EdgeRateModel::print() + EdgeRateMCMC::print();
			}

	//----------------------------------------------------------------------
	//
	// Implementation
	//
	//----------------------------------------------------------------------
	void init()
	{
		rateProb->getRange(min, max);
		if(rateProb->densityName() == "Uniform")
		{
			fixMean();
			fixVariance();
		}
		else
		{
			max = 1000.0;
		}
		update_idx_limits();
		// this is default
		//        suggestion_variance = std::log(0.1/rateProb->getMean() + 1.0);
		// this is  change
		suggestion_variance = std::log(0.01/rateProb->getMean() + 1.0);
		//      std::cout << "# Notice suggestion variance = "
		//		<< suggestion_variance
		//		<< ", i.e., variance of LogN is "
		//		<< std::exp(suggestion_variance)-1
		//		<< std::endl;
		//       suggestion_variance = 0.1 * rateProb->getMean();

	}

	// general version does nothing
	void recursiveGenerateRates(const Node* n, Real parentRate)
	{return;};


	//! Perturbs a randomly chosen edgeRate.
	//! general version, works for e.g., ConstRateModel
	//! Returns propratio !
	//----------------------------------------------------------------------
	Probability perturbRate()
	{
		assert(edgeRates.size() > 0); // Check precondition. TODO: check earlier

		idx_node = T->getNode(R.genrand_modulo(edgeRates.size()));

		oldValue = getRate(idx_node);             // Save previous value

		Probability propRatio(1.0);                    // Create return object
		setRate(perturbNormal(oldValue, suggestion_variance,
				min, max, propRatio), idx_node);
		return propRatio;
	}

	//! Perturbs a randomly chosen edgeRate.
	//! general version, works for e.g., ConstRateModel
	//! Returns propratio !
	//----------------------------------------------------------------------
	Probability perturbRate(unsigned x)
	{
		assert(edgeRates.size() > 0); // Check precondition. TODO: check earlier

		idx_node = T->getNode(x);
		oldValue = getRate(idx_node);             // Save previous value

		Probability propRatio(1.0);                    // Create return object
		setRate(perturbNormal(oldValue, suggestion_variance,
				min, max, propRatio), idx_node);
		return propRatio;
	}

	void updateRatesUsingTree()
	{
		for(unsigned nIndex = 0; nIndex <= (T->getNumberOfNodes()-1); nIndex++)
		{
			Node* n = T->getNode(nIndex);
			if (!n->isRoot())
			{
				Real nTime = n->getTime();
				Real nLength = n->getLength();
				setRate(nLength/nTime,n);
			}
		}
	}

	void writeLengthsToTree()
	{
		for(unsigned nIndex = 0; nIndex <= (T->getNumberOfNodes()-1); nIndex++)
		{
			Node* n = T->getNode(nIndex);
			if (!n->isRoot())
			{
				Real nTime = n->getTime();
				Real nSubstRate = getRate(n);
				n->setLength(nTime*nSubstRate);
			}
		}
	}

	//! If, e.g., we discard the new state, then the new likelihood
	//! calculations are incorrect and needs to be recalculated->reset
	//! perturbedNode()
	//----------------------------------------------------------------------
	void resetPerturbedNode()
	{
#ifdef PERTURBED_NODE
		T->perturbedNode(idx_node); //Reset
#endif
	}

	//! Default version does nothing
	void adjustRates(const Real& ratio)
	{
		return;
	}

	// General form suits e.g., ConstRateModel
	//! \todo{specialize for iid and gbm!/bens}
	std::string ratesStr() const
			{
		return edgeRates.print();
			}

	std::string ratesHeader() const
			{
		if(edgeRates.size() == 1)
		{
			return "edgeRate(float);\t";
		}
		else
		{
			std::ostringstream oss;
			for(unsigned i = 0; i < edgeRates.size(); i++)
			{
				oss << "edgeRate[" << i << "](float);\t";
			}
			return oss.str();
		}
			}

	//! Helper function to perturbRate() for those models that do not model
	//! root rate, e.g iid/gbmRateModel.
	//! Returns propRatio!
	//----------------------------------------------------------------------
	Probability
	perturbRate_notRoot()
	{
		assert(edgeRates.size() > 1); // Check precondition. TODO: check earlier

		Node* p;               // parent node to idx_node
		bool doCont = true;
		do                     // Pick randomly a valid node to perturb
		{
			idx_node = T->getNode(R.genrand_modulo(edgeRates.size()));
			p = idx_node->getParent();            // This is NULL iff n is root
			switch (getRootWeightPerturbation())
			{
			case EdgeWeightModel::BOTH:
				doCont = idx_node->isRoot();
				break;
			case EdgeWeightModel::RIGHT_ONLY:
				doCont = (idx_node->isRoot() || (p->isRoot() && p->getLeftChild()==idx_node));
				break;
			case EdgeWeightModel::NONE:
				doCont = (idx_node->isRoot() || p->isRoot());
				break;
			}
		}
		while (doCont);

		//       oldValue = getRate(idx_node);             // Save previous value
		oldValue = edgeRates[idx_node];             // Save previous value

		Probability propRatio(1.0);                    // return object

		setRate(perturbLogNormal(oldValue, suggestion_variance,
				min, max, propRatio), idx_node);
#ifdef PERTURBED_NODE
		// If possible, only recalculate substlike for perturbed part of tree
		// If other node also perturbed or if idx_node is a child of root (since
		// root's children share same rate) recalculate substlike for whole tree
		if(T->perturbedNode() || idx_node->getParent()->isRoot())
		{
			T->perturbedNode(T->getRootNode());
		}
		else // It suffices to calculate above idx_node
		{
			T->perturbedNode(idx_node);
		}
#endif
		return propRatio;
	}

	//! Helper function to perturbRate() for those models that do not model
	//! root rate, e.g iid/gbmRateModel.
	//! Returns propRatio!
	//----------------------------------------------------------------------
	Probability
	perturbRate_notRoot(unsigned x)
	{
		assert(edgeRates.size() > 1); // Check precondition. TODO: check earlier

		Node* p;               // parent node to idx_node
		bool doCont = true;
		do                     // Pick randomly a valid node to perturb
		{
			idx_node = T->getNode(x);
			p = idx_node->getParent();            // This is NULL iff n is root
			x = x - 1;
			switch (getRootWeightPerturbation())
			{
			case EdgeWeightModel::BOTH:
				doCont = idx_node->isRoot();
				break;
			case EdgeWeightModel::RIGHT_ONLY:
				doCont = (idx_node->isRoot() || (p->isRoot() && p->getLeftChild()==idx_node));
				break;
			case EdgeWeightModel::NONE:
				doCont = (idx_node->isRoot() || p->isRoot());
				break;
			}
		}
		while (doCont);

		//       oldValue = getRate(idx_node);             // Save previous value
		oldValue = edgeRates[idx_node];             // Save previous value

		Probability propRatio(1.0);                    // return object

		setRate(perturbLogNormal(oldValue, suggestion_variance,
				min, max, propRatio), idx_node);
#ifdef PERTURBED_NODE
		// If possible, only recalculate substlike for perturbed part of tree
		// If other node also perturbed or if idx_node is a child of root (since
		// root's children share same rate) recalculate substlike for whole tree
		if(T->perturbedNode() || idx_node->getParent()->isRoot())
		{
			T->perturbedNode(T->getRootNode());
		}
		else // I suffices to calculate above idx_node
		{
			T->perturbedNode(idx_node);
		}
#endif
		return propRatio;
	}
};



//----------------------------------------------------------------------
//
// Specializations
//
//----------------------------------------------------------------------
typedef EdgeRateMCMC_common<ConstRateModel> ConstRateMCMC; //! Specialization alias
typedef EdgeRateMCMC_common<iidRateModel> iidRateMCMC; //! Specialization alias
typedef EdgeRateMCMC_common<gbmRateModel> gbmRateMCMC; //! Specialization alias


/**
* Moved all definitions of these functions to a new file called
* EdgeRateMCMC_common.cc.
* When inluding this headerfile in several files the functions were defined
* multiple times causing a linker error.
* /peter9
*/


//! Helper function of EdgeRateMCMC_common<gbmRatemodel>::generateRates()
//----------------------------------------------------------------------
template<>
void EdgeRateMCMC_common<gbmRateModel>::recursiveGenerateRates(const Node* n,
		Real parentRate);

//! Generates a random sample of edgeRates. iidRateModel version.
//----------------------------------------------------------------------
template<>
void EdgeRateMCMC_common<iidRateModel>::generateRates();

//! Generates a random sample of edgeRates. gbmRateModel version.
//----------------------------------------------------------------------
template<>
void EdgeRateMCMC_common<gbmRateModel>::generateRates();

//! Perturbs a randomly chosen edgeRate.
// Rate of root's left child (rlc) is assumed to be treated
// separately (e.g., iidRate model: rlc = rrc (rate of root's right
// child), gbmRatemodel: rlc is the mean).
//----------------------------------------------------------------------
template<>
Probability EdgeRateMCMC_common<ConstRateModel>::perturbRate();

template<>
Probability EdgeRateMCMC_common<iidRateModel>::perturbRate();

template<>
Probability EdgeRateMCMC_common<gbmRateModel>::perturbRate();

//! Perturbs a randomly chosen edgeRate.
// Rate of root's left child (rlc) is assumed to be treated
// separately (e.g., iidRate model: rlc = rrc (rate of root's right
// child), gbmRatemodel: rlc is the mean).
//----------------------------------------------------------------------
template<>
Probability EdgeRateMCMC_common<ConstRateModel>::perturbRate(unsigned x);

template<>
Probability EdgeRateMCMC_common<iidRateModel>::perturbRate(unsigned x);

template<>
Probability EdgeRateMCMC_common<gbmRateModel>::perturbRate(unsigned x);

//   // Specialization for iid - multiplies all rates with ratio.
//   template<>
//   void EdgeRateMCMC_common<iidRateModel>::adjustRates(const Real& ratio)
//   {
//     for(unsigned i = 0; i < edgeRates.size(); i++)
//       {
// 	Node* n = T->getNode(i);
// 	Node* p = n->getParent();
// #ifdef INCLUDE_ROOT_RATE // joelgs: Use of these flags now forbidden!
//   	if(n->isRoot())
// #else
//   	if(n->isRoot() || (p->isRoot() && p->getLeftChild() == n))
// #endif
// 	  {
// 	    continue;
// 	  }
// 	Real newRate = getRate(n) * ratio;
// 	setRate(newRate, n);
//       }
// #ifdef PERTURBED_NODE
//     // We will always need to recalculate whole tree
//     T->perturbedNode(T->getRootNode());
// #endif

//     return;
//   }

// Specialization for gbm - multiplies all rates with ratio.
//   template<>
//   void EdgeRateMCMC_common<gbmRateModel>::adjustRates(const Real& ratio)
//   {
//     for(unsigned i = 0; i < edgeRates.size(); i++)
//       {
// 	Node* n = T->getNode(i);
// 	Node* p = n->getParent();
// #ifdef INCLUDE_ROOT_RATE // joelgs: Use of these flags now forbidden!
//   	if(n->isRoot())
// #elif defined FIX_ROOT_EDGES_RATE // joelgs: Use of these flags now forbidden!
// 	if(n->isRoot() || p->isRoot())
// #else
//   	if(n->isRoot() || (p->isRoot() && p->getLeftChild() == n))
// #endif
// 	  {
// 	    continue;
// 	  }
// 	Real newRate = getRate(n) * ratio;
// 	setRate(newRate, n);
//       }
// #ifdef PERTURBED_NODE
//     // We will always need to recalculate whole tree
//     T->perturbedNode(T->getRootNode());
// #endif

//     return;
//   }

// Specialization for iid
template<>
std::string EdgeRateMCMC_common<iidRateModel>::ratesStr() const;


// Specialization for iid
template<>
std::string EdgeRateMCMC_common<iidRateModel>::ratesHeader() const;


// Specialization for gbm
template<>
std::string EdgeRateMCMC_common<gbmRateModel>::ratesStr() const;


// Specialization for gbm
template<>
std::string EdgeRateMCMC_common<gbmRateModel>::ratesHeader() const;

}//end namespace beep
#endif



