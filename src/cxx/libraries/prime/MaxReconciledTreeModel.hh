#ifndef MAXRECONCILEDMODEL_HH
#define MAXRECONCILEDMODEL_HH

#include <map>

#include "ReconciledTreeModel.hh"

namespace beep
{

  //------------------------------------------------------------
  //
  //! Computes the probability of and sets the maximum likelihood 
  //! reconciliation
  //! Assume u has children v and w in G and x has children
  //! y and z in S, then
  //! \f[ m_V^{(i)}(x,u,i) = \begin{cases}
  //! 1 & \text{if $i=1, x\in L(S), u\in L(G), \sigma(u)=x$}\\~
  //! \max^{(i)}_{j\in [i], h\in[i]}m_A^{(i)}(y,u)m_A^{(i)}(z,u) &
  //! \text{if $\sigma(u)\leq_S x$}\\~
  //! \max^{(i)}_{j\in [i], h\in[i]}m_A^{(i)}(y,v)m_A^{(i)}(z,w) &
  //! \text{if $\sigma(u)=x, \sigma(v)\leq_S y,\sigma(w)\leq_S z$}\\~
  //! \max^{(i)}_{j\in [i], h\in[i]}m_A^{(i)}(y,w)m_A^{(i)}(z,v) &
  //! \text{if $\sigma(u)=x, \sigma(w)\leq_S y,\sigma(v)\leq_S z$}\\~
  //! 0 & \text{otherwise} 
  //! \end{cases} \f] 
  //!
  //! \f[ m_A^{(i)}(x,u,i) = \begin{cases}
  //! X_A(x) & \text{if $u\not\in V(S_x)$}\\ ~
  //! \max^{(i)}_{k\in [|L(G[\gamma_u(x)])|],j\in [i]}Q_x(k)m_X^{(i)}(y,u)&
  //! \text{otherwise}
  //! \end{cases} \f] 
  //!
  //! \f[ m_X^{(i)}(x,u,i,k) = \begin{cases}
  //! 0 & \text{if $\sigma(u) >_S x $} \\ ~
  //! \max^{(i)}_{j,h\in [i]l\in [k-1]}m_X^{(i)}(x,v,l)m_X^{(i)}(x,w, k-l) &
  //! \text{otherwise} 
  //! \end{cases}  \f] 
  //
  //------------------------------------------------------------
  class MaxReconciledTreeModel : public ReconciledTreeModel
  {
  public:
    MaxReconciledTreeModel(Tree& G, StrStrMap& gs, BirthDeathProbs& bdp);
    MaxReconciledTreeModel(const ReconciliationModel& m);
    ~MaxReconciledTreeModel();

    //------------------------------------------------------------
    //
    // Interface
    //
    //------------------------------------------------------------
    //! Sets attribute gamma to the maximum likelihood reconcilioation
    //! and returns its probability
    Probability getMLReconciliation();
      
  protected:
    //------------------------------------------------------------
    //
    // Implementation
    //
    //------------------------------------------------------------
    //! \name Recursive helper functions for setting gamma that
    //!  works analogously to mV, mA and mX 
    //!@{ 
    //! gA handle the case when a new sliced subtree is started
    void gA(Node& x, Node& u, unsigned i);
    //! gX takes care of the case inside a sliced subtree. gX also
    //! handles cases when we reach a leaf in the subtree, e.g.,
    //! analogously to a gV function.
    void gX(Node& x, Node& u, unsigned k, unsigned i);
    //!@}	 

    //! \name \f$ m_V, m_A, \f$ and \f $ m_X \f$ functions
    //! @{
    //! This function computes \f$ m_V^{(i)}(x,u) \f$ and saves it in MA(x,u), 
    //! together with the ranks of the \f $m_A \f$ that xielded it.
    //! Precondition: x>sigma[u] == false
    //!               x.isLeaf() == u.isLeaf && sigma[u] ==x 
    //------------------------------------------------------------
    void computeMV(Node& x, Node& u, unsigned i);

    //! Computes \f$ m_A^{(i)}(x,u) \f$ and saves it in MA(x,u), 
    //! together with the rank of the \f $m_x \f$ that yielded it.
    //! Precondition: x>sigma[u] == false
    //!               x.isLeaf() == u.isLeaf && sigma[u] ==x 
    //------------------------------------------------------------
    void computeMA(Node& x, Node& u, unsigned i);

    //! This function computes \f$ m_X^{(i)}(x,u,k) \f$ and saves it in 
    //! MX(x,u), together with the ranks of the \f $m_X \f$ that yielded it.
    //! Precondition: x>sigma[u] == false
    //!               x.isLeaf() == u.isLeaf && sigma[u] ==x 
    //------------------------------------------------------------
    void computeMX(Node& x, Node& u, unsigned k, unsigned i);

    //! \f$ I(u,i,j,k,l)
    unsigned computeI(Node& u, unsigned i, unsigned j, unsigned k, unsigned l);
    //! @}

    //------------------------------------------------------------
    //
    // Attributes
    //
    //------------------------------------------------------------
    //! \todo{Consider other ways how this ugly structure can be hidden!}
    //! \name typedefs to simplify code
    //! @{
    //! First unsigned holds k=number of leaves, second holds i = rank
    //! of left prob used and third holds h = rank of right prob used
    typedef std::pair<unsigned, std::pair<unsigned, unsigned> > Utriple;
    typedef std::multimap<Probability, Utriple, 
			  std::greater<Probability> > ProbRanksMap; 
    //!@}

    //! Holds \f$ m_A \f$ and \f$ m_X \cup m_V \f$
    //!@{
    NodeNodeMap<ProbRanksMap> MA; 
    NodeNodeMap<std::map<unsigned, ProbRanksMap> > MX; 
    //! @}
  };
}// end namespace beep
#endif
