#ifndef EPOCHPTSET_HH
#define EPOCHPTSET_HH

#include <string>
#include <utility>
#include <vector>

#include "Node.hh"

namespace beep
{

/**
 * Convenience notation for discretized time, where 'first' refers
 * to epoch index and 'second' to time index within epoch.
 */
typedef std::pair<unsigned,unsigned> EpochTime;

/**
 * Represents discretization information on the part of a host tree spanning an epoch.
 * 
 * An epoch is a time span defined by two branching events on the host tree without any
 * divergence events occurring the time in-between. The definition is intuitively extended
 * to include the single-edge epoch of a top time edge, and the epoch ending with leaves. 
 * 
 * Essentially, an EpochPtSet defines an array of discretization points. The "lateral"
 * dimension (or columns, if you wish) of this array corresponds to the contemporary
 * edges of the epoch. The vertical dimension (or rows) corresponds to a slicing of
 * the epoch into several equidistant time sub-intervals. A discretization mid-point has
 * then been placed in each such interval.
 * 
 * When accessing the times of discretization points, the two endpoints are included.
 * The distance from one these to the adjacent point is half of the normal timestep.
 * 
 * See EpochTree for more info.
 * 
 * @author Joel Sjöstrand.
 */
class EpochPtSet
{
	
public:
	
	/**
	 * Constructor.
	 * @param edges the edges intersecting the epoch's time span.
	 * @param loTime the time of the epoch's lower divergence event.
	 * @param upTime the time of the epoch's upper divergence event.
	 * @param noOfIvs the number of sub-intervals to slice epoch into.
	 *        Must be greater than zero.
	 */
	EpochPtSet(std::vector<const Node*> edges, Real loTime, Real upTime, unsigned noOfIvs);
	
	/**
	 * Destructor.
	 */
	virtual ~EpochPtSet();

	/**
	 * Returns the discretized times of the epoch including endpoints,
	 * implying that the latter coincide with end times of epochs above/below.
	 * Also please note that e.g. for a 4-interval epoch with discretization
	 * times (t0,t1,t2,t3,t4,t5) and timestep dt we have t1-t0 = t5-t4 = dt/2,
	 * while t2-t1 = t3-t2 = t4-t3 = dt.
	 * @return the times of the epoch.
	 */
	const std::vector<Real>& getTimes() const;
	
	/**
	 * Returns a discretized time.
	 * @param index the time index.
	 * @return the time.
	 */
	Real getTime(unsigned index) const;
	
	/**
	 * Returns the epoch time boundary closest to the leaves.
	 * @return the lowermost time.
	 */
	Real getLowerTime() const;
	
	/**
	 * Returns the epoch time boundary closest to the top.
	 * @return the uppermost time.
	 */
	Real getUpperTime() const;
	
	/**
	 * Returns the number of discretized times of an epoch,
	 * both end points included (two greater than the
	 * number of intervals).
	 * @return the number of discretized times.
	 */
	unsigned getNoOfTimes() const;
	
	/**
	 * Returns the number of intervals, see also getNoOfTimes().
	 * @return the number of discretization intervals.
	 */
	unsigned getNoOfIntervals() const;
	
	/**
	 * Returns the timestep of discretization intervals.
	 * Note: the distance from an endpoint to its neighbour
	 * is only half the timestep.
	 * @return the timestep.
	 */
	Real getTimestep() const;
	
	/**
	 * Returns the entire time span of the epoch.
	 * @return the time span.
	 */
	Real getTimeSpan() const;
	
	/**
	 * Returns the vector of edges which intersect the epoch.
	 * Edges are referenced via their lower node in the tree.
	 */
	const std::vector<const Node*>& getEdges() const;
	
	/**
	 * Returns the edge at a specified index from the vector
	 * of edges which intersect the epoch.
	 * Edges are referenced via their lower node.
	 * @param index the index of the edge in the epoch.
	 * @return the edge, defined by its lower node.
	 */
	const Node* getEdge(unsigned index) const;
	
	/**
	 * Returns the number of edges the epoch spans.
	 * @return the number of edges.
	 */
	unsigned getNoOfEdges() const;
	
	/**
	 * Returns the total number of points over all spanned
	 * edges in the epoch (i.e. number of times multiplied with
	 * number of edges).
	 * @return the number of points.
	 */
	unsigned getNoOfPoints() const;
		
private:
	
	/** Vector of intersecting edges in the epoch (defined via lower node). */
	std::vector<const Node*> m_edges;
	
	/** List of times of epoch, both endpoint times included. */
	std::vector<Real> m_times;
		
	/** Timestep between discretized times. Stored explicitly for speed. */
	Real m_timestep;
};

} // end namespace beep

#endif // EPOCHPTSET_HH

