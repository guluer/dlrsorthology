#include "Beep.hh"
#include "AnError.hh"

#include <algorithm>
#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <typeinfo>
#include <cmath>
#include <cctype>
#include <typeinfo> 
#include <cerrno>
#include <limits.h>
#include <cstdio>
// Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved

//-------------------------------------------------------
//
// This file contains various typedefs and other junk that 
// are often used in the namespace beep
//
//-------------------------------------------------------

namespace beep
{
  using namespace std;
  // This function add an indent space and is used in different 
  // classes' ouput/print() function
  //----------------------------------------------------------

  void sprintfFloat(char *str,  size_t size, float f)
  {
    int ret = snprintf(str, size, "%f",f);
    if ( ret >= int ( size )) { fprintf(stderr,"Programming error: the stringbuffer was too small\n"); abort(); // was exit(1);
  }
    return;
  }

  void sprintfDouble(char *str,  size_t size, double d)
  {
    // I realized it should be %f for both float and double. This means  sprintfFloat, sprintfDouble, sprintfReal 
    // are reduntant /Erik Sjolund 20009-12-11
    int ret = snprintf(str, size, "%f",d);
    if ( ret >= int (size)) { 
      fprintf(stderr,"Programming error: the stringbuffer was too small\n"); abort(); // was exit(1); 
    }
    return;
  }


  double xmlReadDouble( const xmlChar *str) {
    char *endptr;
    double ret = strtod(( const char *)str, &endptr);
    errno = 0;
    if ( errno || endptr == ( const char *) str  || '\0' != *endptr ) {  
      fprintf(stderr,"Error: could not convert from string to double\n" ); abort(); // was exit(1); 
    }
    return ret;
  }

  float xmlReadFloat( const xmlChar *str) {
    char *endptr;
    float ret = strtof(( const char * ) str, &endptr);
    errno = 0;
    if ( errno || endptr ==  ( const char *) str  || '\0' != *endptr ) {  
      fprintf(stderr,"Error: could not convert from string to float\n" ); abort(); // was exit(1); 
    }
    return ret;
  }

  int xmlReadInt( const xmlChar *str) {
    char *endptr;
    errno = 0;
    long valueLong;
    int valueInt;
    valueLong = strtol((const char *) str, &endptr,10);

    if ( ERANGE == errno  || valueLong > INT_MAX || valueLong < INT_MIN || endptr == ( const char * ) str || '\0' != *endptr ) {
      fprintf(stderr,"error parsing int\n"); abort(); // was exit(1);
    }
    valueInt = static_cast<int>(valueLong);
    return valueInt;
  }






  std::string 
  indentString(std::string s, const std::string& indent)
  {
    std::string::size_type pos = s.find("\n");
    while(pos < s.size() - 1)
      {
	s.insert(pos + 1, indent);
	pos = s.find("\n", pos + 1);
      }
    s.insert(0, indent);
    return s;
  }

  void 
  capitalize(std::string& s) 
  {
    std::transform(s.begin(), s.end(), s.begin(), (int(*)(int))toupper);
    return;
  }

  void 
  capitalize(char* s) 
  {
    while(*s != '\0')
   {   
      *s = toupper( *s ); 

      s++; 
   }
  }

  Real 
  pow(const Real& p, const unsigned& n)
  {
    int i = static_cast<int>(n);
    if(i < 0)
      {
	ostringstream oss;
	oss << "beep::pow(Real, unsigned)\n"
	    << " Ooops! unsigned "
	    << n 
	    << " became negative int " 
	    << i
	    << "!";
	throw AnError(oss.str(), 1);
      }
    return std::pow(p,i);
  }
 
  std::string 
  typeid2typestring(std::string t)
  {
    if(t == "i")
      return "int";
    else if(t == "j")
      return "unsigned int";
    else if(t == "b")
      return "bool";
    else if(t == "d")
      return "double";
    else if(t == "Ss")
      return "std::string";
    else
      {
	std::cerr << "typeid " + t + 
	  " not recogized by typeid2typstring\n";
	throw bad_typeid();
	return "Error";
      }
  }

  
}//end namespace beep
