#ifndef TOPTIMEMCMC_HH
#define TOPTIMEMCMC_HH

#include <string>
#include <ostream>

#include "StdMCMCModel.hh"
#include "Tree.hh"

namespace beep
{
  //! This actually represent a rather abstract variable representing a
  //! time preceeding than the first branching point in the guest tree
  //! It is definitely not related to the host tree (except that it must
  //! precede - or equal  - its root). Thus the class should be renamed
  //! and the variable should not be stored in S's root time. /bens}
  //! Author: Bengt Sennblad, � the MCMC-club, SBC, all rights reserved

  class TopTimeMCMC : public StdMCMCModel
  {
  public:
    //-------------------------------------------------------------
    //
    // Construct/Destruct/Assign
    //
    //-------------------------------------------------------------
    TopTimeMCMC(MCMCModel& prior, Tree& S, const Real beta);
    ~TopTimeMCMC();

    //-------------------------------------------------------------
    //
    // Interface
    //
    //-------------------------------------------------------------
    void fixTopTime();
    Real& getTopTime();
    void setTopTime(Real t);
    void multiplySuggestionVariance(Real multiplier)
    {
      suggestion_variance = multiplier * max;
    }

    MCMCObject suggestOwnState();
    void commitOwnState();
    void discardOwnState();
    std::string ownStrRep() const;
    std::string ownHeader() const;
    Probability updateDataProbability();

    
    //-------------------------------------------------------------
    // I/O
    //-------------------------------------------------------------
    friend std::ostream& operator<<(std::ostream &o, const TopTimeMCMC& A);
    std::string print() const;

  private:

    //-------------------------------------------------------------
    //
    // Attributes
    //
    //-------------------------------------------------------------
    Real time;
    Real beta;
    Real max;
    Real oldRootTime;

    bool estimateTopTime;
    Real suggestion_variance;
  };}
  //end namespace beep

#endif
