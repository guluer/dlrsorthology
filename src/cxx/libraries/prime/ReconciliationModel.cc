#include "ReconciliationModel.hh"

#include "AnError.hh"
#include "BirthDeathProbs.hh"
#include "Node.hh"
#include "StrStrMap.hh"
#include "Tree.hh"
#include "TreeAnalysis.hh"

#include <cassert>

// Author: Lars Arvestad, � the MCMC-club, SBC, all rights reserved
namespace beep
{
  using namespace std;

  //------------------------------------------------------------------------
  //
  // Construct/Destruct/Assign
  //
  //------------------------------------------------------------------------

  ReconciliationModel::ReconciliationModel(Tree &G_in, 
				 StrStrMap &gs_in,
				 BirthDeathProbs &bdp_in)
    : G(&G_in),
      S(&bdp_in.getStree()),   // S taken from bdp
      gs(&gs_in),
      bdp(&bdp_in), 	   
      sigma(G_in, *S, gs_in),
      gamma(G_in, *S, sigma), //empty gamma
      gamma_star(GammaMap::MostParsimonious(G_in, *S, sigma)),
      slice_L(*S, G_in)
  {
    //    inits();		   // Compute important helper params
  }


  ReconciliationModel::ReconciliationModel(Tree &G_in, 
					   StrStrMap &gs_in,
					   BirthDeathProbs &bdp_in,
					   vector<SetOfNodes>& AC)
    : G(&G_in),
      S(&bdp_in.getStree()),   // S taken from bdp
      gs(&gs_in),
      bdp(&bdp_in), 	   
      sigma(G_in, *S, gs_in),
      gamma(G_in, *S, sigma, AC),
      gamma_star(GammaMap::MostParsimonious(G_in, *S, sigma)),
      slice_L(*S, G_in)
  {
    //    inits();		   // Compute important helper params
  }


  ReconciliationModel::ReconciliationModel(const ReconciliationModel &M)
    : G(M.G),
      S(M.S),
      gs(M.gs),
      bdp(M.bdp),
      sigma(M.sigma),
      gamma(M.gamma),
      gamma_star(M.gamma_star),
      isomorphy(M.isomorphy),
      slice_U(M.slice_U),
      slice_L(M.slice_L)
  {
  }

 
  ReconciliationModel::~ReconciliationModel()
  {
    //  delete gamma;
  }


  ReconciliationModel &
  ReconciliationModel::operator=(const ReconciliationModel &M)
  {
    if (this != &M)
      {
	G = M.G;
	S = M.S;
	gs = M.gs;
	bdp = M.bdp;
	sigma = M.sigma;
	gamma = M.gamma;
	gamma_star = M.gamma_star;
	isomorphy = M.isomorphy;
	slice_U = M.slice_U;
      }

    return *this;
  }


  //------------------------------------------------------------------------
  //
  // Interface
  //
  //------------------------------------------------------------------------

  //------------------------------------------------------------------------
  // Access and manipulation
  //------------------------------------------------------------------------
  Tree &
  ReconciliationModel::getGTree() const
  {
    return *G;
  }


  Tree &
  ReconciliationModel::getSTree() const
  {
    return *S;
  }

  BirthDeathProbs& 
  ReconciliationModel::getBirthDeathProbs()
  {
    return *bdp;
  }

  // Returns \f[\gamma\f] curently saved in attribute gamma
  const GammaMap& 
  ReconciliationModel::getGamma()
  {
    return gamma;
  }

  // Sets \f$\gamma\f$ curently saved in attribute gamma to copy 
  // argument
  void
  ReconciliationModel::setGamma(const GammaMap& newGamma)
  {
    gamma = newGamma;
    inits();
    return;
  }

  // Guess starting rates from the height of the species tree and 
  // some likelihood hints from gene tree.
  //
  // Algorithm:
  // Firstly, birth and death rate will be quite similar, and without
  // lossing much precision, we can decide that they are equal.
  // Secondly, what we are after is to get values on the rates that
  // are in the right ballpark. Therefore, we try values 10^x, where x
  // goes from -2 to 3 and choose the rate which gives the best
  // likelihood.
  //
  // The rates are also normalized by the time from the root of the
  // species tree to the present, thereby removing scale problems.
  //--------------------------------------------------------------------
  void
  ReconciliationModel::chooseStartingRates()
  {
    Real Lambda;
    Real Mu;
    chooseStartingRates(Lambda, Mu);
    bdp->setRates(Lambda, Mu);
    bdp->update();
    return;
  }


  void
  ReconciliationModel::chooseStartingRates(Real &birthRate, Real &deathRate)
  {
    Real height = S->rootToLeafTime();

    if(height <= 0.0 && S->getNumberOfLeaves() == 1)
      {
	height = S->getTopTime();
      }
    if(height <= 0.0)
      {
	throw AnError("ReconciliationModel:\n"
		      "Height of species tree is not a positive value!", 1);
      }

    Real rootTime = S->getTopTime();
    if (rootTime == 0.0)
      {
	S->setTopTime(height / 10.0); //! A bit arbitrary, but OK.
	height *= 1.1;
      }

    Real rate = 0.001 / height;
    bdp->setRates(rate, rate);
    Probability best_likelihood = calculateDataProbability();
    Real best_rate = rate;

    // If we don't start with MAX_INTENSITY / 2, then the setRates method 
    // throws an error, protesting the rates are too low. The setRates call
    // checks too see if the suggested rate is too high, and when 
    // factor == MAX_INTENSITY we are close to being too high.
    // Because of a tiny numerical inprecision in the division, we 
    // accidentally get over the line to "illegal territory". To avoid this, 
    // we start with MAX_INTENSITY / 2. There is probably no reason to have
    // a higher rate anyway.  /arve
    for (Real factor = (MAX_INTENSITY / 2); factor > 0.01; factor /= 2)
      {
	rate = factor / height;
	bdp->setRates(rate, rate);
	Probability L = calculateDataProbability();
	if (L > best_likelihood) 
	  {
	    best_likelihood = L;
	    best_rate = rate;
	  }
      }

    bdp->setRates(best_rate, best_rate);
    birthRate = best_rate;
    deathRate = best_rate;
  }

  // always return 1.0
  Probability 
  ReconciliationModel::calculateDataProbability()
  {
    return 1.0;
  }

  // For when G has changed
  //------------------------------------------------------------------------
  void
  ReconciliationModel::update()
  {
    // We must assume that G and S are unchanged 
//     if(S->perturbedTree())
//       {
// 	sigma.update(*G, *S, gs);
// 	gamma_star = GammaMap::MostParsimonious(*G, *S, sigma); 
// 	gamma = gamma_star;
//       }
//     else if(G->perturbedTree())
//       {
// 	sigma.update(*G, *S);
// 	gamma_star = GammaMap::MostParsimonious(*G, *S, sigma); 
// 	gamma = gamma_star;
//       }
    inits();
  }

  //-------------------------------------------------------------------
  //
  // I/O
  //
  //-------------------------------------------------------------------
  std::ostream& 
  operator<<(std::ostream &o, const ReconciliationModel& pm)
  {
    return o << "ReconciliationModel: base class for reconciliation models\n"
	     << pm.print();
  };
  
  string 
  ReconciliationModel::print() const
  {
    std::ostringstream oss;
    oss << G->getName()
	<< " (guest tree)\n"
	<< gamma.print(true)
	<< " (reconciliation)\n"
	<< bdp->print();
    return oss.str();
  }
  
    // Fill in slice_L using a Depth-First-Search (DFS) through the gene tree.
  // For each gene node, walk upward in the species tree using
  // the lowest possible species node, as found i sigma[v], as
  // a starting point.
  //---------------------------------------------------------------
  void
  ReconciliationModel::computeSliceSizeLowerBound(Node *u)
  {
    Node *x = sigma[u];
  
    if (u->isLeaf())
      {
	// y = x is the case of a single lineage slice ending in x.
	// The other cases, when y >_S x, represents single lineages where
	// u is acting as a representant for an "unobserved" unary node.
	for (Node *y = x; y != NULL; y = y->getParent())
	  {
	    slice_L(y, u) = 1;
	  }
      }
    else 
      {
	Node *left = u->getLeftChild();
	Node *right = u->getRightChild();

	// Recurse
	computeSliceSizeLowerBound(left);
	computeSliceSizeLowerBound(right);

	// Handle current level
	// It is only when x is in the most parsimonious reconciliation
	// that it can be a single gene lineage. Otherwise, there is a
	// node w under it with $\sigma(u) = \sigma(w)$, so u cannot be
	// a speciation.
	if (gamma_star.isInGamma(u, x))
	  {
	    slice_L(x, u) = 1;
	  }
	else
	  {
	    slice_L(x, u) = slice_L(x, left) + slice_L(x, right);
	  }

	// The other cases, when y >_S x, represents single lineages where
	// u is acting as a representant for an "unobserved" unary node.
	for (Node *y = x->getParent(); y != NULL; y = y->getParent())
	  {
	    slice_L(y, u) = 1;
	  }
      }  
  }

  //-----------------------------------------------------------------------
  //
  // Implementation
  //
  //-----------------------------------------------------------------------
  //Initialize some basic structures.
  void 
  ReconciliationModel::inits()
  {
    TreeAnalysis TA(*G);
    isomorphy = TA.isomorphicSubTrees(sigma);

    // We need a lower (slice_L) and an upper (slice_U) bound on
    // $|L(G_{u,\gamma(x)})$ for all $u\in V(G), x\in V(S)$
    // gene nodes.
    slice_U = TA.subtreeSize(); // Upper bound is simply $|L(G_u)|$
    computeSliceSizeLowerBound(G->getRootNode());  // fill in slice_L

  }


}// end namespace beep
