#ifndef SEQFILE
#define SEQFILE

#include <stdio.h>
#include "entry.h"

typedef struct entry seq;	/* Backward compatability. seq is new type name */
struct _sfile {
    FILE *file;
    int flex_start_condition;
/*     YY_BUFFER_STATE flex_buffer_state;  */
    void* flex_buffer_state; 
    seq *entry_list;		/* Used to interface between seq_read */
				/* and end_parse_entry, which is called */ 
				/* by yyparse at the end of each entry */
				/* in the stream. */
};

typedef struct _sfile sfile;

sfile * seq_open(char *filename, char *type); /* type is "r" for read, etc */
sfile * seq_file2sfile(FILE *f);
void    seq_close(sfile *sf);
seq *   seq_read(sfile *sf, int n, int *listsize); /* BUG: n is not used */
seq *   seq_read_all(sfile *sf, int *listsize);

seq * seq_new_sequence(char *locus, char *type, char *definition, unsigned length);
seq * seq_append(seq *head, seq *tail);
void  seq_free(seq *entries);

char * seq_locus(seq *ent);
seq * seq_find_locus(seq *ent, char *locus);
int seq_seq_length(seq *e);
char seq_seq_site(seq *e, int i);
seq* seq_next_entry(seq *e);
unsigned seq_entry_list_length(seq *list);


void seq_add_comment(seq *e, char *s);
void seq_set_definition(seq *e, char *s);
void seq_set_accesssion(seq *e, char *s);

void seq_print(seq *ent);

struct parser_control {
   struct entry *entry_list;	/* To export the list of sequences */
   int n_entries;
   int n_wanted_entries;
};

#define WARNING(S) {fprintf(stderr, "%s  (%s:%d)\n", S, __FILE__, __LINE__);}

//#define FATAL_ERROR(S,C) {fprintf(stderr, "%s (%s:%d)\n", S, __FILE__, __LINE__); exit(C);}

#endif 
