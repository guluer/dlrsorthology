/*
  F�r att komma tillr�tta med minnesl�ckor ska jag g� �ver till att skicka v�rden
  via $$, $1, $2 osv. Problemet �r f�rst�s att det mesta data �r str�ngar. 

  Jag borde ha en datastruktur som 
  struct string {
     int len;			/o F�s automatiskt mha yylen o/
     char *data;
     struct string *previous;
  }
  som jag kan samla ihop sekvensinfo i. Pga yacc-tolkningen s� �r det enklare att prata om 
  previous �n om next.
  
  Ett annat alternativ �r att l�gga str�ngsnuttarna p� en stack eller anna lurig
  datastruktur.
 */

%{
  /* C declarations */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "entry.h"
#include "sfile.h"


static int yyerror();

#define YYDEBUG 1
extern int debug;
extern int yyleng;

//#define YYSTYPE char 

static struct entry *current_entry;

typedef enum {cont_parsing, stop_parsing} parse_status;

struct string_part {
  int length;
  char *data;
  char *info_type; /* String describing what kind of string it is: sequence, description, accession etc */
  struct string_part *previous;
};


struct entry*       new_entry(char *locus, struct string_part *sp);
struct entry*       get_seq_list();
void                add_to_seq(char *locus);
void                set_seq_length(int length);
struct string_part* new_string_part(char *, struct string_part*, char *);
struct string_part* append_string_part(struct string_part*,struct string_part*);
char*               string_parts_to_str(struct string_part *sp);


/* OLD void begin_entry(); */
/* parse_status end_entry(); */

/* #define YYPARSE_PARAM parm */


%}

/* %pure_parser */

%union {
  struct entry *entry;
  struct string_part *strpart;          /* To collect fragments of strings in */
  char         *str;			/* Sequences and names */
  int           integer;
};

%start seq_file
/* %start seq_entries */
%token BASEPAIRS
%token END_OF_ENTRY 
%token <str> NAME
%token NO_MORE_ENTRIES
%token <integer> NUMBER 
%token LOCUS
%token ORIGIN
%token SEQUENCE_PART 
%token WHITESPACE
%token <str> TEXT
%token DEFINITION
%token ACCESSION
%token COMMENT

/*
%token EMBL_LOCUS
%token EMBL_END_OF_ENTRY
%token EMBL_DEFINITION
%token EMBL_ACCESSION
%token EMBL_SEQUENCE_HEAD
%token EMBL_SEQUENCE_PART
%token EMBL_EOL
*/

%token DARWIN_TOKEN
%token DARWIN_END_TOKEN
%token DARWIN_EOL
%token DARWIN_ELEMENT

%token FASTA_START
%token FASTA_SEQ_START

%token <str> IG_SEQ_PART
%token <str> IG_START
%token IG_END_OF_ENTRY

%token PHYLIP_INTERLEAVED
%token PHYLIP_INTERLEAVED_DIVIDER


/* %type <str> */
%type <entry>   seq_entries seq_entry
%type <entry>   gb_entry fasta_entry ig_entry darwin_entry
/*%type <entry>   embl_entry*/
/* %type <integer>  */

/* GenBank related */
%type <strpart> gb_sequence_parts gb_sequence_line gb_sequence
%type <strpart> gb_comment_field gb_comment_lines
%type <strpart> gb_info_field gb_info_fields gb_accession_line gb_definition_line
%type <str>     gb_locus_line

/* FASTA related */
%type <strpart> fasta_sequence_parts fasta_description

/* IG related */
%type <strpart> ig_sequence ig_sequence_line

%%
seq_file        : seq_entries { current_entry = $1; }
                ;

seq_entries	: {$$ = NULL;}
		| seq_entries seq_entry { if ($2) {$2->next = $1;} $$ = $2; }
		;
		
seq_entry	: gb_entry
                | fasta_entry 
                | ig_entry 
/* 		| embl_entry */
/*                 | darwin_entry */
/*		| gde_entry*/
/*		| gde_flat_entry*/
		;


gb_entry	: LOCUS gb_locus_line
		  gb_info_fields
		  ORIGIN
		  gb_sequence
		  END_OF_ENTRY {}
		;

gb_locus_line	: WHITESPACE NAME 
  		  WHITESPACE NUMBER 
		  WHITESPACE BASEPAIRS { $$ = $2; }
		| WHITESPACE NUMBER 
  		  WHITESPACE NUMBER 
		  WHITESPACE BASEPAIRS { $$ = strdup("Number"); }
		;
gb_info_fields	: {$$ = NULL; }
		| gb_info_fields gb_info_field { $$ = append_string_part($2, $1); } 
		;

gb_info_field	: gb_definition_line
		| gb_accession_line
		| gb_comment_field
                | error {$$ = NULL; }
		;

gb_definition_line	: DEFINITION { $$ = NULL; }
			| DEFINITION TEXT { $$ = new_string_part($2, NULL, "Definition"); }
			;

gb_accession_line	: ACCESSION { $$ = NULL; }
			| ACCESSION TEXT { $$ = new_string_part($2, NULL, "Accession"); }
			;

gb_comment_field	: COMMENT gb_comment_lines {$$ = $2; }
	 		; 

gb_comment_lines	: {$$ = NULL; }
			| gb_comment_lines TEXT { $$ = new_string_part($2, $1, "Comment"); }
			;

gb_sequence	: gb_sequence_line
  		| gb_sequence gb_sequence_line
		;

gb_sequence_line	: NUMBER gb_sequence_parts { $$ = $2; }
			;

gb_sequence_parts	: SEQUENCE_PART { add_to_seq(yylval.str); }
			| gb_sequence_parts SEQUENCE_PART { add_to_seq(yylval.str); }
			;

/*
embl_entry	: EMBL_LOCUS embl_locus_line
		  embl_info_lines
		  embl_sequence_start embl_sequence
		  EMBL_END_OF_ENTRY {}
		;

embl_locus_line	: NAME { begin_entry(yylval.str); }
		;

embl_info_lines	:
		| embl_definition
		| embl_accession
		;

embl_definition	: EMBL_DEFINITION TEXT { seq_set_definition(current_entry, parm.str); }
		;

embl_accession	: EMBL_ACCESSION TEXT { seq_set_accession(current_entry, parm.str); }
		;

embl_sequence_start	: EMBL_SEQUENCE_HEAD 
		 	  NUMBER { set_seq_length(atoi(yylval.str)); }
			  BASEPAIRS
			;

embl_sequence	: embl_sequence_line
		| embl_sequence embl_sequence_line
		;

embl_sequence_line	: embl_sequence_parts EMBL_EOL
			;

embl_sequence_parts	: EMBL_SEQUENCE_PART { add_to_seq(yylval.str); }
			| embl_sequence_parts EMBL_SEQUENCE_PART
				{ add_to_seq(yylval.str); }
			;
*/


fasta_entry	: FASTA_START TEXT 
                  fasta_description 
 	          FASTA_SEQ_START fasta_sequence_parts 
                  { 
		    $$ = new_entry($2, $5);
		    seq_set_definition($$, string_parts_to_str($3));
                  }
		;

fasta_description	: { $$ = NULL; }
		        | TEXT { $$ = new_string_part($1, NULL, "Description"); }
		        ;

fasta_sequence_parts	: TEXT { $$ = new_string_part($1, NULL, "Sequence"); }
			| fasta_sequence_parts TEXT { $$ = new_string_part($2, $1, "Sequence"); }
			;

darwin_entry	: TEXT {$$ = NULL; /* Not implemented yet */}
		;

ig_entry        : IG_START 
		  ig_sequence 
		  IG_END_OF_ENTRY { $$ = new_entry($1, $2); }
		;

ig_sequence     : ig_sequence_line 
                | ig_sequence ig_sequence_line
                ;

ig_sequence_line: IG_SEQ_PART { add_to_seq(yylval.str); }
                ;

/* phylip_entry : phylip_first_block */
/*                phylip_other_blocks */
/*              ; */

/* phylip_first_block :  */
/* 		   ; */

/* phylip_other_blocks : phylip_block */
/* 		    | PHYLIP_INTERLEAVED_DIVIDER */
/*                       phylip_other_blocks */
/* 		    ; */

%%

/* C code */

int
yyerror(char *msg) {
       printf("Can't parse file. Error is '%s', on line %d.\n", msg, linenumber());
       return(1);
}


/*
void
current_string(char *s)
{
  string = strdup(s);
/o    fprintf(stderr, "cur str = %s\n", s);  o/
}
*/

/*
 *   This creates and initializes an entry structure
 */
struct entry *
new_entry (char *locus, struct string_part *sp)
{
   struct entry   *ent;

   ent = (struct entry *) malloc (sizeof(struct entry));
   if (ent)
      {
	  ent->locus = locus;
	  *ent->moltype = '\0';
	  *ent->topology = '\0';
	  *ent->endiv = '\0';
	  *ent->distdate = '\0';
	  *ent->definition = '\0';
	  *ent->accession = '\0';
	  ent->segment = -1;
	  ent->segs = -1;
	  *ent->source = '\0';
	  *ent->genusspec = '\0';
	  *ent->origin = '\0';
	  ent->sequence = string_parts_to_str(sp);
	  ent->seqlen = strlen(ent->sequence);
	  ent->secaccs = NULL;
	  ent->keywords = NULL;
	  ent->taxes = NULL;
	  ent->references = NULL;
	  ent->comments = NULL;
	  ent->featoccs = NULL;
	  ent->next = NULL;
      }
   else
      {
      fprintf (stderr, "Out of memory while parsing..\n");
      abort(); // was before exit(1);
      }

   return (ent);
}

/* void */
/* begin_entry(char *name) { */
/*     current_entry = new_entry(name, NULL); */
/* } */

/* parse_status */
/* end_entry(struct parser_control *params) { */
/*   struct entry *e; */
/*   int length; */

/*   /p Make sure the length is OK o/ */
/*   length = strlen(current_entry->sequence); */
/*   if (length != current_entry->seqlen) { */
/*       fprintf(stderr, "Warning: Given length (%d) of sequence %s differs from actual length (%d). Length is corrected.\n", current_entry->seqlen, current_entry->locus, length); */
/*       current_entry->seqlen = length; */
/*   } */
  
/*   /o Append entry to list o/ */
/*   e = params->entry_list; */
/*   if (e) { */
/*     while (e->next) { */
/*       e = e->next; */
/*     } */
/*     e->next = current_entry; */
/*   } else { */
/*     params->entry_list = current_entry; */
/*   } */
/*   params->n_entries++; */
/*   if (params->n_entries == params->n_wanted_entries) { */
/*       return stop_parsing; */
/*   } else { */
/*       return cont_parsing; */
/*   } */
/* } */

void
set_seq_length(int l) {
  if (current_entry) {
    current_entry->seqlen = l;
    if (!current_entry->sequence) {
      current_entry->sequence = (char *) malloc(l+1);
      current_entry->sequence[0] = '\0';
    }
  } else {
    fprintf(stderr, "Something went wrong internally. Aborting.\n");
    abort(); // was before exit(2);
  }
}


void
add_to_seq (text)
    char *text;
{
  int   n = 0, textlen;
  char *new_buf;

  textlen = strlen(text);
  if (current_entry->sequence == NULL) {
      set_seq_length(textlen);
  }
  n = strlen(current_entry->sequence);
  if (n + textlen > current_entry->seqlen) {
      current_entry->sequence = (char *) realloc(current_entry->sequence, (n+textlen+1) * sizeof(char));
      current_entry->seqlen = n + textlen;
  }

  sprintf (current_entry->sequence + n, "%s", text);

/*   fprintf(stderr, "Added: %s\n", text);   */
}


/*
struct entry *
gb_read() {
#ifdef YYDEBUG
  if (debug) {
    yydebug = 1;
  }
#endif

  entry_list = NULL;
  if (yyparse() == 0) {		
    return entry_list;
  } else {
    return NULL;
  }
}

*/
/* 
 * Return a pointer to the locus name. 
 * Don't mess with the character field!
 */
/*
char*
gb_locus(struct entry *ent) {
  if (ent) {
    return ent->locus;
  } else { 
    return NULL;
  }
}

struct entry *
gb_find_locus(struct entry *ent, char *locus) {
  struct entry *i; 

  for (i = ent; i != NULL; i = i->next) {
    if (!strcmp(locus, i->locus)) {
      return i;
    }
  }
  return NULL;
}

*/
/*
 * Returns length of the sequence, -1 if argument == NULL.
 */
/*
int
gb_seq_length(struct entry *e) {
  if (e) {
    return e->seqlen;
  } else { 
    return -1;
  }
}
  
*/
/*
 * Returns the character at site i in sequence e
 */
/*
char
gb_seq_site(struct entry *e, int i) {

  if (e) {
    if ((i >= 0) && (i < gb_seq_length(e))) {
      return e->sequence[i];
    } else {
      fprintf(stderr, "Out of bounds when trying to read site %d of sequence %s.\n", i, gb_locus(e));
    abort(); // was before exit(1);     
    } 
  } else {
    fprintf(stderr, "Tried using an empty locus entry. Aborting.\n");
     abort(); // was before exit(1);
  }
}
     

struct entry*
gb_next_entry(struct entry *e) {
  return e->next;
}


unsigned
gb_entry_list_length(struct entry *list) {
  unsigned length = 0;
    
  while (list) {
    list = gb_next_entry(list);
    length++;
  }

  return length;
}

*/


/* BUG: Should not ignore info_type! */
struct string_part*
new_string_part(char* s, struct string_part* prev, char *info_type)
{
  struct string_part *sp = malloc(sizeof(struct string_part));
  if (sp) {
    sp->length = strlen(s);
    sp->data = s;
    sp->previous = prev;
    return sp;
  } else {
    return NULL;
  }
}

char *
recursive_fill(struct string_part *sp, char *str) {
  if (sp) {
    str = recursive_fill(sp->previous, str);
    strncpy(str, sp->data, sp->length);
    return str + sp->length;
  } else {
    return str;
  }
}
  
char*
string_parts_to_str(struct string_part *sp) {
  struct string_part *tmp = sp;
  int len = 0;
  char *str = NULL;

  while (tmp) {
    len += tmp->length;
    tmp = tmp->previous;
  }

  if (len > 0) {
    str = malloc((len + 1) * sizeof(char));

    recursive_fill(sp, str);
    str[len] = '\0';
  } 
  return str;
  
}  

    
struct string_part *
append_string_part(struct string_part *sp1, struct string_part *sp2) {
  struct string_part *tmp = sp1;
  if (tmp) {
    while (tmp->previous != NULL) {
      tmp = tmp->previous;
    }
    tmp->previous = sp2;
    return sp1;
  } else if (sp2) {
    tmp = sp2;
    while (tmp->previous != NULL) {
      tmp = tmp->previous;
    }
    tmp->previous = sp1;
    return sp2;
  } else {
    return NULL;
  }
}

struct entry* get_seq_list()
{
  return current_entry;
}

