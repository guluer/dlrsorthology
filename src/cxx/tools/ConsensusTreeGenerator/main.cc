#include <iostream>
#include <vector>
#include <fstream>

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>

#include "Tree.hh"
#include "Node.hh"
#include "Tokenizer.hh"
#include "TreeIO.hh"
#include "StrStrMap.hh"
#include "SetOfNodes.hh"
#include "Probability.hh"
#include "VirtualVertex.hh"
#include "ConsensusNode.hh"
#include "ConsensusTree.hh"
#include "AnError.hh"

using namespace std;
using namespace beep;

// The first Probability in pair is Posterior Probability
// The second Probability in pair is Speciation Probability
typedef map< VirtualVertex, pair<Probability, Probability> > vvmap;
typedef vvmap::iterator vvmapel_pt;
#define foreach BOOST_FOREACH
#define SAMPLE_LENGTH 3000

void create_v_vertices(vector<vvmapel_pt> &vv, Tree tree,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices, map<int, double> vv_probs);

void
find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &ID,
        map<string, int> &inv_ID,
        GeneSet &part);

void
find_vertices_single(vvmap &vertices,
        Tree &tree,
        //EdgeDiscGSR* state,
        map<int, string> &ID,
        map<string, int> &inv_ID);

void
create_lookup_tables(map<int, string> &ID,
        map<string, int> &inv_ID,
        StrStrMap &gs_map);

Node* find_lca(SetOfNodes &nodes, Tree &T);

int get_lca_id(VirtualVertex vv, map<int, string> ID, Tree G);

map<int, double> read_gem_file(string gem_file);

static void build_consensus_tree(ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        map<int, string> &D);

static void build_consensus_tree_rec(GeneSet &D,
        ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        ConsensusNode *c,
        map<int, string> &ID);

static void find_mvvs(vector<vvmapel_pt> &mvvs, vector<vvmapel_pt> &V);

int main(int argc, char *argv[]) {
    if(argc != 5){
        cerr << "Error: Wrong number of arguments\n" << endl;
        cerr << "Usage: " << argv[0] << " g_file_name gs_file_name "
                << "gem_file_name cons_tree_out_file" << endl;
        cerr << "g_file_name: name of gene tree file \n"
                << "gs_file_name: name of gene species mapping file \n"
                << "gem_file_name: name of file containing values from PrIME_GEM posteriror file\n"
                << "cons_tree_out_file: name of output file for consensus tree\n";
        exit(EXIT_FAILURE);
    }
    
    StrStrMap *gsMap = new StrStrMap();
    string dir = "/home/ikramu/PrIME/trunk/src/cxx/tools/ConsensusTreeGenerator/data/";
    string g_file_name(argv[1]); // = dir + string("abca.gtree");
    string gs_file_name(argv[2]); // = dir + string("abca.gsmap");  
    string gem_file(argv[3]); // = dir + string("abca_fixed.gem");
    string cons_file(argv[4]); // = gem_file + string(".fig");
    map<int, string> ID;
    map<string, int> inv_ID;
    vvmap vertices;  // all internal vertices
    vector<vvmapel_pt> v_vertices; //virtual vertices
    ConsensusTree ctree; //Consensus tree

    Tree gene_tree = TreeIO::fromFile(g_file_name).readBeepTree(NULL, gsMap);    
    *gsMap = TreeIO::fromFile(g_file_name).readGeneSpeciesInfo(gs_file_name);

    map<int, double> values = read_gem_file(gem_file);

    create_lookup_tables(ID, inv_ID, *gsMap);
    
    create_v_vertices(v_vertices, gene_tree, ID, inv_ID, vertices, values);    

    /* now create the consensus tree */
    cout << "Building consensus tree..  " << endl;
    build_consensus_tree(ctree, v_vertices, ID);
    
    cout << "Drawing tree and saving to file " << cons_file;    
    ofstream file(cons_file.c_str());
    ctree.drawTreeSVG(file,true);
    file.close();
    cout << "  done!" << endl;

    return 0;
}

map<int, double> read_gem_file(string gem_file) {
    map<int, double> val;
    ifstream in_file;
    string line = "";

    in_file.open(gem_file.c_str());
    if (in_file.is_open()) {
        try {
            while (!in_file.eof()) {
                getline(in_file, line);

                /* for some bullshit reason, we have to check for empty string */
                if (line.size() < 1)
                    continue;

                Tokenizer tok("\t");
                tok.setString(line);
                string key = tok.getNextToken();
                int sstart = key.find_first_of("[") + 1;
                int n = key.find_first_of("]") - sstart;
                int vv_id = atoi(key.substr(sstart, n).c_str());
                double prob = atof(tok.getNextToken().c_str());
                cout << vv_id << ": " << prob << endl;
                val[vv_id] = prob;
            }
        } catch (exception &e) {
            cerr << e.what() << endl;
            exit(EXIT_FAILURE);
        }
    }
    return val;
}

void create_v_vertices(vector<vvmapel_pt> &vv, Tree tree,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices, map<int, double> vv_probs) {
    //vvmap vvertices;
    find_vertices_single(vertices, tree, ID, inv_ID);
    //cout << tree << endl;
    for (vvmap::iterator it = vertices.begin(); it != vertices.end(); it++) {
        int lca_id = get_lca_id(it->first, ID, tree);
        ///cout << lca_id << " ----- " << vv_probs[lca_id] << endl;
        Probability pp((double)1);
        Probability sp(vv_probs[lca_id]);
        //double spec_val = vv_probs[lca_id];
        it->second.second += sp;
        //it->second.second.
        //it->second.second.
        vv.push_back(it);
        cout << it->first << " => ";
        cout << lca_id << ";" << vv_probs[lca_id] << " - ";
        cout << it->second.first.getSign() << "," << it->second.second.getSign() << endl;
    }
    cout << "Number of internal vertices in MHC are " << vv.size() << endl;
}

/**
 * get the ID for LCA of virtual vertex
 * @param set gene set
 * @param ID int to string IDs of the leaf names
 * @param G gene tree
 * @return ID of the LCA
 */
int get_lca_id(VirtualVertex vv, map<int, string> ID, Tree G) {
    SetOfNodes lnodes, rnodes;
    vector<unsigned int> left = vv.getLeftDescendantPart().getGeneIDs();
    vector<unsigned int> right = vv.getRightDescendantPart().getGeneIDs();

    //foreach(int i, left) {
    for (unsigned int i = 0; i < left.size(); i++) {
        string g_name = ID[left[i]];
        lnodes.insert(G.findLeaf(g_name));
    }

    for (unsigned int j = 0; j < right.size(); j++) {
        string g_name = ID[right[j]];
        rnodes.insert(G.findLeaf(g_name));
    }

    Node *ln = find_lca(lnodes, G);
    Node *rn = find_lca(rnodes, G);
    //cout << "left id " << ln->getNumber() << ", right id " << rn->getNumber() << endl;
    Node *lca = G.mostRecentCommonAncestor(ln, rn);
    //cout << "lca is " << lca->getNumber() << endl;

    return lca->getNumber();
}

/**
 * find_lca
 *
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A vector of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node* find_lca(SetOfNodes &nodes, Tree &T) {
    Node *lca = nodes[0];
    for (unsigned int i = 1; i < nodes.size(); i++) {
        lca = T.mostRecentCommonAncestor(lca, nodes[i]);
    }
    return lca;
}

/**
 * create_lookup_tables
 *
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void
create_lookup_tables(map<int, string> &ID,
        map<string, int> &inv_ID,
        StrStrMap &gs_map) {
    int nr_genes; //Total number of genes
    string gene_name; //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    //cout << gs_map << endl;
    nr_genes = gs_map.size();
    //cout << "ID, GeneName" << endl;
    for (int i = 0; i < nr_genes; i++) {
        gene_name = gs_map.getNthItem(i);
        cout << i << " , " << gene_name << endl;
        ID.insert(pair<int, string > (i, gene_name));
        inv_ID.insert(pair<string, int>(gene_name, i));
    }
}

/*
 * Find all virtual vertices in the state and update counts.
 * The procedure is recursive and starts at the
 * root of the current gene tree.
 * @param vertices virtual vertex map
 * @param state EdgeDiscGSR object containing all the information
 * @param ID id to leaf name map
 * @param inv_ID leaf name to id map
 */

void
find_vertices_single(vvmap &vertices,
        Tree &tree,
        //EdgeDiscGSR* state,
        map<int, string> &ID,
        map<string, int> &inv_ID) {


    GeneSet g(0);
    //find_vertices_rec(vertices, state->getTree().getRootNode(), state, ID, inv_ID, g);
    find_vertices_rec(vertices, tree.getRootNode(), ID, inv_ID, g);
}

/**
 * find_vertices_rec
 *
 * Finds all virtual vertices in one GSR state recursively.
 *
 * @param vertices Storage for the set of virtual vertices upon return, including the count, for each
 *                  vertex, of how many states it was found in.
 * @param node Current node.
 * @param state The GSR state to consider.
 * @param ID id to gene name map.
 * @param inv_D gene name to id map.
 * @param gs_map gene to species map
 * @param part Gene set to add found leaves to.
 */
void
find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &ID,
        map<string, int> &inv_ID,
        GeneSet &part) {
    /*Error check*/
    if (node == NULL) {
        cerr << "Programming error in find_vertices_rec: node == NULL" << endl;
        exit(EXIT_FAILURE);
    }

    /*
     * If we have reached a leaf, add gene to part of parent vertex.
     */
    if (node->isLeaf()) {
        part.addGene(inv_ID[node->getName()]);
        return;
    }

    /*
     * Non-leaf node.
     * The general idea is to create a new virtual vertex and construct its
     * descendant parts recursively in each subtree of the current node.
     * Then we check if the virtual vertex has been found before. If there is an
     * old record of the virtual vertex, then we update it by:
     * - increasing the count of the virtual vertex.
     *
     * Otherwise we enter the newly found virtual vertex with count 1 to 
     * the set of virtual vertices.
     *
     * Also we add all genes found recursively to the descendant part of the
     * parent of the current node.
     */

    //Create new virtual vertex.
    VirtualVertex vv(ID.size());

    //Create descendant parts and construct them recursively.
    GeneSet left(ID.size()), right(ID.size());
    //find_vertices_rec(vertices, node->getLeftChild(), state, ID, inv_ID, left);
    //find_vertices_rec(vertices, node->getRightChild(), state, ID, inv_ID, right);
    find_vertices_rec(vertices, node->getLeftChild(), ID, inv_ID, left);
    find_vertices_rec(vertices, node->getRightChild(), ID, inv_ID, right);

    //Add descendant parts to the virtual vertex.
    //vv.setLeftDescendantPart(left);
    //vv.setRightDescendantPart(right);
    
    /* IMPORTANT 
     * I have found that existing setup of VirtualVertex doesn't know that 
     * if, for instance, v1=[1 4, 5] and v2=[5, 1 4] then v1 == v2.
     * For the time being, I am adding the condition that the left descendent 
     * of VirtualVertex will contain geneset with greater number of leaves than 
     * its right sibling (in other words, it would always be [1 4, 5] in above case
     * Ikram Ullah (Aug 22, 2013)
     */
    if(left.size() >= right.size()) {
        vv.setLeftDescendantPart(left);
        vv.setRightDescendantPart(right);
    } else {
        vv.setLeftDescendantPart(right);
        vv.setRightDescendantPart(left);
    }


    //Add genes found to the descendant part of the parent to the current node.
    if (!(node->isRoot())) {
        part.addGeneSet(left);
        part.addGeneSet(right);
    }

    //Update virtual vertex set
    /*
     * IMPLEMENTATION NOTE:
     * If vv does not exist in vertices, then the statement vertices[vv]
     * adds vv and the count and cumulative speciation probability
     * both defaults to 0. See documentation for std::map for more info.
     */
    vertices[vv].first += 1.0;
    vertices[vv].second += Probability(0.0);
}

/**
 * build_consensus_tree
 *
 * Constructs a majority rule consensus tree.
 *
 * @param ctree Storage for the final consensus tree.
 * @param hp_vertices A consistent set of virtual vertices with probabilities.
 * @param ID id to gene name map.
 *
 */
void
build_consensus_tree(ConsensusTree &ctree,
        vector<vvmapel_pt> &hp_vertices,
        map<int, string> &ID) {
    /*Create root node*/
    ConsensusNode* root = new ConsensusNode();
    ctree.setRoot(root);

    /*Create gene set with all genes*/
    GeneSet D = GeneSet(ID.size());
    for (unsigned int i = 0; i < ID.size(); i++) {
        D.addGene(i);
    }

    /*Create tree recursively*/
    build_consensus_tree_rec(D, ctree, hp_vertices, root, ID);
}

/**
 * build_consensus_tree_rec
 *
 * Constructs a consensus tree recursively. Used by build_consensus_tree.
 *
 * @param D Gene set for this level.
 * @param ctree Storage of final consensus tree.
 * @param V Current set of virtual vertices.
 * @param c Current node.
 * @param ID id to gene name map.
 * @author peter9
 *
 */
void
build_consensus_tree_rec(GeneSet &D,
        ConsensusTree &ctree,
        vector<vvmapel_pt> &V,
        ConsensusNode *c,
        map<int, string> &ID) {
    /*
     * Error check
     */
    assert(D.size() > 0);
    if (D.size() < 1) {
        throw AnError("Programming error in build_consensus_tree_rec: D is empty.");
    }

    /* 
     * Base cases
     */
    if (D.size() == 1) {
        //If c is the only node in the tree, stop and return.
        if (c->isRoot()) {
            return;
        }
        //Otherwise, we have encountered a leaf that must be added to the tree.

        //Change c to be a leaf
        vector<unsigned int> ids = D.getGeneIDs();
        c->setName(ID[ ids[0] ]);
        return;
    }
    if (V.size() == 0) {
        //Create new nodes for all remaining genes in D and set them as
        //children to the current node
        vector<unsigned int> ids = D.getGeneIDs();

        foreach(unsigned int id, ids) {
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName(ID[id]);
            //Create edge from c
            leaf->setParent(c);
            c->addChild(leaf);
        }
        return;
    }


    /*
     * Recursion step
     */

    //Find MVVs.
    vector<vvmapel_pt> mvvs;
    find_mvvs(mvvs, V);

    //Create combined gene set of the MVVs to check for left-out genes in D
    GeneSet Dprime(ID.size());

    foreach(vvmapel_pt pt, mvvs) {
        Dprime.addGeneSet(pt->first.getLeftDescendantPart());
        Dprime.addGeneSet(pt->first.getRightDescendantPart());
    }
    /*Error check*/
    if (D.size() < Dprime.size()) {
        throw AnError("Programming error in build_consensus_tree_rec. |D|<|Dprime|.");
    }
    //Check for left-out genes
    if (Dprime.size() < D.size()) {
        /*
         * Not every gene is covered by a virtual vertex.
         * We create an edge to every left-out gene from c then make a recursive
         * call with the other genes.
         */

        //Get left-out genes and create leaves for them.
        vector<unsigned int> ids = D.getSetDifference(Dprime).getGeneIDs();

        foreach(unsigned int id, ids) {
            //Create node for gene
            ConsensusNode *leaf = new ConsensusNode();
            leaf->setName(ID[id]);
            //Create edge
            leaf->setParent(c);
            c->addChild(leaf);
        }

        /*
         * Create a new node for all genes in Dprime and make a recursive call
         * with Dprime as the gene set.
         */
        //Create node for gene
        ConsensusNode *node = new ConsensusNode();
        //Create edge from c
        node->setParent(c);
        c->addChild(node);
        //Recursive call
        build_consensus_tree_rec(Dprime, ctree, V, node, ID);
        return;
    }


    /*
     * Now we now that |Dprime| == |D| and we do not have to consider
     * Dprime anymore.
     */

    //Create new virtual vertex sets for recursive calls. One pair for each MVV.

    foreach(vvmapel_pt mvv, mvvs) {
        //Create parent node for MVV, i.e. the parent of the MVVs children sets
        ConsensusNode *pnode;
        if (mvvs.size() == 1) {
            pnode = c;
        } else {
            pnode = new ConsensusNode();
            pnode->setParent(c);
            c->addChild(pnode);
        }

        //An MVV is always a high posterior vertex so add probabilities
        pnode->addProbabilities(mvv->second.first, mvv->second.second);

        //Create new children for each part of the MVV
        ConsensusNode *left = new ConsensusNode();
        ConsensusNode *right = new ConsensusNode();
        left->setParent(pnode);
        right->setParent(pnode);
        pnode->addChild(left);
        pnode->addChild(right);

        //Create new vertex sets for each part
        vector<vvmapel_pt> V_L, V_R;
        GeneSet mvv_left = mvv->first.getLeftDescendantPart();
        GeneSet mvv_right = mvv->first.getRightDescendantPart();
        //Place all virtual vertices in V that are dominated by the MVV
        //in the correct subset

        foreach(vvmapel_pt vv, V) {
            GeneSet uni = vv->first.getLeftDescendantPart().getSetUnion(
                    vv->first.getRightDescendantPart());
            if (uni.subsetOf(mvv_left)) {
                V_L.push_back(vv);
            } else if (uni.subsetOf(mvv_right)) {
                V_R.push_back(vv);
            }
        }
        //Make recursive calls
        build_consensus_tree_rec(mvv_left, ctree, V_L, left, ID);
        build_consensus_tree_rec(mvv_right, ctree, V_R, right, ID);
    }

}

/**
 * find_mvvs
 *
 * Finds all MVVs in a set of virtual vertices.
 *
 * @param mvvs Storage for found MVVs. Assumed to be intitially empty.
 * @param V A set of virtual vertices.
 * @author peter9
 */
void
find_mvvs(vector<vvmapel_pt> &mvvs, vector<vvmapel_pt> &V) {
    /*Error check*/
    if (V.size() <= 0) {
        throw AnError("Error in find_mvvs: Set of virtual vertices empty.");
    }

    /*If there is only one element in V, it must be the only MVV.*/
    if (V.size() == 1) {
        mvvs.push_back(V[0]);
        return;
    }

    /*
     * Here V.size >= 2.
     * We maintain a bitvector stating which virtual vertices we have found a
     * "larger" element for, i.e. which vertices that are dominated by another
     * vertex in V. The analogy is that we "kill off" smaller vertices until we
     * only have a conquering vertex left. If we no longer can find any new dominators
     * but there are more than 1 element left alive, there are several conquerers,
     * which are mutually incomparable.
     */
    vector<bool> dead(V.size(), false);
    vvmapel_pt max;
    vvmapel_pt cur;
    unsigned int max_index, cur_index;
    bool end = false;
    /*
     * Perform several runs of max-finding until we have considered all possibilities.
     * Since some elements are incomparable, we may have more that one maximum.
     */
    while (!end) {
        //Find starting point, i.e. first vertex that is still alive.
        end = true;
        for (max_index = 0; max_index < V.size(); max_index++) {
            if (dead[max_index]) {
                continue;
            }
            end = false;
            max = V[max_index];
            dead[max_index] = true;
            break;
        }
        if (end) {
            break;
        }

        //Find maximum
        if (max_index < V.size()) {
            for (cur_index = 0; cur_index < V.size(); cur_index++) {
                //Skip dead vertices
                if (dead[cur_index]) {
                    continue;
                }
                cur = V[cur_index];
                if (max->first.dominates(cur->first)) {
                    /*
                     * If current maximum dominates the current vertex, then
                     * we "kill" the current vertex.
                     */
                    dead[cur_index] = true;
                } else if (cur->first.dominates(max->first)) {
                    /*
                     * Similarly, if the current vertex dominates the maximum vertex
                     * Then the maximum is no longer needed in any run, so we kill
                     * it.
                     * In this case we must also restart the current run since
                     * the new maximum may dominate a vertex that the previous
                     * maximum was not comparable with, and thus may still
                     * be alive.
                     */
                    max = cur;
                    max_index = cur_index;
                    dead[cur_index] = true;
                    cur_index = 0; //Restart
                }
                /*
                 * Else, the vertices are incomparable. We simply continue in this
                 * case.
                 */
            }
            /*Maximum found, add it to set of MVVs.*/
            mvvs.push_back(max);
        }
    }
}