.\" Comment: Man page for chainsaw 
.pc
.TH chainsaw 1 "@date_for_man_pages@" "@PROJECT_NAME@ @PACKAGE_VERSION@" 
.SH NAME
chainsaw \- Cut a gene tree based on a reconciliation
.SH SYNOPSIS
.B chainsaw
[\fIOPTIONS\fR]\fI \fIreconciled-gene-tree\fR  \fIspecies-tree\fR\fR

.SH DESCRIPTION

.I reconciled-gene-tree\fR is the filename of the file containing trees in PRIME 
format. Notice that the node id is optional and if left out all species 
nodeswill be used recursively.

.I species-tree\fR is the species tree

.PP

.SH OPTIONS
.TP
.BR \-\-help
Display help (this text).
.TP
.BR \-V ", " \-\-version
Print version and exit
.TP
.BR \-g ", " \-\-guest\-tree\-format " " \fRprimeOrHybrid|inputxml
The input format. Default is primeOrHybrid
.TP
.BR \-h ", " \-\-host\-tree\-format " " \fRprimeOrHybrid|inputxml
The input format of the host tree. Default is primeOrHybrid
.TP
.BR \-x ", " \-\-node\-name=STRING
A named species node to cut at
.TP
.BR \-i ", " \-\-node\-id=INT
The numerical id of a species node to cut at

.SH "EXIT STATUS"
.TP
.B 0
Successful program execution.
.TP
.B 1
Some error occurred

.SH URL
.TP
The prime\-phylo home page: http://prime.sbc.su.se

.SH "SEE ALSO"
.BR primeDTLSR (1),
.BR primeDLRS (1),
.BR primeGEM (1),
.BR showtree (1),
.BR reconcile (1),
.BR reroot (1),
.BR tree2leafnames (1),
.BR treesize (1)
