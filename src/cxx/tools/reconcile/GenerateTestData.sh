#! /bin/bash

# 2012-04-07 Erik Sjolund moved data files from 
# src/cxx/tools/reconcile to example_data/data_not_in_source_package/reconcile_datafiles


pushd testdata
aladen -q -o H -Gl 4 -Gh 10 -Gp L -Bt 2 -Gt
for ((i=0; i<10; i++)); do
    aladen -q -Hi H -Gl 4 -o G$i  -Gt 
    ../reconcile G$i H G$i"_H".gsmap > R$i
    ../../reroot/reroot -Gn 0 G$i > G$i"b"
    ../reconcile G$i"b" H G$i"_H".gsmap > R$i"b"
    showtree G$i"b"
    ../reconcile -r random G$i"b" H G$i"_H".gsmap > G$i"c"
    showtree  G$i"c" 
done

popd
