#include "AnError.hh"
#include "Hacks.hh"
#include "HybridTreeInputOutput.hh"
#include "HybridTree.hh"
#include "TreeIOTraits.hh"
#include "cmdline.h"

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <sstream>
#include <stdlib.h>


using namespace std;
using namespace beep;


void
outputSingleTree(Tree &T, TreeIOTraits &traits, struct gengetopt_args_info & args_info )
{
	bool et, nt, bl;
	et = traits.hasET();
	nt = traits.hasNT();
	bl = traits.hasBL();

	if (T.hasTimes() == false)
	{
		if (et || nt)
		{
			cerr << "Required output of times, but no time info in tree.\n";
		}
		et = nt = false;
	}

	if (args_info.show_tot_edgetime_flag)
	{
		Real tot = 0;
		for (Tree::const_iterator it = T.begin(); it != T.end(); ++it)
		{
			tot += T.getEdgeTime(*(*it));
		}
		cout << tot << endl;
	}
	else if (args_info.show_tot_edgeweight_flag)
	{
		Real tot = 0;
		for (Tree::const_iterator it = T.begin(); it != T.end(); ++it)
		{
			tot += T.getLength(*(*it));
		}
		cout << tot << endl;
	}
	else 

	  switch ( args_info.output_format_arg ) {
	  case output_format_arg_prime : {
	    cout << TreeInputOutput::writeBeepTree(T, traits, 0) << ";\n";
            break;
          }
          case output_format_arg_outputxml : {
        	    cout << TreeInputOutput::writeXmlBeepTree(T, traits, 0) << endl;
		break;
	  }
	  case output_format_arg_newick : 
          case output_format_arg_simplenewick : 
           {
		TreeIOTraits emptytraits;
		if(traits.hasNWisET() && et)
		{
			emptytraits.setNWisET(true);
			emptytraits.setET(true);
		}
		else if(args_info.output_format_arg == output_format_arg_newick )
		{
			emptytraits.setBL(true);
		}
		cout << TreeInputOutput::writeBeepTree(T, emptytraits, 0) << ";\n";
            break;
          }
          case output_format_arg_asciidrawing : {
		cout << T.print(et, nt, bl, false);
		break;
	  }
	  default:
	    throw PROGRAMMING_ERROR("Bad option");
	  }

}


int
main (int argc, char **argv) 
{
	using namespace beep;
	using namespace std;

	struct gengetopt_args_info args_info;

	try
		{

		if (cmdline_parser(argc, argv, &args_info) != 0)
			exit(1);

		if ( args_info.read_tree_from_stdin_flag && args_info.inputs_num != 0 )
		{
			cerr << "error: If you specify the flag --read-tree-from-stdin there should not be any unnamed parameters" << endl;
			exit(1);
		}
		if ( ! args_info.read_tree_from_stdin_flag && args_info.inputs_num != 1 )
		{
			cerr << "error: If you don't specify the flag --read-tree-from-stdin there should be one unnamed parameter ( the tree filename )" << endl;
			exit(1);
		}
		if ( args_info.show_id_flag && args_info.output_format_arg != output_format_arg_prime )
		{
			cerr << "error: --show-id is only valid if --format=prime" << endl;
			exit(1);
		}

		if (args_info.tree_number_given && args_info.tree_number_arg < 1 )
		{
			cerr << "error: --tree-number should be a positive integer" << endl;
			exit(1);
		}

		HybridTreeInputOutput io;
		FILE * f;
		if( args_info.read_tree_from_stdin_flag )
		  { f = stdin;
		  } 
                else
		  {    f=  fopen(args_info.inputs[0],  "r");
		    assert(f);
		  }


         	switch(args_info.input_format_arg)
		  {
		  case input_format_arg_primeOrHybrid : io.fromFileStream(f, inputFormatBeepOrHybrid); break;
                  case input_format_arg_inputxml     : io.fromFileStream(f, inputFormatXml); break;
		  }

		TreeIOTraits traits;
		io.checkTagsForTrees(traits);


         	if(args_info.output_format_arg == output_format_arg_inputxml)
		  {
		    io.writeInputXML(stdout, 1);
		    return EXIT_SUCCESS;
		  }


		if (traits.hasHY())
		{
			vector<HybridTree> Tvec = io.readAllHybridTrees(traits,0,0);
			//	  HybridTree& T = Tvec[useTree];
			// When I migrated to gengetopt, --tree-number is optional. Therefore I can't use useTree. I set this to 0 instead. /Erik Sjolund 2009-12-02
			HybridTree& T = Tvec[0];
			traits.setET(args_info.show_edgetimes_flag);
			traits.setNT(args_info.show_nodetimes_flag);
			traits.setBL(args_info.show_edgeweights_flag);
			traits.setNWisET(args_info.read_edge_times_after_colon_flag);
			traits.setID(args_info.show_id_flag);

			if(args_info.output_format_arg == output_format_arg_prime)
			{
				cout << HybridTreeInputOutput::writeHybridTree(T, traits, 0) << ";\n";;
				//	      cout << TreeIO::writeHybridTree(T, args_info.show_id_flag, args_info.show_edgetimes_flag, args_info.show_nodetimes_flag, args_info.show_edgeweights_flag, true, 0) << ";\n";;
			}
			else
			{
				cout << T.print(args_info.show_edgetimes_flag, args_info.show_nodetimes_flag, args_info.show_edgeweights_flag, args_info.read_edge_times_after_colon_flag);
			}
		}
		else
		{
			vector<Tree> Tvec = io.readAllBeepTrees(traits, 0,0);
			traits.setET(args_info.show_edgetimes_flag);
			traits.setNT(args_info.show_nodetimes_flag);
			traits.setBL(args_info.show_edgeweights_flag);
			traits.setNWisET(args_info.read_edge_times_after_colon_flag);
			traits.setID(args_info.show_id_flag);
			if (args_info.tree_number_given)
			{
			  if (static_cast<unsigned>(args_info.tree_number_arg) >= Tvec.size())
				{
					throw AnError("There are not that many trees in the file!");
				}

				Tree T = Tvec[args_info.tree_number_arg];
				outputSingleTree(T, traits, args_info);
			}
			else
			{
				for (unsigned i=0; i<Tvec.size(); i++)
				{
					Tree T = Tvec[i];
					outputSingleTree(T, traits, args_info);
					if (i < Tvec.size() - 1) {
						cout << endl; // Separate the trees
					}
				}
			}
		}

	}
	catch (AnError e)
	{
		e.action();
	}
	return EXIT_SUCCESS;
};



