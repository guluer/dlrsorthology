package "@programname_of_this_subdir@"
version "@VERSION_FOR_HELP_OUTPUT@"
purpose "What this program does"
    
args "--unamed-opts"

option "show-edgetimes" e "Show edge times" flag off 
option "show-nodetimes" n "Show node times" flag off 
option "show-edgeweights" b "Show edge weights (i.e. branch lengths)" flag off 
option "show-tot-edgetime" x "Show total edge time instead of tree" flag off
option "show-tot-edgeweight" y "Show total edge weight instead of tree" flag off
option "read-edge-times-after-colon" c "Read edge times after colon" flag off 
option "convert-and-output-tree-in-prime-format" p "Convert and output tree in PRIME format" flag off 
option "show-id" i "Show ID (only valid together with -p)" flag off 

option "read-tree-from-stdin" S  "read tree from STDIN (no <treefile> or <tree#> needed)" flag off

option "format" f "The output format" values="asciidrawing","prime","newick","simplenewick"  enum default="asciidrawing" optional 


option "tree-number" t  "Use the tree with this number. If not set all trees will be printed out" int optional
#option ""  "" flag off



usage "showtree [OPTIONS] [<treefile>]"

description "<treefile> is the filename of the for the file containing trees in PRIME format. "

text "
formats:
newick       - convert and output tree in NEWICK format
simplenewick - Simple newick format, no branch lengths. This also enables reading of unbalanced trees. 
prime        - prime format
asciidrawing - an ascii drawing
"
	

