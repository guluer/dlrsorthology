
#include <iostream>
#include <string>
#include <fstream>

#include "AnError.hh"
#include "Beep.hh"
#include "BirthDeathProbs.hh"
#include "StrStrMap.hh"
#include "PRNG.hh"
#include "ReconciliationTreeGenerator.hh"
#include "Tree.hh"
#include "TreeIO.hh"

// Global options with default settings
//------------------------------------
int nParams = 1;

bool outputSTree = true;
bool outputGamma = true;
bool edgetimes = false;
bool gamma_tree = false;
bool newick = false;
std::string leaf_name_prefix = "";
double lambda = 1.0;
double mu = 1.0;
char* infile = 0; //NULL
char* outfile = 0; //NULL
char* t90file = 0; //NULL
unsigned minleaves = 1;
unsigned maxleaves = 100;
double topTime = 0.0;
bool generate_rootTime = true;

using namespace beep;
using namespace std;

// helper functions
//-------------------------------------------------------------
void usage(char *cmd);
int readOptions(int argc, char **argv);
void rescale_specie_tree(Tree *S);
bool allSpeciesNotIncluded(Tree G, Tree S, StrStrMap gsMap);


// Main program
//-------------------------------------------------------------

int
main(int argc, char **argv) {

    if (argc < nParams + 1) {
        usage(argv[0]);
    }
    try {
        // Read options
        //---------------------------------------------
        int opt = readOptions(argc, argv);

        // Read arguments
        //---------------------------------------------
        if (opt >= argc) {
            cerr << "Expected argument: number-of-leaves\n";
            usage(argv[0]);
        }
        int leaves = atoi(argv[opt++]);

        Tree S; //NULL;
        if (opt < argc) {
            infile = argv[opt];
            TreeIO io_in = TreeIO::fromFile(infile);
            S = io_in.readHostTree(); 
            rescale_specie_tree(&S);
        } else {
            //Create a dummy Species tree
            //---------------------------------------------------------
            S = Tree::EmptyTree();
        }
        if (S.getName() == "Tree") {
            S.setName("Host");
        }

        if (topTime != 0.0) {
            S.setTopTime(topTime);
        }

        // Set up the model for the species tree
        //---------------------------------------------------------
        BirthDeathProbs BDP(S, lambda, mu);
        PRNG R;
        ReconciliationTreeGenerator RTG(BDP, leaf_name_prefix);
        Tree G; // = RTG.exportGtree();
        GammaMap gamma = RTG.exportGamma();

        //cout << RTG;

        // Generating evolutionary tree
        if (leaves < 1) {
            //	  RTG.generateGammaTree(true);
            RTG.generateGammaTree(generate_rootTime);

            // LIMIT SHOULD BE AN OPTION EVENTUALLY! /bens
            // To avoid too lengthy analyses, we restrict the size of G
            unsigned limit = 5000;
            unsigned trials = 0;
            G = RTG.exportGtree();
            gamma = RTG.exportGamma();

            //unsigned nleaves = RTG.exportGtree().getNumberOfLeaves();  
            unsigned nleaves = G.getNumberOfLeaves();
            while (nleaves > maxleaves || nleaves < minleaves) {
                //	  RTG.generateGammaTree(true);
                RTG.generateGammaTree(generate_rootTime);
                //nleaves = RTG.exportGtree().getNumberOfLeaves();
                nleaves = G.getNumberOfLeaves();
                trials++;
                if (trials > limit) // give up!
                {
                    cerr << "beep_generateTree: I'm sorry, but with the\n"
                            << "parameters you're giving, the trees either \n"
                            << " gets to big or just one leaf.\n"
                            << "I've tried " << limit << " times already!\n";
                    exit(6); // CHECK UP WHAT ERROR CODE SHOULD BE USED! /bens
                }
                G = RTG.exportGtree();
                gamma = RTG.exportGamma();
                nleaves = G.getNumberOfLeaves();
            }
        } else {
            RTG.generateGammaTree(leaves, generate_rootTime);
            unsigned limit = 5000;
            unsigned trials = 0;
            StrStrMap gsMap = RTG.exportGS();
            G = RTG.exportGtree();
            gamma = RTG.exportGamma();
            unsigned nleaves = G.getNumberOfLeaves();
            while (nleaves > maxleaves || nleaves < minleaves || allSpeciesNotIncluded(G, S, gsMap)) {
                //RTG.generateGammaTree(leaves, true);
                trials++;
                if (trials > limit) // give up!
                {
                    cerr << "beep_generateTree: I'm sorry, but with the\n"
                            << "parameters you're giving, the trees either \n"
                            << " gets to big or just one leaf.\n"
                            << "I've tried " << limit << " times already!\n";
                    exit(6); // CHECK UP WHAT ERROR CODE SHOULD BE USED! /bens
                }
                RTG.generateGammaTree(leaves, generate_rootTime);
                G = RTG.exportGtree();
                gsMap = RTG.exportGS();
                gamma = RTG.exportGamma();
                nleaves = G.getNumberOfLeaves();
                cout << "No of leaves are: " << nleaves << endl;
            }
        }

        //Tree G = RTG.exportGtree();
        //GammaMap gamma = RTG.exportGamma();
        cout << "The generated tree: \n"
                << G.print()
                << "The number of leaves: "
                << G.getNumberOfLeaves()
                << "\nThe gene species map:\n"
                << RTG.gs4os()
                << "The generated reconciliation:\n"
                // 	   << RTG.exportGamma()
                << RTG.exportGamma()
                << "\n";

        // saving tree
        if (outfile != NULL) {
            cout << "The tree is written to file "
                    << outfile
                    << "\n";
            TreeIO io_out;
            TreeIOTraits traits;
            traits.setET(edgetimes);
            traits.setBL(newick);
            traits.setNWisET(!newick);

            traits.setET(edgetimes);
            ofstream tree((string(outfile)).data());
            tree << io_out.writeBeepTree(G, traits,
                    (gamma_tree ? &gamma : 0)) << "\n";
            // 	  tree << io_out.writeBeepTree(G, !newick, edgetimes, 
            // 				       false, false, newick, 
            // 				       (gamma_tree?&gamma:0)) << "\n";
            //  	  if(edgetimes)
            //  	    {
            //  	      tree << io_out.writeHostTree(G) << "\n";
            //  	    }
            // 	  else
            //  	    {
            // 	      tree << io_out.writeGuestTree(G) + "\n ";
            //  	    }
            if (infile) {
                
                string only_sptree_name = "";
                string sinfile = string(infile);
                size_t sdirp = sinfile.find_last_of("/");
                if(sdirp != std::string::npos)
                    only_sptree_name.assign(sinfile.begin() + sdirp + 1, sinfile.end());
                else 
                    only_sptree_name = sinfile;
                
                string ofile(outfile);
                string odir = "";
                string gtname = "";
                size_t pos = ofile.find_last_of("/");
                if (pos != std::string::npos) {
                    gtname.assign(ofile.begin() + pos + 1, ofile.end());
                    odir.assign(ofile.begin(), ofile.begin() + pos + 1);
                }
                else 
                    gtname.assign(ofile.begin(), ofile.end());
                
                string outgsfile = odir + only_sptree_name + string("_") + gtname + string(".gs");
                cout << "The gene_species map is written to file "
                        << outgsfile << endl;
                        //<< infile
                        //<< "_"
                        //<< outfile
                        //<< gtname
                        //<< ".gs\n";
                //ofstream gs((string(infile)
                //        + "_" + gtname + ".gs").data());
                ofstream gs(outgsfile.data());
                gs << RTG.gs4os() << "\n";
            }
        }
        if (t90file != 0) {
            cout << "'True' parameters for 90% test is written to file "
                    << t90file
                    << "\n"
                    ;
            ofstream t90((string(t90file)).data());
            TreeIO io_out;
            ostringstream t90header;
            ostringstream t90data;
            t90header << "# True parameters input for 90% test\n"
                    << "# T N\tbirthRate(float)\tdeathRate(float)\ttopTime(float)\tG(tree);";
            t90data << "0 0\t"
                    << lambda
                    << ";\t" 
                    << mu
                    << ";\t"
                    << topTime
                    << ";\t";
            //	  t90data << io_out.writeGuestTree(G)
            TreeIOTraits traits;
            t90data << io_out.writeBeepTree(G, traits, 0)
                    << ";";
            if (outputSTree) {

                t90header << "\tS(tree);";
                t90data << "\t"
                        << io_out.writeHostTree(S)
                        << ";";
            }
            if (outputGamma) {               
                t90header << "\treconciliation(reconciliation);";
                t90data << "\t"
                        << io_out.writeGuestTree(G, &gamma)
                        << ";";
            }

            t90header << "\n";
            t90data << "\n";
            t90 << t90header.str() << t90data.str();
        }
    } catch (AnError e) {
        cerr << "error found\n";
        e.action();
    }
}

/**
 * rescale_specie_tree
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void rescale_specie_tree(Tree *S) {
    Real sc = S->rootToLeafTime();
    beep::RealVector* tms = new beep::RealVector(S->getTimes());
    for (beep::RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    S->setTopTime(S->getTopTime() / sc);
    S->setTimes(*tms, true);
    cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}

bool checkTopologies(string genTree, string trueTree) {
    TreeIO gio = TreeIO::fromFile(genTree);
    TreeIO tio = TreeIO::fromFile(trueTree);
    Tree G = gio.readGuestTree();
    Tree TT = tio.readGuestTree();

    for (int i = 0; i < G.getNumberOfNodes(); i++) {
        Node *n = G.getNode(i);
        if (!n->isLeaf()) {
            vector<Node *> gnodes = G.getDescendentNodes(n);
            vector<Node *> tnodes = G.getDescendentNodes(n);
        }
    }
    return false;
}

bool allSpeciesNotIncluded(Tree G, Tree S, StrStrMap gsMap) {
    std::set<std::string> species;
    std::vector<Node*> gnodes = G.getAllNodes();
    for (unsigned i = 0; i < gnodes.size(); i++) {
        Node *n = gnodes[i];
        if (n == NULL || n->getName() == "") {
            continue;
        } else {
            species.insert(gsMap.find(n->getName()));
        }
    }

    if (S.getNumberOfLeaves() != species.size()) {
        for (std::set<std::string>::iterator s = species.begin(); s != species.end(); s++)
            cout << *s << endl;
        return true;
    } else
        return false;
}

void
usage(char *cmd) {
    using std::cerr;
    cerr
            << "Usage: "
            << cmd
            << " [<options>] <nleaves> [<STree>]\n"
            << "\n"
            << "Parameters:\n"
            << "   <nleaves>          If no host tree, <STree>, is given, a random\n"
            << "                      tree with <nleaves> leaves is generated. If a\n"
            << "                      host tree is given, then a guest tree that has\n"
            << "                      <nleaves> nodes reconciled to the root of the \n"
            << "                      host tree. If <nleaves> < 1.0, a new number\n"
            << "                      is drawn from the birth-death process .\n"
            << "   <STree>            optional; if provided, this argument should be\n"
            << "                      the name of a file containing a tree on which\n"
            << "                      the generated tree is reconciled.\n"


            << "Options:\n"
            << "   -u, -h                This text.\n"
            << "   -o <outfile>          output file. The generated tree will be \n"
            << "                         output to this file and any guest-host\n"
            << "                         leaf map is output to a file named \n"
            << "                         <infile>_<outfile>.gs.\n"
            << "   -T<option>            Options for the output of 'true' parameters\n"
            << "                         used in the generation (off by default)\n"
            << "     -To <filename>      Outputs, unless other 'T'-options state\n"
            << "                         otherwise, all 'true' parameters to file\n"
            << "                         <filename>\n"
            << "     -Th                 Do not output the host tree and edge times\n"
            << "                         used for data generation\n"
            << "     -Tr                 Do not output the generated reconciliation\n"
            << "   -G<option>            Options relating to the generated tree\n"
            << "     -Gt                 Include edge times on tree in output file\n"
            << "                         (default without)\n"
            << "     -Gr                 Include reconciliation in tree output\n"
            << "                         (default without)\n"
            << "     -Gh <uint>          Highest number of leaves (<1000)\n"
            << "     -Gl <uint>          Lowest number of leaves (>0)\n"
            << "     -Gp <string>        Leaf name prefix\n"
            << "     -Gn                 Output tree in newick-like format (Note \n"
            << "                         if edge times are requested (-Gt), they\n"
            << "                         are output in place of newick edge lengths\n"
            << "   -B<option>            Options relating to the birth death model\n"
            << "     -Bp <float> <float> birth and death rate of tree model\n"
            << "                         Default is " << lambda << " and " << mu << ".\n"
            << "     -Bt <float>         Top time\n"
            << "     -Br                 Do not generate time on root edge\n"
            ;
    ;
    exit(1);
}

int
readOptions(int argc, char **argv) {
    using namespace beep;
    using namespace std;

    int opt = 1;
    while (opt < argc && argv[opt][0] == '-') {
        switch (argv[opt][1]) {
            case 'h':
            case 'u':
            {
                usage(argv[0]);
                break;
            }
            case 'o':
            {
                if (opt + 1 < argc) {
                    outfile = argv[++opt];
                } else {
                    cerr << "Expected filename after option '-o'\n";
                    usage(argv[0]);
                }
                break;
            }
            case 'T':
            {
                switch (argv[opt][2]) {
                    case 'o':
                    {
                        if (opt + 1 < argc) {
                            t90file = argv[++opt];
                        } else {
                            cerr << "Expected filename after option '-t'\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 'h':
                    {
                        outputSTree = false;
                        break;
                    }
                    case 'r':
                    {
                        outputGamma = false;
                        break;
                    }
                    default:
                    {
                        cerr << "option " << argv[opt] << " not recognized\n";
                        usage(argv[0]);
                        break;
                    }
                }
                break;
            }
            case 'G':
            {
                switch (argv[opt][2]) {
                    case 't':
                    {
                        edgetimes = true;
                        break;
                    }
                    case 'l':
                    {
                        if (opt + 1 < argc && atoi(argv[++opt]) > 0) {
                            minleaves = atoi(argv[opt]);
                        } else {
                            cerr << "Expected a uint > 0 after '-Gl'\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 'h':
                    {
                        if (opt + 1 < argc && atoi(argv[++opt]) > 0) {
                            maxleaves = atoi(argv[opt]);
                        } else {
                            cerr << "Expected a uint < 1000 after '-Gl'\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 'p':
                    {
                        if (opt + 1 < argc) {
                            leaf_name_prefix = argv[++opt];
                        } else {
                            cerr << "Expected a string after '-Gl'\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 'r':
                    {
                        gamma_tree = true;
                        break;
                    }
                    case 'n':
                    {
                        newick = true;
                        break;
                    }
                    default:
                    {
                        cerr << "option " << argv[opt] << " not recognized\n";
                        usage(argv[0]);
                        break;
                    }
                }
                break;
            }
            case 'B':
            {
                switch (argv[opt][2]) {
                    case 'p':
                    {
                        if (++opt < argc) {
                            lambda = atof(argv[opt]);
                            if (++opt < argc) {
                                mu = atof(argv[opt]);
                            } else {
                                cerr << "Expected float (death rate)\n";
                                usage(argv[0]);
                            }
                        } else {
                            cerr << "Expected pair of floats (birth and death rate "
                                    << "for tree model) for option '-p'!\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 't':
                    {
                        if (++opt < argc && atof(argv[opt]) > 0.0) {
                            topTime = atof(argv[opt]);
                        } else {
                            cerr << "Expected float (top time "
                                    << "for tree model) for option '-t'!\n";
                            usage(argv[0]);
                        }
                        break;
                    }
                    case 'r':
                    {
                        generate_rootTime = false;
                        break;
                    }
                    default:
                    {
                        cerr << "option " << argv[opt] << " not recognized\n";
                        usage(argv[0]);
                        break;
                    }
                }
                break;
            }

        }
        opt++;
    }
    return opt;
}
