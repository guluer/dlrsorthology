#include <iostream>
#include <cstdlib>
#include "Beep.hh"
#include "Node.hh"
#include "Tree.hh"
using namespace std;

using namespace beep;
#include "AnError.hh"
#include "TreeIO.hh"

void
usage(char *s)
{
  cerr << "Usage: "
       << s
       << " <treefile>\n\n"
       << "Output comes in 2 to 4 columns, depending on what the tree contains.\n"
       << "Obligatory columns are leaf name and path length.\n"
       << "If the branchlengths are available, a BL column\n"
       << "appears, and if the ET tag is there an ET columns is added."
       << endl;
}

bool hasWeights;
bool hasTimes;


void
FindLeaves(Tree &G, Node* v, int edge_count, Real weight_sum, Real time_sum) {

  int ecv=0;
  Real wv=0;
  Real tv=0;

  ecv = edge_count + 1;
  if (hasWeights) 
    {
      wv = weight_sum + G.getLength(*v);
    }
  if (hasTimes) 
    {
      tv = time_sum + G.getEdgeTime(*v);
    }

  if (v->isLeaf()) {
    cout << v->getName() << "\t" << ecv;
    if (hasWeights) 
      {
	cout << "\t" << wv;
      }
    if (hasTimes)
      {
	cout << "\t" << tv;
      }
    cout << endl;

  } else {
    FindLeaves(G, v->getLeftChild(), ecv, wv, tv);
    FindLeaves(G, v->getRightChild(), ecv, wv, tv);
  }
}


int
main (int argc, char **argv) 
{
  if (argc != 2)
    {
      usage(argv[0]);
      exit (1);
    }

  try {
    TreeIO io = TreeIO::fromFile(argv[1]);
    TreeIOTraits traits;
    io.checkTagsForTree(traits);
    Tree G = io.readBeepTree(0,0);


    hasWeights = G.hasLengths();
    hasTimes = G.hasTimes();
    cout << "Leaf\tPath";
    if (hasWeights)
      {
	cout << "\tBL";
      }
    if (hasTimes)
      {
	cout << "\tET";
      }
    cout << endl;

    Node *root = G.getRootNode();

    FindLeaves(G, root, -1, 0, 0);

  }
  catch (AnError e) {
    e.action();
  }
  return EXIT_SUCCESS;
}
