include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

find_library(tmp plotter)
if(NOT tmp)
  message(FATAL_ERROR  "libplotter not found" )
endif(NOT tmp )

add_executable(${programname_of_this_subdir}
lsdcombine.cc
GraphPlotter.cc
DuplicationStatistics.cc
${CMAKE_CURRENT_BINARY_DIR}/cmdline.c 
${CMAKE_CURRENT_BINARY_DIR}/cmdline.h
)

target_link_libraries(${programname_of_this_subdir} prime plotter)
install(TARGETS ${programname_of_this_subdir} DESTINATION bin)

