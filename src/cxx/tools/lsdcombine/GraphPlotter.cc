/* 
 * File:   GraphPlotter.cc
 * Author: fmattias
 * 
 * Created on January 13, 2010, 1:44 PM
 */

#include <algorithm>
#include <sstream>
#include <iomanip>

#include "GraphPlotter.hh"

using namespace std;

GraphPlotter::GraphPlotter(double height, double width) : m_width(width),
                                                          m_height(height),
                                                          m_arrowSize(DEFAULT_ARROW_SIZE),
                                                          m_fontSize(DEFAULT_FONT_SIZE),
                                                          m_leftPadding(DEFAULT_LEFT_PADDING),
                                                          m_rightPadding(DEFAULT_RIGHT_PADDING)
{
}

GraphPlotter::GraphPlotter(const GraphPlotter& other) :  m_xDataPoints(other.m_xDataPoints),
                                                         m_yDataPoints(other.m_yDataPoints),
                                                         m_yStds(other.m_yStds),
                                                         m_width(other.m_width),
                                                         m_height(other.m_height),
                                                         m_arrowSize(other.m_arrowSize),
                                                         m_fontSize(other.m_fontSize),
                                                         m_leftPadding(other.m_leftPadding),
                                                         m_rightPadding(other.m_rightPadding),
                                                         m_name(other.m_name)
                                                         
{
}

GraphPlotter::~GraphPlotter() {
}

void GraphPlotter::addDataPoint(double x, double y, double yStd)
{
    m_xDataPoints.push_back(x);
    m_yDataPoints.push_back(y);
    m_yStds.push_back(yStd);
}

void GraphPlotter::setFontSize(int fontSize)
{
    m_fontSize = fontSize;
}

void GraphPlotter::setArrowSize(double arrowSize)
{
    m_arrowSize = arrowSize;
}

void GraphPlotter::setLeftPadding(double leftPadding)
{
    m_leftPadding = leftPadding;
}

void GraphPlotter::setRightPadding(double rightPadding)
{
    m_rightPadding = rightPadding;
}

void GraphPlotter::setName(const string &name)
{
    m_name = name;
}

void GraphPlotter::plotGraph(Plotter &plotter, double startX, double startY)
{
    /* Set black color */
    plotter.pencolor(0, 0, 0);
    plotter.fontsize(m_fontSize);

    /* Plot the axis of the graph */
    plotter.fline(startX, startY, startX + m_width, startY);
    plotter.fline(startX, startY, startX, startY + m_height);

    /* Plot the arrows */
    plotter.fline(startX + m_width, startY, startX + m_width - m_arrowSize, startY + m_arrowSize);
    plotter.fline(startX + m_width, startY, startX + m_width - m_arrowSize, startY - m_arrowSize);

    plotter.fline(startX, startY + m_height, startX + m_arrowSize, startY + m_height - m_arrowSize);
    plotter.fline(startX, startY + m_height, startX - m_arrowSize, startY + m_height - m_arrowSize);

    /* Plot name */
    plotter.move(startX + m_arrowSize*2, startY + m_height - 0.5*m_fontSize);
    plotter.label(m_name.c_str());

    if(m_xDataPoints.size() <= 0) {
        return;
    }

    /* Get max y and x used for scaling the graph */
    double xMax = *std::max_element(m_xDataPoints.begin(), m_xDataPoints.end());
    double yMax = *std::max_element(m_yDataPoints.begin(), m_yDataPoints.end());

    if(xMax < 1e-10) {
        xMax = 1;
    }
    if(yMax < 1e-10) {
        yMax = 1;
    }

    /* Plot the data points */
    for(unsigned int i = 0; i < m_xDataPoints.size(); i++) {
        double x = m_xDataPoints[i];
        double y = m_yDataPoints[i];

        /* Calculate transform x and y cordinates to graph relative cordinates*/
        double plotX = startX + (x/xMax)*(m_width - m_leftPadding);
        double plotY = startY + (y/yMax)*(m_height - m_rightPadding);

        /* Plot lines to data point */
        plotter.colorname("gray77");
        if(m_yStds[i] > 1e-10) {
            double yVariation = (m_yStds[i] / yMax)*(m_height - m_rightPadding);
            plotter.fline(plotX, startY, plotX, plotY + yVariation);
            plotter.colorname("black");
            plotter.fline(plotX - 5.0, plotY + yVariation, plotX + 5.0, plotY + yVariation);
            plotter.fline(plotX - 5.0, plotY - yVariation, plotX + 5.0, plotY - yVariation);
        }
        else {
            plotter.fline(plotX, startY, plotX, plotY);
        }
        plotter.colorname("black");

        /* Plot datapoint as a circle */
        plotter.fcircle(plotX, plotY, 2.0);

        ostringstream xLabel;
        xLabel << i;
        plotter.fmove(plotX - m_fontSize*0.5, startY - m_fontSize*1.25);
        plotter.label(xLabel.str().c_str());
    }

    /* Plot max points for y and x */
    double plotXMax = startX + (m_width - m_leftPadding);
    double plotYMax = startY + (m_height - m_rightPadding);

    plotter.fline(plotXMax, startY - m_arrowSize, plotXMax, startY + m_arrowSize);
    plotter.fline(startX - m_arrowSize, plotYMax, startX + m_arrowSize, plotYMax);

    /* Plot labels for max points */
    ostringstream xMaxLabel;
    xMaxLabel << setprecision(2) << xMax;
    plotter.move(plotXMax, startY + 2*m_arrowSize);
    plotter.label(xMaxLabel.str().c_str());

    ostringstream yMaxLabel;
    yMaxLabel << setprecision(2) << yMax;
    plotter.move(startX + 2*m_arrowSize, plotYMax);
    plotter.label(yMaxLabel.str().c_str());

}

