#include "AnError.hh"
#include "BirthDeathMCMC.hh"
//#include "IntegralBirthDeathMCMC.hh"
#include "DummyMCMC.hh"
#include "Hacks.hh"
#include "TopTimeMCMC.hh"
#include "TreeMCMC.hh"
#include "TreeIO.hh"
#include "SimpleML.hh"
#include "SimpleMCMC.hh"
#include "StrStrMap.hh"
#include "Node.hh"

#include "MCMCOutputReader.hh"

#include "cmdline.h"

#include <stdlib.h>

// Author: Bengt Sennblad, copyright: the MCMC group, SBC
// copyright The MCMC club, SBC

int
main(int argc, char** argv)
{
  using namespace beep;
  using namespace std;

  struct gengetopt_args_info args_info;

  if (cmdline_parser(argc, argv, &args_info) != 0)
    exit(1);
    //    exit(EXIT_FAILURE);

  //  "default" values does not work with "multiple" as of gengetop 2.22.3 ( it seems )
  double start_parameter_birth_rate = 1.0;
  double start_parameter_death_rate = 1.0;

  if (args_info.birth_and_death_rates_given ) 
    {
      if ( args_info.birth_and_death_rates_arg[0] <= 0 ) 
	{     cerr << "error: birth rate must be positive" << endl;
	  exit(1);
	}
      if ( args_info.birth_and_death_rates_arg[1] <= 0 ) 
	{     cerr << "error: death rate must be positive" << endl;
	  exit(1);
	}
      start_parameter_birth_rate = args_info.birth_and_death_rates_arg[0];
      start_parameter_death_rate = args_info.birth_and_death_rates_arg[1];
    }


  if ( args_info.number_of_iterations_arg < 1 ) 
     {     cerr << "error: --number-of-iterations must be a positive integer" << endl;
      exit(1);
     }
  if ( args_info.factor_stderr_stdout_arg < 1 ) 
     {     cerr << "error: --factor-stderr-stdout must be a positive integer" << endl;
      exit(1);
     }

  if ( args_info.thinning_arg < 1 ) 
     {     cerr << "error: --thinning must be a positive integer" << endl;
      exit(1);
     }

  if ( args_info.seed_arg < 0 ) 
     {     cerr << "error: --seed must be a non-negative integer" << endl;
      exit(1);
     }



  //args_info.benchmark_verbose_given

  if( args_info.inputs_num == 2 || args_info.inputs_num == 3  ) 
     {     cerr << "error: not the correct number of unnamed parameters" << endl;
      exit(1);
     }
  try
    {
      // tell the user we've started
      //---------------------------------------------------------
      cerr << "Running: ";
      // If you only want to print the unnamed arguments
      //      for ( unsigned i = 0 ; i < args_info.inputs_num ; ++i )
      //	cerr <<  args_info.inputs[i] << " " ;

      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}

      cerr << " in directory ";
      cerr << getenv("PWD");
      cerr << "\n\n";
      
      //---------------------------------------------------------
      // Read input and options
      //---------------------------------------------------------

      // Check for options
      //-----------------------------------

      //Get the trees
      //---------------------------------------------
      string guest( args_info.inputs[0]);
      TreeIO io = TreeIO::fromFile(guest);
      StrStrMap gs;
      Tree G = io.readBeepTree(0, &gs); 
      if(G.getName() == "Tree")
	{ 
	  G.setName("G");
	}


      string host(args_info.inputs[1]);
      TreeIO io2 = TreeIO::fromFile(host);
      Tree S = io2.readHostTree();  
      if(S.getName() == "Tree")
	{ 
	  S.setName("S");
	}

      if(gs.size() == 0)
	{
          if( args_info.inputs_num != 3 ) 
	    {
	      cerr << "gs was not present in guest tree, "
		   << "therefore I expected a <gs> argument" << endl;
	    }
	  gs = TreeIO::readGeneSpeciesInfo(args_info.inputs[2]);
	}

      //--------------------------------------------------------
      // Set up all classes
      //--------------------------------------------------------

       //Set up random number generator and "end MCMCModel"
      //---------------------------------------------------------
      PRNG rand;
      if ( args_info.seed_arg != 0)
	{
	  rand.setSeed( args_info.seed_arg );
	}
      DummyMCMC dm;

      // Set up priors
      //-------------------------------------------------------
      double Beta;
       if(args_info.beta_parameter_arg < 0)
 	{
 	  Beta = S.getRootNode()->getNodeTime();
 	} else
	{
	  Beta = args_info.beta_parameter_arg;
        }

      TopTimeMCMC stm = TopTimeMCMC(dm, S, Beta); 
      BirthDeathMCMC bdm= BirthDeathMCMC(stm, S, start_parameter_birth_rate , 
				       start_parameter_birth_rate, &stm.getTopTime());
      if(args_info.fixate_birth_and_death_rate_flag )
	{
	  bdm.fixRates();
	}
      
      OrthologyMCMC gtm(bdm, G, gs, bdm);
      if ( args_info.choose_starting_rates_flag ) 
	{
	  gtm.chooseStartingRates();
	}

      if(! args_info.reroot_flag)
	{
	  gtm.fixRoot();
	}
      gtm.fixTree(); 

      if(args_info.estimate_orthology_flag )
	{
	  gtm.estimateOrthology(args_info.record_speciation_probabilities_flag);
	}

      // Set up MCMC handler
      //-------------------------------------------------------
      SimpleMCMC* iterator;
      if(args_info.do_maximum_likelihood_flag)
	{
 	  iterator = new SimpleML(gtm, args_info.thinning_arg);
	}
      else
	{
	  iterator = new SimpleMCMC(gtm, args_info.thinning_arg);
	}

      if (args_info.output_file_given)
	{
	  try 
	    {
	      iterator->setOutputFile(args_info.output_file_arg);
	    }
	  catch(AnError e)
	    {
	      e.action();
	    }
	  catch (int e)
	    {
	      cerr << "Problems opening output file! ('"
		   << args_info.output_file_arg
		   << "') Using stdout instead.\n";
	    }
	}  

      if (args_info.no_diagnostics_to_stderr_flag )
	{
	  iterator->setShowDiagnostics(false);
	}

      if (args_info.output_likelihood_flag)
	{
	  cout << "# L\tN\t" 
	       << gtm.strHeader()
	       << endl
	       << gtm.currentStateProb() 
	       << "\t0\t" 
	       << gtm.strRepresentation() 
	       << endl;
	  exit(0);
	}      
      
      if (!args_info.no_diagnostics_to_stderr_flag) 
	{
	  cerr << "Start MCMC (Seed = " << rand.getSeed() << ")\n";
	}


      // Copy startup info to outfile (can be used to restart analysis)
      cout << "# Running: ";
      for(int i = 0; i < argc; i++)
	{
	  cout << argv[i] << " ";
	}
      cout << " in directory"
	   << getenv("PWD")
	   << "\n";
      
      //--------------------------------------------------------
      // Now we're set to do stuff
      //---------------------------------------------------------

      time_t t0 = time(0);
      clock_t ct0 = clock();


      iterator->iterate(args_info.number_of_iterations_arg, args_info.factor_stderr_stdout_arg );

      time_t t1 = time(0);    

      clock_t ct1 = clock();
      cerr << "Wall time: " << readableTime(t1 - t0) 
	   << endl
	   << "CPU time: " << readableTime((ct1 - ct0)/CLOCKS_PER_SEC)
	   << endl;
      
      if (!args_info.no_diagnostics_to_stderr_flag )
	{
	  cerr << gtm.getAcceptanceRatio()
	       << " = acceptance ratio   Wall time = "
	       << readableTime(t1-t0)
	       << "\n";
	}
      
      if (gtm.getAcceptanceRatio() == 0) 
      {
	cerr << "Warning! MCMC acceptance ratio was 0.\n";
      }
    }      
  catch(AnError& e)
    {
      cerr << "Error:\n";
      e.action();      
    }
  catch(exception& e)
    {
      cerr << "Exception:\n"
	   << e.what()
	   << endl;
    }

  return(0);
};

