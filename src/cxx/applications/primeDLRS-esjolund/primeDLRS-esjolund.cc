// JOELGS: This needs to be synced with a more up-to-date version of primeDLRS,
// and should really only serve as help on the way when implementing mpi.

//#include <vector>
//#include <string>
//
//#include "BeepOption.hh"
//#include "EdgeRateMCMC_common.hh"
//#include "Density2PMCMC.hh"
//#include "DummyMCMC.hh"
//#include "EdgeDiscTree.hh"
//#include "EdgeDiscBDMCMC.hh"
//#include "EdgeDiscBDProbs.hh"
//#include "EdgeDiscGSR.hh"
//#include "EdgeWeightHandler.hh"
//#include "EdgeWeightMCMC.hh"
//#include "GammaDensity.hh"
//#include "InvGaussDensity.hh"
//#include "LogNormDensity.hh"
//#include "MatrixTransitionHandler.hh"
//#include "MpiMCMC.hh"
//#include "PRNG.hh"
//#include "Probability.hh"
//#include "RandomTreeGenerator.hh"
//#include "SeqIO.hh"
//#include "SequenceData.hh"
//
//#include "SimpleMCMC2.hh"
//#include "SimpleObserver.hh"
//#include "SiteRateHandler.hh"
//#include "StrStrMap.hh"
//#include "SubstitutionMCMC.hh"
//#include "TreeDiscretizers.hh"
//#include "TreeIO.hh"
//#include "TreeMCMC.hh"
//#include "UniformDensity.hh"
//
//#include "mpicxx.h"
//#include "cmdline.h"
//
//#include <boost/serialization/string.hpp>
//#include <boost/serialization/vector.hpp>
//#include <boost/mpi/environment.hpp>
//#include <boost/mpi/communicator.hpp>
//#include <boost/mpi/collectives.hpp>
//#include <boost/lexical_cast.hpp>
//
//#include <iostream>
//namespace mpi = boost::mpi;
//
///*
//class optimum_report
//{
//private:
//  friend class boost::serialization::access;
//
//  template<class Archive>
//  void serialize(Archive & ar, const unsigned int version)
//  {
//    ar & state;
//    ar & sign;
//    ar & logProb;
//  }
//public:
//  std::string state;
//  int sign;
//  double logProb;
//
//  optimum_report(){};
//  optimum_report(std::string state_, beep::Probability prob) :
//    state(state_)
//  {
//     logProb = prob.getLogProb();
//     sign = prob.getSign();
//  }
//
//  beep::Probability getProbability() {
//    beep::Probability prob;
//     prob.setLogProb(logProb, sign);
//     return prob;
//  }
//
//};
//BOOST_CLASS_EXPORT(optimum_report);
//*/
//
//struct optimumStruct {
//  std::string state;
//  int sign;
//  double logProb;
//};
//
//
//// Suggestion rates. Affects perturbation frequency of the various parameters.
//static const double   SUGG_RATIO_BD_RATES     = 1.0;
//static const double   SUGG_RATIO_G_TOPO_START = 3.0; // Initial value.
//static const double   SUGG_RATIO_G_TOPO_FINAL = 3.0; // Final value.
//static const unsigned SUGG_RATIO_G_TOPO_STEPS = 1;   // Steps between start and final.
//static const double   SUGG_RATIO_G_LENGTHS    = 1.0;
//
//
//// Forward declaration of helpers.
//
//void createSharedUnderlyingObjects(struct gengetopt_args_info & args_info, beep::Tree*& S, beep::EdgeDiscTree*& DS, beep::StrStrMap*& gsMap);
//void createUnderlyingObjects( struct gengetopt_args_info & args_info, beep::StrStrMap*& gsMap, beep::MatrixTransitionHandler*& Q,	beep::SequenceData*& D, beep::Tree*& G);
//
//
///* The creation of this class was more or less unnecessary. I though the mpi program needed 4 version of this data, one for each worker node. This turned out not to be the case. /Erik Sjolund 2010-03-03 */
//
//
//class SubstitutionMCMCContainer {
//public:
//  SubstitutionMCMCContainer() {
//    Q=NULL;
//    D=NULL;
//    G=NULL;
//    mcmcDummy=NULL;
//    mcmcGTopo=NULL;
//    bdp=NULL;
//    mcmcBDRates=NULL;
//    erdf=NULL;
//    mcmcEdgeRate=NULL;
//    gsrModel=NULL;
//    ewh=NULL;
//    srdf=NULL;
//    mcmcSiteRate=NULL;
//    srh=NULL;
//    partList=NULL;
//    sMCMC=NULL;
//};
//  SubstitutionMCMCContainer(struct gengetopt_args_info & args_info, beep::Tree*& S, beep::EdgeDiscTree*& DS, beep::StrStrMap*& gsMap) {
//
//	using namespace beep;
//	using namespace beep::option;
//	using namespace std;
//
//	createUnderlyingObjects(args_info, gsMap, Q, D, G);
//
//		// Mcmc: End-of-chain.
//		mcmcDummy = new DummyMCMC();
//                assert(mcmcDummy);
//		// MCMC: Birth-death rates (duplication-loss rates).
//		pair<double,double> bdr;
//                if (args_info.birth_and_death_rates_given) {
//                  bdr.first = args_info.birth_and_death_rates_arg[0];
//                  bdr.second = args_info.birth_and_death_rates_arg[1];
//		}
//		else
//		  {
//		    bdr.first = 1.0;
//		    bdr.second = 1.0;
//                }
//		bdp = new EdgeDiscBDProbs(*DS, bdr.first, bdr.second);
//                assert(bdp);
//		mcmcBDRates = new EdgeDiscBDMCMC(*mcmcDummy, *bdp, SUGG_RATIO_BD_RATES);
//                assert(mcmcBDRates);
//		if (args_info.fixate_birth_and_death_rate_flag) { mcmcBDRates->fixRates(); };
//
//		// MCMC: Guest tree topology.
//		mcmcGTopo = new UniformTreeMCMC(*mcmcBDRates, *G, SUGG_RATIO_G_TOPO_START);
//                assert(mcmcGTopo);
//		mcmcGTopo->setChangingSuggestRatio(SUGG_RATIO_G_TOPO_FINAL, SUGG_RATIO_G_TOPO_STEPS);
//		if (args_info.fix_initial_guest_tree_topology_flag)
//		{
//			mcmcGTopo->fixTree();
//			mcmcGTopo->fixRoot();
//		}
//		mcmcGTopo->setDetailedNotification(true);
//
//		// MCMC: Substitution rate variation over edges (IID).
//
//
//                double mean=1.0;
//                double variance=1.0;
//                if (args_info.initial_mean_and_variance_of_sequence_evolution_rate_given) {
//                  mean = args_info.initial_mean_and_variance_of_sequence_evolution_rate_arg[0];
//                  variance = args_info.initial_mean_and_variance_of_sequence_evolution_rate_arg[1];
//		}
//
//		switch ( args_info.edge_rate_distribution_arg )
//		  {
//		  case edge_rate_distribution_arg_InvG : { erdf = new InvGaussDensity(mean, variance); break; };
//		  case edge_rate_distribution_arg_LogN : { erdf = new LogNormDensity(mean, variance); break; };
//		  case edge_rate_distribution_arg_Gamma : { erdf = new GammaDensity(mean, variance); break; };
//		  case edge_rate_distribution_arg_Uniform : { erdf = new UniformDensity(mean, variance,true); break; };
//                  default: throw AnError("Unknown args_info.edge_rate_distribution_arg");
//               }
//		assert(erdf);
//                mcmcEdgeRate = new Density2PMCMC(*mcmcGTopo, *erdf, true);
//                assert(mcmcEdgeRate);
//
//		if (args_info.fix_mean_and_variance_of_evolution_race_flag || args_info.edge_rate_distribution_arg == edge_rate_distribution_arg_Uniform )
//		{
//			mcmcEdgeRate->fixMean();
//			mcmcEdgeRate->fixVariance();
//		}
//
//		// Set up GSR integrator.
//                gsrModel = new EdgeDiscGSR(*G, *DS, *gsMap, *erdf, *bdp);
//                assert(gsrModel);
//		// MCMC: Edge lengths. The GSR class holds these, and may therefore be model.
//		mcmcGLengths = new EdgeWeightMCMC(*mcmcEdgeRate, *gsrModel, SUGG_RATIO_G_LENGTHS, true);
//                assert(mcmcGLengths);
//		mcmcGLengths->generateWeights(true);
//		mcmcGLengths->setDetailedNotification(true);
//
//		// Set up edge weights, equating weights with lengths.
//		ewh = new EdgeWeightHandler(*gsrModel);
//                assert(ewh);
//
//		// MCMC: Disc. gamma substitution rate variation among sites.
//		srdf = new UniformDensity(0, 3, true);	// Uniform prior over [0,3].
//                assert(srdf);
//                mcmcSiteRate = new ConstRateMCMC(*mcmcGLengths, *srdf, *G, "Alpha");
//                assert(mcmcSiteRate);
//
//		// Set up site rate handler.
//
//                 assert( args_info.number_of_steps_of_discretized_gamma_distribution_arg >= 0);
//                 unsigned srdfn = args_info.number_of_steps_of_discretized_gamma_distribution_arg;
//		srh = new SiteRateHandler(srdfn, *mcmcSiteRate);
//                assert(srh);
//		if (srdfn == 1) { mcmcSiteRate->fixRates(); }
//
//		// MCMC: Substitution.
//		partList = new vector<string>;
//                assert(partList);
//		partList->push_back(string("all"));
//		sMCMC = new SubstitutionMCMC(*mcmcSiteRate, *D, *G, *srh, *Q, *ewh, *partList);
//                assert(sMCMC);
//
//		ostringstream oss;
//		if (args_info.debuginfo_flag)
//		{
//			oss << "# DISCTRETIZED HOST TREE TIMES:" << endl << (*DS);
//			oss << "# INITIAL RECONCILIATION AND PROBS:" << endl
//				<< gsrModel->getDebugInfo(false, false, false, false);
//		}
//
//                /* FIX THIS: oss is written to but not used /Erik Sjolund 2010-02-09 */
//};
//
//  ~SubstitutionMCMCContainer() {
//    if(sMCMC) { delete sMCMC; sMCMC=NULL; }
//    if(partList){ delete partList; partList=NULL; }
//    if(srh){ delete srh; srh=NULL; }
//    if(mcmcSiteRate){ delete mcmcSiteRate; mcmcSiteRate=NULL; }
//    if(srdf){ delete srdf; srdf=NULL; }
//    if(ewh){ delete ewh; ewh=NULL; }
//    if(mcmcGLengths){ delete mcmcGLengths; mcmcGLengths=NULL; }
//    if(gsrModel){ delete gsrModel; gsrModel=NULL; }
//    if(mcmcEdgeRate){ delete mcmcEdgeRate; mcmcEdgeRate=NULL; }
//    if(mcmcGTopo){ delete mcmcGTopo; mcmcGTopo=NULL; }
//    if(mcmcBDRates){ delete mcmcBDRates; mcmcBDRates=NULL; }
//    if(mcmcDummy){ delete mcmcDummy; mcmcDummy=NULL; }
//    if(bdp){ delete bdp; bdp=NULL; }
//    if(erdf){ delete erdf; erdf=NULL; }
//    if(Q){ delete Q; Q=NULL; }
//    if(D){ delete D; D=NULL; }
//    if(G){ delete G; G=NULL; }
//
//
//
//
//};
//  beep::SubstitutionMCMC * getSubstitutionMCMC() {  return sMCMC; };
//
//private:
//  std::vector<std::string> *partList;
//  beep::SiteRateHandler *srh;
//  beep::ConstRateMCMC *mcmcSiteRate;
//  beep::UniformDensity *srdf;
//  beep::EdgeWeightHandler *ewh;
//  beep::EdgeWeightMCMC *mcmcGLengths;
//  beep::EdgeDiscGSR *gsrModel;
//  beep::Density2PMCMC *mcmcEdgeRate;
//  beep::UniformTreeMCMC *mcmcGTopo;
//  beep::EdgeDiscBDMCMC *mcmcBDRates;
//  beep::EdgeDiscBDProbs *bdp;
//  beep::DummyMCMC *mcmcDummy;
//  beep::SubstitutionMCMC * sMCMC;
//  beep::Density2P* erdf;
//  beep::MatrixTransitionHandler* Q;
//  beep::SequenceData* D;
//  beep::Tree* G;
//};
//
//
//
///************************************************************************
// * Main program. See help message method for program description.
// ************************************************************************/
//int main(int argc, char **argv)
//{
//  using namespace beep;
//  using namespace beep::option;
//  using namespace std;
//
//  struct gengetopt_args_info args_info;
//
//
//  //
//
//
//
//  try
//    {
//
//      if (cmdline_parser(argc, argv, &args_info) != 0)
//	exit(1);
//      if ( ! ( args_info.inputs_num == 2 || args_info.inputs_num == 3 ))
//	{
//	  cerr << "error: should be either 2 or 3 arguments. " << endl << gengetopt_args_info_usage << endl;
//	  exit(1);
//	}
//      if ( args_info.number_of_iterations_arg < 1 ) {
//	cerr << "error: --number-of-iterations must be a positive integer" << endl;
//	exit(1);
//      }
//
//      if ( args_info.thinning_arg < 1 ) {
//	cerr << "error: --thinning must be a positive integer" << endl;
//	exit(1);
//      }
//
//      if (  args_info.approximate_discretization_timestep_arg < 0 ) {
//	cerr << "error: --approximate-discretization-timestep must not be negative" << endl;
//	exit(1);
//      }
//
//      if (  args_info.minimum_number_of_parts_to_slice_each_edge_arg < 0 ) {
//	cerr << "error: --minimum-number-of-parts-to-slice-each-edge must not be negative" << endl;
//	exit(1);
//      }
//
//      mpi::environment *env = NULL;
//      mpi::communicator *world = NULL;
//
//
//      int rank;
//      if ( args_info.execution_type_arg == execution_type_arg_MPIMCMC ) {
//        // I think MPI::Init should be run as early as possible /Erik Sjolund 2010-02-26
//         env = new mpi::environment(argc, argv);
//         assert(env);
//         world = new mpi::communicator();
//         assert(world);
//	 rank = world->rank();
//      }
//
//
//      // Create default options. Read options and arguments.
//      // Pseudo random number generator.
//      PRNG rand;
//      int seed;
//
//      // Check this, To comply with the previous functionality I added "args_info.seed_arg != 0". Maybe it should be removed. /Erik Sjolund 2010-02-22
//      if ( args_info.seed_given && args_info.seed_arg != 0 ) {
//	seed = args_info.seed_arg;        rand.setSeed(args_info.seed_arg); } else {
//	seed = rand.getSeed();	  // random
//      };
//
//      // Create substitution matrix, sequence data, trees, etc.
//      Tree* S;
//      EdgeDiscTree* DS;
//      StrStrMap* gsMap;
//
//      createSharedUnderlyingObjects( args_info, S, DS,gsMap);
//
//      // Get pre-run info.
//      ostringstream oss;
//      oss << "# Running: " << endl << "#";
//      for (int i = 0; i < argc; i++) { oss << " " << argv[i]; }
//      oss << endl
//	  << " with seed " << seed
//	  << " in directory " << getenv("PWD") << endl;
//
//      // Iterate according to choice of execution type.
//
//      std::string preamble =  oss.str();
//      SubstitutionMCMCContainer *subst = NULL;
//
//      SimpleObserver so(args_info.number_of_iterations_arg);
//
//      AbstractMCMC* iter=NULL;
//
//      subst = new SubstitutionMCMCContainer(args_info, S, DS, gsMap);
//      assert(subst);
//      beep::SubstitutionMCMC *sMCMC = subst->getSubstitutionMCMC();
//      assert(sMCMC);
//      switch(args_info.execution_type_arg) {
//      case execution_type_arg_PD: {
//	cout << sMCMC->currentStateProb() << endl;
//	break;
//      };
//      case execution_type_arg_PDHC: { cerr << "is not yet implemented" << endl;
//	  exit(EXIT_FAILURE);
//	  /*
//	  // Posterior density hill-climbing.
//	  iter = new SimpleML2(sMCMC);
//	  assert(iter);
//	  */
//	  break;
//      };
//      case execution_type_arg_MPIMCMC: {
//        float temperature = 1.0;
//        assert( ( rank <= 4 ) && ( rank >= 0 ) );
//        for ( int i=0; i < ( rank - 1 ); i++ ) {
//           temperature = temperature * args_info.temperature_factor_arg;
//        }
//	iter = new MpiMCMC(*sMCMC,temperature,world);
//	assert(iter);
//	break;
//      };
//
//      case execution_type_arg_MCMC: {
//	iter = new SimpleMCMC2(*sMCMC);
//	assert(iter);
//	break;
//      };
//      }
//
//      if (iter) {
//	// Output, diagnostics, etc.
//
//	if ( args_info.output_file_given )
//	  {
//	    try
//	      {
//		so.setOutputFile(args_info.output_file_arg);
//	      }
//	    catch(AnError& e)
//	      {
//		e.action();
//	      }
//	    catch (...)
//	      {
//		cerr << "Problems opening output file '" << args_info.output_file_arg << "'! "
//		     << "Reverting to stdout." << endl;
//	      }
//	  }
//
//	if (args_info.no_diagnostics_flag) { so.setShowDiagnostics(false); }
//	cout << preamble;
//	so.setPrintFactor(args_info.print_factor_arg);
//	so.setThinning(args_info.thinning_arg);
//	// Iterate!!!!
//	time_t t0 = time(0);
//	clock_t ct0 = clock();
//        unsigned int iterationsDone;
//
//	if ( ! iter->iterate( so,  &iterationsDone)) {
//	  cout << "observer stopped iteration prematurely"  << endl;
//	}
//
//	time_t t1 = time(0);
//	clock_t ct1 = clock();
//
//	if (!( args_info.execution_type_arg == execution_type_arg_MPIMCMC && rank != 0 )) {
//	  cout << "# Wall time: " << difftime(t1, t0) << " s." << endl;
//	  cout << "# CPU time: " << (Real(ct1 - ct0) / CLOCKS_PER_SEC) << " s."	<< endl;
// 	}
//
//	if ( args_info.execution_type_arg == execution_type_arg_MPIMCMC ) {
//	  assert(world);
//	  int signType = 130;
//	  int logProbType = 131;
//	  int bestStateType = 132;
//
//	  if (world->rank() == 0) {
//	    mpi::request reqs[12];
//	    std::vector<  optimumStruct * > opV;
//	    assert( world->size() == 5 );
//
//	    for (int i = 0; i < world->size() - 1; ++i) {
//	      optimumStruct * opS = new optimumStruct;
//	      assert(opS);
//	      opV.push_back(opS);
//	      int workerNode = i+1;
//	      reqs[3*i] = world->irecv(workerNode, signType, opS->sign);
//	      reqs[(3*i)+1] = world->irecv(workerNode, logProbType, opS->logProb);
//	      reqs[(3*i)+2] = world->irecv(workerNode, bestStateType, opS->state);
//	    }
//	    mpi::wait_all(reqs, reqs + 12);
//
//	    std::string bestState;
//	    Probability bestProb;
//	    bestProb.setLogProb( (*opV.begin())->logProb, (*opV.begin())->sign);
//
//	    for ( std::vector<  optimumStruct * >::iterator iter = opV.begin(); iter != opV.end(); iter++ ) {
//	      Probability prob;
//	      prob.setLogProb(  (*iter)->logProb, (*iter)->sign);
//	      if ( prob >= bestProb )  { bestProb = prob; bestState = (*iter)->state ; }
//	    }
//	    std::cout << "best: probility=" << bestProb << " state="  << bestState << std::endl;
//
//	    for ( std::vector<  optimumStruct * >::iterator iter = opV.begin(); iter != opV.end(); iter++ ) {
//                 delete *iter;
//	        *iter = NULL;
//	    }
//
//	  } else {
//	    // the four worker nodes
//	     Probability myProb = so.getLocalOptimum();
//	     std::string myState =  so.getBestState();
//	     double myLogProb = myProb.getLogProb();
//             int mySign = myProb.getSign();
//
//              mpi::request reqs[3];
//  	      reqs[0] = world->isend(0 /* rootNode */, signType, mySign);
//	      reqs[1] = world->isend(0, logProbType, myLogProb);
//	      reqs[2] = world->isend(0, bestStateType, myState);
//  	    mpi::wait_all(reqs, reqs + 3);
//	  }
//
//	  /*
//            Maybe this could be done with gather() instead?
//            I had some problems with creating the user defined data structure: class optimum_report
//            see the beginning of this file /Erik Sjolund 2010-03-03
//
//	    if (world->rank() == 0) {
//	    std::vector<optimum_report> all_reports;
//	    optimum_report opt_r();
//	    int num = 1;
//	    std::vector<int> all_num;
//	    gather(*world, opt_r, all_reports, 0);
//	    //	    gather(*world, num, all_num, 0);
//	    assert(world->size() == 5);
//	    for (int proc = 1; proc < world->size(); ++proc)
//
//	    std::cout << "Process #" << proc <<  all_reports[proc].getProbability()  << std::endl;
//	    } else {
//	    Probability prob = so.getLocalOptimum();
//	    std::string bestState =  so.getBestState();
//	    optimum_report opt_r(bestState, prob);
//	    gather(*world, opt_r, 0);
//	    int num2 = 2 ;
//	    //gather(*world, num2, 0);
//	    }
//	  */
//
//	}
//
//        if(subst && args_info.execution_type_arg != execution_type_arg_MPIMCMC ) {
//	    beep::SubstitutionMCMC *sMCMC = subst->getSubstitutionMCMC();
//	    cout << "# Total acceptance ratio: "
//		 << sMCMC->getAcceptanceRatio() << endl
//		 << sMCMC->getAcceptanceInfo() << '#';
//	  }
//
//	if (world) {
//          delete world;
//          world=NULL;
//	}
//	if (env) {
//          delete env;
//          env=NULL;
//	}
//
//        assert(iter);
//	delete iter;
//        iter = NULL;
//      }
//      // Clean up in (somewhat) reverse order of creation.
//
//
//      if(subst) {
//	delete subst;
//	subst=NULL;
//      }
//      assert(S);
//      delete S;
//      assert(DS);
//      delete DS;
//      assert(gsMap);
//      delete gsMap;
//    }
//  catch (AnError& e)
//    {
//      e.action();
//      cerr << "Use '" << argv[0] << " -h' to display help.";
//    }
//  catch (exception& e)
//    {
//      cerr << e.what() << endl;
//      cerr << "Use '" << argv[0] << " -h' to display help.";
//    }
//  cerr << endl;
//}
//
//
//
//
///************************************************************************
// * Helper. Creates sequence data, trees, etc.
// * Deletion must be taken care of by invoking method.
// ************************************************************************/
//void createUnderlyingObjects( struct gengetopt_args_info & args_info,  beep::StrStrMap*& gsMap,
//		beep::MatrixTransitionHandler*& Q,
//		beep::SequenceData*& D,
//		beep::Tree*& G
//		)
//{
//	using namespace std;
//	using namespace beep;
//	using namespace beep::option;
//
//	// For practical reasons, set up the substitution matrix first.
//	// Use user-defined matrix if such has been provided.
//	if ( args_info.user_defined_substitution_model_given )
//	{
//         	if ( args_info.substitution_model_given )
//			throw AnError("Cannot use both predefined and user-defined subst. model.");
//
//
//         	throw AnError("reimplement this. fix this.");
//		/*
//
//
//
//		UserSubstModelOption* smUsr = Options.getUserSubstModelOption("UserSubstModel");
//		Q = new MatrixTransitionHandler(MatrixTransitionHandler::userDefined(smUsr->type, smUsr->pi, smUsr->r));
//                assert(Q);
//		*/
//	}
//	else
//	{
//                switch(args_info.substitution_model_arg) {
//		  case substitution_model_arg_JC69 : { Q = new MatrixTransitionHandler(MatrixTransitionHandler::JC69()); break; }
//		  case substitution_model_arg_UniformAA : { Q = new MatrixTransitionHandler(MatrixTransitionHandler::UniformAA()); break; }
//		  case substitution_model_arg_JTT : { Q = new MatrixTransitionHandler(MatrixTransitionHandler::JTT()); break; }
//		  case substitution_model_arg_UniformCodon : { Q = new MatrixTransitionHandler(MatrixTransitionHandler::UniformCodon()); break; }
//	   	  case substitution_model_arg_ArveCodon : { Q = new MatrixTransitionHandler(MatrixTransitionHandler::ArveCodon()); break; }
//                  default: throw AnError("Unknown args_info.substitution_model_arg ");
//                }
//                assert(Q);
//	}
//
//	// Get sequence data. This is why Q must already exist.
//	assert(args_info.inputs_num >= 2);
//	D = new SequenceData(SeqIO::readSequences(args_info.inputs[0], Q->getType()));
//        assert(D);
//
//        if ( args_info.initial_guest_tree_topology_file_given ) {
//	  TreeIO tio = TreeIO::fromFile(args_info.initial_guest_tree_topology_file_arg);
//          G = new Tree(tio.readBeepTree(NULL, gsMap));
//        }
//        else {
//          G = new Tree(RandomTreeGenerator::generateRandomTree(D->getAllSequenceNames()));
//        }
//
//        assert(G);
//
//	G->doDeleteTimes();
//	G->doDeleteRates();
//}
//
///************************************************************************
// * Helper.
// * Deletion must be taken care of by invoking method.
// ************************************************************************/
//
//void createSharedUnderlyingObjects(
//                struct gengetopt_args_info & args_info,
//		beep::Tree*& S,
//		beep::EdgeDiscTree*& DS,
//		beep::StrStrMap*& gsMap
//		)
//{
//	using namespace std;
//	using namespace beep;
//	using namespace beep::option;
//
//
//	// Create S from file.
//	assert(args_info.inputs_num >= 2);
//	S = new Tree(TreeIO::fromFile(args_info.inputs[1]).readHostTree());
//
//	if (args_info.rescale_flag)
//	{
//		Real sc = S->rootToLeafTime();
//
//		RealVector* tms = new RealVector(S->getTimes());
//		for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it)
//		{
//			(*it) /= sc;
//		}
//		S->setTopTime(S->getTopTime() / sc);
//		S->setTimes(*tms, true);
//		cout << "# Host tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
//	}
//
//	if (args_info.top_time_given )
//	{
//		Real tt = args_info.top_time_arg ;
//		if (tt <= 0) {
//			tt = S->rootToLeafTime();
//			cout << "# Changing root edge time span to " << tt << ".\n";
//		}
//		S->setTopTime(tt);
//	}
//	cout << "-----------------------------" << endl;
//	if (S->getTopTime() < 1e-8)
//		throw AnError("Host tree top time must be greater than 0.");
//
//
//	// Create discretized tree DS based on S.
//	Real timestep = args_info.approximate_discretization_timestep_arg ;
//	unsigned minIvs = args_info.minimum_number_of_parts_to_slice_each_edge_arg;
//	if (timestep == 0)
//	{
//		EquiSplitEdgeDiscretizer disc(minIvs);
//		DS = disc.getDiscretization(*S);
//	}
//	else
//	{
//		StepSizeEdgeDiscretizer disc(timestep, minIvs);
//		DS = disc.getDiscretization(*S);
//	}
//
//
//	// Create random guest tree G unless file specified.
//	// Fill G-S map. If empty (no mapping info was found in the gene tree or a
//	// random tree was created), we have to use file passed as param with that info.
//	gsMap = new StrStrMap();
//   assert(gsMap);
//	if (gsMap->size() == 0)
//	{
//		if (args_info.inputs_num != 3) throw AnError("Missing guest-to-host leaf map.");
//		else
//		  {       assert(gsMap);
//			delete gsMap;
//			gsMap = new StrStrMap(TreeIO::readGeneSpeciesInfo(args_info.inputs[2]));
//                         assert(gsMap);
//		}
//	}
//}
//
//
//
///************************************************************************
// *  Helper. Creates default values corresponding to options and stores
// *  them in global map.
// ***********************************************************************
//
// Not used anymore. We could remove createDefaultOptions() /Erik Sjolund 2010-02-24
//
//void createDefaultOptions()
//{
//	using namespace std;
//	using namespace beep::option;
//	Options.addStringOption("Outfile", "o", "",
//			"  -o <string>\n"
//			"      Output filename. Defaults to stderr.\n");
//	Options.addUnsignedOption("Seed", "s", 0,
//			"  -s <unsigned int>\n"
//			"      Seed for pseudo-random number generator. Defaults to random seed.\n");
//	Options.addUnsignedOption("Iterations", "i", 1000000,
//			"  -i <unsigned int>\n"
//			"      Number of iterations. Defaults to 1000000.\n");
//	Options.addUnsignedOption("Thinning", "t", 100,
//			"  -t <unsigned int>\n"
//			"      Thinning, i.e. sample every <value>-th iteration. Defaults to 100.\n");
//	Options.addUnsignedOption("PrintFactor", "w", 1,
//			"  -w <unsigned int>\n"
//			"      Output diagnostics to stderr every <value>-th sample. Defaults to 1.\n");
//	Options.addBoolOption("Quiet", "q", false,
//			"  -q \n"
//			"      Do not output diagnostics. Non-quiet by default.\n");
//	Options.addStringAltOption("RunType", "m", "MCMC", "MCMC,PDHC,PD",
//			"  -m <'MCMC'/'PDHC'/'PD'>\n"
//			"      Execution type (MCMC, posterior density hill-climbing from initial\n"
//			"      values, or just initial posterior density). Defaults to MCMC.\n",
//			UPPER);
//	Options.addStringAltOption("SubstModel", "Sm", "JTT", "UniformAA,JC69,JTT,UniformCodon,ArveCodon",
//			"  -Sm <'UniformAA'/'JC69'/'JTT'/'UniformCodon'/'ArveCodon'>\n"
//			"      Substitution model. JTT by default.\n",
//			UPPER);
//	Options.addUserSubstModelOption("UserSubstModel", "Su",
//			"  -Su <'DNA'/'AminoAcid'/'Codon'> <Pi=float1 float2 ... floatn> <R=float1 float2 ...float(n*(n-1)/2)>\n"
//			"      User-defined substitution model. The size of Pi and R must fit data type \n"
//			"      (DNA: n=4, AminoAcid: n=20, Codon: n=62). R is given as a flattened\n"
//			"      upper triangular matrix. Don't use both option -Su and -Sm.\n");
//	Options.addUnsignedOption("SiteRateCats", "Sn", 1,
//			"  -Sn <unsigned int>\n"
//			"      Number of steps of discretized Gamma-distribution for sequence\n"
//			"      evolution rate variation over sites. Defaults to 1 (no variation).\n");
//	Options.addStringAltOption("EdgeRateDistr", "Ed", "Gamma", "Gamma,InvG,LogN,Uniform",
//			"  -Ed <'Gamma'/'InvG'/'LogN'/'Uniform'>\n"
//			"      Distribution for iid sequence evolution rate variation over guest\n"
//			"      tree edges. Defaults to Gamma.\n",
//			UPPER);
//	Options.addDoubleX2Option("EdgeRateParams", "Ep", pair<double,double>(1.0, 1.0),
//			"  -Ep <float> <float>\n"
//			"      Initial mean and variance of sequence evolution rate.\n"
//			"      Defaults to 1.0 and 1.0.\n");
//	Options.addBoolOption("EdgeRateFixed", "Ef", false,
//			"  -Ef \n"
//			"      Fix mean and variance of sequence evolution rate. Non-fixed by default.\n");
//	Options.addStringOption("GTreeFile", "Gi", "",
//			"  -Gi <string>\n"
//			"      Filename with initial guest tree topology.\n");
//	Options.addBoolOption("GTreeFixed", "Gg", false,
//			"  -Gg \n"
//			"      Fix initial guest tree topology, i.e. perform no branch-swapping.\n"
//			"      Non-fixed by default.\n");
//	Options.addDoubleX2Option("BDRates", "Bp", pair<double,double>(1.0, 1.0),
//			"  -Bp <float> <float>\n"
//			"      Initial duplication and loss rates. Defaults to 1.0 and 1.0.\n");
//	Options.addBoolOption("BDRatesFixed", "Bf", false,
//			"  -Bf \n"
//			"      Fix initial duplication and loss rates. Non-fixed by default.\n");
//	Options.addDoubleOption("TopTime", "Bt", 0.0,
//			"  -Bt <float>\n"
//			"      Override time span of edge above root in host tree. If the value is <=0, the span\n"
//			"      will be set to equal the root-to-leaf time. Defaults to value in host tree file.\n");
//	Options.addDoubleOption("DiscTimestep", "Dt", 0.05,
//			"  -Dt <float>\n"
//			"      Approximate discretization timestep. Set to 0 to divide\n"
//			"      every edge in equally many parts (see -Di). Defaults to 0.05.\n");
//	Options.addUnsignedOption("DiscMinIvs", "Di", 3,
//			"  -Di <unsigned int>\n"
//			"      Minimum number of parts to slice each edge in. If -Dt is set to 0,\n"
//			"      this becomes the exact number of parts. Minimum 2. Defaults to 3.\n");
//	Options.addBoolOption("Rescale", "r", false,
//			"  -r \n"
//			"      Rescale the host tree so that the root-to-leaf time equals 1.0. All\n"
//			"      inferred parameters will refer to the new scale. Off by default.\n");
//	Options.addBoolOption("DebugInfo", "debuginfo", false,
//			"  -debuginfo \n"
//			"      Show misc. info to stderr before iterating. Not shown by default.\n");
//};
//*/
