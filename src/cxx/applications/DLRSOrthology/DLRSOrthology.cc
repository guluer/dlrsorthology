/*
 * DLRSOrthology
 *
 * Computes the orhtology probabilities and creates a consensus tree with speciation probabilities from
 * gene trees created in an MCMC run using, for instance, PrIME-GSR.
 *
 * Author: Ikram Ullah (ikramu@kth.se) November/December 2009
 *
 *
 * Acknowledgement: Much of the code in this program has been taken from Prime-CT
 * by Peter Andersson.
 * /Ikram
 */

//Boost
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/tokenizer.hpp>
// include headers that implement a archive in simple text format
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

//Standard libraries
#include <stdlib.h>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <utility>
#include <map>
#include <iosfwd>

//Gengetopt
#include "cmdline.h"

//From PrIME library
#include "Tree.hh"
#include "TreeIO.hh"
#include "SeqIO.hh"
#include "GammaDensity.hh"
#include "SequenceData.hh"
#include "MatrixTransitionHandler.hh"
#include "StrStrMap.hh"
#include "EdgeDiscGSR.hh"
#include "Tokenizer.hh"
#include "Hacks.hh"
#include "TimeEstimator.hh"
#include "RandomTreeGenerator.hh"
#include "CT/EdgeDiscGSRObjects.hh"
#include "mcmc/PosteriorSampler.hh"

//From primeCT
#include "VirtualVertex.hh"
#include "GeneSet.hh"
#include "McmcSample.hh"


/*Namespaces used*/
using namespace beep;
using namespace std;



/*Definitions and constants*/
// maximum size of characters in mcmc sample
#define SAMPLE_LENGTH 3000
//To be able to use foreach() constructs
#define foreach BOOST_FOREACH
//Flag desciding if diagnostics about the flow of the program is written to stdout
static bool DIAGNOSTICS = true;
//Identities for the input arguments
static const int INPUT_SEQUENCES_SAMPLES_FILE = 0;
static const int INPUT_SPECIES_TREE_FILE = 1;
static const int INPUT_GSMAP_FILE = 2;
//Threshold for majority-rule consensus
static const float posterior_threshold = 0.5f;
//Default MCMC parameters
static const float DEFAULT_BIRTH_RATE = 1.0f;
static const float DEFAULT_DEATH_RATE = 1.0f;
static const float DEFAULT_MEAN = 1.0f;
static const float DEFAULT_VARIANCE = 1.0f;
static const float DEFAULT_TIME_STEP = 0.0f;
static const int DEFAULT_MIN_INTERVALS = 3;
//Convenience typedef
// The first Probability is pair is Posterior Probability
// The second Probability is pair is Speciation Probability
typedef map< VirtualVertex, pair<Probability, Probability> > vvmap;
//NOTE:
//For some reason I cannot find a way to make a pointer to an element
//in a vvmap, this is a workaround. The iterator for vvmap is seen as a
//regular pointer. /Peter
typedef vvmap::iterator vvmapel_pt;





/*Pre-declarations*/
//See each function definition for descriptions.

static void read_cmd_opt(struct gengetopt_args_info &args_info,
        int argc, char *argv[]);

static Tree read_species_tree(struct gengetopt_args_info &args_info);

static void rescale_specie_tree(Tree *S);

static void create_disc_tree(Tree &species_tree,
        EdgeDiscTree **DS, struct gengetopt_args_info &args_info);

static void create_lookup_tables(map<int, string> &D,
        map<string, int> &inv_D,
        StrStrMap &gs_map);

static void read_and_process_samples(EdgeDiscTree &DS,
        StrStrMap &gs_map,
        struct gengetopt_args_info &args_info,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices, int &hl);

void get_gene_tree_from_state(string curr_line, Tree &G);

void createObjectFromState(string curr_line, EdgeDiscTree &DS,
        StrStrMap &gs_map, EdgeDiscGSR* &gsr, Tree &G,
        EdgeDiscBDProbs* &bd_probs, GammaDensity* &gamma,
        int &hl, struct gengetopt_args_info &args_info);

static void find_vertices_single(vvmap &vertices,
        Tree &tree,
        map<int, string> &ID,
        map<string, int> &inv_ID);

static void find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &D,
        map<string, int> &inv_D,
        GeneSet &part);

static void find_high_post_vertices(vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices,
        float threshold_count);

static Node* find_lca(SetOfNodes &nodes, Tree &T);

void calc_speciation_probs_from_file(vector<vvmapel_pt> &hp_vertices,
        EdgeDiscTree &DS, map<int, string> &D, StrStrMap &gs_map, int &numStates,
        int &hl, struct gengetopt_args_info &args_info, map<string, double> &leaf_pairs);

void calc_speciation_single(EdgeDiscGSR *state,
        vector<vvmapel_pt> &hp_vertices, ofstream &spfile,
        map<int, string> &ID, Tree S, map<int, Node*> gs_node_map, map<string, double> &lmap);

bool independent(Node * lca_a, Node *lca_b);

static void find_and_replace(string &source, const string find, string replace);

static void normalize_hp_prob(vector<vvmapel_pt> &hp_vertices, int num_states);

void write_leaf_pairs_prob_to_file(map<string, double> leaf_pairs_orthologies, 
        int num_states, string out_file_name);

/**
 * main
 */
int main(int argc, char *argv[]) {

    /*
     * Program variables
     */
    struct gengetopt_args_info args_info; //Used for option parsing
    vector<EdgeDiscGSRObjects*> gsr_objects; //Samples read from file
    vector<EdgeDiscGSR*> states; //GSR states
    TreeIO tree_reader; //Used for reading from files
    Tree species_tree; //The species tree
    EdgeDiscTree *DS; //Discretized species tree
    StrStrMap gs_map; //The gene-species map used
    map<int, string> ID; //Lookup table for id-gene mapping
    map<string, int> inv_ID; //Lookup table for gene-id mapping
    vvmap vertices; //Table of virtual vertices
    vector<vvmapel_pt> hp_vertices; //High posterior vertices
    map<string, double> leaf_pairs_orthologies;
    int num_states;
    int header_tokens = 0; // number of tokens in prime output header (fixed/variable tree)

    /* 
     * Starting comment for test purposes 
     */
    cout << "\t*******************************" << endl;
    cout << "\tWelcome to Orthology Estimator" << endl;
    cout << "\t*******************************" << endl;

    /*
     * Read command-line options
     */
    read_cmd_opt(args_info, argc, argv);

    /*
     * Read species tree
     */
    if (DIAGNOSTICS) {
        cout << "Reading species tree.. ";
    }
    species_tree = read_species_tree(args_info);

    cout << species_tree << endl;
    /*
     * Rescale specie tree to 0-1
     */
    rescale_specie_tree(&species_tree);
    TreeIOTraits iot;
    iot.setET(true);
    //ofstream sout("res_specie.tree");
    //sout << TreeIO::writeBeepTree(species_tree, iot, NULL) << endl;
    //sout.flush();
    //sout.close();
    //ti.writeGuestTree(species_tree);
    //cout << species_tree << endl;
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Create discretized tree
     */
    if (DIAGNOSTICS) {
        cout << "Creating discretized tree.. ";
    }
    create_disc_tree(species_tree, &DS, args_info);
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Read gene-species map
     */
    if (DIAGNOSTICS) {
        cout << "Reading gene-species map.. ";
    }
    cout << "\nspecie file name is " << args_info.inputs[INPUT_GSMAP_FILE] << endl;
    gs_map = tree_reader.readGeneSpeciesInfo(args_info.inputs[INPUT_GSMAP_FILE]);
    if (DIAGNOSTICS) {
        cout << "done!" << endl;
    }

    /*
     * Find vertices in trees and gather data for posterior and
     * speciation probability calculations
     */

    //Give each gene an id in the range 0, 1, ..., gs_map.size()-1
    //and create lookup-tables for gene-id and id-gene.
    if (DIAGNOSTICS) {
        cout << "Creating lookup tables.. ";
    }
    create_lookup_tables(ID, inv_ID, gs_map);
    if (DIAGNOSTICS) {
        cout << "lookup tables created!" << endl;
    }

    if (args_info.mcmc_flag) {
        /* This part is yet to be implemented */
    } else {

        /*
         * Read in samples from file
         */
        if (DIAGNOSTICS) {
            cout << "Reading samples.. " << endl;
        }

        read_and_process_samples(*DS, gs_map, args_info, ID, inv_ID, vertices, hp_vertices, header_tokens);
        if (DIAGNOSTICS) {
            cout << "done!" << endl;
        }

        /*
         * calculate speciation probability
         */
        if (DIAGNOSTICS) {
            cout << "\nCalculating speciation probability.. " << endl;
        }

        calc_speciation_probs_from_file(hp_vertices, *DS, ID, gs_map, num_states, 
                header_tokens, args_info, leaf_pairs_orthologies);
    }

    if (DIAGNOSTICS) {
        cout << "done calculating speciation probs!" << endl;
    }

    /* write leaf pair probabilities to file */
    string leaf_pair_file(args_info.virtual_vertex_file_arg);
    leaf_pair_file += string(".all.pairs");
    write_leaf_pairs_prob_to_file(leaf_pairs_orthologies, num_states, leaf_pair_file);
    
    cout << "Number of states are: " << num_states << endl;
    normalize_hp_prob(hp_vertices, num_states);        

    foreach(vvmapel_pt it, hp_vertices) {
        GeneSet gset = it->first.getLeftDescendantPart();
        stringstream ss;
        //cout << gset << endl;
        //gset.idToNames(ID);
        cout << it->first.getLeftDescendantPart() << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
    }

    /*
     * write the speciation probabilities for each virtual vertex to file
     */
    ofstream spfile(args_info.virtual_vertex_file_arg);
    spfile << "VirtualVertex\tPosterior Probability\tSpeciation Probability" << endl;

    foreach(vvmapel_pt it, hp_vertices) {
        VirtualVertex vver = it->first; 
        spfile << vver.getStrRepresentation(ID) << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
        //spfile << it->first << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
    }
    spfile.close();


    /*
     * writing consensus tree output to tree
     */
    if (DIAGNOSTICS) {
        cout << "Writing consensus tree output to tree...  " << endl;
    }

    cout << "First the high posterior vertices" << endl;
    for (unsigned int i = 0; i < hp_vertices.size(); i++) {
        vvmapel_pt it = hp_vertices[i];
        VirtualVertex vver = it->first;        
        //cout << it->first << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
        cout << vver.getStrRepresentation(ID) << "\t" << it->second.first.val() << "\t" << it->second.second.val() << endl;
    }

    /*
    cout << "And then the int to string IDs" << endl;
    std::map<int, std::string>::iterator iter;
    for (iter = ID.begin(); iter != ID.end(); iter++) {
        cout << iter->first << "\t" << iter->second << endl;
    }
     */
    cout << "End of program..." << endl;

}

/**
 * read_cmd_opt
 *
 * Deal with typical command-line option parsing using gengetopt.
 *
 * @param args_info On return, contains all option and input info.
 * @param argc NO input parameters (same as for main)
 * @param argv Input arguments (same as for main)
 */
void
read_cmd_opt(struct gengetopt_args_info &args_info,
        int argc,
        char *argv[]) {
    /*Parse options and input*/
    if (cmdline_parser(argc, argv, &args_info) != 0) {
        exit(EXIT_FAILURE);
    }

    /*Check for correct number of inputs*/
    if (args_info.inputs_num != 3) {
        cerr << gengetopt_args_info_usage << endl
                << "Type '" << argv[0] << " -h' for help" << endl;
        exit(EXIT_FAILURE);
    }

    /*Set diagnostics printout*/
    DIAGNOSTICS = !(args_info.silent_flag);
}

/* write leaf pair probs to file */
void write_leaf_pairs_prob_to_file(map<string, double> lportho, 
        int num_states, string out_file_name) {
    ofstream outfile(out_file_name.c_str());
    //outfile << "VirtualVertex\tPosterior Probability\tSpeciation Probability" << endl;

    for(map<string, double>::iterator it = lportho.begin(); it != lportho.end(); ++it) {
        it->second = it->second/num_states;
        outfile << it->first << "\t" << it->second << endl;
    }
    outfile.close();
} 

/**
 * read_species_tree
 *
 * Read a species tree from file.
 *
 * @param args_info Program arguments
 */
Tree
read_species_tree(struct gengetopt_args_info &args_info) {
    //cout << "\nspecie file name is " << args_info.inputs[INPUT_SPECIES_TREE_FILE] << endl;
    TreeIO tree_reader = TreeIO::fromFile(args_info.inputs[INPUT_SPECIES_TREE_FILE]);
    return tree_reader.readHostTree();
}

/**
 * rescale_specie_tree
 *
 * Rescales a species tree to [0,1].
 *
 * @param S The specie tree
 */
void
rescale_specie_tree(Tree *S) {
    Real sc = S->rootToLeafTime();
    RealVector* tms = new RealVector(S->getTimes());
    for (RealVector::iterator it = tms->begin(); it != tms->end(); ++it) {
        (*it) /= sc;
    }
    S->setTopTime(S->getTopTime() / sc);
    S->setTimes(*tms, true);
    cout << "Specie tree root-to-leaf span was rescaled from " << sc << " to 1.0.\n";
}

/**
 * create_disc_tree
 *
 * Creates a discretized species tree from a species tree.
 *
 * @param species_tree The species tree
 * @param DS At return: Pointer to the discretized species tree
 * @param args_info Program arguments
 *
 */
void
create_disc_tree(Tree &species_tree,
        EdgeDiscTree **DS,
        struct gengetopt_args_info &args_info) {
    float timestep; //Timestep for discretized tree
    int min_intervals; //Minimum number of intervals in DS
    EdgeDiscretizer *disc; //Used for creating discretized trees

    //Set timestep and min_interval parameters
    timestep = args_info.timestep_arg;
    min_intervals = args_info.min_intervals_arg;

    //Create discretized tree
    if (timestep == 0) {
        disc = new EquiSplitEdgeDiscretizer(min_intervals);
    } else {
        disc = new StepSizeEdgeDiscretizer(timestep, min_intervals);
    }
    *DS = new EdgeDiscTree(species_tree, disc);

    delete disc;
}

/**
 * create_lookup_tables
 *
 * Create an id for each gene in the range 0,..., gs_map.size()-1.
 * Then created lookup tables for gene name to id mapping and vice versa.
 *
 * @param ID Storage of id to gene map upon return.
 * @param inv_ID Storage of gene to id map upon return.
 * @param gs_map Gene to species map
 */
void
create_lookup_tables(map<int, string> &ID,
        map<string, int> &inv_ID,
        StrStrMap &gs_map) {
    int nr_genes; //Total number of genes
    string gene_name; //Gene name

    /*
     * Go through all genes in the gene-species map. Create an
     * id and a record in both lookup-tables for each gene found
     */
    cout << gs_map << endl;
    nr_genes = gs_map.size();
    for (int i = 0; i < nr_genes; i++) {
        gene_name = gs_map.getNthItem(i);
        ID.insert(pair<int, string > (i, gene_name));
        inv_ID.insert(pair<string, int>(gene_name, i));
    }
}

/**
 * read_and_process_samples
 *
 * Read all samples from a prime output file.
 *
 * @param samples Storage for sample objects
 * @param DS Discretized species tree needed to create sample objects
 * @param args_info Input parameter including the filenames for gene/species
 *                  tree files.
 */
void
read_and_process_samples(EdgeDiscTree &DS,
        StrStrMap &gs_map,
        struct gengetopt_args_info &args_info,
        map<int, string> ID, map<string, int> inv_ID,
        vvmap &vertices,
        vector<vvmapel_pt> &hp_vertices, int &hl) {
    ifstream sample_file; //File object for samples
    string curr_line; //Current line when reading samples            

    //GammaDensity *gamma; //Density function
    //EdgeDiscBDProbs *bd_probs; //Birth-death rates holder

    // default initialization
    //gamma = new GammaDensity(DEFAULT_MEAN, DEFAULT_VARIANCE);
    //bd_probs = new EdgeDiscBDProbs(&DS, DEFAULT_BIRTH_RATE, DEFAULT_DEATH_RATE);
    Tree G; //Gene tree

    int burn_in = 1;
    string header = "";
    bool endOfHeader = false;

    /*Read samples and create states*/
    sample_file.open(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE]);
    if (sample_file.is_open()) {
        try {
            while (!sample_file.eof()) {
                //Skip lines of comments
                if ((sample_file.peek() == '#') && !endOfHeader) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    getline(sample_file, header);

                    Tokenizer tok(";");
                    tok.setString(header);
                    hl = 0;
                    while (tok.hasMoreTokens()) {
                        tok.getNextToken();
                        hl++;
                    }

                    continue;
                } else if ((sample_file.peek() == '#') && endOfHeader) {
                    // don't do anything, this is the summary at 
                    // the end of prime output.
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    continue;
                }
                // only take the samples after burn-in                
                cout << "sample number: " << burn_in << endl;
                if (burn_in <= args_info.burn_in_arg) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    burn_in++;
                    continue;
                }

                // set the endOfHeader to true, if not done already
                if (!endOfHeader)
                    endOfHeader = true;

                //Get next line from file
                curr_line = "";
                getline(sample_file, curr_line);

                //Skip empty lines
                if (curr_line == "") {
                    cout << "line is empty..." << endl;
                    continue;
                }

                /*
                //Create density function
                gamma = NULL;
                bd_probs = NULL;

                EdgeDiscGSR *gsr; // = NULL;
                createObjectFromState(curr_line, DS, gs_map, gsr, G, bd_probs, gamma, hl, args_info);
                 */

                get_gene_tree_from_state(curr_line, G);
                cout << "read gene tree from sample no: " << burn_in << endl;
                //cout << G << endl;
                burn_in++;

                //find_vertices_single(vertices, gsr, ID, inv_ID);
                find_vertices_single(vertices, G, ID, inv_ID);

                //find_vertices_single()
                //delete gamma;
                //delete bd_probs;
                //delete gsr;
            }
        } catch (exception &ex) {
            cout << ex.what() << endl;
        }
        sample_file.close();
        int numStates = (burn_in - 1) - args_info.burn_in_arg;
        cout << "Total samples in hp_vertices are " << numStates << endl;
        find_high_post_vertices(vertices, hp_vertices, numStates * posterior_threshold);
    } else {
        cerr << "Could not open file: " << sample_file << endl;
        exit(EXIT_FAILURE);
    }
}

/*
 * get gene tree from state
 * @param curr_line string containing current MCMC state 
 * @param G tree data structure for storing gene tree
 */
void
get_gene_tree_from_state(string curr_line, Tree &G) {

    TreeIO tree_reader; // used for parsing prime trees
    Tokenizer tokenizer(";"); //String tokenizer    

    /*
     * Information in each line is separated by ';'. The tokenizer
     * splits the line at each ';'
     */
    //Set tokenizer
    tokenizer.setString(curr_line);

    //Skip info about likelihoods and number of iterations
    //cout << tokenizer.getNextToken() << endl;
    //cout << tokenizer.getNextToken() << endl;
    tokenizer.getNextToken();
    tokenizer.getNextToken();

    //Read gene tree
    tree_reader.setSourceString(tokenizer.getNextToken());

    G = tree_reader.readNewickTree();
}

/*
 * create EdgeDiscGSR object from state
 */

void
createObjectFromState(string curr_line, EdgeDiscTree &DS,
        StrStrMap &gs_map, EdgeDiscGSR* &gsr, Tree &G,
        EdgeDiscBDProbs* &bd_probs, GammaDensity* &gamma,
        int &hl, struct gengetopt_args_info &args_info) {

    Tokenizer tokenizer(";"); //String tokenizer
    float mean; //Mean for a density function
    float variance; //Variance for a density function
    float birth_rate; //Birth rate for a state
    float death_rate; //Death rate for a state
    TreeIO tree_reader; //Used for parsing prime trees
    //Tree G;
    string token; //Holder for current token
    Node* lca = NULL;
    /////////////////////////////////////////// Look here first ////////////////////////////////////////////////
    UnsignedVector *fixedNodes;
    fixedNodes = NULL;
    //vector<string> g_node_names;
    //g_node_names.push_back("Popy-A01");
    //g_node_names.push_back("Popy-A02");
    //g_node_names.push_back("Gogo-A0101");
    //GammaDensity *gamma; //Density function
    //cout << "constructing ";

    /*
     * Information in each line is separated by ';'. The tokenizer
     * splits the line at each ';'
     */
    //Set tokenizer
    tokenizer.setString(curr_line);

    //Skip info about likelihoods and number of iterations
    tokenizer.getNextToken();
    tokenizer.getNextToken();

    //Read gene tree
    tree_reader.setSourceString(tokenizer.getNextToken());

    G = tree_reader.readNewickTree();

    //for(unsigned int i = 0; i < G.getNumberOfNodes(); i++)
    //    cout << i << " = " << G.getLength(*(G.getNode(i))) << "\t"; 
    //cout << endl;
    //cout << " gene tree, ";
    //Read mean
    token = tokenizer.getNextToken();
    boost::trim<string > (token);
    mean = boost::lexical_cast<float> (token);
    //cout << " mean, ";

    //Read variance
    token = tokenizer.getNextToken();
    boost::trim<string > (token);
    variance = boost::lexical_cast<float> (token);
    //cout << " variance, ";

    // IKRAM: I have commented this out because I am using it
    // only for fixed trees.

    //Skip unnecessary gene tree topology
    if (hl == 9)
        tokenizer.getNextToken();

    //Read birth rate
    token = tokenizer.getNextToken();
    boost::trim<string > (token);
    birth_rate = boost::lexical_cast<float> (token);
    //cout << " birth ";

    //Read death rate
    token = tokenizer.getNextToken();
    boost::trim<string > (token);
    death_rate = boost::lexical_cast<float> (token);
    //cout << " and death rate, ";

    //Create density function
    //gamma->setMean(mean);
    //gamma->setVariance(variance);

    gamma = new GammaDensity(mean, variance);
    //bd_probs->setRates(birth_rate, death_rate, true);
    bd_probs = new EdgeDiscBDProbs(&DS, birth_rate, death_rate);

    // IKRAM: changing it here, rather than saving, just operate
    // on it at the same time, and throw it off

    // check if fixed case, and then find the LCA of desinated nodes
    //cout << "outside" << endl;
    if (args_info.fix_flag) {
        //cout << "within fix_flag" << endl;
        string seqs;
        seqs.append(args_info.leaves_arg);
        cout << "inside" << endl;

        // if the fix flag is ON, but sequences, who's lca is to fixed, isn't given, 
        // show error message and exit
        if (seqs == "none") {
            cerr << "Error while fixing nodes" << endl;
            cerr << "If fixing flag is ON, you should provide the list of sequences who's LCA is to be fixed" << endl;
            cerr << "Exiting now..." << endl;
            exit(EXIT_FAILURE);
        }

        Tokenizer seqTokens(";");
        seqTokens.setString(seqs);
        SetOfNodes nodes;
        while (seqTokens.hasMoreTokens()) {
            string curToken = seqTokens.getNextToken();
            for (unsigned int i = 0; i < G.getNumberOfNodes(); i++) {
                if (curToken == G.getNode(i)->getName()) {
                    nodes.insert(G.getNode(i));
                    break;
                }
            }
        }

        lca = find_lca(nodes, G);
        fixedNodes = new UnsignedVector(G.getNumberOfNodes());
        (*fixedNodes)[lca->getNumber()] = 1;
        gsr = new EdgeDiscGSR(&G, &DS, &gs_map, gamma, bd_probs, fixedNodes);
    } else
        gsr = new EdgeDiscGSR(&G, &DS, &gs_map, gamma, bd_probs);

    //cout << gsr->getDebugInfo(false,false,false) << endl;
    //cout << "Testing for placement probabilities" << endl;

    //for(int i = 0; i < gsr->getTree().getNumberOfNodes(); i++){
    //    cout << "Placement for node " << i << " is " << gsr->getTotalPlacementDensity(gsr->getTree().getNode(i)) << endl;
    //}


    //cout << "\n And total tree probability is " << gsr->calculateDataProbability() << endl;    
    //cout << endl << endl;

    // printing the values of edgediscgsr components
    //cout << "Gene Tree: " << gsr->getTree() << endl;
    //cout << "BD Probs: " << gsr->getEdgeDiscBDProbs()->getBirthRate() << " and " << gsr->getEdgeDiscBDProbs()->getDeathRate() << endl;
    //cout << "Mean and Variance: " << gsr->getEdgeRateDensity()->getMean() << " and " << gsr->getEdgeRateDensity()->getVariance() << endl;
    //cout << "data probability (primeorthology) is " << gsr->calculateDataProbability().val() << endl;
}

/*
 * Find all virtual vertices in the state and update counts.
 * The procedure is recursive and starts at the
 * root of the current gene tree.
 * @param vertices virtual vertex map
 * @param state EdgeDiscGSR object containing all the information
 * @param ID id to leaf name map
 * @param inv_ID leaf name to id map
 */

void
find_vertices_single(vvmap &vertices,
        Tree &tree,
        //EdgeDiscGSR* state,
        map<int, string> &ID,
        map<string, int> &inv_ID) {


    GeneSet g(0);
    //find_vertices_rec(vertices, state->getTree().getRootNode(), state, ID, inv_ID, g);
    find_vertices_rec(vertices, tree.getRootNode(), ID, inv_ID, g);
}

/**
 * find_vertices_rec
 *
 * Finds all virtual vertices in one GSR state recursively.
 *
 * @param vertices Storage for the set of virtual vertices upon return, including the count, for each
 *                  vertex, of how many states it was found in.
 * @param node Current node.
 * @param state The GSR state to consider.
 * @param ID id to gene name map.
 * @param inv_D gene name to id map.
 * @param gs_map gene to species map
 * @param part Gene set to add found leaves to.
 */
void
find_vertices_rec(vvmap &vertices,
        Node *node,
        //EdgeDiscGSR *state,
        map<int, string> &ID,
        map<string, int> &inv_ID,
        GeneSet &part) {
    /*Error check*/
    if (node == NULL) {
        throw AnError("Programming error in find_vertices_rec: node == NULL");
    }

    /*
     * If we have reached a leaf, add gene to part of parent vertex.
     */
    if (node->isLeaf()) {
        part.addGene(inv_ID[node->getName()]);
        return;
    }

    /*
     * Non-leaf node.
     * The general idea is to create a new virtual vertex and construct its
     * descendant parts recursively in each subtree of the current node.
     * Then we check if the virtual vertex has been found before. If there is an
     * old record of the virtual vertex, then we update it by:
     * - increasing the count of the virtual vertex.
     *
     * Otherwise we enter the newly found virtual vertex with count 1 to 
     * the set of virtual vertices.
     *
     * Also we add all genes found recursively to the descendant part of the
     * parent of the current node.
     */

    //Create new virtual vertex.
    VirtualVertex vv(ID.size());

    //Create descendant parts and construct them recursively.
    GeneSet left(ID.size()), right(ID.size());
    //find_vertices_rec(vertices, node->getLeftChild(), state, ID, inv_ID, left);
    //find_vertices_rec(vertices, node->getRightChild(), state, ID, inv_ID, right);
    find_vertices_rec(vertices, node->getLeftChild(), ID, inv_ID, left);
    find_vertices_rec(vertices, node->getRightChild(), ID, inv_ID, right);

    //Add descendant parts to the virtual vertex.
    vv.setLeftDescendantPart(left);
    vv.setRightDescendantPart(right);

    //Add genes found to the descendant part of the parent to the current node.
    if (!(node->isRoot())) {
        part.addGeneSet(left);
        part.addGeneSet(right);
    }

    //Update virtual vertex set
    /*
     * IMPLEMENTATION NOTE:
     * If vv does not exist in vertices, then the statement vertices[vv]
     * adds vv and the count and cumulative speciation probability
     * both defaults to 0. See documentation for std::map for more info.
     */
    vertices[vv].first += 1.0;
}

/**
 * find_high_posterior_vertices
 *
 * Finds all virtual vertices with a posterior probability over some threshold.
 *
 * @param vertices A set of virtual vertices to sift. Assumes count has already been calculated.
 * @param hp_vertices Storage for the found vertices upon return.
 * @param threshold_count Posterior threshold. Note that this is a number reperesenting a count
 *                          of states a vertex must have occured in to qualify as a high posterior
 *                          vertex. It is NOT a probability.
 */
void
find_high_post_vertices(vvmap &vertices,
        vector<vvmap::iterator> &hp_vertices,
        float threshold_count) {
    for (vvmap::iterator it = vertices.begin(); it != vertices.end(); it++) {
        //cout << "Value is " << it->second.first.val() << endl;
        if (it->second.first.val() > threshold_count) {
            hp_vertices.push_back(it);
        }
    }
}

/**
 * find_lca
 *
 * Finds and returns the lca node of a set of nodes.
 *
 * @param nodes A vector of nodes
 * @param T Tree in which the nodes are found.
 *
 */
Node*
find_lca(SetOfNodes &nodes, Tree &T) {
    Node *lca = nodes[0];
    for (unsigned int i = 1; i < nodes.size(); i++) {
        lca = T.mostRecentCommonAncestor(lca, nodes[i]);
    }
    return lca;
}

/* create hashmap from all leaf pairs and return it */

map<string, double> get_all_leaf_pairs(Tree G) {
    map<string, double> leaf_pairs;
    locale loc;

    vector<Node *> nodes = G.getAllNodes();
    for (unsigned int i = 0; i < nodes.size(); i++) {
        if (!nodes[i]->isLeaf())
            break;
        string lnode = nodes[i]->getName();
        for (unsigned int j = i + 1; j < nodes.size(); j++) {
            if (!nodes[j]->isLeaf())
                break;

            string rnode = nodes[j]->getName();
            std::transform(lnode.begin(), lnode.end(), lnode.begin(), ::tolower);
            std::transform(rnode.begin(), rnode.end(), rnode.begin(), ::tolower);
            lnode.erase(lnode.find_last_not_of(" \n\r\t") + 1);
            rnode.erase(rnode.find_last_not_of(" \n\r\t") + 1);

            string lpair;
            if (lnode.compare(rnode) < 0)
                lpair = nodes[i]->getName() + "," + nodes[j]->getName();
            else
                lpair = nodes[j]->getName() + "," + nodes[i]->getName();
            leaf_pairs[lpair] = 0.0;
        }
    }
    return leaf_pairs;
}

/* add speciation probability to leaf set */

void add_hp_vertices_to_leaf_set(SetOfNodes lnodes, SetOfNodes rnodes,
        map<string, double> &leaf_pairs, double prob) {
    for (unsigned int i = 0; i < lnodes.size(); i++) {
        for (unsigned int j = 0; j < rnodes.size(); j++) {
            string leftNodeName = lnodes[i]->getName();
            string rightNodeName = rnodes[j]->getName();
            std::transform(leftNodeName.begin(), leftNodeName.end(), leftNodeName.begin(), ::tolower);
            std::transform(rightNodeName.begin(), rightNodeName.end(), rightNodeName.begin(), ::tolower);
            leftNodeName.erase(leftNodeName.find_last_not_of(" \n\r\t") + 1);
            rightNodeName.erase(rightNodeName.find_last_not_of(" \n\r\t") + 1);
            string key;

            if (leftNodeName.compare(rightNodeName) < 0) {
                key = leftNodeName + "," + rightNodeName;
            } else {
                key = rightNodeName + "," + leftNodeName;
            }
            leaf_pairs[key] = leaf_pairs[key] + prob;
        }
    }
}

/**
 * calc_speciation_probs_from_file
 *
 * Read all trees from a prime output file, and calculate the speciation probability.
 *
 * @param samples Storage for sample objects
 * @param DS Discretized species tree needed to create sample objects
 * @param args_info Input parameter including the filenames for gene/species
 *                  tree files.
 *        
 */
void calc_speciation_probs_from_file(vector<vvmapel_pt> &hp_vertices,
        EdgeDiscTree &DS, map<int, string> &D, StrStrMap &gs_map, int &numStates,
        int &hl, struct gengetopt_args_info &args_info, map<string, double> &leaf_pairs) {
    ifstream sample_file; //File object for samples
    string curr_line; //Current line when reading samples    

    //map<string, double> leaf_pairs;

    int burn_in = 1;

    GammaDensity *gamma; //Density function
    EdgeDiscBDProbs *bd_probs; //Birth-death rates holder
    Tokenizer tokenizer(";");

    // default initialization
    gamma = new GammaDensity(DEFAULT_MEAN, DEFAULT_VARIANCE);
    bd_probs = new EdgeDiscBDProbs(&DS, DEFAULT_BIRTH_RATE, DEFAULT_DEATH_RATE);
    Tree G; //Gene tree
    Tree G2SL;
    map<int, Node*> gs_node_map;

    cout << "#################################################################" << endl;
    cout << "Starting to calculate speciation probabilities from states now..." << endl;
    cout << "#################################################################" << endl;

    ofstream spfile(args_info.gsr_spec_output_file_arg);

    spfile << "L N ";
    spfile << "GeneTree(tree);\t";

    foreach(vvmapel_pt it, hp_vertices) {
        ostringstream os;
        os << it->first.getLeftDescendantPart();
        string ldp = os.str();

        // reset the stream
        os.str("");

        os << it->first.getRightDescendantPart();
        string rdp = os.str();

        find_and_replace(ldp, " ", ".");
        find_and_replace(rdp, " ", ".");
        spfile << "vv_" << ldp << "_" << rdp << "(logfloat);\t";
    }
    spfile << "mean(logfloat);\t" << "variance(logfloat);\t";
    spfile << "birth(logfloat);\t" << "death(logfloat);\t\n";

    //int a = 0;
    bool first_state = true;
    /*Read samples and create states*/
    sample_file.open(args_info.inputs[INPUT_SEQUENCES_SAMPLES_FILE]);
    if (sample_file.is_open()) {
        //cout << "This is " << sample_file.tellg() << " bites" << endl;
        try {
            while (!sample_file.eof()) {
                //Skip lines of comments
                if (sample_file.peek() == '#') {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    continue;
                }

                // only take the samples after burn-in
                //burn_in++;
                cout << "sample number: " << burn_in << endl;
                if (burn_in <= args_info.burn_in_arg) {
                    sample_file.ignore(SAMPLE_LENGTH, '\n');
                    burn_in++;
                    continue;
                }

                //Get next line from file
                curr_line = "";
                getline(sample_file, curr_line);

                //Skip empty lines
                if (curr_line == "") {
                    cout << "line is empty... at speciation" << endl;
                    continue;
                }

                //Create density function
                gamma = NULL;
                bd_probs = NULL;
                EdgeDiscGSR *gsr = NULL;

                // get the likelihood here
                //Set tokenizer
                //tokenizer.setString("");
                tokenizer.setString(curr_line);

                //Skip info about likelihoods and number of iterations
                string llh = tokenizer.getNextToken();
                istringstream pieces(llh);
                string l_i;
                pieces >> l_i;
                spfile << l_i;
                pieces >> l_i;
                spfile << "\t" << l_i << "\t";

                //cout << "creating object from state for sample number " << endl;
                createObjectFromState(curr_line, DS, gs_map, gsr, G, bd_probs, gamma, hl, args_info);
                //cout << "tree probability = " << gsr->calculateDataProbability().val() << endl;

                if (first_state) {
                    cout << "constructing gs_node_map now: ";
                    //Get species tree for all states
                    G2SL = gsr->getDiscretizedHostTree()->getTree();
                    Tree gtree = gsr->getTree();
                    leaf_pairs = get_all_leaf_pairs(gtree);
                    cout << "number of leaves are " << gtree.getNumberOfLeaves() << endl;
                    cout << "number of all pairs in G are " << leaf_pairs.size() << endl;

                    //Create gene_id to species-leaf-node map                    
                    for (unsigned int i = 0; i < D.size(); i++) {
                        gs_node_map[i] = G2SL.findLeaf(gsr->getGSMap().find(D[i]));
                    }
                    first_state = false;
                    cout << "done..." << endl;
                }

                //cout << "starting to calculate spec prob for sample no: " << a << endl;
                try {
                    calc_speciation_single(gsr, hp_vertices, spfile, D,
                            DS.getTree(), gs_node_map, leaf_pairs);

                    cout << "computed orthology probability for sample no: " << burn_in << endl;
                    burn_in++;
                } catch (exception & ex) {
                    cout << "ERROR probably runtime: " << ex.what() << endl;
                }

                delete gamma;
                delete bd_probs;
                delete gsr;
            }
        } catch (exception & ex) {
            cout << "ERROR probably runtime: " << ex.what() << endl;
        }

        numStates = (burn_in - 1) - args_info.burn_in_arg;                
        
        cout << "No of states are " << numStates << endl;
        sample_file.close();
        spfile.close();
    } else {
        cerr << "Could not open file: " << sample_file << endl;
        exit(EXIT_FAILURE);
    }
}

void printVector(Tree &G, map<int, string> &ID, vector<unsigned> v) {
    for (vector<unsigned>::iterator i = v.begin(); i != v.end(); i++)
        cout << "for ID " << *i << ", gene is : " << G.findLeaf(ID[*i])->getName() << endl;
}

void calc_speciation_single(EdgeDiscGSR *state,
        vector<vvmapel_pt> &hp_vertices, ofstream &spfile,
        map<int, string> &ID, Tree S, map<int, Node*> gs_node_map, map<string, double> &leaf_pairs) {
    McmcSample sample;
    Tree G = state->getTree();
    //cout << G << endl;
    //cout << state->getDiscretizedHostTree()->getTree() << endl;

    //cerr << G << endl;
    TreeIO tree_reader;
    //tree_reader.setSourceString(G.print());
    sample.setTreeStrRep(tree_reader.writeGuestTree(G));
    sample.setBirthRate(state->getEdgeDiscBDProbs()->getBirthRate());
    sample.setDeathRate(state->getEdgeDiscBDProbs()->getDeathRate());
    sample.setMeanRate(state->getEdgeRateDensity()->getMean());
    sample.setVarianceRate(state->getEdgeRateDensity()->getVariance());

    map<string, double> vvProbs;

    foreach(vvmapel_pt it, hp_vertices) {
        //cerr << "\n\nworking with " << it->first << "\n\n\n";
        //Find lcas of descendant parts
        //cout << it->first << endl;
        vector<unsigned int> left_gene_ids = it->first.getLeftDescendantPart().getGeneIDs();
        //cout << "left_gene_ids" << endl;
        //printVector(G, ID, left_gene_ids);
        //cout<< "The size of left children is " << left_gene_ids.size() << endl;
        SetOfNodes left_nodes, right_nodes;
        vector<unsigned int> right_gene_ids = it->first.getRightDescendantPart().getGeneIDs();
        //cout << "right_gene_ids" << endl;
        //printVector(G, ID, right_gene_ids);

        foreach(int i, left_gene_ids) {
            //cout << "id = " << i << " and ID[i] = " << ID[i] << ", name = " << G.findLeaf(ID[i])->getName() << endl;
            left_nodes.insert(G.findLeaf(ID[i]));
        }
        Node *lca_left = find_lca(left_nodes, G);

        foreach(int i, right_gene_ids) {
            right_nodes.insert(G.findLeaf(ID[i]));
        }
        Node *lca_right = find_lca(right_nodes, G);

        /* Here we check the first condition as per the equation 4.6
         * on page 15 in thesis. If its 1, we proceed to find
         * o(v_alpha, v_beta) discussed in last paragraph of page 14.
         */
        //Check for independence
        if (independent(lca_left, lca_right)) {
            /**
             * We find speciation probability for current vertex by finding the
             * probability of placing the current node at the LCA in the species tree
             * of the genes in the descendant parts of the current virtual vertex.
             *
             */

            //Find LCA in species tree between all species having genes
            //in one of the descendant parts.
            //This can be optimized by only calculating this once during the first state.
            SetOfNodes species_leaves;
            //Left part

            foreach(int i, left_gene_ids) {
                species_leaves.insert(gs_node_map[i]);
            }
            //Right part

            foreach(int i, right_gene_ids) {
                species_leaves.insert(gs_node_map[i]);
            }

            //cout << species_leaves.strRep() << endl;

            Node *lca_s = find_lca(species_leaves, S);

            //Create "discretized" speciation node
            EdgeDiscretizer::Point point(lca_s, 0);

            //cout << S << endl;
            //cout << point.first << endl;

            //Calculate speciation probability
            Node *lca_g = G.mostRecentCommonAncestor(lca_left, lca_right);
            //cerr << "Finding placement of " << lca_g->getLeaves() << " over " << point.first->getNumber() << endl;
            //cout << "lca g: " << lca_g << endl;
            Probability speciation_prob = state->getPlacementProbability(lca_g, &point);
            //cout << "lca: " << lca_g->getNumber() << " : " << speciation_prob.val() << endl;
            //                cout << speciation_prob.val() << endl;

            if (speciation_prob.val() > 1.0) {
                cout << "Virtual vertex number is " << lca_g->getLeaves() << endl;
                cout << "PrIME-ORTHOLOGY: WARNING, Speciation prob > 1.0." << endl
                        << "speciation_prob: " << speciation_prob.val() << endl
                        << "state->getPlacementProbability(lca_g, &point): " << state->getPlacementProbability(lca_g, &point).val() << endl
                        << "state->calculateDataProbability(): " << state->calculateDataProbability().val() << endl;
                /*
                cout << state->getDebugInfo(false, false, true, true);
                cout << "Total placement probability of node " << lca_g->getNumber() << ": " << state->getTotalPlacementProbability(lca_g).val() << endl;
                cout << "Tree: " << state->getTree() << endl;
                cout << "Species tree: " << S << endl;
                 */
            }
            //Store probability
            it->second.second += speciation_prob;
            ostringstream os;
            os << it->first;
            string f = os.str();
            vvProbs[f] = speciation_prob.val();

            add_hp_vertices_to_leaf_set(left_nodes, right_nodes, leaf_pairs, speciation_prob.val());
            //cerr << "after independent, at it->first = " << os.str() << endl;
        }
    }

    sample.setVvProbs(vvProbs);
    //cout << sample.getStringRepresentation() << endl;
    //cout << "size of vvProbs is " << vvProbs.size() << endl;
    spfile << sample.getStringRepresentation() << endl;
}

/**
 * independent
 *
 * Decide if a pair of nodes are independent.
 *
 * @param lca_a Node 1
 * @param lca_b Node 2
 */
bool
independent(Node * lca_a, Node *lca_b) {
    return !(lca_a->dominates(*lca_b) || lca_b->dominates(*lca_a));
}

/*
 * replaces a substring with another given substring
 * 
 * @param source source string
 * @param find string to be find and replaced
 * @param replace string replacing "find"
 */
void find_and_replace(string &source, const string find, string replace) {
    size_t j;
    for (; (j = source.find(find)) != string::npos;) {
        source.replace(j, find.length(), replace);
    }
}

/*
 * normalizes high posterior probabilities
 * 
 * @param hp_vertices high posterior vertices
 * @param num_states number of states
 */
void normalize_hp_prob(vector<vvmapel_pt> &hp_vertices, int num_states) {
    //Normalize the probabilities by dividing with the number of states

    foreach(vvmapel_pt it, hp_vertices) {

        it->second.first /= static_cast<Real> (num_states);

        /* NOTE: Due to fixed tree case, sometime the error for posterior
         * is given, so changing below to 1.1 (temporary fix)
         */
        if (it->second.first.val() > 1.1) {
            cout << "ERROR: Posterior: " << it->second.first.val() << endl;
            cout << "nr_states: " << num_states << endl;
            cout << "Vertex: " << it->first << endl;
        }
        it->second.second /= static_cast<Real> (num_states);
        if (it->second.second.val() > 1.0) {
            cout << "ERROR: Orthology: " << it->second.second.val() << endl;
            cout << "nr_states: " << num_states << endl;
            cout << "Vertex: " << it->first << endl;
        }
    }
}

