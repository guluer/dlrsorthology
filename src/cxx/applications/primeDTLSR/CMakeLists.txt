include(${CMAKE_SOURCE_DIR}/src/cxx/cmake-include.txt)

conf_value_from_cmake(primeDTLSR Iterations 1000000) # Used in man-page, C++ code. See macro definition in trunk/cmake/macro.cmake
conf_value_from_cmake(primeDTLSR Thinning 100)
conf_value_from_cmake(primeDTLSR PrintFactor 1)
conf_value_from_cmake(primeDTLSR DiscTimestep 0.05)
conf_value_from_cmake(primeDTLSR DiscMinIvs 3)
conf_value_from_cmake(primeDTLSR SiteRateCats 1)
conf_value_from_cmake(primeDTLSR BirthDeathTransfer_first 0.1)
conf_value_from_cmake(primeDTLSR BirthDeathTransfer_second 0.1)
conf_value_from_cmake(primeDTLSR BirthDeathTransfer_third 0.1)
conf_value_from_cmake(primeDTLSR TransferCounts 0)
conf_value_from_cmake(primeDTLSR EdgeRateParams_first 1.0)
conf_value_from_cmake(primeDTLSR EdgeRateParams_second 1.0)
conf_value_from_cmake(primeDTLSR RunType MCMC)
conf_value_from_cmake(primeDTLSR SubstModel JTT)
conf_value_from_cmake(primeDTLSR EdgeRateDistr Gamma)

configure_file(primeDTLSR.1.cmake ${tmp_man_pages_dir}/primeDTLSR.1 @ONLY)

include_directories(${CMAKE_CURRENT_BINARY_DIR})


add_executable(${programname_of_this_subdir}
idolatrous.cc
)

target_link_libraries(${programname_of_this_subdir} prime-phylo prime-phylo-sfile)

prime_add_tests(${CMAKE_CURRENT_SOURCE_DIR}/tests)

install(TARGETS ${programname_of_this_subdir} DESTINATION bin)
