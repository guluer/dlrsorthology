<?xml version="1.0"?>
<article  
 id="manual">
  <articleinfo>
    <title>PrIME: Probabilistic Integrated Models of Evolution</title>
  </articleinfo>
  <sect1 id="introduction">
    <title>Introduction</title>
    <para>
PrIME is a software package for probabilistic integrated models of evolution. The homepage is located at <ulink url="http://prime.sbc.su.se/">http://prime.sbc.su.se</ulink>. It is written in the programming language C++.</para>
  </sect1>



  <sect1 id="download">
    <title>Download</title>
    <para>      
       Download the software from the <ulink url="http://sourceforge.net/projects/diagonalsw">sourceforge</ulink> project page.


 The latest version of PrIME is @PACKAGE_VERSION@. 
    </para>
  </sect1>

  <sect1 id="installation">
    <title>Installation</title>

    <sect2 id="installation_with_prebuilt_package">
      <title>Installation with prebuilt package</title>
<!--
    <sect3 id="installation_on_windows">
      <title>Installation on Windows with .exe file</title>
<para>
To install diagonalsw on Windows, first download the diagonalsw-@PACKAGE_VERSION@-win32.exe and then execute the file ( click on it ). 
    </para>
  </sect3>
-->

    <sect3 id="installation_on_ubuntu_and_debian">
      <title>Installation on Ubuntu and Debian</title>

<para>
To install diagonalsw on Ubuntu or Debian, first download the diagonalsw-@PACKAGE_VERSION@.deb  and then log in as root and  
<programlisting><![CDATA[
# dpkg -i diagonalsw-@PACKAGE_VERSION@.deb 
]]></programlisting>

    </para>
  </sect3>


    <sect3 id="installation_on_centos_and_fedora_linux">
      <title>Installation on CentOS and Fedora</title>

<para>
To install diagonalsw on Centos or Debian, first download the diagonalsw-@PACKAGE_VERSION@.Linux.rpm   and then log in as root and  
<programlisting><![CDATA[
# yum localinstall diagonalsw-@PACKAGE_VERSION@.Linux.rpm 
]]></programlisting>

    </para>
  </sect3> 





    <sect3 id="installation_MacOS_X">
      <title>Installation on Mac OS X</title>

<para>
To install diagonalsw on a Mac OS X v10.5 ( Leopard ) on a Mac computer with Intel cpu, first download the diagonalsw-@PACKAGE_VERSION@-MacOSX10.5.tar.gz   and then 
<programlisting><![CDATA[
$ tar xfz diagonalsw-@PACKAGE_VERSION@-MacOSX10.5.tar.gz  
]]></programlisting>

    </para>
<para>
To install diagonalsw on a Mac OS X v10.4 ( Tiger ) on a Mac computer with Intel cpu, first download the diagonalsw-@PACKAGE_VERSION@-MacOSX10.4.tar.gz   and then 
<programlisting><![CDATA[
$ tar xfz diagonalsw-@PACKAGE_VERSION@-MacOSX10.4.tar.gz
]]></programlisting>

    </para>
  </sect3>




 </sect2>




    <sect2 id="building_from_source">
      <title>Building from source</title>

    <sect3 id="building_from_source_on_unix">
      <title>Building from source on Unix</title>

      <para>To build diagonalsw on Unix ( e.g. Linux, MacOSX, CygWin ) you need to have this installed
        <itemizedlist mark="bullet">
          <listitem>
            <para>
              <ulink url="http://www.cmake.org">cmake</ulink>
            </para>
          </listitem>
<!--
          <listitem>
            <para>
              <ulink url="http://xmlsoft.org/">libxml2</ulink>
            </para>
          </listitem>
-->
          <listitem>
            <para>
                <ulink url="http://www.threadingbuildingblocks.org/">Intel Intel® Threading Building Blocks (TBB)</ulink>
            </para>
          </listitem>

        </itemizedlist>
      </para>
      <para>
If you have installed Intel Intel® Threading Building Blocks (TBB) into a non-standard location, please first source the <filename>tbbvars.sh</filename>, in other words some like this
<programlisting><![CDATA[
$ . /home/myuser/tbb22_20090908oss/build/linux_intel64_gcc_cc4.3.3_libc2.9_kernel2.6.28_release/tbbvars.sh
]]></programlisting>




If you have the diagonalsw source code in the directory <filename>/tmp/diagonalsw</filename> and you want to install diagonalsw into the directory <filename>/tmp/install</filename>, 


first run <command>cmake</command> then <command>make</command> and then <command>make install</command>
<programlisting><![CDATA[
$ mkdir /tmp/build
$ cd /tmp/build
$ cmake -DCMAKE_INSTALL_PREFIX=/tmp/install /tmp/source && make && make install
esjolund@csbl05:/tmp/build$   ~/cmake-2.6.4-Linux-i386/bin/cmake -DCMAKE_INSTALL_PREFIX=/tmp/inst -DCMAKE_PREFIX_PATH=~/gengetopt/inst/bin/  ~/permssw/trunk && make 
-- The C compiler identification is GNU
-- Check for working C compiler: /usr/bin/gcc
-- Check for working C compiler: /usr/bin/gcc -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Configuring done
-- Generating done
-- Build files have been written to: /tmp/build
[  5%] Generating smith_waterman_sse_word.2loadEFH.c
[ 10%] Generating cmdline_diagonalsw.c, cmdline_diagonalsw.h
[ 15%] Generating smith_waterman_sse_byte.2load_HFsave8thnormal.c
[ 20%] Generating smith_waterman_sse_word.2loadnormal.c
[ 25%] Generating smith_waterman_sse_byte.2load_HFsave8thH.c
[ 30%] Generating smith_waterman_sse_word.2loadH.c
[ 35%] Generating smith_waterman_sse_byte.2load_HFsave8thEFH.c
Scanning dependencies of target diagonalsw
[ 40%] Building C object src/c/CMakeFiles/diagonalsw.dir/read_matrix.c.o
[ 45%] Building C object src/c/CMakeFiles/diagonalsw.dir/read_sequence.c.o
[ 50%] Building C object src/c/CMakeFiles/diagonalsw.dir/diagonalsw.c.o
[ 55%] Building C object src/c/CMakeFiles/diagonalsw.dir/cmdline_diagonalsw.c.o
[ 60%] Building C object src/c/CMakeFiles/diagonalsw.dir/create_profile.c.o
[ 65%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_reference_impl.c.o
[ 70%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_byte.2load_HFsave8thnormal.c.o
[ 75%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_word.2loadnormal.c.o
[ 80%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_byte.2load_HFsave8thH.c.o
[ 85%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_word.2loadH.c.o
[ 90%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_byte.2load_HFsave8thEFH.c.o
[ 95%] Building C object src/c/CMakeFiles/diagonalsw.dir/smith_waterman_sse_word.2loadEFH.c.o
[100%] Building C object src/c/CMakeFiles/diagonalsw.dir/sse_funcs.c.o
Linking C executable diagonalsw
[100%] Built target diagonalsw
]]></programlisting>

If you want to build the html documentation ( i.e. this page ) you need to pass the -DBUILD_DOCBOOK=ON option to <application>cmake</application>.
      </para>
    </sect3>


    <sect3 id="building_install_packages">
      <title>Building install packages</title>
      <para>This is section is mainly intended for package maintainers</para>

<!--
    <sect5 id="building_an_exe_file_for_windows">
      <title>Building an .exe file for Windows</title>
<para>
To build the diagonalsw nullsoft installer package ( diagonalsw-@PACKAGE_VERSION@-win32.exe ) 
you need to have this installed
        <itemizedlist mark="bullet">
          <listitem>
            <para>
              <ulink url="http://www.cmake.org">cmake</ulink>
            </para>
          </listitem>
          <listitem>
            <para>
              <ulink url="http://www.mingw.org">mingw</ulink>
            </para>
          </listitem>
          <listitem>
            <para>
              <ulink url="http://www.mingw.org/wiki/msys">msys</ulink>
            </para>
          </listitem>

          <listitem>
            <para>
              <ulink url="http://gnuwin32.sourceforge.net/packages/wget.htm">wget</ulink>
            </para>
          </listitem>

          <listitem>
            <para>
              <ulink url="http://nsis.sourceforge.net">Nullsoft Scriptable Install System</ulink>
            </para>
          </listitem>
        </itemizedlist>
on your Windows machine.
      </para>
      <para>

Just open up a msys bash shell 
<programlisting><![CDATA[
$ mkdir tmpbuild
$ cd tmpbuild
$ cmake path/to/the/diagonalsw/source/code  -DSTATIC=ON -G "MSYS Makefiles" && make win32installer
]]></programlisting>

The source code for gengetopt, libz and libxml will be automatically downloaded and built statically.
      </para>
    </sect5>
-->

    <sect5 id="building_install_package_rpm">
      <title>Building an rpm</title>
<para>
On a CentOS or Fedora machine, first log in as root and install the dependencies
<programlisting><![CDATA[
# yum install xmlto cmake gcc gengetopt
]]></programlisting>

Check that cmake is version 2.6 or later
<programlisting><![CDATA[
$ cmake --version
cmake version 2.6-patch 0
]]></programlisting>
If it is older you could download a cmake binary directly from <ulink url="http://www.cmake.org">www.cmake.org</ulink>

<programlisting><![CDATA[
$ mkdir /tmp/build
$ cd /tmp/build
$ cmake -DCMAKE_INSTALL_PREFIX=/ -DBUILD_DOCBOOK=ON /tmp/source && make package
]]></programlisting>

      </para>
    </sect5>

    <sect5 id="building_install_package_deb">
      <title>Building a deb package</title>
<para>
On a Debian or Ubuntu machine, first log in as root and install the dependencies
<programlisting><![CDATA[
# apt-get install cmake gengetopt gcc
]]></programlisting>

Check that cmake is version 2.6 or later
<programlisting><![CDATA[
$ cmake --version
cmake version 2.6-patch 0
]]></programlisting>
If it is older you could download a cmake binary directly from <ulink url="http://www.cmake.org">www.cmake.org</ulink>. Now build the deb package.


<programlisting><![CDATA[
$ mkdir /tmp/build
$ cd /tmp/build
$ cmake -DCMAKE_INSTALL_PREFIX=/ -DBUILD_DOCBOOK=ON /tmp/source && make package
]]></programlisting>

      </para>
    </sect5>


    <sect5 id="building_install_package_for_macosx">
      <title>Building install package for MacOS X</title>
<para>
To build the diagonalsw install package for MacOS X
you need to have this installed
        <itemizedlist mark="bullet">
          <listitem>
            <para>
              <ulink url="http://www.cmake.org">cmake</ulink>
            </para>
          </listitem>

<!--
          <listitem>
            <para>
              <ulink url="http://www.gnu.org/software/wget/">wget</ulink>
            </para>
          </listitem>

-->
        </itemizedlist>
on your MacOS X computer.
      </para>


      <para>

Check that cmake is version 2.6 or later
<programlisting><![CDATA[
$ cmake --version
cmake version 2.6-patch 0
]]></programlisting>


<programlisting><![CDATA[
$ mkdir /tmp/build
$ cd /tmp/build
$ cmake -DSTATIC=ON -DCPACK_GENERATOR="TGZ" /tmp/source && make package
]]></programlisting>

      </para>
    </sect5>




    </sect3>



    </sect2>





  </sect1>




  <sect1 id="libprime">
    <title>libprime</title>
    <para>      
      <ulink url="doxygen/html/index.html">Documentation for libprime</ulink>   ( generated by Doxygen ) 
    </para>
  </sect1>



  <sect1 id="usage">
    <title>Usage</title>




<sect2 id="primeGSR_usage">
    <title>primeGSR</title>
  <sect3 id="primeGSR_command_line_options">
    <title>primeGSR command line options</title>
<para>

Type <userinput>primeGSR --help</userinput> to see the command line options

<programlisting><repl shell-cmd="primeGSR --help"/></programlisting>
</para></sect3>


  <sect3 id="primeGSR_input_file_formats">
    <title>primeGSR input file formats</title>

<para>

<table frame='all'><title>primeGSR input file formats</title>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<thead>
<row>
<entry>file format</entry>
<entry>short option</entry>
<entry>description</entry>
</row>
</thead>
<tbody>
<row>
<entry>fasta format</entry>
<entry>-I xml</entry>
<entry><xref linkend="fasta_format"/></entry>
</row>
<row>
<entry>Integer similarity matrix file ( scoring matrix  )</entry>
<entry>-s</entry>
<entry><xref linkend="Integer_similarity_matrix_file"/></entry>
</row>
</tbody>
</tgroup>
</table>
</para>
  </sect3>


  <sect3 id="primeGSR_output_formats">
    <title>primeGSR output file formats</title>


<para>

<table frame='all'><title>primeGSR output file formats</title>
<tgroup cols='3' align='left' colsep='1' rowsep='1'>
<thead>
<row>
<entry>file format</entry>
<entry>short option</entry>
<entry>description</entry>
</row>
</thead>
<tbody>
<row>
<entry>primeGSR sequence XML format</entry>
<entry>-O xml</entry>
<entry><xref linkend="primeGSR_distance_matrix_xml_format"/></entry>

</row>
<row>
<entry>phylip distance matrix format</entry>
<entry>-O phylip_dm</entry>
<entry><xref linkend="phylip_distance_matrix_format"/></entry>

</row>
</tbody>
</tgroup>
</table>
</para>
  </sect3>




  <sect3 id="primeGSR_examples">
    <title>Examples</title>
<para>
<example id="primeGSR_phylip_multialignment"><title>primeGSR with input in file Phylip multialignment format</title><para>
We use the file described in <xref linkend="seq.phylip_multialignment"/> as input file. 
The file has two datasets so we pass the option <userinput>-r 2</userinput> to <application>primeGSR</application>. Per default the output is given in XML format 

<programlisting><repl shell-cmd="cat file2"/></programlisting>

</para>
</example>
<example id="primeGSR_db.fasta"><title>primeGSR with input in file fasta format</title><para>
We use the file described in <xref linkend="seq.fasta"/> as input file. 
Per default the output is given in XML format 

<programlisting><repl shell-cmd="primeGSR --help"/></programlisting>

</para>
</example>



<example id="example_primeGSR_seq_xml"><title>primeGSR with input file in XML format</title><para>



We use the file described in <xref linkend="seq.xml"/> as input file. 

<note><para>The -r option can only be used if the input is in phylip_multialignment format. <application>primeGSR</application> will for XML files compute all data sets ( runs ). Fasta files can only contain one data set so the -r option does not make any sense there.</para></note>

<programlisting><repl shell-cmd="primeGSR --help"/></programlisting>


</para>
</example>



      </para>

    </sect3>

    </sect2>





    </sect1>

  <sect1 id="file_formats">
    <title>File formats</title>
    <para>      
This software package handles the following file formats
</para>


  <sect2 id="integer_similarity_matrix_file_format">
    <title>Integer similarity matrix file format ( scoring matrix file format )</title>
<para>

<example id="seq.xml"><title>blosum62.mat, an example file in the Integer similarity matrix file format</title><para>

The example file <ulink url="example_files/blosum62.mat">blosum62.mat</ulink> contains 

<programlisting><xi:include xmlns:xi="http://www.w3.org/2001/XInclude" parse="text" encoding="UTF-8" href="${CMAKE_CURRENT_SOURCE_DIR}/example_files/blosum62.mat">
<xi:fallback>
   couldn't xinclude file
</xi:fallback>
</xi:include></programlisting>
</para></example>

</para></sect2>


  <sect2 id="fasta_format">
    <title>Fasta format</title>
<para>
Read more about the <ulink url="http://en.wikipedia.org/wiki/Fasta_format">Fasta format</ulink> on Wikipedia.
<!--The parser will take the whole header line as the sequence identifier name, i.e. all characters after the greater-than character ( ">" ). -->





<example id="db.fasta"><title>db.fasta, an example file in fasta format</title><para>
The example file <ulink url="example_files/db.fasta">db.fasta</ulink> contains
<programlisting><xi:include xmlns:xi="http://www.w3.org/2001/XInclude" parse="text" encoding="UTF-8" href="${CMAKE_CURRENT_SOURCE_DIR}/example_files/db.fasta">
<xi:fallback>
   couldn't xinclude file
</xi:fallback>
</xi:include></programlisting>

</para></example>


</para></sect2>


<!--
<![CDATA[
  <sect2 id="diagonalsw_distance_matrix_xml_format">
    <title>Diagonalsw distance matrix XML format</title>
<para>
The Diagonalsw sequence XML format is chosen by the option <userinput>-O xml</userinput> to diagonalsw and the option <userinput>-I xml</userinput> to fnj. 

Type <userinput>diagonalsw -print-relaxng-output</userinput> to see its relaxng schema

<programlisting><repl shell-cmd="diagonalsw -help"/></programlisting>

The Relax NG schema specifies that the extrainfo element is optional and can be inserted as a child to a seq element. The extrainfo element may contain any content.

<example id="dm.xml"><title>dm.xml, an example file in  Diagonalsw distance matrix XML format</title><para>
The example file <ulink url="example_files/dm.xml">dm.xml</ulink> contains

<programlisting><xi:include xmlns:xi="http://www.w3.org/2001/XInclude" parse="text" encoding="UTF-8" href="${CMAKE_CURRENT_SOURCE_DIR}/example_files/dm.xml">
<xi:fallback>
   couldn't xinclude file
</xi:fallback>
</xi:include></programlisting>
</para></example>


</para></sect2>
]]>
-->


  </sect1>






<bibliography>
    <title>References</title>

<bibliomixed id="farrar:2006">


  <bibliomset relation="article">
  

    <author><firstname>Michael</firstname> <surname>Farrar</surname></author>
    <title role="article">Striped Smith-Waterman speeds database searches six times over other SIMD implementations</title>.
  </bibliomset>
  <bibliomset relation="journal">
    <title>Bioinformatics</title> 
   <volumenum>23</volumenum>(<issuenum>2</issuenum>):<pagenums>156-161</pagenums>, <pubdate>2006</pubdate></bibliomset>.
</bibliomixed>



<bibliomixed  id="szalkowski:2008">
  <bibliomset relation="article">
 
<!-- <abbrev id="hughey:1996.abbrev">Hughey:1994</abbrev>-->


    <author><firstname>Adam</firstname> <surname>Szalkowski</surname></author>, <author><firstname>Christian</firstname>, <surname>Ledergerber</surname></author> <author><firstname>Philipp</firstname>, <surname>Krähenbühl</surname></author> <author><firstname>Christophe</firstname>, <surname>Dessimoz</surname></author> 
    <title role="article">SWPS3 - fast multi-threaded vectorized Smith-Waterman for IBM Cell/B.E. and 86/SSE</title>.
  </bibliomset>
  <bibliomset relation="journal">
    <title>BMC Research Notes</title>,
    <issuenum>1</issuenum>:<pagenums>107</pagenums>, <pubdate>2008</pubdate></bibliomset>.
</bibliomixed>


    </bibliography>





</article>




