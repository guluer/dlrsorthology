prime-phylo has become a big software project. In order to keep order of things, we need to follow some development practices:

* If you incorporate source code into the prime-phylo software project from some other open source software project, you need to verify that that software license is compatible with the prime-phylo software license. You also need to edit the file:
  https://svn.sbc.su.se/repos/PrIME/trunk/COPYING
  to document the added source code (license and copyright)

* The directory
  https://svn.sbc.su.se/repos/PrIME/trunk/src/cxx/libraries/prime
  has become quite big. Only check in new files here that are actually in use.

* About keeping the release source tarball in good shape:
  If you spot files that are outdated and not used anymore please move them to 
  https://svn.sbc.su.se/repos/PrIME/trunk/src/cxx/libraries/prime/deprecated_files/manually_picked/

  If you spot files that are of little use for an end user of prime-phylo, and hence should not be included in the released source tarball (tar.gz), you could could move the files here:
  https://svn.sbc.su.se/repos/PrIME/trunk/excluded_from_release

  Alternatively could also create a new directory for your files that should not be included in the released source tarball. After that you need to edit 
  https://svn.sbc.su.se/repos/PrIME/trunk/CMakeLists.txt 
  and adjust the line:
  set(CPACK_SOURCE_IGNORE_FILES "\\\\.svn/" "/deprecated_files/" "/docbook/" "/Scripts/" "/primeOrthology/" "/.cdtproject" "/excluded_from_release/" "/.cproject" "/.project")

  You could use the same technique if you want a directory where to put some experimental code. You could then add something like this
  into CMakeLists.txt:

  if(esjolund_experimental_code)
     add_subdirectory(experimental_code)
  endif()

  To actually build the experimental code you need pass a variable to cmake:

  cmake -Desjolund_experimental_code=on ...

* The steps involved in making a new release: 
    edit prime-trunk/excluded_from_release/debian/changelog 
    edit prime-trunk/ChangeLog 
    edit prime-trunk/CMakeLists and adjust the version numbering

       set(CPACK_PACKAGE_VERSION_MAJOR "1")
       set(CPACK_PACKAGE_VERSION_MINOR "0")
       set(CPACK_PACKAGE_VERSION_PATCH "7")

    create a source package with:

    dir=`mktemp -d`
    cd $dir
    cmake /path/to/prime-trunk && make package_source

    create a DEB package on a computer with VirtualBox and Vagrant installed:

    nr_of_cores=5
    dir=`mktemp	-d`
    cd $dir
    python /path/to/prime-trunk/excluded_from_release/packaging/debian/build_all_ubuntu_and_debian_packages.py  /path/to/prime-trunk $nr_of_cores

    (You might want to adjust build_all_ubuntu_and_debian_packages.py to build for a specific Debian or Ubuntu version)




  

  




